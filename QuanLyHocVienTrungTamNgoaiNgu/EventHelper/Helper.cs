﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventHelper
{
    public class Helper
    {

        public static void SetHandHover(Control c)
        {
            Control f = c;
            while (!(f is Form) && f.Parent != null)
                f = f.Parent;
            c.MouseEnter += (s,e) => SetHandHover_MouseEnter(f,c);
            c.MouseLeave += (s,e) => SetHandHover_MouseLeave(f,c);
        }

        public static void SetDoubleClickItemGridView(DataGridView grid, Action ac)
        {
            grid.CellDoubleClick += (s,e) => ac();
        }

        private static void SetHandHover_MouseLeave(Control f, Control c)
        {
            if (!(f is Form))
                f = c;

            while (!(f is Form) && f.Parent != null)
                f = f.Parent;
            if (f is Form)
            {
                (f as Form).Cursor = Cursors.Default;
                Application.DoEvents();
            }
        }

        private static void SetHandHover_MouseEnter(Control f, Control c)
        {
            if (!(f is Form))
                f = c;

            while (!(f is Form) && f.Parent != null)
                f = f.Parent;
            if (f is Form)
            {
                (f as Form).Cursor = Cursors.Hand;
                Application.DoEvents();
            }
        }

        public static bool TryCatch(Action @try)
        {
            return TryCatch(@try, () => { });
        }

        public static object TryCatchReturnValue(Func<object> func)
        {
            try
            {
                return func();
            }catch(Exception e)
            {
                //MessageBox.Show("Xảy ra lỗi trong quá trình xử lý !\n" + e.Message, "Thông báo", MessageBoxButtons.OK);
            }
            return null;

        }

        public static bool TryCatch(Action @try, Action @catch)
        {
            try
            {
                @try();
            }
            catch (Exception e)
            {
                @catch();
                //MessageBox.Show("Xảy ra lỗi trong quá trình xử lý !\n"+e.Message, "Thông báo", MessageBoxButtons.OK);
                return false;
            }
            return true;
        }

        public static void MessageInfomation(string content)
        {
            if (!content.EndsWith(".") && !content.EndsWith("?") && !content.EndsWith("!"))
                content += ".";
            MessageBox.Show(content, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static void MessageLessPermision()
        {
            MessageInfomation("Bạn không đủ quyền hạn để thực hiện chức năng này");
        }

        public static void MessageDefaultSettings()
        {
            MessageInfomation("Không thể thay đổi thông tin mặc định của hệ thống");
        }

        public static void MessageInfomationYesNo(string content, Action clickYes, Action clickNo)
        {
            if (!content.EndsWith(".") && !content.EndsWith("?") && !content.EndsWith("!"))
                content += ".";
            if (MessageBox.Show(content, "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information).Equals(DialogResult.Yes)){
                clickYes();
            }
            else
                clickNo();
        }

        public static void MessageInfomationYesNo(string content, Action clickYes)
        {
            MessageInfomationYesNo(content, clickYes, () => { });
        }

        public static void MessageLessInformation()
        {
            MessageBox.Show("Hãy cung cấp đầy đủ thông tin cần thiết.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static void MessageMissRequest()
        {
            MessageInfomation("Thao tác hoặc yêu cầu không hợp lệ");
        }

        public static void MessageNoChangedInformation()
        {
            MessageBox.Show("Không có bất kỳ thay đổi nào để cập nhật.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }



    }
}
