﻿USE QUANLY_HOCVIEN
GO
---#0 Lấy họ người dùng
create function Get_Ho(@ho_ten nvarchar(50))
returns nvarchar(50)
as
	begin
		declare @s nvarchar(50)
		set @s = substring(@ho_ten,0,LEN(@ho_ten)+1 - charindex(' ',reverse(@ho_ten)))
		return @s
	end
go

create function Get_Ten(@ho_ten nvarchar(50))
returns nvarchar(50)
as
	begin
		declare @s nvarchar(50)
		set @s = reverse(substring(reverse(@ho_ten),0,charindex(' ',reverse(@ho_ten))))
		return @s
	end
go

--#1 Đang nhập
create proc DangNhap(@tentaikhoan varchar(20), @matkhau varchar(50))
as
begin
	declare @soluong int
	set @soluong  = (select count(*) from 
					(select * from NGUOIDUNG
					where MATAIKHOAN = @tentaikhoan and MATKHAU = @matkhau and HOATDONG = 1) as temp)
	select(@soluong)
end
GO

--#2 hiên màn hình chức năng - done
create function dbo.GiaoDienTheoTaiKhoan (@mataikhoan char(6))
returns table
as
	return 
		select MAGIAODIEN from NHOMNGUOIDUNG_GIAODIEN
		WHERE MANHOMNGUOIDUNG = 
		(
			select MANHOMNGUOIDUNG from NGUOIDUNG_NHOMNGUOIDUNG
			where MATAIKHOAN = @mataikhoan
		)
GO

--#3 hiển thị các màn hình chức năng ko cho phép
create proc dbo.GiaoDienKhongChoPhepTheoTaiKhoan(@mataikhoan char(6))
as
	select * from giaodien
	where MAGIAODIEN
	not in
	(select * from   dbo.GiaoDienTheoTaiKhoan(@mataikhoan))
GO

---#4 HIỂN THỊ LỊCH DẠY CỦA GIẢNG VIÊN
create proc dbo.HienThiLichDayGiangVien(@taikhoan char(6))
as
	SELECT MALICHHOC, MACA,NGAYHOC,MACOSO,MAPHONG,MALOPHOC
	FROM LICHHOC   
	WHERE MALOPhoc IN (SELECT MALOPHOC from lophoc
						where magiangvien = @taikhoan)
GO

---#5 HIỂN THỊ LỊCH HỌC CỦA HỌC VIÊN
create proc dbo.HienThiLichHocHocVien(@taikhoan char(6))
as
	SELECT MALICHHOC,LOPHOC.MALOPHOC,TENKHOAHOC,CAHOC.MACA,CAHOC.GIOBATDAU,CAHOC.GIOKETTHUC,NGAYHOC,MACOSO,MAPHONG
	FROM LICHHOC,CAHOC,KHOAHOC,LOPHOC
	WHERE CAHOC.MACA = LICHHOC.MACA
	AND
	KHOAHOC.MAKHOAHOC = LOPHOC.MAKHOAHOC
	AND
	LOPHOC.MALOPHOC = LICHHOC.MALOPHOC
	AND
	LOPHOC.MALOPhoc IN (SELECT MALOPhoc from TT_GHIDANH
						where MAHOCVIEN = @taikhoan)
GO

---#6 Kết quả học tập của một học viên - done
create proc dbo.HienThiKQHTHocVien(@trang int, @soluong int, @mahocvien char(6))
as
	declare @start int
	set @start = (@trang-1)*@soluong
	select d.MALOPHOC 'MÃ LỚP HỌC', k.TENKHOAHOC 'TÊN KHOÁ HỌC', ld.TENLOAIDIEM 'LOẠI ĐIỂM', d.LAN 'LẦN', d.DIEM 'ĐIỂM'
	 from DIEM d, LOPHOC l, KHOAHOC k, LOAIDIEM ld
	 where d.MAHOCVIEN = @mahocvien
	 and d.MALOPHOC = l.MALOPHOC
	 and l.MAKHOAHOC = k.MAKHOAHOC
	 and ld.MALOAIDIEM = d.MALOAIDIEM
	 order by l.MALOPHOC
	 offset @start rows
	 fetch next @soluong rows only
GO

---#7 Hiện thông tin người dùng và nhóm quyền theo trang
create function dbo.HienThiThongTinNguoiDungVaNhomNguoiDung(@trang int null, @soluong int null)
returns table
as
return
	select tb1.[MÃ TÀI KHOẢN], tb1.[TÊN NGƯỜI DÙNG], tb1.[MÃ NHÓM], tb2.TENNHOMNGUOIDUNG 'TÊN NHÓM', 
	tb1.[HOẠT ĐỘNG]
	from
		(select * from dbo.ThongTinNguoiDungVaNhomNguoiDung(@trang, @soluong)) tb1
		left join
		(select * from NHOMNGUOIDUNG) tb2 on tb1.[MÃ NHÓM] = tb2.MANHOMNGUOIDUNG
go


---#8 Người dùng theo nhóm người dùng
create function dbo.HienThiNguoiDungTheoNhomNguoiDung(@manhom char(6))
returns table
as 
return
	select [MÃ TÀI KHOẢN], [TÊN NGƯỜI DÙNG]
	from ThongTinNguoiDungVaNhomNguoiDung(null,null)
	where [MÃ NHÓM] = @manhom
	group by [MÃ TÀI KHOẢN], [TÊN NGƯỜI DÙNG]
GO

---#9 Người dùng không nằm trong nhóm người dùng
create function dbo.HienThiNguoiDungKhongTrongNhomNguoiDung(@manhom char(6))
returns @result table([MÃ TÀI KHOẢN]  char(6), [TÊN NGƯỜI DÙNG] nvarchar(30))
as
begin
	declare @table table(mataikhoan char(6),tennguoidung nvarchar(30))
	insert into @table
	select [MÃ TÀI KHOẢN], [TÊN NGƯỜI DÙNG] from dbo.HienThiNguoiDungTheoNhomNguoiDung(@manhom)

	insert into @result
		select MATAIKHOAN,TENGIANGVIEN from GIANGVIEN
		union
		select MATAIKHOAN, TENHOCVIEN from HOCVIEN
		union
		select MATAIKHOAN, TENNHANVIEN from NHANVIEN
		except
		select tb.mataikhoan, tb.tennguoidung from @table tb
	return
end;
GO

---#10 Thêm 1 người dùng vào 1 nhóm người dùng - done
create proc dbo.ThemNguoiDungVaoNhomNguoiDung(@manhom char(6), @manguoidung char(6))
as
BEGIN
	insert into NGUOIDUNG_NHOMNGUOIDUNG(MANHOMNGUOIDUNG,MATAIKHOAN)
	values(@manhom,@manguoidung)
	return @@ROWCOUNT
end
GO

---#11 Xoá 1 người dùng từ 1 nhóm người dùng - done
create proc dbo.XoaNguoiDungTuNhomNguoiDung(@manhom char(6), @manguoidung char(6))
as
begin
	delete NGUOIDUNG_NHOMNGUOIDUNG 
	where MATAIKHOAN = @manguoidung
	and MANHOMNGUOIDUNG = @manhom
	return @@ROWCOUNT
end
GO

---#12 Lấy thông tin thao tác crua một nhóm người dùng
create proc dbo.HienThiThongTinThaoTacCuaNhomNguoiDung(@manhom char(6))
as
	begin
		declare @CHITIET_THAOTAC table
			(magiaodien char(6) not null primary key, 
			tengiaodien nvarchar(30),
			tt_view bit,
			tt_select bit,
			tt_insert bit,
			tt_update bit,
			tt_delete bit,
			tt_master bit)

		declare @id_giaodien char(6)

		declare myCursor cursor
		for
			select MAGIAODIEN
			from NHOMNGUOIDUNG_GIAODIEN
			WHERE	MANHOMNGUOIDUNG = @manhom
			GROUP BY MAGIAODIEN
		open myCursor
			fetch next from myCursor into @id_giaodien
			while @@FETCH_STATUS = 0
			begin		
				insert into @CHITIET_THAOTAC(magiaodien,tt_view,tt_select,tt_insert,tt_update,tt_delete,tt_master) 
					SELECT @id_giaodien,TT0001,TT0002,TT0003,TT0004,TT0005,TT0006
						from
							(
								select MATHAOTAC
								from NHOMNGUOIDUNG_GIAODIEN
								where MAGIAODIEN = @id_giaodien
								and MANHOMNGUOIDUNG = @manhom
							) d
							pivot
							(
								count(mathaotac)
								for mathaotac in (TT0001,TT0002,TT0003, TT0004,TT0005,TT0006)
							) pvt
				fetch next from myCursor into @id_giaodien
			end
		close myCursor
		deallocate myCursor

		select	GIAODIEN.MAGIAODIEN 'MÃ MÀN HÌNH', GIAODIEN.TENMANHINH 'TÊN MÀN HÌNH', 
				isnull(tt_view,0) 'VIEW', 
				isnull(tt_select,0) 'SELECT',
				isnull(tt_insert,0) 'INSERT',
				isnull(tt_update,0) 'UPDATE',
				isnull(tt_delete,0) 'DELETE', 
				isnull(tt_master,0) 'MASTER'
		from @CHITIET_THAOTAC tb
		right join
			GIAODIEN
				ON GIAODIEN.MAGIAODIEN = tb.magiaodien
	end;
go


----#13 Cập nhật thông tin thao tác  - done
create proc CapNhatThaoTacCuaNhomNguoiDung(@manhomnguoidung char(6), @magiaodien char(6), @mathaotac char(6), @checked bit)
as
	IF	(
			(select count(*) from NHOMNGUOIDUNG_GIAODIEN
			WHERE MAGIAODIEN = @magiaodien
			AND MANHOMNGUOIDUNG = @manhomnguoidung
			AND MATHAOTAC = @mathaotac) = 1
		)
		begin
			if(@checked = 0)
			begin
				delete NHOMNGUOIDUNG_GIAODIEN
					WHERE MAGIAODIEN = @magiaodien
					AND MANHOMNGUOIDUNG =  @manhomnguoidung
					AND MATHAOTAC = @mathaotac
				end
		end
	else
		begin
			insert into NHOMNGUOIDUNG_GIAODIEN
			values(@magiaodien, @manhomnguoidung,@mathaotac)
		end
	return @@ROWCOUNT;
go

----#14 Cập nhật trạng thái hoạt động của tài khoản trong 1 nhóm người dùng - done
create proc CapNhatHoatDongNguoiDungTrongNhomNguoiDung(@manhomnguoidung char(6), @manguoidung char(6))
as
	update NGUOIDUNG_NHOMNGUOIDUNG
	set HOATDONG = case when HOATDONG = 0 then 1 else 0 end
	where MANHOMNGUOIDUNG = @manhomnguoidung
		and MATAIKHOAN = @manguoidung
	return @@ROWCOUNT
go

----#15 Tạo một nhóm người dùng mới
create proc ThemNhomNguoiDung(@tennhom nvarchar(30))
as 
	if @tennhom not in (select TENNHOMNGUOIDUNG from NHOMNGUOIDUNG)
	begin
		declare @id char(6)
		set @id =  concat('00', (select CONVERT(char(6), count(*)) from NHOMNGUOIDUNG))
		set @id = concat('NND',substring(@id,len(@id)-2,4))
		insert into NHOMNGUOIDUNG(MANHOMNGUOIDUNG,TENNHOMNGUOIDUNG) 
		values(@id,@tennhom)
		return @@ROWCOUNT
	end
	return 0
go

----#16 Xoá một nhóm người dùng mới - done
create proc XoaNhomNguoiDung(@manhom char(6))
as
	if @manhom <> 'XMASTR'
	begin
		declare @count_nguoidung int
		set @count_nguoidung = 
			(select count(*) 
			from NGUOIDUNG_NHOMNGUOIDUNG
			where MANHOMNGUOIDUNG = @manhom)
		if @count_nguoidung = 0
		begin
			declare @count_giaodien int
			set @count_giaodien =
				(select count(*)
				from NHOMNGUOIDUNG_GIAODIEN
				where MANHOMNGUOIDUNG = @manhom)
			if @count_giaodien =0
			begin
				--Xoá
				delete NHOMNGUOIDUNG
				where MANHOMNGUOIDUNG = @manhom
			end
		end
	end
go

---#17 thông tin người dùng và nhóm quyền theo trang
create function ThongTinNguoiDungVaNhomNguoiDung(@trang int null, @soluong int null)
returns @result table([MÃ TÀI KHOẢN] Char(6), [TÊN NGƯỜI DÙNG] nvarchar(30), [MÃ NHÓM] char(6), [HOẠT ĐỘNG] bit)
as
begin
	declare @table table(mataikhoan Char(6), manhom char(6), hoatdong bit)
	if isnull(@trang,-1) = -1 or isnull(@trang,-1) = -1
	begin
		insert into @table(mataikhoan,manhom,hoatdong) 
			select MATAIKHOAN, MANHOMNGUOIDUNG, HOATDONG
			from NGUOIDUNG_NHOMNGUOIDUNG
			ORDER BY MATAIKHOAN
	end
	else
	begin
		insert into @table(mataikhoan,manhom,hoatdong) 
			select * from NGUOIDUNG_NHOMNGUOIDUNG
			ORDER BY MATAIKHOAN
			OFFSET (@trang-1)*@soluong ROWS
			FETCH NEXT @soluong ROWS ONLY
	end

	declare @group table(group_id char(2))
	insert into @group
		select SUBSTRING(mataikhoan,1,2) from @table
		GROUP BY SUBSTRING(mataikhoan,1,2)

	--join
	if 'GV' in (select * from @group)
		insert into @result
			select GIANGVIEN.MATAIKHOAN, GIANGVIEN.TENGIANGVIEN, manhom, hoatdong 
			from @table tb
			left join GIANGVIEN ON tb.mataikhoan = GIANGVIEN.mataikhoan
			where isnull(GIANGVIEN.MATAIKHOAN,'e') <> 'e'
	if 'HV' in (select * from @group)
	insert into @result
		select HOCVIEN.MATAIKHOAN, HOCVIEN.TENHOCVIEN, manhom, hoatdong 
		from @table tb
		left join HOCVIEN ON tb.mataikhoan = HOCVIEN.mataikhoan
		where isnull(HOCVIEN.MATAIKHOAN,'e') <> 'e'
	if 'NV' in (select * from @group)
		insert into @result
		select NHANVIEN.MATAIKHOAN, NHANVIEN.TENNHANVIEN, manhom, hoatdong 
		from @table tb
		left join NHANVIEN ON tb.mataikhoan = NHANVIEN.mataikhoan
		where isnull(NHANVIEN.MATAIKHOAN,'e') <> 'e'
	return
	end
GO


--#18 hiển thị danh sách lớp học theo page - done
create proc HienThiTatCaLopHoc(@trang int null, @soluong int null)
as
	declare @start int
	set @start = (@trang-1)*@soluong
	SELECT LOPHOC.MALOPHOC 'MÃ LỚP', KHOAHOC.MAKHOAHOC 'MÃ KHOÁ', KHOAHOC.TENKHOAHOC 'TÊN KHOÁ HỌC', 
	GIANGVIEN.MAGIANGVIEN 'MÃ GIẢNG VIÊN', GIANGVIEN.TENGIANGVIEN 'TÊN GIẢNG VIÊN', 
	LOPHOC.SOLUONG 'SỐ LƯỢNG HV',KHOAHOC.HOCPHI 'HỌC PHÍ'
	FROM LOPHOC, KHOAHOC, GIANGVIEN
	WHERE LOPHOC.MAKHOAHOC = KHOAHOC.MAKHOAHOC
		AND GIANGVIEN.MAGIANGVIEN = LOPHOC.MAGIANGVIEN
	order by MALOPHOC
	offset @start rows
	fetch next @soluong rows only
GO

--#19 Cập nhật số lượng của học viên một lớp học
create proc CapNhatSoLuongHocVien
as
	declare @tb table(malophoc char(8), soluong int)
	insert into @tb 
		select LOPHOC.MALOPHOC, isnull(t.SOLUONG,0)
		from
			(select MALOPHOC, COUNT(*) as 'SOLUONG'
			from TT_GHIDANH
			group by MALOPHOC) t
		right join
			LOPHOC
		on t.MALOPHOC = LOPHOC.MALOPHOC

	declare @malophoc char(8)

	declare myCursor cursor
	for select MALOPHOC from LOPHOC
	open myCursor
	fetch next from myCursor into @malophoc
		while @@FETCH_STATUS = 0
		begin
			update LOPHOC
			set SOLUONG = (select t.soluong 
						from @tb t where t.malophoc = @malophoc)
			where MALOPHOC = @malophoc
			fetch next from myCursor into @malophoc
		end
	close myCursor
	deallocate myCursor
go	


--#20 Hiển thị thông tin của một lớp học - done
create function HienThiThongTinLopHoc(@malop char(8))
returns table
as
	return 
		SELECT l.MALOPHOC, k.MAKHOAHOC, k.TENKHOAHOC, l.SOLUONG, k.HOCPHI, 
				g.MAGIANGVIEN, G.TENGIANGVIEN, g.TRINHDO, g.CHUYENMON 
		FROM giangvien g, lophoc l, khoahoc k
		where g.MAGIANGVIEN = l.MAGIANGVIEN
			and l.MAKHOAHOC = k.MAKHOAHOC
			and l.MALOPHOC = @malop
GO


--#21 Hien Thị lịch học của một lớp học - done
create function HienThiLichHocLopHoc(	 char(8))
returns table
as
	return
		select concat(N'LỊCH HỌC ',convert(int,substring(LICHHOC.MALICHHOC,5,2))) 'MÃ LỊCH', 
		LICHHOC.NGAYHOC 'NGÀY HỌC', 
		CONVERT(varchar, CAHOC.GIOBATDAU, 108) 'BẮT ĐẦU', 
		CONVERT(varchar, CAHOC.GIOKETTHUC,108) 'KẾT THÚC',
		LICHHOC.MAPHONG 'PHÒNG', COSO.TENCOSO 'TÊN CƠ SỞ',
		LICHHOC.TINHTRANG 'TÌNH TRẠNG'
		from LICHHOC , CAHOC, COSO
		where MALOPHOC = @malop
		AND CAHOC.MACA = LICHHOC.MACA
		AND COSO.MACOSO = LICHHOC.MACOSO
go


--#22 Hiển thị danh sách học vien của một lớp - done
create proc HienThiHocVienLopHoc(@malop char(8))
as
	select h.MAHOCVIEN 'MÃ HỌC VIÊN', h.TENHOCVIEN 'TÊN HỌC VIÊN', h.GIOITINH  'GIỚI TÍNH',
	convert(varchar,h.NGAYSINH,103) 'NGÀY SINH', h.SODIENTHOAI 'SĐT', h.MAIL 'MAIL'
	from HOCVIEN h
	where MAHOCVIEN in 
		(select MAHOCVIEN
		from TT_GHIDANH
		where MALOPHOC = @malop
		group by MAHOCVIEN)
go

--#23 Tạo mã lớp học tự động - done
create proc TaoMaLopTuKhoaHoc(@makhoahoc char(6))
as
	DECLARE @num int
	set @num = (select COUNT(*)from LOPHOC
				WHERE MAKHOAHOC = @makhoahoc)
	set @num = @num+1

	Declare @id varchar(8)
	set @id = 'L'+substring(@makhoahoc,4,3)+'0000'

	set @id = SUBSTRING(@id,1,len(@id)-len(convert(nvarchar,@num)))
	select (@id + convert(nvarchar,@num)) 'MALOPHOC'
go
select * from lophoc

--#24 Cập nhật mật khẩu mã hoá TripleDES
create proc CapNhatMatKhau(@user char(6),@pass nvarchar(30))
as
	update nguoidung
	set MATKHAU = @pass
	where MATAIKHOAN = @user
go

--#25 Cập nhật tình trạng cua lịch học  -- done
create proc CapNhatLichHoc
as
	update LICHHOC
	set tinhtrang =
		case when  DATEDIFF(day,GETDATE(),ngayhoc) < 0 THEN N'ĐÃ KẾT THÚC'
		else N'CHƯA KẾT THÚC' end
go

--#26 Hiện thị toàn bộ điểm của trung tâm - done
create proc HienThiTatCaDiem(@trang int, @soluong int)
as
	declare @start int
	set @start = (@trang-1)*@soluong

	select d.MADIEM 'MÃ ĐIỂM',
	d.MAHOCVIEN 'MÃ HỌC VIÊN',
	h.TENHOCVIEN 'TÊN HỌC VIÊN',
	l.MALOPHOC 'MÃ LỚP',
	k.TENKHOAHOC 'TÊN KHOÁ HỌC',
	ld.TENLOAIDIEM 'LOẠI ĐIỂM',
	d.LAN 'LẦN',
	d.DIEM 'ĐIỂM'
	from DIEM d, HOCVIEN h, LOPHOC l, LOAIDIEM ld, KHOAHOC k
	where d.MAHOCVIEN = h.MAHOCVIEN
		AND d.MALOPHOC = l.MALOPHOC
		AND d.MALOAIDIEM = ld.MALOAIDIEM
		AND k.MAKHOAHOC = l.MAKHOAHOC
	order by MADIEM
	offset @start rows
	fetch next @soluong rows only
go
	
--#27 Lấy 1 record điểm - done
create proc HienThiThongTinDiem(@madiem char(6), @mahocvien char(6), @malophoc char(8))
as
	select d.MADIEM,d.MAHOCVIEN,h.TENHOCVIEN,L.MALOPHOC,k.MAKHOAHOC,k.TENKHOAHOC,ld.MALOAIDIEM,ld.TENLOAIDIEM,d.LAN,d.DIEM
	from DIEM d, HOCVIEN h, LOPHOC l, LOAIDIEM ld, KHOAHOC k
	where d.MAHOCVIEN = h.MAHOCVIEN
		AND d.MALOPHOC = l.MALOPHOC
		AND d.MALOAIDIEM = ld.MALOAIDIEM
		AND k.MAKHOAHOC = l.MAKHOAHOC
		AND MADIEM = @madiem
		and h.MAHOCVIEN = @mahocvien
		and l.MALOPHOC = @malophoc
go

--#28 Lấy các lớp học của một khoá học--done
create proc LayMaLopHocCuaKhoaHoc(@makhoa char(6))
as
	select * 
	from LOPHOC L, KHOAHOC K
	where L.MAKHOAHOC = K.MAKHOAHOC
	and K.MAKHOAHOC = @makhoa
go

--#29 Hiển thị các điểm của học viên trong 1 lớp -- done
create proc LayMaDiemHocVienTrongLopHoc(@mahocvien char(6), @malophoc char(8))
as
	select d.MADIEM
	from LOPHOC L, HOCVIEN H, DIEM d
	where d.MAHOCVIEN = H.MAHOCVIEN
		and d.MALOPHOC = L.MALOPHOC
		and d.MALOPHOC = @malophoc
		and h.MAHOCVIEN = @mahocvien
	
--#30 Cấp số lần của 1 loại điểm tự động
create proc TaoLanDiemTuDong(@madiem char(6), @mahocvien char(6), @malophoc char(8), @maloaidiem char(3))
as
	declare @count int
	set @count = (	select count(*)
					from diem
					where MADIEM = @madiem
						and MAHOCVIEN = @mahocvien 
						and MALOPHOC = @malophoc  
						AND MALOAIDIEM = @maloaidiem)

	select case @count 
			when 0 then (select isnull(MAX(LAN),0)+1 
						from DIEM 	
						where MAHOCVIEN = @mahocvien  
						and MALOPHOC = @malophoc 
						AND MALOAIDIEM = @maloaidiem)
			else @count end 'LAN'
go

---#31 Cập nhật điểm của 1 học viên trong 1 lớp học  -- done
create proc CapNhatDiemHocVienLopHoc(@madiem char(6), @mahocvien char(6), @malophoc char(8), @lan int, @loaidiem char(3), @diem float)
as
	update DIEM
	set DIEM = @diem, MALOAIDIEM = @loaidiem, LAN = @lan
	where MADIEM = @madiem
		AND MAHOCVIEN = @mahocvien
		AND MALOPHOC = @malophoc
go

---#32 Tạo mã điểm tự động -- done
create proc TaoMaDiemTuDong(@malophoc char(8), @mahocvien char(6))
as
	declare @madiem varchar(20)
	set @madiem = concat('0000',convert(nvarchar,
					(
					select count(*)+1 from diem
					where MAHOCVIEN = @mahocvien
					and malophoc = @malophoc)
					))

	set @madiem = substring(@madiem, (len(@madiem)-5)+1,5)
	select concat('D',@madiem) 'MADIEM'
go
select * from diem

----#33 Thêm điểm mới -- done
create proc ThemDiem(@madiem char(6), @mahocvien char(6), @malophoc char(8), @maloaidiem char(3), @lan int, @diem float)
as
	insert into DIEM (madiem,mahocvien,malophoc,maloaidiem,lan,diem)
	values(@madiem , @mahocvien , @malophoc, @maloaidiem , @lan, @diem )
go

----#34 các lớp học của một học viên tham gia -- DONE
create proc LayLopHocHocVien(@mahocvien CHAR(6))
as
	select g.MALOPHOC 'MALOP', concat(k.TENKHOAHOC,' (',g.MALOPHOC,')') 'LOPHOCVIEN'
	from TT_GHIDANH g, KHOAHOC k, LOPHOC l
	where MAHOCVIEN = @mahocvien
	and g.MALOPHOC = l.MALOPHOC
	and k.MAKHOAHOC = l.MAKHOAHOC
go


----#35  thông tin điểm của một học viên theo lớp học - done 
create proc HienThiDiemHocVienLopHoc(@mahocvien char(6), @malophoc char(8), @trang int, @soluong int)
as
	declare @start int
	set @start = (@trang-1)*@soluong
	select	d.MALOPHOC 'MÃ LỚP HỌC', k.TENKHOAHOC 'TÊN KHOÁ HỌC', 
			ld.TENLOAIDIEM 'LOẠI ĐIỂM', d.LAN 'LẦN', d.DIEM 'ĐIỂM'
	 from DIEM d, LOPHOC l, KHOAHOC k, LOAIDIEM ld
	 where d.MAHOCVIEN = @mahocvien
	 and d.MALOPHOC = @malophoc
	 and d.MALOPHOC = l.MALOPHOC
	 and l.MAKHOAHOC = k.MAKHOAHOC
	 and ld.MALOAIDIEM = d.MALOAIDIEM
	 order by l.MALOPHOC
	 offset @start rows
	 fetch next @soluong rows only
go

----#36 hiển thị danh sách mail của một lớp học - DONE
create proc HienThiMailMotLop(@malophoc char(8))
as
	(select h.MATAIKHOAN 'MÃ TÀI KHOẢN', h.TENHOCVIEN 'NGƯỜI NHẬN', h.MAIL 'ĐỊA CHỈ MAIL' ,h.SODIENTHOAI 'SỐ ĐIỆN THOẠI'
	from HOCVIEN h, TT_GHIDANH tt
	where h.MAHOCVIEN = tt.MAHOCVIEN
		and tt.MALOPHOC = @malophoc)
	union
	(select g.MATAIKHOAN, g.TENGIANGVIEN, g.MAIL, g.SODIENTHOAI
	from GIANGVIEN g, LOPHOC l
	where l.MAGIANGVIEN = g.MAGIANGVIEN
		and l.MALOPHOC = @malophoc)
GO

----#37 lấy thông tin của một nhân viên - done
create proc LayThongTinNhanVien(@manhanvien char(6))
as 
	select * from NHANVIEN
	where MANHANVIEN = @manhanvien


exec TaoMaDiemTuDong @malophoc = 'L0010001', @mahocvien = 'HV0002'

----#38 Lấy thông tin mail của một người dùng
create proc LayThongTinMailNguoiDung(@manguoidung char(6))
as
	if substring(@manguoidung,1,2) = 'HV'
	begin
		select MATAIKHOAN 'MÃ TÀI KHOẢN', TENHOCVIEN 'NGƯỜI NHẬN', MAIL 'ĐỊA CHỈ MAIL' ,SODIENTHOAI 'SỐ ĐIỆN THOẠI'
		from HOCVIEN
		WHERE MAHOCVIEN = @manguoidung
	end
	else if substring(@manguoidung,1,2) = 'NV'
		begin
		select MATAIKHOAN 'MÃ TÀI KHOẢN', TENNHANVIEN 'NGƯỜI NHẬN', MAIL 'ĐỊA CHỈ MAIL' ,SODIENTHOAI 'SỐ ĐIỆN THOẠI'
		from NHANVIEN
		WHERE MANHANVIEN = @manguoidung
	end
	else if substring(@manguoidung,1,2) = 'GV'
	begin
		select MATAIKHOAN 'MÃ TÀI KHOẢN', TENGIANGVIEN 'NGƯỜI NHẬN', MAIL 'ĐỊA CHỈ MAIL' ,SODIENTHOAI 'SỐ ĐIỆN THOẠI'
		from GIANGVIEN
		WHERE MAGIANGVIEN = @manguoidung
	end
GO

-----#39 Lấy các lớp của một giảng viên dạy - done
create proc LayLopHocGiangVien(@magiangvien char(6))
as
	select l.MALOPHOC, concat(l.MALOPHOC,' - ',k.TENKHOAHOC) 'LOPHOC'
	from  LOPHOC l, KHOAHOC k
	where l.MAKHOAHOC = k.MAKHOAHOC
	and MAGIANGVIEN = @magiangvien
GO


-----#40 Lấy lịch học của lớp học - done
create proc LayLichHocLopHoc(@malophoc char(8))
as
	select MALICHHOC, CONCAT(SUBSTRING(MALICHHOC,5,2),' - ',NGAYHOC) 'LICHHOC'
	from LICHHOC
	where MALOPHOC = @malophoc


----#41 Lấy chi tiết buổi học của một lich học crua 1 lớp 

create function HienThiCTBuoiHocFull(@malichhoc char(6), @malophoc char(8))
returns table
as
return(
			select h.MAHOCVIEN 'MÃ HỌC VIÊN', dbo.Get_Ho(h.TENHOCVIEN) 'HỌ HỌC VIÊN', dbo.Get_Ten(h.TENHOCVIEN) 'TÊN HỌC VIÊN', h.NGAYSINH 'NGÀY SINH', 
			tb3.MALOPHOC 'MÃ LỚP', k.TENKHOAHOC 'TÊN KHOÁ HỌC', ISNULL(tb3.DIEMDANH,0) 'ĐIỂM DANH'
			from
				(select tb2.MADANGKYKHOAHOC, tb2.MALICHHOC, tb2.MACA, tb2.NGAYHOC, tb2.MALOPHOC, c.DIEMDANH
				from 
					(select G.MADANGKYKHOAHOC, tb1.MALICHHOC, tb1.MACA, tb1.NGAYHOC, tb1.MALOPHOC
						from
					(select L.MALICHHOC, L.MACA, L.NGAYHOC, L.MALOPHOC
						from LICHHOC L
						where L.MALOPHOC = @malophoc
						and L.MALICHHOC = @malichhoc) tb1
					left join
						TT_GHIDANH G
					on tb1.MALOPHOC = G.MALOPHOC) tb2
				left join
				CT_BUOIHOC c
				on c.MALICHHOC = tb2.MALICHHOC and c.MADANGKYKHOAHOC = tb2.MADANGKYKHOAHOC and c.MALOPHOC = tb2.MALOPHOC) tb3,HOCVIEN h, TT_DANGKYKHOAHOC d, KHOAHOC k
			where h.MAHOCVIEN = d.MAHOCVIEN
			and d.MADANGKYKHOAHOC = tb3.MADANGKYKHOAHOC
			and k.MAKHOAHOC = d.MAKHOAHOC
		)

go

drop proc HienThiCTBuoiHocLichHoc
create function HienThiCTBuoiHocLichHoc( @malichhoc char(6), @malophoc char(8), @trang int, @soluong int)
as
	begin	
			declare @start int
			set @start = (@trang-1)*@soluong
			select h.MAHOCVIEN 'MÃ HỌC VIÊN', dbo.Get_Ho(h.TENHOCVIEN) 'HỌ HỌC VIÊN', dbo.Get_Ten(h.TENHOCVIEN) 'TÊN HỌC VIÊN', h.NGAYSINH 'NGÀY SINH', 
			tb3.MALOPHOC 'MÃ LỚP', k.TENKHOAHOC 'TÊN KHOÁ HỌC', ISNULL(tb3.DIEMDANH,0) 'ĐIỂM DANH'
			from
				(select tb2.MADANGKYKHOAHOC, tb2.MALICHHOC, tb2.MACA, tb2.NGAYHOC, tb2.MALOPHOC, c.DIEMDANH
				from 
					(select G.MADANGKYKHOAHOC, tb1.MALICHHOC, tb1.MACA, tb1.NGAYHOC, tb1.MALOPHOC
						from
					(select L.MALICHHOC, L.MACA, L.NGAYHOC, L.MALOPHOC
						from LICHHOC L
						where L.MALOPHOC = @malophoc
						and L.MALICHHOC = @malichhoc) tb1
					left join
						TT_GHIDANH G
					on tb1.MALOPHOC = G.MALOPHOC) tb2
				left join
				CT_BUOIHOC c
				on c.MALICHHOC = tb2.MALICHHOC and c.MADANGKYKHOAHOC = tb2.MADANGKYKHOAHOC and c.MALOPHOC = tb2.MALOPHOC) tb3,HOCVIEN h, TT_DANGKYKHOAHOC d, KHOAHOC k
			where h.MAHOCVIEN = d.MAHOCVIEN
			and d.MADANGKYKHOAHOC = tb3.MADANGKYKHOAHOC
			and k.MAKHOAHOC = d.MAKHOAHOC

			order by [TÊN HỌC VIÊN]
			offset @start rows
			fetch next @soluong rows only
end

go


----#42 Cập nhật điểm danh 1 học viên trong 1 lớp học ở 1 buổi học - done
create proc CapNhatDiemDanhHocVien(@mahocvien char(6), @malich char(6), @malophoc char(8), @comat bit)
as
	declare @count int
	set @count =
	(select count(*) 
	from CT_BUOIHOC
	where MAHOCVIEN = @mahocvien
		and MALICHHOC = @malich
		and MALOPHOC = @malophoc)
	if(@count = 1)
		begin
			update CT_BUOIHOC
			set DIEMDANH = @comat
			where MAHOCVIEN = @mahocvien
				and MALICHHOC = @malich
				and MALOPHOC = @malophoc 
		end
	else
		begin
			insert into CT_BUOIHOC(MAHOCVIEN,MALICHHOC,MALOPHOC,DIEMDANH)
			values (@mahocvien,@malich,@malophoc,@comat)
		end

go


---#43 Hiển thị tất cả user
create proc HienThiTatCaNguoiDung(@trang int, @soluong int)
as
	declare @table table(mataikhoan Char(6), hoatdong bit)

	insert into @table(mataikhoan,hoatdong) 
		select MATAIKHOAN,HOATDONG 
		from NGUOIDUNG
		ORDER BY MATAIKHOAN
		OFFSET (@trang-1)*@soluong ROWS
		FETCH NEXT @soluong ROWS ONLY

	declare @group table(group_id char(2))
	insert into @group
	select SUBSTRING(mataikhoan,1,2) from @table
	GROUP BY SUBSTRING(mataikhoan,1,2)

	declare @result table (mataikhoan char(6), tennguoidung nvarchar(30), hoatdong bit)

	--join
	if 'GV' in (select * from @group)
		insert into @result
			select GIANGVIEN.MATAIKHOAN, GIANGVIEN.TENGIANGVIEN, hoatdong 
			from @table tb
			left join GIANGVIEN ON tb.mataikhoan = GIANGVIEN.mataikhoan
			where isnull(GIANGVIEN.MATAIKHOAN,'e') <> 'e'
	if 'HV' in (select * from @group)
	insert into @result
		select HOCVIEN.MATAIKHOAN, HOCVIEN.TENHOCVIEN, hoatdong 
		from @table tb
		left join HOCVIEN ON tb.mataikhoan = HOCVIEN.mataikhoan
		where isnull(HOCVIEN.MATAIKHOAN,'e') <> 'e'
	if 'NV' in (select * from @group)
		insert into @result
		select NHANVIEN.MATAIKHOAN, NHANVIEN.TENNHANVIEN, hoatdong 
		from @table tb
		left join NHANVIEN ON tb.mataikhoan = NHANVIEN.mataikhoan
		where isnull(NHANVIEN.MATAIKHOAN,'e') <> 'e'

	select mataikhoan 'MÃ TÀI KHOẢN', dbo.Get_Ho(tennguoidung) 'HỌ NGƯỜI DÙNG', 
	dbo.Get_Ten(tennguoidung) 'TÊN NGƯỜI DÙNG', hoatdong 'HOẠT ĐỘNG'
	from @result

go

----#44 Cập nhật tình trạng hoạt động của tài khoản - done
create proc CapNhatHoatDongTaiKhoanNguoiDung(@mataikhoan char(6))
as
	declare @active int
	set @active = (select HOATDONG from NGUOIDUNG where MATAIKHOAN = @mataikhoan)
	set @active = case @active when 1 then 0 else 1 end
	update nguoidung
	set HOATDONG = @active
	where MATAIKHOAN = @mataikhoan

go

----#45 Xoá 1 tài khoản - done
drop proc XoaTaiKhoan
create proc XoaTaiKhoan(@mataikhoan char(6))
as
	declare @group_id char(2)
	set @group_id = substring(@mataikhoan,1,2)

	if @group_id = 'GV'
		delete GIANGVIEN
		where MAGIANGVIEN = @mataikhoan
	else if @group_id = 'HV'
		delete HOCVIEN
		where MAHOCVIEN = @mataikhoan
	else if @group_id = 'NV'
		delete NHANVIEN
		where MANHANVIEN = @mataikhoan

	delete NGUOIDUNG
	where MATAIKHOAN = @mataikhoan
go

-----#46 Danh sách giờ học
create proc dbo.HienThiTTChonGioHocHocVien(@madangkykhoahoc nvarchar(20))
as
	begin
		declare @THUTRONHGTUAN table
			(maca char(3) not null primary key, 
			batdau time,
			ketthuc time,
			thu2 bit,
			thu3 bit,
			thu4 bit,
			thu5 bit,
			thu6 bit,
			thu7 bit,
			thu8 bit)

		declare @id_cahoc char(6)

		declare myCursor cursor
		for
			select macahoc
			from TT_CHONGIOHOC
			WHERE	MADANGKYKHOAHOC = @madangkykhoahoc
			GROUP BY macahoc

		open myCursor
			fetch next from myCursor into @id_cahoc
			while @@FETCH_STATUS = 0
			begin		
				insert into @THUTRONHGTUAN(maca,thu2,thu3,thu4,thu5,thu6,thu7,thu8) 

					SELECT @id_cahoc,MON,TUE,WED,THU,FRI,SAT,SUN
						from
							(
								select MATHU
								from TT_CHONGIOHOC
								where MACAHOC = @id_cahoc
									and MADANGKYKHOAHOC = @madangkykhoahoc
							)d
							pivot
							(
								count(MATHU)
								for MATHU in (MON,TUE,WED,THU,FRI,SAT,SUN)
							) pvt
				fetch next from myCursor into @id_cahoc
			end
		close myCursor
		deallocate myCursor

		select	CAHOC.MACA 'CA HỌC',
				CONVERT(varchar, CAHOC.GIOBATDAU, 108) 'BẮT ĐẦU',
				CONVERT(varchar, CAHOC.GIOKETTHUC, 108) 'KẾT THÚC',
				isnull(thu2,0) 'THỨ 2', 
				isnull(thu3,0) 'THỨ 3',
				isnull(thu4,0) 'THỨ 4',
				isnull(thu5,0) 'THỨ 5',
				isnull(thu6,0) 'THỨ 6', 
				isnull(thu7,0) 'THỨ 7',
				isnull(thu8,0) 'CHỦ NHẬT'
		from @THUTRONHGTUAN tb
		right join
			CAHOC
				ON CAHOC.MACA = tb.maca
	end;
go


----- #47 Hiên thị thông tin chọn giờ học chi tiết của các học viên trong 1 khoá học
create proc HienThiThongTinChonGioHocHocVienKhoaHoc(@makhoa char(6))
as 
	declare @table 
	table
		(MAHOCVIEN char(6) primary key,
		TENHOCVIEN nvarchar(50),
		DIEM float,
		THU2 nvarchar(50),
		THU3 nvarchar(50),
		THU4 nvarchar(50),
		THU5 nvarchar(50),
		THU6 nvarchar(50),
		THU7 nvarchar(50),
		CHUNHAT nvarchar(50))

		declare @mon nvarchar(50)
		declare @tue nvarchar(50)
		declare @wed nvarchar(50)
		declare @thu nvarchar(50)
		declare @fri nvarchar(50)
		declare @sat nvarchar(50)
		declare @sun nvarchar(50)

	declare @mahocvien char(6)
	declare cursorHocVien cursor
	for
		select mahocvien
		FROM TT_CHONGIOHOC
		where MAKHOAHOC = MAKHOAHOC
		group by MAHOCVIEN

	open cursorHocVien
	fetch next from cursorHocVien into @mahocvien
		while @@FETCH_STATUS = 0
		begin				
			declare @mathu char(3)
			declare @maca char(3)

			set @mon = null
			set @tue = null
			set @wed = null
			set @thu = null
			set @fri = null
			set @sat = null
			set @sun = null

			declare myCursor cursor
			for
				select MATHU,MACAHOC
				from TT_CHONGIOHOC
				where MAHOCVIEN = @mahocvien
					and MAKHOAHOC = @makhoa

			open myCursor
				fetch next from myCursor into @mathu,@maca
				while @@FETCH_STATUS = 0
				begin
					if(@mathu = 'MON')
						set @mon = concat(@mon, ', ',right(@maca,len(@maca)-2))
					else if(@mathu = 'TUE')
						set @tue =concat( @tue , ', ',right(@maca,len(@maca)-2))
					else if(@mathu = 'WED')
						set @wed = concat(@wed , ', ',right(@maca,len(@maca)-2))
					else if(@mathu = 'THU')
						set @thu = concat(@thu , ', ',right(@maca,len(@maca)-2))
					else if(@mathu = 'FRI')
						set @fri = concat(@fri , ', ',right(@maca,len(@maca)-2))
					else if(@mathu = 'SAT')
						set @sat = concat(@sat , ', ',right(@maca,len(@maca)-2))
					else if(@mathu = 'SUN')
						set @sun = concat(@sun , ', ',right(@maca,len(@maca)-2))
					fetch next from myCursor into @mathu,@maca
				end
			close myCursor
			deallocate myCursor

			insert into @table
				select h.MAHOCVIEN, 
					h.TENHOCVIEN,
					d.DIEM,
					right(@mon,len(@mon)-2), 
					right(@tue,len(@tue)-2), 
					right(@wed,len(@wed)-2), 
					right(@thu,len(@thu)-2), 
					right(@fri,len(@fri)-2), 
					right(@sat,len(@sat)-2), 
					right(@sun,len(@sun)-2)
				from HOCVIEN h, TT_DIEMDAUVAO d
				where h.MAHOCVIEN = @mahocvien
					and h.MAHOCVIEN = d.MAHOCVIEN
					and d.MAKHOAHOC = @makhoa

			fetch next from cursorHocVien into @mahocvien
		end
	close cursorHocVien
	deallocate cursorHocVien

	select MAHOCVIEN 'Mã học viên', 
		TENHOCVIEN 'Tên học viên', 
		DIEM 'Điểm kiểm tra',  
		isnull(THU2,'') 'Thứ 2', 
		isnull(THU3,'') 'Thứ 3',
		isnull(THU4,'') 'Thứ 4',
		isnull(THU5,'') 'Thứ 5', 
		isnull(THU6,'') 'Thứ 6',
		isnull(THU7,'') 'Thứ 7', 
		isnull(CHUNHAT,'') 'Chủ Nhật'
	from @table;
go
--#48 LAY lich trong cua 1 hoc vien
create function LayLichTrongHocVien(@mahocvien char(6))
returns table
as
	--Hoc viên theo lịch học (tình trạng lịch vẫn chưa kết thúc)
	return (
	select THU_INT, f.MACA from
		(SELECT * FROM THU,CAHOC) f
		left join
			(
				SELECT CAHOC.MACA, DATEPART(WEEKDAY,lichhoc.NGAYHOC) DAYOFWEEK
				FROM LICHHOC, LOPHOC, CAHOC, TT_GHIDANH
				where LICHHOC.MALOPHOC = LOPHOC.MALOPHOC
				and LICHHOC.MACA = CAHOC.MACA
				and TT_GHIDANH.MALOPHOC = LOPHOC.MALOPHOC
				and TT_GHIDANH.MAHOCVIEN = 'HV0001'
				and LICHHOC.TINHTRANG = N'CHƯA KẾT THÚC'
				GROUP BY CAHOC.MACA ,DATEPART(WEEKDAY,lichhoc.NGAYHOC)
			) tb
	on tb.DAYOFWEEK = f.THU_INT and f.MACA = tb.MACA
	where isnull(tb.MACA,'0')='0'
	)

go
-----#49 lAY lich trong cua cac hoc vien trong khoa hoc
--create proc LayLichTrongHocVienKhoaHoc(@makhoahoc char(6))
--as
--	declare @table table(thu int, ca char(3))
--	declare @temp table(thu int, ca char(3))
--	declare @mahv char(6)
--	declare myCursor cursor
--	for select TT_GHIDANH.MAHOCVIEN from TT_GHIDANH, LOPHOC
--		where LOPHOC.MALOPHOC = TT_GHIDANH.MALOPHOC
--		and MAKHOAHOC = @makhoahoc

--	open myCursor
--	fetch next from myCursor into @mahv
--	while @@FETCH_STATUS = 0
--		begin
--			delete from @temp
--			if(select count(*) from @table) = 0
--			begin
--				insert into @table
--				select * from dbo.LayLichTrongHocVien(@mahv)
--			end
--			else
--			begin
--				insert into @temp select * from @table
--				delete from @table
--				insert into @table
--				select * from @temp 
--					intersect 
--				select * from dbo.LayLichTrongHocVien(@mahv)
--			end
--		fetch next from myCursor into @mahv
--		end
--	close myCursor
--	deallocate myCursor
--	select * from @table
--GO

--#50 LAY lich trong cua 1 giang vien
create function LayLichTrongGiangVien(@mahocvien char(6))
returns table
as
	--Hoc viên theo lịch học (tình trạng lịch vẫn chưa kết thúc)
	return(select THU_INT, f.MACA from
		(SELECT * FROM THU,CAHOC) f
			left join
			(
				SELECT CAHOC.MACA, DATEPART(WEEKDAY,lichhoc.NGAYHOC) DAYOFWEEK
				FROM LICHHOC, LOPHOC, CAHOC
				where LICHHOC.MALOPHOC = LOPHOC.MALOPHOC
				and LICHHOC.MACA = CAHOC.MACA
				and LOPHOC.MAGIANGVIEN = 'GV0001'
				and LICHHOC.TINHTRANG = N'CHƯA KẾT THÚC'
				GROUP BY CAHOC.MACA, DATEPART(WEEKDAY,lichhoc.NGAYHOC)
			) tb
			on tb.DAYOFWEEK = f.THU_INT and f.MACA = tb.MACA
			where isnull(tb.MACA,'0')='0')
go
--#51 Học viên theo lịch chọn
create function LayLichDaChonHocVien(@mahockhoa char(6),@mahocvien char(6))
returns table
as
	return 
	(SELECT  THU_INT , MACAHOC
	FROM TT_CHONGIOHOC, THU
	WHERE THU.MATHU = TT_CHONGIOHOC.MATHU
	and TT_CHONGIOHOC.MAHOCVIEN = @mahocvien
	and MAKHOAHOC = @mahockhoa)
go

--#52 thêm biểu mẫu
create proc ThemBieuMau(@mabieumau char(6), @tenbieumau nvarchar(50), @taptin varbinary(MAX), @loai varchar(8))
as
	insert into BIEUMAU(MABIEUMAU,TENBIEUMAU,TAPTIN,LOAI)
	values (@mabieumau,@tenbieumau,@taptin,@loai)

--------------------
select * from TT_BACKUP
where MABIEUMAU != 'BM0001' AND MABIEUMAU != 'BM0002' AND
MABIEUMAU != 'BM0003' AND MABIEUMAU != 'BM0004' AND
MABIEUMAU != 'BM0005'

select * from coso

select * from bienlai
where malophoc = 'L0010001'
and NGAYXACNHAN >=  DATETIME2FROMPARTS(

exec HienThiCTBuoiHocLichHoc @malichhoc = 'LICH01', @malophoc = 'L0010001', @trang = -1,@soluong = -1


set dateformat dMy
SELECT * FROM TT_GHIDANH
WHERE malophoc = 'L0010002'

SELECT * FROM LOPHOC
WHERE malophoc = 'L0010002'

SELECT * FROM LICHHOC
WHERE malophoc = 'L0010002'

delete from TT_GHIDANH
where malophoc = 'L0010002'

delete from LOPHOC
where malophoc = 'L0010002'

delete from LICHHOC
where malophoc = 'L0010002'

select * from NGUOIDUNG_NHOMNGUOIDUNG
where MATAIKHOAN = 'GV0001'

SELECT * FROM GIANGVIEN

select TT_CHONGIOHOC.MACAHOC, THU.THU_INT
from TT_CHONGIOHOC, THU
where THU.MATHU = TT_CHONGIOHOC.MATHU
AND MADANGKYKHOAHOC = 'DK000100181'

SELECT G.MADANGKYKHOAHOC, D.MADIEM, DK.MAHOCVIEN, H.TENHOCVIEN, G.MALOPHOC, K.TENKHOAHOC, LD.TENLOAIDIEM, D.LAN, D.DIEM
FROM  TT_GHIDANH G, TT_DANGKYKHOAHOC DK, DIEM D,HOCVIEN H, KHOAHOC K, LOAIDIEM LD
where G.MADANGKYKHOAHOC = DK.MADANGKYKHOAHOC
and D.MADANGKYKHOAHOC = DK.MADANGKYKHOAHOC
and H.MAHOCVIEN = DK.MAHOCVIEN
and K.MAKHOAHOC = DK.MAKHOAHOC
and LD.MALOAIDIEM = D.MALOAIDIEM

select * 
from  LOPHOC L, KHOAHOC K
where L.MAKHOAHOC = K.MAKHOAHOC

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
---------------------------------------------TEST------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
SELECT * FROM dbo.ThongTinNguoiDungVaNhomQuyen ()
select * from NHOMNGUOIDUNG

delete NGUOIDUNG_NHOMNGUOIDUNG 
where MATAIKHOAN = 'GV0001'

select * from GiaoDienTheoTaiKhoan('NV0001')

select * from NHOMNGUOIDUNG_GIAODIEN
where MANHOMNGUOIDUNG = 'NND001'

EXEC dbo.CapNhatThaoTacCuaNhomNguoiDung @manhomnguoidung = 'NND001', 
@magiaodien = 'MH0003', @mathaotac = 'TT0003', @checked = 1

 select * from THAOTAC
 select * from NHOMNGUOIDUNG_MANHINH
 exec dbo.ThemNguoiDungVaoNhomNguoiDung @manhom = 'NND001', @manguoidung = 'HV0001'
 

insert into nhomnguoidung
values ('NND004','MASTER',null)

insert into NGUOIDUNG_NHOMNGUOIDUNG
values('GV0003','NND004')

DELETE FROM NGUOIDUNG_NHOMNGUOIDUNG
WHERE        (MANHOMNGUOIDUNG= 'NND001') AND (MATAIKHOAN  = 'HV0024')

delete NGUOIDUNG_NHOMNGUOIDUNG 
where MATAIKHOAN = 'GV0003'
and MANHOMNGUOIDUNG = 'NND001'
