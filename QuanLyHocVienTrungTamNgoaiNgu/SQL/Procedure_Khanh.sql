﻿
--#1 Hiển thị danh sách người dùng NHANVIEN
go
create proc HienThiDanhSachNguoiDungNhanVien(@trang int,@soluong int)
as
	declare @start int
	set @start = (@trang-1)*@soluong
		select NV.MANHANVIEN 'Mã Nhân Viên', NV.TENNHANVIEN 'Tên Nhân Viên',NV.MATAIKHOAN 'Tên Tài Khoản',NV.GIOITINH 'Giới Tính', 
		convert(varchar, NV.NGAYSINH,103) 'Ngày Sinh', NV.SODIENTHOAI 'Số Điện Thoại', NV.CMND 'CMND', NV.MAIL 'Mail', NV.DIACHI 'Địa Chỉ', NV.GHICHU 'Ghi Chú'
		from NHANVIEN NV	
	order by MANHANVIEN
	offset @start rows
	fetch next @soluong rows only


--#2 Hiển thị danh sách người dùng HOCVIEN
go
create proc HienThiDanhSachNguoiDungHocVien(@trang int, @soluong int)
as
	declare @start int
	set @start = (@trang-1)*@soluong
		select HV.MAHOCVIEN 'Mã Học Viên', HV.TENHOCVIEN 'Tên Học Viên',HV.MATAIKHOAN 'Tên Tài Khoản',HV.GIOITINH 'Giới Tính',
		convert(varchar, HV.NGAYSINH,103)  'Ngày Sinh', HV.SODIENTHOAI 'Số Điện Thoại', HV.CMND 'CMND', HV.MAIL 'Mail', HV.DIACHI 'Địa Chỉ', HV.GHICHU 'Ghi Chú'
		from HOCVIEN HV	
	order by MAHOCVIEN
	offset @start rows
	fetch next @soluong rows only



--#3 Hiển thị danh sách người dùng GIANGVIEN
go
create proc HienThiDanhSachNguoiDungGiangVien(@trang int, @soluong int)
as
	declare @start int
	set @start = (@trang-1)*@soluong
		select GV.MAGIANGVIEN 'Mã Giảng Viên', GV.TENGIANGVIEN 'Tên Giảng Viên',GV.MATAIKHOAN 'Tên Tài Khoản',GV.GIOITINH 'Giới Tính',
		convert(varchar, GV.NGAYSINH,103) 'Ngày Sinh', GV.SODIENTHOAI 'Số Điện Thoại', GV.CMND 'CMND', GV.MAIL 'Mail', GV.DIACHI 'Địa Chỉ', GV.GHICHU 'Ghi Chú'
		from GIANGVIEN GV	
	order by MAGIANGVIEN
	offset @start rows
	fetch next @soluong rows only


--#4 Đổi mật khẩu thông tin người dùng NhanVien
go
create proc dbo.DoiMatKhauNguoiDungNhanVien(@manhanvien char(6),@matkhaucu varchar(30),@matkhaumoi varchar(30))
as
	if( (select count(*)
		 from NGUOIDUNG, NHANVIEN 
		 where NHANVIEN.MATAIKHOAN=NGUOIDUNG.MATAIKHOAN
		 and NHANVIEN.MANHANVIEN = @manhanvien) =1)
		BEGIN
		update NGUOIDUNG
		set NGUOIDUNG.MATKHAU = @matkhaumoi
			where NGUOIDUNG.MATAIKHOAN = @manhanvien
			and   NGUOIDUNG.MATKHAU = @matkhaucu
		END
	ELSE
		return 0


	


--#5 Đổi mật khẩu người dùng HOCVIEN
go
create proc dbo.DoiMatKhauNguoiDungHocVien(@mahocvien char(6),@matkhaucu varchar(30),@matkhaumoi varchar(30))
as
	if( (select count(*)
		 from NGUOIDUNG, HOCVIEN
		 where HOCVIEN.MATAIKHOAN=NGUOIDUNG.MATAIKHOAN
		  and HOCVIEN.MAHOCVIEN = @mahocvien) =1)
		BEGIN
		update NGUOIDUNG
		set NGUOIDUNG.MATKHAU = @matkhaumoi
			where NGUOIDUNG.MATAIKHOAN = @mahocvien
			and   NGUOIDUNG.MATKHAU = @matkhaucu
		END
	ELSE
		return 0


--#6 Đổi mật khẩu người dùng GIANGVIEN
go
create proc dbo.DoiMatKhauNguoiDungGiangVien(@magiangvien char(6),@matkhaucu varchar(30),@matkhaumoi varchar(30))
as
	if( (select count(*)
		 from NGUOIDUNG, GIANGVIEN 
		 where GIANGVIEN.MATAIKHOAN=NGUOIDUNG.MATAIKHOAN 
		 and GIANGVIEN.MAGIANGVIEN = @magiangvien) =1)
		BEGIN
		update NGUOIDUNG
		set NGUOIDUNG.MATKHAU = @matkhaumoi
			where NGUOIDUNG.MATAIKHOAN = @magiangvien
			and   NGUOIDUNG.MATKHAU = @matkhaucu
		END
	ELSE
		return 0
--#7 Cập nhật thông tin người dùng NHANVIEN
go
create proc dbo.CapNhatThongTinNhanVien(@manhanvien char(6), @tennhanvien nvarchar(30), @mataikhoan char(6),@gioitinh nvarchar(5), @ngaysinh date, @cmnd char(14),@mail varchar(30), @sodienthoai varchar(12), @diachi nvarchar(50))
as
	if(
		(select count(*)
		 from NHANVIEN
		 where MANHANVIEN = @manhanvien)=1)
		begin 
			update NHANVIEN
			set	   TENNHANVIEN = @tennhanvien, GIOITINH = @gioitinh, NGAYSINH = @ngaysinh, CMND = @cmnd, MAIL = @mail, SODIENTHOAI = @sodienthoai, DIACHI = @diachi
			where  MANHANVIEN = @manhanvien
		
		end
	else
		begin
			declare @manv char(6)
			declare @lasthv int
			set @manv= (select Max(MAHOCVIEN) from HOCVIEN) 
			set @manv = SUBSTRING(@manv,len(@manv)-3,4)
			set @lasthv = CONVERT(int,@manv)+1
			set @manv = concat('000', CONVERT(char(6), @lasthv ))
			set @manv =concat('NV',substring(@manv,len(@manv)-3,4))
			insert into NHANVIEN(MANHANVIEN,MATAIKHOAN,TENNHANVIEN, GIOITINH,NGAYSINH,CMND,MAIL,SODIENTHOAI,DIACHI)
			values(@manv,@manv, @tennhanvien, @gioitinh, @ngaysinh, @cmnd, @mail, @sodienthoai, @diachi)	
			return @@rowcount
		end  
--#8 Cập nhật thông tin người dùng HOCVIEN
go
create proc dbo.CapNhatThongTinHocVien(@mahocvien char(6), @tenhocvien nvarchar(30), @mataikhoan char(6),@gioitinh nvarchar(5), @ngaysinh date, @cmnd char(14),@mail varchar(30), @sodienthoai varchar(12), @diachi nvarchar(50))
as
	if(
		(select count(*)
		 from HOCVIEN
		 where MAHOCVIEN = @mahocvien)=1)
		begin 
			update HOCVIEN
			set	   TENHOCVIEN = @tenhocvien, MATAIKHOAN = @mataikhoan, GIOITINH = @gioitinh, NGAYSINH = @ngaysinh, CMND = @cmnd, MAIL = @mail, SODIENTHOAI = @sodienthoai, DIACHI = @diachi
			where  MAHOCVIEN = @mahocvien
		
		end
	else
		begin
			declare @mahv char(6)
			declare @lasthv int
			set @mahv= (select Max(MAHOCVIEN) from HOCVIEN) 
			set @mahv = SUBSTRING(@mahv,len(@mahv)-3,4)
			set @lasthv = CONVERT(int,@mahv)+1
			set @mahv = concat('000', CONVERT(char(6), @lasthv ))
			set @mahv =concat('HV',substring(@mahv,len(@mahv)-3,4))
			insert into HOCVIEN(MAHOCVIEN,MATAIKHOAN,TENHOCVIEN,GIOITINH,NGAYSINH,CMND,SODIENTHOAI,MAIL,DIACHI)
			values(@mahv,@mahv,@tenhocvien ,@gioitinh, @ngaysinh, @cmnd, @sodienthoai, @mail, @diachi)	
		end  
--#9 Cập nhật thông tin người dùng GIANGVIEN
go
create proc dbo.CapNhatThongTinGiangVien(@magiangvien char(6), @tengiangvien nvarchar(30), @mataikhoan char(6),@gioitinh nvarchar(5), @ngaysinh date, @cmnd char(14),@mail varchar(30), @sodienthoai varchar(12), @diachi nvarchar(50),@trinhdo nvarchar(30), @chuyenmon nvarchar(30))
as
	if(
		(select count(*)
		 from GIANGVIEN
		 where MAGIANGVIEN = @magiangvien)=1)
		begin 
			update GIANGVIEN
			set	   TENGIANGVIEN = @tengiangvien, MATAIKHOAN = @mataikhoan, GIOITINH = @gioitinh, NGAYSINH = @ngaysinh, CMND = @cmnd, MAIL = @mail, SODIENTHOAI = @sodienthoai, DIACHI = @diachi,TRINHDO = @trinhdo, CHUYENMON=@chuyenmon
			where  MAGIANGVIEN = @magiangvien
	
		end
	else
		begin
			declare @magv char(6)
			declare @lasthv int
			set @magv= (select Max(MAHOCVIEN) from HOCVIEN) 
			set @magv = SUBSTRING(@magv,len(@magv)-3,4)
			set @lasthv = CONVERT(int,@magv)+1
			set @magv = concat('000', CONVERT(char(6), @lasthv ))
			set @magv =concat('GV',substring(@magv,len(@magv)-3,4))
			insert into GIANGVIEN(MAGIANGVIEN, MATAIKHOAN, TENGIANGVIEN,GIOITINH,NGAYSINH,CMND,SODIENTHOAI,MAIL,DIACHI,TRINHDO,CHUYENMON)
			values(@magv,@magv,@tengiangvien ,@gioitinh, @ngaysinh, @cmnd, @sodienthoai, @mail, @diachi,@trinhdo, @chuyenmon)	
			
		end  



--#10 Xóa tài khoản người dùng
go
create proc dbo.XoaNguoiDung(@mataikhoan char(6))
as
begin
	delete NGUOIDUNG
	where MATAIKHOAN = @mataikhoan
	return @@ROWCOUNT
end

--#11 Khóa / Mở tài khoản người dùng 
go
create proc KhoaMoTaiKhoanNguoiDung(@mataikhoan char(6))
as
	update NGUOIDUNG
	set HOATDONG = case when HOATDONG = 0 then 1 else 0 end
	where MATAIKHOAN = @mataikhoan
	return @@ROWCOUNT

go

--#12 Cập nhật avatar NHANVIEN
create proc CapNhatAnhDaiDienNhanVien(@mataikhoan char(6), @hinh varbinary(MAX))
as
	update NHANVIEN
	set HINH = @hinh
	WHERE MATAIKHOAN = @mataikhoan

	select * from nhanvien

--#13 Cập nhật avatar 
go
create proc CapNhatAnhDaiDienHocVien(@mataikhoan char(6), @hinh varbinary(max))
as
	update HOCVIEN
	set HINH = @hinh
	WHERE MATAIKHOAN = @mataikhoan
--#14 Cập nhật avatar GIANGVIEN
go
create proc CapNhatAnhDaiDienGiangVien(@mataikhoan char(6), @hinh varbinary(max))
as
	update GIANGVIEN
	set HINH = @hinh
	WHERE MATAIKHOAN = @mataikhoan
	
--#15 Lấy Thông tin HOCVIEN
go
create proc LayThongTinHocVien(@mahocvien char(6))
as 
	select MAHOCVIEN,MATAIKHOAN,TENHOCVIEN,GIOITINH,convert(varchar(10),NGAYSINH,103)'NGAYSINH',CMND,SODIENTHOAI,MAIL,DIACHI,GHICHU,HINH from HOCVIEN
	where MAHOCVIEN = @mahocvien
--#17 Lấy thông tin NHANVIEN
go
create proc LayThongTinNhanVien(@manhanvien char(6))
as 
	select MANHANVIEN,MATAIKHOAN,TENNHANVIEN,GIOITINH,convert(varchar(10),NGAYSINH,103)'NGAYSINH',CMND,SODIENTHOAI,MAIL,DIACHI,GHICHU,HINH from NHANVIEN
	where MANHANVIEN = @manhanvien
--#18 Lấy Thông tin GIANGVIEN
go
create proc LayThongTinGiangVien(@magiangvien char(6))
as 
	select MAGIANGVIEN,MATAIKHOAN,TENGIANGVIEN,GIOITINH,convert(varchar(10),NGAYSINH,103)'NGAYSINH',CMND,SODIENTHOAI,MAIL,DIACHI,TRINHDO,CHUYENMON,GHICHU,HINH from GIANGVIEN
	where MAGIANGVIEN = @magiangvien

--#19 Thêm một Nhân Viên

go
create proc dbo.ThemNhanVien(@tennhanvien nvarchar(30),@gioitinh nvarchar(5), @ngaysinh date, @cmnd char(14),@mail varchar(30), @sodienthoai varchar(12), @diachi nvarchar(50),@hinh varbinary(max))
as
	begin
		declare @manv char(6)
		declare @lasthv int
		set @manv= (select Max(MAHOCVIEN) from HOCVIEN) 
		set @manv = SUBSTRING(@manv,len(@manv)-3,4)
		set @lasthv = CONVERT(int,@manv)+1
		set @manv = concat('000', CONVERT(char(6), @lasthv ))
		set @manv =concat('NV',substring(@manv,len(@manv)-3,4))
		insert into NGUOIDUNG
		values(@manv,'1234',1)
		insert into NHANVIEN(MANHANVIEN,MATAIKHOAN,TENNHANVIEN, GIOITINH,NGAYSINH,CMND,MAIL,SODIENTHOAI,DIACHI, HINH)
		values(@manv,@manv, @tennhanvien, @gioitinh, @ngaysinh, @cmnd, @mail, @sodienthoai, @diachi,@hinh)	
		return @@rowcount
	end  

--#20 Thêm một Học Viên
go
create proc dbo.ThemHocVien(@tenhocvien nvarchar(30),@gioitinh nvarchar(5), @ngaysinh date, @cmnd char(14),@mail varchar(30), @sodienthoai varchar(12), @diachi nvarchar(50),@hinh varbinary(max))
as
	begin
		declare @mahv char(6)
		declare @lasthv int
		set @mahv= (select Max(MAHOCVIEN) from HOCVIEN) 
		set @mahv = SUBSTRING(@mahv,len(@mahv)-3,4)
		set @lasthv = CONVERT(int,@mahv)+1
		set @mahv = concat('000', CONVERT(char(6), @lasthv ))
		set @mahv =concat('HV',substring(@mahv,len(@mahv)-3,4))
		insert into NGUOIDUNG
		values(@mahv,'1234',1)
		insert into HOCVIEN(MAHOCVIEN,MATAIKHOAN,TENHOCVIEN,GIOITINH,NGAYSINH,CMND,SODIENTHOAI,MAIL,DIACHI,HINH)
		values(@mahv,@mahv,@tenhocvien ,@gioitinh, @ngaysinh, @cmnd, @sodienthoai, @mail, @diachi,@hinh)	
	end  

	

--#21 Thêm một Giảng Viên
go
create proc dbo.ThemGiangVien( @tengiangvien nvarchar(30),@gioitinh nvarchar(5), @ngaysinh date, @cmnd char(14),@mail varchar(30), @sodienthoai varchar(12), @diachi nvarchar(50),@trinhdo nvarchar(30), @chuyenmon nvarchar(30),@hinh varbinary(max))
as
	begin
		declare @magv char(6)
		declare @lasthv int
		set @magv= (select Max(MAHOCVIEN) from HOCVIEN) 
		set @magv = SUBSTRING(@magv,len(@magv)-3,4)
		set @lasthv = CONVERT(int,@magv)+1
		set @magv = concat('000', CONVERT(char(6), @lasthv ))
		set @magv =concat('GV',substring(@magv,len(@magv)-3,4))
		insert into NGUOIDUNG
		values(@magv,'1234',1)
		insert into GIANGVIEN(MAGIANGVIEN, MATAIKHOAN, TENGIANGVIEN,GIOITINH,NGAYSINH,CMND,SODIENTHOAI,MAIL,DIACHI,TRINHDO,CHUYENMON,HINH)
		values(@magv,@magv,@tengiangvien ,@gioitinh, @ngaysinh, @cmnd, @sodienthoai, @mail, @diachi,@trinhdo, @chuyenmon,@hinh)	
	end  
--#22 Đổi mật khẩu
go
create proc dbo.DoiMatKhau(@mataikhoan char(6), @matkhau varchar(20))
as
	begin
		update NGUOIDUNG
		set MATKHAU = @matkhau
		where MATAIKHOAN = @mataikhoan
	end
--#23 Lấy thông tin tài khoản
go
create proc dbo.LayThongTinTaiKhoan(@mataikhoan char(6))
as 
	select * from NGUOIDUNG
	where MATAIKHOAN = @mataikhoan
	
--#24 Backup toàn bộ dữ liệu
go
create proc dbo.FullBackup(@link nvarchar(Max))
as
	begin
		backup database QUANLY_HOCVIEN
		to disk = @link
	end

--#25 Backup different
go
create proc dbo.DifferentBackup(@link nvarchar(max))
as
	begin
		backup database QUANLY_HOCVIEN
		to disk = @link
		with differential
	end
--#26 Backup Transaction log
go
create proc dbo.LogBackup(@link nvarchar(max))
as
	begin
		backup log QUANLY_HOCVIEN
		to disk = @link	
	end

--#28 Restone Full or Differential
go
create proc dbo.RestoreDatabaseCompleteOrDatabaseDiff(@link nvarchar(max))
as
	begin
		restore database QuanLy_HocVien
		from disk = @link With NoRecovery	
	end
--#29 dbo.Restore Transaction log
go
create proc dbo.RestoreTransactionLog(@link nvarchar(max))
as
	begin
		restore log QuanLy_HocVien
		from disk = @link
	end

--#30 Hiển thị thông danh sách các giờ học
go 
create proc dbo.HienThiDanhDachGioHoc
as
	select MAGIOHOC 'Mã Giờ Học',THU 'Thứ', MACA 'Ca Học'
	from GIOHOC

--#31 Học viên chọn giờ học
go
create proc dbo.ChonGioHoc(@mahocvien char(6),@magiohoc char(6))
as
	begin
		declare @machongio char(6)
		declare @lastgh int
		set @machongio= (select Max(MAHOCVIEN) from HOCVIEN) 
		set @machongio = SUBSTRING(@machongio,len(@machongio)-3,4)
		set @lastgh = CONVERT(int,@machongio)+1
		set @machongio = concat('000', CONVERT(char(6), @lastgh ))
		set @machongio =concat('CG',substring(@machongio,len(@machongio)-3,4))
		insert into TT_CHONGIOHOC
		values(@machongio,@mahocvien,@magiohoc)
	end
--#32 Hiển thị danh sách đã chọn giờ học
go 
create proc dbo.HienThiDanhSachGioHocHocVienChon(@mahocvien char(6))
as
	begin
	
		select ch.MACHONGIOHOC 'Mã Chọn Giờ Học',hv.TENHOCVIEN 'Họ Và Tên',gh.THU 'Thứ',gh.MACA 'Ca Học'
		from TT_CHONGIOHOC ch, HOCVIEN hv,GIOHOC gh
		where ch.MAGIOHOC = gh.MAGIOHOC
		  and hv.MAHOCVIEN = ch.MAHOCVIEN
		  and hv.MAHOCVIEN = @mahocvien
	end
	
--#33 Xóa giờ học học viên đã chọn
go 
create proc dbo.XoaGioHocDaChon(@machongiohoc char(6))
as
	if(
		(select count(*)
		 from TT_CHONGIOHOC
		 where MACHONGIOHOC = @machongiohoc)=1)
		begin
			delete from TT_CHONGIOHOC
			where MACHONGIOHOC = @machongiohoc
		end