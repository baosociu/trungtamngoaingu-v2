﻿/*==============================================================*/
/* DBMS name:      Sybase SQL Anywhere 12                       */
/* Created on:     9/30/2017 8:21:12 PM                         */
/*==============================================================*/
use master
go
drop DATABASE QUANLY_HOCVIEN
go
CREATE DATABASE QUANLY_HOCVIEN
go
use QUANLY_HOCVIEN
go


/*==============================================================*/
/* Table: LICHSU_HUYBIENLAI                                              */
/*==============================================================*/
create table LICHSU_HUYBIENLAI 
(
	MABIENLAI            char(12)                       not null,
   MALOAIBIENLAI        char(3)                        not null,
   MADANGKYKHOAHOC      nvarchar(20)                        not null,
   MALOPHOC            char(8)                        null,
   MANHANVIENLAP			char(6)						not null,
   NGAYXACNHAN          date                           NOT null,
   SOTIEN               float                   NOT null,
   DOT                  integer                     NOT    null,
   MANHANVIENHUY		char(6)					not null,
   NGAYHUY				datetime					not null,
   constraint PK_LICHSU_HUYBIENLAI  primary key (MABIENLAI)
);


/*==============================================================*/
/* Table: BIEUMAU                                               */
/*==============================================================*/
create table BIEUMAU 
(
   MABIEUMAU            char(6)                       not null,
   TENBIEUMAU			nvarchar(50)				not null,
   TAPTIN				varbinary(max)    			not null,
   LOAI					varchar(8)					not null,
   constraint PK_MABIEUMAU primary key (MABIEUMAU)
);


/*==============================================================*/
/* Table: BIENLAI                                               */
/*==============================================================*/
create table BIENLAI 
(
   MABIENLAI            char(12)                       not null,
   MALOAIBIENLAI        char(3)                        not null,
   MADANGKYKHOAHOC      nvarchar(20)                        not null,
   MALOPHOC            char(8)                        null,
   MANHANVIEN			char(6)						not null,
   NGAYXACNHAN          date                           NOT null,
   SOTIEN               float                   NOT null,
   DOT                  integer                     NOT    null,
   constraint PK_BIENLAI primary key (MABIENLAI)
);



/*==============================================================*/
/* Table: CAHOC                                                 */
/*==============================================================*/
create table CAHOC 
(
   MACA                 char(3)                        not null,
   GIOBATDAU            time                           null,
   GIOKETTHUC           time                           null,
   constraint PK_CAHOC primary key (MACA)
);



/*==============================================================*/
/* Table: NGUOIDUNG_NHONNGUOIDUNG                                                   */
/*==============================================================*/
create table NGUOIDUNG_NHOMNGUOIDUNG
(
   MATAIKHOAN           char(6)                        not null,
   MANHOMNGUOIDUNG      char(6)                        not null,
   HOATDONG				bit								default 1,
   constraint PK_NGUOIDUNG_NHOMNGUOIDUNG primary key  (MATAIKHOAN, MANHOMNGUOIDUNG)
);

/*==============================================================*/
/* Table: NHOMNGUOIDUNG_GIAODIEN                                 */
/*==============================================================*/
create table NHOMNGUOIDUNG_GIAODIEN
(
   MAGIAODIEN            char(6)                        not null,
   MANHOMNGUOIDUNG       char(6)                        not null,
   MATHAOTAC			char(6) default 'TT0001'		not null,
   constraint PK_NHOMNGUOIDUNG_GIAODIEN primary key  (MAGIAODIEN, MANHOMNGUOIDUNG,MATHAOTAC)
);




/*==============================================================*/
/* Table: COSO                                                  */
/*==============================================================*/
create table COSO 
(
   MACOSO               char(5)                        not null,
   TENCOSO              nvarchar(50)                    null,
   constraint PK_COSO primary key (MACOSO)
);



/*==============================================================*/
/* Table: CT_BUOIHOC                                            */
/*==============================================================*/
create table CT_BUOIHOC 
(
   MALICHHOC            char(6)                        not null,
   MADANGKYKHOAHOC     nvarchar(20)                       not null,
   MALOPHOC            char(8)                        not null,
   DIEMDANH             bit                            null,
   constraint PK_CT_BUOIHOC primary key  (MALICHHOC, MADANGKYKHOAHOC, MALOPHOC)
);


/*==============================================================*/
/* Table: DIEM                                                  */
/*==============================================================*/
create table DIEM 
(
   MADIEM               char(6)                        not null,
   MADANGKYKHOAHOC            nvarchar(20)                        not null,
   MALOPHOC            char(8)                        not null,
   MALOAIDIEM           char(3)                        not null,
   LAN                  integer                        not null,
   DIEM                 float                          null,
   GHICHU               nvarchar(50)                   null,
   constraint PK_DIEM primary key (MADIEM,MADANGKYKHOAHOC,MALOPHOC)
);


/*==============================================================*/
/* Table: GIANGVIEN                                             */
/*==============================================================*/
create table GIANGVIEN 
(
   MAGIANGVIEN          char(6)                         not null,
   MATAIKHOAN           char(6)                         null,
   TENGIANGVIEN         nvarchar(50)                    null,
   GIOITINH             nvarchar(5)                     null,
   NGAYSINH             date                            null,
   CMND                 char(14)                        null,
   SODIENTHOAI          char(12)                        null,
   MAIL                 varchar(30)                     null,
   DIACHI               nvarchar(50)                    null,
   TRINHDO              nvarchar(30)                    null,
   CHUYENMON            nvarchar(30)                    null,
   GHICHU               nvarchar(50)                    null,
   HINH                 varbinary(MAX)                      null,
   constraint PK_GIANGVIEN primary key (MAGIANGVIEN)
);

/*==============================================================*/
/* Table: GIAODIEN                                              */
/*==============================================================*/
create table GIAODIEN 
(
   MAGIAODIEN            char(6)                        not null,
   TENMANHINH           nvarchar(50)                   null,
   constraint PK_GIAODIEN primary key (MAGIAODIEN)
);

/*==============================================================*/
/* Table: HOCVIEN                                               */
/*==============================================================*/
create table HOCVIEN 
(
   MAHOCVIEN            char(6)                        not null,
   MATAIKHOAN           char(6)                        null,
   TENHOCVIEN           nvarchar(30)                   null,
   GIOITINH             nvarchar(5)                    null,
   NGAYSINH             date                           null,
   CMND                 char(14)                       null,
   SODIENTHOAI          char(12)                       null,
   MAIL                 varchar(30)                    null,
   DIACHI               nvarchar(50)                   null,
   GHICHU               nvarchar(50)                   null,
   HINH                 varbinary(MAX)                 null,
   constraint PK_HOCVIEN primary key (MAHOCVIEN)
);


/*==============================================================*/
/* Table: KHOAHOC                                               */
/*==============================================================*/
create table KHOAHOC 
(
   MAKHOAHOC            char(6)                        not null,
   TENKHOAHOC			NVARCHAR(50)						null,
   HOCPHI				float                       null,
   constraint PK_KHOAHOC primary key (MAKHOAHOC)
);


/*==============================================================*/
/* Table: LOPHOC                                               */
/*==============================================================*/
create table LOPHOC 
(
   MALOPHOC            char(8)                        not null,
   MAKHOAHOC			char(6)						not null,
   MAGIANGVIEN          char(6)                        null,
   SOLUONG				int		default 0		null,
   TINHTRANG			nvarchar(20)	default N'CHƯA KẾT THÚC' null,
   constraint PK_LOPHOC primary key (MALOPHOC)
);
/*==============================================================*/
/* Table: LICHHOC                                               */
/*==============================================================*/
create table LICHHOC 
(
   MALICHHOC            char(6)                        not null,
   MACA                 char(3)                        not null,
   NGAYHOC              date                           not null,
   MACOSO               char(5)                        not null,
   MAPHONG              char(5)                        not null,
   MALOPHOC            char(8)                         not null,
   TINHTRANG			nvarchar(20) default N'CHƯA KẾT THÚC' null,
    constraint PK_LICHHOC primary key  (MALICHHOC, MALOPHOC)
);


/*==============================================================*/
/* Table: LOAIDIEM                                              */
/*==============================================================*/
create table LOAIDIEM 
(
   MALOAIDIEM           char(3)                        not null,
   TENLOAIDIEM          nvarchar(20)                   null,
   constraint PK_LOAIDIEM primary key (MALOAIDIEM)
);



/*==============================================================*/
/* Table: LOAI_BIENLAI                                          */
/*==============================================================*/
create table LOAI_BIENLAI 
(
   MALOAIBIENLAI        char(3)                        not null,
   TENLOAIBIENLAI       nvarchar(30)                   null,
   constraint PK_LOAI_BIENLAI primary key (MALOAIBIENLAI)
);



/*==============================================================*/
/* Table: NGAYHOC                                               */
/*==============================================================*/
create table NGAYHOC 
(
   NGAYHOC              date                           not null,
   NGHILE               bit                            null,
   constraint PK_NGAYHOC primary key (NGAYHOC)
);


/*==============================================================*/
/* Table: NGUOIDUNG                                             */
/*==============================================================*/
create table NGUOIDUNG 
(
   MATAIKHOAN           char(6)                        not null,
   MATKHAU              varchar(30)                    null,
   HOATDONG             bit                            null,
   constraint PK_NGUOIDUNG primary key (MATAIKHOAN)
);


/*==============================================================*/
/* Table: NHANVIEN                                              */
/*==============================================================*/
create table NHANVIEN 
(
   MANHANVIEN           char(6)                         not null,
   MATAIKHOAN           char(6)                         null,
   TENNHANVIEN          nvarchar(30)                    null,
   GIOITINH             nvarchar(5)                     null,
   NGAYSINH             date                            null,
   CMND                 char(14)                        null,
   MAIL                 varchar(30)                     null,
   SODIENTHOAI          varchar(12)                     null,
   DIACHI               nvarchar(50)                    null,
   GHICHU               nvarchar(50)                    null,
   HINH                 varbinary(MAX)                        null,
   constraint PK_NHANVIEN primary key (MANHANVIEN)
);


/*==============================================================*/
/* Table: NHOMNGUOIDUNG                                         */
/*==============================================================*/
create table NHOMNGUOIDUNG 
(
   MANHOMNGUOIDUNG      char(6)                        not null,
   TENNHOMNGUOIDUNG     nvarchar(50)                   null,
   GHICHU               nvarchar(50)                   null,
   constraint PK_NHOMNGUOIDUNG primary key (MANHOMNGUOIDUNG)
);

/*==============================================================*/
/* Table: PHONG                                                 */
/*==============================================================*/
create table PHONG 
(
   MACOSO               char(5)                        not null,
   MAPHONG              char(5)                        not null,
   SOLUONGCHONGOI		int							not null,
   constraint PK_PHONG primary key (MACOSO, MAPHONG)
);


/*==============================================================*/
/* Table: THAOTAC                                               */
/*==============================================================*/
create table THAOTAC 
(
   MATHAOTAC            char(6)                        not null,
   TENTHAOTAC           nvarchar(30)                   null,
   constraint PK_THAOTAC primary key (MATHAOTAC)
);


/*==============================================================*/
/* Table: THOIGIANHOC                                           */
/*==============================================================*/
create table THOIGIANHOC 
(
   MACA                 char(3)                        not null,
   NGAYHOC              date                           not null,
   constraint PK_THOIGIANHOC primary key  (MACA, NGAYHOC)
);


/*==============================================================*/
/* Table: TT_DANGKYKHOAHOC				                            */
/*==============================================================*/

GO
create table TT_DANGKYKHOAHOC
(
	MADANGKYKHOAHOC		nvarchar(20)			not null, --DK+MAKHOAHOC+HOCVIEN+LAN
   MAHOCVIEN            char(6)                        not null,
   MANHANVIEN           char(6)                        not null,
   MAKHOAHOC			char(6)							not null,
   langhidanh			int							not null,
   DIEMDAUVAO			float						null,
   NGAY					date						not null,
   constraint PK_TT_DANGKYKHOAHOC primary key  (MADANGKYKHOAHOC)
);

/*==============================================================*/
/* Table: TT_GHIDANH				                            */
/*==============================================================*/

GO
create table TT_GHIDANH 
(
	MADANGKYKHOAHOC		nvarchar(20)			not null,	
   MALOPHOC             char(8)                        not null,
   constraint PK_TT_GHIDANH primary key  (MADANGKYKHOAHOC,MALOPHOC)
);

/*==============================================================*/
/* Table: THU                                                   */
/*==============================================================*/
create table THU 
(
   MATHU                        char(3)                         not null,
   TENTHU_VI			nvarchar(20)                    not null,	
   TENTHU_EN			nvarchar(20)                    not null,	
   THU_INT			int not null,	
   constraint PK_THU primary key (MATHU)
);

/*==============================================================*/
/* Table: CHONTHOIGIAN                                          */
/*==============================================================*/
create table TT_CHONGIOHOC 
(
	MADANGKYKHOAHOC		nvarchar(20)					not null,
   MACAHOC				char(3)                         not null,
   MATHU				char(3)							not null,
   GHICHU				varchar(50)						
   constraint PK_TT_CHONGIOHOC primary key (MADANGKYKHOAHOC,MACAHOC, MATHU)
);

/*==============================================================*/
/* Table: BACKUP                                                */
/*==============================================================*/
create table TT_BACKUP 
(
   MABACKUP             char(20)                         not null,
   TENTAPTIN		    nvarchar(50)                    not null,
   KIEUBACKUP			nvarchar(30)                    not null,
   NGAYLUU				date				            not null,
   MANHANVIEN 			char(6)							not null,
   DUONGDAN				nvarchar(max)					not null,						
   constraint PK_TT_BACKUP primary key (MABACKUP)
);

/*==============================================================*/
/* Table: KHOA NGOAI                                            */
/*==============================================================*/

alter table TT_BACKUP
   add constraint FK_TT_BACKUP_DO_NHANVIEN foreign key (MANHANVIEN)
      references NHANVIEN (MANHANVIEN);

go
alter table TT_CHONGIOHOC
   add constraint FK_TT_CHONGIOHOC_CUA_THU foreign key (MATHU)
      references THU (MATHU);

go
alter table TT_CHONGIOHOC
   add constraint FK_TT_CHONGIOHOC_CUA_TTDANGKYKHOAHOC foreign key (MADANGKYKHOAHOC)
      references TT_DANGKYKHOAHOC (MADANGKYKHOAHOC);
go


alter table TT_CHONGIOHOC
   add constraint FK_TT_CHONGIOHOC_CUA_CAHOC foreign key (MACAHOC)
      references CAHOC (MACA);

go
alter table LICHSU_HUYBIENLAI
   add constraint FK_HUYBIENLAI_DO_NHANVIEN foreign key (MANHANVIENLAP)
      references NHANVIEN (MANHANVIEN);

go
alter table BIENLAI
   add constraint FK_BIENLAI_DO_NHANVIEN foreign key (MANHANVIEN)
      references NHANVIEN (MANHANVIEN);
GO
alter table BIENLAI
   add constraint FK_BIENLAI_CUA_TT_DANGKYKHOAHOC foreign key (MADANGKYKHOAHOC)
      references TT_DANGKYKHOAHOC (MADANGKYKHOAHOC);


go
alter table BIENLAI
   add constraint FK_BIENLAI_THUOC__LOAI_BIENLAI foreign key (MALOAIBIENLAI)
      references LOAI_BIENLAI (MALOAIBIENLAI);


go


alter table NGUOIDUNG_NHOMNGUOIDUNG
   add constraint FK_NGUOIDUNG_THUOC_NHOMNGUOIDUNG foreign key (MANHOMNGUOIDUNG)
      references NHOMNGUOIDUNG (MANHOMNGUOIDUNG);
go

alter table NGUOIDUNG_NHOMNGUOIDUNG
   add constraint FK_NHOMNGUOIDUNG_CO_NGUOIDUNG foreign key (MATAIKHOAN)
      references NGUOIDUNG (MATAIKHOAN);

go



alter table NHOMNGUOIDUNG_GIAODIEN
   add constraint FK_GIAODIEN_CUA_NHOMNGUOIDUNG foreign key (MANHOMNGUOIDUNG)
      references NHOMNGUOIDUNG (MANHOMNGUOIDUNG);
go

alter table NHOMNGUOIDUNG_GIAODIEN
   add constraint FK_NHOMNGUOIDUNG_TRUYCAP_GIAODIEN foreign key (MAGIAODIEN)
      references GIAODIEN (MAGIAODIEN) ;
go

alter table NHOMNGUOIDUNG_GIAODIEN
   add constraint FK_NHOMNGUOIDUNG_CO_THAOTAC_VOI_GIAODIEN foreign key (MATHAOTAC)
      references THAOTAC (MATHAOTAC) ;
go



alter table CT_BUOIHOC
   add constraint FK_CT_BUOIH_CUA_LICHHOC foreign key (MALICHHOC,MALOPHOC)
      references LICHHOC (MALICHHOC,MALOPHOC);
go

alter table CT_BUOIHOC
   add constraint FK_CT_BUOIH_TU_TT_GHIDA foreign key (MADANGKYKHOAHOC, MALOPHOC)
      references TT_GHIDANH (MADANGKYKHOAHOC, MALOPHOC);

go


alter table DIEM
   add constraint FK_DIEM_LAYTU_TT_GHIDA foreign key (MADANGKYKHOAHOC, MALOPHOC)
      references TT_GHIDANH (MADANGKYKHOAHOC, MALOPHOC) ;
go

alter table DIEM
   add constraint FK_DIEM_THUOC_LOAIDIEM foreign key (MALOAIDIEM)
      references LOAIDIEM (MALOAIDIEM);


go



alter table GIANGVIEN
   add constraint FK_GIANGVIEN_THUOC_NGUOIDUNG foreign key (MATAIKHOAN)
      references NGUOIDUNG (MATAIKHOAN);

go



alter table HOCVIEN
   add constraint FK_HOCVIEN_THUOC_NGUOIDUNG foreign key (MATAIKHOAN)
      references NGUOIDUNG (MATAIKHOAN) ;

go



alter table LOPHOC
	add constraint FK_LOPHOC_CUA_KHOAHOC foreign key (MAKHOAHOC)
		references KHOAHOC(MAKHOAHOC)

go


alter table LOPHOC
   add constraint FK_LOPHOC_CUA_GIANGVIEN foreign key (MAGIANGVIEN)
      references GIANGVIEN (MAGIANGVIEN) ;

go


alter table LICHHOC
   add constraint FK_LICHHOC_CO_THOIGIAN foreign key (MACA, NGAYHOC)
      references THOIGIANHOC (MACA, NGAYHOC);
go

alter table LICHHOC
   add constraint FK_LICHHOC_O_PHONG foreign key (MACOSO, MAPHONG)
      references PHONG (MACOSO, MAPHONG);
go

alter table LICHHOC
   add constraint FK_LICHHOC_CUA_LOPHOC foreign key (MALOPHOC)
      references LOPHOC (MALOPHOC);
go




alter table NHANVIEN
   add constraint FK_NHANVIEN_THUOC_NGUOIDUNG foreign key (MATAIKHOAN)
      references NGUOIDUNG (MATAIKHOAN);

go


alter table PHONG
   add constraint FK_PHONG_THUOC_COSO foreign key (MACOSO)
      references COSO (MACOSO) ;
go



alter table THOIGIANHOC
   add constraint FK_THOIGIAN_CHUA_CAHOC foreign key (MACA)
      references CAHOC (MACA);
go

alter table THOIGIANHOC
   add constraint FK_THOIGIAN_CHUA_NGAYHOC foreign key (NGAYHOC)
      references NGAYHOC (NGAYHOC) ;

go


alter table TT_GHIDANH
   add constraint FK_TT_GHIDANH_CUA_LOPHOC foreign key (MALOPHOC)
      references LOPHOC (MALOPHOC);
go

alter table TT_GHIDANH
   add constraint FK_TT_GHIDANH_CUA_DANGKYKHOAHOC foreign key (MADANGKYKHOAHOC)
      references TT_DANGKYKHOAHOC (MADANGKYKHOAHOC);

ALTER table TT_DANGKYKHOAHOC
add constraint FK_TT_DANGKYKHOAHOC_CUA_HOCVIEN foreign key (MAHOCVIEN)
	references HOCVIEN(MAHOCVIEN);
go

ALTER table TT_DANGKYKHOAHOC
add constraint FK_TT_DANGKYKHOAHOC_DO_NHANVIEN foreign key (MANHANVIEN)
	references NHANVIEN(MANHANVIEN);

go

ALTER table TT_DANGKYKHOAHOC
add constraint FK_TT_DANGKYKHOAHOC_CUA_KHOAHOC foreign key (MAKHOAHOC)
	references KHOAHOC(MAKHOAHOC);
/*==============================================================*/
/* Table: CONSTRAINT                                             */
/*==============================================================*/
------DEFAULT
go
--TABLE NGUOIDUNG
ALTER TABLE NGUOIDUNG
ADD CONSTRAINT DEFAULT_HOATDONG_NGUOIDUNG DEFAULT 1 FOR HOATDONG
--NGAYHOC
ALTER TABLE NGAYHOC
ADD CONSTRAINT DEFAULT_NGHILE_NGAYHOC DEFAULT 0 FOR NGHILE
--CT_BUOIHOC
ALTER TABLE CT_BUOIHOC
ADD CONSTRAINT DEFAULT_DIEMDANH_CTBUOIHOC DEFAULT 0 FOR DIEMDANH
--BIENLAI
ALTER TABLE BIENLAI
ADD CONSTRAINT DEFAULT_DOT_BIENLAI DEFAULT 1 FOR DOT


--CHECK

go
ALTER TABLE KHOAHOC
ADD CONSTRAINT CK_HOCPHI_KHOAHOC CHECK (HOCPHI >= 0)

ALTER TABLE HOCVIEN
ADD CONSTRAINT CK_GIOITINH_HOCVIEN CHECK (GIOITINH = N'NAM' OR GIOITINH = N'NỮ')

ALTER TABLE GIANGVIEN
ADD CONSTRAINT CK_GIOITINH_GIANGVIEN CHECK (GIOITINH = N'NAM' OR GIOITINH = N'NỮ')

ALTER TABLE NHANVIEN
ADD CONSTRAINT CK_GIOITINH_NHANVIEN CHECK (GIOITINH = N'NAM' OR GIOITINH = N'NỮ')


ALTER TABLE DIEM
ADD CONSTRAINT CK_DIEM_DIEM CHECK (DIEM >= 0)

ALTER TABLE BIENLAI
ADD CONSTRAINT CK_SOTIEN_BIENLAI CHECK (SOTIEN >= 0)

ALTER TABLE CAHOC
ADD CONSTRAINT CK_GIOBATDAU_GIOKETTHUC_CAHOC CHECK (GIOBATDAU < GIOKETTHUC)


 

/*==============================================================*/
/* Table: ADD DATA                                              */
/*=============================================================*/
go
SET DATEFORMAT dmy


--NGUOIDUNG
INSERT INTO NGUOIDUNG(MATAIKHOAN, MATKHAU)
VALUES('HV0001','1234'),
('HV0002','1234'),
('HV0003','1234'),
('HV0004','1234'),
('HV0005','1234'),
('HV0006','1234'),
('HV0007','1234'),
('HV0008','1234'),
('HV0009','1234'),
('HV0010','1234'),
('HV0011','1234'),
('HV0012','1234'),
('HV0013','1234'),
('HV0014','1234'),
('HV0015','1234'),
('HV0016','1234'),
('HV0017','1234'),
('HV0018','1234'),
('HV0019','1234'),
('HV0020','1234'),
('HV0021','1234'),
('HV0022','1234'),
('HV0023','1234'),
('HV0024','1234'),
('GV0001','1234'),
('GV0002','1234'),
('GV0003','1234'),
('GV0004','1234'),
('GV0005','1234'),
('GV0006','1234'),
('GV0007','1234'),
('GV0008','1234'),
('NV0001','1234'),
('NV0002','1234'),
('NV0003','1234'),
('NV0004','1234'),
('NV0005','1234'),
('NV0006','1234'),
('NV0007','1234'),
('NV0008','1234'),
('NV0009','1234')
go
--HOCVIEN
INSERT INTO HOCVIEN(MAHOCVIEN, MATAIKHOAN,TENHOCVIEN, GIOITINH, NGAYSINH, CMND, SODIENTHOAI,MAIL, DIACHI)
VALUES('HV0001','HV0001',N'PHẠM QUÔC KHÁNH','NAM','10/10/1996','184536754','01696574356','PHAMKHANH18081996@GMAIL.COM',N'Q. TÂN PHÚ, TP HỒ CHÍ MINH'),
('HV0002','HV0002',N'TRƯƠNG HUỲNH GIA BẢO','NAM','02/10/1996','194586704','01646594350','BAOBAO@GMAIL.COM',N'Q. PHÚ NHUẬN, TP HỒ CHÍ MINH'),
('HV0003','HV0003',N'TRẦN VŨ ĐỨC','NAM','11/12/1996','184500759','01656594312','VUDUC1996@GMAIL.COM',N'Q. TAM ĐÔNG, ĐÀ NẴNG'),
('HV0004','HV0004',N'NGUYỄN KIỀU ĐÔNG NỮ',N'NỮ','10/07/1997','164599753','0986374550','DONGNU@GMAIL.COM',N'Q. TÂN BÌNH, TP HỒ CHÍ MINH'),
('HV0005','HV0005',N'MINH GIA HÂN',N'NỮ','01/11/1998','194577750','01646974000','GIAHAN1998@GMAIL.COM',N'H. HOOC MÔN, TP HỒ CHÍ MINH'),
('HV0006','HV0006',N'LÂM TIẾN NGHĨA',N'NAM','04/11/1999','185578851','01656984070','TIENNGHIA@GMAIL.COM',N'H. CẨM XUYÊN, HÀ TĨNH'),
('HV0007','HV0007',N'ĐINH GIA ĐẠT',N'NAM','11/11/1988','145624551','01636994020','GIADAT@GMAIL.COM',N'Q. HOÀN KIẾM, HÀ NỘI'),
('HV0008','HV0008',N'LÂM VẤN THIÊN',N'NAM','01/12/1999','195574758','01686274340','LAMVANTHIEN@GMAIL.COM',N'H. NAM ĐÀN, NGHỆ AN'),
('HV0009','HV0009',N'NAM KIẾN TRUNG',N'NAM','09/01/1998','134567751','01636998029','TRUNGVANNAM@GMAIL.COM',N'H. BÀU CÁT, ĐỒNG NAI'),
('HV0010','HV0010',N'HOÀNG KIỀU TRINH',N'NỮ','10/01/1996','294588850','01643974020','TRINHHOANGKIEU@GMAIL.COM',N'Q. BÌNH THẠNH, TP HỒ CHÍ MINH'),
('HV0011','HV0011',N'NGUYỄN MINH TÂM',N'NAM','14/02/1998','276579951','01676973980','TAMTAM@GMAIL.COM',N'Q. 5, TP HỒ CHÍ MINH'),
('HV0012','HV0012',N'MAI XUÂN DẦN',N'NAM','01/06/1995','155573721','01696975003','XUANDANTRAN@GMAIL.COM',N'H. HOOC MÔN, TP HỒ CHÍ MINH'),
('HV0013','HV0013',N'TRẦN ĐÌNH TÙNG',N'NAM','04/07/1994','1985350','01686525550','TUNGDINHTRUNG@GMAIL.COM',N'H. NGHI XUÂN, HÀ TĨNH'),
('HV0014','HV0014',N'PHẠM KIỀU NHI',N'NỮ','10/10/1999','144588792','01646974222','KIEUNHI@GMAIL.COM',N'H. ĐỒNG XOÀI, ĐỒNG NAI'),
('HV0015','HV0015',N'PHẠM LONG HOÀNG',N'NAM','18/10/1988','164570096','0946974550','HOANGLONG@GMAIL.COM',N'H. BÌNH CHÁNH, TP HỒ CHÍ MINH'),
('HV0016','HV0016',N'DƯƠNG THẾ MINH',N'NAM','01/08/1995','199777750','01696925990','THEMINHDUONG@GMAIL.COM',N'H. LONG KHÁNH, ĐỒNG NAI'),
('HV0017','HV0017',N'LÂM VĂN VÂN',N'NAM','05/10/1999','114555778','01656984008','LAMVAN@GMAIL.COM',N'H. KONTUM, ĐẮKLĂK'),
('HV0018','HV0018',N'HOÀNG KIM THỊNH',N'NAM','11/11/1997','118879951','01694978903','THINHTHINH@GMAIL.COM',N'H. NAM ĐÀN, NGHỆ AN'),
('HV0019','HV0019',N'ĐOÀN XUÂN DỰ',N'NAM','01/11/1998','104579743','01682374044','XUANDU@GMAIL.COM',N'TP. NHA TRANG, KHÁNH HÒA'),
('HV0020','HV0020',N'TRƯƠNG THẾ VINH',N'NAM','09/11/1997','142574759','01246975501','THEVINHTRUONG@GMAIL.COM',N'TP HÀ TĨNH, HÀ TĨNH'),
('HV0021','HV0021',N'NGUYỄN MINH HOA',N'NỮ','09/01/1997','199547771','016959790402','HOAHOAMINH@GMAIL.COM',N'H. KỲ ANH, HÀ TĨNH'),
('HV0022','HV0022',N'TRẦN NGUYỆT LOAN',N'NỮ','01/11/1998','104583750','01636874321','NGUYETLOAN@GMAIL.COM',N'Q. TÂN BÌNH, TP HỒ CHÍ MINH'),
('HV0023','HV0023',N'PHAN VĂN TRƯỜNG',N'NAM','11/04/1988','104475745','01698674789','VANTRUONGPHAN@GMAIL.COM',N'Q. 1, TP HỒ CHÍ MINH'),
('HV0024','HV0024',N'LẠI TÂM TÂM',N'NỮ','12/02/1995','133536097','01236574653','LAITAMTAM@GMAIL.COM',N'Q. 3, TP HỒ CHÍ MINH')

go
--GIANGVIEN 
INSERT INTO GIANGVIEN(MAGIANGVIEN, MATAIKHOAN, TENGIANGVIEN, GIOITINH, NGAYSINH, CMND, SODIENTHOAI, MAIL, DIACHI, TRINHDO, CHUYENMON)
VALUES('GV0001','GV0001',N'PHẠM LINH LINH',N'NỮ','10/11/1990','194534784','01690574055','LINHLINH1990@GMAIL.COM',N'Q. 7, TP HỒ CHÍ MINH', 'TOEIC 800', N'TIẾNG ANH THƯƠNG MẠI'),
('GV0002','GV0002',N'TRƯƠNG HUỲNH ANH',N'NAM','12/11/1989','184539981','01695544000','HUYNHANTRAN1989@GMAIL.COM',N'Q. 9, TP HỒ CHÍ MINH', 'TOEIC 750', N'TIẾNG ANH THƯƠNG MẠI'),
('GV0003','GV0003',N'LONG NHẬT NAM',N'NAM','09/07/1991','184534776','01240574000','NHATNAM@GMAIL.COM',N'Q. THANH XUÂN, HÀ NỘI', 'IELTS 7.0', N'GIAO TIẾP QUỐC TẾ'),
('GV0004','GV0004',N'HÀN TUYẾT NHI',N'NỮ','09/10/1992','198934709','01680577032','HANTUYET1992@GMAIL.COM',N'Q. ĐỐNG ĐA, HÀ NỘI', 'IELTS 7.5', N'GIAO TIẾP QUỐC TẾ'),
('GV0005','GV0005',N'LONG VẤN THIÊN',N'NAM','10/11/1990','194544799','01670574865','VANTHIEN1990@GMAIL.COM',N'Q. HAI BÀ TRƯNG, TP HỒ CHÍ MINH', 'IELTS 8.0', N'GIAO TIẾP QUỐC TẾ'),
('GV0006','GV0006',N'PHẠM NGỌC PHƯỢNG',N'NAM','12/01/1990','164524764','0960579043','VANTHIEN1990@GMAIL.COM',N'Q. 2, TP HỒ CHÍ MINH', 'TOEIC 800', N'TIẾNG ANH THƯƠNG MẠI'),
('GV0007','GV0007',N'HOANG NAM PHƯƠNG',N'NỮ','11/01/1992','194534714','0960579043','NAMPHUONG@GMAIL.COM',N'Q. 1, TP HỒ CHÍ MINH', 'N4', N'TIẾNG NHẬT'),
('GV0008','GV0008',N'NGUYỄN TIẾN ANH',N'NAM','10/06/1990','193328700','0970599032','TIENTIEN@GMAIL.COM',N'Q. 12, TP HỒ CHÍ MINH', 'N5', N'TIẾNG NHẬT')

go

--NHANVIEN
INSERT INTO NHANVIEN(MANHANVIEN,TENNHANVIEN, MATAIKHOAN, GIOITINH, NGAYSINH, CMND,MAIL, SODIENTHOAI, DIACHI)
VALUES('NV0001',N'NGUYỄN VĂN CỪ','NV0001',N'NAM','10/12/1990','186375653','HOANGCU@GMAIL.COM','0967354320',N'Q. 1. TP HỒ CHÍ MINH'),
('NV0002',N'TRẦN TIẾN BẢO','NV0002',N'NAM','12/02/1991','188375001','TIENBA01991@GMAIL.COM','0987654398',N'Q. 2. TP HỒ CHÍ MINH'),
('NV0003',N'HOA THẾ VINH','NV0003',N'NAM','03/11/1993','196345655','THEVINHHOA@GMAIL.COM','01637034101',N'Q. 5. TP HỒ CHÍ MINH'),
('NV0004',N'TRƯƠNG LAN LINH','NV0004',N'NỮ','11/07/1991','166345000','LINHLAN@GMAIL.COM','0937654365',N'Q. PHÚ NHUẬN. TP HỒ CHÍ MINH'),
('NV0005',N'PHAN XUÂN HIỂN','NV0005',N'NAM','10/01/1995','286378659','HIENXUAN@GMAIL.COM','01687654398',N'Q. BÌNH TÂN. TP HỒ CHÍ MINH'),
('NV0006',N'ĐÀO DUY TÂN','NV0006',N'NAM','12/05/1989','286685650','DUYTANDAO10@GMAIL.COM','0977854397',N'H. BÌNH CHÁNH. TP HỒ CHÍ MINH'),
('NV0007',N'LẠI THẾ NHÂN','NV0007',N'NAM','04/11/1988','183375753','LAITHENHAN@GMAIL.COM','0927854929',N'Q. ĐỐNG ĐA. HÀ NỘI'),
('NV0008',N'ĐÀM NGỌC LAN','NV0008',N'NỮ','02/07/1992','176375500','NGOCLANDAM@GMAIL.COM','01647554320',N'Q. 5. TP HỒ CHÍ MINH'),
('NV0009',N'LƯƠNG BÍCH NGỌC','NV0009',N'NỮ','08/10/1994','226375773','BICHNGOCLAN@GMAIL.COM','01697655321',N'Q. 7. TP HỒ CHÍ MINH')
go
--COSO
INSERT INTO COSO(MACOSO, TENCOSO)
VALUES('CS001',N'CS. VÕ THỊ SÁU'),
('CS002',N'CS. LÝ THÁI TỔ'),
('CS003',N'CS LÝ THƯỜNG KIỆT'),
('CS004',N'CS CỘNG HÒA')
go
--PHONG
INSERT INTO PHONG(MAPHONG,MACOSO,SOLUONGCHONGOI)
VALUES('PH001','CS001',20),
('PH002','CS001',20),
('PH003','CS001',20),
('PH004','CS001',20),
('PH001','CS002',20),
('PH002','CS002',20),
('PH003','CS002',20),
('PH004','CS002',20),
('PH001','CS003',20),
('PH002','CS003',20),
('PH003','CS003',20),
('PH004','CS003',20),
('PH001','CS004',20),
('PH002','CS004',20),
('PH003','CS004',20),
('PH004','CS004',20)
GO
--KHOAHOC
INSERT INTO KHOAHOC(MAKHOAHOC,HOCPHI,TENKHOAHOC)
VALUES('KH0001',2000000,N'TOEIC 450'),
('KH0002',2500000,N'TOEIC 550'),
('KH0003',3000000,N'TOEIC 600'),
('KH0004',3500000,N'TOEIC 650'),
('KH0005',2000000,N'GIAO TIẾP QUỐC TẾ - SƠ CẤP'),
('KH0006',3000000,N'GIAO TIẾP QUỐC TẾ - TRUNG CẤP'),
('KH0007',4000000,N'GIAO TIẾP QUỐC TẾ - CAO CẤP'),
('KH0008',2000000,N'IELTS 4.0'),
('KH0009',3000000,N'IELTS 5.0'),
('KH0010',5000000,N'IELTS 7.0'),
('KH0011',2000000,N'TIẾNG NHẤT - N1'),
('KH0012',3000000,N'TIẾNG NHẬT - N2'),
('KH0013',4000000,N'TIẾNG NHẬT - N3'),
('KH0014',5500000,N'TIẾNG NHẬT - N4'),
('KH0015',700000,N'TIẾNG NHẬT - N5')

go
--LOPHOC
DELETE FROM LOPHOC
INSERT INTO LOPHOC(MALOPHOC,MAKHOAHOC, MAGIANGVIEN)
VALUES('L0010001','KH0001','GV0001'),
('L0050001','KH0005','GV0003'),
('L0110001','KH0011','GV0007')

go
--CAHOC
INSERT INTO CAHOC(MACA, GIOBATDAU, GIOKETTHUC)
VALUES('CA1','07:30','09:00'),
('CA2','09:30','11:00'),
('CA3','13:30','15:00'),
('CA4','15:30','17:00'),
('CA5','17:30','19:00'),
('CA6','19:30','21:00')
go
--NGAYHOC
INSERT INTO NGAYHOC(NGAYHOC, NGHILE)
VALUES('01/11/2017',0),
('02/11/2017',0),
('03/11/2017',0),
('04/11/2017',0),
('05/11/2017',0),
('06/11/2017',0),
('07/11/2017',0),
('08/11/2017',0),
('09/11/2017',0),
('10/11/2017',0),
('11/11/2017',0),
('12/11/2017',0),
('13/11/2017',0),
('14/11/2017',0),
('15/11/2017',0),
('16/11/2017',0),
('17/11/2017',0),
('18/11/2017',0),
('19/11/2017',0),
('20/11/2017',0),
('21/11/2017',0),
('22/11/2017',0),
('23/11/2017',0),
('24/11/2017',0),
('25/11/2017',0),
('26/11/2017',0),
('27/11/2017',0),
('28/11/2017',0),
('29/11/2017',0),
('30/11/2017',0),
('01/12/2017',0),
('02/12/2017',0),
('03/12/2017',0),
('04/12/2017',0),
('05/12/2017',0),
('06/12/2017',0),
('07/12/2017',0),
('08/12/2017',0),
('09/12/2017',0),
('10/12/2017',0),
('11/12/2017',0),
('12/12/2017',0),
('13/12/2017',0),
('14/12/2017',0),
('15/12/2017',0),
('16/12/2017',0),
('17/12/2017',0),
('18/12/2017',0),
('19/12/2017',0),
('20/12/2017',0),
('21/12/2017',0),
('22/12/2017',0),
('23/12/2017',0),
('24/12/2017',0),
('25/12/2017',0),
('26/12/2017',0),
('27/12/2017',0),
('28/12/2017',0),
('29/12/2017',0),
('30/12/2017',0),
('31/12/2017',0)


go
--NHOMNGUOIDUNG
INSERT INTO NHOMNGUOIDUNG(MANHOMNGUOIDUNG, TENNHOMNGUOIDUNG)
VALUES('NND001',N'STUDENT'),
('NND002',N'TEACHER'),
('NND003',N'STAFF'),
('XMASTR',N'MASTER')

go
--NGUOIDUNG_NHOMNGUOIDUNG
INSERT INTO NGUOIDUNG_NHOMNGUOIDUNG(MATAIKHOAN,MANHOMNGUOIDUNG)
VALUES('HV0001','NND001'),
('HV0002','NND001'),
('HV0003','NND001'),
('HV0004','NND001'),
('HV0005','NND001'),
('HV0006','NND001'),
('HV0007','NND001'),
('HV0008','NND001'),
('HV0009','NND001'),
('HV0010','NND001'),
('HV0011','NND001'),
('HV0012','NND001'),
('HV0013','NND001'),
('HV0014','NND001'),
('HV0015','NND001'),
('HV0016','NND001'),
('HV0017','NND001'),
('HV0018','NND001'),
('HV0019','NND001'),
('HV0020','NND001'),
('HV0021','NND001'),
('HV0022','NND001'),
('HV0023','NND001'),
('HV0024','NND001'),
('GV0001','NND002'),
('GV0002','NND002'),
('GV0003','NND002'),
('GV0004','NND002'),
('GV0005','NND002'),
('GV0006','NND002'),
('GV0007','NND002'),
('GV0008','NND003'),
('NV0001','NND003'),
('NV0002','NND003'),
('NV0003','NND003'),
('NV0004','NND003'),
('NV0005','NND003'),
('NV0006','NND003'),
('NV0007','NND003'),
('NV0008','NND003'),
('NV0009','NND003')


go
--GIAODIEN
INSERT INTO GIAODIEN
VALUES('MH0000',N'QUẢN LÝ TÀI KHOẢN'), --nhân viên'
('MH0001',N'QUẢN LÝ LỚP HỌC'), --nhân viên
('MH0002',N'QUẢN LÝ PHÂN QUYỀN'), --nhân viên
('MH0003',N'QUẢN LÝ BIÊN LAI'), --nhân viên
('MH0004',N'QUẢN LÝ LỊCH'),--nhân viên
('MH0005',N'XEM LỊCH DẠY'), --giảng viên
('MH0006',N'XEM LỊCH HỌC'),--học viên
('MH0007',N'QUẢN LÝ ĐIỂM'),--nhân viên
('MH0008',N'QUẢN LÝ THỐNG KÊ'),--nhân viên
('MH0009',N'QUẢN LÝ HỆ THỐNG'),--nhân viên
('MH0010',N'QUẢN LÝ NHÂN VIÊN'),--nhân viên
('MH0011',N'QUẢN LÝ GIẢNG VIÊN'),--nhân viên
('MH0012',N'QUẢN LÝ HỌC VIÊN'),--nhân viên
('MH0013',N'XEM KẾT QUẢ HỌC TẬP'),--học viên
('MH0014',N'QUẢN LÝ ĐIỂM DANH'),--giảng viên
('MH0015',N'THÔNG TIN CÁ NHÂN NHÂN VIÊN'),--nhân viên
('MH0016',N'THÔNG TIN CÁ NHÂN GIẢNG VIÊN'),--giảng viên
('MH0017',N'THÔNG TIN CÁ NHÂN HỌC VIÊN')--học viên


go
--THAOTAC
INSERT INTO THAOTAC
VALUES('TT0001',N'VIEW'),
('TT0002',N'SELECT'),
('TT0003',N'INSERT'),
('TT0004',N'UPDATE'),
('TT0005',N'DELETE'),
('TT0006',N'MASTER')



go
--GIAODIEN_NHOMNGUOIDUNG
INSERT INTO NHOMNGUOIDUNG_GIAODIEN(MAGIAODIEN,MANHOMNGUOIDUNG)
VALUES ('MH0006','NND001'), --học viên
('MH0013','NND001'),
('MH0017','NND001'),
('MH0005','NND002'), --giảng viên
('MH0014','NND002'),
('MH0016','NND002'),
('MH0000','NND003'), --nhân viên
('MH0001','NND003'),
('MH0002','NND003'),
('MH0003','NND003'),
('MH0004','NND003'),
('MH0007','NND003'),
('MH0008','NND003'),
('MH0009','NND003'),
('MH0010','NND003'),
('MH0011','NND003'),
('MH0012','NND003'),
('MH0015','NND003')



go
--LOAIBIENLAI
INSERT INTO LOAI_BIENLAI
VALUES('L01',N'ĐÓNG MỘT LẦN'),
('L02',N'ĐÓNG THEO TUẦN'),
('L03',N'ĐÓNG THEO THÁNG')

go
--LOAIDIEM
INSERT INTO LOAIDIEM
VALUES('LD1',N'ĐIỂM QUÁ TRÌNH'),
('LD2',N'ĐIỂM ĐẦU RA')


go
--THOIGIANHOC
INSERT INTO THOIGIANHOC
VALUES('CA1','01/11/2017'),
('CA2','01/11/2017'),
('CA3','01/11/2017'),
('CA4','01/11/2017'),
('CA5','01/11/2017'),
('CA6','01/11/2017'),
('CA1','02/11/2017'),
('CA2','02/11/2017'),
('CA3','02/11/2017'),
('CA4','02/11/2017'),
('CA5','02/11/2017'),
('CA6','02/11/2017'),
('CA1','03/11/2017'),
('CA2','03/11/2017'),
('CA3','03/11/2017'),
('CA4','03/11/2017'),
('CA5','03/11/2017'),
('CA6','03/11/2017'),
('CA1','04/11/2017'),
('CA2','04/11/2017'),
('CA3','04/11/2017'),
('CA4','04/11/2017'),
('CA5','04/11/2017'),
('CA6','04/11/2017'),
('CA1','05/11/2017'),
('CA2','05/11/2017'),
('CA3','05/11/2017'),
('CA4','05/11/2017'),
('CA5','05/11/2017'),
('CA6','05/11/2017'),
('CA1','06/11/2017'),
('CA2','06/11/2017'),
('CA3','06/11/2017'),
('CA4','06/11/2017'),
('CA5','06/11/2017'),
('CA6','06/11/2017'),
('CA1','07/11/2017'),
('CA2','07/11/2017'),
('CA3','07/11/2017'),
('CA4','07/11/2017'),
('CA5','07/11/2017'),
('CA6','07/11/2017'),
('CA1','08/11/2017'),
('CA2','08/11/2017'),
('CA3','08/11/2017'),
('CA4','08/11/2017'),
('CA5','08/11/2017'),
('CA6','08/11/2017'),
('CA1','09/11/2017'),
('CA2','09/11/2017'),
('CA3','09/11/2017'),
('CA4','09/11/2017'),
('CA5','09/11/2017'),
('CA6','09/11/2017'),
('CA1','10/11/2017'),
('CA2','10/11/2017'),
('CA3','10/11/2017'),
('CA4','10/11/2017'),
('CA5','10/11/2017'),
('CA6','10/11/2017'),
('CA1','11/11/2017'),
('CA2','11/11/2017'),
('CA3','11/11/2017'),
('CA4','11/11/2017'),
('CA5','11/11/2017'),
('CA6','11/11/2017'),
('CA1','12/11/2017'),
('CA2','12/11/2017'),
('CA3','12/11/2017'),
('CA4','12/11/2017'),
('CA5','12/11/2017'),
('CA6','12/11/2017'),
('CA1','13/11/2017'),
('CA2','13/11/2017'),
('CA3','13/11/2017'),
('CA4','13/11/2017'),
('CA5','13/11/2017'),
('CA6','13/11/2017'),
('CA1','14/11/2017'),
('CA2','14/11/2017'),
('CA3','14/11/2017'),
('CA4','14/11/2017'),
('CA5','14/11/2017'),
('CA6','14/11/2017'),
('CA1','15/11/2017'),
('CA2','15/11/2017'),
('CA3','15/11/2017'),
('CA4','15/11/2017'),
('CA5','15/11/2017'),
('CA6','15/11/2017'),
('CA1','16/11/2017'),
('CA2','16/11/2017'),
('CA3','16/11/2017'),
('CA4','16/11/2017'),
('CA5','16/11/2017'),
('CA6','16/11/2017'),
('CA1','17/11/2017'),
('CA2','17/11/2017'),
('CA3','17/11/2017'),
('CA4','17/11/2017'),
('CA5','17/11/2017'),
('CA6','17/11/2017'),
('CA1','18/11/2017'),
('CA2','18/11/2017'),
('CA3','18/11/2017'),
('CA4','18/11/2017'),
('CA5','18/11/2017'),
('CA6','18/11/2017'),
('CA1','19/11/2017'),
('CA2','19/11/2017'),
('CA3','19/11/2017'),
('CA4','19/11/2017'),
('CA5','19/11/2017'),
('CA6','19/11/2017'),
('CA1','20/11/2017'),
('CA2','20/11/2017'),
('CA3','20/11/2017'),
('CA4','20/11/2017'),
('CA5','20/11/2017'),
('CA6','20/11/2017'),
('CA1','21/11/2017'),
('CA2','21/11/2017'),
('CA3','21/11/2017'),
('CA4','21/11/2017'),
('CA5','21/11/2017'),
('CA6','21/11/2017'),
('CA1','22/11/2017'),
('CA2','22/11/2017'),
('CA3','22/11/2017'),
('CA4','22/11/2017'),
('CA5','22/11/2017'),
('CA6','22/11/2017'),
('CA1','23/11/2017'),
('CA2','23/11/2017'),
('CA3','23/11/2017'),
('CA4','23/11/2017'),
('CA5','23/11/2017'),
('CA6','23/11/2017'),
('CA1','24/11/2017'),
('CA2','24/11/2017'),
('CA3','24/11/2017'),
('CA4','24/11/2017'),
('CA5','24/11/2017'),
('CA6','24/11/2017'),
('CA1','25/11/2017'),
('CA2','25/11/2017'),
('CA3','25/11/2017'),
('CA4','25/11/2017'),
('CA5','25/11/2017'),
('CA6','25/11/2017'),
('CA1','26/11/2017'),
('CA2','26/11/2017'),
('CA3','26/11/2017'),
('CA4','26/11/2017'),
('CA5','26/11/2017'),
('CA6','26/11/2017'),
('CA1','27/11/2017'),
('CA2','27/11/2017'),
('CA3','27/11/2017'),
('CA4','27/11/2017'),
('CA5','27/11/2017'),
('CA6','27/11/2017'),
('CA1','28/11/2017'),
('CA2','28/11/2017'),
('CA3','28/11/2017'),
('CA4','28/11/2017'),
('CA5','28/11/2017'),
('CA6','28/11/2017'),
('CA1','29/11/2017'),
('CA2','29/11/2017'),
('CA3','29/11/2017'),
('CA4','29/11/2017'),
('CA5','29/11/2017'),
('CA6','29/11/2017'),
('CA1','30/11/2017'),
('CA2','30/11/2017'),
('CA3','30/11/2017'),
('CA4','30/11/2017'),
('CA5','30/11/2017'),
('CA6','30/11/2017'),
('CA1','01/12/2017'),
('CA2','01/12/2017'),
('CA3','01/12/2017'),
('CA4','01/12/2017'),
('CA5','01/12/2017'),
('CA6','01/12/2017'),
('CA1','02/12/2017'),
('CA2','02/12/2017'),
('CA3','02/12/2017'),
('CA4','02/12/2017'),
('CA5','02/12/2017'),
('CA6','02/12/2017'),
('CA1','03/12/2017'),
('CA2','03/12/2017'),
('CA3','03/12/2017'),
('CA4','03/12/2017'),
('CA5','03/12/2017'),
('CA6','03/12/2017'),
('CA1','04/12/2017'),
('CA2','04/12/2017'),
('CA3','04/12/2017'),
('CA4','04/12/2017'),
('CA5','04/12/2017'),
('CA6','04/12/2017'),
('CA1','05/12/2017'),
('CA2','05/12/2017'),
('CA3','05/12/2017'),
('CA4','05/12/2017'),
('CA5','05/12/2017'),
('CA6','05/12/2017'),
('CA1','06/12/2017'),
('CA2','06/12/2017'),
('CA3','06/12/2017'),
('CA4','06/12/2017'),
('CA5','06/12/2017'),
('CA6','06/12/2017'),
('CA1','07/12/2017'),
('CA2','07/12/2017'),
('CA3','07/12/2017'),
('CA4','07/12/2017'),
('CA5','07/12/2017'),
('CA6','07/12/2017'),
('CA1','08/12/2017'),
('CA2','08/12/2017'),
('CA3','08/12/2017'),
('CA4','08/12/2017'),
('CA5','08/12/2017'),
('CA6','08/12/2017'),
('CA1','09/12/2017'),
('CA2','09/12/2017'),
('CA3','09/12/2017'),
('CA4','09/12/2017'),
('CA5','09/12/2017'),
('CA6','09/12/2017'),
('CA1','10/12/2017'),
('CA2','10/12/2017'),
('CA3','10/12/2017'),
('CA4','10/12/2017'),
('CA5','10/12/2017'),
('CA6','10/12/2017'),
('CA1','11/12/2017'),
('CA2','11/12/2017'),
('CA3','11/12/2017'),
('CA4','11/12/2017'),
('CA5','11/12/2017'),
('CA6','11/12/2017'),
('CA1','12/12/2017'),
('CA2','12/12/2017'),
('CA3','12/12/2017'),
('CA4','12/12/2017'),
('CA5','12/12/2017'),
('CA6','12/12/2017'),
('CA1','13/12/2017'),
('CA2','13/12/2017'),
('CA3','13/12/2017'),
('CA4','13/12/2017'),
('CA5','13/12/2017'),
('CA6','13/12/2017'),
('CA1','14/12/2017'),
('CA2','14/12/2017'),
('CA3','14/12/2017'),
('CA4','14/12/2017'),
('CA5','14/12/2017'),
('CA6','14/12/2017'),
('CA1','15/12/2017'),
('CA2','15/12/2017'),
('CA3','15/12/2017'),
('CA4','15/12/2017'),
('CA5','15/12/2017'),
('CA6','15/12/2017'),
('CA1','16/12/2017'),
('CA2','16/12/2017'),
('CA3','16/12/2017'),
('CA4','16/12/2017'),
('CA5','16/12/2017'),
('CA6','16/12/2017'),
('CA1','17/12/2017'),
('CA2','17/12/2017'),
('CA3','17/12/2017'),
('CA4','17/12/2017'),
('CA5','17/12/2017'),
('CA6','17/12/2017'),
('CA1','18/12/2017'),
('CA2','18/12/2017'),
('CA3','18/12/2017'),
('CA4','18/12/2017'),
('CA5','18/12/2017'),
('CA6','18/12/2017'),
('CA1','19/12/2017'),
('CA2','19/12/2017'),
('CA3','19/12/2017'),
('CA4','19/12/2017'),
('CA5','19/12/2017'),
('CA6','19/12/2017'),
('CA1','20/12/2017'),
('CA2','20/12/2017'),
('CA3','20/12/2017'),
('CA4','20/12/2017'),
('CA5','20/12/2017'),
('CA6','20/12/2017'),
('CA1','21/12/2017'),
('CA2','21/12/2017'),
('CA3','21/12/2017'),
('CA4','21/12/2017'),
('CA5','21/12/2017'),
('CA6','21/12/2017'),
('CA1','22/12/2017'),
('CA2','22/12/2017'),
('CA3','22/12/2017'),
('CA4','22/12/2017'),
('CA5','22/12/2017'),
('CA6','22/12/2017'),
('CA1','23/12/2017'),
('CA2','23/12/2017'),
('CA3','23/12/2017'),
('CA4','23/12/2017'),
('CA5','23/12/2017'),
('CA6','23/12/2017'),
('CA1','24/12/2017'),
('CA2','24/12/2017'),
('CA3','24/12/2017'),
('CA4','24/12/2017'),
('CA5','24/12/2017'),
('CA6','24/12/2017'),
('CA1','25/12/2017'),
('CA2','25/12/2017'),
('CA3','25/12/2017'),
('CA4','25/12/2017'),
('CA5','25/12/2017'),
('CA6','25/12/2017'),
('CA1','26/12/2017'),
('CA2','26/12/2017'),
('CA3','26/12/2017'),
('CA4','26/12/2017'),
('CA5','26/12/2017'),
('CA6','26/12/2017'),
('CA1','27/12/2017'),
('CA2','27/12/2017'),
('CA3','27/12/2017'),
('CA4','27/12/2017'),
('CA5','27/12/2017'),
('CA6','27/12/2017'),
('CA1','28/12/2017'),
('CA2','28/12/2017'),
('CA3','28/12/2017'),
('CA4','28/12/2017'),
('CA5','28/12/2017'),
('CA6','28/12/2017'),
('CA1','29/12/2017'),
('CA2','29/12/2017'),
('CA3','29/12/2017'),
('CA4','29/12/2017'),
('CA5','29/12/2017'),
('CA6','29/12/2017'),
('CA1','30/12/2017'),
('CA2','30/12/2017'),
('CA3','30/12/2017'),
('CA4','30/12/2017'),
('CA5','30/12/2017'),
('CA6','30/12/2017'),
('CA1','31/12/2017'),
('CA2','31/12/2017'),
('CA3','31/12/2017'),
('CA4','31/12/2017'),
('CA5','31/12/2017'),
('CA6','31/12/2017')


go
--LICHHOC
INSERT INTO LICHHOC(MALICHHOC,MACA,NGAYHOC,MACOSO,MAPHONG,MALOPHOC)
VALUES ('LICH01','CA1','01/11/2017','CS001','PH001','L0010001'),
('LICH02','CA1','03/11/2017','CS001','PH001','L0010001'),
('LICH03','CA1','06/11/2017','CS001','PH001','L0010001'),
('LICH04','CA1','08/11/2017','CS001','PH001','L0010001'),
('LICH05','CA1','10/11/2017','CS001','PH001','L0010001'),
('LICH06','CA1','13/11/2017','CS001','PH001','L0010001'),
('LICH07','CA1','15/11/2017','CS001','PH001','L0010001'),
('LICH08','CA1','17/11/2017','CS001','PH001','L0010001'),
('LICH09','CA1','20/11/2017','CS001','PH001','L0010001'),
('LICH10','CA1','22/11/2017','CS001','PH001','L0010001'),
('LICH11','CA1','24/11/2017','CS001','PH001','L0010001'),
('LICH12','CA1','27/11/2017','CS001','PH001','L0010001'),
('LICH13','CA1','29/11/2017','CS001','PH001','L0010001'),
('LICH14','CA1','01/12/2017','CS001','PH001','L0010001'),
('LICH15','CA1','04/12/2017','CS001','PH001','L0010001'),
('LICH16','CA1','06/12/2017','CS001','PH001','L0010001'),
('LICH17','CA1','08/12/2017','CS001','PH001','L0010001'),
('LICH18','CA1','11/12/2017','CS001','PH001','L0010001'),
('LICH19','CA1','13/12/2017','CS001','PH001','L0010001'),
('LICH20','CA1','15/12/2017','CS001','PH001','L0010001'),
('LICH21','CA1','18/12/2017','CS001','PH001','L0010001'),
('LICH22','CA1','20/12/2017','CS001','PH001','L0010001'),
('LICH23','CA1','22/12/2017','CS001','PH001','L0010001'),
('LICH24','CA1','25/12/2017','CS001','PH001','L0010001'),

('LICH01','CA2','01/11/2017','CS002','PH001','L0050001'),
('LICH02','CA2','03/11/2017','CS002','PH001','L0050001'),
('LICH03','CA2','06/11/2017','CS002','PH001','L0050001'),
('LICH04','CA2','08/11/2017','CS002','PH001','L0050001'),
('LICH05','CA2','10/11/2017','CS002','PH001','L0050001'),
('LICH06','CA2','13/11/2017','CS002','PH001','L0050001'),
('LICH07','CA2','15/11/2017','CS002','PH001','L0050001'),
('LICH08','CA2','17/11/2017','CS002','PH001','L0050001'),
('LICH09','CA2','20/11/2017','CS002','PH001','L0050001'),
('LICH10','CA2','22/11/2017','CS002','PH001','L0050001'),
('LICH11','CA2','24/11/2017','CS002','PH001','L0050001'),
('LICH12','CA2','27/11/2017','CS002','PH001','L0050001'),
('LICH13','CA2','29/11/2017','CS002','PH001','L0050001'),
('LICH14','CA2','01/12/2017','CS002','PH001','L0050001'),
('LICH15','CA2','04/12/2017','CS002','PH001','L0050001'),
('LICH16','CA2','06/12/2017','CS002','PH001','L0050001'),
('LICH17','CA2','08/12/2017','CS002','PH001','L0050001'),
('LICH18','CA2','11/12/2017','CS002','PH001','L0050001'),
('LICH19','CA2','13/12/2017','CS002','PH001','L0050001'),
('LICH20','CA2','15/12/2017','CS002','PH001','L0050001'),
('LICH21','CA2','18/12/2017','CS002','PH001','L0050001'),
('LICH22','CA2','20/12/2017','CS002','PH001','L0050001'),
('LICH23','CA2','22/12/2017','CS002','PH001','L0050001'),
('LICH24','CA2','25/12/2017','CS002','PH001','L0050001'),

('LICH01','CA3','01/11/2017','CS003','PH001','L0110001'),
('LICH02','CA3','03/11/2017','CS003','PH001','L0110001'),
('LICH03','CA3','06/11/2017','CS003','PH001','L0110001'),
('LICH04','CA3','08/11/2017','CS003','PH001','L0110001'),
('LICH05','CA3','10/11/2017','CS003','PH001','L0110001'),
('LICH06','CA3','13/11/2017','CS003','PH001','L0110001'),
('LICH07','CA3','15/11/2017','CS003','PH001','L0110001'),
('LICH08','CA3','17/11/2017','CS003','PH001','L0110001'),
('LICH09','CA3','20/11/2017','CS003','PH001','L0110001'),
('LICH10','CA3','22/11/2017','CS003','PH001','L0110001'),
('LICH11','CA3','24/11/2017','CS003','PH001','L0110001'),
('LICH12','CA3','27/11/2017','CS003','PH001','L0110001'),
('LICH13','CA3','29/11/2017','CS003','PH001','L0110001'),
('LICH14','CA3','01/12/2017','CS003','PH001','L0110001'),
('LICH15','CA3','04/12/2017','CS003','PH001','L0110001'),
('LICH16','CA3','06/12/2017','CS003','PH001','L0110001'),
('LICH17','CA3','08/12/2017','CS003','PH001','L0110001'),
('LICH18','CA3','11/12/2017','CS003','PH001','L0110001'),
('LICH19','CA3','13/12/2017','CS003','PH001','L0110001'),
('LICH20','CA3','15/12/2017','CS003','PH001','L0110001'),
('LICH21','CA3','18/12/2017','CS003','PH001','L0110001'),
('LICH22','CA3','20/12/2017','CS003','PH001','L0110001'),
('LICH23','CA3','22/12/2017','CS003','PH001','L0110001'),
('LICH24','CA3','25/12/2017','CS003','PH001','L0110001')



go
insert into TT_DANGKYKHOAHOC VALUES
('DK000100011','HV0001','NV0001','KH0001',1,600,'29/10/2017'),
('DK000100021','HV0002','NV0001','KH0001',1,500,'30/10/2017'),
('DK000100031','HV0003','NV0002','KH0001',1,550,'28/10/2017'),
('DK000100041','HV0004','NV0002','KH0001',1,450,'27/10/2017'),
('DK000100051','HV0005','NV0003','KH0001',1,300,'29/10/2017'),
('DK000100061','HV0006','NV0004','KH0001',1,600,'29/10/2017'),
('DK000100071','HV0007','NV0001','KH0001',1,500,'30/10/2017'),
('DK000100081','HV0008','NV0002','KH0001',1,550,'28/10/2017'),
('DK000100091','HV0009','NV0002','KH0001',1,450,'27/10/2017'),
('DK000100101','HV0010','NV0001','KH0001',1,300,'29/10/2017'),

('DK000500011','HV0001','NV0001','KH0005',1,600,'28/10/2017'),
('DK000500021','HV0002','NV0001','KH0005',1,500,'26/10/2017'),
('DK000500031','HV0003','NV0002','KH0005',1,550,'25/10/2017'),
('DK000500041','HV0004','NV0002','KH0005',1,450,'22/10/2017'),
('DK000500051','HV0005','NV0001','KH0005',1,300,'28/10/2017'),

('DK001100011','HV0001','NV0001','KH0011',1,600,'29/10/2017'),
('DK001100021','HV0002','NV0001','KH0011',1,500,'30/10/2017'),
('DK001100031','HV0003','NV0002','KH0011',1,550,'28/10/2017'),
('DK001100041','HV0004','NV0002','KH0011',1,450,'27/10/2017'),
('DK001100051','HV0005','NV0001','KH0011',1,300,'29/10/2017'),
('DK001100061','HV0006','NV0001','KH0011',1,600,'28/10/2017'),
('DK001100071','HV0007','NV0001','KH0011',1,500,'26/10/2017'),
('DK001100081','HV0008','NV0002','KH0011',1,550,'25/10/2017'),
('DK001100091','HV0009','NV0002','KH0011',1,450,'22/10/2017'),
('DK001100101','HV0010','NV0001','KH0011',1,300,'28/10/2017')

GO
--TT_GHIDANH
INSERT INTO TT_GHIDANH
VALUES('DK000100051','L0010001'),
('DK000100061','L0010001'),
('DK000100071','L0010001'),
('DK000100081','L0010001'),

('DK000500011','L0050001'),
('DK000500021','L0050001'),
('DK000500031','L0050001'),
('DK000500041','L0050001'),

('DK001100091','L0110001'),
('DK001100101','L0110001')

go
--CT_BUOIHOC
INSERT INTO CT_BUOIHOC
VALUES('LICH01','DK000100051','L0010001',0),
('LICH02','DK000100051','L0010001',0),
('LICH03','DK000100051','L0010001',0),
('LICH04','DK000100051','L0010001',0),
('LICH05','DK000100051','L0010001',0),
('LICH06','DK000100051','L0010001',0),
('LICH07','DK000100051','L0010001',0),
('LICH08','DK000100051','L0010001',0),
('LICH09','DK000100051','L0010001',0),
('LICH10','DK000100051','L0010001',0),
('LICH11','DK000100051','L0010001',0),
('LICH12','DK000100051','L0010001',0),
('LICH13','DK000100051','L0010001',0),
('LICH14','DK000100051','L0010001',0),
('LICH15','DK000100051','L0010001',0),
('LICH16','DK000100051','L0010001',0),
('LICH17','DK000100051','L0010001',0),
('LICH18','DK000100051','L0010001',0),
('LICH19','DK000100051','L0010001',0),
('LICH20','DK000100051','L0010001',0),
('LICH21','DK000100051','L0010001',0),
('LICH22','DK000100051','L0010001',0),
('LICH23','DK000100051','L0010001',0),

('LICH01','DK000100061','L0010001',0),
('LICH02','DK000100061','L0010001',0),
('LICH03','DK000100061','L0010001',0),
('LICH04','DK000100061','L0010001',0),
('LICH05','DK000100061','L0010001',0),
('LICH06','DK000100061','L0010001',0),
('LICH07','DK000100061','L0010001',0),
('LICH08','DK000100061','L0010001',0),
('LICH09','DK000100061','L0010001',0),
('LICH10','DK000100061','L0010001',0),
('LICH11','DK000100061','L0010001',0),
('LICH12','DK000100061','L0010001',0),
('LICH13','DK000100061','L0010001',0),
('LICH14','DK000100061','L0010001',0),
('LICH15','DK000100061','L0010001',0),
('LICH16','DK000100061','L0010001',0),
('LICH17','DK000100061','L0010001',0),
('LICH18','DK000100061','L0010001',0),
('LICH19','DK000100061','L0010001',0),
('LICH20','DK000100061','L0010001',0),
('LICH21','DK000100061','L0010001',0),
('LICH22','DK000100061','L0010001',0),
('LICH23','DK000100061','L0010001',0),
('LICH24','DK000100061','L0010001',0),

('LICH01','DK000100071','L0010001',0),
('LICH02','DK000100071','L0010001',0),
('LICH03','DK000100071','L0010001',0),
('LICH04','DK000100071','L0010001',0),
('LICH05','DK000100071','L0010001',0),
('LICH06','DK000100071','L0010001',0),
('LICH07','DK000100071','L0010001',0),
('LICH08','DK000100071','L0010001',0),
('LICH09','DK000100071','L0010001',0),
('LICH10','DK000100071','L0010001',0),
('LICH11','DK000100071','L0010001',0),
('LICH12','DK000100071','L0010001',0),
('LICH13','DK000100071','L0010001',0),
('LICH14','DK000100071','L0010001',0),
('LICH15','DK000100071','L0010001',0),
('LICH16','DK000100071','L0010001',0),
('LICH17','DK000100071','L0010001',0),
('LICH18','DK000100071','L0010001',0),
('LICH19','DK000100071','L0010001',0),
('LICH20','DK000100071','L0010001',0),
('LICH21','DK000100071','L0010001',0),
('LICH22','DK000100071','L0010001',0),
('LICH23','DK000100071','L0010001',0),
('LICH24','DK000100071','L0010001',0),

('LICH01','DK000100081','L0010001',0),
('LICH02','DK000100081','L0010001',0),
('LICH03','DK000100081','L0010001',0),
('LICH04','DK000100081','L0010001',0),
('LICH05','DK000100081','L0010001',0),
('LICH06','DK000100081','L0010001',0),
('LICH07','DK000100081','L0010001',0),
('LICH08','DK000100081','L0010001',0),
('LICH09','DK000100081','L0010001',0),
('LICH10','DK000100081','L0010001',0),
('LICH11','DK000100081','L0010001',0),
('LICH12','DK000100081','L0010001',0),
('LICH13','DK000100081','L0010001',0),
('LICH14','DK000100081','L0010001',0),
('LICH15','DK000100081','L0010001',0),
('LICH16','DK000100081','L0010001',0),
('LICH17','DK000100081','L0010001',0),
('LICH18','DK000100081','L0010001',0),
('LICH19','DK000100081','L0010001',0),
('LICH20','DK000100081','L0010001',0),
('LICH21','DK000100081','L0010001',0),
('LICH22','DK000100081','L0010001',0),
('LICH23','DK000100081','L0010001',0),
('LICH24','DK000100081','L0010001',0)


go
--DIEM
INSERT INTO DIEM(MADIEM, MADANGKYKHOAHOC, MALOPHOC, MALOAIDIEM, LAN, DIEM)
VALUES
('D00001','DK000100051','L0010001','LD1',1,300),
('D00002','DK000100051','L0010001','LD2',1,200),

('D00001','DK000100061','L0010001','LD1',1,320),
('D00002','DK000100061','L0010001','LD2',1,500),

('D00001','DK000100071','L0010001','LD1',1,350),
('D00002','DK000100071','L0010001','LD2',1,300),

('D00001','DK000100081','L0010001','LD1',1,200),
('D00002','DK000100081','L0010001','LD2',1,550),

('D00001','DK001100091','L0110001','LD1',1,3.0),
('D00002','DK001100091','L0110001','LD2',1,150),

('D00001','DK001100101','L0110001','LD1',1,2.5),
('D00002','DK001100101','L0110001','LD2',1,200),

('D00001','DK000500011','L0050001','LD1',1,60),
('D00001','DK000500021','L0050001','LD1',1,70),
('D00001','DK000500031','L0050001','LD1',1,90),
('D00001','DK000500041','L0050001','LD1',1,50)



go
--BIENLAI
set dateformat dMy
go
INSERT INTO BIENLAI
VALUES('BL0000000001','L01','DK000100051','L0010001','NV0001','10/10/2017',2000000.00,1),
('BL0000000002','L01','DK000100061','L0010001','NV0001','11/10/2017',2000000.00,1),
('BL0000000003','L03','DK000100071','L0010001','NV0001','10/10/2017',1000000.00,1),
('BL0000000004','L03','DK000100071','L0010001','NV0001','10/11/2017',1000000.00,2),
('BL0000000005','L02','DK000100081','L0010001','NV0001','10/10/2017',250000.00,1),
('BL0000000006','L02','DK000100081','L0010001','NV0001','17/10/2017',250000.00,2),
('BL0000000007','L02','DK000100081','L0010001','NV0001','24/10/2017',250000.00,3),
('BL0000000008','L02','DK000100081','L0010001','NV0001','31/10/2017',250000.00,4),
('BL0000000009','L02','DK000100081','L0010001','NV0001','07/10/2017',250000.00,5),
('BL0000000010','L02','DK000100081','L0010001','NV0001','14/10/2017',250000.00,6),
('BL0000000011','L02','DK000100081','L0010001','NV0001','21/10/2017',250000.00,7),
('BL0000000012','L02','DK000100081','L0010001','NV0001','28/10/2017',250000.00,8),

('BL0000000013','L01','DK000500011','L0050001','NV0001','10/10/2017',2500000.00,1),
('BL0000000014','L01','DK000500021','L0050001','NV0001','11/10/2017',2500000.00,1),
('BL0000000015','L01','DK000500031','L0050001','NV0001','10/10/2017',3000000.00,1),
('BL0000000016','L01','DK000500041','L0050001','NV0001','10/10/2017',3000000.00,1),

('BL0000000017','L01','DK001100091','L0110001','NV0001','10/10/2017',2000000.00,1),
('BL0000000018','L01','DK001100101','L0110001','NV0001','12/10/2017',2000000.00,1),
('BL0000000019','L02','DK000100081','L0010001','NV0001','5/11/2017',250000.00,9),
('BL0000000020','L02','DK000100081','L0010001','NV0001','12/11/2017',250000.00,10),
('BL0000000021','L02','DK000100081','L0010001','NV0001','19/11/2017',250000.00,11),
('BL0000000022','L02','DK000100081','L0010001','NV0001','26/11/2017',250000.00,12),
('BL0000000023','L02','DK000100081','L0010001','NV0001','02/12/2017',250000.00,13),
('BL0000000024','L02','DK000100081','L0010001','NV0001','09/12/2017',250000.00,14),
('BL0000000025','L02','DK000100081','L0010001','NV0001','16/12/2017',250000.00,15),
('BL0000000026','L02','DK000100081','L0010001','NV0001','24/10/2017',250000.00,3),
('BL0000000027','L02','DK000100081','L0010001','NV0001','01/11/2017',250000.00,4)

go
--THU
insert into THU values
('MON',N'THỨ 2','MONDAY',1),
('TUE',N'THỨ 3','TUESDAY',2),
('WED',N'THỨ 4','WEDNESDAY',3),
('THU',N'THỨ 5','THURSDAY',4),
('FRI',N'THỨ 6','FRIDAY',5),
('SAT',N'THỨ 7','SATURDAY',6),
('SUN',N'THỨ 8','SUNDAY',7)
