﻿---#0 Lấy họ người dùng
create function Get_Ho(@ho_ten nvarchar(50))
returns nvarchar(50)
as
	begin
		declare @s nvarchar(50)
		set @s = substring(@ho_ten,0,LEN(@ho_ten)+1 - charindex(' ',reverse(@ho_ten)))
		return @s
	end
go

create function Get_Ten(@ho_ten nvarchar(50))
returns nvarchar(50)
as
	begin
		declare @s nvarchar(50)
		set @s = reverse(substring(reverse(@ho_ten),0,charindex(' ',reverse(@ho_ten))))
		return @s
	end
go

--#24 Cập nhật mật khẩu mã hoá TripleDES
create proc CapNhatMatKhau(@user char(6),@pass nvarchar(30))
as
	update nguoidung
	set MATKHAU = @pass
	where MATAIKHOAN = @user
go


--#19 Cập nhật số lượng của học viên một lớp học
create proc CapNhatSoLuongHocVien
as
	declare @tb table(malophoc char(8), soluong int)
	insert into @tb 
		select LOPHOC.MALOPHOC, isnull(t.SOLUONG,0)
		from
			(select MALOPHOC, COUNT(*) as 'SOLUONG'
			from TT_GHIDANH
			group by MALOPHOC) t
		right join
			LOPHOC
		on t.MALOPHOC = LOPHOC.MALOPHOC

	declare @malophoc char(8)

	declare myCursor cursor
	for select MALOPHOC from LOPHOC
	open myCursor
	fetch next from myCursor into @malophoc
		while @@FETCH_STATUS = 0
		begin
			update LOPHOC
			set SOLUONG = (select t.soluong 
						from @tb t where t.malophoc = @malophoc)
			where MALOPHOC = @malophoc
			fetch next from myCursor into @malophoc
		end
	close myCursor
	deallocate myCursor
go	
--#24 Backup toàn bộ dữ liệu
go
create proc dbo.FullBackup(@link nvarchar(Max))
as
	begin
		backup database QUANLY_HOCVIEN
		to disk = @link
	end

--#25 Backup different
go
create proc dbo.DifferentBackup(@link nvarchar(max))
as
	begin
		backup database QUANLY_HOCVIEN
		to disk = @link
		with differential
	end
--#26 Backup Transaction log
go
create proc dbo.LogBackup(@link nvarchar(max))
as
	begin
		backup log QUANLY_HOCVIEN
		to disk = @link	
	end

	--#28 Restone Full or Differential
go
create proc dbo.RestoreDatabaseCompleteOrDatabaseDiff(@link nvarchar(max))
as
	begin
		restore database QuanLy_HocVien
		from disk = @link With NoRecovery	
	end
--#29 dbo.Restore Transaction log
go
create proc dbo.RestoreTransactionLog(@link nvarchar(max))
as
	begin
		restore log QuanLy_HocVien
		from disk = @link
	end
go
	
--#52 thêm biểu mẫu
create proc ThemBieuMau(@mabieumau char(6), @tenbieumau nvarchar(50), @taptin varbinary(MAX), @loai varchar(8))
as
	insert into BIEUMAU(MABIEUMAU,TENBIEUMAU,TAPTIN,LOAI)
	values (@mabieumau,@tenbieumau,@taptin,@loai)
	go
	---#17 thông tin người dùng và nhóm quyền theo trang
create function ThongTinNguoiDungVaNhomNguoiDung(@trang int null, @soluong int null)
returns @result table([MÃ TÀI KHOẢN] Char(6), [TÊN NGƯỜI DÙNG] nvarchar(30), [MÃ NHÓM] char(6), [HOẠT ĐỘNG] bit)
as
begin
	declare @table table(mataikhoan Char(6), manhom char(6), hoatdong bit)
	if isnull(@trang,-1) = -1 or isnull(@trang,-1) = -1
	begin
		insert into @table(mataikhoan,manhom,hoatdong) 
			select MATAIKHOAN, MANHOMNGUOIDUNG, HOATDONG
			from NGUOIDUNG_NHOMNGUOIDUNG
			ORDER BY MATAIKHOAN
	end
	else
	begin
		insert into @table(mataikhoan,manhom,hoatdong) 
			select * from NGUOIDUNG_NHOMNGUOIDUNG
			ORDER BY MATAIKHOAN
			OFFSET (@trang-1)*@soluong ROWS
			FETCH NEXT @soluong ROWS ONLY
	end

	declare @group table(group_id char(2))
	insert into @group
		select SUBSTRING(mataikhoan,1,2) from @table
		GROUP BY SUBSTRING(mataikhoan,1,2)

	--join
	if 'GV' in (select * from @group)
		insert into @result
			select GIANGVIEN.MATAIKHOAN, GIANGVIEN.TENGIANGVIEN, manhom, hoatdong 
			from @table tb
			left join GIANGVIEN ON tb.mataikhoan = GIANGVIEN.mataikhoan
			where isnull(GIANGVIEN.MATAIKHOAN,'e') <> 'e'
	if 'HV' in (select * from @group)
	insert into @result
		select HOCVIEN.MATAIKHOAN, HOCVIEN.TENHOCVIEN, manhom, hoatdong 
		from @table tb
		left join HOCVIEN ON tb.mataikhoan = HOCVIEN.mataikhoan
		where isnull(HOCVIEN.MATAIKHOAN,'e') <> 'e'
	if 'NV' in (select * from @group)
		insert into @result
		select NHANVIEN.MATAIKHOAN, NHANVIEN.TENNHANVIEN, manhom, hoatdong 
		from @table tb
		left join NHANVIEN ON tb.mataikhoan = NHANVIEN.mataikhoan
		where isnull(NHANVIEN.MATAIKHOAN,'e') <> 'e'
	return
	end
GO

---#8 Người dùng theo nhóm người dùng
create function dbo.HienThiNguoiDungTheoNhomNguoiDung(@manhom char(6))
returns table
as 
return
	select [MÃ TÀI KHOẢN], [TÊN NGƯỜI DÙNG]
	from ThongTinNguoiDungVaNhomNguoiDung(null,null)
	where [MÃ NHÓM] = @manhom
	group by [MÃ TÀI KHOẢN], [TÊN NGƯỜI DÙNG]
GO


---#9 Người dùng không nằm trong nhóm người dùng
create function dbo.HienThiNguoiDungKhongTrongNhomNguoiDung(@manhom char(6))
returns @result table([MÃ TÀI KHOẢN]  char(6), [TÊN NGƯỜI DÙNG] nvarchar(30))
as
begin
	declare @table table(mataikhoan char(6),tennguoidung nvarchar(30))
	insert into @table
	select [MÃ TÀI KHOẢN], [TÊN NGƯỜI DÙNG] from dbo.HienThiNguoiDungTheoNhomNguoiDung(@manhom)

	insert into @result
		select MATAIKHOAN,TENGIANGVIEN from GIANGVIEN
		union
		select MATAIKHOAN, TENHOCVIEN from HOCVIEN
		union
		select MATAIKHOAN, TENNHANVIEN from NHANVIEN
		except
		select tb.mataikhoan, tb.tennguoidung from @table tb
	return
end;

go
----#41 Lấy chi tiết buổi học của một lich học crua 1 lớp 
create proc HienThiCTBuoiHocLichHoc(@malichhoc char(6), @malophoc char(8), @trang int, @soluong int)
as
	if(@trang <> -1 and @soluong <> -1)
		begin
			declare @start int
			set @start = (@trang-1)*@soluong

			select h.MAHOCVIEN 'MÃ HỌC VIÊN', dbo.Get_Ho(h.TENHOCVIEN) 'HỌ HỌC VIÊN', dbo.Get_Ten(h.TENHOCVIEN) 'TÊN HỌC VIÊN', h.NGAYSINH 'NGÀY SINH', 
			tb3.MALOPHOC 'MÃ LỚP', k.TENKHOAHOC 'TÊN KHOÁ HỌC', ISNULL(tb3.DIEMDANH,0) 'ĐIỂM DANH'
			from
				(select tb2.MADANGKYKHOAHOC, tb2.MALICHHOC, tb2.MACA, tb2.NGAYHOC, tb2.MALOPHOC, c.DIEMDANH
				from 
					(select G.MADANGKYKHOAHOC, tb1.MALICHHOC, tb1.MACA, tb1.NGAYHOC, tb1.MALOPHOC
						from
					(select L.MALICHHOC, L.MACA, L.NGAYHOC, L.MALOPHOC
						from LICHHOC L
						where L.MALOPHOC = @malophoc
						and L.MALICHHOC = @malichhoc) tb1
					left join
						TT_GHIDANH G
					on tb1.MALOPHOC = G.MALOPHOC) tb2
				left join
				CT_BUOIHOC c
				on c.MALICHHOC = tb2.MALICHHOC and c.MADANGKYKHOAHOC = tb2.MADANGKYKHOAHOC and c.MALOPHOC = tb2.MALOPHOC) tb3,HOCVIEN h, TT_DANGKYKHOAHOC d, KHOAHOC k
			where h.MAHOCVIEN = d.MAHOCVIEN
			and d.MADANGKYKHOAHOC = tb3.MADANGKYKHOAHOC
			and k.MAKHOAHOC = d.MAKHOAHOC

			order by [TÊN HỌC VIÊN]
			offset @start rows
			fetch next @soluong rows only
	end
	else
	begin
					select h.MAHOCVIEN 'MÃ HỌC VIÊN', dbo.Get_Ho(h.TENHOCVIEN) 'HỌ HỌC VIÊN', dbo.Get_Ten(h.TENHOCVIEN) 'TÊN HỌC VIÊN', h.NGAYSINH 'NGÀY SINH', 
			tb3.MALOPHOC 'MÃ LỚP', k.TENKHOAHOC 'TÊN KHOÁ HỌC', ISNULL(tb3.DIEMDANH,0) 'ĐIỂM DANH'
			from
				(select tb2.MADANGKYKHOAHOC, tb2.MALICHHOC, tb2.MACA, tb2.NGAYHOC, tb2.MALOPHOC, c.DIEMDANH
				from 
					(select G.MADANGKYKHOAHOC, tb1.MALICHHOC, tb1.MACA, tb1.NGAYHOC, tb1.MALOPHOC
						from
					(select L.MALICHHOC, L.MACA, L.NGAYHOC, L.MALOPHOC
						from LICHHOC L
						where L.MALOPHOC = @malophoc
						and L.MALICHHOC = @malichhoc) tb1
					left join
						TT_GHIDANH G
					on tb1.MALOPHOC = G.MALOPHOC) tb2
				left join
				CT_BUOIHOC c
				on c.MALICHHOC = tb2.MALICHHOC and c.MADANGKYKHOAHOC = tb2.MADANGKYKHOAHOC and c.MALOPHOC = tb2.MALOPHOC) tb3,HOCVIEN h, TT_DANGKYKHOAHOC d, KHOAHOC k
			where h.MAHOCVIEN = d.MAHOCVIEN
			and d.MADANGKYKHOAHOC = tb3.MADANGKYKHOAHOC
			and k.MAKHOAHOC = d.MAKHOAHOC
	end
go



---#12 Lấy thông tin thao tác crua một nhóm người dùng
create proc dbo.HienThiThongTinThaoTacCuaNhomNguoiDung(@manhom char(6))
as
	begin
		declare @CHITIET_THAOTAC table
			(magiaodien char(6) not null primary key, 
			tengiaodien nvarchar(30),
			tt_view bit,
			tt_select bit,
			tt_insert bit,
			tt_update bit,
			tt_delete bit,
			tt_master bit)

		declare @id_giaodien char(6)

		declare myCursor cursor
		for
			select MAGIAODIEN
			from NHOMNGUOIDUNG_GIAODIEN
			WHERE	MANHOMNGUOIDUNG = @manhom
			GROUP BY MAGIAODIEN
		open myCursor
			fetch next from myCursor into @id_giaodien
			while @@FETCH_STATUS = 0
			begin		
				insert into @CHITIET_THAOTAC(magiaodien,tt_view,tt_select,tt_insert,tt_update,tt_delete,tt_master) 
					SELECT @id_giaodien,TT0001,TT0002,TT0003,TT0004,TT0005,TT0006
						from
							(
								select MATHAOTAC
								from NHOMNGUOIDUNG_GIAODIEN
								where MAGIAODIEN = @id_giaodien
								and MANHOMNGUOIDUNG = @manhom
							) d
							pivot
							(
								count(mathaotac)
								for mathaotac in (TT0001,TT0002,TT0003, TT0004,TT0005,TT0006)
							) pvt
				fetch next from myCursor into @id_giaodien
			end
		close myCursor
		deallocate myCursor

		select	GIAODIEN.MAGIAODIEN 'MÃ MÀN HÌNH', GIAODIEN.TENMANHINH 'TÊN MÀN HÌNH', 
				isnull(tt_view,0) 'VIEW', 
				isnull(tt_select,0) 'SELECT',
				isnull(tt_insert,0) 'INSERT',
				isnull(tt_update,0) 'UPDATE',
				isnull(tt_delete,0) 'DELETE', 
				isnull(tt_master,0) 'MASTER'
		from @CHITIET_THAOTAC tb
		right join
			GIAODIEN
				ON GIAODIEN.MAGIAODIEN = tb.magiaodien
	end;
go



---#43 Hiển thị tất cả user
create proc HienThiTatCaNguoiDung(@trang int, @soluong int)
as
	declare @table table(mataikhoan Char(6), hoatdong bit)

	insert into @table(mataikhoan,hoatdong) 
		select MATAIKHOAN,HOATDONG 
		from NGUOIDUNG
		ORDER BY MATAIKHOAN
		OFFSET (@trang-1)*@soluong ROWS
		FETCH NEXT @soluong ROWS ONLY

	declare @group table(group_id char(2))
	insert into @group
	select SUBSTRING(mataikhoan,1,2) from @table
	GROUP BY SUBSTRING(mataikhoan,1,2)

	declare @result table (mataikhoan char(6), tennguoidung nvarchar(30), hoatdong bit)

	--join
	if 'GV' in (select * from @group)
		insert into @result
			select GIANGVIEN.MATAIKHOAN, GIANGVIEN.TENGIANGVIEN, hoatdong 
			from @table tb
			left join GIANGVIEN ON tb.mataikhoan = GIANGVIEN.mataikhoan
			where isnull(GIANGVIEN.MATAIKHOAN,'e') <> 'e'
	if 'HV' in (select * from @group)
	insert into @result
		select HOCVIEN.MATAIKHOAN, HOCVIEN.TENHOCVIEN, hoatdong 
		from @table tb
		left join HOCVIEN ON tb.mataikhoan = HOCVIEN.mataikhoan
		where isnull(HOCVIEN.MATAIKHOAN,'e') <> 'e'
	if 'NV' in (select * from @group)
		insert into @result
		select NHANVIEN.MATAIKHOAN, NHANVIEN.TENNHANVIEN, hoatdong 
		from @table tb
		left join NHANVIEN ON tb.mataikhoan = NHANVIEN.mataikhoan
		where isnull(NHANVIEN.MATAIKHOAN,'e') <> 'e'

	select mataikhoan 'MÃ TÀI KHOẢN', dbo.Get_Ho(tennguoidung) 'HỌ NGƯỜI DÙNG', 
	dbo.Get_Ten(tennguoidung) 'TÊN NGƯỜI DÙNG', hoatdong 'HOẠT ĐỘNG'
	from @result

go



-----#46 Danh sách giờ học
create proc dbo.HienThiTTChonGioHocHocVien(@madangkykhoahoc nvarchar(20))
as
	begin
		declare @THUTRONHGTUAN table
			(maca char(3) not null primary key, 
			batdau time,
			ketthuc time,
			thu2 bit,
			thu3 bit,
			thu4 bit,
			thu5 bit,
			thu6 bit,
			thu7 bit,
			thu8 bit)

		declare @id_cahoc char(6)

		declare myCursor cursor
		for
			select macahoc
			from TT_CHONGIOHOC
			WHERE	MADANGKYKHOAHOC = @madangkykhoahoc
			GROUP BY macahoc

		open myCursor
			fetch next from myCursor into @id_cahoc
			while @@FETCH_STATUS = 0
			begin		
				insert into @THUTRONHGTUAN(maca,thu2,thu3,thu4,thu5,thu6,thu7,thu8) 

					SELECT @id_cahoc,MON,TUE,WED,THU,FRI,SAT,SUN
						from
							(
								select MATHU
								from TT_CHONGIOHOC
								where MACAHOC = @id_cahoc
									and MADANGKYKHOAHOC = @madangkykhoahoc
							)d
							pivot
							(
								count(MATHU)
								for MATHU in (MON,TUE,WED,THU,FRI,SAT,SUN)
							) pvt
				fetch next from myCursor into @id_cahoc
			end
		close myCursor
		deallocate myCursor

		select	CAHOC.MACA 'CA HỌC',
				CONVERT(varchar, CAHOC.GIOBATDAU, 108) 'BẮT ĐẦU',
				CONVERT(varchar, CAHOC.GIOKETTHUC, 108) 'KẾT THÚC',
				isnull(thu2,0) 'THỨ 2', 
				isnull(thu3,0) 'THỨ 3',
				isnull(thu4,0) 'THỨ 4',
				isnull(thu5,0) 'THỨ 5',
				isnull(thu6,0) 'THỨ 6', 
				isnull(thu7,0) 'THỨ 7',
				isnull(thu8,0) 'CHỦ NHẬT'
		from @THUTRONHGTUAN tb
		right join
			CAHOC
				ON CAHOC.MACA = tb.maca
	end;
go


---#7 Hiện thông tin người dùng và nhóm quyền theo trang
create function dbo.HienThiThongTinNguoiDungVaNhomNguoiDung(@trang int null, @soluong int null)
returns table
as
return
	select tb1.[MÃ TÀI KHOẢN], tb1.[TÊN NGƯỜI DÙNG], tb1.[MÃ NHÓM], tb2.TENNHOMNGUOIDUNG 'TÊN NHÓM', 
	tb1.[HOẠT ĐỘNG]
	from
		(select * from dbo.ThongTinNguoiDungVaNhomNguoiDung(@trang, @soluong)) tb1
		left join
		(select * from NHOMNGUOIDUNG) tb2 on tb1.[MÃ NHÓM] = tb2.MANHOMNGUOIDUNG
go