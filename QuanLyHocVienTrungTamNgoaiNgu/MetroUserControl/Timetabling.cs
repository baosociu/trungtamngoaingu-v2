﻿using MetroDataset;
using MetroDataset.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetroUserControl
{
    public class Timetabling
    {
        /*Input: -những ca rảnh trong 1 tuần
        */
        List<Lich> input;
        ThoiKhoaBieu tkb;
        String magiangvien;

        public Timetabling(List<Lich> input, String magiangvien)
        {
            this.input = new List<Lich>(input);
            this.magiangvien = magiangvien;
        }

        public ThoiKhoaBieu Tkb { get => tkb; set => tkb = value; }

        public void CreateSchedule()
        {
            if (input.Count == 0)
                return;

            //***********************1 TKB****************************
            List<LICHHOC> lichdaygiangvien = (new Select()).LayLichDayGiangVien(this.magiangvien);
            //24 buổi
            List<LichHoc> lh = new List<LichHoc>();
            DateTime s = DateTime.Now;
            while(lh.Count != 24)
            {
                //Lich l = input[i % input.Count];
                //while (l.Thu != (int)s.DayOfWeek)
                List<Lich> l = isInWeek((int)s.DayOfWeek);

                while (l == null || l.Count == 0)
                {

                    s = s.AddDays(1);
                    l = isInWeek((int)s.DayOfWeek);
                }

                foreach(Lich lich in l)
                    if(!collision(lichdaygiangvien, s, lich.Ca) && lh.Count!=24)
                        lh.Add(new LichHoc(lich.Thu, lich.Ca, s));
                // s = s.AddDays(1);
                s = s.AddDays(1);
            }
            //ds phòng học
            List<PhongHoc> phongs = (new Select()).LayPhongHocChoLichHoc(lh);

            tkb = new ThoiKhoaBieu(lh, phongs);
            //********************************************************
        }

        private bool collision(List<LICHHOC> lichdaygiangvien, DateTime s, int ca)
        {
            return lichdaygiangvien.Any(t => (
            t.NGAYHOC.Day == s.Day &&
            t.NGAYHOC.Month == s.Month &&
            t.NGAYHOC.Year == s.Year &&
           t. MACA.Contains(ca.ToString())
            )
            );
        }

        private List<Lich> isInWeek(int i)
        {
            i--;
            if (i == -1)
                i = 6;
            List<Lich> lich = new List<Lich>();

            foreach (Lich l in input)
                if (l.Thu == i)
                   lich.Add(l);
            return lich;
        }


    }
}
