﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroUserControl.ContentMenu.Receipt;
using MetroUserControl.ContentMenu.Permission;
using MetroUserControl.ContentMenu.Score;
using MetroUserControl.ContentMenu.User;
using MetroUserControl.ContentMenu.Class;
using MetroUserControl.ContentMenu.Schedule;
using MetroUserControl.ContentMenu.Account;
using MetroUserControl.ContentMenu.Systems;
using MetroUserControl.ContentMenu.User.Staff;
using MetroUserControl.ContentMenu.User.Teacher;
using MetroUserControl.ContentMenu.User.Student;
using MetroUserControl.ContentMenu.Summary;
using MetroUserControl.ContentMenu.Roll_up;
using MetroControl.Controls;
using MetroControl.Components;
using MetroUserControl.ContentMenu.Mail;
using MetroUserControl.ContentMenu.Export;
using MetroControl;
using static MetroDataset.Helper;
using MetroDataset;
using MetroUserControl.ContentMenu.Schedule.Staff;
using MetroUserControl.ContentMenu.Schedule.Student;
using MetroUserControl.ContentMenu.Schedule.Teacher;
using static MetroControl.MetroHelper;

namespace MetroUserControl
{
    public partial class MetroBubble : MetroBridge.Bridge, IMetroList
    {
        public string id;
        public string title;
        public Func<object> funcData;
        public Func<int, int, int, string, string, object> inputFunc;
        //Chỉ có list danh mục content mới có ID 
        public class ID
        {
            public const string LIST_ACCOUNT_BYSTAFF = "MH0000"; //DS tài khoản của nhân vien
            public const string LIST_CLASS_BYSTAFF = "MH0001"; //DS lớp của nhân viên
            public const string LIST_PERMISSION_BYSTAFF = "MH0002"; //DS phân quyền của nhân viên
            public const string LIST_RECEIPT_BYSTAFF = "MH0003"; //DS biên lai của nhân viên

            public const string LIST_SCHEDULE_BYSTAFF = "MH0004"; //DS quản lý lịch của nhân viên
            public const string LIST_SCHEDULE_BYTEACHER = "MH0005"; //DS lịch dạy của giảng viên
            public const string LIST_SCHEDULE_BYSTUDENT = "MH0006"; //DS lịch học của học viên

            public const string LIST_SCORE_BYSTAFF = "MH0007"; //DS kết quả học tập của Nhân viên
            public const string LIST_SUMMARY_BYSTAFF = "MH0008"; //DS thống kê của Nhân viên
            public const string LIST_SYSTEM_BYSTAFF = "MH0009"; //DS Retore của Nhân viên

            public const string LIST_STAFF_BYSTAFF = "MH0010"; //DS Nhân viên của nhân viên
            public const string LIST_TEACHER_BYSTAFF = "MH0011"; //DS Giảng viên của nhân viên
            public const string LIST_STUDENT_BYSTAFF = "MH0012"; //DS Học viên của nhân viên

            public const string LIST_SCORE_BYSTUDENT = "MH0013"; //Xem điểm của học viên
            public const string LIST_ROLLUP_BYTEACHER = "MH0014"; //Điểm danh của giảng viên

            public const string INFO_BYSTAFF = "MH0015"; //Thông tin cá nhân của nhân viên
            public const string INFO_BYTEACHER = "MH0016"; //Thông tin cá nhân của giảng viên
            public const string INFO_BYSTUDENT = "MH0017"; //Thông tin cá nhân của học vien

            //Học viên: 3 tab
            //Gianrg viên: 3 tab
            //Nhân viên: 12 tab

        }
        //Các user kế thừa MetroBubble phải có NAME
        public class NAME
        {
            public const string LIST_ACCOUNT_BYSTAFF = "Quản lý tài khoản"; //DS tài khoản của nhân vien
            public const string LIST_CLASS_BYSTAFF = "Quản lý lớp học"; //DS lớp của nhân viên
            public const string LIST_PERMISSION_BYSTAFF = "Quản lý phân quyền"; //DS phân quyền của nhân viên
            public const string LIST_RECEIPT_BYSTAFF = "Quản lý biên lai"; //DS biên lai của nhân viên

            public const string LIST_SCHEDULE_BYSTAFF = "Quản lý lịch"; //DS quản lý lịch của nhân viên
            public const string LIST_SCHEDULE_BYTEACHER = "Xem lịch dạy"; //DS lịch dạy của giảng viên
            public const string LIST_SCHEDULE_BYSTUDENT = "Xem lịch học"; //DS lịch học của học viên

            public const string LIST_SCORE_BYSTAFF = "Quản lý điểm"; //DS kết quả học tập của Nhân viên
            public const string LIST_SUMMARY_BYSTAFF = "Quản lý thống kê"; //DS thống kê của Nhân viên
            public const string LIST_SYSTEM_BYSTAFF = "Quản lý hệ thống"; //DS Retore của Nhân viên

            public const string LIST_USER = "Quản lý người dùng"; //DS Người dùng
            public const string LIST_STAFF_BYSTAFF = "Quản lý nhân viên"; //DS Nhân viên của nhân viên
            public const string LIST_TEACHER_BYSTAFF = "Quản lý giảng viên"; //DS Giảng viên của nhân viên
            public const string LIST_STUDENT_BYSTAFF = "Quản lý học viên"; //DS Học viên của nhân viên

            public const string LIST_SCORE_BYSTUDENT = "Xem kết quả học tập"; //Xem điểm của học viên
            public const string LIST_ROLLUP_BYTEACHER = "Quản lý điểm danh"; //Điểm danh của giảng viên

            public const string INFO_BYSTAFF = "Thông tin cá nhân"; //Thông tin cá nhân của nhân viên
            public const string INFO_BYTEACHER = "Thông tin cá nhân"; //Thông tin cá nhân của giảng viên
            public const string INFO_BYSTUDENT = "Thông tin cá nhân"; //Thông tin cá nhân của học vien

            public const string DETAIL_CLASS = "Thông tin lớp học";
            public const string DETAIL_GROUP_PERMISSION = "Thông tin nhóm quyền";
            public const string DETAIL_ACTION_PERMISSION = "Thông tin thao tác của nhóm quyền";
            public const string DETAIL_RECEIPT = "Thông tin biên lai";
            public const string DETAIL_UPDATE_SCHEDILE = "Thông tin cập nhật lịch";
            public const string DETAIL_BACKUP = "Thông tin sao lưu";
            public const string DETAIL_CHANGEPASS = "Cập nhật mật khẩu";
            public const string DETAIL_SCORE = "Thông tin điểm";
            public const string CREATE_CLASS = "Tạo lớp mới";
            public const string MAIL = "Hộp thư điện tử";
            public const string EXPORT = "Xuất danh sách";
            public const string SELECT_SCHEDULE = "Chọn giờ học";
            public const string VIEW_HISTORY = "Xem lịch sử huỷ biên lai";
            public const string INFO_COURSE = "Thông tin khóa học";
        }

        public MetroBubble()
        {
            InitializeComponent();
            // this.VisibleChanged += MetroBubble_VisibleChanged;
            this.Load += MetroBubble_Load;
        }

        private void MetroBubble_Load(object sender, EventArgs e)
        {

        }



        //Load data: load mỗi khi người dùng chuyển item menu
        #region

            
        private DataGridView LoadDataForListScheduleTeacher(MetroListScheduleTeacher metroListScheduleTeacher)
        {
            int page = metroListScheduleTeacher.gridpage.PageNumber;
            int count = metroListScheduleTeacher.gridpage.PageNumber;
            metroListScheduleTeacher.gridpage.grid.DataSource = funcData();
            return metroListScheduleTeacher.gridpage.grid;
        }


        private DataGridView LoadDataForListScheduleStudent(MetroListScheduleStudent metroListScheduleStudent)
        {
            metroListScheduleStudent.gridpage.grid.DataSource = funcData();
            return metroListScheduleStudent.gridpage.grid;
        }

        private DataGridView LoadDataForCentralSummary(MetroCentralSummary metroCentralSummary)
        {
            metroCentralSummary.gridDetail.grid.DataSource = funcData();
            return metroCentralSummary.gridDetail.grid;
        }


        private DataGridView LoadDataForListSystem(MetroListSystem metroListSystem)
        {
            int page = metroListSystem.grid.PageNumber;
            int count = metroListSystem.grid.NumberRow;
            List<object> data_backup = (new Select()).HienThiTatCaTTBackUp(page, count);
            if (data_backup != null)
            {
                metroListSystem.grid.grid.DataSource = data_backup;
                metroListSystem.grid.AlignColumn(0, DataGridViewContentAlignment.MiddleCenter);
                metroListSystem.grid.AlignColumn(3, DataGridViewContentAlignment.MiddleCenter);
            }
            return metroListSystem.grid.grid;
        }

        private DataGridView LoadDataForViewScore(MetroViewScore metroViewScore)
        {
            metroViewScore.gridPage.grid.DataSource = funcData();
            metroViewScore.gridPage.AlignColumn(0, DataGridViewContentAlignment.MiddleCenter);
            metroViewScore.gridPage.AlignColumn(3, DataGridViewContentAlignment.MiddleCenter);
            metroViewScore.gridPage.AlignColumn(4, DataGridViewContentAlignment.MiddleCenter);
            return metroViewScore.gridPage.grid;
        }

        private DataGridView LoadDataForListPermission(MetroListPermission metroListPermission)
        {
            metroListPermission.gridPage.grid.DataSource = funcData();
            metroListPermission.gridPage.AlignColumn(0, DataGridViewContentAlignment.MiddleCenter);
            metroListPermission.gridPage.AlignColumn(3, DataGridViewContentAlignment.MiddleCenter);
            metroListPermission.gridPage.AlignColumn(4, DataGridViewContentAlignment.MiddleCenter);
            metroListPermission.gridPage.AlignColumn(5, DataGridViewContentAlignment.MiddleCenter);
            return metroListPermission.gridPage.grid;
        }

        private DataGridView LoadDataForListAccount(MetroListAccount metroListAccount)
        {
            metroListAccount.gridAccount.grid.DataSource = funcData();
            metroListAccount.gridAccount.AlignColumn(0, DataGridViewContentAlignment.MiddleCenter);
            metroListAccount.gridAccount.AlignColumn(3, DataGridViewContentAlignment.MiddleCenter);
            return metroListAccount.gridAccount.grid;
        }

        private DataGridView LoadDataForListScore(MetroListScore metroListScore)
        {
            metroListScore.gridDiem.grid.DataSource = funcData();
            metroListScore.gridDiem.AlignColumn(0, DataGridViewContentAlignment.MiddleCenter);
            metroListScore.gridDiem.AlignColumn(1, DataGridViewContentAlignment.MiddleCenter);
            metroListScore.gridDiem.AlignColumn(2, DataGridViewContentAlignment.MiddleCenter);
            metroListScore.gridDiem.AlignColumn(4, DataGridViewContentAlignment.MiddleCenter);
            metroListScore.gridDiem.AlignColumn(5, DataGridViewContentAlignment.MiddleCenter);
            metroListScore.gridDiem.AlignColumn(6, DataGridViewContentAlignment.MiddleCenter);
            metroListScore.gridDiem.AlignColumn(7, DataGridViewContentAlignment.MiddleCenter);
            return metroListScore.gridDiem.grid;
        }

    
        private DataGridView LoadDataForListSchedule(MetroListSchedule metroListSchedule)
        {
            int page = metroListSchedule.gridpage.PageNumber;
            int count = metroListSchedule.gridpage.PageNumber;
            metroListSchedule.gridpage.grid.DataSource = funcData();
            return metroListSchedule.gridpage.grid;
        }

        private DataGridView LoadDataForListClass(MetroListClass metroListClass)
        {
            using (DataSetQuanLyHocVienTableAdapters.QueriesTableAdapter
                q = new DataSetQuanLyHocVienTableAdapters.QueriesTableAdapter())
            {
                q.CapNhatSoLuongHocVien();
            }

            metroListClass.grdLop.grid.DataSource = funcData();
            if (metroListClass.grdLop.grid.DataSource != null)
            {
                metroListClass.grdLop.AlignColumn(0, DataGridViewContentAlignment.MiddleCenter);
                metroListClass.grdLop.AlignColumn(1, DataGridViewContentAlignment.MiddleCenter);
                metroListClass.grdLop.AlignColumn(3, DataGridViewContentAlignment.MiddleCenter);
                metroListClass.grdLop.AlignColumn(5, DataGridViewContentAlignment.MiddleCenter);
                metroListClass.grdLop.AlignColumn(6, DataGridViewContentAlignment.MiddleCenter);
            }
            return metroListClass.grdLop.grid;
        }

        private DataGridView LoadDataForListReceipt(MetroListReceipt metroListReceipt)
        {
            metroListReceipt.grdReceipt.grid.DataSource = funcData();
            if (metroListReceipt.grdReceipt.grid.DataSource != null)
            {
                metroListReceipt.grdReceipt.AlignColumn(0, DataGridViewContentAlignment.MiddleCenter);
                metroListReceipt.grdReceipt.AlignColumn(1, DataGridViewContentAlignment.MiddleCenter);
                metroListReceipt.grdReceipt.AlignColumn(2, DataGridViewContentAlignment.MiddleCenter);
                metroListReceipt.grdReceipt.AlignColumn(3, DataGridViewContentAlignment.MiddleCenter);
                metroListReceipt.grdReceipt.AlignColumn(4, DataGridViewContentAlignment.MiddleCenter);
                metroListReceipt.grdReceipt.AlignColumn(5, DataGridViewContentAlignment.MiddleCenter);
                metroListReceipt.grdReceipt.AlignColumn(6, DataGridViewContentAlignment.MiddleCenter);
            }
            return metroListReceipt.grdReceipt.grid;
        }

        private DataGridView LoadDataForListSummary(MetroListSummary metroListSummary)
        {
            if (funcData != null)
            {
                metroListSummary.gridParent.grid.DataSource = funcData();
            }
            return metroListSummary.gridParent.grid;

        }

        private DataGridView LoadDataForListUser(MetroListUser metroListUser)
        {
            MetroListUser.Type? type = metroListUser.getType;
            if (type == null)
                return metroListUser.grdUser.grid;

            metroListUser.grdUser.grid.DataSource = funcData();

            metroListUser.grdUser.AlignColumn(0, DataGridViewContentAlignment.MiddleCenter);
            metroListUser.grdUser.AlignColumn(2, DataGridViewContentAlignment.MiddleCenter);
            metroListUser.grdUser.AlignColumn(3, DataGridViewContentAlignment.MiddleCenter);
            metroListUser.grdUser.AlignColumn(4, DataGridViewContentAlignment.MiddleCenter);
            metroListUser.grdUser.AlignColumn(5, DataGridViewContentAlignment.MiddleCenter);
            metroListUser.grdUser.AlignColumn(6, DataGridViewContentAlignment.MiddleCenter);
            metroListUser.grdUser.AlignColumn(7, DataGridViewContentAlignment.MiddleCenter);
            metroListUser.grdUser.AlignColumn(8, DataGridViewContentAlignment.MiddleCenter);

            return metroListUser.grdUser.grid;
        }


        private DataGridView LoadDataForListRollup(MetroListRollUp metroListRollUp)
        {
            List<object> data = (new MetroDataset.Select()).HienThiLopHocGiangVien_Cbb(MetroHelper.iduser);
            if (data != null)
            {
                metroListRollUp.cbbLop.DataSource = data;
                if (data.Count != 0)
                    metroListRollUp.cbbLop.SelectedItem = metroListRollUp.cbbLop.Items[0];
            }
            return metroListRollUp.gridRollup.grid;
        }

        #endregion
        //Reload grid danh mục
        
        public void ReloadData(DataGridView grid, bool selected)
        {
            //lưu trạng thái đã chọn của người dùng
            int index = 0;
            try
            {
                if (selected)
                    index = (grid as MetroGridView).IndexCurrentRow();
            }
            catch (Exception e) { };

            if (this is MetroListPermission)
                grid = LoadDataForListPermission(this as MetroListPermission);
            else if (this is MetroListClass)
                grid = LoadDataForListClass(this as MetroListClass);
            else if (this is MetroListScore)
                grid = LoadDataForListScore(this as MetroListScore);
            else if (this is MetroViewScore)
                grid = LoadDataForViewScore(this as MetroViewScore);
            else if (this is MetroListUser)
                grid = LoadDataForListUser(this as MetroListUser);
            else if (this is MetroListRollUp)
                grid = LoadDataForListRollup(this as MetroListRollUp);
            else if (this is MetroListAccount)
                grid = LoadDataForListAccount(this as MetroListAccount);
            else if (this is MetroListSystem)
                grid = LoadDataForListSystem(this as MetroListSystem);
            else if (this is MetroListSchedule)
                grid = LoadDataForListSchedule(this as MetroListSchedule);
            else if (this is MetroListScheduleStudent)
                grid = LoadDataForListScheduleStudent(this as MetroListScheduleStudent);
            else if (this is MetroListScheduleTeacher)
                grid = LoadDataForListScheduleTeacher(this as MetroListScheduleTeacher);
            else if (this is MetroListReceipt)
                grid = LoadDataForListReceipt(this as MetroListReceipt);
            else if (this is MetroListSummary)
                grid = LoadDataForListSummary(this as MetroListSummary);
            else if (this is MetroCentralSummary)
                grid = LoadDataForCentralSummary(this as MetroCentralSummary);
            else if (this is MetroViewHistory)
                grid = LoadDataForViewHistory(this as MetroViewHistory);
            else if (this is MetroCreateCourse)
                grid = LoadDataForInfoCourse(this as MetroCreateCourse);
            this.Focus();

            try
            {
                grid.CurrentCell = grid.Rows[index].Cells[0];
            }
            catch (Exception e) { }
        }

        private DataGridView LoadDataForViewHistory(MetroViewHistory metroViewHistory)
        {
            if (funcData != null)
            {
                metroViewHistory.grdReceipt.grid.DataSource = funcData();
                metroViewHistory.grdReceipt.AlignColumn(0, DataGridViewContentAlignment.MiddleCenter);
                metroViewHistory.grdReceipt.AlignColumn(1, DataGridViewContentAlignment.MiddleCenter);
                metroViewHistory.grdReceipt.AlignColumn(2, DataGridViewContentAlignment.MiddleCenter);
                metroViewHistory.grdReceipt.AlignColumn(3, DataGridViewContentAlignment.MiddleCenter);
                metroViewHistory.grdReceipt.AlignColumn(4, DataGridViewContentAlignment.MiddleCenter);
                metroViewHistory.grdReceipt.AlignColumn(6, DataGridViewContentAlignment.MiddleRight);
                metroViewHistory.grdReceipt.AlignColumn(7, DataGridViewContentAlignment.MiddleCenter);
                metroViewHistory.grdReceipt.AlignColumn(8, DataGridViewContentAlignment.MiddleCenter);
            }
            return metroViewHistory.grdReceipt.grid;
        }
        private DataGridView LoadDataForInfoCourse(MetroCreateCourse metroCreateCourse)
        {
            if (funcData != null)
            {
                metroCreateCourse.grdCourse.grid.DataSource = funcData();
                metroCreateCourse.grdCourse.AlignColumn(0, DataGridViewContentAlignment.MiddleCenter);
                metroCreateCourse.grdCourse.AlignColumn(1, DataGridViewContentAlignment.MiddleCenter);
                metroCreateCourse.grdCourse.AlignColumn(2, DataGridViewContentAlignment.MiddleCenter);

            }
            return metroCreateCourse.grdCourse.grid;
        }

        private void MetroBubble_VisibleChanged(object sender, EventArgs e)
        {
            if (!this.Visible || funcData == null)
                return;

            if (this is MetroListUser)
                LoadDataForListUser(this as MetroListUser);
            else if (this is MetroListReceipt)
                LoadDataForListReceipt(this as MetroListReceipt);
            else if (this is MetroListClass)
                LoadDataForListClass(this as MetroListClass);
            else if (this is MetroListSchedule)
                LoadDataForListSchedule(this as MetroListSchedule);
            else if (this is MetroListScheduleStudent)
                LoadDataForListScheduleStudent(this as MetroListScheduleStudent);
            else if (this is MetroListScheduleTeacher)
                LoadDataForListScheduleTeacher(this as MetroListScheduleTeacher);
            else if (this is MetroListScore)
                LoadDataForListScore(this as MetroListScore);
            else if (this is MetroListAccount)
                LoadDataForListAccount(this as MetroListAccount);
            else if (this is MetroListPermission)
                LoadDataForListPermission(this as MetroListPermission);
            else if (this is MetroViewScore)
                LoadDataForViewScore(this as MetroViewScore);
            else if (this is MetroListRollUp)
                LoadDataForListRollup(this as MetroListRollUp);
            else if (this is MetroDetailStaff)
                (this as MetroDetailStaff).settingLayoutByStaff();
            else if (this is MetroDetailStudent)
                (this as MetroDetailStudent).settingLayoutByStudent();
            else if (this is MetroDetailTeacher)
                (this as MetroDetailTeacher).settingLayoutByTeacher();
            else if (this is MetroListSystem)
                LoadDataForListSystem(this as MetroListSystem);
            else if (this is MetroListSummary)
                LoadDataForListSummary(this as MetroListSummary);
            else if (this is MetroCentralSummary)
                LoadDataForCentralSummary(this as MetroCentralSummary);
            else if (this is MetroViewHistory)
                LoadDataForViewHistory(this as MetroViewHistory);
            else if (this is MetroCreateCourse)
                LoadDataForInfoCourse(this as MetroCreateCourse);

        }

        public override void Init()
        {

            if (this is MetroListUser)
            {
                if ((this as MetroListUser).getType == MetroListUser.Type.Staff)
                {
                    id = ID.LIST_STAFF_BYSTAFF;
                    title = NAME.LIST_STAFF_BYSTAFF;
                }
                else if ((this as MetroListUser).getType == MetroListUser.Type.Student)
                {
                    id = ID.LIST_STUDENT_BYSTAFF;
                    title = NAME.LIST_STUDENT_BYSTAFF;
                }
                else if ((this as MetroListUser).getType == MetroListUser.Type.Teacher)
                {
                    id = ID.LIST_TEACHER_BYSTAFF;
                    title = NAME.LIST_TEACHER_BYSTAFF;
                }
            }
            else if (this is MetroListReceipt)
            {
                id = ID.LIST_RECEIPT_BYSTAFF;
                title = NAME.LIST_RECEIPT_BYSTAFF;
            }
            else if (this is MetroListClass)
            {
                id = ID.LIST_CLASS_BYSTAFF;
                title = NAME.LIST_CLASS_BYSTAFF;
            }
            else if (this is MetroListSchedule)
            {
                id = ID.LIST_SCHEDULE_BYSTAFF;
                title = NAME.LIST_SCHEDULE_BYSTAFF;
            }
            else if (this is MetroListScheduleStudent)
            {
                id = ID.LIST_SCHEDULE_BYSTUDENT;
                title = NAME.LIST_SCHEDULE_BYSTUDENT;
            }
            else if (this is MetroListScheduleTeacher)
            {
                id = ID.LIST_SCHEDULE_BYTEACHER;
                title = NAME.LIST_SCHEDULE_BYTEACHER;
            }
            else if (this is MetroListScore)
            {
                id = ID.LIST_SCORE_BYSTAFF;
                title = NAME.LIST_SCORE_BYSTAFF;
            }
            else if (this is MetroListAccount)
            {
                id = ID.LIST_ACCOUNT_BYSTAFF;
                title = NAME.LIST_ACCOUNT_BYSTAFF;
            }
            else if (this is MetroListPermission)
            {
                id = ID.LIST_PERMISSION_BYSTAFF;
                title = NAME.LIST_PERMISSION_BYSTAFF;
            }
            else if (this is MetroViewScore)
            {
                id = ID.LIST_SCORE_BYSTUDENT;
                title = NAME.LIST_SCORE_BYSTUDENT;
            }
            else if (this is MetroTabListSummary)
            {
                id = ID.LIST_SUMMARY_BYSTAFF;
                title = NAME.LIST_SUMMARY_BYSTAFF;
            }
            else if (this is MetroListSystem)
            {
                id = ID.LIST_SYSTEM_BYSTAFF;
                title = NAME.LIST_SYSTEM_BYSTAFF;
            }
            else if (this is MetroListRollUp)
            {
                id = ID.LIST_ROLLUP_BYTEACHER;
                title = NAME.LIST_ROLLUP_BYTEACHER;
            }
            else if (this is MetroDetailStaff)
            {
                id = ID.INFO_BYSTAFF;
                title = NAME.INFO_BYSTAFF;
            }
            else if (this is MetroDetailTeacher)
            {
                id = ID.INFO_BYTEACHER;
                title = NAME.INFO_BYTEACHER;
            }
            else if (this is MetroDetailStudent)
            {
                id = ID.INFO_BYSTUDENT;
                title = NAME.INFO_BYSTUDENT;
            }
            else if (this is MetroTabListUser)
            {
                id = ID.LIST_STAFF_BYSTAFF + "," + ID.LIST_STUDENT_BYSTAFF + "," + ID.LIST_TEACHER_BYSTAFF;
                title = NAME.LIST_USER;
            }
            else if (this is MetroDetailClass)
                title = NAME.DETAIL_CLASS;
            else if (this is MetroGroupPermission)
                title = NAME.DETAIL_GROUP_PERMISSION;
            else if (this is MetroActionPermission)
                title = NAME.DETAIL_ACTION_PERMISSION;
            else if (this is MetroReceipt)
                title = NAME.DETAIL_RECEIPT;
            else if (this is MetroUpdateSchedule)
                title = NAME.DETAIL_UPDATE_SCHEDILE;
            else if (this is MetroBackup)
                title = NAME.DETAIL_BACKUP;
            else if (this is MetroChangePass)
                title = NAME.DETAIL_CHANGEPASS;
            else if (this is MetroDetailScore)
                title = NAME.DETAIL_SCORE;
            else if (this is MetroMail)
                title = NAME.MAIL;
            else if (this is MetroExport)
                title = NAME.EXPORT;
            else if (this is MetroSelectSchedule)
                title = NAME.SELECT_SCHEDULE;
            else if (this is MetroCreateClass)
                title = NAME.CREATE_CLASS;
            else if (this is MetroViewHistory)
                title = NAME.VIEW_HISTORY;
            else if (this is MetroCreateCourse)
                title = NAME.INFO_COURSE;


        }
        public void VisibleByPermission()
        {
            if (MetroHelper.dschitietquyen.Any(T => T.MAGIAODIEN == id))
                foreach (Control c in this.Controls)
                    VisibleControlByDetailPermission(id, c);
        }

        public override string GetId()
        {
            return id;
        }

        public override string GetTitle()
        {
            return title;
        }

        public NHANVIEN GetStaff()
        {
            NHANVIEN nv = (new Select()).GetStaffByID(MetroHelper.iduser);
            return nv;
        }

        ///*****************************RELOAD - FILTER - SEARCH*****************************************
        ///Func<int, int, int, string, string,object> inputFunc
        ///page - count - index - keyfilter - key search

        public void SettingLoadByPageFilterSearch(MetroGridViewPage gridPage, Func<int, int, int, string, string, object> funcSourceGrid, MetroCombobox cbbLoc, Func<object> funcSourceCbb, Control btnSearch, TextBox txtSearch)
        {
            if (this.inputFunc != null || !this.Visible)
                return;

            this.inputFunc = funcSourceGrid;
            //filter
            if (cbbLoc != null)
            {
                cbbLoc.DataSource = funcSourceCbb();
                cbbLoc.SelectedValueChanged += (s, e) =>
                {
                    if (cbbLoc.SelectedValue == null)
                        return;
                    txtSearch.Clear();
                    String key = cbbLoc.SelectedValue.ToString();
                    LoadByKey(gridPage, key);
                };
            }
            //search
            EventHelper.Helper.SetHandHover(btnSearch);
            btnSearch.Click += (s, en) => LoadWithSearch(gridPage, cbbLoc, txtSearch.Text);
            //load page
            if(cbbLoc != null)
                this.LoadByKey(gridPage, cbbLoc.SelectedValue == null? "" : cbbLoc.SelectedValue.ToString());
            else
                this.LoadByKey(gridPage, "");

            gridPage.SetOnClick(() => { (this as MetroBubble).ReloadData(gridPage.grid, false); });
        }
        public void LoadByKey(MetroGridViewPage gridPage, string key)
        {
            IndexAndKey i = MetroHelper.GetIndexAndKeyColumnFilter(key);
            if (i != null)
                LoadData(gridPage, i.Index, i.Key, "");
            else
                LoadData(gridPage, 0, "", ""); //ko có search/filter
        }

        private void LoadData(MetroGridViewPage gridPage, int index, string keyfilter, string keysearch)
        {
            funcData = new Func<object>(() =>
            {
                int page = gridPage.PageNumber;
                int count = gridPage.NumberRow;
                return inputFunc(page, count, index, keyfilter, keysearch);//hàm chính
            });
            this.ReloadData(gridPage.grid, false);
        }

        public void LoadWithSearch(MetroGridViewPage gridPage, MetroCombobox cbbLoc, string searchkey)
        {
            if (cbbLoc != null)
            {
                if (cbbLoc.SelectedValue == null)
                    return;
                String key = cbbLoc.SelectedValue.ToString(); //key filter
                IndexAndKey i = MetroHelper.GetIndexAndKeyColumnFilter(key);
                if (i != null)
                    LoadData(gridPage, i.Index, i.Key, searchkey);
            }
            else
                LoadData(gridPage,0, "", searchkey);

        }
    }
}

