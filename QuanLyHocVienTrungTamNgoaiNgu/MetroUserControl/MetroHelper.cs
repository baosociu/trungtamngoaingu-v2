﻿using MetroControl.Components;
using MetroControl.Controls;
using MetroDataset;
using MetroUserControl;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static MetroUserControl.MetroBubble;

namespace MetroControl
{
    public class MetroHelper
    {

        static MetroBubble p;
        static public List<NHOMNGUOIDUNG_GIAODIEN> dschitietquyen;
        public static String XMASTER = "XMASTR";
        public static String STUDENT = "NND001";
        public static String TEACHER = "NND002";
        public static String STAFF = "NND003";
        public static List<String> default_student = new List<string>() { ID.INFO_BYSTUDENT, ID.LIST_SCHEDULE_BYSTUDENT, ID.LIST_SCORE_BYSTUDENT };
        public static List<String> default_teacher = new List<string>() { ID.INFO_BYTEACHER, ID.LIST_ROLLUP_BYTEACHER, ID.LIST_SCHEDULE_BYTEACHER };
        public static List<String> default_staff = new List<string>() { ID.INFO_BYSTAFF, ID.LIST_ACCOUNT_BYSTAFF, ID.LIST_CLASS_BYSTAFF,
        ID.LIST_PERMISSION_BYSTAFF, ID.LIST_RECEIPT_BYSTAFF, ID.LIST_SCHEDULE_BYSTAFF,
        ID.LIST_SCORE_BYSTAFF, ID.LIST_STAFF_BYSTAFF, ID.LIST_STUDENT_BYSTAFF,
        ID.LIST_SUMMARY_BYSTAFF, ID.LIST_TEACHER_BYSTAFF };
        public static List<String> default_master = new List<string>() { ID.INFO_BYSTAFF, ID.LIST_ACCOUNT_BYSTAFF, ID.LIST_CLASS_BYSTAFF,
        ID.LIST_PERMISSION_BYSTAFF, ID.LIST_RECEIPT_BYSTAFF, ID.LIST_SCHEDULE_BYSTAFF,
        ID.LIST_SCORE_BYSTAFF, ID.LIST_STAFF_BYSTAFF, ID.LIST_STUDENT_BYSTAFF,
        ID.LIST_SUMMARY_BYSTAFF, ID.LIST_TEACHER_BYSTAFF, ID.LIST_SYSTEM_BYSTAFF };
        public static List<string> default_group = new List<string>() { "NND001", "NND002", "NND003", "XMASTR" };

        public static void OpenNewTab(MetroBubble parent, MetroBubble u, Size size, bool disableResize, Action<object, FormClosingEventArgs> formClosing)
        {
            p = parent;
            u.Init();
            //TODO: create metro form
            MetroFramework.Forms.MetroForm f = new MetroFramework.Forms.MetroForm();
            f.Size = size;
            f.StartPosition = FormStartPosition.CenterScreen;
            //f.TopMost = true;
            //TODO: add user control
            f.Controls.Add(new MetroContentMenu().AddUserControl(u));
            f.Text = u.title.ToUpper();
            //f.ShowInTaskbar = false;
            if (disableResize)
            {
                f.MaximizeBox = false;
                f.MinimizeBox = false;
                f.MinimumSize = size;
                f.MaximumSize = size;
            }
            f.Icon = parent.ParentForm.Icon;
            f.Show();

            f.FormClosing += (s, e) =>
            {
                formClosing(s, e);
                if (!e.Cancel)
                {
                    F_FormClosing(s, e);
                }
            };
            p.ParentForm.Hide();
        }



        public static void OpenNewTab(MetroBubble parent, MetroBubble u, Size size, bool disableResize)
        {
            p = parent;
            u.Init();
            //TODO: create metro form
            MetroFramework.Forms.MetroForm f = new MetroFramework.Forms.MetroForm();
            f.Size = size;
            f.StartPosition = FormStartPosition.CenterScreen;
            //f.TopMost = true;
            //TODO: add user control
            f.Controls.Add(new MetroContentMenu().AddUserControl(u));
            f.Text = u.title.ToUpper();
            //f.ShowInTaskbar = false;
            if (disableResize)
            {
                f.MaximizeBox = false;
                f.MinimizeBox = false;
                f.MinimumSize = size;
                f.MaximumSize = size;
            }
            f.Icon = parent.ParentForm.Icon;
            f.Show();
            f.FormClosing += F_FormClosing;
            p.ParentForm.Hide();
        }


        public static void OpenNewTab(MetroBubble parent, MetroBubble u)
        {
            OpenNewTab(parent, u, new System.Drawing.Size(920, 660), false, F_FormClosing);
        }

        public static void OpenNewTab(MetroBubble parent, MetroBubble u, Action<object, FormClosingEventArgs> formClosing)
        {
            OpenNewTab(parent, u, new System.Drawing.Size(920, 660), false, formClosing);
        }


        public static void GoToTab(Form f, MetroUserControl.MetroBubble u)
        {
            foreach (Control c in f.Controls)
                if (c is MetroContentMenu)
                    f.Controls.Remove(c);
            u.Init();
            f.Controls.Add(new MetroContentMenu().AddUserControl(u));
            f.Text = u.title;
        }

        private static void F_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (p == null)
                return;
            p.ParentForm.Show();
            p.Focus();
            MetroGridView grid = FindMetroGridView(p);
            if (p is MetroBubble)
                (p as MetroBubble).ReloadData(grid, true);
        }

        private static MetroGridView FindMetroGridView(Control control)
        {
            if (control is MetroGridViewPage)
                return (control as MetroGridViewPage).grid;

            foreach (Control c in control.Controls)
            {
                MetroGridView grid = FindMetroGridView(c);
                if (grid != null)
                    return grid;
            }
            return null;

        }

        public static void ReadOnlyInputs(Control control)
        {
            if (control is TextBox || control is MetroTextbox || control is MaskedTextBox || control is CheckBox || control is RadioButton)
            {
                if (control is TextBox)
                    (control as TextBox).ReadOnly = true;
                else
                if (control is MetroTextbox)
                    (control as MetroTextbox).ReadOnly = true;
                else
                if (control is MaskedTextBox)
                    (control as MaskedTextBox).ReadOnly = true;
                else
                if (control is RadioButton)
                    (control as RadioButton).Enabled = false;
                else
                    if (control is CheckBox)
                    (control as CheckBox).Enabled = false;
            }

            if (control.Controls.Count == 0)
                return;
            foreach (Control c in control.Controls)
                ReadOnlyInputs(c);
        }
        //phương thức chuyển từ image sang string
        public static string ImageToBase64(Image image, ImageFormat format)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                image.Save(ms, format);
                byte[] imageBytes = ms.ToArray();

                string base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
        }

        public static Binary ImageToBinary(Image image)
        {
            ImageFormat format = image.RawFormat;
            using (MemoryStream ms = new MemoryStream())
            {
                image.Save(ms, format);
                byte[] imageBytes = ms.ToArray();

                string base64String = Convert.ToBase64String(imageBytes);
                byte[] bytes = Encoding.ASCII.GetBytes(base64String);
                return new Binary(bytes);
            }
        }

        public static OpenFileDialog createAvatarOpenFileDialog()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Chọn ảnh đại diện";
            openFileDialog.DefaultExt = "jpg";
            openFileDialog.Filter = "JPG Files (*.jpg)|*.jpg| PNG Files (*.png)|*.png";
            openFileDialog.FileName = "";
            openFileDialog.CheckFileExists = true;
            openFileDialog.CheckPathExists = true;
            return openFileDialog;
        }


        //phương thức chuyển string sang image
        public static Image Base64ToImage(string base64String)
        {
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
              imageBytes.Length);

            ms.Write(imageBytes, 0, imageBytes.Length);
            Image image = Image.FromStream(ms, true);
            return image;
        }
        public static Image Base64ToImage(Binary binary)
        {
            byte[] bytes = binary.ToArray();
            string base64String = Encoding.Default.GetString(bytes);
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
              imageBytes.Length);

            ms.Write(imageBytes, 0, imageBytes.Length);
            Image image = Image.FromStream(ms, true);
            return image;
        }

        public static void OpenChoseAvatar(OpenFileDialog openFileDialog, PictureBox pictureBox)
        {
            DialogResult r = openFileDialog.ShowDialog();
            if (r == DialogResult.OK)
            {
                EventHelper.Helper.TryCatch(() =>
                {
                    Image image = Image.FromFile(openFileDialog.FileName);
                    pictureBox.Image = image;
                    //if (image.Width > image.Height)
                    //    pictureBox.Image.RotateFlip(RotateFlipType.Rotate90FlipNone);
                    pictureBox.Refresh();

                });
            }
        }

        public static bool IsNullOrEmpty(object[] input)
        {
            Func<object> fun = new Func<object>(() =>
                {
                    foreach (object o in input)
                        if ((o == null) || (String.IsNullOrEmpty(o.ToString())) || (String.IsNullOrWhiteSpace(o.ToString())))
                            return true;
                    return false;
                }
                );
            var r = EventHelper.Helper.TryCatchReturnValue(fun);
            if (r == null)
                return true;
            return (Boolean)r;
        }



        public static NHANVIEN staff;
        public static GIANGVIEN teacher;
        public static HOCVIEN student;
        public static string iduser;
        public static void SetCurrentUser(string id)
        {
            if (iduser != null)
                return;

            iduser = id;
            MetroDataset.Helper.idUser = id;
            string type = EventHelper.Helper.TryCatchReturnValue(() => iduser.Substring(0, 2)).ToString();
            if (type == "NV")
                staff = (new Select()).GetStaffByID(id);
            else
                if (type == "HV")
                student = (new Select()).GetStudentByID(id);
            else
                if (type == "GV")
                teacher = (new Select()).GetTeacherByID(id);
        }
        public static NGUOIDUNG GetCurrentAccount()
        {
            if (iduser != null)
                return (new Select()).GetAccountByID(iduser);
            else
                return null;
        }

        public static string GetOnlyNameUser()
        {
            string name = "";
            if (staff != null)
                name = staff.TENNHANVIEN;
            else if (teacher != null)
                name = teacher.TENGIANGVIEN;
            else if (student != null)
                name = student.TENHOCVIEN;
            if (!String.IsNullOrEmpty(name) && !String.IsNullOrWhiteSpace(name))
                return MetroDataset.Helper.GetHoTen(name).Ten;
            return "";
        }

        public static void ClearUser()
        {
            iduser = null;
            staff = null;
            teacher = null;
            student = null;
            MetroDataset.Helper.idUser = null;

        }

        public class IndexAndKey
        {
            int index;
            string key;
            public IndexAndKey(int index, string key)
            {
                this.Index = index;
                this.Key = key;
            }

            public int Index { get => index; set => index = value; }
            public string Key { get => key; set => key = value; }
        }

        public static bool isMaster()
        {
            bool r = dschitietquyen.Any(t=>t.MANHOMNGUOIDUNG ==XMASTER);
            return r;
        }

        public static bool isMasterByID(string mataikhoan)
        {
            List<NHOMNGUOIDUNG_GIAODIEN> lst = (new Select()).LayDanhSachChiTietThaoTacQuyen(mataikhoan);
            bool r = lst.Any(t => t.MANHOMNGUOIDUNG == XMASTER);
            return r;
        }

        public static bool isStaffByIDGroup(String idgroup)
        {
            return idgroup.StartsWith("S") || idgroup == STAFF;
        }

        public static bool isTeacherByIDGroup(String idgroup)
        {
            return idgroup.StartsWith("G") || idgroup == TEACHER;
        }

        public static bool isStudentByIDGroup(String idgroup)
        {
            return idgroup.StartsWith("H") || idgroup == STUDENT;
        }


        public static bool isStudentByID(String id)
        {
            return id.StartsWith("HV");
        }
        public static bool isStaffByID(String id)
        {
            return id.StartsWith("NV");
        }
        public static bool isTeacherByID(String id)
        {
            return id.StartsWith("GV");
        }

        public static IndexAndKey GetIndexAndKeyColumnFilter(string valuecbb)
        {
            Func<IndexAndKey> f = new Func<IndexAndKey>(() =>
            {
                string index = valuecbb.Substring(valuecbb.LastIndexOf(" ") + 1);
                string key = valuecbb.Substring(0, valuecbb.IndexOf(" "));
                return new IndexAndKey(int.Parse(index), key);
            });
            object o = EventHelper.Helper.TryCatchReturnValue(f);
            if (o != null)
                return (IndexAndKey)o;
            return null;
        }


        public static void VisibleControlByDetailPermission(string mamanhinh, Control control)
        {
            foreach (Control c in control.Controls)
                VisibleControlByDetailPermission(mamanhinh,c);
            PropertyInfo p = control.GetType().GetProperties().FirstOrDefault(t => t.Name == "TypeData");
            if(p!= null)
            {
                MetroDataType a = (MetroDataType)p.GetValue(control);
                if (a != null && control.Visible)
                {
                    List<String> list = a.ToListString();
                    if (dschitietquyen.Any(t => t.MANHOMNGUOIDUNG == "XMASTR" && t.MAGIAODIEN == mamanhinh))
                        control.Visible = true;
                    else
                    {
                        List<String> quyen = dschitietquyen.Where(t => t.MAGIAODIEN == mamanhinh).Select(t => t.MATHAOTAC).ToList();

                        if (list.All(t => quyen.Contains(t)) || quyen.Contains("TT0006"))
                            control.Visible = true;
                        else
                            control.Visible = false;
                    }
                }
            }
        }
    }
}
