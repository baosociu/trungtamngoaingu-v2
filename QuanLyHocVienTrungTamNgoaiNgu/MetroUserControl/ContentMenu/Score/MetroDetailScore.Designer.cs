﻿namespace MetroUserControl.ContentMenu.Score
{
    partial class MetroDetailScore
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbScore = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.txtLan = new MetroControl.Components.MetroTextbox();
            this.txtDiem = new MetroControl.Components.MetroTextbox();
            this.metroLabel3 = new MetroControl.Components.MetroLabel();
            this.metroLabel4 = new MetroControl.Components.MetroLabel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.lblLoaiDiem = new MetroControl.Components.MetroLabel();
            this.radioDiemQuaTrinh = new MetroControl.Components.MetroRadioButton();
            this.radioDiemDauRa = new MetroControl.Components.MetroRadioButton();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.cbbMaKhoa = new MetroControl.Components.MetroCombobox();
            this.cbbMaLop = new MetroControl.Components.MetroCombobox();
            this.txtMaDiem = new MetroControl.Components.MetroTextbox();
            this.metroLabel6 = new MetroControl.Components.MetroLabel();
            this.metroLabel7 = new MetroControl.Components.MetroLabel();
            this.txtTenHocVien = new MetroControl.Components.MetroTextbox();
            this.metroLabel11 = new MetroControl.Components.MetroLabel();
            this.metroLabel1 = new MetroControl.Components.MetroLabel();
            this.metroLabel5 = new MetroControl.Components.MetroLabel();
            this.cbbMaHocVien = new MetroControl.Components.MetroCombobox();
            this.gboxSearch = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.btnRight = new MetroControl.Components.MetroButton();
            this.btnLeft = new MetroControl.Components.MetroButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.gbScore.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.gboxSearch.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbScore
            // 
            this.tableLayoutPanel5.SetColumnSpan(this.gbScore, 4);
            this.gbScore.Controls.Add(this.tableLayoutPanel6);
            this.gbScore.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbScore.Location = new System.Drawing.Point(3, 3);
            this.gbScore.Name = "gbScore";
            this.gbScore.Size = new System.Drawing.Size(240, 214);
            this.gbScore.TabIndex = 13;
            this.gbScore.TabStop = false;
            this.gbScore.Text = "Thông tin điểm";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.AllowDrop = true;
            this.tableLayoutPanel6.AutoScroll = true;
            this.tableLayoutPanel6.AutoSize = true;
            this.tableLayoutPanel6.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel6.ColumnCount = 4;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.33333F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel6.Controls.Add(this.txtLan, 2, 2);
            this.tableLayoutPanel6.Controls.Add(this.txtDiem, 2, 1);
            this.tableLayoutPanel6.Controls.Add(this.metroLabel3, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.metroLabel4, 1, 2);
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel7, 1, 3);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 7;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(234, 195);
            this.tableLayoutPanel6.TabIndex = 10;
            // 
            // txtLan
            // 
            this.txtLan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLan.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtLan.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtLan.Location = new System.Drawing.Point(74, 53);
            this.txtLan.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtLan.Name = "txtLan";
            this.txtLan.ReadOnly = true;
            this.txtLan.Size = new System.Drawing.Size(145, 23);
            this.txtLan.TabIndex = 42;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtDiem
            // 
            this.txtDiem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDiem.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtDiem.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtDiem.Location = new System.Drawing.Point(74, 21);
            this.txtDiem.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtDiem.Name = "txtDiem";
            this.txtDiem.Size = new System.Drawing.Size(145, 23);
            this.txtDiem.TabIndex = 41;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel3.Location = new System.Drawing.Point(17, 17);
            this.metroLabel3.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(47, 32);
            this.metroLabel3.TabIndex = 39;
            this.metroLabel3.Text = "Điểm:";
            this.metroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel4.Location = new System.Drawing.Point(14, 49);
            this.metroLabel4.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(50, 32);
            this.metroLabel4.TabIndex = 2;
            this.metroLabel4.Text = "Lần:";
            this.metroLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel6.SetColumnSpan(this.tableLayoutPanel7, 2);
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Controls.Add(this.lblLoaiDiem, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.radioDiemQuaTrinh, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.radioDiemDauRa, 1, 1);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(14, 81);
            this.tableLayoutPanel7.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 4;
            this.tableLayoutPanel6.SetRowSpan(this.tableLayoutPanel7, 3);
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(205, 96);
            this.tableLayoutPanel7.TabIndex = 43;
            // 
            // lblLoaiDiem
            // 
            this.lblLoaiDiem.AutoSize = true;
            this.lblLoaiDiem.BackColor = System.Drawing.Color.Transparent;
            this.lblLoaiDiem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLoaiDiem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblLoaiDiem.Location = new System.Drawing.Point(3, 0);
            this.lblLoaiDiem.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.lblLoaiDiem.Name = "lblLoaiDiem";
            this.lblLoaiDiem.Size = new System.Drawing.Size(47, 28);
            this.lblLoaiDiem.TabIndex = 30;
            this.lblLoaiDiem.Text = "Loại";
            this.lblLoaiDiem.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // radioDiemQuaTrinh
            // 
            this.radioDiemQuaTrinh.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.radioDiemQuaTrinh.AutoSize = true;
            this.radioDiemQuaTrinh.Location = new System.Drawing.Point(70, 3);
            this.radioDiemQuaTrinh.Margin = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.radioDiemQuaTrinh.Name = "radioDiemQuaTrinh";
            this.radioDiemQuaTrinh.Size = new System.Drawing.Size(99, 22);
            this.radioDiemQuaTrinh.TabIndex = 32;
            this.radioDiemQuaTrinh.TabStop = true;
            this.radioDiemQuaTrinh.Text = "Điểm Quá Trình";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.radioDiemQuaTrinh.UseVisualStyleBackColor = true;
            // 
            // radioDiemDauRa
            // 
            this.radioDiemDauRa.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.radioDiemDauRa.AutoSize = true;
            this.radioDiemDauRa.Location = new System.Drawing.Point(70, 31);
            this.radioDiemDauRa.Margin = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.radioDiemDauRa.Name = "radioDiemDauRa";
            this.radioDiemDauRa.Size = new System.Drawing.Size(89, 22);
            this.radioDiemDauRa.TabIndex = 33;
            this.radioDiemDauRa.TabStop = true;
            this.radioDiemDauRa.Text = "Điểm Đầu Ra";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.radioDiemDauRa.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 4;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.33334F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanel5.Controls.Add(this.gbScore, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(489, 0);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 220F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(246, 220);
            this.tableLayoutPanel5.TabIndex = 12;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.AllowDrop = true;
            this.tableLayoutPanel3.AutoScroll = true;
            this.tableLayoutPanel3.AutoSize = true;
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.33334F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanel3.Controls.Add(this.cbbMaKhoa, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.cbbMaLop, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.txtMaDiem, 2, 5);
            this.tableLayoutPanel3.Controls.Add(this.metroLabel6, 1, 5);
            this.tableLayoutPanel3.Controls.Add(this.metroLabel7, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.txtTenHocVien, 2, 4);
            this.tableLayoutPanel3.Controls.Add(this.metroLabel11, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.metroLabel1, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.metroLabel5, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.cbbMaHocVien, 2, 3);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 7;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(396, 195);
            this.tableLayoutPanel3.TabIndex = 10;
            // 
            // cbbMaKhoa
            // 
            this.cbbMaKhoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbMaKhoa.DisplayMember = "Value";
            this.cbbMaKhoa.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbbMaKhoa.FormattingEnabled = true;
            this.cbbMaKhoa.ItemHeight = 19;
            this.cbbMaKhoa.Location = new System.Drawing.Point(124, 20);
            this.cbbMaKhoa.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.cbbMaKhoa.Name = "cbbMaKhoa";
            this.cbbMaKhoa.Size = new System.Drawing.Size(246, 25);
            this.cbbMaKhoa.TabIndex = 46;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.cbbMaKhoa.UseSelectable = true;
            this.cbbMaKhoa.ValueMember = "Key";
            // 
            // cbbMaLop
            // 
            this.cbbMaLop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbMaLop.DisplayMember = "Value";
            this.cbbMaLop.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbbMaLop.FormattingEnabled = true;
            this.cbbMaLop.ItemHeight = 19;
            this.cbbMaLop.Location = new System.Drawing.Point(124, 52);
            this.cbbMaLop.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.cbbMaLop.Name = "cbbMaLop";
            this.cbbMaLop.Size = new System.Drawing.Size(246, 25);
            this.cbbMaLop.TabIndex = 45;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.cbbMaLop.UseSelectable = true;
            this.cbbMaLop.ValueMember = "Key";
            // 
            // txtMaDiem
            // 
            this.txtMaDiem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaDiem.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtMaDiem.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtMaDiem.Location = new System.Drawing.Point(124, 149);
            this.txtMaDiem.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtMaDiem.Name = "txtMaDiem";
            this.txtMaDiem.Size = new System.Drawing.Size(246, 23);
            this.txtMaDiem.TabIndex = 44;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel6.Location = new System.Drawing.Point(24, 145);
            this.metroLabel6.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(90, 32);
            this.metroLabel6.TabIndex = 43;
            this.metroLabel6.Text = "Mã Điểm:";
            this.metroLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel7.Location = new System.Drawing.Point(24, 81);
            this.metroLabel7.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(90, 32);
            this.metroLabel7.TabIndex = 41;
            this.metroLabel7.Text = "Mã Học Viên";
            this.metroLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtTenHocVien
            // 
            this.txtTenHocVien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTenHocVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtTenHocVien.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtTenHocVien.Location = new System.Drawing.Point(124, 117);
            this.txtTenHocVien.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtTenHocVien.Name = "txtTenHocVien";
            this.txtTenHocVien.ReadOnly = true;
            this.txtTenHocVien.Size = new System.Drawing.Size(246, 23);
            this.txtTenHocVien.TabIndex = 40;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel11.Location = new System.Drawing.Point(24, 49);
            this.metroLabel11.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(90, 32);
            this.metroLabel11.TabIndex = 36;
            this.metroLabel11.Text = "Lớp Học:";
            this.metroLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel1.Location = new System.Drawing.Point(24, 17);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(90, 32);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "Khoá Học:";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel5.Location = new System.Drawing.Point(24, 113);
            this.metroLabel5.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(90, 32);
            this.metroLabel5.TabIndex = 22;
            this.metroLabel5.Text = "Tên Học Viên";
            this.metroLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // cbbMaHocVien
            // 
            this.cbbMaHocVien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbMaHocVien.DisplayMember = "Value";
            this.cbbMaHocVien.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbbMaHocVien.FormattingEnabled = true;
            this.cbbMaHocVien.ItemHeight = 19;
            this.cbbMaHocVien.Location = new System.Drawing.Point(124, 84);
            this.cbbMaHocVien.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.cbbMaHocVien.Name = "cbbMaHocVien";
            this.cbbMaHocVien.Size = new System.Drawing.Size(246, 25);
            this.cbbMaHocVien.TabIndex = 32;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.cbbMaHocVien.UseSelectable = true;
            this.cbbMaHocVien.ValueMember = "Key";
            // 
            // gboxSearch
            // 
            this.gboxSearch.Controls.Add(this.tableLayoutPanel3);
            this.gboxSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gboxSearch.Location = new System.Drawing.Point(3, 3);
            this.gboxSearch.Name = "gboxSearch";
            this.gboxSearch.Size = new System.Drawing.Size(402, 214);
            this.gboxSearch.TabIndex = 11;
            this.gboxSearch.TabStop = false;
            this.gboxSearch.Text = "Thông tin tra cứu";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 5;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel4, 5);
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Controls.Add(this.btnRight, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnLeft, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 346);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(741, 36);
            this.tableLayoutPanel4.TabIndex = 24;
            // 
            // btnRight
            // 
            this.btnRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnRight.Location = new System.Drawing.Point(463, 3);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(94, 30);
            this.btnRight.TabIndex = 2;
            this.btnRight.Text = "RIGHT";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnRight.UseSelectable = true;
            // 
            // btnLeft
            // 
            this.btnLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnLeft.Location = new System.Drawing.Point(183, 3);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Size = new System.Drawing.Size(94, 30);
            this.btnLeft.TabIndex = 1;
            this.btnLeft.Text = "LEFT";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnLeft.UseSelectable = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(741, 382);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 55.55556F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Controls.Add(this.gboxSearch, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel5, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 220F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(735, 340);
            this.tableLayoutPanel2.TabIndex = 25;
            // 
            // MetroDetailScore
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "MetroDetailScore";
            this.Size = new System.Drawing.Size(741, 382);
            this.gbScore.ResumeLayout(false);
            this.gbScore.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.gboxSearch.ResumeLayout(false);
            this.gboxSearch.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroControl.Components.MetroTextbox txtDiem;
        private MetroControl.Components.MetroLabel metroLabel3;
        private MetroControl.Components.MetroLabel metroLabel7;
        private MetroControl.Components.MetroTextbox txtTenHocVien;
        private MetroControl.Components.MetroLabel metroLabel4;
        private MetroControl.Components.MetroLabel metroLabel11;
        private MetroControl.Components.MetroLabel metroLabel1;
        private MetroControl.Components.MetroLabel metroLabel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.GroupBox gbScore;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.GroupBox gboxSearch;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private MetroControl.Components.MetroCombobox cbbMaHocVien;
        private MetroControl.Components.MetroButton btnRight;
        private MetroControl.Components.MetroButton btnLeft;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroControl.Components.MetroCombobox cbbMaKhoa;
        private MetroControl.Components.MetroCombobox cbbMaLop;
        private MetroControl.Components.MetroTextbox txtMaDiem;
        private MetroControl.Components.MetroLabel metroLabel6;
        private MetroControl.Components.MetroTextbox txtLan;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private MetroControl.Components.MetroLabel lblLoaiDiem;
        private MetroControl.Components.MetroRadioButton radioDiemQuaTrinh;
        private MetroControl.Components.MetroRadioButton radioDiemDauRa;
    }
}
