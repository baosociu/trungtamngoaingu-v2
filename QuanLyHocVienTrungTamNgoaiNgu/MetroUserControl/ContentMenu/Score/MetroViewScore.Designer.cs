﻿namespace MetroUserControl.ContentMenu.Score
{
    partial class MetroViewScore
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.btnPrint = new MetroControl.Components.MetroButton();
            this.metroLabel1 = new MetroControl.Components.MetroLabel();
            this.cbbLoc = new MetroControl.Components.MetroCombobox();
            this.metroLabel2 = new MetroControl.Components.MetroLabel();
            this.txtTimKiem = new MetroControl.Components.MetroTextbox();
            this.picSearch = new MetroControl.Components.MetroPicture();
            this.gridPage = new MetroControl.Controls.MetroGridViewPage();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSearch)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.ColumnCount = 6;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.45455F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.09091F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.45455F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.cbbLoc, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel2, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtTimKiem, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.picSearch, 5, 0);
            this.tableLayoutPanel2.Controls.Add(this.gridPage, 0, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 5;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545455F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90.90909F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545455F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(724, 375);
            this.tableLayoutPanel2.TabIndex = 4;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 7;
            this.tableLayoutPanel2.SetColumnSpan(this.tableLayoutPanel4, 6);
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.Controls.Add(this.btnPrint, 3, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 337);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(724, 38);
            this.tableLayoutPanel4.TabIndex = 24;
            // 
            // btnPrint
            // 
            this.btnPrint.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPrint.Location = new System.Drawing.Point(315, 3);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(94, 32);
            this.btnPrint.TabIndex = 1;
            this.btnPrint.Text = "IN BẢNG ĐIỂM";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnPrint.UseSelectable = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel1.Location = new System.Drawing.Point(3, 0);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(87, 32);
            this.metroLabel1.TabIndex = 25;
            this.metroLabel1.Text = "Lọc:";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // cbbLoc
            // 
            this.cbbLoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbLoc.DisplayMember = "Value";
            this.cbbLoc.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbbLoc.FormattingEnabled = true;
            this.cbbLoc.ItemHeight = 19;
            this.cbbLoc.Items.AddRange(new object[] {
            "Toàn Bộ",
            "Trình Độ"});
            this.cbbLoc.Location = new System.Drawing.Point(100, 3);
            this.cbbLoc.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.cbbLoc.Name = "cbbLoc";
            this.cbbLoc.Size = new System.Drawing.Size(220, 25);
            this.cbbLoc.TabIndex = 0;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.cbbLoc.UseSelectable = true;
            this.cbbLoc.ValueMember = "Key";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel2.Location = new System.Drawing.Point(367, 0);
            this.metroLabel2.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(87, 32);
            this.metroLabel2.TabIndex = 27;
            this.metroLabel2.Text = "Tìm Kiếm:";
            this.metroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtTimKiem
            // 
            this.txtTimKiem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTimKiem.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtTimKiem.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtTimKiem.Location = new System.Drawing.Point(464, 4);
            this.txtTimKiem.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtTimKiem.Name = "txtTimKiem";
            this.txtTimKiem.Size = new System.Drawing.Size(220, 23);
            this.txtTimKiem.TabIndex = 28;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // picSearch
            // 
            this.picSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picSearch.Image = global::MetroUserControl.Properties.Resources.magnifying_glass;
            this.picSearch.Location = new System.Drawing.Point(687, 3);
            this.picSearch.Name = "picSearch";
            this.picSearch.Size = new System.Drawing.Size(34, 26);
            this.picSearch.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picSearch.TabIndex = 29;
            this.picSearch.TabStop = false;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // gridPage
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.gridPage, 6);
            this.gridPage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridPage.Location = new System.Drawing.Point(3, 48);
            this.gridPage.Name = "gridPage";
            this.gridPage.Size = new System.Drawing.Size(718, 273);
            this.gridPage.TabIndex = 30;
            // 
            // MetroViewScore
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel2);
            this.Name = "MetroViewScore";
            this.Size = new System.Drawing.Size(724, 375);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picSearch)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private MetroControl.Components.MetroButton btnPrint;
        private MetroControl.Components.MetroLabel metroLabel1;
        private MetroControl.Components.MetroLabel metroLabel2;
        private MetroControl.Components.MetroPicture picSearch;
        internal MetroControl.Controls.MetroGridViewPage gridPage;
        internal MetroControl.Components.MetroCombobox cbbLoc;
        internal MetroControl.Components.MetroTextbox txtTimKiem;
    }
}
