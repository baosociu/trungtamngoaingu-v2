﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroControl;
using MetroUserControl.ContentMenu.Mail;
using Export;
using MetroUserControl.ContentMenu.Export;
using MetroDataset;
using static MetroControl.MetroHelper;

namespace MetroUserControl.ContentMenu.Score
{
    public partial class MetroListScore : MetroBubble
    {
        public MetroListScore()
        {
            InitializeComponent();
            this.Load += MetroListScore_Load;
            this.VisibleChanged += MetroListScore_VisibleChanged;
        }

        private void MetroListScore_VisibleChanged(object sender, EventArgs e)
        {
            this.SettingLoadByPageFilterSearch(this.gridDiem, (new LoadByPage()).HienThiDiem, this.cbbLoc, (new Select()).HienThiFilterDiem_Cbb, this.picSearch, this.txtTimKiem);

        }

        private void MetroListScore_Load(object sender, EventArgs e)
        {
            this.gridDiem.SetOnClick(() => { (this as MetroBubble).ReloadData(this.gridDiem.grid, false); });
            this.btnXemChiTiet.Click += (s, en) =>
            {
                DataGridViewRow r = this.gridDiem.grid.CurrentRow;
                string madangky = r.Cells[0].Value.ToString();
                string madiem = r.Cells[1].Value.ToString();
                string mahocvien = r.Cells[5].Value.ToString();
                string malop = r.Cells[2].Value.ToString();
                MetroHelper.OpenNewTab(this, new MetroDetailScore().SetAsDetail(madiem, madangky, mahocvien, malop));
            };
            this.btnNhapDiem.Click += (s, en) => { MetroHelper.OpenNewTab(this, new MetroDetailScore().SetAsCreate()); };
            EventHelper.Helper.SetDoubleClickItemGridView(this.gridDiem.grid, btnXemChiTiet.PerformClick);
            this.gridDiem.grid.ContextMenuStrip = this.contextMenuStrip1;
            this.gửiMailChoHọcViênNàyToolStripMenuItem.Click += GửiMailChoHọcViênNàyToolStripMenuItem_Click;
            this.btnInDanhSach.Click += BtnInDanhSach_Click;

            this.btnNhapDiem.TypeData.Add(MetroControl.Components.Type.Insert);
           
            this.VisibleByPermission();
        }

        private void BtnInDanhSach_Click(object sender, EventArgs e)
        {

            IndexAndKey i = MetroHelper.GetIndexAndKeyColumnFilter(cbbLoc.SelectedValue.ToString());

            BieuMau.InDanhSachTatCaDiem(i.Index, i.Key, txtTimKiem.Text);
        }

        private void GửiMailChoHọcViênNàyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string ma_hocvien = gridDiem.grid.CurrentRow.Cells[5].Value.ToString();
            List<object> tb = (new Select()).HienThiMailNguoiDung(ma_hocvien);
            MetroHelper.OpenNewTab(this, new MetroMail().SetDataReceiver(tb));
        }
    }
}
