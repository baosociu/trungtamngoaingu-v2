﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroControl.Components;
using System.Reflection;
using MetroDataset;
using MetroControl;

namespace MetroUserControl.ContentMenu.Score
{
    public partial class MetroDetailScore : MetroBubble
    {
        private DIEM _diem;
        public MetroDetailScore()
        {
            InitializeComponent();
            this.Load += MetroDetailScore_Load;
        }

        private void MetroDetailScore_Load(object sender, EventArgs e)
        {
            btnRight.Text = "Hủy";
            btnRight.Click += (s, en) => { this.ParentForm.Close(); };

            //radio change
            radioDiemDauRa.CheckedChanged += (s, en) => ThayDoiLoaiDiem();
            radioDiemQuaTrinh.CheckedChanged += (s, en) => ThayDoiLoaiDiem();
        }


        private void ThayDoiLoaiDiem()
        {
            object tag = radioDiemQuaTrinh.Tag;
            if (radioDiemDauRa.Checked)
                tag = radioDiemDauRa.Tag;

            object madiem = txtMaDiem.Text;
            object malop = cbbMaLop.SelectedValue;
            object mahocvien = cbbMaHocVien.SelectedValue;

            if (tag == null || madiem == null || malop == null || mahocvien == null)
                return;
            txtLan.Text = (new Select()).TaoMaLanDiemTuDong(madiem.ToString(), mahocvien.ToString(), malop.ToString(), tag.ToString());
        }

        #region SelectedValueChanged

        private void ChonLopHoc(object malop)
        {
            if (malop == null)
                return;
            //chọn lớp học:
            //+ cập nhật cbbHocVien cua lớp đó

            LoadCbbHocVien(malop.ToString());
            if (cbbMaHocVien.Items.Count != 0)
                cbbMaHocVien.SelectedItem = cbbMaHocVien.Items[0];
            else
            {
                txtMaDiem.Text = "";
                VisibilityRight(false);
            }

        }

        private void ChonKhoaHoc(object makhoa)
        {
            if (makhoa == null)
                return;
            //chọn khoá học:
            //+ cập nhật cbbLop
            LoadCbbLopHoc(makhoa.ToString());
            if (cbbMaLop.Items.Count != 0)
                cbbMaLop.SelectedItem = cbbMaLop.Items[0];
            else
            {
                cbbMaHocVien.DataSource = null;
                cbbMaHocVien.Items.Clear();

                txtMaDiem.Text = "";

                txtTenHocVien.Clear();
                VisibilityRight(false);
            }
        }

        private void VisibilityRight(bool visible)
        {
            txtDiem.Clear();
            txtLan.Clear();

            txtDiem.Enabled = visible;
            txtLan.Enabled = visible;
            radioDiemDauRa.Enabled = visible;
            radioDiemQuaTrinh.Enabled = visible;

            btnLeft.Visible = visible;

            //radioDiemDauRa.Checked = false;
            //radioDiemDauVao.Checked = false;
            //radioDiemQuaTrinh.Checked = false;
        }

        #endregion

        #region Load Combobox
        private string LoadRight(string madiem, string madangky, string malophoc)
        {
            string makhoa = "";
            object[] data = (new MetroDataset.Select()).HienThiThongTinDiem(madiem, madangky, malophoc);
            if (data != null)
            {
                EventHelper.Helper.TryCatch(() =>
                {
                    _diem = (DIEM)data[0];
                    HOCVIEN hocvien = (HOCVIEN)data[1];
                    KHOAHOC khoahoc = (KHOAHOC)data[3];
                    LOAIDIEM loaidiem = (LOAIDIEM)data[4];

                    txtDiem.Text = _diem.DIEM1.ToString();
                    txtLan.Text = _diem.LAN.ToString();
                    makhoa = khoahoc.MAKHOAHOC;
                    txtTenHocVien.Text = hocvien.TENHOCVIEN;
                    string maloaidiem = loaidiem.MALOAIDIEM;
                    switch (maloaidiem)
                    {
                        case "LD1": radioDiemQuaTrinh.Checked = true; break;
                        case "LD2": radioDiemDauRa.Checked = true; break;
                    }
                    radioDiemQuaTrinh.Tag = "LD1";
                    radioDiemDauRa.Tag = "LD2";
                });
            }
            return makhoa;
        }

        private void LoadCbbKhoa()
        {
            List<object> data = (new MetroDataset.Select()).HienThiTatCaKhoaHoc_Cbb();
            if (data != null && data.Count != 0)
                cbbMaKhoa.DataSource = data;
        }



        private void LoadCbbHocVien(string malophoc)
        {
            cbbMaHocVien.DataSource = null;
            cbbMaHocVien.Items.Clear();

            var c = (new MetroDataset.Select()).HienThiHocVienLopHoc_Cbb(malophoc);
            if (c != null && c.Count != 0)
                cbbMaHocVien.DataSource = c;

        }

        private void LoadCbbLopHoc(string makhoa)
        {
            cbbMaLop.DataSource = null;
            cbbMaLop.Items.Clear();
            var d = (new MetroDataset.Select()).HienThiLopHocKhoaHoc_Cbb(makhoa);
            if (d != null && d.Count != 0)
                cbbMaLop.DataSource = d;
        }
        #endregion

        public MetroDetailScore SetAsCreate()
        {
            btnLeft.Text = "Thêm";
            txtMaDiem.Enabled = false;

            //load tất cả khoá học
            LoadCbbKhoa();
            if (cbbMaKhoa.Items.Count != 0)
                cbbMaKhoa.SelectedItem = cbbMaKhoa.Items[0];

            //loà tất cả lớp học
            if (cbbMaKhoa.SelectedValue != null)
            {
                LoadCbbLopHoc(cbbMaKhoa.SelectedValue.ToString());
                if (cbbMaLop.Items.Count != 0)
                    cbbMaLop.SelectedItem = cbbMaLop.Items[0];
            }

            //load tất cả học viên của lớp học
            if (cbbMaLop.SelectedValue != null)
            {
                LoadCbbHocVien(cbbMaLop.SelectedValue.ToString());
                if (cbbMaHocVien.Items.Count != 0)
                    cbbMaHocVien.SelectedItem = cbbMaLop.Items[0];
            }


            radioDiemQuaTrinh.Tag = "LD1";
            radioDiemDauRa.Tag = "LD2";
            radioDiemQuaTrinh.Checked = true;

            //tạo ma điểm tự động
            if (cbbMaLop.SelectedValue != null && cbbMaHocVien.SelectedValue != null)
                TaoMaDiemTuDong(cbbMaLop.SelectedValue.ToString(), cbbMaHocVien.SelectedValue.ToString());

            if (txtMaDiem.Text != "")
                ThayDoiLoaiDiem();

            btnLeft.Click += (s, en) => TaoDiemMoi();

            //changed selection cbb
            cbbMaKhoa.SelectedValueChanged += (s, en) => ChonKhoaHoc(cbbMaKhoa.SelectedValue);
            cbbMaLop.SelectedValueChanged += (s, en) => ChonLopHoc(cbbMaLop.SelectedValue);
            cbbMaHocVien.SelectedValueChanged += (s, en) => TaoMaDiemTuDong(cbbMaLop.SelectedValue, cbbMaHocVien.SelectedValue);
            return this;
        }

        private void TaoMaDiemTuDong(object malop, object madangkykhoahoc)
        {
            if (malop == null || madangkykhoahoc == null)
                return;
            
            txtMaDiem.Text = (new Select()).TaoMaDiemTuDong(malop.ToString(),madangkykhoahoc.ToString());
            VisibilityRight(true);
            //txtTenHocVien.Text = ((DataSetQuanLyHocVien.HienThiHocVienLopHocRow)((DataRowView)cbbMaHocVien.SelectedItem).Row).TÊN_HỌC_VIÊN;
            txtTenHocVien.Text = (new MetroDataset.Select()).GetStudentByMaDangKy(madangkykhoahoc.ToString()).TENHOCVIEN;
            ThayDoiLoaiDiem();
        }

        public MetroDetailScore SetAsDetail(string madiem, string madangky, string mahocvien, string malophoc)
        {
            string makhoa = "";
            btnLeft.Text = "Cập nhật";
            txtMaDiem.Enabled = false;
            cbbMaLop.Enabled = false;
            cbbMaHocVien.Enabled = false;
            cbbMaKhoa.Enabled = false;
            radioDiemDauRa.Enabled = false;
            radioDiemQuaTrinh.Enabled = false;
            //load thông tin bên phải
            makhoa = LoadRight(madiem, madangky, malophoc);

            //load tất cả khoá học
            LoadCbbKhoa();

            cbbMaKhoa.SelectedValue = makhoa;

            //load lớp học của khoá học
            LoadCbbLopHoc(makhoa);

            cbbMaLop.Text = malophoc;

            //load học viên của lớp học
            LoadCbbHocVien(malophoc);

            cbbMaHocVien.Text = mahocvien;

            txtMaDiem.Text = madiem;

            btnLeft.Click += (s, en) => CapNhatDiem();

            return this;
        }

        private void CapNhatDiem()
        {
            object madiem = txtMaDiem.Text;
            object madangkykhoahoc = cbbMaHocVien.SelectedValue;
            object malophoc = cbbMaLop.SelectedValue;
            object lan = txtLan.Text;
            object diem = txtDiem.Text;
            object loaidiem = radioDiemDauRa.Tag;
            if (radioDiemQuaTrinh.Checked)
                loaidiem = radioDiemQuaTrinh.Tag;
            if (MetroHelper.IsNullOrEmpty(new[] { madiem, madangkykhoahoc, malophoc, lan, diem, loaidiem }))
            {
                EventHelper.Helper.MessageLessInformation();
                return;
            }

            EventHelper.Helper.TryCatch(() => {
            DIEM d = new DIEM()
            {
                MADIEM = madiem.ToString(),
                MADANGKYKHOAHOC = madangkykhoahoc.ToString(),
                MALOPHOC = malophoc.ToString(),
                MALOAIDIEM = loaidiem.ToString(),
                LAN = int.Parse(lan.ToString()),
                DIEM1 = double.Parse(diem.ToString())
            };

                //kiểm tra thay đổi
                if (d.MALOAIDIEM == _diem.MALOAIDIEM && d.DIEM1 == _diem.DIEM1)
                {
                    EventHelper.Helper.MessageNoChangedInformation();
                    return;
                }

                ResultExcuteQuery r = (new MetroDataset.Update()).CapNhatDiemHocVienLopHoc(d);
            EventHelper.Helper.MessageInfomation(r.GetMessage());
            if (r.isSuccessfully())
                _diem = d;

            });
        }


        private void TaoDiemMoi()
        {
            object madiem = txtMaDiem.Text;
            object madangkykhoahoc = cbbMaHocVien.SelectedValue;
            object malophoc = cbbMaLop.SelectedValue;
            object lan = txtLan.Text;
            object diem = txtDiem.Text;
            object loaidiem = radioDiemDauRa.Tag;
            if (radioDiemQuaTrinh.Checked)
                loaidiem = radioDiemQuaTrinh.Tag;
            if (MetroHelper.IsNullOrEmpty(new[] { madiem, madangkykhoahoc, malophoc, lan, diem, loaidiem }))
            {
                EventHelper.Helper.MessageLessInformation();
                return;
            }


            EventHelper.Helper.TryCatch(() =>
            {
                string MADIEM = madiem.ToString();
                string MADANGKYKHOAHOC = madangkykhoahoc.ToString();
                string MALOPHOC = malophoc.ToString();
                string MALOAIDIEM = loaidiem.ToString();
                int LAN = int.Parse(lan.ToString());
                double DIEM1 = double.Parse(diem.ToString());
                
                //CHECK rỗng
                if(MetroHelper.IsNullOrEmpty(new object[] { MADIEM, MADANGKYKHOAHOC, MALOPHOC, MALOAIDIEM, LAN, DIEM1}))
                {
                    EventHelper.Helper.MessageLessInformation();
                    return;
                }

                DIEM d = new DIEM()
                {
                    MADIEM = MADIEM,
                    MADANGKYKHOAHOC = MADANGKYKHOAHOC,
                    MALOPHOC = MALOPHOC,
                    MALOAIDIEM = MALOAIDIEM,
                    LAN = LAN,
                    DIEM1 = DIEM1
                };

                ResultExcuteQuery r = (new MetroDataset.Insert()).ThemDiemMoi(d);
                EventHelper.Helper.MessageInfomation(r.GetMessage());
                if (r.isSuccessfully())
                    _diem = d;

                TaoMaDiemTuDong(malophoc, madangkykhoahoc);

            }, () => { });

        }

    }
}
