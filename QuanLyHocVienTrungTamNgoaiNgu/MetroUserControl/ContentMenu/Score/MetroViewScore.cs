﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroControl;
using MetroDataset;
using System.Threading;
using static MetroControl.MetroHelper;

namespace MetroUserControl.ContentMenu.Score
{
    public partial class MetroViewScore : MetroBubble
    {
        string mahv = MetroHelper.iduser;
        public MetroViewScore()
        {

            InitializeComponent();
            this.VisibleChanged += MetroViewScore_VisibleChanged;
            this.btnPrint.Click += (s, ev) => InBangDiem(mahv);
        }

        private void InBangDiem(string mahocvien)
        {
            EventHelper.Helper.TryCatch(() => {
                IndexAndKey i = MetroHelper.GetIndexAndKeyColumnFilter(cbbLoc.SelectedValue.ToString());

                BieuMau.InDanhSachTatCaDiemHocVien(i.Index, i.Key, txtTimKiem.Text);
            });

        }

        private void MetroViewScore_VisibleChanged(object sender, EventArgs e)
        {
            this.SettingLoadByPageFilterSearch(this.gridPage, (new LoadByPage()).HienThiDiemHocVien, this.cbbLoc, (new Select()).HienThiFilterDiemHocVien_Cbb, this.picSearch, txtTimKiem);

        }

    }
}
