﻿namespace MetroUserControl.ContentMenu.Summary
{
    partial class MetroCentralSummary
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelMain = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.dateTo = new MetroControl.Components.MetroDatePicker();
            this.lblDenNgay = new MetroControl.Components.MetroLabel();
            this.dateFrom = new MetroControl.Components.MetroDatePicker();
            this.lblTuNgay = new MetroControl.Components.MetroLabel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.btnXuatReport = new MetroControl.Components.MetroButton();
            this.groupDetail = new System.Windows.Forms.GroupBox();
            this.gridDetail = new MetroControl.Controls.MetroGridViewPage();
            this.panelMain.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.groupDetail.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelMain
            // 
            this.panelMain.BackColor = System.Drawing.Color.Transparent;
            this.panelMain.ColumnCount = 6;
            this.panelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.panelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.panelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.panelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.panelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.panelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.panelMain.Controls.Add(this.tableLayoutPanel1, 0, 0);
            this.panelMain.Controls.Add(this.tableLayoutPanel4, 0, 4);
            this.panelMain.Controls.Add(this.groupDetail, 0, 2);
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(0, 0);
            this.panelMain.Name = "panelMain";
            this.panelMain.RowCount = 5;
            this.panelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.panelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545455F));
            this.panelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90.90909F));
            this.panelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545455F));
            this.panelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.panelMain.Size = new System.Drawing.Size(789, 339);
            this.panelMain.TabIndex = 2;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.panelMain.SetColumnSpan(this.tableLayoutPanel1, 6);
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.78049F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.439024F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.78049F));
            this.tableLayoutPanel1.Controls.Add(this.dateTo, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblDenNgay, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.dateFrom, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblTuNgay, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(789, 30);
            this.tableLayoutPanel1.TabIndex = 39;
            // 
            // dateTo
            // 
            this.dateTo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.dateTo.FontSize = MetroFramework.MetroDateTimeSize.Small;
            this.dateTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTo.Location = new System.Drawing.Point(481, 3);
            this.dateTo.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.dateTo.MinimumSize = new System.Drawing.Size(0, 25);
            this.dateTo.Name = "dateTo";
            this.dateTo.Size = new System.Drawing.Size(308, 25);
            this.dateTo.TabIndex = 38;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lblDenNgay
            // 
            this.lblDenNgay.AutoSize = true;
            this.lblDenNgay.BackColor = System.Drawing.Color.Transparent;
            this.lblDenNgay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDenNgay.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblDenNgay.Location = new System.Drawing.Point(404, 0);
            this.lblDenNgay.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.lblDenNgay.Name = "lblDenNgay";
            this.lblDenNgay.Size = new System.Drawing.Size(67, 29);
            this.lblDenNgay.TabIndex = 37;
            this.lblDenNgay.Text = "Đến Ngày:";
            this.lblDenNgay.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // dateFrom
            // 
            this.dateFrom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.dateFrom.FontSize = MetroFramework.MetroDateTimeSize.Small;
            this.dateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateFrom.Location = new System.Drawing.Point(80, 3);
            this.dateFrom.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.dateFrom.MinimumSize = new System.Drawing.Size(0, 25);
            this.dateFrom.Name = "dateFrom";
            this.dateFrom.Size = new System.Drawing.Size(306, 25);
            this.dateFrom.TabIndex = 32;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lblTuNgay
            // 
            this.lblTuNgay.AutoSize = true;
            this.lblTuNgay.BackColor = System.Drawing.Color.Transparent;
            this.lblTuNgay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTuNgay.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblTuNgay.Location = new System.Drawing.Point(3, 0);
            this.lblTuNgay.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.lblTuNgay.Name = "lblTuNgay";
            this.lblTuNgay.Size = new System.Drawing.Size(67, 29);
            this.lblTuNgay.TabIndex = 26;
            this.lblTuNgay.Text = "Từ Ngày:";
            this.lblTuNgay.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 3;
            this.panelMain.SetColumnSpan(this.tableLayoutPanel4, 6);
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Controls.Add(this.btnXuatReport, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 302);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(789, 37);
            this.tableLayoutPanel4.TabIndex = 24;
            // 
            // btnXuatReport
            // 
            this.btnXuatReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnXuatReport.Location = new System.Drawing.Point(349, 5);
            this.btnXuatReport.Margin = new System.Windows.Forms.Padding(5);
            this.btnXuatReport.Name = "btnXuatReport";
            this.btnXuatReport.Size = new System.Drawing.Size(90, 27);
            this.btnXuatReport.TabIndex = 0;
            this.btnXuatReport.Text = "LẬP BÁO CÁO";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnXuatReport.UseSelectable = true;
            // 
            // groupDetail
            // 
            this.panelMain.SetColumnSpan(this.groupDetail, 6);
            this.groupDetail.Controls.Add(this.gridDetail);
            this.groupDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupDetail.Location = new System.Drawing.Point(0, 42);
            this.groupDetail.Margin = new System.Windows.Forms.Padding(0);
            this.groupDetail.Name = "groupDetail";
            this.groupDetail.Padding = new System.Windows.Forms.Padding(0);
            this.groupDetail.Size = new System.Drawing.Size(789, 248);
            this.groupDetail.TabIndex = 38;
            this.groupDetail.TabStop = false;
            this.groupDetail.Text = "groupBox1";
            // 
            // gridDetail
            // 
            this.gridDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridDetail.Location = new System.Drawing.Point(0, 13);
            this.gridDetail.Name = "gridDetail";
            this.gridDetail.Size = new System.Drawing.Size(789, 235);
            this.gridDetail.TabIndex = 34;
            // 
            // MetroCentralSummary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelMain);
            this.Name = "MetroCentralSummary";
            this.Size = new System.Drawing.Size(789, 339);
            this.panelMain.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.groupDetail.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel panelMain;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private MetroControl.Components.MetroButton btnXuatReport;
        private System.Windows.Forms.GroupBox groupDetail;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroControl.Components.MetroDatePicker dateTo;
        private MetroControl.Components.MetroLabel lblDenNgay;
        private MetroControl.Components.MetroDatePicker dateFrom;
        private MetroControl.Components.MetroLabel lblTuNgay;
        internal MetroControl.Controls.MetroGridViewPage gridDetail;
    }
}
