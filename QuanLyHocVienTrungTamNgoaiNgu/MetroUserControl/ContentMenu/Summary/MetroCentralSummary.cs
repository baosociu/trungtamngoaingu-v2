﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroDataset;

namespace MetroUserControl.ContentMenu.Summary
{
    public partial class MetroCentralSummary : MetroBubble
    {
        public enum Type
        {
            None,
            DoanhThu,
            TongDoanhThu,
        }
        Type type;
        public MetroCentralSummary()
        {
            InitializeComponent();
            this.Load += MetroCentralSummary_Load;
            this.btnXuatReport.Click += BtnXuatReport_Click;
        }

        private void BtnXuatReport_Click(object sender, EventArgs e)
        {
            if (type == Type.DoanhThu)
                BieuMau.InDoanhThuTrungTam(dateFrom.Value, dateTo.Value);
            else if (type == Type.TongDoanhThu)
                BieuMau.InTongDoanhThuTrungTam(dateFrom.Value, dateTo.Value);

        }

        private void MetroCentralSummary_Load(object sender, EventArgs e)
        {
            this.dateFrom.Value = this.dateFrom.Value.AddMonths(-3);
            this.dateTo.Value = this.dateFrom.Value.AddMonths(3);
        }

        public MetroCentralSummary SetAs(Type type)
        {
            if (type == this.type)
                return this;
            this.type = type;

            if (type == Type.DoanhThu)
                setupDoanhThu();
            else if (type == Type.TongDoanhThu)
                setupTongDoanhThu();

            return this;
        }

        private void setupTongDoanhThu()
        {
            groupDetail.Text = "Chi tiết tổng doanh thu của trung tâm";
            //load gridview

            funcData = new Func<object>(() =>
            {
                int page = gridDetail.PageNumber;
                int count = gridDetail.NumberRow;
                return (new LoadByPage()).TongDoanhThuTheoTrungTam_Grid("", dateFrom.Value, dateTo.Value, page, count);//hàm chính
            });
            this.ReloadData(gridDetail.grid, false);
            gridDetail.SetOnClick(() => { (this as MetroBubble).ReloadData(gridDetail.grid, false); });

            this.dateFrom.ValueChanged += DateChanged;
            this.dateTo.ValueChanged += DateChanged;

            this.gridDetail.AlignColumn(0, DataGridViewContentAlignment.MiddleCenter);
            this.gridDetail.AlignColumn(2, DataGridViewContentAlignment.MiddleCenter);
            this.gridDetail.AlignColumn(3, DataGridViewContentAlignment.MiddleCenter);
            this.gridDetail.AlignColumn(4, DataGridViewContentAlignment.MiddleCenter);
            this.gridDetail.AlignColumn(5, DataGridViewContentAlignment.MiddleRight);
            this.gridDetail.AlignColumn(6, DataGridViewContentAlignment.MiddleRight);
            this.gridDetail.AlignColumn(7, DataGridViewContentAlignment.MiddleRight);
        }

        private void setupDoanhThu()
        {
            groupDetail.Text = "Chi tiết doanh thu của trung tâm";
            //load gridview

            funcData = new Func<object>(() =>
            {
                int page = gridDetail.PageNumber;
                int count = gridDetail.NumberRow;
                return (new LoadByPage()).DoanhThuTheoTrungTam_Grid("", dateFrom.Value, dateTo.Value, page, count);//hàm chính
            });
            this.ReloadData(gridDetail.grid, false);
            gridDetail.SetOnClick(() => { (this as MetroBubble).ReloadData(gridDetail.grid, false); });

            this.dateFrom.ValueChanged += DateChanged;
            this.dateTo.ValueChanged += DateChanged;


            this.gridDetail.AlignColumn(0, DataGridViewContentAlignment.MiddleCenter);
            this.gridDetail.AlignColumn(1, DataGridViewContentAlignment.MiddleCenter);
            this.gridDetail.AlignColumn(2, DataGridViewContentAlignment.MiddleCenter);
            this.gridDetail.AlignColumn(4, DataGridViewContentAlignment.MiddleCenter);
            this.gridDetail.AlignColumn(5, DataGridViewContentAlignment.MiddleCenter);
            this.gridDetail.AlignColumn(7, DataGridViewContentAlignment.MiddleCenter);
            this.gridDetail.AlignColumn(9, DataGridViewContentAlignment.MiddleCenter);
            this.gridDetail.AlignColumn(11, DataGridViewContentAlignment.MiddleCenter);
            this.gridDetail.AlignColumn(12, DataGridViewContentAlignment.MiddleCenter);
            this.gridDetail.AlignColumn(13, DataGridViewContentAlignment.MiddleCenter);

            this.gridDetail.AlignColumn(14, DataGridViewContentAlignment.MiddleRight);
        }

        private void DateChanged(object sender, EventArgs e)
        {
            this.ReloadData(gridDetail.grid, false);
        }
    }
}
