﻿namespace MetroUserControl.ContentMenu.Summary
{
    partial class MetroListSummary
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupParent = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.gridParent = new MetroControl.Controls.MetroGridViewPage();
            this.lblFilter = new MetroControl.Components.MetroLabel();
            this.lblSearch = new MetroControl.Components.MetroLabel();
            this.dateTo = new MetroControl.Components.MetroDatePicker();
            this.lblDenNgay = new MetroControl.Components.MetroLabel();
            this.dateFrom = new MetroControl.Components.MetroDatePicker();
            this.lblTuNgay = new MetroControl.Components.MetroLabel();
            this.cbbFilter = new MetroControl.Components.MetroCombobox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.picSearch = new MetroControl.Components.MetroPicture();
            this.txtSearch = new MetroControl.Components.MetroTextbox();
            this.groupDetail = new System.Windows.Forms.GroupBox();
            this.gridDetail = new MetroControl.Controls.MetroGridViewPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.btnXuatReport = new MetroControl.Components.MetroButton();
            this.panelMain = new System.Windows.Forms.TableLayoutPanel();
            this.groupParent.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSearch)).BeginInit();
            this.groupDetail.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.panelMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupParent
            // 
            this.panelMain.SetColumnSpan(this.groupParent, 6);
            this.groupParent.Controls.Add(this.tableLayoutPanel1);
            this.groupParent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupParent.Location = new System.Drawing.Point(0, 0);
            this.groupParent.Margin = new System.Windows.Forms.Padding(0);
            this.groupParent.Name = "groupParent";
            this.groupParent.Padding = new System.Windows.Forms.Padding(0);
            this.groupParent.Size = new System.Drawing.Size(726, 160);
            this.groupParent.TabIndex = 40;
            this.groupParent.TabStop = false;
            this.groupParent.Text = "groupBox1";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.gridParent, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblFilter, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblSearch, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.dateTo, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblDenNgay, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.dateFrom, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblTuNgay, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.cbbFilter, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 13);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(726, 147);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // gridParent
            // 
            this.gridParent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridParent.Location = new System.Drawing.Point(260, 0);
            this.gridParent.Margin = new System.Windows.Forms.Padding(0);
            this.gridParent.Name = "gridParent";
            this.tableLayoutPanel1.SetRowSpan(this.gridParent, 5);
            this.gridParent.Size = new System.Drawing.Size(466, 147);
            this.gridParent.TabIndex = 49;
            // 
            // lblFilter
            // 
            this.lblFilter.AutoSize = true;
            this.lblFilter.BackColor = System.Drawing.Color.Transparent;
            this.lblFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblFilter.Location = new System.Drawing.Point(3, 0);
            this.lblFilter.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.lblFilter.Name = "lblFilter";
            this.lblFilter.Size = new System.Drawing.Size(67, 29);
            this.lblFilter.TabIndex = 44;
            this.lblFilter.Text = "Lọc:";
            this.lblFilter.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lblSearch
            // 
            this.lblSearch.AutoSize = true;
            this.lblSearch.BackColor = System.Drawing.Color.Transparent;
            this.lblSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblSearch.Location = new System.Drawing.Point(3, 29);
            this.lblSearch.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.lblSearch.Name = "lblSearch";
            this.lblSearch.Size = new System.Drawing.Size(67, 29);
            this.lblSearch.TabIndex = 43;
            this.lblSearch.Text = "Tìm kiếm:";
            this.lblSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // dateTo
            // 
            this.dateTo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.dateTo.FontSize = MetroFramework.MetroDateTimeSize.Small;
            this.dateTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTo.Location = new System.Drawing.Point(80, 90);
            this.dateTo.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.dateTo.MinimumSize = new System.Drawing.Size(0, 25);
            this.dateTo.Name = "dateTo";
            this.dateTo.Size = new System.Drawing.Size(160, 25);
            this.dateTo.TabIndex = 38;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lblDenNgay
            // 
            this.lblDenNgay.AutoSize = true;
            this.lblDenNgay.BackColor = System.Drawing.Color.Transparent;
            this.lblDenNgay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDenNgay.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblDenNgay.Location = new System.Drawing.Point(3, 87);
            this.lblDenNgay.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.lblDenNgay.Name = "lblDenNgay";
            this.lblDenNgay.Size = new System.Drawing.Size(67, 29);
            this.lblDenNgay.TabIndex = 37;
            this.lblDenNgay.Text = "Đến Ngày:";
            this.lblDenNgay.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // dateFrom
            // 
            this.dateFrom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.dateFrom.FontSize = MetroFramework.MetroDateTimeSize.Small;
            this.dateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateFrom.Location = new System.Drawing.Point(80, 61);
            this.dateFrom.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.dateFrom.MinimumSize = new System.Drawing.Size(0, 25);
            this.dateFrom.Name = "dateFrom";
            this.dateFrom.Size = new System.Drawing.Size(160, 25);
            this.dateFrom.TabIndex = 32;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lblTuNgay
            // 
            this.lblTuNgay.AutoSize = true;
            this.lblTuNgay.BackColor = System.Drawing.Color.Transparent;
            this.lblTuNgay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTuNgay.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblTuNgay.Location = new System.Drawing.Point(3, 58);
            this.lblTuNgay.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.lblTuNgay.Name = "lblTuNgay";
            this.lblTuNgay.Size = new System.Drawing.Size(67, 29);
            this.lblTuNgay.TabIndex = 26;
            this.lblTuNgay.Text = "Từ Ngày:";
            this.lblTuNgay.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // cbbFilter
            // 
            this.cbbFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbFilter.DisplayMember = "Value";
            this.cbbFilter.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbbFilter.FormattingEnabled = true;
            this.cbbFilter.ItemHeight = 19;
            this.cbbFilter.Location = new System.Drawing.Point(80, 3);
            this.cbbFilter.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.cbbFilter.Name = "cbbFilter";
            this.cbbFilter.Size = new System.Drawing.Size(160, 25);
            this.cbbFilter.TabIndex = 47;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.cbbFilter.UseSelectable = true;
            this.cbbFilter.ValueMember = "Key";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel3.Controls.Add(this.picSearch, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtSearch, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(80, 29);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(160, 29);
            this.tableLayoutPanel3.TabIndex = 48;
            // 
            // picSearch
            // 
            this.picSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picSearch.Image = global::MetroUserControl.Properties.Resources.magnifying_glass;
            this.picSearch.Location = new System.Drawing.Point(134, 3);
            this.picSearch.Name = "picSearch";
            this.picSearch.Size = new System.Drawing.Size(23, 23);
            this.picSearch.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picSearch.TabIndex = 48;
            this.picSearch.TabStop = false;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtSearch
            // 
            this.txtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtSearch.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtSearch.Location = new System.Drawing.Point(0, 3);
            this.txtSearch.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(131, 23);
            this.txtSearch.TabIndex = 47;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // groupDetail
            // 
            this.panelMain.SetColumnSpan(this.groupDetail, 6);
            this.groupDetail.Controls.Add(this.gridDetail);
            this.groupDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupDetail.Location = new System.Drawing.Point(0, 167);
            this.groupDetail.Margin = new System.Windows.Forms.Padding(0);
            this.groupDetail.Name = "groupDetail";
            this.groupDetail.Padding = new System.Windows.Forms.Padding(0);
            this.groupDetail.Size = new System.Drawing.Size(726, 158);
            this.groupDetail.TabIndex = 38;
            this.groupDetail.TabStop = false;
            this.groupDetail.Text = "groupBox1";
            // 
            // gridDetail
            // 
            this.gridDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridDetail.Location = new System.Drawing.Point(0, 13);
            this.gridDetail.Name = "gridDetail";
            this.gridDetail.Size = new System.Drawing.Size(726, 145);
            this.gridDetail.TabIndex = 34;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 3;
            this.panelMain.SetColumnSpan(this.tableLayoutPanel4, 6);
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Controls.Add(this.btnXuatReport, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 332);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(726, 38);
            this.tableLayoutPanel4.TabIndex = 24;
            // 
            // btnXuatReport
            // 
            this.btnXuatReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnXuatReport.Location = new System.Drawing.Point(318, 5);
            this.btnXuatReport.Margin = new System.Windows.Forms.Padding(5);
            this.btnXuatReport.Name = "btnXuatReport";
            this.btnXuatReport.Size = new System.Drawing.Size(90, 28);
            this.btnXuatReport.TabIndex = 0;
            this.btnXuatReport.Text = "LẬP BÁO CÁO";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnXuatReport.UseSelectable = true;
            // 
            // panelMain
            // 
            this.panelMain.BackColor = System.Drawing.Color.Transparent;
            this.panelMain.ColumnCount = 6;
            this.panelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.panelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.panelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.panelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.panelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.panelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.panelMain.Controls.Add(this.tableLayoutPanel4, 0, 4);
            this.panelMain.Controls.Add(this.groupDetail, 0, 2);
            this.panelMain.Controls.Add(this.groupParent, 0, 0);
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(0, 0);
            this.panelMain.Name = "panelMain";
            this.panelMain.RowCount = 5;
            this.panelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.panelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545455F));
            this.panelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90.90909F));
            this.panelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545455F));
            this.panelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.panelMain.Size = new System.Drawing.Size(726, 370);
            this.panelMain.TabIndex = 1;
            // 
            // MetroListSummary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelMain);
            this.Name = "MetroListSummary";
            this.Size = new System.Drawing.Size(726, 370);
            this.groupParent.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSearch)).EndInit();
            this.groupDetail.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.panelMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupParent;
        private System.Windows.Forms.TableLayoutPanel panelMain;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private MetroControl.Components.MetroButton btnXuatReport;
        private System.Windows.Forms.GroupBox groupDetail;
        private MetroControl.Controls.MetroGridViewPage gridDetail;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public MetroControl.Controls.MetroGridViewPage gridParent;
        private MetroControl.Components.MetroLabel lblFilter;
        private MetroControl.Components.MetroLabel lblSearch;
        private MetroControl.Components.MetroDatePicker dateTo;
        private MetroControl.Components.MetroLabel lblDenNgay;
        private MetroControl.Components.MetroDatePicker dateFrom;
        private MetroControl.Components.MetroLabel lblTuNgay;
        private MetroControl.Components.MetroCombobox cbbFilter;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private MetroControl.Components.MetroPicture picSearch;
        private MetroControl.Components.MetroTextbox txtSearch;
    }
}
