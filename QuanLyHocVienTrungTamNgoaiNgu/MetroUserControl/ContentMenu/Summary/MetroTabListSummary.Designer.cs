﻿namespace MetroUserControl.ContentMenu.Summary
{
    partial class MetroTabListSummary
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl = new MetroControl.Components.MetroTabControl();
            this.tabPageDoanhThu = new System.Windows.Forms.TabPage();
            this.tabControlDoanhThu = new MetroControl.Components.MetroTabControl();
            this.pageDoanhThuHocVien = new MetroFramework.Controls.MetroTabPage();
            this.listSummaryHocVien = new MetroUserControl.ContentMenu.Summary.MetroListSummary();
            this.pageDoanhThuLopHoc = new MetroFramework.Controls.MetroTabPage();
            this.listSummaryLopHoc = new MetroUserControl.ContentMenu.Summary.MetroListSummary();
            this.pageDoanhThuKhoaHoc = new MetroFramework.Controls.MetroTabPage();
            this.listSummaryKhoaHoc = new MetroUserControl.ContentMenu.Summary.MetroListSummary();
            this.pageDoanhThuCoSo = new MetroFramework.Controls.MetroTabPage();
            this.listSummaryCoSo = new MetroUserControl.ContentMenu.Summary.MetroListSummary();
            this.pageDoanhThuTrungTam = new MetroFramework.Controls.MetroTabPage();
            this.centralDoanhThu = new MetroUserControl.ContentMenu.Summary.MetroCentralSummary();
            this.tabPageTongDoanhThu = new System.Windows.Forms.TabPage();
            this.tabControlTongDoanhThu = new MetroControl.Components.MetroTabControl();
            this.pageTongDoanhThuLopHoc = new MetroFramework.Controls.MetroTabPage();
            this.listSummaryTongLopHoc = new MetroUserControl.ContentMenu.Summary.MetroListSummary();
            this.pageTongDoanhThuKhoaHoc = new MetroFramework.Controls.MetroTabPage();
            this.listSummaryTongKhoaHoc = new MetroUserControl.ContentMenu.Summary.MetroListSummary();
            this.pageTongDoanhThuCoSo = new MetroFramework.Controls.MetroTabPage();
            this.listSummaryTongCoSo = new MetroUserControl.ContentMenu.Summary.MetroListSummary();
            this.pageTongDoanhThuTrungTam = new MetroFramework.Controls.MetroTabPage();
            this.centralTongDoanhThu = new MetroUserControl.ContentMenu.Summary.MetroCentralSummary();
            this.tabControl.SuspendLayout();
            this.tabPageDoanhThu.SuspendLayout();
            this.tabControlDoanhThu.SuspendLayout();
            this.pageDoanhThuHocVien.SuspendLayout();
            this.pageDoanhThuLopHoc.SuspendLayout();
            this.pageDoanhThuKhoaHoc.SuspendLayout();
            this.pageDoanhThuCoSo.SuspendLayout();
            this.pageDoanhThuTrungTam.SuspendLayout();
            this.tabPageTongDoanhThu.SuspendLayout();
            this.tabControlTongDoanhThu.SuspendLayout();
            this.pageTongDoanhThuLopHoc.SuspendLayout();
            this.pageTongDoanhThuKhoaHoc.SuspendLayout();
            this.pageTongDoanhThuCoSo.SuspendLayout();
            this.pageTongDoanhThuTrungTam.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPageDoanhThu);
            this.tabControl.Controls.Add(this.tabPageTongDoanhThu);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.FontSize = MetroFramework.MetroTabControlSize.Tall;
            this.tabControl.FontWeight = MetroFramework.MetroTabControlWeight.Regular;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 1;
            this.tabControl.Size = new System.Drawing.Size(719, 358);
            this.tabControl.TabIndex = 0;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.tabControl.UseSelectable = true;
            // 
            // tabPageDoanhThu
            // 
            this.tabPageDoanhThu.Controls.Add(this.tabControlDoanhThu);
            this.tabPageDoanhThu.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.tabPageDoanhThu.Location = new System.Drawing.Point(4, 44);
            this.tabPageDoanhThu.Name = "tabPageDoanhThu";
            this.tabPageDoanhThu.Size = new System.Drawing.Size(711, 310);
            this.tabPageDoanhThu.TabIndex = 0;
            this.tabPageDoanhThu.Text = "THỐNG KÊ DOANH THU";
            // 
            // tabControlDoanhThu
            // 
            this.tabControlDoanhThu.Controls.Add(this.pageDoanhThuHocVien);
            this.tabControlDoanhThu.Controls.Add(this.pageDoanhThuLopHoc);
            this.tabControlDoanhThu.Controls.Add(this.pageDoanhThuKhoaHoc);
            this.tabControlDoanhThu.Controls.Add(this.pageDoanhThuCoSo);
            this.tabControlDoanhThu.Controls.Add(this.pageDoanhThuTrungTam);
            this.tabControlDoanhThu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlDoanhThu.Location = new System.Drawing.Point(0, 0);
            this.tabControlDoanhThu.Name = "tabControlDoanhThu";
            this.tabControlDoanhThu.SelectedIndex = 4;
            this.tabControlDoanhThu.Size = new System.Drawing.Size(711, 310);
            this.tabControlDoanhThu.TabIndex = 0;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.tabControlDoanhThu.UseSelectable = true;
            // 
            // pageDoanhThuHocVien
            // 
            this.pageDoanhThuHocVien.Controls.Add(this.listSummaryHocVien);
            this.pageDoanhThuHocVien.HorizontalScrollbarBarColor = true;
            this.pageDoanhThuHocVien.HorizontalScrollbarHighlightOnWheel = false;
            this.pageDoanhThuHocVien.HorizontalScrollbarSize = 10;
            this.pageDoanhThuHocVien.Location = new System.Drawing.Point(4, 38);
            this.pageDoanhThuHocVien.Name = "pageDoanhThuHocVien";
            this.pageDoanhThuHocVien.Size = new System.Drawing.Size(703, 268);
            this.pageDoanhThuHocVien.TabIndex = 0;
            this.pageDoanhThuHocVien.Text = "THEO HỌC VIÊN";
            this.pageDoanhThuHocVien.VerticalScrollbarBarColor = true;
            this.pageDoanhThuHocVien.VerticalScrollbarHighlightOnWheel = false;
            this.pageDoanhThuHocVien.VerticalScrollbarSize = 10;
            // 
            // listSummaryHocVien
            // 
            this.listSummaryHocVien.BackColor = System.Drawing.Color.Transparent;
            this.listSummaryHocVien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listSummaryHocVien.Location = new System.Drawing.Point(0, 0);
            this.listSummaryHocVien.Margin = new System.Windows.Forms.Padding(4);
            this.listSummaryHocVien.Name = "listSummaryHocVien";
            this.listSummaryHocVien.Size = new System.Drawing.Size(703, 268);
            this.listSummaryHocVien.TabIndex = 4;
            this.listSummaryHocVien.Tag = "";
            // 
            // pageDoanhThuLopHoc
            // 
            this.pageDoanhThuLopHoc.Controls.Add(this.listSummaryLopHoc);
            this.pageDoanhThuLopHoc.HorizontalScrollbarBarColor = true;
            this.pageDoanhThuLopHoc.HorizontalScrollbarHighlightOnWheel = false;
            this.pageDoanhThuLopHoc.HorizontalScrollbarSize = 10;
            this.pageDoanhThuLopHoc.Location = new System.Drawing.Point(4, 38);
            this.pageDoanhThuLopHoc.Name = "pageDoanhThuLopHoc";
            this.pageDoanhThuLopHoc.Size = new System.Drawing.Size(703, 268);
            this.pageDoanhThuLopHoc.TabIndex = 1;
            this.pageDoanhThuLopHoc.Text = "THEO LỚP HỌC";
            this.pageDoanhThuLopHoc.VerticalScrollbarBarColor = true;
            this.pageDoanhThuLopHoc.VerticalScrollbarHighlightOnWheel = false;
            this.pageDoanhThuLopHoc.VerticalScrollbarSize = 10;
            // 
            // listSummaryLopHoc
            // 
            this.listSummaryLopHoc.BackColor = System.Drawing.Color.Transparent;
            this.listSummaryLopHoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listSummaryLopHoc.Location = new System.Drawing.Point(0, 0);
            this.listSummaryLopHoc.Margin = new System.Windows.Forms.Padding(4);
            this.listSummaryLopHoc.Name = "listSummaryLopHoc";
            this.listSummaryLopHoc.Size = new System.Drawing.Size(703, 268);
            this.listSummaryLopHoc.TabIndex = 5;
            this.listSummaryLopHoc.Tag = "";
            // 
            // pageDoanhThuKhoaHoc
            // 
            this.pageDoanhThuKhoaHoc.Controls.Add(this.listSummaryKhoaHoc);
            this.pageDoanhThuKhoaHoc.HorizontalScrollbarBarColor = true;
            this.pageDoanhThuKhoaHoc.HorizontalScrollbarHighlightOnWheel = false;
            this.pageDoanhThuKhoaHoc.HorizontalScrollbarSize = 10;
            this.pageDoanhThuKhoaHoc.Location = new System.Drawing.Point(4, 38);
            this.pageDoanhThuKhoaHoc.Name = "pageDoanhThuKhoaHoc";
            this.pageDoanhThuKhoaHoc.Size = new System.Drawing.Size(703, 268);
            this.pageDoanhThuKhoaHoc.TabIndex = 2;
            this.pageDoanhThuKhoaHoc.Text = "THEO KHOÁ HỌC";
            this.pageDoanhThuKhoaHoc.VerticalScrollbarBarColor = true;
            this.pageDoanhThuKhoaHoc.VerticalScrollbarHighlightOnWheel = false;
            this.pageDoanhThuKhoaHoc.VerticalScrollbarSize = 10;
            // 
            // listSummaryKhoaHoc
            // 
            this.listSummaryKhoaHoc.BackColor = System.Drawing.Color.Transparent;
            this.listSummaryKhoaHoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listSummaryKhoaHoc.Location = new System.Drawing.Point(0, 0);
            this.listSummaryKhoaHoc.Margin = new System.Windows.Forms.Padding(4);
            this.listSummaryKhoaHoc.Name = "listSummaryKhoaHoc";
            this.listSummaryKhoaHoc.Size = new System.Drawing.Size(703, 268);
            this.listSummaryKhoaHoc.TabIndex = 5;
            this.listSummaryKhoaHoc.Tag = "";
            // 
            // pageDoanhThuCoSo
            // 
            this.pageDoanhThuCoSo.Controls.Add(this.listSummaryCoSo);
            this.pageDoanhThuCoSo.HorizontalScrollbarBarColor = true;
            this.pageDoanhThuCoSo.HorizontalScrollbarHighlightOnWheel = false;
            this.pageDoanhThuCoSo.HorizontalScrollbarSize = 10;
            this.pageDoanhThuCoSo.Location = new System.Drawing.Point(4, 38);
            this.pageDoanhThuCoSo.Name = "pageDoanhThuCoSo";
            this.pageDoanhThuCoSo.Size = new System.Drawing.Size(703, 268);
            this.pageDoanhThuCoSo.TabIndex = 3;
            this.pageDoanhThuCoSo.Text = "THEO CƠ SỞ";
            this.pageDoanhThuCoSo.VerticalScrollbarBarColor = true;
            this.pageDoanhThuCoSo.VerticalScrollbarHighlightOnWheel = false;
            this.pageDoanhThuCoSo.VerticalScrollbarSize = 10;
            // 
            // listSummaryCoSo
            // 
            this.listSummaryCoSo.BackColor = System.Drawing.Color.Transparent;
            this.listSummaryCoSo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listSummaryCoSo.Location = new System.Drawing.Point(0, 0);
            this.listSummaryCoSo.Margin = new System.Windows.Forms.Padding(4);
            this.listSummaryCoSo.Name = "listSummaryCoSo";
            this.listSummaryCoSo.Size = new System.Drawing.Size(703, 268);
            this.listSummaryCoSo.TabIndex = 5;
            this.listSummaryCoSo.Tag = "";
            // 
            // pageDoanhThuTrungTam
            // 
            this.pageDoanhThuTrungTam.Controls.Add(this.centralDoanhThu);
            this.pageDoanhThuTrungTam.HorizontalScrollbarBarColor = true;
            this.pageDoanhThuTrungTam.HorizontalScrollbarHighlightOnWheel = false;
            this.pageDoanhThuTrungTam.HorizontalScrollbarSize = 10;
            this.pageDoanhThuTrungTam.Location = new System.Drawing.Point(4, 38);
            this.pageDoanhThuTrungTam.Name = "pageDoanhThuTrungTam";
            this.pageDoanhThuTrungTam.Size = new System.Drawing.Size(703, 268);
            this.pageDoanhThuTrungTam.TabIndex = 4;
            this.pageDoanhThuTrungTam.Text = "CẢ TRUNG TÂM";
            this.pageDoanhThuTrungTam.VerticalScrollbarBarColor = true;
            this.pageDoanhThuTrungTam.VerticalScrollbarHighlightOnWheel = false;
            this.pageDoanhThuTrungTam.VerticalScrollbarSize = 10;
            // 
            // centralDoanhThu
            // 
            this.centralDoanhThu.BackColor = System.Drawing.Color.Transparent;
            this.centralDoanhThu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.centralDoanhThu.Location = new System.Drawing.Point(0, 0);
            this.centralDoanhThu.Name = "centralDoanhThu";
            this.centralDoanhThu.Size = new System.Drawing.Size(703, 268);
            this.centralDoanhThu.TabIndex = 2;
            // 
            // tabPageTongDoanhThu
            // 
            this.tabPageTongDoanhThu.Controls.Add(this.tabControlTongDoanhThu);
            this.tabPageTongDoanhThu.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.tabPageTongDoanhThu.Location = new System.Drawing.Point(4, 44);
            this.tabPageTongDoanhThu.Name = "tabPageTongDoanhThu";
            this.tabPageTongDoanhThu.Size = new System.Drawing.Size(711, 310);
            this.tabPageTongDoanhThu.TabIndex = 1;
            this.tabPageTongDoanhThu.Text = "TỔNG DOANH THU";
            // 
            // tabControlTongDoanhThu
            // 
            this.tabControlTongDoanhThu.Controls.Add(this.pageTongDoanhThuLopHoc);
            this.tabControlTongDoanhThu.Controls.Add(this.pageTongDoanhThuKhoaHoc);
            this.tabControlTongDoanhThu.Controls.Add(this.pageTongDoanhThuCoSo);
            this.tabControlTongDoanhThu.Controls.Add(this.pageTongDoanhThuTrungTam);
            this.tabControlTongDoanhThu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlTongDoanhThu.Location = new System.Drawing.Point(0, 0);
            this.tabControlTongDoanhThu.Name = "tabControlTongDoanhThu";
            this.tabControlTongDoanhThu.SelectedIndex = 3;
            this.tabControlTongDoanhThu.Size = new System.Drawing.Size(711, 310);
            this.tabControlTongDoanhThu.TabIndex = 1;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.tabControlTongDoanhThu.UseSelectable = true;
            // 
            // pageTongDoanhThuLopHoc
            // 
            this.pageTongDoanhThuLopHoc.Controls.Add(this.listSummaryTongLopHoc);
            this.pageTongDoanhThuLopHoc.HorizontalScrollbarBarColor = true;
            this.pageTongDoanhThuLopHoc.HorizontalScrollbarHighlightOnWheel = false;
            this.pageTongDoanhThuLopHoc.HorizontalScrollbarSize = 10;
            this.pageTongDoanhThuLopHoc.Location = new System.Drawing.Point(4, 38);
            this.pageTongDoanhThuLopHoc.Name = "pageTongDoanhThuLopHoc";
            this.pageTongDoanhThuLopHoc.Size = new System.Drawing.Size(703, 268);
            this.pageTongDoanhThuLopHoc.TabIndex = 0;
            this.pageTongDoanhThuLopHoc.Text = "THEO LỚP HỌC";
            this.pageTongDoanhThuLopHoc.VerticalScrollbarBarColor = true;
            this.pageTongDoanhThuLopHoc.VerticalScrollbarHighlightOnWheel = false;
            this.pageTongDoanhThuLopHoc.VerticalScrollbarSize = 10;
            // 
            // listSummaryTongLopHoc
            // 
            this.listSummaryTongLopHoc.BackColor = System.Drawing.Color.Transparent;
            this.listSummaryTongLopHoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listSummaryTongLopHoc.Location = new System.Drawing.Point(0, 0);
            this.listSummaryTongLopHoc.Margin = new System.Windows.Forms.Padding(4);
            this.listSummaryTongLopHoc.Name = "listSummaryTongLopHoc";
            this.listSummaryTongLopHoc.Size = new System.Drawing.Size(703, 268);
            this.listSummaryTongLopHoc.TabIndex = 5;
            this.listSummaryTongLopHoc.Tag = "";
            // 
            // pageTongDoanhThuKhoaHoc
            // 
            this.pageTongDoanhThuKhoaHoc.Controls.Add(this.listSummaryTongKhoaHoc);
            this.pageTongDoanhThuKhoaHoc.HorizontalScrollbarBarColor = true;
            this.pageTongDoanhThuKhoaHoc.HorizontalScrollbarHighlightOnWheel = false;
            this.pageTongDoanhThuKhoaHoc.HorizontalScrollbarSize = 10;
            this.pageTongDoanhThuKhoaHoc.Location = new System.Drawing.Point(4, 38);
            this.pageTongDoanhThuKhoaHoc.Name = "pageTongDoanhThuKhoaHoc";
            this.pageTongDoanhThuKhoaHoc.Size = new System.Drawing.Size(703, 268);
            this.pageTongDoanhThuKhoaHoc.TabIndex = 1;
            this.pageTongDoanhThuKhoaHoc.Text = "THEO KHOÁ HỌC";
            this.pageTongDoanhThuKhoaHoc.VerticalScrollbarBarColor = true;
            this.pageTongDoanhThuKhoaHoc.VerticalScrollbarHighlightOnWheel = false;
            this.pageTongDoanhThuKhoaHoc.VerticalScrollbarSize = 10;
            // 
            // listSummaryTongKhoaHoc
            // 
            this.listSummaryTongKhoaHoc.BackColor = System.Drawing.Color.Transparent;
            this.listSummaryTongKhoaHoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listSummaryTongKhoaHoc.Location = new System.Drawing.Point(0, 0);
            this.listSummaryTongKhoaHoc.Margin = new System.Windows.Forms.Padding(4);
            this.listSummaryTongKhoaHoc.Name = "listSummaryTongKhoaHoc";
            this.listSummaryTongKhoaHoc.Size = new System.Drawing.Size(703, 268);
            this.listSummaryTongKhoaHoc.TabIndex = 5;
            this.listSummaryTongKhoaHoc.Tag = "";
            // 
            // pageTongDoanhThuCoSo
            // 
            this.pageTongDoanhThuCoSo.Controls.Add(this.listSummaryTongCoSo);
            this.pageTongDoanhThuCoSo.HorizontalScrollbarBarColor = true;
            this.pageTongDoanhThuCoSo.HorizontalScrollbarHighlightOnWheel = false;
            this.pageTongDoanhThuCoSo.HorizontalScrollbarSize = 10;
            this.pageTongDoanhThuCoSo.Location = new System.Drawing.Point(4, 38);
            this.pageTongDoanhThuCoSo.Name = "pageTongDoanhThuCoSo";
            this.pageTongDoanhThuCoSo.Size = new System.Drawing.Size(703, 268);
            this.pageTongDoanhThuCoSo.TabIndex = 2;
            this.pageTongDoanhThuCoSo.Text = "THEO CƠ SỞ";
            this.pageTongDoanhThuCoSo.VerticalScrollbarBarColor = true;
            this.pageTongDoanhThuCoSo.VerticalScrollbarHighlightOnWheel = false;
            this.pageTongDoanhThuCoSo.VerticalScrollbarSize = 10;
            // 
            // listSummaryTongCoSo
            // 
            this.listSummaryTongCoSo.BackColor = System.Drawing.Color.Transparent;
            this.listSummaryTongCoSo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listSummaryTongCoSo.Location = new System.Drawing.Point(0, 0);
            this.listSummaryTongCoSo.Margin = new System.Windows.Forms.Padding(4);
            this.listSummaryTongCoSo.Name = "listSummaryTongCoSo";
            this.listSummaryTongCoSo.Size = new System.Drawing.Size(703, 268);
            this.listSummaryTongCoSo.TabIndex = 5;
            this.listSummaryTongCoSo.Tag = "";
            // 
            // pageTongDoanhThuTrungTam
            // 
            this.pageTongDoanhThuTrungTam.Controls.Add(this.centralTongDoanhThu);
            this.pageTongDoanhThuTrungTam.HorizontalScrollbarBarColor = true;
            this.pageTongDoanhThuTrungTam.HorizontalScrollbarHighlightOnWheel = false;
            this.pageTongDoanhThuTrungTam.HorizontalScrollbarSize = 10;
            this.pageTongDoanhThuTrungTam.Location = new System.Drawing.Point(4, 38);
            this.pageTongDoanhThuTrungTam.Name = "pageTongDoanhThuTrungTam";
            this.pageTongDoanhThuTrungTam.Size = new System.Drawing.Size(703, 268);
            this.pageTongDoanhThuTrungTam.TabIndex = 3;
            this.pageTongDoanhThuTrungTam.Text = "CẢ TRUNG TÂM";
            this.pageTongDoanhThuTrungTam.VerticalScrollbarBarColor = true;
            this.pageTongDoanhThuTrungTam.VerticalScrollbarHighlightOnWheel = false;
            this.pageTongDoanhThuTrungTam.VerticalScrollbarSize = 10;
            // 
            // centralTongDoanhThu
            // 
            this.centralTongDoanhThu.BackColor = System.Drawing.Color.Transparent;
            this.centralTongDoanhThu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.centralTongDoanhThu.Location = new System.Drawing.Point(0, 0);
            this.centralTongDoanhThu.Name = "centralTongDoanhThu";
            this.centralTongDoanhThu.Size = new System.Drawing.Size(703, 268);
            this.centralTongDoanhThu.TabIndex = 2;
            // 
            // MetroTabListSummary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl);
            this.Name = "MetroTabListSummary";
            this.Size = new System.Drawing.Size(719, 358);
            this.tabControl.ResumeLayout(false);
            this.tabPageDoanhThu.ResumeLayout(false);
            this.tabControlDoanhThu.ResumeLayout(false);
            this.pageDoanhThuHocVien.ResumeLayout(false);
            this.pageDoanhThuLopHoc.ResumeLayout(false);
            this.pageDoanhThuKhoaHoc.ResumeLayout(false);
            this.pageDoanhThuCoSo.ResumeLayout(false);
            this.pageDoanhThuTrungTam.ResumeLayout(false);
            this.tabPageTongDoanhThu.ResumeLayout(false);
            this.tabControlTongDoanhThu.ResumeLayout(false);
            this.pageTongDoanhThuLopHoc.ResumeLayout(false);
            this.pageTongDoanhThuKhoaHoc.ResumeLayout(false);
            this.pageTongDoanhThuCoSo.ResumeLayout(false);
            this.pageTongDoanhThuTrungTam.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroControl.Components.MetroTabControl tabControl;
        private System.Windows.Forms.TabPage tabPageDoanhThu;
        private System.Windows.Forms.TabPage tabPageTongDoanhThu;
        private MetroControl.Components.MetroTabControl tabControlDoanhThu;
        private MetroFramework.Controls.MetroTabPage pageDoanhThuHocVien;
        private MetroFramework.Controls.MetroTabPage pageDoanhThuLopHoc;
        private MetroFramework.Controls.MetroTabPage pageDoanhThuKhoaHoc;
        private MetroFramework.Controls.MetroTabPage pageDoanhThuCoSo;
        private MetroFramework.Controls.MetroTabPage pageDoanhThuTrungTam;
        private MetroControl.Components.MetroTabControl tabControlTongDoanhThu;
        private MetroFramework.Controls.MetroTabPage pageTongDoanhThuLopHoc;
        private MetroFramework.Controls.MetroTabPage pageTongDoanhThuKhoaHoc;
        private MetroFramework.Controls.MetroTabPage pageTongDoanhThuCoSo;
        private MetroFramework.Controls.MetroTabPage pageTongDoanhThuTrungTam;
        internal MetroListSummary listSummaryCoSo;
        internal MetroListSummary listSummaryKhoaHoc;
        internal MetroListSummary listSummaryLopHoc;
        internal MetroListSummary listSummaryHocVien;
        internal MetroListSummary listSummaryTongLopHoc;
        internal MetroListSummary listSummaryTongKhoaHoc;
        internal MetroListSummary listSummaryTongCoSo;
        private MetroCentralSummary centralDoanhThu;
        private MetroCentralSummary centralTongDoanhThu;
    }
}
