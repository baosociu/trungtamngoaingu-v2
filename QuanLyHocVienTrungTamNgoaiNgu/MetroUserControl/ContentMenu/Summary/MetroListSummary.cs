﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroDataset;
using System.Threading;

namespace MetroUserControl.ContentMenu.Summary
{
    public partial class MetroListSummary : MetroBubble
    {
        public enum Type
        {
            None,
            TheoHocVien,
            TheoLopHoc,
            TheoKhoaHoc,
            TheoCoSo,
            TongLopHoc,
            TongKhoaHoc,
            TongCoSo
        }
        Type type;
        public MetroListSummary()
        {
            InitializeComponent();
            this.gridParent.grid.SelectionChanged += Grid_SelectionChanged;

            this.dateFrom.Value = this.dateFrom.Value.AddMonths(-3);
            this.dateTo.Value = this.dateFrom.Value.AddMonths(3);

            this.dateFrom.ValueChanged += DateValueChanged;
            this.dateTo.ValueChanged += DateValueChanged;

            this.gridDetail.grid.DataSourceChanged += (s,e)=> AlignDetailContent();

            this.Load += MetroListSummary_Load;
            this.btnXuatReport.Click += BtnXuatReport_Click;
        }

        private void BtnXuatReport_Click(object sender, EventArgs e)
        {
           if(type == Type.TheoHocVien)
            {
                if(gridParent.grid.CurrentRow != null)
                    BieuMau.InDoanhThuTheoHocVien(gridParent.grid.CurrentRow.Cells[0].Value.ToString(), dateFrom.Value, dateTo.Value);
            }
            else if (type == Type.TheoLopHoc)
            {
                if (gridParent.grid.CurrentRow != null)
                    BieuMau.InDoanhThuTheoLopHoc(gridParent.grid.CurrentRow.Cells[0].Value.ToString(), dateFrom.Value, dateTo.Value);
            }
            else if (type == Type.TheoKhoaHoc)
            {
                if (gridParent.grid.CurrentRow != null)
                    BieuMau.InDoanhThuTheoKhoaHoc(gridParent.grid.CurrentRow.Cells[0].Value.ToString(), dateFrom.Value, dateTo.Value);
            }
            else if (type == Type.TheoCoSo)
            {
                if (gridParent.grid.CurrentRow != null)
                    BieuMau.InDoanhThuTheoCoSo(gridParent.grid.CurrentRow.Cells[0].Value.ToString(), dateFrom.Value, dateTo.Value);
            }
            else if (type == Type.TongLopHoc)
            {
                if (gridParent.grid.CurrentRow != null)
                    BieuMau.InTongDoanhThuTheoLop(gridParent.grid.CurrentRow.Cells[0].Value.ToString(), dateFrom.Value, dateTo.Value);
            }
            else if (type == Type.TongKhoaHoc)
            {
                if (gridParent.grid.CurrentRow != null)
                    BieuMau.InTongDoanhThuTheoKhoaHoc(gridParent.grid.CurrentRow.Cells[0].Value.ToString(), dateFrom.Value, dateTo.Value);
            }
            else if (type == Type.TongCoSo)
            {
                if (gridParent.grid.CurrentRow != null)
                    BieuMau.InTongDoanhThuTheoCoSo(gridParent.grid.CurrentRow.Cells[0].Value.ToString(), dateFrom.Value, dateTo.Value);
            }
        }


        private void DateValueChanged(object sender, EventArgs e)
        {
            if (gridParent.grid.CurrentRow != null)
            {
                String value = gridParent.grid.CurrentRow.Cells[0].Value.ToString();
                LoadGridView(value);
            }
        }

        private void Grid_SelectionChanged(object sender, EventArgs e)
        {
            DataGridView grid = sender as DataGridView;
            if (grid.CurrentRow != null)
            {
                String value = grid.CurrentRow.Cells[0].Value.ToString();
                LoadGridView(value);
            }
        }



        private void MetroListSummary_Load(object sender, EventArgs e)
        {


        }
        public MetroListSummary SetAs(Type type)
        {
            if (type == this.type)
                return this;
            this.type = type;
            if (this.type == Type.TheoHocVien)
                SetupTheoHocVien();
            else if (this.type == Type.TheoLopHoc)
                SetupTheoLopHoc();
            else if (this.type == Type.TheoKhoaHoc)
                SetupTheoKhoaHoc();
            else if (this.type == Type.TheoCoSo)
                SetupTheoCoSo();
            else if (this.type == Type.TongLopHoc)
                SetupTongLopHoc();
            else if (this.type == Type.TongKhoaHoc)
                SetupTongKhoaHoc();
            else if (this.type == Type.TongCoSo)
                SetupTongCoSo();
            this.gridDetail.SetOnClick(() => { (this as MetroBubble).ReloadData(gridDetail.grid, false); });
            return this;
        }



        private void LoadGridView(String value)
        {
            if (this.type == Type.TheoHocVien)
                gridDetail.grid.DataSource = (new LoadByPage()).DoanhThuTheoHocVien_Grid(value, dateFrom.Value, dateTo.Value, gridDetail.PageNumber, gridDetail.NumberRow);
            else if (this.type == Type.TheoLopHoc)
                gridDetail.grid.DataSource = (new LoadByPage()).DoanhThuTheoLopHoc_Grid(value, dateFrom.Value, dateTo.Value, gridDetail.PageNumber, gridDetail.NumberRow);
            else if (this.type == Type.TheoKhoaHoc)
                gridDetail.grid.DataSource = (new LoadByPage()).DoanhThuTheoKhoaHoc_Grid(value, dateFrom.Value, dateTo.Value, gridDetail.PageNumber, gridDetail.NumberRow);
            else if (this.type == Type.TheoCoSo)
                gridDetail.grid.DataSource = (new LoadByPage()).DoanhThuTheoCoSo_Grid(value, dateFrom.Value, dateTo.Value, gridDetail.PageNumber, gridDetail.NumberRow);
            else if (this.type == Type.TongLopHoc)
                gridDetail.grid.DataSource =(new LoadByPage()).TongDoanhThuTheoLopHoc_Grid(value, dateFrom.Value, dateTo.Value, gridDetail.PageNumber, gridDetail.NumberRow);
            else if(this.type == Type.TongKhoaHoc)
                gridDetail.grid.DataSource =(new LoadByPage()).TongDoanhThuTheoKhoaHoc_Grid(value, dateFrom.Value, dateTo.Value, gridDetail.PageNumber, gridDetail.NumberRow);
            else if(this.type == Type.TongCoSo)
                gridDetail.grid.DataSource = (new LoadByPage()).TongDoanhThuTheoCoSo_Grid(value, dateFrom.Value, dateTo.Value, gridDetail.PageNumber, gridDetail.NumberRow);

        }


        private void AlignDetailContent()
        {
            if (type == Type.TongLopHoc)
                new Thread(new ThreadStart(() =>
                {
                    this.gridDetail.AlignColumn(0, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(1, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(2, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(4, DataGridViewContentAlignment.MiddleRight);
                    this.gridDetail.AlignColumn(5, DataGridViewContentAlignment.MiddleRight);
                })).Start();
            else if (type == Type.TongKhoaHoc)
                new Thread(new ThreadStart(() =>
                {
                    this.gridDetail.AlignColumn(0, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(1, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(2, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(3, DataGridViewContentAlignment.MiddleRight);
                    this.gridDetail.AlignColumn(4, DataGridViewContentAlignment.MiddleRight);
                    this.gridDetail.AlignColumn(5, DataGridViewContentAlignment.MiddleRight);
                })).Start();
            else if (type == Type.TheoHocVien)
                new Thread(new ThreadStart(() =>
                {
                    this.gridDetail.AlignColumn(0, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(1, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(2, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(4, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(6, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(7, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(8, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(9, DataGridViewContentAlignment.MiddleRight);
                })).Start();
            else if (type == Type.TongCoSo)
                new Thread(new ThreadStart(() =>
                {
                    this.gridDetail.AlignColumn(0, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(2, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(3, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(4, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(5, DataGridViewContentAlignment.MiddleRight);
                    this.gridDetail.AlignColumn(6, DataGridViewContentAlignment.MiddleRight);
                    this.gridDetail.AlignColumn(7, DataGridViewContentAlignment.MiddleRight);
                })).Start();
            else if (type == Type.TheoLopHoc)
                new Thread(new ThreadStart(() =>
                {
                    this.gridDetail.AlignColumn(0, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(1, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(2, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(4, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(6, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(7, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(8, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(9, DataGridViewContentAlignment.MiddleRight);
                })).Start();
            else if (type == Type.TheoKhoaHoc)
                new Thread(new ThreadStart(() =>
                {
                    this.gridDetail.AlignColumn(0, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(1, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(2, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(4, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(5, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(7, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(8, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(9, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(10, DataGridViewContentAlignment.MiddleRight);
                })).Start();
            else if (type == Type.TheoCoSo)
                new Thread(new ThreadStart(() =>
                {
                    this.gridDetail.AlignColumn(0, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(1, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(2, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(4, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(5, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(7, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(9, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(10, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(11, DataGridViewContentAlignment.MiddleCenter);
                    this.gridDetail.AlignColumn(12, DataGridViewContentAlignment.MiddleRight);
                })).Start() ;
        }


        #region Setup Tong doanh thu
        private void SetupTongLopHoc()
        {
            Func<int, int, int, string, string, object> funcGrid = (new LoadByPage()).HienThiLopHoc;
            Func<object> funCbb = (new Select()).HienThiFilterLopHoc_Cbb;
            Func<string, DateTime, DateTime, int, int, object> funcDetail = (new LoadByPage()).TongDoanhThuTheoLopHoc_Grid;

            this.SettingLoadByPage(funcGrid, funcDetail, funCbb);
            this.groupParent.Text = "Danh sách lớp học";
            this.groupDetail.Text = "Tổng doanh thu theo lớp học";
        }

        private void SetupTongKhoaHoc()
        {
            HideSearchFilter();
            Func<int, int, int, string, string, object> funcGrid = (new LoadByPage()).HienThiKhoaHoc;
            Func<object> funCbb = null;
            Func<string, DateTime, DateTime, int, int, object> funcDetail = (new LoadByPage()).TongDoanhThuTheoKhoaHoc_Grid;

            this.SettingLoadByPage(funcGrid, funcDetail, funCbb);
            this.groupParent.Text = "Danh sách khoá học";
            this.groupDetail.Text = "Tổng doanh thu theo khoá học";
        }

        private void SetupTongCoSo()
        {
            HideSearchFilter();
            Func<int, int, int, string, string, object> funcGrid = (new LoadByPage()).HienThiCoSo;
            Func<object> funCbb = null;
            Func<string, DateTime, DateTime, int, int, object> funcDetail = (new LoadByPage()).TongDoanhThuTheoCoSo_Grid;

            this.SettingLoadByPage(funcGrid, funcDetail, funCbb);
            this.groupParent.Text = "Danh sách cơ sở";
            this.groupDetail.Text = "Tổng doanh thu theo cơ sở";
        }

        #endregion



        #region Setup Doanh thu
        public void SetupTheoHocVien()
        {
            Func<int, int, int, string, string, object> funcGrid = (new LoadByPage()).HienThiHocVien;
            Func<object> funCbb = (new Select()).HienThiFilterHocVien_Cbb;
            Func<string, DateTime, DateTime, int, int, object> funcDetail = (new LoadByPage()).DoanhThuTheoHocVien_Grid;

            this.SettingLoadByPage(funcGrid, funcDetail, funCbb);
            this.groupParent.Text = "Danh sách học viên";
            this.groupDetail.Text = "Chi tiết doanh thu theo học viên";
        }

        private void SetupTheoLopHoc()
        {
            Func<int, int, int, string, string, object> funcGrid = (new LoadByPage()).HienThiLopHoc;
            Func<object> funCbb = (new Select()).HienThiFilterLopHoc_Cbb;
            Func<string, DateTime, DateTime, int, int, object> funcDetail = (new LoadByPage()).DoanhThuTheoLopHoc_Grid;

            this.SettingLoadByPage(funcGrid, funcDetail, funCbb);
            this.groupParent.Text = "Danh sách lớp học";
            this.groupDetail.Text = "Chi tiết doanh thu theo lớp học";
        }

        private void SetupTheoKhoaHoc()
        {
            HideSearchFilter();
            Func<int, int, int, string, string, object> funcGrid = (new LoadByPage()).HienThiKhoaHoc;
            Func<object> funCbb = null;
            Func<string, DateTime, DateTime, int, int, object> funcDetail = (new LoadByPage()).DoanhThuTheoKhoaHoc_Grid;

            this.SettingLoadByPage(funcGrid, funcDetail, funCbb);
            this.groupParent.Text = "Danh sách khoá học";
            this.groupDetail.Text = "Chi tiết doanh thu theo khoá học";
        }

        private void SetupTheoCoSo()
        {
            HideSearchFilter();
            Func<int, int, int, string, string, object> funcGrid = (new LoadByPage()).HienThiCoSo;
            Func<object> funCbb = null;
            Func<string, DateTime, DateTime, int, int, object> funcDetail = (new LoadByPage()).DoanhThuTheoCoSo_Grid;

            this.SettingLoadByPage(funcGrid, funcDetail, funCbb);
            this.groupParent.Text = "Danh sách cơ sở";
            this.groupDetail.Text = "Chi tiết doanh thu theo cơ sở";
        }

        #endregion


    
        #region load data
        private void HideSearchFilter()
        {
            picSearch.Visible = false;
            txtSearch.Visible = false;
            cbbFilter.Visible = false;
            lblFilter.Visible = false;
            lblSearch.Visible = false;
        }

        private void SettingLoadByPage(Func<int, int, int, string, string, object> funcSourceGrid, Func<string, DateTime, DateTime, int, int, object> funcSourceDetailGrid, Func<object> funcSourceCbb)
        {
            this.inputFunc = funcSourceGrid;

            if (funcSourceCbb != null)
            {
                //filter
                cbbFilter.DataSource = funcSourceCbb();
                cbbFilter.SelectedValueChanged += (s, e) =>
                {
                    if (cbbFilter.SelectedValue == null)
                        return;
                    txtSearch.Clear();
                    String key = cbbFilter.SelectedValue.ToString();
                    LoadByKey(gridParent, key);
                };

                //search
                EventHelper.Helper.SetHandHover(picSearch);
                picSearch.Click += (s, en) => LoadWithSearch(gridParent, cbbFilter, txtSearch.Text);
            }

            //load page
            this.LoadByKey(gridParent, cbbFilter.SelectedValue == null ? "" : cbbFilter.SelectedValue.ToString());
            gridParent.SetOnClick(() => { (this as MetroBubble).ReloadData(gridParent.grid, false); });
        }
        #endregion
    }
}
