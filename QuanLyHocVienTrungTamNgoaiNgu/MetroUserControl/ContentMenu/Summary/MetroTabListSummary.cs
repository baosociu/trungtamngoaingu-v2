﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace MetroUserControl.ContentMenu.Summary
{
    public partial class MetroTabListSummary : MetroBubble
    {
        public MetroTabListSummary()
        {
            InitializeComponent();
            this.Load += MetroTabListSummary_Load;
            this.tabControl.SelectedIndexChanged += TabControl_SelectedIndexChanged;
            this.tabControlDoanhThu.SelectedIndexChanged += TabControlDoanhThu_SelectedIndexChanged;
            this.tabControlTongDoanhThu.SelectedIndexChanged += TabControlTongDoanhThu_SelectedIndexChanged;
        }

        private void TabControlTongDoanhThu_SelectedIndexChanged(object sender, EventArgs e)
        {
            EventHelper.Helper.TryCatch(() =>
            {
                if (this.tabControlTongDoanhThu.SelectedTab.Equals(this.pageTongDoanhThuLopHoc))
                    listSummaryTongLopHoc.SetAs(MetroListSummary.Type.TongLopHoc);
                else if (this.tabControlTongDoanhThu.SelectedTab.Equals(this.pageTongDoanhThuKhoaHoc))
                    listSummaryTongKhoaHoc.SetAs(MetroListSummary.Type.TongKhoaHoc);
                else if (this.tabControlTongDoanhThu.SelectedTab.Equals(this.pageTongDoanhThuCoSo))
                    listSummaryTongCoSo.SetAs(MetroListSummary.Type.TongCoSo);
                else if (this.tabControlTongDoanhThu.SelectedTab.Equals(this.pageTongDoanhThuTrungTam))
                    centralTongDoanhThu.SetAs(MetroCentralSummary.Type.TongDoanhThu);
            });
        }

        private void TabControlDoanhThu_SelectedIndexChanged(object sender, EventArgs e)
        {
            EventHelper.Helper.TryCatch(() =>
            {
                if (this.tabControlDoanhThu.SelectedTab.Equals(this.pageDoanhThuHocVien))
                    listSummaryHocVien.SetAs(MetroListSummary.Type.TheoHocVien);
                else if (this.tabControlDoanhThu.SelectedTab.Equals(this.pageDoanhThuLopHoc))
                    listSummaryLopHoc.SetAs(MetroListSummary.Type.TheoLopHoc);
                else if (this.tabControlDoanhThu.SelectedTab.Equals(this.pageDoanhThuKhoaHoc))
                    listSummaryKhoaHoc.SetAs(MetroListSummary.Type.TheoKhoaHoc);
                else if (this.tabControlDoanhThu.SelectedTab.Equals(this.pageDoanhThuCoSo))
                    listSummaryCoSo.SetAs(MetroListSummary.Type.TheoCoSo);
                else if (this.tabControlDoanhThu.SelectedTab.Equals(this.pageDoanhThuTrungTam))
                    centralDoanhThu.SetAs(MetroCentralSummary.Type.DoanhThu);
            });
        }

        private void TabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            EventHelper.Helper.TryCatch(() =>
            {
                if (this.tabControl.SelectedTab.Equals(this.tabPageDoanhThu))
                    this.tabControlDoanhThu.SelectTab(pageDoanhThuHocVien);
                else if (this.tabControl.SelectedTab.Equals(this.tabPageTongDoanhThu))
                    this.tabControlTongDoanhThu.SelectTab(pageTongDoanhThuLopHoc);
            });
        }

        private void MetroTabListSummary_Load(object sender, EventArgs e)
        {
            listSummaryHocVien.SetAs(MetroListSummary.Type.TheoHocVien);
            this.tabControl.SelectTab(this.tabPageDoanhThu);
            this.tabControlDoanhThu.SelectTab(this.pageDoanhThuHocVien);
        }
    }
}
