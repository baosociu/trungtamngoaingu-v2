﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroDataset;
using MetroControl;
using MetroUserControl.ContentMenu.Mail;

namespace MetroUserControl.ContentMenu.Schedule.Staff
{
    public partial class MetroUpdateSchedule : MetroBubble
    {
        LOPHOC myLop;

        public MetroUpdateSchedule()
        {
            InitializeComponent();
            this.btnCancel.Click += (s, e) => this.ParentForm.Close();
            this.gridLichHoc.SelectionChanged += GridLichHoc_SelectionChanged;
            this.btnSave.Click += BtnSave_Click;
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            //text
            string malichhoc = txtMaLichHocRight.Text.ToString();
            LICHHOC lichhoc = (new Select()).LayLichByMaLopMaLich(myLop.MALOPHOC, malichhoc);
            if (lichhoc != null)
            {
                string macahoc = cbbCaHocRight.SelectedValue.ToString();
                DateTime ngayhoc = dateRight.Value;
                string phonghoc = cbbPhongRight.SelectedValue.ToString();
                string coso = lichhoc.MACOSO;

                if (macahoc == lichhoc.MACA && ngayhoc.ToShortDateString() == lichhoc.NGAYHOC.ToShortDateString() && phonghoc == lichhoc.MAPHONG && coso == lichhoc.MACOSO)
                {
                    EventHelper.Helper.MessageNoChangedInformation();
                    return;
                }

                LICHHOC _lichhoc = new LICHHOC()
                {
                    MALICHHOC = lichhoc.MALICHHOC,
                    MACA = macahoc,
                    NGAYHOC = ngayhoc,
                    MACOSO = coso,
                    MAPHONG = phonghoc,
                    MALOPHOC = myLop.MALOPHOC,
                    TINHTRANG = lichhoc.TINHTRANG
                };

                ResultExcuteQuery r = (new Update()).ThayDoiLichHoc(_lichhoc);

                EventHelper.Helper.MessageInfomation(r.GetMessage());

                if (r.isSuccessfully())
                {
                    gridLichHoc.DataSource = (new Select()).HienThiLichHocLopHoc(myLop.MALOPHOC);
                    SetCurrentGridByIDMaLich(lichhoc.MALICHHOC);

                    //thông báo cho lớp bị thay đổi lịch
                    EventHelper.Helper.MessageInfomationYesNo("Gửi mail thông báo thay đổi thông tin lịch học cho lớp " + myLop.MALOPHOC + " ?", () =>
                        {
                            List<object> data = (new MetroDataset.LoadByPage()).HienThiMailLopHoc(myLop.MALOPHOC);
                            MetroMail mm = new MetroMail().SetDataClass(data, myLop.MALOPHOC).SetStaff(MetroHelper.staff);
                            mm.infoMail.cbbChuDe.SelectedIndex = 2;
                            mm.pre_user = this;
                            MetroHelper.GoToTab(this.ParentForm, mm);
                        });


                }
            }
        }

        private void GridLichHoc_SelectionChanged(object sender, EventArgs e)
        {
            if (myLop != null)
            {
                string malich = getMaLichGridView();
                LICHHOC lich = (new Select()).LayLichByMaLopMaLich(myLop.MALOPHOC, malich);
                if (lich != null)
                    UpdateLayout(lich);
            }
        }

        public MetroUpdateSchedule SetByID(string idMaLopHoc, string idMaLichHoc)
        {
            myLop = (new Select()).GetClassByID(idMaLopHoc);
            if (myLop != null)
            {
                LICHHOC lichhoc = myLop.LICHHOCs.FirstOrDefault(t => t.MALICHHOC == idMaLichHoc);
                if (lichhoc != null)
                    UpdateLayout(lichhoc);

                //*************Grid**********************
                gridLichHoc.DataSource = (new Select()).HienThiLichHocLopHoc(myLop.MALOPHOC);
                SetCurrentGridByIDMaLich(lichhoc.MALICHHOC);

            }


            return this;
        }

        private void UpdateLayout(LICHHOC lichhoc)
        {
            //***********left****************
            CAHOC cahoc = (new Select()).LayCaHocByID(lichhoc.MACA);
            COSO cs = (new Select()).GetBranchByID(lichhoc.MACOSO);

            txtMaLichHocLeft.Text = lichhoc.MALICHHOC;
            cbbCaHocLeft.DataSource = new List<object>()
            {
                new
                {
                    Value = cahoc == null ? "" :
                    ("Ca " + cahoc.MACA.Replace("CA", "") +
                    " (" + cahoc.GIOBATDAU.Value.ToString().Substring(0, 5) +
                    " - " + cahoc.GIOKETTHUC.Value.ToString().Substring(0, 5) + ")"),
                    Key = cahoc == null ? "" : cahoc.MACA
                }
            };
            dateLeft.Value = lichhoc.NGAYHOC;
            cbbPhongLeft.DataSource = new List<object>()
            {
                new
                {
                    Value = "Phòng " + lichhoc.MAPHONG,
                    Key = lichhoc.MAPHONG
                }
            };
            txtLopHocLeft.Text = lichhoc.MALOPHOC;

            //************Right*********************
            txtMaLichHocRight.Text = lichhoc.MALICHHOC;
            dateRight.MinDate = DateTime.Now;
            cbbCaHocRight.DataSource = (new Select()).HienThiTatCaCaHoc_Cbb();
            cbbCaHocRight.SelectedValue = cahoc.MACA;
            cbbPhongRight.DataSource = (new Select()).HienThiPhongHocCuaCoSo_Cbb(cs.MACOSO);
            cbbPhongRight.SelectedValue = lichhoc.MAPHONG;
            txtLopHocRight.Text = lichhoc.MALOPHOC;


        }

        private void SetCurrentGridByIDMaLich(string malich)
        {
            for (int i = 0; i < gridLichHoc.RowCount; i++)
                if (gridLichHoc.Rows[i].Cells[0].Value.ToString().EndsWith(malich.Substring(4)))
                    gridLichHoc.SetCurrentRow(i);
        }

        private string getMaLichGridView()
        {
            if (gridLichHoc.CurrentRow != null)
            {
                string str = gridLichHoc.CurrentRow.Cells[0].Value.ToString();
                return "LICH" + str.Substring(str.Length - 2);
            }
            return "";
        }
    }
}
