﻿namespace MetroUserControl.ContentMenu.Schedule.Staff
{
    partial class MetroUpdateSchedule
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCancel = new MetroControl.Components.MetroButton();
            this.btnSave = new MetroControl.Components.MetroButton();
            this.groupDetail = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.metroLabel8 = new MetroControl.Components.MetroLabel();
            this.metroLabel9 = new MetroControl.Components.MetroLabel();
            this.metroLabel10 = new MetroControl.Components.MetroLabel();
            this.metroLabel11 = new MetroControl.Components.MetroLabel();
            this.txtMaLichHocRight = new MetroControl.Components.MetroTextbox();
            this.cbbCaHocRight = new MetroControl.Components.MetroCombobox();
            this.dateRight = new MetroControl.Components.MetroDatePicker();
            this.cbbPhongRight = new MetroControl.Components.MetroCombobox();
            this.metroLabel14 = new MetroControl.Components.MetroLabel();
            this.txtLopHocRight = new MetroControl.Components.MetroTextbox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.metroLabel1 = new MetroControl.Components.MetroLabel();
            this.metroLabel2 = new MetroControl.Components.MetroLabel();
            this.metroLabel3 = new MetroControl.Components.MetroLabel();
            this.metroLabel4 = new MetroControl.Components.MetroLabel();
            this.txtMaLichHocLeft = new MetroControl.Components.MetroTextbox();
            this.cbbCaHocLeft = new MetroControl.Components.MetroCombobox();
            this.dateLeft = new MetroControl.Components.MetroDatePicker();
            this.cbbPhongLeft = new MetroControl.Components.MetroCombobox();
            this.metroLabel7 = new MetroControl.Components.MetroLabel();
            this.txtLopHocLeft = new MetroControl.Components.MetroTextbox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.gridLichHoc = new MetroControl.Components.MetroGridView();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.groupDetail.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLichHoc)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.groupDetail, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.groupBox3, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 220F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(660, 660);
            this.tableLayoutPanel2.TabIndex = 3;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 5;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.Controls.Add(this.btnCancel, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnSave, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 624);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(660, 36);
            this.tableLayoutPanel4.TabIndex = 23;
            // 
            // btnCancel
            // 
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCancel.Location = new System.Drawing.Point(409, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(94, 30);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "HỦY";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnCancel.UseSelectable = true;
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSave.Location = new System.Drawing.Point(156, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(94, 30);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "LƯU";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnSave.UseSelectable = true;
            // 
            // groupDetail
            // 
            this.groupDetail.Controls.Add(this.tableLayoutPanel1);
            this.groupDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupDetail.Location = new System.Drawing.Point(3, 3);
            this.groupDetail.Name = "groupDetail";
            this.groupDetail.Size = new System.Drawing.Size(654, 214);
            this.groupDetail.TabIndex = 24;
            this.groupDetail.TabStop = false;
            this.groupDetail.Text = "Thông tin lịch học";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.groupBox2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 195F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(648, 195);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tableLayoutPanel5);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(327, 12);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 12, 3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(318, 180);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Lịch mới";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 4;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.33334F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanel5.Controls.Add(this.metroLabel8, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.metroLabel9, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.metroLabel10, 1, 3);
            this.tableLayoutPanel5.Controls.Add(this.metroLabel11, 1, 4);
            this.tableLayoutPanel5.Controls.Add(this.txtMaLichHocRight, 2, 1);
            this.tableLayoutPanel5.Controls.Add(this.cbbCaHocRight, 2, 2);
            this.tableLayoutPanel5.Controls.Add(this.dateRight, 2, 3);
            this.tableLayoutPanel5.Controls.Add(this.cbbPhongRight, 2, 4);
            this.tableLayoutPanel5.Controls.Add(this.metroLabel14, 1, 5);
            this.tableLayoutPanel5.Controls.Add(this.txtLopHocRight, 2, 5);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 7;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(312, 161);
            this.tableLayoutPanel5.TabIndex = 2;
            // 
            // metroLabel8
            // 
            this.metroLabel8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel8.IsFocus = false;
            this.metroLabel8.Location = new System.Drawing.Point(20, 8);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(94, 15);
            this.metroLabel8.TabIndex = 0;
            this.metroLabel8.Text = "Lịch Học:";
            this.metroLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel9
            // 
            this.metroLabel9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel9.IsFocus = false;
            this.metroLabel9.Location = new System.Drawing.Point(20, 40);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(94, 15);
            this.metroLabel9.TabIndex = 1;
            this.metroLabel9.Text = "Ca Học:";
            this.metroLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel10
            // 
            this.metroLabel10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel10.IsFocus = false;
            this.metroLabel10.Location = new System.Drawing.Point(20, 72);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(94, 15);
            this.metroLabel10.TabIndex = 2;
            this.metroLabel10.Text = "Ngày Học:";
            this.metroLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel11
            // 
            this.metroLabel11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel11.IsFocus = false;
            this.metroLabel11.Location = new System.Drawing.Point(20, 104);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(94, 15);
            this.metroLabel11.TabIndex = 3;
            this.metroLabel11.Text = "Phòng Học:";
            this.metroLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtMaLichHocRight
            // 
            this.txtMaLichHocRight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaLichHocRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtMaLichHocRight.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtMaLichHocRight.Location = new System.Drawing.Point(117, 4);
            this.txtMaLichHocRight.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtMaLichHocRight.Name = "txtMaLichHocRight";
            this.txtMaLichHocRight.ReadOnly = true;
            this.txtMaLichHocRight.Size = new System.Drawing.Size(176, 23);
            this.txtMaLichHocRight.TabIndex = 7;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // cbbCaHocRight
            // 
            this.cbbCaHocRight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbCaHocRight.DisplayMember = "Value";
            this.cbbCaHocRight.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbbCaHocRight.FormattingEnabled = true;
            this.cbbCaHocRight.ItemHeight = 19;
            this.cbbCaHocRight.Location = new System.Drawing.Point(117, 35);
            this.cbbCaHocRight.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.cbbCaHocRight.Name = "cbbCaHocRight";
            this.cbbCaHocRight.Size = new System.Drawing.Size(176, 25);
            this.cbbCaHocRight.TabIndex = 8;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.cbbCaHocRight.UseSelectable = true;
            this.cbbCaHocRight.ValueMember = "Key";
            // 
            // dateRight
            // 
            this.dateRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.dateRight.FontSize = MetroFramework.MetroDateTimeSize.Small;
            this.dateRight.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateRight.Location = new System.Drawing.Point(117, 67);
            this.dateRight.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.dateRight.MinimumSize = new System.Drawing.Size(0, 25);
            this.dateRight.Name = "dateRight";
            this.dateRight.Size = new System.Drawing.Size(176, 25);
            this.dateRight.TabIndex = 9;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // cbbPhongRight
            // 
            this.cbbPhongRight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbPhongRight.DisplayMember = "Value";
            this.cbbPhongRight.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbbPhongRight.FormattingEnabled = true;
            this.cbbPhongRight.ItemHeight = 19;
            this.cbbPhongRight.Location = new System.Drawing.Point(117, 99);
            this.cbbPhongRight.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.cbbPhongRight.Name = "cbbPhongRight";
            this.cbbPhongRight.Size = new System.Drawing.Size(176, 25);
            this.cbbPhongRight.TabIndex = 10;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.cbbPhongRight.UseSelectable = true;
            this.cbbPhongRight.ValueMember = "Key";
            // 
            // metroLabel14
            // 
            this.metroLabel14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel14.AutoSize = true;
            this.metroLabel14.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel14.IsFocus = false;
            this.metroLabel14.Location = new System.Drawing.Point(20, 136);
            this.metroLabel14.Name = "metroLabel14";
            this.metroLabel14.Size = new System.Drawing.Size(94, 15);
            this.metroLabel14.TabIndex = 6;
            this.metroLabel14.Text = "Lớp Học:";
            this.metroLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtLopHocRight
            // 
            this.txtLopHocRight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLopHocRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtLopHocRight.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtLopHocRight.Location = new System.Drawing.Point(117, 132);
            this.txtLopHocRight.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtLopHocRight.Name = "txtLopHocRight";
            this.txtLopHocRight.ReadOnly = true;
            this.txtLopHocRight.Size = new System.Drawing.Size(176, 23);
            this.txtLopHocRight.TabIndex = 12;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel3);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 12);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 12, 3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(318, 180);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Lịch cần thay đổi";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.33334F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanel3.Controls.Add(this.metroLabel1, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.metroLabel2, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.metroLabel3, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.metroLabel4, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.txtMaLichHocLeft, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.cbbCaHocLeft, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.dateLeft, 2, 3);
            this.tableLayoutPanel3.Controls.Add(this.cbbPhongLeft, 2, 4);
            this.tableLayoutPanel3.Controls.Add(this.metroLabel7, 1, 5);
            this.tableLayoutPanel3.Controls.Add(this.txtLopHocLeft, 2, 5);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 7;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(312, 161);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // metroLabel1
            // 
            this.metroLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel1.IsFocus = false;
            this.metroLabel1.Location = new System.Drawing.Point(20, 8);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(94, 15);
            this.metroLabel1.TabIndex = 0;
            this.metroLabel1.Text = "Lịch Học:";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel2
            // 
            this.metroLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel2.IsFocus = false;
            this.metroLabel2.Location = new System.Drawing.Point(20, 40);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(94, 15);
            this.metroLabel2.TabIndex = 1;
            this.metroLabel2.Text = "Ca Học:";
            this.metroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel3
            // 
            this.metroLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel3.IsFocus = false;
            this.metroLabel3.Location = new System.Drawing.Point(20, 72);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(94, 15);
            this.metroLabel3.TabIndex = 2;
            this.metroLabel3.Text = "Ngày Học:";
            this.metroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel4
            // 
            this.metroLabel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel4.IsFocus = false;
            this.metroLabel4.Location = new System.Drawing.Point(20, 104);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(94, 15);
            this.metroLabel4.TabIndex = 3;
            this.metroLabel4.Text = "Phòng Học:";
            this.metroLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtMaLichHocLeft
            // 
            this.txtMaLichHocLeft.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaLichHocLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtMaLichHocLeft.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtMaLichHocLeft.Location = new System.Drawing.Point(117, 4);
            this.txtMaLichHocLeft.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtMaLichHocLeft.Name = "txtMaLichHocLeft";
            this.txtMaLichHocLeft.ReadOnly = true;
            this.txtMaLichHocLeft.Size = new System.Drawing.Size(176, 23);
            this.txtMaLichHocLeft.TabIndex = 7;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // cbbCaHocLeft
            // 
            this.cbbCaHocLeft.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbCaHocLeft.DisplayMember = "Value";
            this.cbbCaHocLeft.Enabled = false;
            this.cbbCaHocLeft.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbbCaHocLeft.FormattingEnabled = true;
            this.cbbCaHocLeft.ItemHeight = 19;
            this.cbbCaHocLeft.Location = new System.Drawing.Point(117, 35);
            this.cbbCaHocLeft.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.cbbCaHocLeft.Name = "cbbCaHocLeft";
            this.cbbCaHocLeft.Size = new System.Drawing.Size(176, 25);
            this.cbbCaHocLeft.TabIndex = 8;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.cbbCaHocLeft.UseSelectable = true;
            this.cbbCaHocLeft.ValueMember = "Key";
            // 
            // dateLeft
            // 
            this.dateLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateLeft.Enabled = false;
            this.dateLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.dateLeft.FontSize = MetroFramework.MetroDateTimeSize.Small;
            this.dateLeft.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateLeft.Location = new System.Drawing.Point(117, 67);
            this.dateLeft.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.dateLeft.MinimumSize = new System.Drawing.Size(0, 25);
            this.dateLeft.Name = "dateLeft";
            this.dateLeft.Size = new System.Drawing.Size(176, 25);
            this.dateLeft.TabIndex = 9;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // cbbPhongLeft
            // 
            this.cbbPhongLeft.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbPhongLeft.DisplayMember = "Value";
            this.cbbPhongLeft.Enabled = false;
            this.cbbPhongLeft.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbbPhongLeft.FormattingEnabled = true;
            this.cbbPhongLeft.ItemHeight = 19;
            this.cbbPhongLeft.Location = new System.Drawing.Point(117, 99);
            this.cbbPhongLeft.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.cbbPhongLeft.Name = "cbbPhongLeft";
            this.cbbPhongLeft.Size = new System.Drawing.Size(176, 25);
            this.cbbPhongLeft.TabIndex = 10;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.cbbPhongLeft.UseSelectable = true;
            this.cbbPhongLeft.ValueMember = "Key";
            // 
            // metroLabel7
            // 
            this.metroLabel7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel7.IsFocus = false;
            this.metroLabel7.Location = new System.Drawing.Point(20, 136);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(94, 15);
            this.metroLabel7.TabIndex = 6;
            this.metroLabel7.Text = "Lớp Học:";
            this.metroLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtLopHocLeft
            // 
            this.txtLopHocLeft.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLopHocLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtLopHocLeft.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtLopHocLeft.Location = new System.Drawing.Point(117, 132);
            this.txtLopHocLeft.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtLopHocLeft.Name = "txtLopHocLeft";
            this.txtLopHocLeft.ReadOnly = true;
            this.txtLopHocLeft.Size = new System.Drawing.Size(176, 23);
            this.txtLopHocLeft.TabIndex = 12;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.gridLichHoc);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(3, 223);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(654, 398);
            this.groupBox3.TabIndex = 25;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Lịch học của lớp ";
            // 
            // gridLichHoc
            // 
            this.gridLichHoc.AllowUserToAddRows = false;
            this.gridLichHoc.AllowUserToDeleteRows = false;
            this.gridLichHoc.AllowUserToResizeRows = false;
            this.gridLichHoc.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridLichHoc.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gridLichHoc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridLichHoc.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.gridLichHoc.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridLichHoc.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridLichHoc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridLichHoc.DefaultCellStyle = dataGridViewCellStyle2;
            this.gridLichHoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridLichHoc.EnableHeadersVisualStyles = false;
            this.gridLichHoc.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.gridLichHoc.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gridLichHoc.Location = new System.Drawing.Point(3, 16);
            this.gridLichHoc.MultiSelect = false;
            this.gridLichHoc.Name = "gridLichHoc";
            this.gridLichHoc.ReadOnly = true;
            this.gridLichHoc.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridLichHoc.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.gridLichHoc.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridLichHoc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridLichHoc.Size = new System.Drawing.Size(648, 379);
            this.gridLichHoc.TabIndex = 27;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // MetroUpdateSchedule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel2);
            this.Name = "MetroUpdateSchedule";
            this.Size = new System.Drawing.Size(660, 660);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.groupDetail.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridLichHoc)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private MetroControl.Components.MetroButton btnCancel;
        private MetroControl.Components.MetroButton btnSave;
        private System.Windows.Forms.GroupBox groupDetail;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private MetroControl.Components.MetroLabel metroLabel1;
        private MetroControl.Components.MetroLabel metroLabel2;
        private MetroControl.Components.MetroLabel metroLabel3;
        private MetroControl.Components.MetroLabel metroLabel4;
        private MetroControl.Components.MetroLabel metroLabel7;
        private MetroControl.Components.MetroTextbox txtMaLichHocLeft;
        private MetroControl.Components.MetroCombobox cbbCaHocLeft;
        private MetroControl.Components.MetroDatePicker dateLeft;
        private MetroControl.Components.MetroCombobox cbbPhongLeft;
        private MetroControl.Components.MetroTextbox txtLopHocLeft;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private MetroControl.Components.MetroLabel metroLabel8;
        private MetroControl.Components.MetroLabel metroLabel9;
        private MetroControl.Components.MetroLabel metroLabel10;
        private MetroControl.Components.MetroLabel metroLabel11;
        private MetroControl.Components.MetroLabel metroLabel14;
        private MetroControl.Components.MetroTextbox txtMaLichHocRight;
        private MetroControl.Components.MetroCombobox cbbCaHocRight;
        private MetroControl.Components.MetroDatePicker dateRight;
        private MetroControl.Components.MetroCombobox cbbPhongRight;
        private MetroControl.Components.MetroTextbox txtLopHocRight;
        private System.Windows.Forms.GroupBox groupBox3;
        private MetroControl.Components.MetroGridView gridLichHoc;
    }
}
