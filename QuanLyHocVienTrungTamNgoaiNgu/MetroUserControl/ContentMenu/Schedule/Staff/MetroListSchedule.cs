﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroControl;
using MetroDataset;
using static MetroControl.MetroHelper;

namespace MetroUserControl.ContentMenu.Schedule.Staff
{
    public partial class MetroListSchedule : MetroBubble
    {
        public MetroListSchedule()
        {
            InitializeComponent();
            this.VisibleChanged += MetroListSchedule_VisibleChanged;
            this.Load += MetroListSchedule_Load;
        }

        private void MetroListSchedule_Load(object sender, EventArgs e)
        {
            this.btnChange.Click += (s, en) => {
                if (gridpage.grid.CurrentRow != null)
                {
                    string malich = gridpage.grid.CurrentRow.Cells[0].Value.ToString();
                    string malop = gridpage.grid.CurrentRow.Cells[1].Value.ToString();
                    MetroHelper.OpenNewTab(this, new MetroUpdateSchedule().SetByID(malop, malich));

                }
            };
            EventHelper.Helper.SetDoubleClickItemGridView(gridpage.grid, ()=> { if (btnChange.Visible) btnChange.PerformClick(); });
            this.btnChange.TypeData.Add(MetroControl.Components.Type.Update);
            this.VisibleByPermission();
        }

        private void MetroListSchedule_VisibleChanged(object sender, EventArgs e)
        {
            

            SettingLoadByPageFilterSearch(this.gridpage, (new LoadByPage()).HienThiLichHoc, cbbLoc, (new Select()).HienThiFilterLichHoc_Cbb, picSearch, txtTimKiem);
        }
    }
}
