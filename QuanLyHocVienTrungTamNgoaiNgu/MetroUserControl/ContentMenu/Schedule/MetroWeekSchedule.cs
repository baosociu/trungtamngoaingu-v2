﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroDataset;
using MetroDataset.Model;
using MetroControl;
using MetroControl.Components;

namespace MetroUserControl.ContentMenu.Schedule
{
    public partial class MetroWeekSchedule : UserControl
    {
        DateTime startWeek; //thứ 2
        DateTime endWeek; //chủ nhật
        Func<string, DateTime, DateTime, List<object[]>> funcDataSource;

        public MetroWeekSchedule()
        {
            InitializeComponent();
            EventHelper.Helper.TryCatch(SetLabelThu);
            EventHelper.Helper.TryCatch(SetLabelCaHoc);
            panel.ScrollControlIntoView(tableLayoutPanel3);
            this.datePicker.ValueChanged += DatePicker_ValueChanged;
            this.SizeChanged += TableLayoutPanel3_SizeChanged;

            //set row size
            panel.RowStyles[0].SizeType = SizeType.Absolute;
            panel.RowStyles[0].Height = 60;
            for (int i = 1; i < panel.RowStyles.Count; i++)
            {
                panel.RowStyles[i].SizeType = SizeType.Absolute;
                panel.RowStyles[i].Height = 80;
            }
            EventHelper.Helper.SetHandHover(picNext);
            EventHelper.Helper.SetHandHover(picBack);
            picBack.Click += (s, e) => datePicker.Value = datePicker.Value.AddDays(-7);
            picNext.Click += (s, e) => datePicker.Value = datePicker.Value.AddDays(7);
        }

        private void TableLayoutPanel3_SizeChanged(object sender, EventArgs e)
        {
            EventHelper.Helper.TryCatch(UpdateColumnsSize);
        }


        private void UpdateColumnsSize()
        {
            Size size = this.tableLayoutPanel3.Size;
            float width = size.Width - panel.ColumnStyles[0].Width - SystemInformation.VerticalScrollBarWidth - 5;
            float one_column = width / 7;
            for (int i = 1; i < panel.ColumnStyles.Count; i++)
            {
                panel.ColumnStyles[i].SizeType = SizeType.Absolute;
                panel.ColumnStyles[i].Width = one_column;
            }
        }

        public void SetFuncDataSource(Func<string, DateTime, DateTime, List<object[]>> funcDataSource)
        {
            this.funcDataSource = funcDataSource;
            UpdateData();
        }

        private void DatePicker_ValueChanged(object sender, EventArgs e)
        {
            UpdateData();
        }

        private void UpdateData()
        {
            DateTime oldStart = startWeek;

            UpdateStartEnd();

            if (oldStart.Date == startWeek.Date)
                return;

            EventHelper.Helper.TryCatch(SetLabelThu);
            //get data
            List<object[]> datasource = this.funcDataSource(MetroHelper.iduser, startWeek, endWeek);
            HiddenAll();
            foreach (object[] arr_o in datasource)
            {
                string lop = arr_o[1].ToString();
                string khoahoc = arr_o[3].ToString();
                string phong = arr_o[4].ToString();
                string coso = arr_o[6].ToString();
                DateTime ngayhoc = (DateTime)arr_o[7];
                string cahoc = arr_o[8].ToString();
                TimeSpan batdau = (TimeSpan)arr_o[9];
                TimeSpan ketthuc = (TimeSpan)arr_o[10];
                string tinhtrang = arr_o[11].ToString();

                int indexrow = int.Parse(cahoc.Replace("CA", "")) - 1;
                int indexcol = (int)ngayhoc.DayOfWeek == 0 ? 6 : ((int)ngayhoc.DayOfWeek - 1);

                SetInformation(indexrow, indexcol, lop, khoahoc, phong, coso);
            }
        }

        private void SetInformation(int indexrow, int indexcol, string lop, string khoahoc, string phong, string coso)
        {
            Label lbl = GetLabelByName("lbl" + indexrow + indexcol);
            String info = "LỚP: " + lop + "\n" + khoahoc + "\nPHÒNG: " + phong + "\n" + coso;
            lbl.Text = info.ToUpper();
            //lbl.Visible = true;
            lbl.BorderStyle = BorderStyle.FixedSingle;
            lbl.Margin = new Padding(1, 1, 1, 1);
        }

        private void HiddenAll()
        {
            for (int i = 0; i < 6; i++)
                for (int j = 0; j < 7; j++)
                {
                    Label lbl = GetLabelByName("lbl" + i.ToString() + j.ToString());
                    lbl.BorderStyle = BorderStyle.None;
                    lbl.Margin = new Padding(0, 0, 0, 0);
                    lbl.Text = "";
                }
        }

        private void SetLabelCaHoc()
        {
            List<MetroDataset.CAHOC> list = (new Select()).HienThiTatCaCaHoc();
            for (int i = 0; i < 6; i++)
            {
                Label lbl = GetLabelByName("lblCa" + (i + 1).ToString());
                if (lbl != null)
                {
                    string tg = list[i].GIOBATDAU.Value.ToString().Substring(0, 5) + " - " + list[i].GIOKETTHUC.Value.ToString().Substring(0, 5);
                    lbl.Text = "Ca " + (i + 1).ToString() + "\n" + tg;
                    lbl.Text = lbl.Text.ToUpper();
                }
            }
        }

        private void SetLabelThu()
        {
            for (int i = 0; i < 7; i++)
            {
                Label lbl = GetLabelByName("lblThu" + (i + 2).ToString());
                if (lbl != null)
                {
                    string ngay = startWeek.AddDays(i).ToShortDateString();
                    lbl.Text = (i != 6 ? "Thứ " + (i + 2) : "Chủ Nhật") + "\n" + ngay;
                    lbl.Text = lbl.Text.ToUpper();
                }
            }
        }

        private void UpdateStartEnd()
        {
            DateTime d = this.datePicker.Value;
            if ((int)d.DayOfWeek != 0)
                startWeek = d.AddDays(-(int)d.DayOfWeek + 1);
            else
                startWeek = d.AddDays(-6);

            endWeek = startWeek.AddDays(6);
        }

        private Label GetLabelByName(string name)
        {
            foreach (Label lbl in panel.Controls.OfType<Label>())
                if (lbl.Name == name)
                    return lbl;
            return null;
        }
    }
}
