﻿namespace MetroUserControl.ContentMenu.Schedule
{
    partial class MetroWeekSchedule
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.picNext = new MetroControl.Components.MetroPicture();
            this.datePicker = new MetroControl.Components.MetroDatePicker();
            this.picBack = new MetroControl.Components.MetroPicture();
            this.tableLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel = new System.Windows.Forms.TableLayoutPanel();
            this.lbl50 = new MetroControl.Components.MetroLabel();
            this.lbl51 = new MetroControl.Components.MetroLabel();
            this.lbl52 = new MetroControl.Components.MetroLabel();
            this.lbl53 = new MetroControl.Components.MetroLabel();
            this.lbl54 = new MetroControl.Components.MetroLabel();
            this.lbl55 = new MetroControl.Components.MetroLabel();
            this.lbl56 = new MetroControl.Components.MetroLabel();
            this.lbl40 = new MetroControl.Components.MetroLabel();
            this.lbl41 = new MetroControl.Components.MetroLabel();
            this.lbl42 = new MetroControl.Components.MetroLabel();
            this.lbl43 = new MetroControl.Components.MetroLabel();
            this.lbl44 = new MetroControl.Components.MetroLabel();
            this.lbl45 = new MetroControl.Components.MetroLabel();
            this.lbl46 = new MetroControl.Components.MetroLabel();
            this.lbl30 = new MetroControl.Components.MetroLabel();
            this.lbl31 = new MetroControl.Components.MetroLabel();
            this.lbl32 = new MetroControl.Components.MetroLabel();
            this.lbl33 = new MetroControl.Components.MetroLabel();
            this.lbl34 = new MetroControl.Components.MetroLabel();
            this.lbl35 = new MetroControl.Components.MetroLabel();
            this.lbl36 = new MetroControl.Components.MetroLabel();
            this.lbl20 = new MetroControl.Components.MetroLabel();
            this.lbl21 = new MetroControl.Components.MetroLabel();
            this.lbl22 = new MetroControl.Components.MetroLabel();
            this.lbl23 = new MetroControl.Components.MetroLabel();
            this.lbl24 = new MetroControl.Components.MetroLabel();
            this.lbl25 = new MetroControl.Components.MetroLabel();
            this.lbl26 = new MetroControl.Components.MetroLabel();
            this.lbl10 = new MetroControl.Components.MetroLabel();
            this.lbl11 = new MetroControl.Components.MetroLabel();
            this.lbl12 = new MetroControl.Components.MetroLabel();
            this.lbl13 = new MetroControl.Components.MetroLabel();
            this.lbl14 = new MetroControl.Components.MetroLabel();
            this.lbl15 = new MetroControl.Components.MetroLabel();
            this.lbl16 = new MetroControl.Components.MetroLabel();
            this.lbl05 = new MetroControl.Components.MetroLabel();
            this.lbl06 = new MetroControl.Components.MetroLabel();
            this.lbl03 = new MetroControl.Components.MetroLabel();
            this.lbl04 = new MetroControl.Components.MetroLabel();
            this.lbl02 = new MetroControl.Components.MetroLabel();
            this.lbl01 = new MetroControl.Components.MetroLabel();
            this.lbl00 = new MetroControl.Components.MetroLabel();
            this.lblThu8 = new MetroControl.Components.MetroLabel();
            this.lblThu7 = new MetroControl.Components.MetroLabel();
            this.lblThu6 = new MetroControl.Components.MetroLabel();
            this.lblThu5 = new MetroControl.Components.MetroLabel();
            this.lblThu4 = new MetroControl.Components.MetroLabel();
            this.lblThu3 = new MetroControl.Components.MetroLabel();
            this.lblThu2 = new MetroControl.Components.MetroLabel();
            this.lblCa6 = new MetroControl.Components.MetroLabel();
            this.lblCa5 = new MetroControl.Components.MetroLabel();
            this.lblCa4 = new MetroControl.Components.MetroLabel();
            this.lblCa3 = new MetroControl.Components.MetroLabel();
            this.lblCa2 = new MetroControl.Components.MetroLabel();
            this.lblCa1 = new MetroControl.Components.MetroLabel();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picNext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBack)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            this.panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(696, 435);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.Controls.Add(this.picNext, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.datePicker, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.picBack, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(696, 32);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // picNext
            // 
            this.picNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picNext.Image = global::MetroUserControl.Properties.Resources.right_chevron;
            this.picNext.Location = new System.Drawing.Point(667, 3);
            this.picNext.Name = "picNext";
            this.picNext.Size = new System.Drawing.Size(26, 26);
            this.picNext.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picNext.TabIndex = 3;
            this.picNext.TabStop = false;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // datePicker
            // 
            this.datePicker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.datePicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.datePicker.FontWeight = MetroFramework.MetroDateTimeWeight.Light;
            this.datePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datePicker.Location = new System.Drawing.Point(464, 3);
            this.datePicker.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.datePicker.MinimumSize = new System.Drawing.Size(0, 29);
            this.datePicker.Name = "datePicker";
            this.datePicker.Size = new System.Drawing.Size(200, 29);
            this.datePicker.TabIndex = 1;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // picBack
            // 
            this.picBack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picBack.Image = global::MetroUserControl.Properties.Resources.chevron_pointing_to_the_left;
            this.picBack.Location = new System.Drawing.Point(435, 3);
            this.picBack.Name = "picBack";
            this.picBack.Size = new System.Drawing.Size(26, 26);
            this.picBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBack.TabIndex = 2;
            this.picBack.TabStop = false;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.AllowDrop = true;
            this.tableLayoutPanel3.AutoScroll = true;
            this.tableLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel3.Controls.Add(this.panel);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 35);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.tableLayoutPanel3.Size = new System.Drawing.Size(690, 397);
            this.tableLayoutPanel3.TabIndex = 1;
            this.tableLayoutPanel3.WrapContents = false;
            // 
            // panel
            // 
            this.panel.AutoSize = true;
            this.panel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel.ColumnCount = 8;
            this.panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.panel.Controls.Add(this.lbl50, 1, 6);
            this.panel.Controls.Add(this.lbl51, 2, 6);
            this.panel.Controls.Add(this.lbl52, 3, 6);
            this.panel.Controls.Add(this.lbl53, 4, 6);
            this.panel.Controls.Add(this.lbl54, 5, 6);
            this.panel.Controls.Add(this.lbl55, 6, 6);
            this.panel.Controls.Add(this.lbl56, 7, 6);
            this.panel.Controls.Add(this.lbl40, 1, 5);
            this.panel.Controls.Add(this.lbl41, 2, 5);
            this.panel.Controls.Add(this.lbl42, 3, 5);
            this.panel.Controls.Add(this.lbl43, 4, 5);
            this.panel.Controls.Add(this.lbl44, 5, 5);
            this.panel.Controls.Add(this.lbl45, 6, 5);
            this.panel.Controls.Add(this.lbl46, 7, 5);
            this.panel.Controls.Add(this.lbl30, 1, 4);
            this.panel.Controls.Add(this.lbl31, 2, 4);
            this.panel.Controls.Add(this.lbl32, 3, 4);
            this.panel.Controls.Add(this.lbl33, 4, 4);
            this.panel.Controls.Add(this.lbl34, 5, 4);
            this.panel.Controls.Add(this.lbl35, 6, 4);
            this.panel.Controls.Add(this.lbl36, 7, 4);
            this.panel.Controls.Add(this.lbl20, 1, 3);
            this.panel.Controls.Add(this.lbl21, 2, 3);
            this.panel.Controls.Add(this.lbl22, 3, 3);
            this.panel.Controls.Add(this.lbl23, 4, 3);
            this.panel.Controls.Add(this.lbl24, 5, 3);
            this.panel.Controls.Add(this.lbl25, 6, 3);
            this.panel.Controls.Add(this.lbl26, 7, 3);
            this.panel.Controls.Add(this.lbl10, 1, 2);
            this.panel.Controls.Add(this.lbl11, 2, 2);
            this.panel.Controls.Add(this.lbl12, 3, 2);
            this.panel.Controls.Add(this.lbl13, 4, 2);
            this.panel.Controls.Add(this.lbl14, 5, 2);
            this.panel.Controls.Add(this.lbl15, 6, 2);
            this.panel.Controls.Add(this.lbl16, 7, 2);
            this.panel.Controls.Add(this.lbl05, 6, 1);
            this.panel.Controls.Add(this.lbl06, 7, 1);
            this.panel.Controls.Add(this.lbl03, 4, 1);
            this.panel.Controls.Add(this.lbl04, 5, 1);
            this.panel.Controls.Add(this.lbl02, 3, 1);
            this.panel.Controls.Add(this.lbl01, 2, 1);
            this.panel.Controls.Add(this.lbl00, 1, 1);
            this.panel.Controls.Add(this.lblThu8, 7, 0);
            this.panel.Controls.Add(this.lblThu7, 6, 0);
            this.panel.Controls.Add(this.lblThu6, 5, 0);
            this.panel.Controls.Add(this.lblThu5, 4, 0);
            this.panel.Controls.Add(this.lblThu4, 3, 0);
            this.panel.Controls.Add(this.lblThu3, 2, 0);
            this.panel.Controls.Add(this.lblThu2, 1, 0);
            this.panel.Controls.Add(this.lblCa6, 0, 6);
            this.panel.Controls.Add(this.lblCa5, 0, 5);
            this.panel.Controls.Add(this.lblCa4, 0, 4);
            this.panel.Controls.Add(this.lblCa3, 0, 3);
            this.panel.Controls.Add(this.lblCa2, 0, 2);
            this.panel.Controls.Add(this.lblCa1, 0, 1);
            this.panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel.Location = new System.Drawing.Point(0, 0);
            this.panel.Margin = new System.Windows.Forms.Padding(0);
            this.panel.Name = "panel";
            this.panel.RowCount = 7;
            this.panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.panel.Size = new System.Drawing.Size(569, 164);
            this.panel.TabIndex = 2;
            // 
            // lbl50
            // 
            this.lbl50.AutoSize = true;
            this.lbl50.BackColor = System.Drawing.SystemColors.Info;
            this.lbl50.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl50.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl50.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl50.ForeColor = System.Drawing.Color.Black;
            this.lbl50.Location = new System.Drawing.Point(101, 141);
            this.lbl50.Margin = new System.Windows.Forms.Padding(1);
            this.lbl50.Name = "lbl50";
            this.lbl50.Size = new System.Drawing.Size(64, 22);
            this.lbl50.TabIndex = 55;
            this.lbl50.Text = "00";
            this.lbl50.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl51
            // 
            this.lbl51.AutoSize = true;
            this.lbl51.BackColor = System.Drawing.SystemColors.Info;
            this.lbl51.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl51.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl51.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl51.ForeColor = System.Drawing.Color.Black;
            this.lbl51.Location = new System.Drawing.Point(167, 141);
            this.lbl51.Margin = new System.Windows.Forms.Padding(1);
            this.lbl51.Name = "lbl51";
            this.lbl51.Size = new System.Drawing.Size(64, 22);
            this.lbl51.TabIndex = 54;
            this.lbl51.Text = "00";
            this.lbl51.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl52
            // 
            this.lbl52.AutoSize = true;
            this.lbl52.BackColor = System.Drawing.SystemColors.Info;
            this.lbl52.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl52.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl52.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl52.ForeColor = System.Drawing.Color.Black;
            this.lbl52.Location = new System.Drawing.Point(233, 141);
            this.lbl52.Margin = new System.Windows.Forms.Padding(1);
            this.lbl52.Name = "lbl52";
            this.lbl52.Size = new System.Drawing.Size(64, 22);
            this.lbl52.TabIndex = 53;
            this.lbl52.Text = "00";
            this.lbl52.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl53
            // 
            this.lbl53.AutoSize = true;
            this.lbl53.BackColor = System.Drawing.SystemColors.Info;
            this.lbl53.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl53.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl53.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl53.ForeColor = System.Drawing.Color.Black;
            this.lbl53.Location = new System.Drawing.Point(299, 141);
            this.lbl53.Margin = new System.Windows.Forms.Padding(1);
            this.lbl53.Name = "lbl53";
            this.lbl53.Size = new System.Drawing.Size(64, 22);
            this.lbl53.TabIndex = 52;
            this.lbl53.Text = "00";
            this.lbl53.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl54
            // 
            this.lbl54.AutoSize = true;
            this.lbl54.BackColor = System.Drawing.SystemColors.Info;
            this.lbl54.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl54.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl54.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl54.ForeColor = System.Drawing.Color.Black;
            this.lbl54.Location = new System.Drawing.Point(365, 141);
            this.lbl54.Margin = new System.Windows.Forms.Padding(1);
            this.lbl54.Name = "lbl54";
            this.lbl54.Size = new System.Drawing.Size(64, 22);
            this.lbl54.TabIndex = 51;
            this.lbl54.Text = "00";
            this.lbl54.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl55
            // 
            this.lbl55.AutoSize = true;
            this.lbl55.BackColor = System.Drawing.SystemColors.Info;
            this.lbl55.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl55.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl55.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl55.ForeColor = System.Drawing.Color.Black;
            this.lbl55.Location = new System.Drawing.Point(431, 141);
            this.lbl55.Margin = new System.Windows.Forms.Padding(1);
            this.lbl55.Name = "lbl55";
            this.lbl55.Size = new System.Drawing.Size(64, 22);
            this.lbl55.TabIndex = 50;
            this.lbl55.Text = "00";
            this.lbl55.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl56
            // 
            this.lbl56.AutoSize = true;
            this.lbl56.BackColor = System.Drawing.SystemColors.Info;
            this.lbl56.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl56.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl56.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl56.ForeColor = System.Drawing.Color.Black;
            this.lbl56.Location = new System.Drawing.Point(497, 141);
            this.lbl56.Margin = new System.Windows.Forms.Padding(1);
            this.lbl56.Name = "lbl56";
            this.lbl56.Size = new System.Drawing.Size(71, 22);
            this.lbl56.TabIndex = 49;
            this.lbl56.Text = "00";
            this.lbl56.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl40
            // 
            this.lbl40.AutoSize = true;
            this.lbl40.BackColor = System.Drawing.Color.Azure;
            this.lbl40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl40.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl40.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl40.ForeColor = System.Drawing.Color.Black;
            this.lbl40.Location = new System.Drawing.Point(101, 123);
            this.lbl40.Margin = new System.Windows.Forms.Padding(1);
            this.lbl40.Name = "lbl40";
            this.lbl40.Size = new System.Drawing.Size(64, 16);
            this.lbl40.TabIndex = 48;
            this.lbl40.Text = "00";
            this.lbl40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl41
            // 
            this.lbl41.AutoSize = true;
            this.lbl41.BackColor = System.Drawing.Color.Azure;
            this.lbl41.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl41.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl41.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl41.ForeColor = System.Drawing.Color.Black;
            this.lbl41.Location = new System.Drawing.Point(167, 123);
            this.lbl41.Margin = new System.Windows.Forms.Padding(1);
            this.lbl41.Name = "lbl41";
            this.lbl41.Size = new System.Drawing.Size(64, 16);
            this.lbl41.TabIndex = 47;
            this.lbl41.Text = "00";
            this.lbl41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl42
            // 
            this.lbl42.AutoSize = true;
            this.lbl42.BackColor = System.Drawing.Color.Azure;
            this.lbl42.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl42.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl42.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl42.ForeColor = System.Drawing.Color.Black;
            this.lbl42.Location = new System.Drawing.Point(233, 123);
            this.lbl42.Margin = new System.Windows.Forms.Padding(1);
            this.lbl42.Name = "lbl42";
            this.lbl42.Size = new System.Drawing.Size(64, 16);
            this.lbl42.TabIndex = 46;
            this.lbl42.Text = "00";
            this.lbl42.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl43
            // 
            this.lbl43.AutoSize = true;
            this.lbl43.BackColor = System.Drawing.Color.Azure;
            this.lbl43.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl43.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl43.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl43.ForeColor = System.Drawing.Color.Black;
            this.lbl43.Location = new System.Drawing.Point(299, 123);
            this.lbl43.Margin = new System.Windows.Forms.Padding(1);
            this.lbl43.Name = "lbl43";
            this.lbl43.Size = new System.Drawing.Size(64, 16);
            this.lbl43.TabIndex = 45;
            this.lbl43.Text = "00";
            this.lbl43.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl44
            // 
            this.lbl44.AutoSize = true;
            this.lbl44.BackColor = System.Drawing.Color.Azure;
            this.lbl44.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl44.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl44.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl44.ForeColor = System.Drawing.Color.Black;
            this.lbl44.Location = new System.Drawing.Point(365, 123);
            this.lbl44.Margin = new System.Windows.Forms.Padding(1);
            this.lbl44.Name = "lbl44";
            this.lbl44.Size = new System.Drawing.Size(64, 16);
            this.lbl44.TabIndex = 44;
            this.lbl44.Text = "00";
            this.lbl44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl45
            // 
            this.lbl45.AutoSize = true;
            this.lbl45.BackColor = System.Drawing.Color.Azure;
            this.lbl45.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl45.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl45.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl45.ForeColor = System.Drawing.Color.Black;
            this.lbl45.Location = new System.Drawing.Point(431, 123);
            this.lbl45.Margin = new System.Windows.Forms.Padding(1);
            this.lbl45.Name = "lbl45";
            this.lbl45.Size = new System.Drawing.Size(64, 16);
            this.lbl45.TabIndex = 43;
            this.lbl45.Text = "00";
            this.lbl45.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl46
            // 
            this.lbl46.AutoSize = true;
            this.lbl46.BackColor = System.Drawing.Color.Azure;
            this.lbl46.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl46.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl46.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl46.ForeColor = System.Drawing.Color.Black;
            this.lbl46.Location = new System.Drawing.Point(497, 123);
            this.lbl46.Margin = new System.Windows.Forms.Padding(1);
            this.lbl46.Name = "lbl46";
            this.lbl46.Size = new System.Drawing.Size(71, 16);
            this.lbl46.TabIndex = 42;
            this.lbl46.Text = "00";
            this.lbl46.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl30
            // 
            this.lbl30.AutoSize = true;
            this.lbl30.BackColor = System.Drawing.SystemColors.Info;
            this.lbl30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl30.ForeColor = System.Drawing.Color.Black;
            this.lbl30.Location = new System.Drawing.Point(101, 105);
            this.lbl30.Margin = new System.Windows.Forms.Padding(1);
            this.lbl30.Name = "lbl30";
            this.lbl30.Size = new System.Drawing.Size(64, 16);
            this.lbl30.TabIndex = 41;
            this.lbl30.Text = "00";
            this.lbl30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl31
            // 
            this.lbl31.AutoSize = true;
            this.lbl31.BackColor = System.Drawing.SystemColors.Info;
            this.lbl31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl31.ForeColor = System.Drawing.Color.Black;
            this.lbl31.Location = new System.Drawing.Point(167, 105);
            this.lbl31.Margin = new System.Windows.Forms.Padding(1);
            this.lbl31.Name = "lbl31";
            this.lbl31.Size = new System.Drawing.Size(64, 16);
            this.lbl31.TabIndex = 40;
            this.lbl31.Text = "00";
            this.lbl31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl32
            // 
            this.lbl32.AutoSize = true;
            this.lbl32.BackColor = System.Drawing.SystemColors.Info;
            this.lbl32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl32.ForeColor = System.Drawing.Color.Black;
            this.lbl32.Location = new System.Drawing.Point(233, 105);
            this.lbl32.Margin = new System.Windows.Forms.Padding(1);
            this.lbl32.Name = "lbl32";
            this.lbl32.Size = new System.Drawing.Size(64, 16);
            this.lbl32.TabIndex = 39;
            this.lbl32.Text = "00";
            this.lbl32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl33
            // 
            this.lbl33.AutoSize = true;
            this.lbl33.BackColor = System.Drawing.SystemColors.Info;
            this.lbl33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl33.ForeColor = System.Drawing.Color.Black;
            this.lbl33.Location = new System.Drawing.Point(299, 105);
            this.lbl33.Margin = new System.Windows.Forms.Padding(1);
            this.lbl33.Name = "lbl33";
            this.lbl33.Size = new System.Drawing.Size(64, 16);
            this.lbl33.TabIndex = 38;
            this.lbl33.Text = "00";
            this.lbl33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl34
            // 
            this.lbl34.AutoSize = true;
            this.lbl34.BackColor = System.Drawing.SystemColors.Info;
            this.lbl34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl34.ForeColor = System.Drawing.Color.Black;
            this.lbl34.Location = new System.Drawing.Point(365, 105);
            this.lbl34.Margin = new System.Windows.Forms.Padding(1);
            this.lbl34.Name = "lbl34";
            this.lbl34.Size = new System.Drawing.Size(64, 16);
            this.lbl34.TabIndex = 37;
            this.lbl34.Text = "00";
            this.lbl34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl35
            // 
            this.lbl35.AutoSize = true;
            this.lbl35.BackColor = System.Drawing.SystemColors.Info;
            this.lbl35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl35.ForeColor = System.Drawing.Color.Black;
            this.lbl35.Location = new System.Drawing.Point(431, 105);
            this.lbl35.Margin = new System.Windows.Forms.Padding(1);
            this.lbl35.Name = "lbl35";
            this.lbl35.Size = new System.Drawing.Size(64, 16);
            this.lbl35.TabIndex = 36;
            this.lbl35.Text = "00";
            this.lbl35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl36
            // 
            this.lbl36.AutoSize = true;
            this.lbl36.BackColor = System.Drawing.SystemColors.Info;
            this.lbl36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl36.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl36.ForeColor = System.Drawing.Color.Black;
            this.lbl36.Location = new System.Drawing.Point(497, 105);
            this.lbl36.Margin = new System.Windows.Forms.Padding(1);
            this.lbl36.Name = "lbl36";
            this.lbl36.Size = new System.Drawing.Size(71, 16);
            this.lbl36.TabIndex = 35;
            this.lbl36.Text = "00";
            this.lbl36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl20
            // 
            this.lbl20.AutoSize = true;
            this.lbl20.BackColor = System.Drawing.Color.Azure;
            this.lbl20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl20.ForeColor = System.Drawing.Color.Black;
            this.lbl20.Location = new System.Drawing.Point(101, 87);
            this.lbl20.Margin = new System.Windows.Forms.Padding(1);
            this.lbl20.Name = "lbl20";
            this.lbl20.Size = new System.Drawing.Size(64, 16);
            this.lbl20.TabIndex = 34;
            this.lbl20.Text = "00";
            this.lbl20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl21
            // 
            this.lbl21.AutoSize = true;
            this.lbl21.BackColor = System.Drawing.Color.Azure;
            this.lbl21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl21.ForeColor = System.Drawing.Color.Black;
            this.lbl21.Location = new System.Drawing.Point(167, 87);
            this.lbl21.Margin = new System.Windows.Forms.Padding(1);
            this.lbl21.Name = "lbl21";
            this.lbl21.Size = new System.Drawing.Size(64, 16);
            this.lbl21.TabIndex = 33;
            this.lbl21.Text = "00";
            this.lbl21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl22
            // 
            this.lbl22.AutoSize = true;
            this.lbl22.BackColor = System.Drawing.Color.Azure;
            this.lbl22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl22.ForeColor = System.Drawing.Color.Black;
            this.lbl22.Location = new System.Drawing.Point(233, 87);
            this.lbl22.Margin = new System.Windows.Forms.Padding(1);
            this.lbl22.Name = "lbl22";
            this.lbl22.Size = new System.Drawing.Size(64, 16);
            this.lbl22.TabIndex = 32;
            this.lbl22.Text = "00";
            this.lbl22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl23
            // 
            this.lbl23.AutoSize = true;
            this.lbl23.BackColor = System.Drawing.Color.Azure;
            this.lbl23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl23.ForeColor = System.Drawing.Color.Black;
            this.lbl23.Location = new System.Drawing.Point(299, 87);
            this.lbl23.Margin = new System.Windows.Forms.Padding(1);
            this.lbl23.Name = "lbl23";
            this.lbl23.Size = new System.Drawing.Size(64, 16);
            this.lbl23.TabIndex = 31;
            this.lbl23.Text = "00";
            this.lbl23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl24
            // 
            this.lbl24.AutoSize = true;
            this.lbl24.BackColor = System.Drawing.Color.Azure;
            this.lbl24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl24.ForeColor = System.Drawing.Color.Black;
            this.lbl24.Location = new System.Drawing.Point(365, 87);
            this.lbl24.Margin = new System.Windows.Forms.Padding(1);
            this.lbl24.Name = "lbl24";
            this.lbl24.Size = new System.Drawing.Size(64, 16);
            this.lbl24.TabIndex = 30;
            this.lbl24.Text = "00";
            this.lbl24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl25
            // 
            this.lbl25.AutoSize = true;
            this.lbl25.BackColor = System.Drawing.Color.Azure;
            this.lbl25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl25.ForeColor = System.Drawing.Color.Black;
            this.lbl25.Location = new System.Drawing.Point(431, 87);
            this.lbl25.Margin = new System.Windows.Forms.Padding(1);
            this.lbl25.Name = "lbl25";
            this.lbl25.Size = new System.Drawing.Size(64, 16);
            this.lbl25.TabIndex = 29;
            this.lbl25.Text = "00";
            this.lbl25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl26
            // 
            this.lbl26.AutoSize = true;
            this.lbl26.BackColor = System.Drawing.Color.Azure;
            this.lbl26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl26.ForeColor = System.Drawing.Color.Black;
            this.lbl26.Location = new System.Drawing.Point(497, 87);
            this.lbl26.Margin = new System.Windows.Forms.Padding(1);
            this.lbl26.Name = "lbl26";
            this.lbl26.Size = new System.Drawing.Size(71, 16);
            this.lbl26.TabIndex = 28;
            this.lbl26.Text = "00";
            this.lbl26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl10
            // 
            this.lbl10.AutoSize = true;
            this.lbl10.BackColor = System.Drawing.SystemColors.Info;
            this.lbl10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl10.ForeColor = System.Drawing.Color.Black;
            this.lbl10.Location = new System.Drawing.Point(101, 69);
            this.lbl10.Margin = new System.Windows.Forms.Padding(1);
            this.lbl10.Name = "lbl10";
            this.lbl10.Size = new System.Drawing.Size(64, 16);
            this.lbl10.TabIndex = 27;
            this.lbl10.Text = "00";
            this.lbl10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl11
            // 
            this.lbl11.AutoSize = true;
            this.lbl11.BackColor = System.Drawing.SystemColors.Info;
            this.lbl11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl11.ForeColor = System.Drawing.Color.Black;
            this.lbl11.Location = new System.Drawing.Point(167, 69);
            this.lbl11.Margin = new System.Windows.Forms.Padding(1);
            this.lbl11.Name = "lbl11";
            this.lbl11.Size = new System.Drawing.Size(64, 16);
            this.lbl11.TabIndex = 26;
            this.lbl11.Text = "00";
            this.lbl11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl12
            // 
            this.lbl12.AutoSize = true;
            this.lbl12.BackColor = System.Drawing.SystemColors.Info;
            this.lbl12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl12.ForeColor = System.Drawing.Color.Black;
            this.lbl12.Location = new System.Drawing.Point(233, 69);
            this.lbl12.Margin = new System.Windows.Forms.Padding(1);
            this.lbl12.Name = "lbl12";
            this.lbl12.Size = new System.Drawing.Size(64, 16);
            this.lbl12.TabIndex = 25;
            this.lbl12.Text = "00";
            this.lbl12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl13
            // 
            this.lbl13.AutoSize = true;
            this.lbl13.BackColor = System.Drawing.SystemColors.Info;
            this.lbl13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl13.ForeColor = System.Drawing.Color.Black;
            this.lbl13.Location = new System.Drawing.Point(299, 69);
            this.lbl13.Margin = new System.Windows.Forms.Padding(1);
            this.lbl13.Name = "lbl13";
            this.lbl13.Size = new System.Drawing.Size(64, 16);
            this.lbl13.TabIndex = 24;
            this.lbl13.Text = "00";
            this.lbl13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl14
            // 
            this.lbl14.AutoSize = true;
            this.lbl14.BackColor = System.Drawing.SystemColors.Info;
            this.lbl14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl14.ForeColor = System.Drawing.Color.Black;
            this.lbl14.Location = new System.Drawing.Point(365, 69);
            this.lbl14.Margin = new System.Windows.Forms.Padding(1);
            this.lbl14.Name = "lbl14";
            this.lbl14.Size = new System.Drawing.Size(64, 16);
            this.lbl14.TabIndex = 23;
            this.lbl14.Text = "00";
            this.lbl14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl15
            // 
            this.lbl15.AutoSize = true;
            this.lbl15.BackColor = System.Drawing.SystemColors.Info;
            this.lbl15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl15.ForeColor = System.Drawing.Color.Black;
            this.lbl15.Location = new System.Drawing.Point(431, 69);
            this.lbl15.Margin = new System.Windows.Forms.Padding(1);
            this.lbl15.Name = "lbl15";
            this.lbl15.Size = new System.Drawing.Size(64, 16);
            this.lbl15.TabIndex = 22;
            this.lbl15.Text = "00";
            this.lbl15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl16
            // 
            this.lbl16.AutoSize = true;
            this.lbl16.BackColor = System.Drawing.SystemColors.Info;
            this.lbl16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl16.ForeColor = System.Drawing.Color.Black;
            this.lbl16.Location = new System.Drawing.Point(497, 69);
            this.lbl16.Margin = new System.Windows.Forms.Padding(1);
            this.lbl16.Name = "lbl16";
            this.lbl16.Size = new System.Drawing.Size(71, 16);
            this.lbl16.TabIndex = 21;
            this.lbl16.Text = "00";
            this.lbl16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl05
            // 
            this.lbl05.AutoSize = true;
            this.lbl05.BackColor = System.Drawing.Color.Azure;
            this.lbl05.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl05.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl05.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl05.ForeColor = System.Drawing.Color.Black;
            this.lbl05.Location = new System.Drawing.Point(431, 51);
            this.lbl05.Margin = new System.Windows.Forms.Padding(1);
            this.lbl05.Name = "lbl05";
            this.lbl05.Size = new System.Drawing.Size(64, 16);
            this.lbl05.TabIndex = 20;
            this.lbl05.Text = "00";
            this.lbl05.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl06
            // 
            this.lbl06.AutoSize = true;
            this.lbl06.BackColor = System.Drawing.Color.Azure;
            this.lbl06.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl06.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl06.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl06.ForeColor = System.Drawing.Color.Black;
            this.lbl06.Location = new System.Drawing.Point(497, 51);
            this.lbl06.Margin = new System.Windows.Forms.Padding(1);
            this.lbl06.Name = "lbl06";
            this.lbl06.Size = new System.Drawing.Size(71, 16);
            this.lbl06.TabIndex = 19;
            this.lbl06.Text = "00";
            this.lbl06.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl03
            // 
            this.lbl03.AutoSize = true;
            this.lbl03.BackColor = System.Drawing.Color.Azure;
            this.lbl03.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl03.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl03.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl03.ForeColor = System.Drawing.Color.Black;
            this.lbl03.Location = new System.Drawing.Point(299, 51);
            this.lbl03.Margin = new System.Windows.Forms.Padding(1);
            this.lbl03.Name = "lbl03";
            this.lbl03.Size = new System.Drawing.Size(64, 16);
            this.lbl03.TabIndex = 17;
            this.lbl03.Text = "00";
            this.lbl03.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl04
            // 
            this.lbl04.AutoSize = true;
            this.lbl04.BackColor = System.Drawing.Color.Azure;
            this.lbl04.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl04.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl04.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl04.ForeColor = System.Drawing.Color.Black;
            this.lbl04.Location = new System.Drawing.Point(365, 51);
            this.lbl04.Margin = new System.Windows.Forms.Padding(1);
            this.lbl04.Name = "lbl04";
            this.lbl04.Size = new System.Drawing.Size(64, 16);
            this.lbl04.TabIndex = 16;
            this.lbl04.Text = "00";
            this.lbl04.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl02
            // 
            this.lbl02.AutoSize = true;
            this.lbl02.BackColor = System.Drawing.Color.Azure;
            this.lbl02.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl02.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl02.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl02.ForeColor = System.Drawing.Color.Black;
            this.lbl02.Location = new System.Drawing.Point(233, 51);
            this.lbl02.Margin = new System.Windows.Forms.Padding(1);
            this.lbl02.Name = "lbl02";
            this.lbl02.Size = new System.Drawing.Size(64, 16);
            this.lbl02.TabIndex = 15;
            this.lbl02.Text = "00";
            this.lbl02.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl01
            // 
            this.lbl01.AutoSize = true;
            this.lbl01.BackColor = System.Drawing.Color.Azure;
            this.lbl01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl01.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl01.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl01.ForeColor = System.Drawing.Color.Black;
            this.lbl01.Location = new System.Drawing.Point(167, 51);
            this.lbl01.Margin = new System.Windows.Forms.Padding(1);
            this.lbl01.Name = "lbl01";
            this.lbl01.Size = new System.Drawing.Size(64, 16);
            this.lbl01.TabIndex = 14;
            this.lbl01.Text = "00";
            this.lbl01.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbl00
            // 
            this.lbl00.AutoSize = true;
            this.lbl00.BackColor = System.Drawing.Color.Azure;
            this.lbl00.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl00.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl00.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl00.ForeColor = System.Drawing.Color.Black;
            this.lbl00.Location = new System.Drawing.Point(101, 51);
            this.lbl00.Margin = new System.Windows.Forms.Padding(1);
            this.lbl00.Name = "lbl00";
            this.lbl00.Size = new System.Drawing.Size(64, 16);
            this.lbl00.TabIndex = 13;
            this.lbl00.Text = "00";
            this.lbl00.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lblThu8
            // 
            this.lblThu8.AutoSize = true;
            this.lblThu8.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblThu8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblThu8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblThu8.ForeColor = System.Drawing.Color.White;
            this.lblThu8.Location = new System.Drawing.Point(497, 0);
            this.lblThu8.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblThu8.Name = "lblThu8";
            this.lblThu8.Size = new System.Drawing.Size(71, 50);
            this.lblThu8.TabIndex = 12;
            this.lblThu8.Text = "CHUNHAT";
            this.lblThu8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lblThu7
            // 
            this.lblThu7.AutoSize = true;
            this.lblThu7.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblThu7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblThu7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblThu7.ForeColor = System.Drawing.Color.White;
            this.lblThu7.Location = new System.Drawing.Point(431, 0);
            this.lblThu7.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblThu7.Name = "lblThu7";
            this.lblThu7.Size = new System.Drawing.Size(64, 50);
            this.lblThu7.TabIndex = 11;
            this.lblThu7.Text = "THU7";
            this.lblThu7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lblThu6
            // 
            this.lblThu6.AutoSize = true;
            this.lblThu6.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblThu6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblThu6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblThu6.ForeColor = System.Drawing.Color.White;
            this.lblThu6.Location = new System.Drawing.Point(365, 0);
            this.lblThu6.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblThu6.Name = "lblThu6";
            this.lblThu6.Size = new System.Drawing.Size(64, 50);
            this.lblThu6.TabIndex = 10;
            this.lblThu6.Text = "THU6";
            this.lblThu6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lblThu5
            // 
            this.lblThu5.AutoSize = true;
            this.lblThu5.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblThu5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblThu5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblThu5.ForeColor = System.Drawing.Color.White;
            this.lblThu5.Location = new System.Drawing.Point(299, 0);
            this.lblThu5.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblThu5.Name = "lblThu5";
            this.lblThu5.Size = new System.Drawing.Size(64, 50);
            this.lblThu5.TabIndex = 9;
            this.lblThu5.Text = "THU5";
            this.lblThu5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lblThu4
            // 
            this.lblThu4.AutoSize = true;
            this.lblThu4.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblThu4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblThu4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblThu4.ForeColor = System.Drawing.Color.White;
            this.lblThu4.Location = new System.Drawing.Point(233, 0);
            this.lblThu4.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblThu4.Name = "lblThu4";
            this.lblThu4.Size = new System.Drawing.Size(64, 50);
            this.lblThu4.TabIndex = 8;
            this.lblThu4.Text = "THU4";
            this.lblThu4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lblThu3
            // 
            this.lblThu3.AutoSize = true;
            this.lblThu3.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblThu3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblThu3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblThu3.ForeColor = System.Drawing.Color.White;
            this.lblThu3.Location = new System.Drawing.Point(167, 0);
            this.lblThu3.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblThu3.Name = "lblThu3";
            this.lblThu3.Size = new System.Drawing.Size(64, 50);
            this.lblThu3.TabIndex = 7;
            this.lblThu3.Text = "THU3";
            this.lblThu3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lblThu2
            // 
            this.lblThu2.AutoSize = true;
            this.lblThu2.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblThu2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblThu2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblThu2.ForeColor = System.Drawing.Color.White;
            this.lblThu2.Location = new System.Drawing.Point(101, 0);
            this.lblThu2.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblThu2.Name = "lblThu2";
            this.lblThu2.Size = new System.Drawing.Size(64, 50);
            this.lblThu2.TabIndex = 6;
            this.lblThu2.Text = "THU2";
            this.lblThu2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lblCa6
            // 
            this.lblCa6.AutoSize = true;
            this.lblCa6.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblCa6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCa6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblCa6.ForeColor = System.Drawing.Color.White;
            this.lblCa6.Location = new System.Drawing.Point(0, 141);
            this.lblCa6.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.lblCa6.Name = "lblCa6";
            this.lblCa6.Size = new System.Drawing.Size(100, 22);
            this.lblCa6.TabIndex = 5;
            this.lblCa6.Text = "CA6";
            this.lblCa6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lblCa5
            // 
            this.lblCa5.AutoSize = true;
            this.lblCa5.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblCa5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCa5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblCa5.ForeColor = System.Drawing.Color.White;
            this.lblCa5.Location = new System.Drawing.Point(0, 123);
            this.lblCa5.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.lblCa5.Name = "lblCa5";
            this.lblCa5.Size = new System.Drawing.Size(100, 16);
            this.lblCa5.TabIndex = 4;
            this.lblCa5.Text = "CA5";
            this.lblCa5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lblCa4
            // 
            this.lblCa4.AutoSize = true;
            this.lblCa4.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblCa4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCa4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblCa4.ForeColor = System.Drawing.Color.White;
            this.lblCa4.Location = new System.Drawing.Point(0, 105);
            this.lblCa4.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.lblCa4.Name = "lblCa4";
            this.lblCa4.Size = new System.Drawing.Size(100, 16);
            this.lblCa4.TabIndex = 3;
            this.lblCa4.Text = "CA4";
            this.lblCa4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lblCa3
            // 
            this.lblCa3.AutoSize = true;
            this.lblCa3.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblCa3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCa3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblCa3.ForeColor = System.Drawing.Color.White;
            this.lblCa3.Location = new System.Drawing.Point(0, 87);
            this.lblCa3.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.lblCa3.Name = "lblCa3";
            this.lblCa3.Size = new System.Drawing.Size(100, 16);
            this.lblCa3.TabIndex = 2;
            this.lblCa3.Text = "CA3";
            this.lblCa3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lblCa2
            // 
            this.lblCa2.AutoSize = true;
            this.lblCa2.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblCa2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCa2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblCa2.ForeColor = System.Drawing.Color.White;
            this.lblCa2.Location = new System.Drawing.Point(0, 69);
            this.lblCa2.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.lblCa2.Name = "lblCa2";
            this.lblCa2.Size = new System.Drawing.Size(100, 16);
            this.lblCa2.TabIndex = 1;
            this.lblCa2.Text = "CA2";
            this.lblCa2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lblCa1
            // 
            this.lblCa1.AutoSize = true;
            this.lblCa1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblCa1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCa1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblCa1.ForeColor = System.Drawing.Color.White;
            this.lblCa1.Location = new System.Drawing.Point(0, 51);
            this.lblCa1.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.lblCa1.Name = "lblCa1";
            this.lblCa1.Size = new System.Drawing.Size(100, 16);
            this.lblCa1.TabIndex = 0;
            this.lblCa1.Text = "CA1";
            this.lblCa1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // MetroWeekSchedule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "MetroWeekSchedule";
            this.Size = new System.Drawing.Size(696, 435);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picNext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBack)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private MetroControl.Components.MetroPicture picNext;
        private MetroControl.Components.MetroDatePicker datePicker;
        private MetroControl.Components.MetroPicture picBack;
        private System.Windows.Forms.TableLayoutPanel panel;
        private MetroControl.Components.MetroLabel lbl50;
        private MetroControl.Components.MetroLabel lbl51;
        private MetroControl.Components.MetroLabel lbl52;
        private MetroControl.Components.MetroLabel lbl53;
        private MetroControl.Components.MetroLabel lbl54;
        private MetroControl.Components.MetroLabel lbl55;
        private MetroControl.Components.MetroLabel lbl56;
        private MetroControl.Components.MetroLabel lbl40;
        private MetroControl.Components.MetroLabel lbl41;
        private MetroControl.Components.MetroLabel lbl42;
        private MetroControl.Components.MetroLabel lbl43;
        private MetroControl.Components.MetroLabel lbl44;
        private MetroControl.Components.MetroLabel lbl45;
        private MetroControl.Components.MetroLabel lbl46;
        private MetroControl.Components.MetroLabel lbl30;
        private MetroControl.Components.MetroLabel lbl31;
        private MetroControl.Components.MetroLabel lbl32;
        private MetroControl.Components.MetroLabel lbl33;
        private MetroControl.Components.MetroLabel lbl34;
        private MetroControl.Components.MetroLabel lbl35;
        private MetroControl.Components.MetroLabel lbl36;
        private MetroControl.Components.MetroLabel lbl20;
        private MetroControl.Components.MetroLabel lbl21;
        private MetroControl.Components.MetroLabel lbl22;
        private MetroControl.Components.MetroLabel lbl23;
        private MetroControl.Components.MetroLabel lbl24;
        private MetroControl.Components.MetroLabel lbl25;
        private MetroControl.Components.MetroLabel lbl26;
        private MetroControl.Components.MetroLabel lbl10;
        private MetroControl.Components.MetroLabel lbl11;
        private MetroControl.Components.MetroLabel lbl12;
        private MetroControl.Components.MetroLabel lbl13;
        private MetroControl.Components.MetroLabel lbl14;
        private MetroControl.Components.MetroLabel lbl15;
        private MetroControl.Components.MetroLabel lbl16;
        private MetroControl.Components.MetroLabel lbl05;
        private MetroControl.Components.MetroLabel lbl06;
        private MetroControl.Components.MetroLabel lbl03;
        private MetroControl.Components.MetroLabel lbl04;
        private MetroControl.Components.MetroLabel lbl02;
        private MetroControl.Components.MetroLabel lbl01;
        private MetroControl.Components.MetroLabel lbl00;
        private MetroControl.Components.MetroLabel lblThu8;
        private MetroControl.Components.MetroLabel lblThu7;
        private MetroControl.Components.MetroLabel lblThu6;
        private MetroControl.Components.MetroLabel lblThu5;
        private MetroControl.Components.MetroLabel lblThu4;
        private MetroControl.Components.MetroLabel lblThu3;
        private MetroControl.Components.MetroLabel lblThu2;
        private MetroControl.Components.MetroLabel lblCa6;
        private MetroControl.Components.MetroLabel lblCa5;
        private MetroControl.Components.MetroLabel lblCa4;
        private MetroControl.Components.MetroLabel lblCa3;
        private MetroControl.Components.MetroLabel lblCa2;
        private MetroControl.Components.MetroLabel lblCa1;
        private System.Windows.Forms.FlowLayoutPanel tableLayoutPanel3;
    }
}
