﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroDataset;
using static MetroControl.MetroHelper;
using MetroControl;

namespace MetroUserControl.ContentMenu.Schedule.Teacher
{
    public partial class MetroListScheduleTeacher : MetroBubble
    {
        public MetroListScheduleTeacher()
        {
            InitializeComponent();
            this.VisibleChanged += MetroListScheduleTeacher_VisibleChanged;
            this.btnPrint.Click += BtnPrint_Click;
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            EventHelper.Helper.TryCatch(() =>
            {
                IndexAndKey i = MetroHelper.GetIndexAndKeyColumnFilter(cbbLoc.SelectedValue.ToString());

                BieuMau.InDanhSachLichDayGiangVien(i.Index, i.Key, txtTimKiem.Text);
            });
        }

        private void MetroListScheduleTeacher_VisibleChanged(object sender, EventArgs e)
        {
            SettingLoadByPageFilterSearch(this.gridpage, (new LoadByPage()).HienThiLichDayGiangVien, cbbLoc, (new Select()).HienThiFilterLichDayGiangVien_Cbb, picSearch, txtTimKiem);
            this.metroWeekSchedule1.SetFuncDataSource((new Select()).HienThiLichTheoTuanGiangVien);
        }
    }
}
