﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroDataset;
using MetroControl;
using MetroControl.Components;

namespace MetroUserControl.ContentMenu.Receipt
{
    public partial class MetroReceipt : MetroBubble
    {
        public enum Type
        {
            None,
            InsertReceipt,
            DetailReceipt
        }
        Type type;


        public MetroReceipt()
        {
            InitializeComponent();


        }

        public MetroReceipt SetAs(Type type, string maReceipt)
        {
            this.type = type;

            if (type == Type.DetailReceipt)
                SetupViewReceipt(maReceipt);
            else
                if (type == Type.InsertReceipt)
                SetupInsertReceipt();
            return this;
        }

        private void SetupInsertReceipt()
        {
            //xoá in biên lai
            tableBottom.Controls.Remove(btnInBienLai);
            tableBottom.SetCellPosition(btnRightReceipt, new TableLayoutPanelCellPosition(3, 0));
            tableBottom.ColumnCount = 5;
            tableBottom.Refresh();

            cbbHinhThuc.DataSource = (new Select()).HienThiTatCaLoaiBienLai_Cbb();
            cbbKhoaHoc.DataSource = (new Select()).HienThiTatCaKhoaHoc_Cbb();
            this.cbbKhoaHoc.DataSourceChanged += DataSourceChanged;
            this.cbbKhoaHoc.SelectedValueChanged += CbbKhoaHoc_SelectedIndexChanged;

            this.cbbLopHoc.DataSourceChanged += DataSourceChanged;
            this.cbbLopHoc.SelectedValueChanged += CbbLopHoc_SelectedIndexChanged;

            this.cbbDangkyKhoaHoc.DataSourceChanged += DataSourceChanged;
            this.cbbDangkyKhoaHoc.SelectedValueChanged += CbbDangkyKhoaHoc_SelectedIndexChanged;

            //FIRST
            if (cbbKhoaHoc.SelectedValue != null)
            {
                string makhoahoc = cbbKhoaHoc.SelectedValue.ToString();
                cbbLopHoc.DataSource = (new Select()).HienThiLopHocByKhoaHoc_Cbb(makhoahoc);
            }

            if (cbbLopHoc.SelectedValue != null)
            {
                string malophoc = cbbLopHoc.SelectedValue.ToString();
                if (malophoc == "CHUAXEPLOP")
                {
                    if (this.cbbKhoaHoc.SelectedValue != null)
                        cbbDangkyKhoaHoc.DataSource = (new Select()).HienThiHocVienChuaXepLop_Cbb(cbbKhoaHoc.SelectedValue.ToString());

                }
                else
                    cbbDangkyKhoaHoc.DataSource = (new Select()).HienThiHocVienLopHoc_Cbb(malophoc);
            }

            if (cbbDangkyKhoaHoc.SelectedValue != null)
            {
                string madangky = cbbDangkyKhoaHoc.SelectedValue.ToString();
                UpdateLayoutByHocVien(madangky);

                if (cbbLopHoc.SelectedValue != null)
                {
                    string malophoc = cbbLopHoc.SelectedValue.ToString();
                    UpdateLayoutByLopHoc(malophoc);
                }


                UpdateDotLapBienLai(madangky);
            }

            this.btnLeftReceipt.Text = "LẬP BIÊN LAI";
            this.btnRightReceipt.Text = "HUỶ";
            this.txtMaBienLaiReceipt.Text = (new Select()).TaoMaBienLaiTuDong();

            this.btnLeftReceipt.Click += LapBienLai;
            this.btnRightReceipt.Click += (s, e) => this.ParentForm.Close();

        }

        private void UpdateDotLapBienLai(string madangky)
        {
            //KIỂM TRA HỌC VIÊN ĐÃ TỪNG ĐÓNG HỌC PHÍ CHƯA ?
            LOAI_BIENLAI LBL = (new Select()).LayLoaiBienLaiDangKyKhoaHoc(madangky);
            if (LBL != null)
            {
                cbbHinhThuc.SelectedValue = LBL.MALOAIBIENLAI;
                cbbHinhThuc.Enabled = false;
                txtDotReceipt.Text = (new Select()).TaoDotBienLaiByMaDangKy(madangky);

            }
            else
            {
                txtDotReceipt.Text = "1";
                cbbHinhThuc.Enabled = true;
            }
        }

        private void LapBienLai(object sender, EventArgs e)
        {
            if (cbbDangkyKhoaHoc.SelectedValue != null && cbbHinhThuc.SelectedValue != null && cbbLopHoc.SelectedValue != null && cbbKhoaHoc.SelectedValue != null)
            {
                string mabienlai = txtMaBienLaiReceipt.Text;
                string maloaibienlai = cbbHinhThuc.SelectedValue.ToString();
                string madangky = cbbDangkyKhoaHoc.SelectedValue.ToString();
                string malophoc = cbbLopHoc.SelectedValue.ToString();

                string manhanvien = Helper.idUser;
                DateTime ngayxacnhan = dateXacNhan.Value;
                string sotien = txtSoTienReceipt.Text;
                string dot = txtDotReceipt.Text;
                bool huy = false;

                EventHelper.Helper.TryCatch(() =>
                {
                    if (MetroHelper.IsNullOrEmpty(new object[] { mabienlai, maloaibienlai, madangky, manhanvien, sotien, dot }))
                        EventHelper.Helper.MessageLessInformation();
                    else
                    {
                        if (Double.Parse(txtSoTienReceipt.Text) == 0)
                        {
                            EventHelper.Helper.MessageInfomation("Số tiền lập biên lai phải khác 0");
                            return;
                        }

                        if (malophoc == "CHUAXEPLOP")
                            malophoc = "";

                        //kierm tra số tiền phải đóng và số tiền cần đóng
                        double tiencandong = (new Select()).LaySoTienBienLai(madangky);
                        if (double.Parse(sotien) > tiencandong)
                        {
                            EventHelper.Helper.MessageInfomation("Số tiền học viên đóng vượt số tiền cần thu (" + tiencandong + ")");
                            return;
                        }

                        EventHelper.Helper.MessageInfomationYesNo("Đồng ý lập biên lai " + mabienlai + " ?", () =>
                        {

                            BIENLAI bl = new BIENLAI()
                            {
                                MABIENLAI = mabienlai,
                                MALOAIBIENLAI = maloaibienlai,
                                MADANGKYKHOAHOC = madangky,
                                MALOPHOC = malophoc,
                                MANHANVIEN = manhanvien,
                                NGAYXACNHAN = ngayxacnhan,
                                SOTIEN = double.Parse(sotien),
                                DOT = int.Parse(dot),
                            };

                            ResultExcuteQuery r = (new Insert()).ThemTTBienLai(bl);
                            EventHelper.Helper.MessageInfomation(r.GetMessage());
                            if (r.isSuccessfully())
                                btnRightReceipt.PerformClick();
                        });
                    }
                });

            }
        }

        private void CbbDangkyKhoaHoc_SelectedIndexChanged(object sender, EventArgs e)
        {
            MetroCombobox cbb = (sender as MetroCombobox);
            if (cbb.SelectedValue != null)
            {
                string madangky = cbb.SelectedValue.ToString();
                UpdateLayoutByHocVien(madangky);

                if (cbbLopHoc.SelectedValue != null)
                {
                    string malophoc = cbbLopHoc.SelectedValue.ToString();
                    UpdateLayoutByLopHoc(malophoc);
                }
            }

        }

        private void UpdateLayoutByLopHoc(string malophoc)
        {
            if (malophoc != "")
            {
                LOPHOC lh = (new Select()).GetClassByID(malophoc);
                GIANGVIEN gv = lh == null ? null : lh.GIANGVIEN;
                if (lh != null && gv != null)
                {
                    txtMaLopHocReceipt.Text = lh.MALOPHOC;
                    txtTenKhoaHocReceipt.Text = lh.MAKHOAHOC;
                    txtMaGiangVienReceipt.Text = gv.MAGIANGVIEN;
                    txtTenGiangVien.Text = gv.TENGIANGVIEN;
                    txtTrinhDo.Text = gv.TRINHDO;
                    txtChuyenMon.Text = gv.CHUYENMON;
                    VisibleGroupLopHoc(true);
                }
                else
                    VisibleGroupLopHoc(false);
            }
            else
                VisibleGroupLopHoc(false);
        }

        private void UpdateLayoutByHocVien(string madangky)
        {
            HOCVIEN hv = (new Select()).GetStudentByMaDangKy(madangky);

            if (hv != null)
            {
                txtMaHocVienReceipt.Text = hv.MAHOCVIEN;
                txtHoTenHocVienRecept.Text = hv.TENHOCVIEN;
                dateNgaySinh.Value = hv.NGAYSINH.Value;
                if (hv.GIOITINH.ToUpper() == "NAM")
                    rdbtnNamHocVienReceipt.Checked = true;
                else
                    rdbtnNuHocVienReceipt.Checked = true;
                txtCMNDReceipt.Text = hv.CMND;
                txtSDTReceipt.Text = hv.SODIENTHOAI;

                if (type == Type.InsertReceipt)
                    UpdateDotLapBienLai(madangky);

                VisibleGroupHocVien(true);
            }
            else
                VisibleGroupHocVien(false);
        }

        private void CbbLopHoc_SelectedIndexChanged(object sender, EventArgs e)
        {
            MetroCombobox cbb = (sender as MetroCombobox);
            if (cbb.SelectedValue != null)
            {
                string malophoc = cbb.SelectedValue.ToString();
                if (malophoc == "CHUAXEPLOP")
                {
                    if (this.cbbKhoaHoc.SelectedValue != null)
                        cbbDangkyKhoaHoc.DataSource = (new Select()).HienThiHocVienChuaXepLop_Cbb(cbbKhoaHoc.SelectedValue.ToString());
                    if (cbbDangkyKhoaHoc.Items.Count == 0)
                    {
                        VisibleGroupHocVien(false);
                        VisibleGroupLopHoc(false);
                    }
                }
                else
                    cbbDangkyKhoaHoc.DataSource = (new Select()).HienThiHocVienLopHoc_Cbb(malophoc);
            }

        }

        private void CbbKhoaHoc_SelectedIndexChanged(object sender, EventArgs e)
        {
            MetroCombobox cbb = (sender as MetroCombobox);
            if (cbb.SelectedValue != null)
            {
                string makhoahoc = cbb.SelectedValue.ToString();
                cbbLopHoc.DataSource = (new Select()).HienThiLopHocByKhoaHoc_Cbb(makhoahoc);
            }
        }

        private void DataSourceChanged(object sender, EventArgs e)
        {
            MetroCombobox cbb = (sender as MetroCombobox);
            if (cbb.DataSource != null && cbb.Items.Count != 0)
                cbb.SelectedItem = cbb.Items[0];
            else
            {
                if (cbb.Equals(cbbKhoaHoc))
                    cbbLopHoc.DataSource = null;
                else if (cbb.Equals(cbbLopHoc))
                    cbbDangkyKhoaHoc.DataSource = null;
                else if (cbb.Equals(cbbDangkyKhoaHoc))
                {
                    VisibleGroupHocVien(false);
                    VisibleGroupLopHoc(false);
                }
            }
        }

        private void SetupViewReceipt(string maReceipt)
        {
            if (maReceipt != "")
            {
                BIENLAI bl = (new Select()).LayBienLai(maReceipt);
                if (bl != null)
                {

                    txtMaBienLaiReceipt.Text = bl.MABIENLAI;

                    cbbHinhThuc.DataSource = (new Select()).HienThiTatCaLoaiBienLai_Cbb();
                    cbbHinhThuc.SelectedValue = bl.MALOAIBIENLAI;

                    cbbKhoaHoc.DataSource = (new Select()).HienThiTatCaKhoaHoc_Cbb();
                    cbbKhoaHoc.SelectedValue = bl.TT_DANGKYKHOAHOC.MAKHOAHOC;

                    cbbLopHoc.DataSource = (new Select()).HienThiLopHocByKhoaHoc_Cbb(bl.TT_DANGKYKHOAHOC.MAKHOAHOC);
                    cbbLopHoc.SelectedValue = bl.MALOPHOC.Trim() == "" ? "CHUAXEPLOP" : bl.MALOPHOC;

                    if (bl.MALOPHOC.Trim() != "")
                        cbbDangkyKhoaHoc.DataSource = (new Select()).HienThiHocVienLopHoc_Cbb(bl.MALOPHOC);
                    else
                        cbbDangkyKhoaHoc.DataSource = (new Select()).HienThiHocVienChuaXepLop_Cbb(bl.TT_DANGKYKHOAHOC.MAKHOAHOC);
                    cbbDangkyKhoaHoc.SelectedValue = bl.MADANGKYKHOAHOC;

                    txtDotReceipt.Text = bl.DOT.ToString();
                    txtSoTienReceipt.Text = bl.SOTIEN.ToString();
                    dateXacNhan.Value = bl.NGAYXACNHAN;

                    UpdateLayoutByHocVien(bl.MADANGKYKHOAHOC);

                    UpdateLayoutByLopHoc(bl.MALOPHOC);

                    btnLeftReceipt.Text = "HUỶ BIÊN LAI";
                    btnLeftReceipt.Click += HuyBienLai;
                    btnRightReceipt.Text = "HUỶ";
                    btnRightReceipt.Click += (s, e) => this.ParentForm.Close();

                    txtMaBienLaiReceipt.ReadOnly = true;
                    cbbKhoaHoc.Enabled = false;
                    cbbLopHoc.Enabled = false;
                    cbbDangkyKhoaHoc.Enabled = false;
                    cbbHinhThuc.Enabled = false;
                    txtSoTienReceipt.ReadOnly = true;
                    dateXacNhan.Enabled = false;

                    btnInBienLai.Click += (s, e) => { bool r = (BieuMau.InBienLaiThuHocPhi(bl.MABIENLAI)); };

                }
            }
        }

        private void HuyBienLai(object sender, EventArgs e)
        {
            string mabienlai = txtMaBienLaiReceipt.Text;
            string manhanvien = Helper.idUser;

            if (mabienlai != "" && manhanvien != "")
            {
                EventHelper.Helper.MessageInfomationYesNo("Bạn có chắc chắn muốn huỷ biên lai " + mabienlai + " ?", () =>
                    {
                        ResultExcuteQuery r = (new Update()).HuyBienLai(mabienlai, manhanvien);
                        EventHelper.Helper.MessageInfomation(r.GetMessage());

                        if (r.isSuccessfully())
                        {
                            this.btnRightReceipt.PerformClick();
                        }
                    });
            }
        }

        void VisibleGroupHocVien(bool visible)
        {
            grboxInfoHocVien.Visible = visible;
        }

        void VisibleGroupLopHoc(bool visible)
        {
            grboxInfoLopHoc.Visible = visible;
        }
    }
}
