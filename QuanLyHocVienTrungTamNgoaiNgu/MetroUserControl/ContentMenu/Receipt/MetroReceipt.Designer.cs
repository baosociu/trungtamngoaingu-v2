﻿namespace MetroUserControl.ContentMenu.Receipt
{
    partial class MetroReceipt
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableBottom = new System.Windows.Forms.TableLayoutPanel();
            this.btnInBienLai = new MetroControl.Components.MetroButton();
            this.btnLeftReceipt = new MetroControl.Components.MetroButton();
            this.btnRightReceipt = new MetroControl.Components.MetroButton();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.grboxInfoBienLai = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lbKhoaHoc = new MetroControl.Components.MetroLabel();
            this.cbbKhoaHoc = new MetroControl.Components.MetroCombobox();
            this.metroLabel10 = new MetroControl.Components.MetroLabel();
            this.cbbDangkyKhoaHoc = new MetroControl.Components.MetroCombobox();
            this.cbbLopHoc = new MetroControl.Components.MetroCombobox();
            this.metroLabel3 = new MetroControl.Components.MetroLabel();
            this.txtMaBienLaiReceipt = new MetroControl.Components.MetroTextbox();
            this.cbbHinhThuc = new MetroControl.Components.MetroCombobox();
            this.metroLabel11 = new MetroControl.Components.MetroLabel();
            this.txtDotReceipt = new MetroControl.Components.MetroTextbox();
            this.txtSoTienReceipt = new MetroControl.Components.MetroTextbox();
            this.dateXacNhan = new MetroControl.Components.MetroDatePicker();
            this.lblHinhThuc = new MetroControl.Components.MetroLabel();
            this.metroLabel5 = new MetroControl.Components.MetroLabel();
            this.metroLabel6 = new MetroControl.Components.MetroLabel();
            this.metroLabel7 = new MetroControl.Components.MetroLabel();
            this.grboxInfoHocVien = new System.Windows.Forms.GroupBox();
            this.tablePanelInfo = new System.Windows.Forms.TableLayoutPanel();
            this.lbMaHocVienReceipt = new MetroControl.Components.MetroLabel();
            this.txtSDTReceipt = new MetroControl.Components.MetroTextbox();
            this.txtCMNDReceipt = new MetroControl.Components.MetroTextbox();
            this.lbSoDienThoaiReceipt = new MetroControl.Components.MetroLabel();
            this.lbCMNDReceipt = new MetroControl.Components.MetroLabel();
            this.lbGioiTinhReceipt = new MetroControl.Components.MetroLabel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.rdbtnNuHocVienReceipt = new System.Windows.Forms.RadioButton();
            this.rdbtnNamHocVienReceipt = new System.Windows.Forms.RadioButton();
            this.lbNgaySinhReceipt = new MetroControl.Components.MetroLabel();
            this.txtHoTenHocVienRecept = new MetroControl.Components.MetroTextbox();
            this.lbHoVaTenReceipt = new MetroControl.Components.MetroLabel();
            this.dateNgaySinh = new MetroControl.Components.MetroDatePicker();
            this.txtMaHocVienReceipt = new MetroControl.Components.MetroTextbox();
            this.grboxInfoLopHoc = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.txtChuyenMon = new MetroControl.Components.MetroTextbox();
            this.metroLabel9 = new MetroControl.Components.MetroLabel();
            this.metroLabel8 = new MetroControl.Components.MetroLabel();
            this.metroLabel4 = new MetroControl.Components.MetroLabel();
            this.metroLabel17 = new MetroControl.Components.MetroLabel();
            this.lbTenGiangVienReceipt = new MetroControl.Components.MetroLabel();
            this.txtTenKhoaHocReceipt = new MetroControl.Components.MetroTextbox();
            this.lbTenKhoaHoc = new MetroControl.Components.MetroLabel();
            this.txtMaLopHocReceipt = new MetroControl.Components.MetroTextbox();
            this.txtMaGiangVienReceipt = new MetroControl.Components.MetroTextbox();
            this.txtTenGiangVien = new MetroControl.Components.MetroTextbox();
            this.txtTrinhDo = new MetroControl.Components.MetroTextbox();
            this.metroLabel2 = new MetroControl.Components.MetroLabel();
            this.metroLabel15 = new MetroControl.Components.MetroLabel();
            this.tableBottom.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.grboxInfoBienLai.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.grboxInfoHocVien.SuspendLayout();
            this.tablePanelInfo.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.grboxInfoLopHoc.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableBottom
            // 
            this.tableBottom.ColumnCount = 7;
            this.tableLayoutPanel2.SetColumnSpan(this.tableBottom, 3);
            this.tableBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableBottom.Controls.Add(this.btnInBienLai, 3, 0);
            this.tableBottom.Controls.Add(this.btnLeftReceipt, 1, 0);
            this.tableBottom.Controls.Add(this.btnRightReceipt, 5, 0);
            this.tableBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableBottom.Location = new System.Drawing.Point(0, 414);
            this.tableBottom.Margin = new System.Windows.Forms.Padding(0);
            this.tableBottom.Name = "tableBottom";
            this.tableBottom.RowCount = 1;
            this.tableBottom.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableBottom.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableBottom.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableBottom.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableBottom.Size = new System.Drawing.Size(660, 36);
            this.tableBottom.TabIndex = 23;
            // 
            // btnInBienLai
            // 
            this.btnInBienLai.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnInBienLai.Location = new System.Drawing.Point(283, 3);
            this.btnInBienLai.Name = "btnInBienLai";
            this.btnInBienLai.Size = new System.Drawing.Size(94, 30);
            this.btnInBienLai.TabIndex = 2;
            this.btnInBienLai.Text = "IN BIÊN LAI";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnInBienLai.UseSelectable = true;
            // 
            // btnLeftReceipt
            // 
            this.btnLeftReceipt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnLeftReceipt.Location = new System.Drawing.Point(93, 3);
            this.btnLeftReceipt.Name = "btnLeftReceipt";
            this.btnLeftReceipt.Size = new System.Drawing.Size(94, 30);
            this.btnLeftReceipt.TabIndex = 0;
            this.btnLeftReceipt.Text = "LEFT";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnLeftReceipt.UseSelectable = true;
            // 
            // btnRightReceipt
            // 
            this.btnRightReceipt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnRightReceipt.Location = new System.Drawing.Point(473, 3);
            this.btnRightReceipt.Name = "btnRightReceipt";
            this.btnRightReceipt.Size = new System.Drawing.Size(94, 30);
            this.btnRightReceipt.TabIndex = 1;
            this.btnRightReceipt.Text = "RIGHT";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnRightReceipt.UseSelectable = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 47.27273F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.242424F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.48485F));
            this.tableLayoutPanel2.Controls.Add(this.tableBottom, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.grboxInfoBienLai, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.grboxInfoHocVien, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.grboxInfoLopHoc, 2, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 177F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(660, 450);
            this.tableLayoutPanel2.TabIndex = 3;
            // 
            // grboxInfoBienLai
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.grboxInfoBienLai, 3);
            this.grboxInfoBienLai.Controls.Add(this.tableLayoutPanel1);
            this.grboxInfoBienLai.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grboxInfoBienLai.Location = new System.Drawing.Point(3, 3);
            this.grboxInfoBienLai.Name = "grboxInfoBienLai";
            this.grboxInfoBienLai.Size = new System.Drawing.Size(654, 171);
            this.grboxInfoBienLai.TabIndex = 24;
            this.grboxInfoBienLai.TabStop = false;
            this.grboxInfoBienLai.Text = "Thông tin biên lai";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 7;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.598607F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 38.05679F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.25424F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 39.49176F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.598607F));
            this.tableLayoutPanel1.Controls.Add(this.lbKhoaHoc, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.cbbKhoaHoc, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel10, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.cbbDangkyKhoaHoc, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.cbbLopHoc, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel3, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtMaBienLaiReceipt, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.cbbHinhThuc, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel11, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtDotReceipt, 5, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtSoTienReceipt, 5, 3);
            this.tableLayoutPanel1.Controls.Add(this.dateXacNhan, 5, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblHinhThuc, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel5, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel6, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel7, 4, 4);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(648, 152);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lbKhoaHoc
            // 
            this.lbKhoaHoc.AutoSize = true;
            this.lbKhoaHoc.BackColor = System.Drawing.Color.Transparent;
            this.lbKhoaHoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbKhoaHoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbKhoaHoc.IsFocus = false;
            this.lbKhoaHoc.Location = new System.Drawing.Point(19, 44);
            this.lbKhoaHoc.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.lbKhoaHoc.Name = "lbKhoaHoc";
            this.lbKhoaHoc.Size = new System.Drawing.Size(87, 32);
            this.lbKhoaHoc.TabIndex = 50;
            this.lbKhoaHoc.Text = "Khoá Học:";
            this.lbKhoaHoc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // cbbKhoaHoc
            // 
            this.cbbKhoaHoc.DisplayMember = "Value";
            this.cbbKhoaHoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbbKhoaHoc.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbbKhoaHoc.FormattingEnabled = true;
            this.cbbKhoaHoc.ItemHeight = 19;
            this.cbbKhoaHoc.Location = new System.Drawing.Point(116, 47);
            this.cbbKhoaHoc.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.cbbKhoaHoc.Name = "cbbKhoaHoc";
            this.cbbKhoaHoc.Size = new System.Drawing.Size(170, 25);
            this.cbbKhoaHoc.TabIndex = 49;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.cbbKhoaHoc.UseSelectable = true;
            this.cbbKhoaHoc.ValueMember = "Key";
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel10.IsFocus = false;
            this.metroLabel10.Location = new System.Drawing.Point(19, 76);
            this.metroLabel10.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(87, 32);
            this.metroLabel10.TabIndex = 48;
            this.metroLabel10.Text = "Lớp Học:";
            this.metroLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // cbbDangkyKhoaHoc
            // 
            this.cbbDangkyKhoaHoc.DisplayMember = "Value";
            this.cbbDangkyKhoaHoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbbDangkyKhoaHoc.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbbDangkyKhoaHoc.FormattingEnabled = true;
            this.cbbDangkyKhoaHoc.ItemHeight = 19;
            this.cbbDangkyKhoaHoc.Location = new System.Drawing.Point(116, 111);
            this.cbbDangkyKhoaHoc.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.cbbDangkyKhoaHoc.Name = "cbbDangkyKhoaHoc";
            this.cbbDangkyKhoaHoc.Size = new System.Drawing.Size(170, 25);
            this.cbbDangkyKhoaHoc.TabIndex = 47;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.cbbDangkyKhoaHoc.UseSelectable = true;
            this.cbbDangkyKhoaHoc.ValueMember = "Key";
            // 
            // cbbLopHoc
            // 
            this.cbbLopHoc.DisplayMember = "Value";
            this.cbbLopHoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbbLopHoc.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbbLopHoc.FormattingEnabled = true;
            this.cbbLopHoc.ItemHeight = 19;
            this.cbbLopHoc.Location = new System.Drawing.Point(116, 79);
            this.cbbLopHoc.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.cbbLopHoc.Name = "cbbLopHoc";
            this.cbbLopHoc.Size = new System.Drawing.Size(170, 25);
            this.cbbLopHoc.TabIndex = 46;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.cbbLopHoc.UseSelectable = true;
            this.cbbLopHoc.ValueMember = "Key";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel3.IsFocus = false;
            this.metroLabel3.Location = new System.Drawing.Point(19, 108);
            this.metroLabel3.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(87, 32);
            this.metroLabel3.TabIndex = 45;
            this.metroLabel3.Text = "Học Viên:";
            this.metroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtMaBienLaiReceipt
            // 
            this.txtMaBienLaiReceipt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaBienLaiReceipt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtMaBienLaiReceipt.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtMaBienLaiReceipt.Location = new System.Drawing.Point(116, 16);
            this.txtMaBienLaiReceipt.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtMaBienLaiReceipt.Name = "txtMaBienLaiReceipt";
            this.txtMaBienLaiReceipt.ReadOnly = true;
            this.txtMaBienLaiReceipt.Size = new System.Drawing.Size(170, 23);
            this.txtMaBienLaiReceipt.TabIndex = 8;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // cbbHinhThuc
            // 
            this.cbbHinhThuc.DisplayMember = "Value";
            this.cbbHinhThuc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbbHinhThuc.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbbHinhThuc.FormattingEnabled = true;
            this.cbbHinhThuc.ItemHeight = 19;
            this.cbbHinhThuc.Location = new System.Drawing.Point(454, 15);
            this.cbbHinhThuc.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.cbbHinhThuc.Name = "cbbHinhThuc";
            this.cbbHinhThuc.Size = new System.Drawing.Size(176, 25);
            this.cbbHinhThuc.TabIndex = 11;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.cbbHinhThuc.UseSelectable = true;
            this.cbbHinhThuc.ValueMember = "Key";
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel11.IsFocus = false;
            this.metroLabel11.Location = new System.Drawing.Point(19, 12);
            this.metroLabel11.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(87, 32);
            this.metroLabel11.TabIndex = 37;
            this.metroLabel11.Text = "Mã Biên Lai:";
            this.metroLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtDotReceipt
            // 
            this.txtDotReceipt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDotReceipt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtDotReceipt.InputType = MetroControl.Components.MetroTextbox.Input.NumberInput;
            this.txtDotReceipt.Location = new System.Drawing.Point(454, 48);
            this.txtDotReceipt.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtDotReceipt.Name = "txtDotReceipt";
            this.txtDotReceipt.ReadOnly = true;
            this.txtDotReceipt.Size = new System.Drawing.Size(176, 23);
            this.txtDotReceipt.TabIndex = 12;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtSoTienReceipt
            // 
            this.txtSoTienReceipt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSoTienReceipt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtSoTienReceipt.InputType = MetroControl.Components.MetroTextbox.Input.NumberInput;
            this.txtSoTienReceipt.Location = new System.Drawing.Point(454, 80);
            this.txtSoTienReceipt.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtSoTienReceipt.Name = "txtSoTienReceipt";
            this.txtSoTienReceipt.Size = new System.Drawing.Size(176, 23);
            this.txtSoTienReceipt.TabIndex = 13;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // dateXacNhan
            // 
            this.dateXacNhan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateXacNhan.Enabled = false;
            this.dateXacNhan.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.dateXacNhan.FontSize = MetroFramework.MetroDateTimeSize.Small;
            this.dateXacNhan.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateXacNhan.Location = new System.Drawing.Point(454, 111);
            this.dateXacNhan.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.dateXacNhan.MinimumSize = new System.Drawing.Size(0, 25);
            this.dateXacNhan.Name = "dateXacNhan";
            this.dateXacNhan.Size = new System.Drawing.Size(176, 25);
            this.dateXacNhan.TabIndex = 15;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lblHinhThuc
            // 
            this.lblHinhThuc.AutoSize = true;
            this.lblHinhThuc.BackColor = System.Drawing.Color.Transparent;
            this.lblHinhThuc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblHinhThuc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblHinhThuc.IsFocus = false;
            this.lblHinhThuc.Location = new System.Drawing.Point(357, 12);
            this.lblHinhThuc.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.lblHinhThuc.Name = "lblHinhThuc";
            this.lblHinhThuc.Size = new System.Drawing.Size(87, 32);
            this.lblHinhThuc.TabIndex = 38;
            this.lblHinhThuc.Text = "Hình Thức:";
            this.lblHinhThuc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel5.IsFocus = false;
            this.metroLabel5.Location = new System.Drawing.Point(357, 44);
            this.metroLabel5.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(87, 32);
            this.metroLabel5.TabIndex = 41;
            this.metroLabel5.Text = "Đợt:";
            this.metroLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel6.IsFocus = true;
            this.metroLabel6.Location = new System.Drawing.Point(357, 76);
            this.metroLabel6.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(87, 32);
            this.metroLabel6.TabIndex = 42;
            this.metroLabel6.Text = "Số Tiền(*):";
            this.metroLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel7.IsFocus = false;
            this.metroLabel7.Location = new System.Drawing.Point(357, 108);
            this.metroLabel7.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(87, 32);
            this.metroLabel7.TabIndex = 43;
            this.metroLabel7.Text = "Ngày Lập:";
            this.metroLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // grboxInfoHocVien
            // 
            this.grboxInfoHocVien.Controls.Add(this.tablePanelInfo);
            this.grboxInfoHocVien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grboxInfoHocVien.Location = new System.Drawing.Point(3, 180);
            this.grboxInfoHocVien.Name = "grboxInfoHocVien";
            this.grboxInfoHocVien.Size = new System.Drawing.Size(306, 231);
            this.grboxInfoHocVien.TabIndex = 27;
            this.grboxInfoHocVien.TabStop = false;
            this.grboxInfoHocVien.Text = "Thông tin học viên";
            // 
            // tablePanelInfo
            // 
            this.tablePanelInfo.ColumnCount = 4;
            this.tablePanelInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tablePanelInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tablePanelInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.33333F));
            this.tablePanelInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tablePanelInfo.Controls.Add(this.lbMaHocVienReceipt, 1, 1);
            this.tablePanelInfo.Controls.Add(this.txtSDTReceipt, 2, 6);
            this.tablePanelInfo.Controls.Add(this.txtCMNDReceipt, 2, 5);
            this.tablePanelInfo.Controls.Add(this.lbSoDienThoaiReceipt, 1, 6);
            this.tablePanelInfo.Controls.Add(this.lbCMNDReceipt, 1, 5);
            this.tablePanelInfo.Controls.Add(this.lbGioiTinhReceipt, 1, 4);
            this.tablePanelInfo.Controls.Add(this.tableLayoutPanel3, 2, 4);
            this.tablePanelInfo.Controls.Add(this.lbNgaySinhReceipt, 1, 3);
            this.tablePanelInfo.Controls.Add(this.txtHoTenHocVienRecept, 2, 2);
            this.tablePanelInfo.Controls.Add(this.lbHoVaTenReceipt, 1, 2);
            this.tablePanelInfo.Controls.Add(this.dateNgaySinh, 2, 3);
            this.tablePanelInfo.Controls.Add(this.txtMaHocVienReceipt, 2, 1);
            this.tablePanelInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePanelInfo.Location = new System.Drawing.Point(3, 16);
            this.tablePanelInfo.Name = "tablePanelInfo";
            this.tablePanelInfo.RowCount = 8;
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tablePanelInfo.Size = new System.Drawing.Size(300, 212);
            this.tablePanelInfo.TabIndex = 0;
            // 
            // lbMaHocVienReceipt
            // 
            this.lbMaHocVienReceipt.AutoSize = true;
            this.lbMaHocVienReceipt.BackColor = System.Drawing.Color.Transparent;
            this.lbMaHocVienReceipt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbMaHocVienReceipt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbMaHocVienReceipt.IsFocus = false;
            this.lbMaHocVienReceipt.Location = new System.Drawing.Point(19, 10);
            this.lbMaHocVienReceipt.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.lbMaHocVienReceipt.Name = "lbMaHocVienReceipt";
            this.lbMaHocVienReceipt.Size = new System.Drawing.Size(87, 32);
            this.lbMaHocVienReceipt.TabIndex = 38;
            this.lbMaHocVienReceipt.Text = "Mã Học Viên:";
            this.lbMaHocVienReceipt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtSDTReceipt
            // 
            this.txtSDTReceipt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSDTReceipt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtSDTReceipt.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtSDTReceipt.Location = new System.Drawing.Point(116, 174);
            this.txtSDTReceipt.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtSDTReceipt.Name = "txtSDTReceipt";
            this.txtSDTReceipt.ReadOnly = true;
            this.txtSDTReceipt.Size = new System.Drawing.Size(166, 23);
            this.txtSDTReceipt.TabIndex = 42;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtCMNDReceipt
            // 
            this.txtCMNDReceipt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCMNDReceipt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtCMNDReceipt.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtCMNDReceipt.Location = new System.Drawing.Point(116, 142);
            this.txtCMNDReceipt.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtCMNDReceipt.Name = "txtCMNDReceipt";
            this.txtCMNDReceipt.ReadOnly = true;
            this.txtCMNDReceipt.Size = new System.Drawing.Size(166, 23);
            this.txtCMNDReceipt.TabIndex = 41;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbSoDienThoaiReceipt
            // 
            this.lbSoDienThoaiReceipt.AutoSize = true;
            this.lbSoDienThoaiReceipt.BackColor = System.Drawing.Color.Transparent;
            this.lbSoDienThoaiReceipt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbSoDienThoaiReceipt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbSoDienThoaiReceipt.IsFocus = false;
            this.lbSoDienThoaiReceipt.Location = new System.Drawing.Point(16, 170);
            this.lbSoDienThoaiReceipt.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.lbSoDienThoaiReceipt.Name = "lbSoDienThoaiReceipt";
            this.lbSoDienThoaiReceipt.Size = new System.Drawing.Size(90, 32);
            this.lbSoDienThoaiReceipt.TabIndex = 40;
            this.lbSoDienThoaiReceipt.Text = "Số Điện Thoại:";
            this.lbSoDienThoaiReceipt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbCMNDReceipt
            // 
            this.lbCMNDReceipt.AutoSize = true;
            this.lbCMNDReceipt.BackColor = System.Drawing.Color.Transparent;
            this.lbCMNDReceipt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbCMNDReceipt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbCMNDReceipt.IsFocus = false;
            this.lbCMNDReceipt.Location = new System.Drawing.Point(16, 138);
            this.lbCMNDReceipt.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.lbCMNDReceipt.Name = "lbCMNDReceipt";
            this.lbCMNDReceipt.Size = new System.Drawing.Size(90, 32);
            this.lbCMNDReceipt.TabIndex = 39;
            this.lbCMNDReceipt.Text = "CMND:";
            this.lbCMNDReceipt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbGioiTinhReceipt
            // 
            this.lbGioiTinhReceipt.AutoSize = true;
            this.lbGioiTinhReceipt.BackColor = System.Drawing.Color.Transparent;
            this.lbGioiTinhReceipt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbGioiTinhReceipt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbGioiTinhReceipt.IsFocus = false;
            this.lbGioiTinhReceipt.Location = new System.Drawing.Point(16, 106);
            this.lbGioiTinhReceipt.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.lbGioiTinhReceipt.Name = "lbGioiTinhReceipt";
            this.lbGioiTinhReceipt.Size = new System.Drawing.Size(90, 32);
            this.lbGioiTinhReceipt.TabIndex = 23;
            this.lbGioiTinhReceipt.Text = "Giới Tính:";
            this.lbGioiTinhReceipt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.rdbtnNuHocVienReceipt, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.rdbtnNamHocVienReceipt, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(116, 106);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(166, 32);
            this.tableLayoutPanel3.TabIndex = 22;
            // 
            // rdbtnNuHocVienReceipt
            // 
            this.rdbtnNuHocVienReceipt.AutoSize = true;
            this.rdbtnNuHocVienReceipt.BackColor = System.Drawing.Color.Transparent;
            this.rdbtnNuHocVienReceipt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rdbtnNuHocVienReceipt.Enabled = false;
            this.rdbtnNuHocVienReceipt.Location = new System.Drawing.Point(83, 0);
            this.rdbtnNuHocVienReceipt.Margin = new System.Windows.Forms.Padding(0);
            this.rdbtnNuHocVienReceipt.Name = "rdbtnNuHocVienReceipt";
            this.rdbtnNuHocVienReceipt.Size = new System.Drawing.Size(83, 32);
            this.rdbtnNuHocVienReceipt.TabIndex = 20;
            this.rdbtnNuHocVienReceipt.Text = "Nữ";
            this.rdbtnNuHocVienReceipt.UseVisualStyleBackColor = false;
            // 
            // rdbtnNamHocVienReceipt
            // 
            this.rdbtnNamHocVienReceipt.AutoSize = true;
            this.rdbtnNamHocVienReceipt.BackColor = System.Drawing.Color.Transparent;
            this.rdbtnNamHocVienReceipt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rdbtnNamHocVienReceipt.Enabled = false;
            this.rdbtnNamHocVienReceipt.Location = new System.Drawing.Point(0, 0);
            this.rdbtnNamHocVienReceipt.Margin = new System.Windows.Forms.Padding(0);
            this.rdbtnNamHocVienReceipt.Name = "rdbtnNamHocVienReceipt";
            this.rdbtnNamHocVienReceipt.Size = new System.Drawing.Size(83, 32);
            this.rdbtnNamHocVienReceipt.TabIndex = 19;
            this.rdbtnNamHocVienReceipt.Text = "Nam";
            this.rdbtnNamHocVienReceipt.UseVisualStyleBackColor = false;
            // 
            // lbNgaySinhReceipt
            // 
            this.lbNgaySinhReceipt.AutoSize = true;
            this.lbNgaySinhReceipt.BackColor = System.Drawing.Color.Transparent;
            this.lbNgaySinhReceipt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbNgaySinhReceipt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbNgaySinhReceipt.IsFocus = false;
            this.lbNgaySinhReceipt.Location = new System.Drawing.Point(16, 74);
            this.lbNgaySinhReceipt.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.lbNgaySinhReceipt.Name = "lbNgaySinhReceipt";
            this.lbNgaySinhReceipt.Size = new System.Drawing.Size(90, 32);
            this.lbNgaySinhReceipt.TabIndex = 14;
            this.lbNgaySinhReceipt.Text = "Ngày Sinh:";
            this.lbNgaySinhReceipt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtHoTenHocVienRecept
            // 
            this.txtHoTenHocVienRecept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHoTenHocVienRecept.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtHoTenHocVienRecept.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtHoTenHocVienRecept.Location = new System.Drawing.Point(116, 46);
            this.txtHoTenHocVienRecept.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtHoTenHocVienRecept.Name = "txtHoTenHocVienRecept";
            this.txtHoTenHocVienRecept.ReadOnly = true;
            this.txtHoTenHocVienRecept.Size = new System.Drawing.Size(166, 23);
            this.txtHoTenHocVienRecept.TabIndex = 13;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbHoVaTenReceipt
            // 
            this.lbHoVaTenReceipt.AutoSize = true;
            this.lbHoVaTenReceipt.BackColor = System.Drawing.Color.Transparent;
            this.lbHoVaTenReceipt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbHoVaTenReceipt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbHoVaTenReceipt.IsFocus = false;
            this.lbHoVaTenReceipt.Location = new System.Drawing.Point(16, 42);
            this.lbHoVaTenReceipt.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.lbHoVaTenReceipt.Name = "lbHoVaTenReceipt";
            this.lbHoVaTenReceipt.Size = new System.Drawing.Size(90, 32);
            this.lbHoVaTenReceipt.TabIndex = 5;
            this.lbHoVaTenReceipt.Text = "Họ Và Tên:";
            this.lbHoVaTenReceipt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // dateNgaySinh
            // 
            this.dateNgaySinh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateNgaySinh.Enabled = false;
            this.dateNgaySinh.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.dateNgaySinh.FontSize = MetroFramework.MetroDateTimeSize.Small;
            this.dateNgaySinh.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateNgaySinh.Location = new System.Drawing.Point(116, 77);
            this.dateNgaySinh.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.dateNgaySinh.MinimumSize = new System.Drawing.Size(0, 25);
            this.dateNgaySinh.Name = "dateNgaySinh";
            this.dateNgaySinh.Size = new System.Drawing.Size(166, 25);
            this.dateNgaySinh.TabIndex = 25;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtMaHocVienReceipt
            // 
            this.txtMaHocVienReceipt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaHocVienReceipt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtMaHocVienReceipt.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtMaHocVienReceipt.Location = new System.Drawing.Point(116, 14);
            this.txtMaHocVienReceipt.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtMaHocVienReceipt.Name = "txtMaHocVienReceipt";
            this.txtMaHocVienReceipt.ReadOnly = true;
            this.txtMaHocVienReceipt.Size = new System.Drawing.Size(166, 23);
            this.txtMaHocVienReceipt.TabIndex = 43;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // grboxInfoLopHoc
            // 
            this.grboxInfoLopHoc.Controls.Add(this.tableLayoutPanel5);
            this.grboxInfoLopHoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grboxInfoLopHoc.Location = new System.Drawing.Point(342, 180);
            this.grboxInfoLopHoc.Name = "grboxInfoLopHoc";
            this.grboxInfoLopHoc.Size = new System.Drawing.Size(315, 231);
            this.grboxInfoLopHoc.TabIndex = 27;
            this.grboxInfoLopHoc.TabStop = false;
            this.grboxInfoLopHoc.Text = "Thông tin lớp học";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 4;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.33333F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel5.Controls.Add(this.txtChuyenMon, 2, 6);
            this.tableLayoutPanel5.Controls.Add(this.metroLabel9, 1, 6);
            this.tableLayoutPanel5.Controls.Add(this.metroLabel8, 1, 5);
            this.tableLayoutPanel5.Controls.Add(this.metroLabel4, 1, 4);
            this.tableLayoutPanel5.Controls.Add(this.metroLabel17, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.lbTenGiangVienReceipt, 1, 3);
            this.tableLayoutPanel5.Controls.Add(this.txtTenKhoaHocReceipt, 2, 2);
            this.tableLayoutPanel5.Controls.Add(this.lbTenKhoaHoc, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.txtMaLopHocReceipt, 2, 1);
            this.tableLayoutPanel5.Controls.Add(this.txtMaGiangVienReceipt, 2, 3);
            this.tableLayoutPanel5.Controls.Add(this.txtTenGiangVien, 2, 4);
            this.tableLayoutPanel5.Controls.Add(this.txtTrinhDo, 2, 5);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 8;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(309, 212);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // txtChuyenMon
            // 
            this.txtChuyenMon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtChuyenMon.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtChuyenMon.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtChuyenMon.Location = new System.Drawing.Point(126, 174);
            this.txtChuyenMon.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtChuyenMon.Name = "txtChuyenMon";
            this.txtChuyenMon.ReadOnly = true;
            this.txtChuyenMon.Size = new System.Drawing.Size(165, 23);
            this.txtChuyenMon.TabIndex = 44;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel9.IsFocus = false;
            this.metroLabel9.Location = new System.Drawing.Point(19, 170);
            this.metroLabel9.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(97, 32);
            this.metroLabel9.TabIndex = 43;
            this.metroLabel9.Text = "Chuyên Môn:";
            this.metroLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel8.IsFocus = false;
            this.metroLabel8.Location = new System.Drawing.Point(19, 138);
            this.metroLabel8.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(97, 32);
            this.metroLabel8.TabIndex = 41;
            this.metroLabel8.Text = "Trình Độ:";
            this.metroLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel4.IsFocus = false;
            this.metroLabel4.Location = new System.Drawing.Point(19, 106);
            this.metroLabel4.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(97, 32);
            this.metroLabel4.TabIndex = 39;
            this.metroLabel4.Text = "Tên Giảng Viên:";
            this.metroLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel17
            // 
            this.metroLabel17.AutoSize = true;
            this.metroLabel17.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel17.IsFocus = false;
            this.metroLabel17.Location = new System.Drawing.Point(19, 10);
            this.metroLabel17.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel17.Name = "metroLabel17";
            this.metroLabel17.Size = new System.Drawing.Size(97, 32);
            this.metroLabel17.TabIndex = 37;
            this.metroLabel17.Text = "Mã Lớp Học:";
            this.metroLabel17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbTenGiangVienReceipt
            // 
            this.lbTenGiangVienReceipt.AutoSize = true;
            this.lbTenGiangVienReceipt.BackColor = System.Drawing.Color.Transparent;
            this.lbTenGiangVienReceipt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbTenGiangVienReceipt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbTenGiangVienReceipt.IsFocus = false;
            this.lbTenGiangVienReceipt.Location = new System.Drawing.Point(16, 74);
            this.lbTenGiangVienReceipt.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.lbTenGiangVienReceipt.Name = "lbTenGiangVienReceipt";
            this.lbTenGiangVienReceipt.Size = new System.Drawing.Size(100, 32);
            this.lbTenGiangVienReceipt.TabIndex = 14;
            this.lbTenGiangVienReceipt.Text = "Giảng Viên:";
            this.lbTenGiangVienReceipt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtTenKhoaHocReceipt
            // 
            this.txtTenKhoaHocReceipt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTenKhoaHocReceipt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtTenKhoaHocReceipt.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtTenKhoaHocReceipt.Location = new System.Drawing.Point(126, 46);
            this.txtTenKhoaHocReceipt.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtTenKhoaHocReceipt.Name = "txtTenKhoaHocReceipt";
            this.txtTenKhoaHocReceipt.ReadOnly = true;
            this.txtTenKhoaHocReceipt.Size = new System.Drawing.Size(165, 23);
            this.txtTenKhoaHocReceipt.TabIndex = 13;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lbTenKhoaHoc
            // 
            this.lbTenKhoaHoc.AutoSize = true;
            this.lbTenKhoaHoc.BackColor = System.Drawing.Color.Transparent;
            this.lbTenKhoaHoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbTenKhoaHoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbTenKhoaHoc.IsFocus = false;
            this.lbTenKhoaHoc.Location = new System.Drawing.Point(16, 42);
            this.lbTenKhoaHoc.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.lbTenKhoaHoc.Name = "lbTenKhoaHoc";
            this.lbTenKhoaHoc.Size = new System.Drawing.Size(100, 32);
            this.lbTenKhoaHoc.TabIndex = 5;
            this.lbTenKhoaHoc.Text = "Khóa Học:";
            this.lbTenKhoaHoc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtMaLopHocReceipt
            // 
            this.txtMaLopHocReceipt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaLopHocReceipt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtMaLopHocReceipt.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtMaLopHocReceipt.Location = new System.Drawing.Point(126, 14);
            this.txtMaLopHocReceipt.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtMaLopHocReceipt.Name = "txtMaLopHocReceipt";
            this.txtMaLopHocReceipt.ReadOnly = true;
            this.txtMaLopHocReceipt.Size = new System.Drawing.Size(165, 23);
            this.txtMaLopHocReceipt.TabIndex = 38;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtMaGiangVienReceipt
            // 
            this.txtMaGiangVienReceipt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaGiangVienReceipt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtMaGiangVienReceipt.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtMaGiangVienReceipt.Location = new System.Drawing.Point(126, 78);
            this.txtMaGiangVienReceipt.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtMaGiangVienReceipt.Name = "txtMaGiangVienReceipt";
            this.txtMaGiangVienReceipt.ReadOnly = true;
            this.txtMaGiangVienReceipt.Size = new System.Drawing.Size(165, 23);
            this.txtMaGiangVienReceipt.TabIndex = 38;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtTenGiangVien
            // 
            this.txtTenGiangVien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTenGiangVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtTenGiangVien.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtTenGiangVien.Location = new System.Drawing.Point(126, 110);
            this.txtTenGiangVien.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtTenGiangVien.Name = "txtTenGiangVien";
            this.txtTenGiangVien.ReadOnly = true;
            this.txtTenGiangVien.Size = new System.Drawing.Size(165, 23);
            this.txtTenGiangVien.TabIndex = 40;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtTrinhDo
            // 
            this.txtTrinhDo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTrinhDo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtTrinhDo.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtTrinhDo.Location = new System.Drawing.Point(126, 142);
            this.txtTrinhDo.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtTrinhDo.Name = "txtTrinhDo";
            this.txtTrinhDo.ReadOnly = true;
            this.txtTrinhDo.Size = new System.Drawing.Size(165, 23);
            this.txtTrinhDo.TabIndex = 42;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel2.IsFocus = false;
            this.metroLabel2.Location = new System.Drawing.Point(19, 8);
            this.metroLabel2.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(80, 15);
            this.metroLabel2.TabIndex = 37;
            this.metroLabel2.Text = "Mã Học Viên:";
            this.metroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel15
            // 
            this.metroLabel15.AutoSize = true;
            this.metroLabel15.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel15.IsFocus = false;
            this.metroLabel15.Location = new System.Drawing.Point(21, 12);
            this.metroLabel15.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel15.Name = "metroLabel15";
            this.metroLabel15.Size = new System.Drawing.Size(80, 15);
            this.metroLabel15.TabIndex = 37;
            this.metroLabel15.Text = "Mã Học Viên:";
            this.metroLabel15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // MetroReceipt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel2);
            this.Name = "MetroReceipt";
            this.Size = new System.Drawing.Size(660, 450);
            this.tableBottom.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.grboxInfoBienLai.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.grboxInfoHocVien.ResumeLayout(false);
            this.tablePanelInfo.ResumeLayout(false);
            this.tablePanelInfo.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.grboxInfoLopHoc.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableBottom;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private MetroControl.Components.MetroButton btnLeftReceipt;
        private MetroControl.Components.MetroButton btnRightReceipt;
        private System.Windows.Forms.GroupBox grboxInfoBienLai;
        private System.Windows.Forms.GroupBox grboxInfoHocVien;
        private System.Windows.Forms.TableLayoutPanel tablePanelInfo;
        private MetroControl.Components.MetroTextbox txtSDTReceipt;
        private MetroControl.Components.MetroTextbox txtCMNDReceipt;
        private MetroControl.Components.MetroLabel lbSoDienThoaiReceipt;
        private MetroControl.Components.MetroLabel lbCMNDReceipt;
        private MetroControl.Components.MetroLabel lbGioiTinhReceipt;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.RadioButton rdbtnNuHocVienReceipt;
        private System.Windows.Forms.RadioButton rdbtnNamHocVienReceipt;
        private MetroControl.Components.MetroLabel lbNgaySinhReceipt;
        private MetroControl.Components.MetroTextbox txtHoTenHocVienRecept;
        private MetroControl.Components.MetroLabel lbHoVaTenReceipt;
        private MetroControl.Components.MetroDatePicker dateNgaySinh;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroControl.Components.MetroTextbox txtMaBienLaiReceipt;
        private MetroControl.Components.MetroCombobox cbbHinhThuc;
        private MetroControl.Components.MetroLabel metroLabel11;
        private MetroControl.Components.MetroTextbox txtDotReceipt;
        private MetroControl.Components.MetroTextbox txtSoTienReceipt;
        private MetroControl.Components.MetroDatePicker dateXacNhan;
        private MetroControl.Components.MetroLabel lblHinhThuc;
        private MetroControl.Components.MetroLabel metroLabel5;
        private MetroControl.Components.MetroLabel metroLabel6;
        private MetroControl.Components.MetroLabel metroLabel7;
        private MetroControl.Components.MetroLabel lbMaHocVienReceipt;
        private System.Windows.Forms.GroupBox grboxInfoLopHoc;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private MetroControl.Components.MetroLabel metroLabel17;
        private MetroControl.Components.MetroTextbox txtTenKhoaHocReceipt;
        private MetroControl.Components.MetroLabel lbTenKhoaHoc;
        private MetroControl.Components.MetroTextbox txtMaLopHocReceipt;
        private MetroControl.Components.MetroLabel metroLabel2;
        private MetroControl.Components.MetroLabel metroLabel15;
        private MetroControl.Components.MetroTextbox txtMaHocVienReceipt;
        private MetroControl.Components.MetroCombobox cbbDangkyKhoaHoc;
        private MetroControl.Components.MetroCombobox cbbLopHoc;
        private MetroControl.Components.MetroLabel metroLabel3;
        private MetroControl.Components.MetroTextbox txtChuyenMon;
        private MetroControl.Components.MetroLabel metroLabel9;
        private MetroControl.Components.MetroLabel metroLabel8;
        private MetroControl.Components.MetroLabel metroLabel4;
        private MetroControl.Components.MetroLabel lbTenGiangVienReceipt;
        private MetroControl.Components.MetroTextbox txtMaGiangVienReceipt;
        private MetroControl.Components.MetroTextbox txtTenGiangVien;
        private MetroControl.Components.MetroTextbox txtTrinhDo;
        private MetroControl.Components.MetroCombobox cbbKhoaHoc;
        private MetroControl.Components.MetroLabel metroLabel10;
        private MetroControl.Components.MetroLabel lbKhoaHoc;
        private MetroControl.Components.MetroButton btnInBienLai;
    }
}
