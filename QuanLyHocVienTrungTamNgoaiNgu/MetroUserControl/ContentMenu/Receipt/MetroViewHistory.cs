﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroDataset;

namespace MetroUserControl.ContentMenu.Receipt
{
    public partial class MetroViewHistory : MetroBubble
    {
        public MetroViewHistory()
        {
            InitializeComponent();
            this.VisibleChanged += MetroListReceipt_VisibleChanged;
            this.Load += MetroListReceipt_Load;
        }

        private void MetroListReceipt_VisibleChanged(object sender, EventArgs e)
        {
            this.SettingLoadByPageFilterSearch(this.grdReceipt, (new LoadByPage()).HienThiLichSuHuyBienLai, cbbLoc, (new Select()).HienThiFilterLichSuHuyBienLai_Cbb, picSearch, txtTimKiem);
        }

        private void MetroListReceipt_Load(object sender, EventArgs e)
        {
            this.btnCancel.Click += (s, en) => this.ParentForm.Close();
        }
    }
}
