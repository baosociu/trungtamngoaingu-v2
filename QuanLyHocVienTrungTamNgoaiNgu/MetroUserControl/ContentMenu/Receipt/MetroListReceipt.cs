﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroControl;
using MetroDataset;

namespace MetroUserControl.ContentMenu.Receipt
{
    public partial class MetroListReceipt : MetroBubble
    {
        public MetroListReceipt()
        {
            InitializeComponent();
            this.VisibleChanged += MetroListReceipt_VisibleChanged;
            this.Load += MetroListReceipt_Load;


            //btn
            
        }

        private void MetroListReceipt_VisibleChanged(object sender, EventArgs e)
        {
            this.SettingLoadByPageFilterSearch(this.grdReceipt, (new LoadByPage()).HienThiBienLai, cbbLoc, (new Select()).HienThiFilterBienLai_Cbb, picSearch, txtTimKiem);
        }

        private void MetroListReceipt_Load(object sender, EventArgs e)
        {
            EventHelper.Helper.SetDoubleClickItemGridView(grdReceipt.grid, this.btnXemChiTiet.PerformClick);
            this.btnLapBienLai.Click += (s, en) => MetroHelper.OpenNewTab(this, new MetroReceipt().SetAs(MetroReceipt.Type.InsertReceipt,""));
            this.btnXemChiTiet.Click += (s, en) =>
            {
                string mabienlai = grdReceipt.grid.CurrentRow.Cells[0].Value.ToString();
                MetroHelper.OpenNewTab(this, new MetroReceipt().SetAs(MetroReceipt.Type.DetailReceipt, mabienlai));
            };
            this.btnViewHistory.Click += (s, en) => MetroHelper.OpenNewTab(this, new MetroViewHistory());

            this.btnLapBienLai.TypeData.Add(MetroControl.Components.Type.Insert);
            this.VisibleByPermission();
        }
    }
}
