﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroDataset;
using static MetroControl.MetroHelper;
using MetroControl;

namespace MetroUserControl.ContentMenu.Account
{
    public partial class MetroListAccount : MetroBubble
    {
        public MetroListAccount()
        {
            InitializeComponent();
            this.Load += MetroListAccount_Load;
            this.VisibleChanged += MetroListAccount_VisibleChanged;
        }

        private void MetroListAccount_VisibleChanged(object sender, EventArgs e)
        {
            this.SettingLoadByPageFilterSearch(this.gridAccount, (new LoadByPage()).HienThiTaiKhoanNguoiDung, this.cbbLoc, (new Select()).HienThiFilterTaiKhoan_Cbb, picSearch, txtTimKiem);

        }

        private void MetroListAccount_Load(object sender, EventArgs e)
        {


            this.gridAccount.grid.CellContentClick += Grid_CellContentClick;
            this.btnXoa.Click += BtnXoa_Click;
            EventHelper.Helper.SetDoubleClickItemGridView(gridAccount.grid, () => { if (btnXoa.Visible) ThayDoiHoatDong(gridAccount.grid.CurrentCell.RowIndex, 3); });


            this.btnXoa.TypeData.Add(MetroControl.Components.Type.Delete);
            this.VisibleByPermission();
        }

        private void ThayDoiHoatDong(int row, int col)
        {
            bool b = !bool.Parse(gridAccount.grid[col, row].Value.ToString());
            string str = b ? "mở" : "khoá";
            EventHelper.Helper.MessageInfomationYesNo("Đồng ý \'" + str + "\' tài khoản ?", () =>
            {
                gridAccount.grid[col, row].Value = b;
                string mataikhoan = gridAccount.grid.Rows[row].Cells[0].Value.ToString();
                (new MetroDataset.Update()).CapNhatHoatDongTaiKhoanNguoiDung(mataikhoan, b);
            });
        }



        private void BtnXoa_Click(object sender, EventArgs e)
        {
            EventHelper.Helper.TryCatch(() =>
            {
                string mataikhoan = gridAccount.grid.CurrentRow.Cells[0].Value.ToString();

                if ((MetroHelper.isStaffByID(mataikhoan) && !isMaster()) || (isMaster() && MetroHelper.isMasterByID(mataikhoan)))
                {
                    EventHelper.Helper.MessageLessPermision();
                    return;
                }
                EventHelper.Helper.MessageInfomationYesNo("Đồng ý xoá tài khoản \'" + mataikhoan + "\' ?", () =>
                {
                    ResultExcuteQuery r = (new MetroDataset.Delete()).XoaTaiKhoan(mataikhoan);
                    EventHelper.Helper.MessageInfomation(r.GetMessage());
                    if (r.isSuccessfully())
                        ReloadData(this.gridAccount.grid, false);
                });
            });

        }

        private void Grid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3 && e.RowIndex != -1)
            {
                string mataikhoan = gridAccount.grid.Rows[e.RowIndex].Cells[0].Value.ToString();


                if ((MetroHelper.isStaffByID(mataikhoan) && !isMaster()) || (isMaster() && MetroHelper.isMasterByID(mataikhoan)))
                {
                    EventHelper.Helper.MessageLessPermision();
                    return;
                }

                ThayDoiHoatDong(e.RowIndex, e.ColumnIndex);
            }
        }
    }
}
