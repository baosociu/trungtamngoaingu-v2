﻿namespace MetroUserControl.ContentMenu.User.Student
{
    partial class MetroDetailStudent
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lblMaHocVien = new MetroControl.Components.MetroLabel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.picboxHocVien = new MetroControl.Components.MetroPicture();
            this.gBoxContact = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.txtEmailHocVien = new MetroControl.Components.MetroTextbox();
            this.metroLabel7 = new MetroControl.Components.MetroLabel();
            this.metroLabel6 = new MetroControl.Components.MetroLabel();
            this.metroLabel9 = new MetroControl.Components.MetroLabel();
            this.txtDienThoaiHocVien = new MetroControl.Components.MetroTextbox();
            this.txtDiaChiHocVien = new MetroControl.Components.MetroTextbox();
            this.tablePanelBottom = new System.Windows.Forms.TableLayoutPanel();
            this.btnLeft = new MetroControl.Components.MetroButton();
            this.btnRight = new MetroControl.Components.MetroButton();
            this.gboxInfo = new System.Windows.Forms.GroupBox();
            this.tablePanelInfo = new System.Windows.Forms.TableLayoutPanel();
            this.metroLabel5 = new MetroControl.Components.MetroLabel();
            this.metroLabel2 = new MetroControl.Components.MetroLabel();
            this.txtCMNDHocVien = new MetroControl.Components.MetroTextbox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.rdbtnNuHocVien = new System.Windows.Forms.RadioButton();
            this.rdbtnNamHocVien = new System.Windows.Forms.RadioButton();
            this.metroLabel1 = new MetroControl.Components.MetroLabel();
            this.txtHoTenHocVien = new MetroControl.Components.MetroTextbox();
            this.metroLabel3 = new MetroControl.Components.MetroLabel();
            this.pickerNgay = new MetroControl.Components.MetroDatePicker();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxHocVien)).BeginInit();
            this.gBoxContact.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tablePanelBottom.SuspendLayout();
            this.gboxInfo.SuspendLayout();
            this.tablePanelInfo.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.gBoxContact, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tablePanelBottom, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.gboxInfo, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(753, 416);
            this.tableLayoutPanel1.TabIndex = 36;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel2.Controls.Add(this.lblMaHocVien, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 1, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(454, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 8;
            this.tableLayoutPanel1.SetRowSpan(this.tableLayoutPanel2, 2);
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(296, 314);
            this.tableLayoutPanel2.TabIndex = 29;
            // 
            // lblMaHocVien
            // 
            this.lblMaHocVien.AutoSize = true;
            this.lblMaHocVien.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.SetColumnSpan(this.lblMaHocVien, 2);
            this.lblMaHocVien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMaHocVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblMaHocVien.IsFocus = false;
            this.lblMaHocVien.Location = new System.Drawing.Point(19, 224);
            this.lblMaHocVien.Name = "lblMaHocVien";
            this.lblMaHocVien.Size = new System.Drawing.Size(257, 90);
            this.lblMaHocVien.TabIndex = 0;
            this.lblMaHocVien.Text = "Mã Học Viên: ";
            this.lblMaHocVien.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel2.SetColumnSpan(this.tableLayoutPanel4, 2);
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 141F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.picboxHocVien, 1, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(19, 35);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel2.SetRowSpan(this.tableLayoutPanel4, 6);
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 188F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(257, 186);
            this.tableLayoutPanel4.TabIndex = 14;
            // 
            // picboxHocVien
            // 
            this.picboxHocVien.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picboxHocVien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picboxHocVien.Image = global::MetroUserControl.Properties.Resources.user_shape;
            this.picboxHocVien.Location = new System.Drawing.Point(61, 2);
            this.picboxHocVien.Name = "picboxHocVien";
            this.picboxHocVien.Size = new System.Drawing.Size(135, 182);
            this.picboxHocVien.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picboxHocVien.TabIndex = 17;
            this.picboxHocVien.TabStop = false;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // gBoxContact
            // 
            this.gBoxContact.Controls.Add(this.tableLayoutPanel5);
            this.gBoxContact.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gBoxContact.Location = new System.Drawing.Point(3, 183);
            this.gBoxContact.Name = "gBoxContact";
            this.gBoxContact.Size = new System.Drawing.Size(445, 134);
            this.gBoxContact.TabIndex = 28;
            this.gBoxContact.TabStop = false;
            this.gBoxContact.Text = "Thông tin liên lạc";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 4;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.33333F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel5.Controls.Add(this.txtEmailHocVien, 2, 3);
            this.tableLayoutPanel5.Controls.Add(this.metroLabel7, 1, 3);
            this.tableLayoutPanel5.Controls.Add(this.metroLabel6, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.metroLabel9, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.txtDienThoaiHocVien, 2, 2);
            this.tableLayoutPanel5.Controls.Add(this.txtDiaChiHocVien, 2, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 5;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(439, 115);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // txtEmailHocVien
            // 
            this.txtEmailHocVien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEmailHocVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtEmailHocVien.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtEmailHocVien.Location = new System.Drawing.Point(137, 77);
            this.txtEmailHocVien.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtEmailHocVien.Name = "txtEmailHocVien";
            this.txtEmailHocVien.Size = new System.Drawing.Size(274, 23);
            this.txtEmailHocVien.TabIndex = 2;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel7.IsFocus = true;
            this.metroLabel7.Location = new System.Drawing.Point(30, 73);
            this.metroLabel7.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(97, 32);
            this.metroLabel7.TabIndex = 27;
            this.metroLabel7.Text = "Email(*):";
            this.metroLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel6.IsFocus = false;
            this.metroLabel6.Location = new System.Drawing.Point(30, 41);
            this.metroLabel6.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(97, 32);
            this.metroLabel6.TabIndex = 26;
            this.metroLabel6.Text = "Số Điện Thoại:";
            this.metroLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel9.IsFocus = false;
            this.metroLabel9.Location = new System.Drawing.Point(30, 9);
            this.metroLabel9.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(97, 32);
            this.metroLabel9.TabIndex = 25;
            this.metroLabel9.Text = "Địa Chỉ:";
            this.metroLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtDienThoaiHocVien
            // 
            this.txtDienThoaiHocVien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDienThoaiHocVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtDienThoaiHocVien.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtDienThoaiHocVien.Location = new System.Drawing.Point(137, 45);
            this.txtDienThoaiHocVien.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtDienThoaiHocVien.Name = "txtDienThoaiHocVien";
            this.txtDienThoaiHocVien.Size = new System.Drawing.Size(274, 23);
            this.txtDienThoaiHocVien.TabIndex = 1;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtDiaChiHocVien
            // 
            this.txtDiaChiHocVien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDiaChiHocVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtDiaChiHocVien.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtDiaChiHocVien.Location = new System.Drawing.Point(137, 13);
            this.txtDiaChiHocVien.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtDiaChiHocVien.Name = "txtDiaChiHocVien";
            this.txtDiaChiHocVien.Size = new System.Drawing.Size(274, 23);
            this.txtDiaChiHocVien.TabIndex = 0;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // tablePanelBottom
            // 
            this.tablePanelBottom.ColumnCount = 5;
            this.tableLayoutPanel1.SetColumnSpan(this.tablePanelBottom, 2);
            this.tablePanelBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tablePanelBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tablePanelBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tablePanelBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tablePanelBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tablePanelBottom.Controls.Add(this.btnLeft, 1, 0);
            this.tablePanelBottom.Controls.Add(this.btnRight, 3, 0);
            this.tablePanelBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePanelBottom.Location = new System.Drawing.Point(0, 380);
            this.tablePanelBottom.Margin = new System.Windows.Forms.Padding(0);
            this.tablePanelBottom.Name = "tablePanelBottom";
            this.tablePanelBottom.RowCount = 1;
            this.tablePanelBottom.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tablePanelBottom.Size = new System.Drawing.Size(753, 36);
            this.tablePanelBottom.TabIndex = 26;
            // 
            // btnLeft
            // 
            this.btnLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnLeft.Location = new System.Drawing.Point(187, 3);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Size = new System.Drawing.Size(94, 30);
            this.btnLeft.TabIndex = 0;
            this.btnLeft.Text = "LEFT";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnLeft.UseSelectable = true;
            // 
            // btnRight
            // 
            this.btnRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnRight.Location = new System.Drawing.Point(471, 3);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(94, 30);
            this.btnRight.TabIndex = 1;
            this.btnRight.Text = "RIGHT";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnRight.UseSelectable = true;
            // 
            // gboxInfo
            // 
            this.gboxInfo.Controls.Add(this.tablePanelInfo);
            this.gboxInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gboxInfo.Location = new System.Drawing.Point(3, 3);
            this.gboxInfo.Name = "gboxInfo";
            this.gboxInfo.Size = new System.Drawing.Size(445, 174);
            this.gboxInfo.TabIndex = 25;
            this.gboxInfo.TabStop = false;
            this.gboxInfo.Text = "Thông tin cá nhân";
            // 
            // tablePanelInfo
            // 
            this.tablePanelInfo.ColumnCount = 4;
            this.tablePanelInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tablePanelInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tablePanelInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.33334F));
            this.tablePanelInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tablePanelInfo.Controls.Add(this.metroLabel5, 1, 4);
            this.tablePanelInfo.Controls.Add(this.metroLabel2, 1, 3);
            this.tablePanelInfo.Controls.Add(this.txtCMNDHocVien, 2, 4);
            this.tablePanelInfo.Controls.Add(this.tableLayoutPanel3, 2, 3);
            this.tablePanelInfo.Controls.Add(this.metroLabel1, 1, 2);
            this.tablePanelInfo.Controls.Add(this.txtHoTenHocVien, 2, 1);
            this.tablePanelInfo.Controls.Add(this.metroLabel3, 1, 1);
            this.tablePanelInfo.Controls.Add(this.pickerNgay, 2, 2);
            this.tablePanelInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePanelInfo.Location = new System.Drawing.Point(3, 16);
            this.tablePanelInfo.Name = "tablePanelInfo";
            this.tablePanelInfo.RowCount = 6;
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tablePanelInfo.Size = new System.Drawing.Size(439, 155);
            this.tablePanelInfo.TabIndex = 0;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel5.IsFocus = true;
            this.metroLabel5.Location = new System.Drawing.Point(27, 109);
            this.metroLabel5.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(100, 32);
            this.metroLabel5.TabIndex = 24;
            this.metroLabel5.Text = "Số CMND(*):";
            this.metroLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel2.IsFocus = false;
            this.metroLabel2.Location = new System.Drawing.Point(27, 77);
            this.metroLabel2.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(100, 32);
            this.metroLabel2.TabIndex = 23;
            this.metroLabel2.Text = "Giới Tính:";
            this.metroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtCMNDHocVien
            // 
            this.txtCMNDHocVien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCMNDHocVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtCMNDHocVien.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtCMNDHocVien.Location = new System.Drawing.Point(137, 113);
            this.txtCMNDHocVien.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtCMNDHocVien.Name = "txtCMNDHocVien";
            this.txtCMNDHocVien.Size = new System.Drawing.Size(274, 23);
            this.txtCMNDHocVien.TabIndex = 2;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.rdbtnNuHocVien, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.rdbtnNamHocVien, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(137, 77);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(274, 32);
            this.tableLayoutPanel3.TabIndex = 22;
            // 
            // rdbtnNuHocVien
            // 
            this.rdbtnNuHocVien.AutoSize = true;
            this.rdbtnNuHocVien.BackColor = System.Drawing.Color.Transparent;
            this.rdbtnNuHocVien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rdbtnNuHocVien.Location = new System.Drawing.Point(137, 0);
            this.rdbtnNuHocVien.Margin = new System.Windows.Forms.Padding(0);
            this.rdbtnNuHocVien.Name = "rdbtnNuHocVien";
            this.rdbtnNuHocVien.Size = new System.Drawing.Size(137, 32);
            this.rdbtnNuHocVien.TabIndex = 1;
            this.rdbtnNuHocVien.Text = "Nữ";
            this.rdbtnNuHocVien.UseVisualStyleBackColor = false;
            // 
            // rdbtnNamHocVien
            // 
            this.rdbtnNamHocVien.AutoSize = true;
            this.rdbtnNamHocVien.BackColor = System.Drawing.Color.Transparent;
            this.rdbtnNamHocVien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rdbtnNamHocVien.Location = new System.Drawing.Point(0, 0);
            this.rdbtnNamHocVien.Margin = new System.Windows.Forms.Padding(0);
            this.rdbtnNamHocVien.Name = "rdbtnNamHocVien";
            this.rdbtnNamHocVien.Size = new System.Drawing.Size(137, 32);
            this.rdbtnNamHocVien.TabIndex = 0;
            this.rdbtnNamHocVien.Text = "Nam";
            this.rdbtnNamHocVien.UseVisualStyleBackColor = false;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel1.IsFocus = true;
            this.metroLabel1.Location = new System.Drawing.Point(27, 45);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(100, 32);
            this.metroLabel1.TabIndex = 14;
            this.metroLabel1.Text = "Ngày Sinh(*):";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtHoTenHocVien
            // 
            this.txtHoTenHocVien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHoTenHocVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtHoTenHocVien.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtHoTenHocVien.Location = new System.Drawing.Point(137, 17);
            this.txtHoTenHocVien.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtHoTenHocVien.Name = "txtHoTenHocVien";
            this.txtHoTenHocVien.Size = new System.Drawing.Size(274, 23);
            this.txtHoTenHocVien.TabIndex = 0;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel3.IsFocus = true;
            this.metroLabel3.Location = new System.Drawing.Point(27, 13);
            this.metroLabel3.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(100, 32);
            this.metroLabel3.TabIndex = 5;
            this.metroLabel3.Text = "Họ Và Tên(*):";
            this.metroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // pickerNgay
            // 
            this.pickerNgay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pickerNgay.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.pickerNgay.FontSize = MetroFramework.MetroDateTimeSize.Small;
            this.pickerNgay.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.pickerNgay.Location = new System.Drawing.Point(137, 48);
            this.pickerNgay.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.pickerNgay.MinimumSize = new System.Drawing.Size(0, 25);
            this.pickerNgay.Name = "pickerNgay";
            this.pickerNgay.Size = new System.Drawing.Size(274, 25);
            this.pickerNgay.TabIndex = 1;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // MetroDetailStudent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "MetroDetailStudent";
            this.Size = new System.Drawing.Size(753, 416);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picboxHocVien)).EndInit();
            this.gBoxContact.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tablePanelBottom.ResumeLayout(false);
            this.gboxInfo.ResumeLayout(false);
            this.tablePanelInfo.ResumeLayout(false);
            this.tablePanelInfo.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tablePanelBottom;
        private MetroControl.Components.MetroButton btnLeft;
        private MetroControl.Components.MetroButton btnRight;
        private System.Windows.Forms.GroupBox gboxInfo;
        private System.Windows.Forms.TableLayoutPanel tablePanelInfo;
        private MetroControl.Components.MetroLabel metroLabel5;
        private MetroControl.Components.MetroLabel metroLabel2;
        private MetroControl.Components.MetroTextbox txtCMNDHocVien;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.RadioButton rdbtnNuHocVien;
        private System.Windows.Forms.RadioButton rdbtnNamHocVien;
        private MetroControl.Components.MetroLabel metroLabel1;
        private MetroControl.Components.MetroTextbox txtHoTenHocVien;
        private MetroControl.Components.MetroLabel metroLabel3;
        private System.Windows.Forms.GroupBox gBoxContact;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private MetroControl.Components.MetroLabel metroLabel6;
        private MetroControl.Components.MetroLabel metroLabel9;
        private MetroControl.Components.MetroTextbox txtDienThoaiHocVien;
        private MetroControl.Components.MetroTextbox txtDiaChiHocVien;
        private MetroControl.Components.MetroTextbox txtEmailHocVien;
        private MetroControl.Components.MetroLabel metroLabel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private MetroControl.Components.MetroLabel lblMaHocVien;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private MetroControl.Components.MetroPicture picboxHocVien;
        private MetroControl.Components.MetroDatePicker pickerNgay;
    }
}
