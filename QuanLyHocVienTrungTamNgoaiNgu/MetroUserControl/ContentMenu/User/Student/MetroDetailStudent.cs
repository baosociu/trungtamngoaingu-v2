﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroControl;
using MetroControl.Components;
using MetroDataset;
using System.Data.Linq;

namespace MetroUserControl.ContentMenu.User.Student
{
    public partial class MetroDetailStudent : MetroBubble
    {
        HOCVIEN _hocvien;
        OpenFileDialog openFileDialog;
        public enum Type
        {
            InsertStudent, //btnleft = Them - btnRight = Huy
            UpdateStudent, //btnLeft = CapNhat - btnRight = Huy
            ChangePass //btnLeft = CapNhAT - btnRight = ChangePass
        }
        Type? type = null;

        public MetroDetailStudent()
        {
            InitializeComponent();
            EventHelper.Helper.SetHandHover(this.picboxHocVien);
            openFileDialog = MetroHelper.createAvatarOpenFileDialog();
            this.picboxHocVien.Click += (s, e) => MetroHelper.OpenChoseAvatar(openFileDialog, picboxHocVien);

        }


        public MetroDetailStudent  SetID(string ma)
        {
            _hocvien = (new Select()).GetStudentByID(ma);
            settingLayoutByStudent();
            return this;
        }
        public MetroDetailStudent SetAs(Type type)
        {
            if (this.type == type)
                return this;
            this.type = type;

            if (type == Type.InsertStudent)
            {
                //giao diện thêm học viên
                this.btnLeft.Text = "THÊM";
                this.btnLeft.TypeData.AddType(MetroControl.Components.Type.Insert);
                this.btnLeft.Click += BtnInsert_Click;

                this.btnRight.Text = "HUỶ";
                this.btnRight.TypeData.AddType(MetroControl.Components.Type.View);
                this.btnRight.Click += BtnCancel_Click;
                //layout
                lblMaHocVien.Text = "Mã Học Viên: " + (new Select()).TaoMaHocVienTuDong();

            }
            else if (type == Type.UpdateStudent)
            {
                //giao diện cập nhật thông tin
                this.btnLeft.Text = "CẬP NHẬT";
                this.btnLeft.TypeData.AddType(MetroControl.Components.Type.Update);
                this.btnLeft.Click += BtnUpdate_Click;

                this.btnRight.Text = "HUỶ";
                this.btnRight.TypeData.AddType(MetroControl.Components.Type.View);
                this.btnRight.Click += BtnCancel_Click;

                //không cho nhập liệu một số thuộc tính
                txtCMNDHocVien.ReadOnly = true;
                txtHoTenHocVien.ReadOnly = true;

            }
            else if (type == Type.ChangePass)
            {
                //giao diện cập nhật thông tin
                this.btnLeft.Text = "CẬP NHẬT";
                this.btnLeft.TypeData.AddType(MetroControl.Components.Type.Update);
                this.btnLeft.Click += BtnUpdate_Click;

                this.btnRight.Text = "ĐỔI MẬT KHẨU";
                this.btnRight.TypeData.AddType(MetroControl.Components.Type.Update);
                this.btnRight.Click += BtnChangePass_Click;

                //không cho nhập liệu một số thuộc tính
                txtCMNDHocVien.ReadOnly = true;
                txtHoTenHocVien.ReadOnly = true;

                _hocvien = MetroHelper.student;
                settingLayoutByStudent();
            }
            this.tablePanelBottom.Refresh();
            return this;

        }

        //TODO: Left button
        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            string GIOITINH = rdbtnNamHocVien.Checked ? "NAM" : "NỮ";
            string SODIENTHOAI = txtDienThoaiHocVien.Text;
            string TENHOCVIEN = txtHoTenHocVien.Text;
            string DIACHI = txtDiaChiHocVien.Text;
            string CMND = txtCMNDHocVien.Text;
            string MAIL = txtEmailHocVien.Text;
            DateTime NGAYSINH = pickerNgay.Value;
            Binary HINH = MetroHelper.ImageToBinary(picboxHocVien.Image);
            //kiểm tra rỗng
            if (MetroHelper.IsNullOrEmpty(new object[] { txtDienThoaiHocVien.Text, txtDiaChiHocVien.Text, txtCMNDHocVien.Text, pickerNgay.Value }))
            {
                EventHelper.Helper.MessageLessInformation();
                return;
            }
            
            //kiểm tra thay đổi
            if(GIOITINH == _hocvien.GIOITINH && SODIENTHOAI == _hocvien.SODIENTHOAI && TENHOCVIEN == _hocvien.TENHOCVIEN
                && DIACHI == _hocvien.DIACHI && CMND == _hocvien.CMND && MAIL == _hocvien.MAIL
                && NGAYSINH == _hocvien.NGAYSINH && HINH == _hocvien.HINH)
            {
                EventHelper.Helper.MessageNoChangedInformation();
                return;
            }

            HOCVIEN updateHocVien = new HOCVIEN();
            updateHocVien.MAHOCVIEN = _hocvien.MAHOCVIEN;
            updateHocVien.MATAIKHOAN = _hocvien.MATAIKHOAN;

            updateHocVien.GIOITINH = GIOITINH;
            updateHocVien.SODIENTHOAI = SODIENTHOAI;
            updateHocVien.TENHOCVIEN = TENHOCVIEN;
            updateHocVien.DIACHI = DIACHI;
            updateHocVien.CMND = CMND;
            updateHocVien.MAIL = MAIL;
            updateHocVien.NGAYSINH = NGAYSINH;
            updateHocVien.HINH = HINH;

            //UPDATE
            ResultExcuteQuery result = (new Update()).UpdateStudent(updateHocVien);
            EventHelper.Helper.MessageInfomation(result.GetMessage());
            if (result.isSuccessfully())
            {
                _hocvien = updateHocVien;

                if (type == Type.ChangePass)
                    MetroHelper.student = _hocvien;
            }
        }

        private void BtnInsert_Click(object sender, EventArgs e)
        {

            string TENHOCVIEN = txtHoTenHocVien.Text;
            string GIOITINH = rdbtnNamHocVien.Checked ? "NAM" : "NỮ";
            string SODIENTHOAI = txtDienThoaiHocVien.Text;
            string DIACHI = txtDiaChiHocVien.Text;
            string CMND = txtCMNDHocVien.Text;
            string MAIL = txtEmailHocVien.Text;
            DateTime NGAYSINH = pickerNgay.Value;
            Binary HINH = MetroHelper.ImageToBinary(picboxHocVien.Image);

            //kiểm tra rỗng
            if (MetroHelper.IsNullOrEmpty(new object[] { SODIENTHOAI, DIACHI, CMND, NGAYSINH }))
            {
                EventHelper.Helper.MessageLessInformation();
                return;
            }
            string MAHOCVIEN = (new Select()).TaoMaHocVienTuDong();
            HOCVIEN hv = new HOCVIEN();
            hv.MAHOCVIEN = MAHOCVIEN;
            hv.MATAIKHOAN = MAHOCVIEN;
            hv.TENHOCVIEN = TENHOCVIEN.ToUpper();
            hv.GIOITINH = GIOITINH;
            hv.SODIENTHOAI = SODIENTHOAI;
            hv.DIACHI = DIACHI.ToUpper();
            hv.CMND = CMND;
            hv.MAIL = MAIL.ToUpper();
            hv.NGAYSINH = NGAYSINH;
            hv.HINH = HINH;
            //Insert
            ResultExcuteQuery result = (new Insert()).ThemHocVien(hv);
            EventHelper.Helper.MessageInfomation(result.GetMessage());
            if (result.isSuccessfully())
                this.ParentForm.Close();
        }

        //TODO: Right button
        private void BtnChangePass_Click(object sender, EventArgs e)
        {
            MetroHelper.OpenNewTab(this, new MetroChangePass());
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            (this.ParentForm).Close();
        }

        internal void settingLayoutByStudent()
        {
            if (_hocvien != null)
            {
                lblMaHocVien.Text = "Mã Học Viên: " + _hocvien.MAHOCVIEN;
                txtHoTenHocVien.Text = _hocvien.TENHOCVIEN;
                pickerNgay.Value = _hocvien.NGAYSINH.Value;
                txtDiaChiHocVien.Text = _hocvien.DIACHI;
                txtCMNDHocVien.Text = _hocvien.CMND;
                txtEmailHocVien.Text = _hocvien.MAIL;
                txtDienThoaiHocVien.Text = _hocvien.SODIENTHOAI;
                rdbtnNamHocVien.Checked = _hocvien.GIOITINH.ToUpper().Equals("NAM");
                rdbtnNuHocVien.Checked = _hocvien.GIOITINH.ToUpper().Equals("NỮ");
                EventHelper.Helper.TryCatch(() => picboxHocVien.Image = MetroHelper.Base64ToImage(_hocvien.HINH));
            }
        }
    }
}
