﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TripleDES;
using MetroDataset;
using MetroControl;

namespace MetroUserControl.ContentMenu.User
{
    public partial class MetroChangePass : MetroBubble
    {
        public MetroChangePass()
        {
            InitializeComponent();
            btnAccept.Click += (s, e) => ChangePass();
            btnHuy.Click += (s, e) => Huy();
        }


        private void Huy()
        {
            (this.ParentForm).Close();
        }

        public void ChangePass()
        {
            string newPass = txtMatKhauMoiChangePass.Text;
            string determPass = txtXacThucChangePass.Text;

            if(!newPass.Equals(determPass))
            {
                EventHelper.Helper.MessageInfomation("Xác nhận mật khẩu mới không trùng khớp.");
                return;
            }

            NGUOIDUNG nguoidung = MetroHelper.GetCurrentAccount();

            string oldPass = txtMatKhauCuChangePass.Text;
            string oldPass_triple = TripleDES.TripleDES.Crypto.EncryptAccount(nguoidung.MATAIKHOAN, oldPass);


            if (oldPass_triple.Equals(nguoidung.MATKHAU))
            {
                string newPass_triple = TripleDES.TripleDES.Crypto.EncryptAccount(nguoidung.MATAIKHOAN, newPass);
                ResultExcuteQuery r = (new Update()).DoiMatKhau(nguoidung.MATAIKHOAN, newPass_triple);
                EventHelper.Helper.MessageInfomation(r.GetMessage());
                if (r.isSuccessfully())
                    this.ParentForm.Close();
            }
            else
                EventHelper.Helper.MessageInfomation("Mật khẩu cũ cung cấp không chính xác.");
        }
    }
}
