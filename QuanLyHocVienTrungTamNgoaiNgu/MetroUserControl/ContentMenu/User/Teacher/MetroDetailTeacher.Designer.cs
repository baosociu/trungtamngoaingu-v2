﻿namespace MetroUserControl.ContentMenu.User.Teacher
{
    partial class MetroDetailTeacher
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroLabel3 = new MetroControl.Components.MetroLabel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lblMaGiangVien = new MetroControl.Components.MetroLabel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.picboxGiangVien = new MetroControl.Components.MetroPicture();
            this.metroLabel1 = new MetroControl.Components.MetroLabel();
            this.rdbtnNamGiangVien = new System.Windows.Forms.RadioButton();
            this.gBoxContact = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.txtEmailGiangVien = new MetroControl.Components.MetroTextbox();
            this.metroLabel7 = new MetroControl.Components.MetroLabel();
            this.metroLabel6 = new MetroControl.Components.MetroLabel();
            this.metroLabel9 = new MetroControl.Components.MetroLabel();
            this.txtDienThoaiGiangVien = new MetroControl.Components.MetroTextbox();
            this.txtDiaChiGiangVien = new MetroControl.Components.MetroTextbox();
            this.rdbtnNuGiangVien = new System.Windows.Forms.RadioButton();
            this.metroLabel5 = new MetroControl.Components.MetroLabel();
            this.metroLabel2 = new MetroControl.Components.MetroLabel();
            this.txtCMNDGiangVien = new MetroControl.Components.MetroTextbox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tablePanelInfo = new System.Windows.Forms.TableLayoutPanel();
            this.metroLabel8 = new MetroControl.Components.MetroLabel();
            this.metroLabel4 = new MetroControl.Components.MetroLabel();
            this.txtHoTenGiangVien = new MetroControl.Components.MetroTextbox();
            this.txtTrinhDoGiangVien = new MetroControl.Components.MetroTextbox();
            this.txtChuyenMonGiangVien = new MetroControl.Components.MetroTextbox();
            this.pickerNgay = new MetroControl.Components.MetroDatePicker();
            this.btnLeftGiangVien = new MetroControl.Components.MetroButton();
            this.btnRightGiangVien = new MetroControl.Components.MetroButton();
            this.tablePanelBottom = new System.Windows.Forms.TableLayoutPanel();
            this.gboxInfo = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxGiangVien)).BeginInit();
            this.gBoxContact.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tablePanelInfo.SuspendLayout();
            this.tablePanelBottom.SuspendLayout();
            this.gboxInfo.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel3.IsFocus = true;
            this.metroLabel3.Location = new System.Drawing.Point(26, 1);
            this.metroLabel3.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(100, 32);
            this.metroLabel3.TabIndex = 5;
            this.metroLabel3.Text = "Họ Và Tên(*):";
            this.metroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel2.Controls.Add(this.lblMaGiangVien, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 1, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(445, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 8;
            this.tableLayoutPanel1.SetRowSpan(this.tableLayoutPanel2, 2);
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(289, 354);
            this.tableLayoutPanel2.TabIndex = 29;
            // 
            // lblMaGiangVien
            // 
            this.lblMaGiangVien.AutoSize = true;
            this.lblMaGiangVien.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.SetColumnSpan(this.lblMaGiangVien, 2);
            this.lblMaGiangVien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMaGiangVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblMaGiangVien.IsFocus = false;
            this.lblMaGiangVien.Location = new System.Drawing.Point(18, 224);
            this.lblMaGiangVien.Name = "lblMaGiangVien";
            this.lblMaGiangVien.Size = new System.Drawing.Size(251, 130);
            this.lblMaGiangVien.TabIndex = 0;
            this.lblMaGiangVien.Text = "Mã Giảng Viên: ";
            this.lblMaGiangVien.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel2.SetColumnSpan(this.tableLayoutPanel4, 2);
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 141F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.picboxGiangVien, 1, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(18, 35);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel2.SetRowSpan(this.tableLayoutPanel4, 6);
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 188F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(251, 186);
            this.tableLayoutPanel4.TabIndex = 14;
            // 
            // picboxGiangVien
            // 
            this.picboxGiangVien.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picboxGiangVien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picboxGiangVien.Image = global::MetroUserControl.Properties.Resources.user_shape;
            this.picboxGiangVien.Location = new System.Drawing.Point(58, 2);
            this.picboxGiangVien.Name = "picboxGiangVien";
            this.picboxGiangVien.Size = new System.Drawing.Size(135, 182);
            this.picboxGiangVien.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picboxGiangVien.TabIndex = 17;
            this.picboxGiangVien.TabStop = false;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel1.IsFocus = true;
            this.metroLabel1.Location = new System.Drawing.Point(26, 33);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(100, 32);
            this.metroLabel1.TabIndex = 14;
            this.metroLabel1.Text = "Ngày Sinh(*):";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // rdbtnNamGiangVien
            // 
            this.rdbtnNamGiangVien.AutoSize = true;
            this.rdbtnNamGiangVien.BackColor = System.Drawing.Color.Transparent;
            this.rdbtnNamGiangVien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rdbtnNamGiangVien.Location = new System.Drawing.Point(0, 0);
            this.rdbtnNamGiangVien.Margin = new System.Windows.Forms.Padding(0);
            this.rdbtnNamGiangVien.Name = "rdbtnNamGiangVien";
            this.rdbtnNamGiangVien.Size = new System.Drawing.Size(133, 32);
            this.rdbtnNamGiangVien.TabIndex = 0;
            this.rdbtnNamGiangVien.Text = "Nam";
            this.rdbtnNamGiangVien.UseVisualStyleBackColor = false;
            // 
            // gBoxContact
            // 
            this.gBoxContact.Controls.Add(this.tableLayoutPanel5);
            this.gBoxContact.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gBoxContact.Location = new System.Drawing.Point(3, 223);
            this.gBoxContact.Name = "gBoxContact";
            this.gBoxContact.Size = new System.Drawing.Size(436, 134);
            this.gBoxContact.TabIndex = 28;
            this.gBoxContact.TabStop = false;
            this.gBoxContact.Text = "Thông tin liên lạc";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 4;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.33334F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanel5.Controls.Add(this.txtEmailGiangVien, 2, 3);
            this.tableLayoutPanel5.Controls.Add(this.metroLabel7, 1, 3);
            this.tableLayoutPanel5.Controls.Add(this.metroLabel6, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.metroLabel9, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.txtDienThoaiGiangVien, 2, 2);
            this.tableLayoutPanel5.Controls.Add(this.txtDiaChiGiangVien, 2, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 5;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(430, 115);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // txtEmailGiangVien
            // 
            this.txtEmailGiangVien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEmailGiangVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtEmailGiangVien.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtEmailGiangVien.Location = new System.Drawing.Point(136, 77);
            this.txtEmailGiangVien.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtEmailGiangVien.Name = "txtEmailGiangVien";
            this.txtEmailGiangVien.Size = new System.Drawing.Size(266, 23);
            this.txtEmailGiangVien.TabIndex = 2;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel7.IsFocus = true;
            this.metroLabel7.Location = new System.Drawing.Point(29, 73);
            this.metroLabel7.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(97, 32);
            this.metroLabel7.TabIndex = 27;
            this.metroLabel7.Text = "Email(*):";
            this.metroLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel6.IsFocus = false;
            this.metroLabel6.Location = new System.Drawing.Point(29, 41);
            this.metroLabel6.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(97, 32);
            this.metroLabel6.TabIndex = 26;
            this.metroLabel6.Text = "Số Điện Thoại:";
            this.metroLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel9.IsFocus = false;
            this.metroLabel9.Location = new System.Drawing.Point(29, 9);
            this.metroLabel9.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(97, 32);
            this.metroLabel9.TabIndex = 25;
            this.metroLabel9.Text = "Địa Chỉ:";
            this.metroLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtDienThoaiGiangVien
            // 
            this.txtDienThoaiGiangVien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDienThoaiGiangVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtDienThoaiGiangVien.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtDienThoaiGiangVien.Location = new System.Drawing.Point(136, 45);
            this.txtDienThoaiGiangVien.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtDienThoaiGiangVien.Name = "txtDienThoaiGiangVien";
            this.txtDienThoaiGiangVien.Size = new System.Drawing.Size(266, 23);
            this.txtDienThoaiGiangVien.TabIndex = 1;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtDiaChiGiangVien
            // 
            this.txtDiaChiGiangVien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDiaChiGiangVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtDiaChiGiangVien.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtDiaChiGiangVien.Location = new System.Drawing.Point(136, 13);
            this.txtDiaChiGiangVien.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtDiaChiGiangVien.Name = "txtDiaChiGiangVien";
            this.txtDiaChiGiangVien.Size = new System.Drawing.Size(266, 23);
            this.txtDiaChiGiangVien.TabIndex = 0;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // rdbtnNuGiangVien
            // 
            this.rdbtnNuGiangVien.AutoSize = true;
            this.rdbtnNuGiangVien.BackColor = System.Drawing.Color.Transparent;
            this.rdbtnNuGiangVien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rdbtnNuGiangVien.Location = new System.Drawing.Point(133, 0);
            this.rdbtnNuGiangVien.Margin = new System.Windows.Forms.Padding(0);
            this.rdbtnNuGiangVien.Name = "rdbtnNuGiangVien";
            this.rdbtnNuGiangVien.Size = new System.Drawing.Size(133, 32);
            this.rdbtnNuGiangVien.TabIndex = 1;
            this.rdbtnNuGiangVien.Text = "Nữ";
            this.rdbtnNuGiangVien.UseVisualStyleBackColor = false;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel5.IsFocus = true;
            this.metroLabel5.Location = new System.Drawing.Point(26, 97);
            this.metroLabel5.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(100, 32);
            this.metroLabel5.TabIndex = 24;
            this.metroLabel5.Text = "Số CMND(*):";
            this.metroLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel2.IsFocus = false;
            this.metroLabel2.Location = new System.Drawing.Point(26, 65);
            this.metroLabel2.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(100, 32);
            this.metroLabel2.TabIndex = 23;
            this.metroLabel2.Text = "Giới Tính:";
            this.metroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtCMNDGiangVien
            // 
            this.txtCMNDGiangVien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCMNDGiangVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtCMNDGiangVien.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtCMNDGiangVien.Location = new System.Drawing.Point(136, 101);
            this.txtCMNDGiangVien.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtCMNDGiangVien.Name = "txtCMNDGiangVien";
            this.txtCMNDGiangVien.Size = new System.Drawing.Size(266, 23);
            this.txtCMNDGiangVien.TabIndex = 2;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.rdbtnNuGiangVien, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.rdbtnNamGiangVien, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(136, 65);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(266, 32);
            this.tableLayoutPanel3.TabIndex = 22;
            // 
            // tablePanelInfo
            // 
            this.tablePanelInfo.ColumnCount = 4;
            this.tablePanelInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tablePanelInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tablePanelInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.33333F));
            this.tablePanelInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tablePanelInfo.Controls.Add(this.metroLabel8, 1, 6);
            this.tablePanelInfo.Controls.Add(this.metroLabel4, 1, 5);
            this.tablePanelInfo.Controls.Add(this.metroLabel5, 1, 4);
            this.tablePanelInfo.Controls.Add(this.metroLabel2, 1, 3);
            this.tablePanelInfo.Controls.Add(this.txtCMNDGiangVien, 2, 4);
            this.tablePanelInfo.Controls.Add(this.tableLayoutPanel3, 2, 3);
            this.tablePanelInfo.Controls.Add(this.metroLabel1, 1, 2);
            this.tablePanelInfo.Controls.Add(this.txtHoTenGiangVien, 2, 1);
            this.tablePanelInfo.Controls.Add(this.metroLabel3, 1, 1);
            this.tablePanelInfo.Controls.Add(this.txtTrinhDoGiangVien, 2, 5);
            this.tablePanelInfo.Controls.Add(this.txtChuyenMonGiangVien, 2, 6);
            this.tablePanelInfo.Controls.Add(this.pickerNgay, 2, 2);
            this.tablePanelInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePanelInfo.Location = new System.Drawing.Point(3, 16);
            this.tablePanelInfo.Name = "tablePanelInfo";
            this.tablePanelInfo.RowCount = 8;
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tablePanelInfo.Size = new System.Drawing.Size(430, 195);
            this.tablePanelInfo.TabIndex = 0;
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel8.IsFocus = true;
            this.metroLabel8.Location = new System.Drawing.Point(26, 161);
            this.metroLabel8.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(100, 32);
            this.metroLabel8.TabIndex = 27;
            this.metroLabel8.Text = "Chuyên Môn(*):";
            this.metroLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel4.IsFocus = true;
            this.metroLabel4.Location = new System.Drawing.Point(26, 129);
            this.metroLabel4.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(100, 32);
            this.metroLabel4.TabIndex = 25;
            this.metroLabel4.Text = "Trình Độ(*):";
            this.metroLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtHoTenGiangVien
            // 
            this.txtHoTenGiangVien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHoTenGiangVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtHoTenGiangVien.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtHoTenGiangVien.Location = new System.Drawing.Point(136, 5);
            this.txtHoTenGiangVien.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtHoTenGiangVien.Name = "txtHoTenGiangVien";
            this.txtHoTenGiangVien.Size = new System.Drawing.Size(266, 23);
            this.txtHoTenGiangVien.TabIndex = 0;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtTrinhDoGiangVien
            // 
            this.txtTrinhDoGiangVien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTrinhDoGiangVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtTrinhDoGiangVien.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtTrinhDoGiangVien.Location = new System.Drawing.Point(136, 133);
            this.txtTrinhDoGiangVien.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtTrinhDoGiangVien.Name = "txtTrinhDoGiangVien";
            this.txtTrinhDoGiangVien.Size = new System.Drawing.Size(266, 23);
            this.txtTrinhDoGiangVien.TabIndex = 3;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtChuyenMonGiangVien
            // 
            this.txtChuyenMonGiangVien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtChuyenMonGiangVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtChuyenMonGiangVien.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtChuyenMonGiangVien.Location = new System.Drawing.Point(136, 165);
            this.txtChuyenMonGiangVien.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtChuyenMonGiangVien.Name = "txtChuyenMonGiangVien";
            this.txtChuyenMonGiangVien.Size = new System.Drawing.Size(266, 23);
            this.txtChuyenMonGiangVien.TabIndex = 4;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // pickerNgay
            // 
            this.pickerNgay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pickerNgay.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.pickerNgay.FontSize = MetroFramework.MetroDateTimeSize.Small;
            this.pickerNgay.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.pickerNgay.Location = new System.Drawing.Point(136, 36);
            this.pickerNgay.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.pickerNgay.MinimumSize = new System.Drawing.Size(0, 25);
            this.pickerNgay.Name = "pickerNgay";
            this.pickerNgay.Size = new System.Drawing.Size(266, 25);
            this.pickerNgay.TabIndex = 1;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // btnLeftGiangVien
            // 
            this.btnLeftGiangVien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnLeftGiangVien.Location = new System.Drawing.Point(184, 5);
            this.btnLeftGiangVien.Margin = new System.Windows.Forms.Padding(5);
            this.btnLeftGiangVien.Name = "btnLeftGiangVien";
            this.btnLeftGiangVien.Size = new System.Drawing.Size(90, 26);
            this.btnLeftGiangVien.TabIndex = 0;
            this.btnLeftGiangVien.Text = "LEFT";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnLeftGiangVien.UseSelectable = true;
            // 
            // btnRightGiangVien
            // 
            this.btnRightGiangVien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnRightGiangVien.Location = new System.Drawing.Point(463, 5);
            this.btnRightGiangVien.Margin = new System.Windows.Forms.Padding(5);
            this.btnRightGiangVien.Name = "btnRightGiangVien";
            this.btnRightGiangVien.Size = new System.Drawing.Size(90, 26);
            this.btnRightGiangVien.TabIndex = 1;
            this.btnRightGiangVien.Text = "RIGHT";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnRightGiangVien.UseSelectable = true;
            // 
            // tablePanelBottom
            // 
            this.tablePanelBottom.ColumnCount = 5;
            this.tableLayoutPanel1.SetColumnSpan(this.tablePanelBottom, 2);
            this.tablePanelBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tablePanelBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tablePanelBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tablePanelBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tablePanelBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tablePanelBottom.Controls.Add(this.btnLeftGiangVien, 1, 0);
            this.tablePanelBottom.Controls.Add(this.btnRightGiangVien, 3, 0);
            this.tablePanelBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePanelBottom.Location = new System.Drawing.Point(0, 358);
            this.tablePanelBottom.Margin = new System.Windows.Forms.Padding(0);
            this.tablePanelBottom.Name = "tablePanelBottom";
            this.tablePanelBottom.RowCount = 1;
            this.tablePanelBottom.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tablePanelBottom.Size = new System.Drawing.Size(737, 36);
            this.tablePanelBottom.TabIndex = 26;
            // 
            // gboxInfo
            // 
            this.gboxInfo.Controls.Add(this.tablePanelInfo);
            this.gboxInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gboxInfo.Location = new System.Drawing.Point(3, 3);
            this.gboxInfo.Name = "gboxInfo";
            this.gboxInfo.Size = new System.Drawing.Size(436, 214);
            this.gboxInfo.TabIndex = 25;
            this.gboxInfo.TabStop = false;
            this.gboxInfo.Text = "Thông tin cá nhân";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.gBoxContact, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tablePanelBottom, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.gboxInfo, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 220F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(737, 394);
            this.tableLayoutPanel1.TabIndex = 37;
            // 
            // MetroDetailTeacher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "MetroDetailTeacher";
            this.Size = new System.Drawing.Size(737, 394);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picboxGiangVien)).EndInit();
            this.gBoxContact.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tablePanelInfo.ResumeLayout(false);
            this.tablePanelInfo.PerformLayout();
            this.tablePanelBottom.ResumeLayout(false);
            this.gboxInfo.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroControl.Components.MetroLabel metroLabel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private MetroControl.Components.MetroLabel lblMaGiangVien;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private MetroControl.Components.MetroPicture picboxGiangVien;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox gBoxContact;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private MetroControl.Components.MetroTextbox txtEmailGiangVien;
        private MetroControl.Components.MetroLabel metroLabel7;
        private MetroControl.Components.MetroLabel metroLabel6;
        private MetroControl.Components.MetroLabel metroLabel9;
        private MetroControl.Components.MetroTextbox txtDienThoaiGiangVien;
        private MetroControl.Components.MetroTextbox txtDiaChiGiangVien;
        private System.Windows.Forms.TableLayoutPanel tablePanelBottom;
        private MetroControl.Components.MetroButton btnLeftGiangVien;
        private MetroControl.Components.MetroButton btnRightGiangVien;
        private System.Windows.Forms.GroupBox gboxInfo;
        private System.Windows.Forms.TableLayoutPanel tablePanelInfo;
        private MetroControl.Components.MetroLabel metroLabel5;
        private MetroControl.Components.MetroLabel metroLabel2;
        private MetroControl.Components.MetroTextbox txtCMNDGiangVien;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.RadioButton rdbtnNuGiangVien;
        private System.Windows.Forms.RadioButton rdbtnNamGiangVien;
        private MetroControl.Components.MetroLabel metroLabel1;
        private MetroControl.Components.MetroTextbox txtHoTenGiangVien;
        private MetroControl.Components.MetroLabel metroLabel8;
        private MetroControl.Components.MetroLabel metroLabel4;
        private MetroControl.Components.MetroTextbox txtTrinhDoGiangVien;
        private MetroControl.Components.MetroTextbox txtChuyenMonGiangVien;
        private MetroControl.Components.MetroDatePicker pickerNgay;
    }
}
