﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroControl;
using MetroControl.Components;
using MetroDataset;
using System.Data.Linq;

namespace MetroUserControl.ContentMenu.User.Teacher
{
    public partial class MetroDetailTeacher : MetroBubble
    {
        GIANGVIEN _giangvien;
        OpenFileDialog openFileDialog;
        public enum Type
        {
            InsertTeacher, //btnleft = Them - btnRight = Huy
            UpdateTeacher, //btnLeft = CapNhat - btnRight = Huy
            ChangePass //btnLeft = CapNhAT - btnRight = ChangePass
        }
        Type? type = null;

        public MetroDetailTeacher()
        {
            InitializeComponent();
            EventHelper.Helper.SetHandHover(this.picboxGiangVien);
            openFileDialog = MetroHelper.createAvatarOpenFileDialog();
            this.picboxGiangVien.Click += (s, e) => MetroHelper.OpenChoseAvatar(openFileDialog, picboxGiangVien);

        }


        public MetroDetailTeacher SetID(string ma)
        {
            _giangvien = (new Select()).GetTeacherByID(ma);
            settingLayoutByTeacher();

            return this;
        }

        public MetroDetailTeacher SetAs(Type type)
        {
            if (this.type == type)
                return this;
            this.type = type;

            if (type == Type.InsertTeacher)
            {
                //giao diện thêm học viên
                this.btnLeftGiangVien.Text = "THÊM";
                this.btnLeftGiangVien.TypeData.AddType(MetroControl.Components.Type.Insert);
                this.btnLeftGiangVien.Click += BtnInsert_Click;

                this.btnRightGiangVien.Text = "HUỶ";
                this.btnRightGiangVien.TypeData.AddType(MetroControl.Components.Type.View);
                this.btnRightGiangVien.Click += BtnCancel_Click;
                //layout
                lblMaGiangVien.Text = "Mã Giảng Viên: "+(new Select()).TaoMaGiangVienTuDong(); 

            }
            else if (type == Type.UpdateTeacher)
            {
                //giao diện cập nhật thông tin
                this.btnLeftGiangVien.Text = "CẬP NHẬT";
                this.btnLeftGiangVien.TypeData.AddType(MetroControl.Components.Type.Update);
                this.btnLeftGiangVien.Click += BtnUpdate_Click;

                this.btnRightGiangVien.Text = "HUỶ";
                this.btnRightGiangVien.TypeData.AddType(MetroControl.Components.Type.View);
                this.btnRightGiangVien.Click += BtnCancel_Click;

                //không cho nhập liệu một số thuộc tính
                txtCMNDGiangVien.ReadOnly = true;
                txtHoTenGiangVien.ReadOnly = true;

            }
            else if (type == Type.ChangePass)
            {
                //giao diện cập nhật thông tin
                this.btnLeftGiangVien.Text = "CẬP NHẬT";
                this.btnLeftGiangVien.TypeData.AddType(MetroControl.Components.Type.Update);
                this.btnLeftGiangVien.Click += BtnUpdate_Click;

                this.btnRightGiangVien.Text = "ĐỔI MẬT KHẨU";
                this.btnRightGiangVien.TypeData.AddType(MetroControl.Components.Type.Update);
                this.btnRightGiangVien.Click += BtnChangePass_Click;

                //không cho nhập liệu một số thuộc tính
                txtCMNDGiangVien.ReadOnly = true;
                txtHoTenGiangVien.ReadOnly = true;

                _giangvien = MetroHelper.teacher;
                settingLayoutByTeacher();
            }
            this.tablePanelBottom.Refresh();
            return this;
        }

        //TODO: Left button
        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            string TENGIANGVIEN = txtHoTenGiangVien.Text;
            string GIOITINH = rdbtnNamGiangVien.Checked ? "NAM" : "NỮ";
            string SODIENTHOAI = txtDienThoaiGiangVien.Text;
            string DIACHI = txtDiaChiGiangVien.Text;
            string CMND = txtCMNDGiangVien.Text;
            string MAIL = txtEmailGiangVien.Text;
            string TRINHDO = txtTrinhDoGiangVien.Text;
            string CHUYENMON = txtChuyenMonGiangVien.Text;
            DateTime NGAYSINH = pickerNgay.Value;
            Binary HINH = MetroHelper.ImageToBinary(picboxGiangVien.Image);

            //kiểm tra rỗng
            if (MetroHelper.IsNullOrEmpty(new object[] { SODIENTHOAI, DIACHI, CMND, NGAYSINH }))
            {
                EventHelper.Helper.MessageLessInformation();
                return;
            }

            //kiểm tra thay đổi
            if (TENGIANGVIEN == _giangvien.TENGIANGVIEN && GIOITINH == _giangvien.GIOITINH && SODIENTHOAI == _giangvien.SODIENTHOAI
                && DIACHI == _giangvien.DIACHI && CMND == _giangvien.CMND && MAIL == _giangvien.MAIL
                && TRINHDO == _giangvien.TRINHDO && CHUYENMON == _giangvien.CHUYENMON
                && NGAYSINH == _giangvien.NGAYSINH && HINH == _giangvien.HINH)
            {
                EventHelper.Helper.MessageNoChangedInformation();
                return;
            }

            GIANGVIEN gv = new GIANGVIEN();
            gv.MAGIANGVIEN = _giangvien.MAGIANGVIEN;
            gv.MATAIKHOAN = _giangvien.MATAIKHOAN;

            gv.TENGIANGVIEN = TENGIANGVIEN;
            gv.GIOITINH = GIOITINH;
            gv.SODIENTHOAI = SODIENTHOAI;
            gv.DIACHI = DIACHI;
            gv.CMND = CMND;
            gv.MAIL = MAIL;
            gv.TRINHDO = TRINHDO;
            gv.CHUYENMON = CHUYENMON;
            gv.NGAYSINH = NGAYSINH;
            gv.HINH = HINH;

            ResultExcuteQuery result = (new Update()).UpdateTeacher(gv);
            EventHelper.Helper.MessageInfomation(result.GetMessage());
            if (result.isSuccessfully())
            {
                _giangvien = gv;

                if (type == Type.ChangePass)
                    MetroHelper.teacher = _giangvien;
            }


        }

        private void BtnInsert_Click(object sender, EventArgs e)
        {
            string TENGIANGVIEN = txtHoTenGiangVien.Text;
            string GIOITINH = rdbtnNamGiangVien.Checked ? "NAM" : "NỮ";
            string SODIENTHOAI = txtDienThoaiGiangVien.Text;
            string DIACHI = txtDiaChiGiangVien.Text;
            string CMND = txtCMNDGiangVien.Text;
            string MAIL = txtEmailGiangVien.Text;
            string TRINHDO = txtTrinhDoGiangVien.Text;
            string CHUYENMON = txtChuyenMonGiangVien.Text;
            DateTime NGAYSINH = pickerNgay.Value;
            Binary HINH = MetroHelper.ImageToBinary(picboxGiangVien.Image);

            //kiểm tra rỗng
            if (MetroHelper.IsNullOrEmpty(new object[] { SODIENTHOAI, DIACHI, CMND, NGAYSINH }))
            {
                EventHelper.Helper.MessageLessInformation();
                return;
            }
            string MAGIANGVIEN = (new Select()).TaoMaGiangVienTuDong();
            GIANGVIEN gv = new GIANGVIEN();
            gv.MAGIANGVIEN = MAGIANGVIEN;
            gv.MATAIKHOAN = MAGIANGVIEN;
            gv.TENGIANGVIEN = TENGIANGVIEN.ToUpper();
            gv.GIOITINH = GIOITINH;
            gv.SODIENTHOAI = SODIENTHOAI;
            gv.DIACHI = DIACHI.ToUpper();
            gv.CMND = CMND;
            gv.MAIL = MAIL.ToUpper();
            gv.NGAYSINH = NGAYSINH;
            gv.TRINHDO = TRINHDO.ToUpper();
            gv.CHUYENMON = CHUYENMON.ToUpper();
            gv.HINH = HINH;
            //Insert
            ResultExcuteQuery result = (new Insert()).ThemGiangVien(gv);
            EventHelper.Helper.MessageInfomation(result.GetMessage());
            if (result.isSuccessfully())
                this.ParentForm.Close();

        }

        //TODO: Right button
        private void BtnChangePass_Click(object sender, EventArgs e)
        {
            MetroHelper.OpenNewTab(this, new MetroChangePass());
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            (this.ParentForm).Close();
        }

        internal void settingLayoutByTeacher()
        {
            if (_giangvien != null)
            {
                lblMaGiangVien.Text = "Mã Giảng Viên: " + _giangvien.MAGIANGVIEN;
                txtHoTenGiangVien.Text = _giangvien.TENGIANGVIEN;
                pickerNgay.Value = _giangvien.NGAYSINH.Value;
                txtDiaChiGiangVien.Text = _giangvien.DIACHI;
                txtCMNDGiangVien.Text = _giangvien.CMND;
                txtEmailGiangVien.Text = _giangvien.MAIL;
                txtTrinhDoGiangVien.Text = _giangvien.TRINHDO;
                txtChuyenMonGiangVien.Text = _giangvien.CHUYENMON;
                txtDienThoaiGiangVien.Text = _giangvien.SODIENTHOAI;
                rdbtnNamGiangVien.Checked = _giangvien.GIOITINH.ToUpper().Equals("NAM");
                rdbtnNuGiangVien.Checked = _giangvien.GIOITINH.ToUpper().Equals("NỮ");

                EventHelper.Helper.TryCatch(() => picboxGiangVien.Image = MetroHelper.Base64ToImage(_giangvien.HINH));

            }
        }
    }
}
