﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroControl;
using MetroUserControl.ContentMenu.User.Teacher;
using MetroUserControl.ContentMenu.User.Staff;
using MetroUserControl.ContentMenu.User.Student;
using EventHelper;
using MetroDataset;
using System.IO;
using Export;
using MetroUserControl.ContentMenu.Mail;

namespace MetroUserControl.ContentMenu.User
{
    public partial class MetroListUser : MetroBubble
    {
        Func<int, int, int, string, string, object> sourceGrid;
        Func<object> sourceCbb;
        public enum Type
        {
            Student, Staff, Teacher
        }
        Type? type = null;
        public Type? getType { get => this.type; }
        public MetroListUser()
        {
            InitializeComponent();

            #region ------------------------ Demo

            #endregion -----------------------------
            this.VisibleChanged += MetroListUser_VisibleChanged;
            this.Load += MetroListUser_Load;
        }

        private void MetroListUser_Load(object sender, EventArgs e)
        {
            this.btnThem.TypeData.Add(MetroControl.Components.Type.Insert);
            this.btnChonGioHoc.TypeData.Add(MetroControl.Components.Type.Update);
            this.VisibleByPermission();
        }

        private void MetroListUser_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible == true)
            {
                if (type == Type.Staff && funcData != null)
                    ReloadData(this.grdUser.grid, true);
                this.SettingLoadByPageFilterSearch(this.grdUser, sourceGrid, this.cbbLoc, sourceCbb, this.picSearch, txtTimKiem);
            }
        }

        public MetroListUser SetAs(Type type)
        {

            if (this.type == type)
                return this;

            Func<int, int, int, string, string, object> f;
            if (type == Type.Staff)
                SetUpStaff();
            else if (type == Type.Student)
                SetUpStudent();
            else
                SetUpTeacher();

            this.type = type;

            Init();

            EventHelper.Helper.SetDoubleClickItemGridView(this.grdUser.grid, this.btnXemChiTiet.PerformClick);
            return this;
        }




        //************************STUDENT************************
        #region STUDENT
        private void SetUpStudent()
        {
            this.btnChonGioHoc.Visible = true;
            this.panelBottom.ColumnStyles[3].Width = 100;
            this.btnThem.Text = "THÊM HỌC VIÊN";
            EventHelper.Helper.SetHandHover(this.picSearch);
            this.btnThem.Click += BtnThemHocVien_Click;
            this.btnXemChiTiet.Click += BtnXemChiTietHocVien_Click;
            this.btnChonGioHoc.Click += BtnChonGioHoc_Click;
            sourceGrid = (new LoadByPage()).HienThiHocVien;
            sourceCbb = (new Select()).HienThiFilterHocVien_Cbb;
        }

        private void BtnChonGioHoc_Click(object sender, EventArgs e)
        {
            string ma = this.grdUser.grid.CurrentRow.Cells[0].Value.ToString();
            MetroHelper.OpenNewTab(this, new MetroSelectSchedule(ma));
        }

        private void BtnXemChiTietHocVien_Click(object sender, EventArgs e)
        {
            string ma = this.grdUser.grid.CurrentRow.Cells[0].Value.ToString();
            MetroHelper.OpenNewTab(this, new MetroDetailStudent().SetAs(MetroDetailStudent.Type.UpdateStudent).SetID(ma));
        }

        private void BtnThemHocVien_Click(object sender, EventArgs e)
        {
            MetroHelper.OpenNewTab(this, new MetroDetailStudent().SetAs(MetroDetailStudent.Type.InsertStudent));
        }
        #endregion

        //************************TEACHER************************
        #region TEACHER
        private void SetUpTeacher()
        {
            this.btnChonGioHoc.Visible = false;
            this.panelBottom.ColumnStyles[3].Width = 120;
            this.btnThem.Text = "THÊM GIẢNG VIÊN";
            this.btnThem.Click += BtnThemGiangVien_Click;
            this.btnXemChiTiet.Click += BtnXemChiTietGiangVien_Click;
            sourceGrid = (new LoadByPage()).HienThiGiangVien;
            sourceCbb = (new Select()).HienThiFilterGiangVien_Cbb;

        }

        private void BtnXemChiTietGiangVien_Click(object sender, EventArgs e)
        {
            string ma = this.grdUser.grid.CurrentRow.Cells[0].Value.ToString();
            MetroHelper.OpenNewTab(this, new MetroDetailTeacher().SetAs(MetroDetailTeacher.Type.UpdateTeacher).SetID(ma));

        }

        private void BtnThemGiangVien_Click(object sender, EventArgs e)
        {
            MetroHelper.OpenNewTab(this, new MetroDetailTeacher().SetAs(MetroDetailTeacher.Type.InsertTeacher));
        }
        #endregion

        //************************STAFF************************
        #region STAFF
        private void SetUpStaff()
        {
            this.panelBottom.ColumnStyles[3].Width = 120;
            this.btnThem.Text = "THÊM NHÂN VIÊN";
            this.btnChonGioHoc.Visible = false;
            this.btnThem.Click += BtnThemNhanVien_Click;
            this.btnXemChiTiet.Click += BtnXemChiTietNhanVien_Click;
            sourceGrid = (new LoadByPage()).HienThiNhanVien;
            sourceCbb = (new Select()).HienThiFilterNhanVien_Cbb;

        }

        private void BtnXemChiTietNhanVien_Click(object sender, EventArgs e)
        {

            string ma = this.grdUser.grid.SelectedRows[0].Cells[0].Value.ToString();
            MetroHelper.OpenNewTab(this, new MetroDetailStaff().SetAs(MetroDetailStaff.Type.UpdateStaff).SetID(ma));

        }

        private void BtnThemNhanVien_Click(object sender, EventArgs e)
        {
            MetroHelper.OpenNewTab(this, new MetroDetailStaff().SetAs(MetroDetailStaff.Type.InsertStaff));

        }


        #endregion

        private void gửiMailChoTàiKhoảnNàyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string mataikhoan = grdUser.grid.CurrentRow.Cells[0].Value.ToString();
            List<object> tb = (new Select()).HienThiMailNguoiDung(mataikhoan);
            MetroHelper.OpenNewTab(this, new MetroMail().SetDataReceiver(tb));
        }
    }
}
