﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MetroUserControl.ContentMenu.User
{
    public partial class MetroTabListUser : MetroBubble
    {
        public MetroTabListUser()
        {
            InitializeComponent();
            this.Load += MetroTabListUser_Load;
            this.metroTabControl1.SelectedIndexChanged += MetroTabControl1_SelectedIndexChanged;

            this.tabGiangVien.Hide();
            this.tabNhanVien.Hide();
            this.tabHocVien.Hide();
            metroUserNhanVien.SetAs(MetroListUser.Type.Staff);
            metroUserHocVien.SetAs(MetroListUser.Type.Student);
            metroUserGiangVien.SetAs(MetroListUser.Type.Teacher);
        }
        //public MetroTabListUser SetID(string idUserNhanVien, string idUserHocVien, string idUserGiangVien)
        //{
        //    id = idUserNhanVien + "," + idUserHocVien + "," + idUserGiangVien;
        //    return this;
        //}
        public bool ShowTabById(List<string> idmanhinhs)
        {
            bool r = false;
            if (idmanhinhs.Contains(metroUserGiangVien.id))
            {
                tabGiangVien.Show();
                r = true;
            }
            if (idmanhinhs.Contains(metroUserHocVien.id))
            {
                tabHocVien.Show();
                r = true;
            }
            if (idmanhinhs.Contains(metroUserNhanVien.id))
            {
                tabNhanVien.Show();
                r = true;
            }
            this.metroTabControl1.SelectTab(tabNhanVien);
            return r;

        }

        private void MetroTabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.metroTabControl1.SelectedTab.Equals(this.metroUserGiangVien))
                metroUserGiangVien.ReloadData(metroUserGiangVien.grdUser.grid, false);
            else if (this.metroTabControl1.SelectedTab.Equals(this.metroUserHocVien))
                metroUserHocVien.ReloadData(metroUserHocVien.grdUser.grid, false);
            else if (this.metroTabControl1.SelectedTab.Equals(this.metroUserNhanVien))
                metroUserNhanVien.ReloadData(metroUserNhanVien.grdUser.grid, false);

        }

        private void MetroTabListUser_Load(object sender, EventArgs e)
        {

        }
    }
}
