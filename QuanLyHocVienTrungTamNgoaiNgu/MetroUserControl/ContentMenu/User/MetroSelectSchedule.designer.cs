﻿namespace MetroUserControl.ContentMenu.User
{
    partial class MetroSelectSchedule
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnHuy = new MetroControl.Components.MetroButton();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.gBoxContact = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.btnInPhieu = new MetroControl.Components.MetroButton();
            this.btnTaoPhieu = new MetroControl.Components.MetroButton();
            this.btnHuyPhieu = new MetroControl.Components.MetroButton();
            this.metroLabel5 = new MetroControl.Components.MetroLabel();
            this.cbbPhieuDangKy = new MetroControl.Components.MetroCombobox();
            this.cbbKhoaHoc = new MetroControl.Components.MetroCombobox();
            this.metroLabel1 = new MetroControl.Components.MetroLabel();
            this.metroLabel8 = new MetroControl.Components.MetroLabel();
            this.metroLabel9 = new MetroControl.Components.MetroLabel();
            this.txtLopHoc = new MetroControl.Components.MetroTextbox();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.txtDiem = new MetroControl.Components.MetroTextbox();
            this.picSave = new MetroControl.Components.MetroPicture();
            this.gboxInfo = new System.Windows.Forms.GroupBox();
            this.tablePanelInfo = new System.Windows.Forms.TableLayoutPanel();
            this.txtSDT = new MetroControl.Components.MetroTextbox();
            this.txtCMND = new MetroControl.Components.MetroTextbox();
            this.metroLabel10 = new MetroControl.Components.MetroLabel();
            this.metroLabel7 = new MetroControl.Components.MetroLabel();
            this.cbbMaHocVien = new MetroControl.Components.MetroCombobox();
            this.metroLabel2 = new MetroControl.Components.MetroLabel();
            this.metroLabel3 = new MetroControl.Components.MetroLabel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.rdbtnNuHocVien = new System.Windows.Forms.RadioButton();
            this.rdbtnNamHocVien = new System.Windows.Forms.RadioButton();
            this.metroLabel4 = new MetroControl.Components.MetroLabel();
            this.txtHoTenHocVien = new MetroControl.Components.MetroTextbox();
            this.metroLabel6 = new MetroControl.Components.MetroLabel();
            this.pickerNgay = new MetroControl.Components.MetroDatePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.btnXacNhan = new MetroControl.Components.MetroButton();
            this.grdGioHoc = new MetroControl.Components.MetroGridView();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.gBoxContact.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSave)).BeginInit();
            this.gboxInfo.SuspendLayout();
            this.tablePanelInfo.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdGioHoc)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.ColumnCount = 6;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.45454F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.45454F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 79F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel1, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.groupBox1, 0, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 5;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 220F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.622479F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 88.75981F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.61771F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(730, 500);
            this.tableLayoutPanel2.TabIndex = 5;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel2.SetColumnSpan(this.tableLayoutPanel1, 6);
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.btnHuy, 3, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 462);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(730, 38);
            this.tableLayoutPanel1.TabIndex = 32;
            // 
            // btnHuy
            // 
            this.btnHuy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnHuy.Location = new System.Drawing.Point(455, 3);
            this.btnHuy.Name = "btnHuy";
            this.btnHuy.Size = new System.Drawing.Size(94, 32);
            this.btnHuy.TabIndex = 1;
            this.btnHuy.Text = "HUỶ";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnHuy.UseSelectable = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel2.SetColumnSpan(this.tableLayoutPanel3, 6);
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.78049F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.439024F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.78049F));
            this.tableLayoutPanel3.Controls.Add(this.gBoxContact, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.gboxInfo, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(730, 220);
            this.tableLayoutPanel3.TabIndex = 33;
            // 
            // gBoxContact
            // 
            this.gBoxContact.Controls.Add(this.tableLayoutPanel5);
            this.gBoxContact.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gBoxContact.Location = new System.Drawing.Point(376, 3);
            this.gBoxContact.Name = "gBoxContact";
            this.gBoxContact.Size = new System.Drawing.Size(351, 214);
            this.gBoxContact.TabIndex = 29;
            this.gBoxContact.TabStop = false;
            this.gBoxContact.Text = "Thông tin đăng ký khoá học";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 4;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.33333F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel8, 0, 6);
            this.tableLayoutPanel5.Controls.Add(this.metroLabel5, 1, 3);
            this.tableLayoutPanel5.Controls.Add(this.cbbPhieuDangKy, 2, 3);
            this.tableLayoutPanel5.Controls.Add(this.cbbKhoaHoc, 2, 2);
            this.tableLayoutPanel5.Controls.Add(this.metroLabel1, 1, 4);
            this.tableLayoutPanel5.Controls.Add(this.metroLabel8, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.metroLabel9, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.txtLopHoc, 2, 4);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel9, 2, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 8;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(345, 195);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 7;
            this.tableLayoutPanel5.SetColumnSpan(this.tableLayoutPanel8, 4);
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel8.Controls.Add(this.btnInPhieu, 5, 0);
            this.tableLayoutPanel8.Controls.Add(this.btnTaoPhieu, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.btnHuyPhieu, 3, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(0, 148);
            this.tableLayoutPanel8.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(345, 36);
            this.tableLayoutPanel8.TabIndex = 39;
            // 
            // btnInPhieu
            // 
            this.btnInPhieu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnInPhieu.Location = new System.Drawing.Point(236, 3);
            this.btnInPhieu.Name = "btnInPhieu";
            this.btnInPhieu.Size = new System.Drawing.Size(94, 30);
            this.btnInPhieu.TabIndex = 5;
            this.btnInPhieu.Text = "IN PHIẾU";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnInPhieu.UseSelectable = true;
            // 
            // btnTaoPhieu
            // 
            this.btnTaoPhieu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTaoPhieu.Location = new System.Drawing.Point(14, 3);
            this.btnTaoPhieu.Name = "btnTaoPhieu";
            this.btnTaoPhieu.Size = new System.Drawing.Size(94, 30);
            this.btnTaoPhieu.TabIndex = 4;
            this.btnTaoPhieu.Text = "TẠO PHIẾU MỚI";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnTaoPhieu.UseSelectable = true;
            // 
            // btnHuyPhieu
            // 
            this.btnHuyPhieu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnHuyPhieu.Location = new System.Drawing.Point(125, 3);
            this.btnHuyPhieu.Name = "btnHuyPhieu";
            this.btnHuyPhieu.Size = new System.Drawing.Size(94, 30);
            this.btnHuyPhieu.TabIndex = 1;
            this.btnHuyPhieu.Text = "HUỶ PHIẾU";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnHuyPhieu.UseSelectable = true;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel5.Location = new System.Drawing.Point(22, 74);
            this.metroLabel5.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(97, 32);
            this.metroLabel5.TabIndex = 38;
            this.metroLabel5.Text = "Phiếu Đăng Ký:";
            this.metroLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // cbbPhieuDangKy
            // 
            this.cbbPhieuDangKy.DisplayMember = "Value";
            this.cbbPhieuDangKy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbbPhieuDangKy.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbbPhieuDangKy.FormattingEnabled = true;
            this.cbbPhieuDangKy.ItemHeight = 19;
            this.cbbPhieuDangKy.Location = new System.Drawing.Point(129, 77);
            this.cbbPhieuDangKy.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.cbbPhieuDangKy.Name = "cbbPhieuDangKy";
            this.cbbPhieuDangKy.Size = new System.Drawing.Size(195, 25);
            this.cbbPhieuDangKy.TabIndex = 37;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.cbbPhieuDangKy.UseSelectable = true;
            this.cbbPhieuDangKy.ValueMember = "Key";
            // 
            // cbbKhoaHoc
            // 
            this.cbbKhoaHoc.DisplayMember = "Value";
            this.cbbKhoaHoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbbKhoaHoc.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbbKhoaHoc.FormattingEnabled = true;
            this.cbbKhoaHoc.ItemHeight = 19;
            this.cbbKhoaHoc.Location = new System.Drawing.Point(129, 45);
            this.cbbKhoaHoc.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.cbbKhoaHoc.Name = "cbbKhoaHoc";
            this.cbbKhoaHoc.Size = new System.Drawing.Size(195, 25);
            this.cbbKhoaHoc.TabIndex = 35;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.cbbKhoaHoc.UseSelectable = true;
            this.cbbKhoaHoc.ValueMember = "Key";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel1.Location = new System.Drawing.Point(22, 106);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(97, 32);
            this.metroLabel1.TabIndex = 27;
            this.metroLabel1.Text = "Lớp Học:";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel8.Location = new System.Drawing.Point(22, 42);
            this.metroLabel8.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(97, 32);
            this.metroLabel8.TabIndex = 26;
            this.metroLabel8.Text = "Khoá Học:";
            this.metroLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel9.Location = new System.Drawing.Point(22, 10);
            this.metroLabel9.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(97, 32);
            this.metroLabel9.TabIndex = 25;
            this.metroLabel9.Text = "Điểm Kiểm Tra:";
            this.metroLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtLopHoc
            // 
            this.txtLopHoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLopHoc.Enabled = false;
            this.txtLopHoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtLopHoc.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtLopHoc.Location = new System.Drawing.Point(129, 110);
            this.txtLopHoc.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtLopHoc.Name = "txtLopHoc";
            this.txtLopHoc.Size = new System.Drawing.Size(195, 23);
            this.txtLopHoc.TabIndex = 28;
            this.txtLopHoc.Text = "Chưa có";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel9.Controls.Add(this.txtDiem, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.picSave, 1, 0);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(129, 10);
            this.tableLayoutPanel9.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(195, 32);
            this.tableLayoutPanel9.TabIndex = 40;
            // 
            // txtDiem
            // 
            this.txtDiem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDiem.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtDiem.InputType = MetroControl.Components.MetroTextbox.Input.NumberInput;
            this.txtDiem.Location = new System.Drawing.Point(0, 4);
            this.txtDiem.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtDiem.Name = "txtDiem";
            this.txtDiem.Size = new System.Drawing.Size(163, 23);
            this.txtDiem.TabIndex = 21;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // picSave
            // 
            this.picSave.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picSave.Image = global::MetroUserControl.Properties.Resources.correct_symbol;
            this.picSave.Location = new System.Drawing.Point(166, 3);
            this.picSave.Name = "picSave";
            this.picSave.Size = new System.Drawing.Size(26, 26);
            this.picSave.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picSave.TabIndex = 22;
            this.picSave.TabStop = false;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // gboxInfo
            // 
            this.gboxInfo.Controls.Add(this.tablePanelInfo);
            this.gboxInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gboxInfo.Location = new System.Drawing.Point(3, 3);
            this.gboxInfo.Name = "gboxInfo";
            this.gboxInfo.Size = new System.Drawing.Size(350, 214);
            this.gboxInfo.TabIndex = 26;
            this.gboxInfo.TabStop = false;
            this.gboxInfo.Text = "Thông tin cá nhân";
            // 
            // tablePanelInfo
            // 
            this.tablePanelInfo.ColumnCount = 4;
            this.tablePanelInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tablePanelInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tablePanelInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.33334F));
            this.tablePanelInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tablePanelInfo.Controls.Add(this.txtSDT, 2, 6);
            this.tablePanelInfo.Controls.Add(this.txtCMND, 2, 5);
            this.tablePanelInfo.Controls.Add(this.metroLabel10, 1, 6);
            this.tablePanelInfo.Controls.Add(this.metroLabel7, 1, 5);
            this.tablePanelInfo.Controls.Add(this.cbbMaHocVien, 2, 1);
            this.tablePanelInfo.Controls.Add(this.metroLabel2, 1, 1);
            this.tablePanelInfo.Controls.Add(this.metroLabel3, 1, 4);
            this.tablePanelInfo.Controls.Add(this.tableLayoutPanel4, 2, 4);
            this.tablePanelInfo.Controls.Add(this.metroLabel4, 1, 3);
            this.tablePanelInfo.Controls.Add(this.txtHoTenHocVien, 2, 2);
            this.tablePanelInfo.Controls.Add(this.metroLabel6, 1, 2);
            this.tablePanelInfo.Controls.Add(this.pickerNgay, 2, 3);
            this.tablePanelInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePanelInfo.Location = new System.Drawing.Point(3, 16);
            this.tablePanelInfo.Name = "tablePanelInfo";
            this.tablePanelInfo.RowCount = 8;
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tablePanelInfo.Size = new System.Drawing.Size(344, 195);
            this.tablePanelInfo.TabIndex = 0;
            // 
            // txtSDT
            // 
            this.txtSDT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSDT.Enabled = false;
            this.txtSDT.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtSDT.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtSDT.Location = new System.Drawing.Point(120, 165);
            this.txtSDT.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtSDT.Name = "txtSDT";
            this.txtSDT.Size = new System.Drawing.Size(203, 23);
            this.txtSDT.TabIndex = 42;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtCMND
            // 
            this.txtCMND.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCMND.Enabled = false;
            this.txtCMND.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtCMND.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtCMND.Location = new System.Drawing.Point(120, 133);
            this.txtCMND.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtCMND.Name = "txtCMND";
            this.txtCMND.Size = new System.Drawing.Size(203, 23);
            this.txtCMND.TabIndex = 41;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel10.Location = new System.Drawing.Point(20, 161);
            this.metroLabel10.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(90, 32);
            this.metroLabel10.TabIndex = 40;
            this.metroLabel10.Text = "Số Điện Thoại:";
            this.metroLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel7.Location = new System.Drawing.Point(20, 129);
            this.metroLabel7.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(90, 32);
            this.metroLabel7.TabIndex = 39;
            this.metroLabel7.Text = "CMND:";
            this.metroLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // cbbMaHocVien
            // 
            this.cbbMaHocVien.DisplayMember = "Value";
            this.cbbMaHocVien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbbMaHocVien.Enabled = false;
            this.cbbMaHocVien.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbbMaHocVien.FormattingEnabled = true;
            this.cbbMaHocVien.ItemHeight = 19;
            this.cbbMaHocVien.Location = new System.Drawing.Point(120, 4);
            this.cbbMaHocVien.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.cbbMaHocVien.Name = "cbbMaHocVien";
            this.cbbMaHocVien.Size = new System.Drawing.Size(203, 25);
            this.cbbMaHocVien.TabIndex = 38;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.cbbMaHocVien.UseSelectable = true;
            this.cbbMaHocVien.ValueMember = "Key";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel2.Location = new System.Drawing.Point(23, 1);
            this.metroLabel2.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(87, 32);
            this.metroLabel2.TabIndex = 37;
            this.metroLabel2.Text = "Mã Học Viên:";
            this.metroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel3.Location = new System.Drawing.Point(20, 97);
            this.metroLabel3.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(90, 32);
            this.metroLabel3.TabIndex = 23;
            this.metroLabel3.Text = "Giới Tính:";
            this.metroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.rdbtnNuHocVien, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.rdbtnNamHocVien, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(120, 97);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(203, 32);
            this.tableLayoutPanel4.TabIndex = 22;
            // 
            // rdbtnNuHocVien
            // 
            this.rdbtnNuHocVien.AutoSize = true;
            this.rdbtnNuHocVien.BackColor = System.Drawing.Color.Transparent;
            this.rdbtnNuHocVien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rdbtnNuHocVien.Enabled = false;
            this.rdbtnNuHocVien.Location = new System.Drawing.Point(101, 0);
            this.rdbtnNuHocVien.Margin = new System.Windows.Forms.Padding(0);
            this.rdbtnNuHocVien.Name = "rdbtnNuHocVien";
            this.rdbtnNuHocVien.Size = new System.Drawing.Size(102, 32);
            this.rdbtnNuHocVien.TabIndex = 20;
            this.rdbtnNuHocVien.Text = "Nữ";
            this.rdbtnNuHocVien.UseVisualStyleBackColor = false;
            // 
            // rdbtnNamHocVien
            // 
            this.rdbtnNamHocVien.AutoSize = true;
            this.rdbtnNamHocVien.BackColor = System.Drawing.Color.Transparent;
            this.rdbtnNamHocVien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rdbtnNamHocVien.Enabled = false;
            this.rdbtnNamHocVien.Location = new System.Drawing.Point(0, 0);
            this.rdbtnNamHocVien.Margin = new System.Windows.Forms.Padding(0);
            this.rdbtnNamHocVien.Name = "rdbtnNamHocVien";
            this.rdbtnNamHocVien.Size = new System.Drawing.Size(101, 32);
            this.rdbtnNamHocVien.TabIndex = 19;
            this.rdbtnNamHocVien.Text = "Nam";
            this.rdbtnNamHocVien.UseVisualStyleBackColor = false;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel4.Location = new System.Drawing.Point(20, 65);
            this.metroLabel4.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(90, 32);
            this.metroLabel4.TabIndex = 14;
            this.metroLabel4.Text = "Ngày Sinh:";
            this.metroLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtHoTenHocVien
            // 
            this.txtHoTenHocVien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHoTenHocVien.Enabled = false;
            this.txtHoTenHocVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtHoTenHocVien.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtHoTenHocVien.Location = new System.Drawing.Point(120, 37);
            this.txtHoTenHocVien.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtHoTenHocVien.Name = "txtHoTenHocVien";
            this.txtHoTenHocVien.Size = new System.Drawing.Size(203, 23);
            this.txtHoTenHocVien.TabIndex = 13;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel6.Location = new System.Drawing.Point(20, 33);
            this.metroLabel6.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(90, 32);
            this.metroLabel6.TabIndex = 5;
            this.metroLabel6.Text = "Họ Và Tên:";
            this.metroLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // pickerNgay
            // 
            this.pickerNgay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pickerNgay.Enabled = false;
            this.pickerNgay.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.pickerNgay.FontSize = MetroFramework.MetroDateTimeSize.Small;
            this.pickerNgay.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.pickerNgay.Location = new System.Drawing.Point(120, 68);
            this.pickerNgay.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.pickerNgay.MinimumSize = new System.Drawing.Size(0, 25);
            this.pickerNgay.Name = "pickerNgay";
            this.pickerNgay.Size = new System.Drawing.Size(203, 25);
            this.pickerNgay.TabIndex = 25;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // groupBox1
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.groupBox1, 6);
            this.groupBox1.Controls.Add(this.tableLayoutPanel6);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 236);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(724, 210);
            this.groupBox1.TabIndex = 34;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin chọn giờ học";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel7, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.grdGioHoc, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(718, 191);
            this.tableLayoutPanel6.TabIndex = 0;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 3;
            this.tableLayoutPanel6.SetColumnSpan(this.tableLayoutPanel7, 6);
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.Controls.Add(this.btnXacNhan, 1, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(0, 155);
            this.tableLayoutPanel7.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(718, 36);
            this.tableLayoutPanel7.TabIndex = 33;
            // 
            // btnXacNhan
            // 
            this.btnXacNhan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnXacNhan.Location = new System.Drawing.Point(312, 3);
            this.btnXacNhan.Name = "btnXacNhan";
            this.btnXacNhan.Size = new System.Drawing.Size(94, 30);
            this.btnXacNhan.TabIndex = 3;
            this.btnXacNhan.Text = "LƯU";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnXacNhan.UseSelectable = true;
            // 
            // grdGioHoc
            // 
            this.grdGioHoc.AllowUserToAddRows = false;
            this.grdGioHoc.AllowUserToDeleteRows = false;
            this.grdGioHoc.AllowUserToResizeRows = false;
            this.grdGioHoc.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdGioHoc.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.grdGioHoc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdGioHoc.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.grdGioHoc.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdGioHoc.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grdGioHoc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdGioHoc.DefaultCellStyle = dataGridViewCellStyle2;
            this.grdGioHoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdGioHoc.EnableHeadersVisualStyles = false;
            this.grdGioHoc.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.grdGioHoc.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.grdGioHoc.Location = new System.Drawing.Point(3, 3);
            this.grdGioHoc.MultiSelect = false;
            this.grdGioHoc.Name = "grdGioHoc";
            this.grdGioHoc.ReadOnly = true;
            this.grdGioHoc.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdGioHoc.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.grdGioHoc.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdGioHoc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdGioHoc.Size = new System.Drawing.Size(712, 149);
            this.grdGioHoc.TabIndex = 32;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // MetroSelectSchedule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel2);
            this.Name = "MetroSelectSchedule";
            this.Size = new System.Drawing.Size(730, 500);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.gBoxContact.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSave)).EndInit();
            this.gboxInfo.ResumeLayout(false);
            this.tablePanelInfo.ResumeLayout(false);
            this.tablePanelInfo.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdGioHoc)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroControl.Components.MetroButton btnHuy;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.GroupBox gboxInfo;
        private System.Windows.Forms.TableLayoutPanel tablePanelInfo;
        private MetroControl.Components.MetroLabel metroLabel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.RadioButton rdbtnNuHocVien;
        private System.Windows.Forms.RadioButton rdbtnNamHocVien;
        private MetroControl.Components.MetroLabel metroLabel4;
        private MetroControl.Components.MetroTextbox txtHoTenHocVien;
        private MetroControl.Components.MetroLabel metroLabel6;
        private MetroControl.Components.MetroDatePicker pickerNgay;
        private System.Windows.Forms.GroupBox gBoxContact;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private MetroControl.Components.MetroLabel metroLabel8;
        private MetroControl.Components.MetroLabel metroLabel9;
        private MetroControl.Components.MetroLabel metroLabel1;
        private MetroControl.Components.MetroTextbox txtLopHoc;
        private MetroControl.Components.MetroCombobox cbbKhoaHoc;
        private MetroControl.Components.MetroCombobox cbbMaHocVien;
        private MetroControl.Components.MetroLabel metroLabel2;
        private MetroControl.Components.MetroLabel metroLabel5;
        private MetroControl.Components.MetroCombobox cbbPhieuDangKy;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private MetroControl.Components.MetroButton btnXacNhan;
        private MetroControl.Components.MetroGridView grdGioHoc;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private MetroControl.Components.MetroButton btnTaoPhieu;
        private MetroControl.Components.MetroButton btnHuyPhieu;
        private MetroControl.Components.MetroTextbox txtSDT;
        private MetroControl.Components.MetroTextbox txtCMND;
        private MetroControl.Components.MetroLabel metroLabel10;
        private MetroControl.Components.MetroLabel metroLabel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private MetroControl.Components.MetroTextbox txtDiem;
        private MetroControl.Components.MetroPicture picSave;
        private MetroControl.Components.MetroButton btnInPhieu;
    }
}
