﻿namespace MetroUserControl.ContentMenu.User
{
    partial class MetroListUser
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panelBottom = new System.Windows.Forms.TableLayoutPanel();
            this.btnXemChiTiet = new MetroControl.Components.MetroButton();
            this.btnThem = new MetroControl.Components.MetroButton();
            this.btnChonGioHoc = new MetroControl.Components.MetroButton();
            this.metroLabel1 = new MetroControl.Components.MetroLabel();
            this.cbbLoc = new MetroControl.Components.MetroCombobox();
            this.metroLabel2 = new MetroControl.Components.MetroLabel();
            this.txtTimKiem = new MetroControl.Components.MetroTextbox();
            this.picSearch = new MetroControl.Components.MetroPicture();
            this.grdUser = new MetroControl.Controls.MetroGridViewPage();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.gửiMailChoTàiKhoảnNàyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel2.SuspendLayout();
            this.panelBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSearch)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.ColumnCount = 6;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.45455F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.09091F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.45455F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel2.Controls.Add(this.panelBottom, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.cbbLoc, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel2, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtTimKiem, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.picSearch, 5, 0);
            this.tableLayoutPanel2.Controls.Add(this.grdUser, 0, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 5;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545455F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90.90909F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545455F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(660, 400);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // panelBottom
            // 
            this.panelBottom.ColumnCount = 7;
            this.tableLayoutPanel2.SetColumnSpan(this.panelBottom, 6);
            this.panelBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.panelBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.panelBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.panelBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.panelBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.panelBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.panelBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.panelBottom.Controls.Add(this.btnXemChiTiet, 1, 0);
            this.panelBottom.Controls.Add(this.btnThem, 3, 0);
            this.panelBottom.Controls.Add(this.btnChonGioHoc, 5, 0);
            this.panelBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBottom.Location = new System.Drawing.Point(0, 363);
            this.panelBottom.Margin = new System.Windows.Forms.Padding(0);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.RowCount = 1;
            this.panelBottom.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.panelBottom.Size = new System.Drawing.Size(660, 37);
            this.panelBottom.TabIndex = 24;
            // 
            // btnXemChiTiet
            // 
            this.btnXemChiTiet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnXemChiTiet.Location = new System.Drawing.Point(93, 3);
            this.btnXemChiTiet.Name = "btnXemChiTiet";
            this.btnXemChiTiet.Size = new System.Drawing.Size(94, 31);
            this.btnXemChiTiet.TabIndex = 0;
            this.btnXemChiTiet.Text = "XEM CHI TIẾT";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnXemChiTiet.UseSelectable = true;
            // 
            // btnThem
            // 
            this.btnThem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnThem.Location = new System.Drawing.Point(283, 3);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(94, 31);
            this.btnThem.TabIndex = 1;
            this.btnThem.Text = "THÊM";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnThem.UseSelectable = true;
            // 
            // btnChonGioHoc
            // 
            this.btnChonGioHoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnChonGioHoc.Location = new System.Drawing.Point(473, 3);
            this.btnChonGioHoc.Name = "btnChonGioHoc";
            this.btnChonGioHoc.Size = new System.Drawing.Size(94, 31);
            this.btnChonGioHoc.TabIndex = 2;
            this.btnChonGioHoc.Text = "CHỌN GIỜ HỌC";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnChonGioHoc.UseSelectable = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel1.IsFocus = false;
            this.metroLabel1.Location = new System.Drawing.Point(3, 0);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(87, 32);
            this.metroLabel1.TabIndex = 25;
            this.metroLabel1.Text = "Lọc:";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // cbbLoc
            // 
            this.cbbLoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbLoc.DisplayMember = "Value";
            this.cbbLoc.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbbLoc.FormattingEnabled = true;
            this.cbbLoc.ItemHeight = 19;
            this.cbbLoc.Items.AddRange(new object[] {
            "Toàn Bộ",
            "Trình Độ"});
            this.cbbLoc.Location = new System.Drawing.Point(100, 3);
            this.cbbLoc.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.cbbLoc.Name = "cbbLoc";
            this.cbbLoc.Size = new System.Drawing.Size(188, 25);
            this.cbbLoc.TabIndex = 0;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.cbbLoc.UseSelectable = true;
            this.cbbLoc.ValueMember = "Key";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel2.IsFocus = false;
            this.metroLabel2.Location = new System.Drawing.Point(328, 0);
            this.metroLabel2.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(87, 32);
            this.metroLabel2.TabIndex = 27;
            this.metroLabel2.Text = "Tìm Kiếm:";
            this.metroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtTimKiem
            // 
            this.txtTimKiem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTimKiem.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtTimKiem.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtTimKiem.Location = new System.Drawing.Point(425, 4);
            this.txtTimKiem.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtTimKiem.Name = "txtTimKiem";
            this.txtTimKiem.Size = new System.Drawing.Size(188, 23);
            this.txtTimKiem.TabIndex = 28;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // picSearch
            // 
            this.picSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picSearch.Image = global::MetroUserControl.Properties.Resources.magnifying_glass;
            this.picSearch.Location = new System.Drawing.Point(616, 3);
            this.picSearch.Name = "picSearch";
            this.picSearch.Size = new System.Drawing.Size(41, 26);
            this.picSearch.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picSearch.TabIndex = 29;
            this.picSearch.TabStop = false;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // grdUser
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.grdUser, 6);
            this.grdUser.ContextMenuStrip = this.contextMenuStrip1;
            this.grdUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdUser.Location = new System.Drawing.Point(3, 50);
            this.grdUser.Name = "grdUser";
            this.grdUser.Size = new System.Drawing.Size(654, 295);
            this.grdUser.TabIndex = 30;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gửiMailChoTàiKhoảnNàyToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(216, 48);
            // 
            // gửiMailChoTàiKhoảnNàyToolStripMenuItem
            // 
            this.gửiMailChoTàiKhoảnNàyToolStripMenuItem.Name = "gửiMailChoTàiKhoảnNàyToolStripMenuItem";
            this.gửiMailChoTàiKhoảnNàyToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.gửiMailChoTàiKhoảnNàyToolStripMenuItem.Text = "Gửi mail cho tài khoản này";
            this.gửiMailChoTàiKhoảnNàyToolStripMenuItem.Click += new System.EventHandler(this.gửiMailChoTàiKhoảnNàyToolStripMenuItem_Click);
            // 
            // MetroListUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel2);
            this.Name = "MetroListUser";
            this.Size = new System.Drawing.Size(660, 400);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.panelBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picSearch)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel panelBottom;
        private MetroControl.Components.MetroButton btnXemChiTiet;
        private MetroControl.Components.MetroButton btnThem;
        private MetroControl.Components.MetroButton btnChonGioHoc;
        private MetroControl.Components.MetroLabel metroLabel1;
        private MetroControl.Components.MetroCombobox cbbLoc;
        private MetroControl.Components.MetroLabel metroLabel2;
        private MetroControl.Components.MetroTextbox txtTimKiem;
        private MetroControl.Components.MetroPicture picSearch;
        internal MetroControl.Controls.MetroGridViewPage grdUser;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem gửiMailChoTàiKhoảnNàyToolStripMenuItem;
    }
}
