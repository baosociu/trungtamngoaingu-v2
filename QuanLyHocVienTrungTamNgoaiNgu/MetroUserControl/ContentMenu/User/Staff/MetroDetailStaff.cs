﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroControl;
using MetroControl.Components;
using MetroDataset;
using System.Data.Linq;

namespace MetroUserControl.ContentMenu.User.Staff
{
    public partial class MetroDetailStaff : MetroBubble
    {
        public NHANVIEN _nhanvien;
        String id_nhanvien;
        OpenFileDialog openFileDialog;
        
        public enum Type
        {
            InsertStaff, //btnleft = Them - btnRight = Huy
            UpdateStaff, //btnLeft = CapNhat - btnRight = Huy
            ChangePass //btnLeft = CapNhAT - btnRight = ChangePass
        }
        Type? type = null;

        public MetroDetailStaff()
        {
            InitializeComponent();
            EventHelper.Helper.SetHandHover(this.picboxNhanVien);
            openFileDialog = MetroHelper.createAvatarOpenFileDialog();
            this.VisibleChanged += MetroDetailStaff_VisibleChanged;
            this.picboxNhanVien.Click += (s, e) => MetroHelper.OpenChoseAvatar(openFileDialog,picboxNhanVien);
        }

        private void MetroDetailStaff_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
                settingLayoutByStaff();
        }

        public MetroDetailStaff SetID(string ma)
        {
            id_nhanvien = ma;
            settingLayoutByStaff();
            return this;
        }


        public MetroDetailStaff SetAs(Type? type)
        {
            if (this.type == type)
                return this;
            this.type = type;

            if (type == Type.InsertStaff)
            {
                //giao diện thêm học viên
                this.btnLeft.Text = "THÊM";
                this.btnLeft.TypeData.AddType(MetroControl.Components.Type.Insert);
                this.btnLeft.Click += BtnInsert_Click;

                this.btnRight.Text = "HUỶ";
                this.btnRight.TypeData.AddType(MetroControl.Components.Type.View);
                this.btnRight.Click += BtnCancel_Click;

                //layout
                lblMaNhanVien.Text = "Mã Nhân Viên: "+(new Select()).TaoMaNhanVienTuDong();
            }
            else if (type == Type.UpdateStaff)
            {
                //giao diện cập nhật thông tin
                this.btnLeft.Text = "CẬP NHẬT";
                this.btnLeft.TypeData.AddType(MetroControl.Components.Type.Update);
                this.btnLeft.Click += BtnUpdate_Click;

                this.btnRight.Text = "HUỶ";
                this.btnRight.TypeData.AddType(MetroControl.Components.Type.View);
                this.btnRight.Click += BtnCancel_Click;

                //không cho nhập liệu một số thuộc tính
                txtCMNDNhanVien.ReadOnly = true;
                txtHoTenNhanVien.ReadOnly = true;


            }
            else if (type == Type.ChangePass)
            {
                //giao diện cập nhật thông tin
                this.btnLeft.Text = "CẬP NHẬT";
                this.btnLeft.TypeData.AddType(MetroControl.Components.Type.Update);
                this.btnLeft.Click += BtnUpdate_Click;

                this.btnRight.Text = "ĐỔI MẬT KHẨU";
                this.btnRight.TypeData.AddType(MetroControl.Components.Type.Update);
                this.btnRight.Click += BtnChangePass_Click;

                //không cho nhập liệu một số thuộc tính
                txtCMNDNhanVien.ReadOnly = true;
                txtHoTenNhanVien.ReadOnly = true;

                id_nhanvien = MetroHelper.iduser;
                settingLayoutByStaff(); 
            }
            this.tablePanelBottom.Refresh();
            return this;
        }

        internal void settingLayoutByStaff()
        {
            _nhanvien = (new Select()).GetStaffByID(id_nhanvien);

            if (_nhanvien != null)
            {
                lblMaNhanVien.Text = "Mã Nhân Viên: " + _nhanvien.MANHANVIEN;
                txtHoTenNhanVien.Text = _nhanvien.TENNHANVIEN;
                pickerNgay.Value = _nhanvien.NGAYSINH.Value;
                txtDiaChiNhanVien.Text = _nhanvien.DIACHI;
                txtCMNDNhanVien.Text = _nhanvien.CMND;
                txtEmailNhanVien.Text = _nhanvien.MAIL;
                txtDienThoaiNhanVien.Text = _nhanvien.SODIENTHOAI;
                rdbtnNamNhanVien.Checked = _nhanvien.GIOITINH.ToUpper().Equals("NAM");
                rdbtnNuNhanVien.Checked = _nhanvien.GIOITINH.ToUpper().Equals("NỮ");
                EventHelper.Helper.TryCatch(() => picboxNhanVien.Image = MetroHelper.Base64ToImage(_nhanvien.HINH));
            }
        }

        //TODO: Left button
        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            string TENNHANVIEN = txtHoTenNhanVien.Text;
            string GIOITINH = rdbtnNamNhanVien.Checked ? "NAM" : "NỮ";
            string SODIENTHOAI = txtDienThoaiNhanVien.Text;
            string DIACHI = txtDiaChiNhanVien.Text;
            string CMND = txtCMNDNhanVien.Text;
            string MAIL = txtEmailNhanVien.Text;
            DateTime NGAYSINH = pickerNgay.Value;
            Binary HINH = MetroHelper.ImageToBinary(picboxNhanVien.Image);

            //kiểm tra quyền
            if((!MetroHelper.isMaster()  || MetroHelper.isMasterByID(_nhanvien.MANHANVIEN)) && _nhanvien.MANHANVIEN != MetroHelper.iduser)
            {
                EventHelper.Helper.MessageLessPermision();
                return;
            }

            //kiểm tra rỗng
            if (MetroHelper.IsNullOrEmpty(new object[] { SODIENTHOAI, DIACHI, CMND, NGAYSINH }))
            {
                EventHelper.Helper.MessageLessInformation();
                return;
            }
            //kiểm tra thay đổi
            if(TENNHANVIEN == _nhanvien.TENNHANVIEN && GIOITINH == _nhanvien.GIOITINH && SODIENTHOAI == _nhanvien.SODIENTHOAI
                && DIACHI == _nhanvien.DIACHI && CMND == _nhanvien.CMND && MAIL == _nhanvien.MAIL
                && NGAYSINH == _nhanvien.NGAYSINH && HINH == _nhanvien.HINH)
            {
                EventHelper.Helper.MessageNoChangedInformation();
                return;
            }

            //update
            NHANVIEN nv = new NHANVIEN();
            nv.MANHANVIEN = _nhanvien.MANHANVIEN;
            nv.MATAIKHOAN = _nhanvien.MATAIKHOAN;

            nv.TENNHANVIEN = TENNHANVIEN;
            nv.GIOITINH = GIOITINH;
            nv.SODIENTHOAI = SODIENTHOAI;
            nv.DIACHI = DIACHI;
            nv.CMND = CMND;
            nv.MAIL = MAIL;
            nv.NGAYSINH = NGAYSINH;
            nv.HINH = HINH;

            ResultExcuteQuery result = (new Update()).UpdateStaff(nv);
            EventHelper.Helper.MessageInfomation(result.GetMessage());
            if (result.isSuccessfully())
            {
                _nhanvien = nv;
                if (type == Type.ChangePass)
                    MetroHelper.staff = _nhanvien;
            }
        }

        private void BtnInsert_Click(object sender, EventArgs e)
        {
            string TENNHANVIEN = txtHoTenNhanVien.Text;
            string GIOITINH = rdbtnNamNhanVien.Checked ? "NAM" : "NỮ";
            string SODIENTHOAI = txtDienThoaiNhanVien.Text;
            string DIACHI = txtDiaChiNhanVien.Text;
            string CMND = txtCMNDNhanVien.Text;
            string MAIL = txtEmailNhanVien.Text;
            DateTime NGAYSINH = pickerNgay.Value;
            Binary HINH = MetroHelper.ImageToBinary(picboxNhanVien.Image);

            //kiểm tra rỗng
            if (MetroHelper.IsNullOrEmpty(new object[] { SODIENTHOAI, DIACHI, CMND, NGAYSINH }))
            {
                EventHelper.Helper.MessageLessInformation();
                return;
            }
            string MANHANVIEN = (new Select()).TaoMaNhanVienTuDong();
            NHANVIEN nv = new NHANVIEN();
            nv.MANHANVIEN = MANHANVIEN;
            nv.MATAIKHOAN = MANHANVIEN;
            nv.TENNHANVIEN = TENNHANVIEN.ToUpper();
            nv.GIOITINH = GIOITINH;
            nv.SODIENTHOAI = SODIENTHOAI;
            nv.DIACHI = DIACHI.ToUpper();
            nv.CMND = CMND;
            nv.MAIL = MAIL.ToUpper();
            nv.NGAYSINH = NGAYSINH;
            nv.HINH = HINH;
            //Insert
            ResultExcuteQuery result = (new Insert()).ThemNhanVien(nv);
            EventHelper.Helper.MessageInfomation(result.GetMessage());
            if (result.isSuccessfully())
                this.ParentForm.Close();
        }

        //TODO: Right button
        private void BtnChangePass_Click(object sender, EventArgs e)
        {
            MetroHelper.OpenNewTab(this, new MetroChangePass());
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            (this.ParentForm).Close();
        }
    }
}
