﻿namespace MetroUserControl.ContentMenu.User.Staff
{
    partial class MetroDetailStaff
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lblMaNhanVien = new MetroControl.Components.MetroLabel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.picboxNhanVien = new MetroControl.Components.MetroPicture();
            this.rdbtnNuNhanVien = new System.Windows.Forms.RadioButton();
            this.rdbtnNamNhanVien = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tablePanelInfo = new System.Windows.Forms.TableLayoutPanel();
            this.metroLabel5 = new MetroControl.Components.MetroLabel();
            this.metroLabel2 = new MetroControl.Components.MetroLabel();
            this.txtCMNDNhanVien = new MetroControl.Components.MetroTextbox();
            this.metroLabel1 = new MetroControl.Components.MetroLabel();
            this.txtHoTenNhanVien = new MetroControl.Components.MetroTextbox();
            this.metroLabel3 = new MetroControl.Components.MetroLabel();
            this.pickerNgay = new MetroControl.Components.MetroDatePicker();
            this.gBoxContact = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.txtEmailNhanVien = new MetroControl.Components.MetroTextbox();
            this.metroLabel7 = new MetroControl.Components.MetroLabel();
            this.metroLabel6 = new MetroControl.Components.MetroLabel();
            this.metroLabel9 = new MetroControl.Components.MetroLabel();
            this.txtDienThoaiNhanVien = new MetroControl.Components.MetroTextbox();
            this.txtDiaChiNhanVien = new MetroControl.Components.MetroTextbox();
            this.gboxInfo = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tablePanelBottom = new System.Windows.Forms.TableLayoutPanel();
            this.btnLeft = new MetroControl.Components.MetroButton();
            this.btnRight = new MetroControl.Components.MetroButton();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxNhanVien)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            this.tablePanelInfo.SuspendLayout();
            this.gBoxContact.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.gboxInfo.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tablePanelBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel2.Controls.Add(this.lblMaNhanVien, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 1, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(429, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 8;
            this.tableLayoutPanel1.SetRowSpan(this.tableLayoutPanel2, 2);
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(279, 314);
            this.tableLayoutPanel2.TabIndex = 29;
            // 
            // lblMaNhanVien
            // 
            this.lblMaNhanVien.AutoSize = true;
            this.lblMaNhanVien.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.SetColumnSpan(this.lblMaNhanVien, 2);
            this.lblMaNhanVien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMaNhanVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblMaNhanVien.IsFocus = false;
            this.lblMaNhanVien.Location = new System.Drawing.Point(17, 224);
            this.lblMaNhanVien.Name = "lblMaNhanVien";
            this.lblMaNhanVien.Size = new System.Drawing.Size(243, 90);
            this.lblMaNhanVien.TabIndex = 0;
            this.lblMaNhanVien.Text = "Mã Nhân Viên:";
            this.lblMaNhanVien.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel2.SetColumnSpan(this.tableLayoutPanel4, 2);
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 141F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.picboxNhanVien, 1, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(17, 35);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel2.SetRowSpan(this.tableLayoutPanel4, 6);
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 188F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(243, 186);
            this.tableLayoutPanel4.TabIndex = 14;
            // 
            // picboxNhanVien
            // 
            this.picboxNhanVien.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picboxNhanVien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picboxNhanVien.Image = global::MetroUserControl.Properties.Resources.user_shape;
            this.picboxNhanVien.Location = new System.Drawing.Point(54, 2);
            this.picboxNhanVien.Name = "picboxNhanVien";
            this.picboxNhanVien.Size = new System.Drawing.Size(135, 182);
            this.picboxNhanVien.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picboxNhanVien.TabIndex = 17;
            this.picboxNhanVien.TabStop = false;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // rdbtnNuNhanVien
            // 
            this.rdbtnNuNhanVien.AutoSize = true;
            this.rdbtnNuNhanVien.BackColor = System.Drawing.Color.Transparent;
            this.rdbtnNuNhanVien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rdbtnNuNhanVien.Location = new System.Drawing.Point(126, 0);
            this.rdbtnNuNhanVien.Margin = new System.Windows.Forms.Padding(0);
            this.rdbtnNuNhanVien.Name = "rdbtnNuNhanVien";
            this.rdbtnNuNhanVien.Size = new System.Drawing.Size(127, 32);
            this.rdbtnNuNhanVien.TabIndex = 1;
            this.rdbtnNuNhanVien.Text = "Nữ";
            this.rdbtnNuNhanVien.UseVisualStyleBackColor = false;
            // 
            // rdbtnNamNhanVien
            // 
            this.rdbtnNamNhanVien.AutoSize = true;
            this.rdbtnNamNhanVien.BackColor = System.Drawing.Color.Transparent;
            this.rdbtnNamNhanVien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rdbtnNamNhanVien.Location = new System.Drawing.Point(0, 0);
            this.rdbtnNamNhanVien.Margin = new System.Windows.Forms.Padding(0);
            this.rdbtnNamNhanVien.Name = "rdbtnNamNhanVien";
            this.rdbtnNamNhanVien.Size = new System.Drawing.Size(126, 32);
            this.rdbtnNamNhanVien.TabIndex = 0;
            this.rdbtnNamNhanVien.Text = "Nam";
            this.rdbtnNamNhanVien.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.rdbtnNuNhanVien, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.rdbtnNamNhanVien, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(135, 77);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(253, 32);
            this.tableLayoutPanel3.TabIndex = 22;
            // 
            // tablePanelInfo
            // 
            this.tablePanelInfo.ColumnCount = 4;
            this.tablePanelInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tablePanelInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tablePanelInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.33334F));
            this.tablePanelInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tablePanelInfo.Controls.Add(this.metroLabel5, 1, 4);
            this.tablePanelInfo.Controls.Add(this.metroLabel2, 1, 3);
            this.tablePanelInfo.Controls.Add(this.txtCMNDNhanVien, 2, 4);
            this.tablePanelInfo.Controls.Add(this.tableLayoutPanel3, 2, 3);
            this.tablePanelInfo.Controls.Add(this.metroLabel1, 1, 2);
            this.tablePanelInfo.Controls.Add(this.txtHoTenNhanVien, 2, 1);
            this.tablePanelInfo.Controls.Add(this.metroLabel3, 1, 1);
            this.tablePanelInfo.Controls.Add(this.pickerNgay, 2, 2);
            this.tablePanelInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePanelInfo.Location = new System.Drawing.Point(3, 16);
            this.tablePanelInfo.Name = "tablePanelInfo";
            this.tablePanelInfo.RowCount = 6;
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tablePanelInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tablePanelInfo.Size = new System.Drawing.Size(414, 155);
            this.tablePanelInfo.TabIndex = 0;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel5.IsFocus = true;
            this.metroLabel5.Location = new System.Drawing.Point(25, 109);
            this.metroLabel5.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(100, 32);
            this.metroLabel5.TabIndex = 24;
            this.metroLabel5.Text = "Số CMND(*):";
            this.metroLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel2.IsFocus = false;
            this.metroLabel2.Location = new System.Drawing.Point(25, 77);
            this.metroLabel2.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(100, 32);
            this.metroLabel2.TabIndex = 23;
            this.metroLabel2.Text = "Giới Tính:";
            this.metroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtCMNDNhanVien
            // 
            this.txtCMNDNhanVien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCMNDNhanVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtCMNDNhanVien.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtCMNDNhanVien.Location = new System.Drawing.Point(135, 113);
            this.txtCMNDNhanVien.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtCMNDNhanVien.Name = "txtCMNDNhanVien";
            this.txtCMNDNhanVien.Size = new System.Drawing.Size(253, 23);
            this.txtCMNDNhanVien.TabIndex = 2;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel1.IsFocus = true;
            this.metroLabel1.Location = new System.Drawing.Point(25, 45);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(100, 32);
            this.metroLabel1.TabIndex = 14;
            this.metroLabel1.Text = "Ngày Sinh(*):";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtHoTenNhanVien
            // 
            this.txtHoTenNhanVien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHoTenNhanVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtHoTenNhanVien.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtHoTenNhanVien.Location = new System.Drawing.Point(135, 17);
            this.txtHoTenNhanVien.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtHoTenNhanVien.Name = "txtHoTenNhanVien";
            this.txtHoTenNhanVien.Size = new System.Drawing.Size(253, 23);
            this.txtHoTenNhanVien.TabIndex = 0;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel3.IsFocus = true;
            this.metroLabel3.Location = new System.Drawing.Point(25, 13);
            this.metroLabel3.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(100, 32);
            this.metroLabel3.TabIndex = 5;
            this.metroLabel3.Text = "Họ Và Tên(*):";
            this.metroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // pickerNgay
            // 
            this.pickerNgay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pickerNgay.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.pickerNgay.FontSize = MetroFramework.MetroDateTimeSize.Small;
            this.pickerNgay.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.pickerNgay.Location = new System.Drawing.Point(135, 48);
            this.pickerNgay.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.pickerNgay.MinimumSize = new System.Drawing.Size(0, 25);
            this.pickerNgay.Name = "pickerNgay";
            this.pickerNgay.Size = new System.Drawing.Size(253, 25);
            this.pickerNgay.TabIndex = 1;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // gBoxContact
            // 
            this.gBoxContact.Controls.Add(this.tableLayoutPanel5);
            this.gBoxContact.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gBoxContact.Location = new System.Drawing.Point(3, 183);
            this.gBoxContact.Name = "gBoxContact";
            this.gBoxContact.Size = new System.Drawing.Size(420, 134);
            this.gBoxContact.TabIndex = 28;
            this.gBoxContact.TabStop = false;
            this.gBoxContact.Text = "Thông tin liên lạc";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 4;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.33334F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanel5.Controls.Add(this.txtEmailNhanVien, 2, 3);
            this.tableLayoutPanel5.Controls.Add(this.metroLabel7, 1, 3);
            this.tableLayoutPanel5.Controls.Add(this.metroLabel6, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.metroLabel9, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.txtDienThoaiNhanVien, 2, 2);
            this.tableLayoutPanel5.Controls.Add(this.txtDiaChiNhanVien, 2, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 5;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(414, 115);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // txtEmailNhanVien
            // 
            this.txtEmailNhanVien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEmailNhanVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtEmailNhanVien.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtEmailNhanVien.Location = new System.Drawing.Point(135, 77);
            this.txtEmailNhanVien.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtEmailNhanVien.Name = "txtEmailNhanVien";
            this.txtEmailNhanVien.Size = new System.Drawing.Size(253, 23);
            this.txtEmailNhanVien.TabIndex = 2;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel7.IsFocus = true;
            this.metroLabel7.Location = new System.Drawing.Point(28, 73);
            this.metroLabel7.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(97, 32);
            this.metroLabel7.TabIndex = 27;
            this.metroLabel7.Text = "Email(*):";
            this.metroLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel6.IsFocus = false;
            this.metroLabel6.Location = new System.Drawing.Point(28, 41);
            this.metroLabel6.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(97, 32);
            this.metroLabel6.TabIndex = 26;
            this.metroLabel6.Text = "Số Điện Thoại:";
            this.metroLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel9.IsFocus = false;
            this.metroLabel9.Location = new System.Drawing.Point(28, 9);
            this.metroLabel9.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(97, 32);
            this.metroLabel9.TabIndex = 25;
            this.metroLabel9.Text = "Địa Chỉ:";
            this.metroLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtDienThoaiNhanVien
            // 
            this.txtDienThoaiNhanVien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDienThoaiNhanVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtDienThoaiNhanVien.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtDienThoaiNhanVien.Location = new System.Drawing.Point(135, 45);
            this.txtDienThoaiNhanVien.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtDienThoaiNhanVien.Name = "txtDienThoaiNhanVien";
            this.txtDienThoaiNhanVien.Size = new System.Drawing.Size(253, 23);
            this.txtDienThoaiNhanVien.TabIndex = 1;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtDiaChiNhanVien
            // 
            this.txtDiaChiNhanVien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDiaChiNhanVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtDiaChiNhanVien.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtDiaChiNhanVien.Location = new System.Drawing.Point(135, 13);
            this.txtDiaChiNhanVien.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtDiaChiNhanVien.Name = "txtDiaChiNhanVien";
            this.txtDiaChiNhanVien.Size = new System.Drawing.Size(253, 23);
            this.txtDiaChiNhanVien.TabIndex = 0;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // gboxInfo
            // 
            this.gboxInfo.Controls.Add(this.tablePanelInfo);
            this.gboxInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gboxInfo.Location = new System.Drawing.Point(3, 3);
            this.gboxInfo.Name = "gboxInfo";
            this.gboxInfo.Size = new System.Drawing.Size(420, 174);
            this.gboxInfo.TabIndex = 25;
            this.gboxInfo.TabStop = false;
            this.gboxInfo.Text = "Thông tin cá nhân";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.gBoxContact, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tablePanelBottom, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.gboxInfo, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(711, 397);
            this.tableLayoutPanel1.TabIndex = 37;
            // 
            // tablePanelBottom
            // 
            this.tablePanelBottom.ColumnCount = 5;
            this.tableLayoutPanel1.SetColumnSpan(this.tablePanelBottom, 2);
            this.tablePanelBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tablePanelBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tablePanelBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tablePanelBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tablePanelBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tablePanelBottom.Controls.Add(this.btnLeft, 1, 0);
            this.tablePanelBottom.Controls.Add(this.btnRight, 3, 0);
            this.tablePanelBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePanelBottom.Location = new System.Drawing.Point(0, 361);
            this.tablePanelBottom.Margin = new System.Windows.Forms.Padding(0);
            this.tablePanelBottom.Name = "tablePanelBottom";
            this.tablePanelBottom.RowCount = 1;
            this.tablePanelBottom.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tablePanelBottom.Size = new System.Drawing.Size(711, 36);
            this.tablePanelBottom.TabIndex = 26;
            // 
            // btnLeft
            // 
            this.btnLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnLeft.Location = new System.Drawing.Point(173, 3);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Size = new System.Drawing.Size(94, 30);
            this.btnLeft.TabIndex = 0;
            this.btnLeft.Text = "LEFT";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnLeft.UseSelectable = true;
            // 
            // btnRight
            // 
            this.btnRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnRight.Location = new System.Drawing.Point(443, 3);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(94, 30);
            this.btnRight.TabIndex = 1;
            this.btnRight.Text = "RIGHT";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnRight.UseSelectable = true;
            // 
            // MetroDetailStaff
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "MetroDetailStaff";
            this.Size = new System.Drawing.Size(711, 397);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picboxNhanVien)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tablePanelInfo.ResumeLayout(false);
            this.tablePanelInfo.PerformLayout();
            this.gBoxContact.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.gboxInfo.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tablePanelBottom.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private MetroControl.Components.MetroLabel metroLabel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private MetroControl.Components.MetroLabel lblMaNhanVien;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private MetroControl.Components.MetroPicture picboxNhanVien;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox gBoxContact;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private MetroControl.Components.MetroTextbox txtEmailNhanVien;
        private MetroControl.Components.MetroLabel metroLabel7;
        private MetroControl.Components.MetroLabel metroLabel6;
        private MetroControl.Components.MetroLabel metroLabel9;
        private MetroControl.Components.MetroTextbox txtDienThoaiNhanVien;
        private MetroControl.Components.MetroTextbox txtDiaChiNhanVien;
        private System.Windows.Forms.TableLayoutPanel tablePanelBottom;
        private MetroControl.Components.MetroButton btnLeft;
        private MetroControl.Components.MetroButton btnRight;
        private System.Windows.Forms.GroupBox gboxInfo;
        private System.Windows.Forms.TableLayoutPanel tablePanelInfo;
        private MetroControl.Components.MetroLabel metroLabel5;
        private MetroControl.Components.MetroLabel metroLabel2;
        private MetroControl.Components.MetroTextbox txtCMNDNhanVien;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.RadioButton rdbtnNuNhanVien;
        private System.Windows.Forms.RadioButton rdbtnNamNhanVien;
        private MetroControl.Components.MetroLabel metroLabel1;
        private MetroControl.Components.MetroTextbox txtHoTenNhanVien;
        private MetroControl.Components.MetroDatePicker pickerNgay;
    }
}
