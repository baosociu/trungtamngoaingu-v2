﻿using System;

namespace MetroUserControl.ContentMenu.User
{
    partial class MetroTabListUser
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroTabControl1 = new MetroControl.Components.MetroTabControl();
            this.tabHocVien = new MetroFramework.Controls.MetroTabPage();
            this.tabGiangVien = new MetroFramework.Controls.MetroTabPage();
            this.tabNhanVien = new MetroFramework.Controls.MetroTabPage();
            this.metroUserNhanVien = new MetroUserControl.ContentMenu.User.MetroListUser();
            this.metroUserHocVien = new MetroUserControl.ContentMenu.User.MetroListUser();
            this.metroUserGiangVien = new MetroUserControl.ContentMenu.User.MetroListUser();

            // 
            // metroTabControl1
            // 
            this.metroTabControl1.Controls.Add(this.tabNhanVien);
            this.metroTabControl1.Controls.Add(this.tabHocVien);
            this.metroTabControl1.Controls.Add(this.tabGiangVien);
            this.metroTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroTabControl1.Location = new System.Drawing.Point(0, 0);
            this.metroTabControl1.Name = "metroTabControl1";
            this.metroTabControl1.SelectedIndex = 2;
            this.metroTabControl1.Size = new System.Drawing.Size(789, 363);
            this.metroTabControl1.TabIndex = 0;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.metroTabControl1.UseSelectable = true;

            // tabHocVien
            // 
            this.tabHocVien.Controls.Add(this.metroUserHocVien);
            this.tabHocVien.HorizontalScrollbarBarColor = true;
            this.tabHocVien.HorizontalScrollbarHighlightOnWheel = false;
            this.tabHocVien.HorizontalScrollbarSize = 10;
            this.tabHocVien.Location = new System.Drawing.Point(4, 38);
            this.tabHocVien.Name = "tabHocVien";
            this.tabHocVien.Size = new System.Drawing.Size(781, 321);
            this.tabHocVien.TabIndex = 0;
            this.tabHocVien.Text = "HỌC VIÊN";
            this.tabHocVien.VerticalScrollbarBarColor = true;
            this.tabHocVien.VerticalScrollbarHighlightOnWheel = false;
            this.tabHocVien.VerticalScrollbarSize = 10;
            // 
            // tabGiangVien
            // 
            this.tabGiangVien.Controls.Add(this.metroUserGiangVien);
            this.tabGiangVien.HorizontalScrollbarBarColor = true;
            this.tabGiangVien.HorizontalScrollbarHighlightOnWheel = false;
            this.tabGiangVien.HorizontalScrollbarSize = 10;
            this.tabGiangVien.Location = new System.Drawing.Point(4, 38);
            this.tabGiangVien.Name = "tabGiangVien";
            this.tabGiangVien.Size = new System.Drawing.Size(781, 321);
            this.tabGiangVien.TabIndex = 1;
            this.tabGiangVien.Text = "GIẢNG VIÊN";
            this.tabGiangVien.VerticalScrollbarBarColor = true;
            this.tabGiangVien.VerticalScrollbarHighlightOnWheel = false;
            this.tabGiangVien.VerticalScrollbarSize = 10;

            // tabNhanVien
            // 
            this.tabNhanVien.Controls.Add(this.metroUserNhanVien);
            this.tabNhanVien.HorizontalScrollbarBarColor = true;
            this.tabNhanVien.HorizontalScrollbarHighlightOnWheel = false;
            this.tabNhanVien.HorizontalScrollbarSize = 10;
            this.tabNhanVien.Location = new System.Drawing.Point(4, 38);
            this.tabNhanVien.Name = "tabNhanVien";
            this.tabNhanVien.Size = new System.Drawing.Size(781, 321);
            this.tabNhanVien.TabIndex = 2;
            this.tabNhanVien.Text = "NHÂN VIÊN";
            this.tabNhanVien.VerticalScrollbarBarColor = true;
            this.tabNhanVien.VerticalScrollbarHighlightOnWheel = false;
            this.tabNhanVien.VerticalScrollbarSize = 10;

            // metroUserNhanVien
            // 
            this.metroUserNhanVien.BackColor = System.Drawing.Color.Transparent;
            this.metroUserNhanVien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroUserNhanVien.Location = new System.Drawing.Point(0, 0);
            this.metroUserNhanVien.Name = "metroUserNhanVien";
            this.metroUserNhanVien.Size = new System.Drawing.Size(781, 321);
            this.metroUserNhanVien.TabIndex = 2;
            // 
            // metroUserHocVien
            // 
            this.metroUserHocVien.BackColor = System.Drawing.Color.Transparent;
            this.metroUserHocVien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroUserHocVien.Location = new System.Drawing.Point(0, 0);
            this.metroUserHocVien.Name = "metroUserHocVien";
            this.metroUserHocVien.Size = new System.Drawing.Size(781, 321);
            this.metroUserHocVien.TabIndex = 3;
            // 
            // metroUserGiangVien
            // 
            this.metroUserGiangVien.BackColor = System.Drawing.Color.Transparent;
            this.metroUserGiangVien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroUserGiangVien.Location = new System.Drawing.Point(0, 0);
            this.metroUserGiangVien.Name = "metroUserGiangVien";
            this.metroUserGiangVien.Size = new System.Drawing.Size(781, 321);
            this.metroUserGiangVien.TabIndex = 3;
            // 
            // MetroTabListUser
            // 
            //this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "MetroTabListUser";
            this.Size = new System.Drawing.Size(789, 363);
            this.Controls.Add(this.metroTabControl1);
            this.metroTabControl1.ResumeLayout(false);
            this.tabHocVien.ResumeLayout(false);
            this.tabGiangVien.ResumeLayout(false);
            this.tabNhanVien.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroControl.Components.MetroTabControl metroTabControl1;
        private MetroFramework.Controls.MetroTabPage tabNhanVien;
        private MetroListUser metroUserNhanVien;
        private MetroFramework.Controls.MetroTabPage tabHocVien;
        private MetroFramework.Controls.MetroTabPage tabGiangVien;
        private MetroListUser metroUserHocVien;
        private MetroListUser metroUserGiangVien;
    }
}
