﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroControl.Components;
using MetroDataset;
using MetroControl;
using Export;

namespace MetroUserControl.ContentMenu.User
{
    public partial class MetroSelectSchedule : MetroBubble
    {
        private HOCVIEN _hocvien;
        private TT_DANGKYKHOAHOC phieudangky;

        private string[] thus = new string[] { "MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN" };
        private class Value
        {
            string maca;
            string mathu;
            bool check;

            public string MaCa { get => maca; }
            public string MaThu { get => mathu; }
            public bool Check { get => check; }


            public Value(string maca, string mathu, bool check)
            {
                this.check = check;
                this.maca = maca;
                this.mathu = mathu;
            }
        }
        List<Value> values;


        public MetroSelectSchedule(string id)
        {
            InitializeComponent();
            this.Load += MetroSelectSchedule_Load;
            _hocvien = (new Select()).GetStudentByID(id);
        }

        private void MetroSelectSchedule_Load(object sender, EventArgs e)
        {
            EventHelper.Helper.SetHandHover(picSave);
            this.grdGioHoc.ReadOnly = true;
            this.grdGioHoc.DataSourceChanged += GrdThaoTac_DataSourceChanged;
            this.grdGioHoc.CellContentClick += GrdThaoTac_CellContentClick;
            this.values = new List<Value>();

            //button
            this.btnXacNhan.Click += (s, en) => saveThaoTac();
            this.btnHuy.Click += (s, en) => this.ParentForm.Close();
            this.btnHuyPhieu.Click += (s, en) => clickHuyPhieu();
            this.btnTaoPhieu.Click += (s, en) => clickTaoPhieu();
            this.btnInPhieu.Click += (s, en) => clickInPhieu();

            //cbb
            this.cbbKhoaHoc.SelectedValueChanged += (s, ev) => ChangedCourse(cbbKhoaHoc.SelectedValue);
            this.cbbPhieuDangKy.SelectedValueChanged += (s, eV) => ChangedRegister(cbbPhieuDangKy.SelectedValue);

            picSave.Click += (s, en) => CapNhatDiemDauVao();
            LoadFirst();
        }

        private void clickInPhieu()
        {
            if (MetroHelper.IsNullOrEmpty(new object[] { cbbPhieuDangKy.SelectedValue }))
            {
                EventHelper.Helper.MessageInfomation("Không có thông tin phiếu đăng ký để in");
                return;
            }
            string maphieu = cbbPhieuDangKy.SelectedValue.ToString();
            BieuMau.InChonGioHocHocVien(maphieu);
           
        }

        private void clickTaoPhieu()
        {
            if (_hocvien == null || cbbKhoaHoc.SelectedValue == null || MetroHelper.staff == null)
                return;
            string makhoahoc = cbbKhoaHoc.SelectedValue.ToString();
            string mahocvien = _hocvien.MAHOCVIEN;

            //kiểm tra có lớp đang chờ xếp lớp khoá học ko, nếu có thì ko cho tạo thêm phiếu ddawng ký

            bool b = (new Select()).TonTaiLopChuaXep(mahocvien, makhoahoc);
            if(b)
            {
                EventHelper.Helper.MessageInfomation("Học viên \'" + mahocvien + "\' đã đăng ký khoá học \'" + makhoahoc + "\' và đang chờ xếp lớp");
                return;
            }

            EventHelper.Helper.MessageInfomationYesNo("Thêm phiếu đăng ký khoá học \'" + makhoahoc+"\' cho học viên \'"+mahocvien+"\' ?",()=>{

                ResultExcuteQuery r = (new Insert()).ThemPhieuDangKyKhoaHoc(mahocvien, makhoahoc, MetroHelper.staff.MANHANVIEN);
                EventHelper.Helper.MessageInfomation(r.GetMessage());
                if (r.isSuccessfully())
                    LoadRegisters(makhoahoc, mahocvien);
            });
        }

        private void clickHuyPhieu()
        {
            if(phieudangky == null)
            {
                EventHelper.Helper.MessageInfomation("Hãy chọn một phiếu đăng ký để huỷ");
                return;
            }

            LOPHOC lh = (new Select()).LayLopHocDangKyKhoaHoc(phieudangky.MADANGKYKHOAHOC);
            if (lh != null)
            {
                EventHelper.Helper.MessageInfomation("Phiếu đăng ký này đã được xếp lịch học. Không thể huỷ phiếu");
                return;
            }

            BIENLAI bl = (new Select()).LayBienLaiKhongHuyByMaDangKy(phieudangky.MADANGKYKHOAHOC);
            if(bl != null)
            {
                EventHelper.Helper.MessageInfomation("Phiếu đăng ký này tồn tại thông tin đóng học phí. Hãy huỷ các thông tin biên lai liên quan để tiếp tục.");
                return;
            }

            //huỷ phiếu khi không xếp lớp
            Action clickYes = () =>
            {
                ResultExcuteQuery r= (new Delete()).XoaPhieuDangKyKhoaHoc(phieudangky.MADANGKYKHOAHOC);
                EventHelper.Helper.MessageInfomation(r.GetMessage());
                if (r.isSuccessfully())
                    LoadRegisters(phieudangky.MAKHOAHOC, phieudangky.MAHOCVIEN);
            };

            EventHelper.Helper.MessageInfomationYesNo("Bạn có chắc chắn muốn huỷ phiếu đăng ký khoá học \'" + phieudangky.MADANGKYKHOAHOC + "\' ?", clickYes);
        }

        private void CapNhatDiemDauVao()
        {
            //lưu thông tin điểm số nếu có cập nhật
            string diem = txtDiem.Text;
            if(string.IsNullOrWhiteSpace(diem))
            {
                EventHelper.Helper.MessageLessInformation();
                return;
            }


            if(diem.Equals(phieudangky.DIEMDAUVAO.ToString()))
            {
                EventHelper.Helper.MessageNoChangedInformation();
                return;
            }

            string madangkykhoahoc = cbbPhieuDangKy.SelectedValue.ToString();
            TT_DANGKYKHOAHOC dangkykhoahoc = new TT_DANGKYKHOAHOC()
            {
                MADANGKYKHOAHOC = madangkykhoahoc,
                DIEMDAUVAO = float.Parse(diem),
                NGAY = DateTime.Now
            };

            ResultExcuteQuery r = (new Update()).CapNhatTTDangKyKhoaHoc(dangkykhoahoc);
            EventHelper.Helper.MessageInfomation(r.GetMessage());
        }

        private void LoadFirst()
        {
            LoadInforStudent();

            string mahocvien = _hocvien.MAHOCVIEN;
            this.cbbKhoaHoc.DataSource = (new Select()).HienThiTatCaKhoaHoc_Cbb();

        }

        //chọn check
        private void GrdThaoTac_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex >= 3)
            {
                bool b = !bool.Parse((sender as DataGridView)[e.ColumnIndex, e.RowIndex].Value.ToString());
                (sender as DataGridView)[e.ColumnIndex, e.RowIndex].Value = b;

                //LƯU THAO TÁC CỦA NGƯỜI DÙNG
                string id_ca = "CA" + (e.RowIndex + 1).ToString();
                string id_thu = thus[e.ColumnIndex - 3];
                this.values.Add(new Value(id_ca, id_thu, b));
            }
        }

        private void GrdThaoTac_DataSourceChanged(object sender, EventArgs e)
        {
            DataGridView grid = (sender as DataGridView);
            for (int i = 2; i < grid.Columns.Count; i++)
                grid.Columns[i].ReadOnly = false;
        }

        private void saveThaoTac()
        {
            if (MetroHelper.IsNullOrEmpty(new object[] { cbbPhieuDangKy.SelectedValue}))
            {
                EventHelper.Helper.MessageLessInformation();
                return;
            }

            //nếu như phiếu đăng ký khoá học đã được xếp lớp thì ko được thay đổi thông tin chọn giờ
            LOPHOC lh = (new Select()).LayLopHocDangKyKhoaHoc(phieudangky.MADANGKYKHOAHOC);
            if (lh != null)
            {
                EventHelper.Helper.MessageInfomation("Phiếu đăng ký này đã được xếp lịch học. Không thể thay đổi thông tin chọn giờ học");
                values = new List<Value>();
                if (_hocvien != null && phieudangky != null)
                    LoadGridView(phieudangky.MADANGKYKHOAHOC);
                return;
            }

            //lưu lại tt_chọn giờ học
            if(values.Count == 0)
            {
                EventHelper.Helper.MessageNoChangedInformation();
                return;
            }

            string madangkykhoahoc = cbbPhieuDangKy.SelectedValue.ToString();
            foreach (Value v in values)
            {
                string maca = v.MaCa;
                string mathu = v.MaThu;
                bool check = v.Check;
                (new MetroDataset.Update()).CapNhatTTChonGioHocVien(madangkykhoahoc, maca, mathu, check);
            }
            values = new List<Value>();
            EventHelper.Helper.MessageInfomation("Cập nhật thông tin chọn giờ học học viên hoàn tất.");
        }

        private void ChangedCourse(object selectedValue)
        {
            if (MetroHelper.IsNullOrEmpty(new object[] { selectedValue }))
                return;

            string mahocvien = _hocvien.MAHOCVIEN;
            string makhoahoc = cbbKhoaHoc.SelectedValue.ToString(); //tên

            //load cbb


            LoadRegisters(makhoahoc, mahocvien);
        }

        private void LoadRegisters(string makhoahoc, string mahocvien)
        {
            txtDiem.Clear();
            txtLopHoc.Clear();
            var data = (new Select()).HienThiDangKyKhoaHocHocVien_Cbb(makhoahoc, mahocvien);
            cbbPhieuDangKy.DataSource = data;
            if (cbbPhieuDangKy.Items.Count != 0)
            {
                ChangedRegister(cbbPhieuDangKy.SelectedValue);
                VisibleGridBox(true);
            }
            else
            {
                VisibleGridBox(false);
                phieudangky = null;
                EventHelper.Helper.MessageInfomation("Học viên \'" + _hocvien.MAHOCVIEN + "\' hiện chưa đăng ký khoá học \'" + cbbKhoaHoc.Text + "\'");
            }
        }

        private void VisibleGridBox(bool visible)
        {
            this.grdGioHoc.Visible = visible;
            this.btnXacNhan.Visible = visible;
            this.txtDiem.Enabled = visible;
            this.pickerNgay.Enabled = visible;
        }

        private void ChangedRegister(object selectedValue)
        {
            if (MetroHelper.IsNullOrEmpty(new object[] { selectedValue }))
                return;

            string madangky = selectedValue.ToString();
            phieudangky = (new Select()).LayTTDangKhoaHoc(madangky);

            txtDiem.Text = phieudangky.DIEMDAUVAO.ToString();
            LOPHOC lh = (new Select()).LayLopHocDangKyKhoaHoc(madangky);
            txtLopHoc.Text = lh != null ? lh.MALOPHOC : "Chưa có";

            if (_hocvien != null && phieudangky != null)
                LoadGridView(phieudangky.MADANGKYKHOAHOC);

        }

        private void LoadGridView(String madangky)
        {
            using (DataSetQuanLyHocVienTableAdapters.HienThiTTChonGioHocHocVienTableAdapter dt = new DataSetQuanLyHocVienTableAdapters.HienThiTTChonGioHocHocVienTableAdapter())
            {
                DataTable tb = dt.GetData(madangky);
                foreach (System.Data.DataColumn col in tb.Columns)
                    col.ReadOnly = false;
                grdGioHoc.DataSource = tb;
            }
            EventHelper.Helper.TryCatch(() =>
            {
                grdGioHoc.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                grdGioHoc.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                grdGioHoc.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            });
        }

        private void LoadInforStudent()
        {
            if (_hocvien != null)
            {
                txtHoTenHocVien.Text = _hocvien.TENHOCVIEN;
                txtCMND.Text = _hocvien.CMND;
                txtSDT.Text = _hocvien.SODIENTHOAI;
                pickerNgay.Value = _hocvien.NGAYSINH.Value;
                cbbMaHocVien.DataSource = new List<object>() { new { Key = _hocvien.MAHOCVIEN, Value = _hocvien.MAHOCVIEN } };
                rdbtnNamHocVien.Checked = _hocvien.GIOITINH.ToUpper().Equals("NAM");
                rdbtnNuHocVien.Checked = _hocvien.GIOITINH.ToUpper().Equals("NỮ");
            }
        }
    }
}
