﻿namespace MetroUserControl.ContentMenu.User
{
    partial class MetroChangePass
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.metroLabel1 = new MetroControl.Components.MetroLabel();
            this.metroLabel2 = new MetroControl.Components.MetroLabel();
            this.metroLabel3 = new MetroControl.Components.MetroLabel();
            this.txtMatKhauMoiChangePass = new MetroControl.Components.MetroTextbox();
            this.txtXacThucChangePass = new MetroControl.Components.MetroTextbox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btnAccept = new MetroControl.Components.MetroButton();
            this.btnHuy = new MetroControl.Components.MetroButton();
            this.txtMatKhauCuChangePass = new MetroControl.Components.MetroTextbox();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.74129F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.25871F));
            this.tableLayoutPanel2.Controls.Add(this.metroLabel1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel2, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel3, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.txtMatKhauMoiChangePass, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtXacThucChangePass, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.txtMatKhauCuChangePass, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 5;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(340, 160);
            this.tableLayoutPanel2.TabIndex = 4;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel1.IsFocus = true;
            this.metroLabel1.Location = new System.Drawing.Point(0, 0);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(102, 32);
            this.metroLabel1.TabIndex = 3;
            this.metroLabel1.Text = "Mật Khẩu Cũ(*):";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel2.IsFocus = true;
            this.metroLabel2.Location = new System.Drawing.Point(0, 32);
            this.metroLabel2.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(102, 32);
            this.metroLabel2.TabIndex = 4;
            this.metroLabel2.Text = "Mật Khẩu Mới(*):";
            this.metroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel3.IsFocus = true;
            this.metroLabel3.Location = new System.Drawing.Point(0, 64);
            this.metroLabel3.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(102, 32);
            this.metroLabel3.TabIndex = 5;
            this.metroLabel3.Text = "Xác Nhận(*):";
            this.metroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtMatKhauMoiChangePass
            // 
            this.txtMatKhauMoiChangePass.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.SetColumnSpan(this.txtMatKhauMoiChangePass, 2);
            this.txtMatKhauMoiChangePass.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtMatKhauMoiChangePass.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtMatKhauMoiChangePass.Location = new System.Drawing.Point(112, 36);
            this.txtMatKhauMoiChangePass.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtMatKhauMoiChangePass.Name = "txtMatKhauMoiChangePass";
            this.txtMatKhauMoiChangePass.Size = new System.Drawing.Size(228, 23);
            this.txtMatKhauMoiChangePass.TabIndex = 7;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.txtMatKhauMoiChangePass.UseSystemPasswordChar = true;
            // 
            // txtXacThucChangePass
            // 
            this.txtXacThucChangePass.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.SetColumnSpan(this.txtXacThucChangePass, 2);
            this.txtXacThucChangePass.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtXacThucChangePass.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtXacThucChangePass.Location = new System.Drawing.Point(112, 68);
            this.txtXacThucChangePass.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtXacThucChangePass.Name = "txtXacThucChangePass";
            this.txtXacThucChangePass.Size = new System.Drawing.Size(228, 23);
            this.txtXacThucChangePass.TabIndex = 8;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.txtXacThucChangePass.UseSystemPasswordChar = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 5;
            this.tableLayoutPanel2.SetColumnSpan(this.tableLayoutPanel3, 3);
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.Controls.Add(this.btnAccept, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnHuy, 3, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 124);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(340, 36);
            this.tableLayoutPanel3.TabIndex = 9;
            // 
            // btnAccept
            // 
            this.btnAccept.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAccept.Location = new System.Drawing.Point(49, 3);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(94, 30);
            this.btnAccept.TabIndex = 0;
            this.btnAccept.Text = "ĐỒNG Ý";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnAccept.UseSelectable = true;
            // 
            // btnHuy
            // 
            this.btnHuy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnHuy.Location = new System.Drawing.Point(195, 3);
            this.btnHuy.Name = "btnHuy";
            this.btnHuy.Size = new System.Drawing.Size(94, 30);
            this.btnHuy.TabIndex = 1;
            this.btnHuy.Text = "HUỶ";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnHuy.UseSelectable = true;
            // 
            // txtMatKhauCuChangePass
            // 
            this.txtMatKhauCuChangePass.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.SetColumnSpan(this.txtMatKhauCuChangePass, 2);
            this.txtMatKhauCuChangePass.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtMatKhauCuChangePass.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtMatKhauCuChangePass.Location = new System.Drawing.Point(112, 4);
            this.txtMatKhauCuChangePass.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtMatKhauCuChangePass.Name = "txtMatKhauCuChangePass";
            this.txtMatKhauCuChangePass.Size = new System.Drawing.Size(228, 23);
            this.txtMatKhauCuChangePass.TabIndex = 6;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.txtMatKhauCuChangePass.UseSystemPasswordChar = true;
            // 
            // MetroChangePass
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel2);
            this.Name = "MetroChangePass";
            this.Size = new System.Drawing.Size(340, 160);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private MetroControl.Components.MetroLabel metroLabel1;
        private MetroControl.Components.MetroLabel metroLabel2;
        private MetroControl.Components.MetroLabel metroLabel3;
        private MetroControl.Components.MetroTextbox txtMatKhauMoiChangePass;
        private MetroControl.Components.MetroTextbox txtXacThucChangePass;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private MetroControl.Components.MetroButton btnAccept;
        private MetroControl.Components.MetroButton btnHuy;
        private MetroControl.Components.MetroTextbox txtMatKhauCuChangePass;
    }
}
