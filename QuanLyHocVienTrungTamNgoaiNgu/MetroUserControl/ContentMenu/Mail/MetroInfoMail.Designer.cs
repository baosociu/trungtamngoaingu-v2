﻿namespace MetroUserControl.ContentMenu.Mail
{
    partial class MetroInfoMail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.gboxMail = new System.Windows.Forms.GroupBox();
            this.panelInfoMail = new System.Windows.Forms.TableLayoutPanel();
            this.metroLabel7 = new MetroControl.Components.MetroLabel();
            this.txtMailGui = new MetroControl.Components.MetroTextbox();
            this.metroLabel11 = new MetroControl.Components.MetroLabel();
            this.metroLabel1 = new MetroControl.Components.MetroLabel();
            this.metroLabel5 = new MetroControl.Components.MetroLabel();
            this.txtMatKhau = new MetroControl.Components.MetroTextbox();
            this.cbbChuDe = new MetroControl.Components.MetroCombobox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.richTextBox1 = new MetroControl.Components.MetroRichTextbox();
            this.gboxList = new System.Windows.Forms.GroupBox();
            this.grid = new MetroControl.Components.MetroGridView();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.lblDinhKem = new MetroControl.Components.MetroLabel();
            this.metroLabel2 = new MetroControl.Components.MetroLabel();
            this.txtCountMail = new MetroControl.Components.MetroTextbox();
            this.cbDinhKem = new MetroControl.Components.MetroCheckbox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.gboxMail.SuspendLayout();
            this.panelInfoMail.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.gboxList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(635, 435);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 47.05882F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.882353F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 47.05883F));
            this.tableLayoutPanel2.Controls.Add(this.gboxMail, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.gboxList, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(629, 397);
            this.tableLayoutPanel2.TabIndex = 25;
            // 
            // gboxMail
            // 
            this.gboxMail.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.gboxMail, 3);
            this.gboxMail.Controls.Add(this.panelInfoMail);
            this.gboxMail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gboxMail.Location = new System.Drawing.Point(3, 3);
            this.gboxMail.Name = "gboxMail";
            this.gboxMail.Size = new System.Drawing.Size(623, 232);
            this.gboxMail.TabIndex = 11;
            this.gboxMail.TabStop = false;
            this.gboxMail.Text = "Thông tin mail";
            // 
            // panelInfoMail
            // 
            this.panelInfoMail.AllowDrop = true;
            this.panelInfoMail.AutoScroll = true;
            this.panelInfoMail.AutoSize = true;
            this.panelInfoMail.BackColor = System.Drawing.Color.Transparent;
            this.panelInfoMail.ColumnCount = 7;
            this.panelInfoMail.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.545537F));
            this.panelInfoMail.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.panelInfoMail.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.45356F));
            this.panelInfoMail.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.panelInfoMail.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.panelInfoMail.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.45537F));
            this.panelInfoMail.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.545537F));
            this.panelInfoMail.Controls.Add(this.metroLabel7, 1, 1);
            this.panelInfoMail.Controls.Add(this.txtMailGui, 2, 0);
            this.panelInfoMail.Controls.Add(this.metroLabel11, 4, 0);
            this.panelInfoMail.Controls.Add(this.metroLabel1, 1, 0);
            this.panelInfoMail.Controls.Add(this.metroLabel5, 1, 2);
            this.panelInfoMail.Controls.Add(this.txtMatKhau, 5, 0);
            this.panelInfoMail.Controls.Add(this.cbbChuDe, 2, 1);
            this.panelInfoMail.Controls.Add(this.tableLayoutPanel4, 2, 2);
            this.panelInfoMail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelInfoMail.Location = new System.Drawing.Point(3, 16);
            this.panelInfoMail.Name = "panelInfoMail";
            this.panelInfoMail.RowCount = 4;
            this.panelInfoMail.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.panelInfoMail.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.panelInfoMail.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.panelInfoMail.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.panelInfoMail.Size = new System.Drawing.Size(617, 213);
            this.panelInfoMail.TabIndex = 10;
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel7.IsFocus = false;
            this.metroLabel7.Location = new System.Drawing.Point(18, 32);
            this.metroLabel7.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(80, 32);
            this.metroLabel7.TabIndex = 41;
            this.metroLabel7.Text = "Chủ Đề:";
            this.metroLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtMailGui
            // 
            this.txtMailGui.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMailGui.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtMailGui.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtMailGui.Location = new System.Drawing.Point(108, 4);
            this.txtMailGui.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtMailGui.Name = "txtMailGui";
            this.txtMailGui.ReadOnly = true;
            this.txtMailGui.Size = new System.Drawing.Size(184, 23);
            this.txtMailGui.TabIndex = 37;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel11.IsFocus = true;
            this.metroLabel11.Location = new System.Drawing.Point(312, 0);
            this.metroLabel11.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(90, 32);
            this.metroLabel11.TabIndex = 36;
            this.metroLabel11.Text = "Mật Khẩu(*):";
            this.metroLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel1.IsFocus = false;
            this.metroLabel1.Location = new System.Drawing.Point(18, 0);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(80, 32);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "Mail Gửi:";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel5.IsFocus = false;
            this.metroLabel5.Location = new System.Drawing.Point(18, 64);
            this.metroLabel5.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(80, 32);
            this.metroLabel5.TabIndex = 22;
            this.metroLabel5.Text = "Nội Dung:";
            this.metroLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtMatKhau
            // 
            this.txtMatKhau.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMatKhau.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtMatKhau.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtMatKhau.Location = new System.Drawing.Point(412, 4);
            this.txtMatKhau.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtMatKhau.Name = "txtMatKhau";
            this.txtMatKhau.Size = new System.Drawing.Size(185, 23);
            this.txtMatKhau.TabIndex = 42;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.txtMatKhau.UseSystemPasswordChar = true;
            // 
            // cbbChuDe
            // 
            this.cbbChuDe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.panelInfoMail.SetColumnSpan(this.cbbChuDe, 4);
            this.cbbChuDe.DisplayMember = "Value";
            this.cbbChuDe.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbbChuDe.FormattingEnabled = true;
            this.cbbChuDe.ItemHeight = 19;
            this.cbbChuDe.Location = new System.Drawing.Point(108, 35);
            this.cbbChuDe.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.cbbChuDe.Name = "cbbChuDe";
            this.cbbChuDe.Size = new System.Drawing.Size(489, 25);
            this.cbbChuDe.TabIndex = 32;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.cbbChuDe.UseSelectable = true;
            this.cbbChuDe.ValueMember = "Key";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel4.ColumnCount = 1;
            this.panelInfoMail.SetColumnSpan(this.tableLayoutPanel4, 4);
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.richTextBox1, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(108, 64);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.panelInfoMail.SetRowSpan(this.tableLayoutPanel4, 2);
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(489, 149);
            this.tableLayoutPanel4.TabIndex = 43;
            // 
            // richTextBox1
            // 
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tableLayoutPanel4.SetColumnSpan(this.richTextBox1, 4);
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox1.Location = new System.Drawing.Point(1, 1);
            this.richTextBox1.Margin = new System.Windows.Forms.Padding(0);
            this.richTextBox1.Name = "richTextBox1";
            this.tableLayoutPanel4.SetRowSpan(this.richTextBox1, 2);
            this.richTextBox1.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.richTextBox1.Size = new System.Drawing.Size(487, 147);
            this.richTextBox1.TabIndex = 44;
            this.richTextBox1.Text = "";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // gboxList
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.gboxList, 3);
            this.gboxList.Controls.Add(this.grid);
            this.gboxList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gboxList.Location = new System.Drawing.Point(3, 241);
            this.gboxList.Name = "gboxList";
            this.gboxList.Padding = new System.Windows.Forms.Padding(8, 3, 8, 8);
            this.gboxList.Size = new System.Drawing.Size(623, 153);
            this.gboxList.TabIndex = 12;
            this.gboxList.TabStop = false;
            this.gboxList.Text = "Danh sách mail nhận";
            // 
            // grid
            // 
            this.grid.AllowUserToAddRows = false;
            this.grid.AllowUserToDeleteRows = false;
            this.grid.AllowUserToResizeRows = false;
            this.grid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.grid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.grid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grid.DefaultCellStyle = dataGridViewCellStyle2;
            this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid.EnableHeadersVisualStyles = false;
            this.grid.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.grid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.grid.Location = new System.Drawing.Point(8, 16);
            this.grid.MultiSelect = false;
            this.grid.Name = "grid";
            this.grid.ReadOnly = true;
            this.grid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grid.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.grid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grid.Size = new System.Drawing.Size(607, 129);
            this.grid.TabIndex = 14;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.Controls.Add(this.lblDinhKem, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.metroLabel2, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtCountMail, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.cbDinhKem, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(12, 403);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(12, 0, 12, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(611, 32);
            this.tableLayoutPanel3.TabIndex = 26;
            // 
            // lblDinhKem
            // 
            this.lblDinhKem.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblDinhKem.AutoSize = true;
            this.lblDinhKem.BackColor = System.Drawing.Color.Transparent;
            this.lblDinhKem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Underline);
            this.lblDinhKem.ForeColor = System.Drawing.Color.DarkBlue;
            this.lblDinhKem.IsFocus = false;
            this.lblDinhKem.Location = new System.Drawing.Point(103, 8);
            this.lblDinhKem.Name = "lblDinhKem";
            this.lblDinhKem.Size = new System.Drawing.Size(37, 15);
            this.lblDinhKem.TabIndex = 4;
            this.lblDinhKem.Text = "None";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel2
            // 
            this.metroLabel2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel2.IsFocus = false;
            this.metroLabel2.Location = new System.Drawing.Point(425, 8);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(83, 15);
            this.metroLabel2.TabIndex = 1;
            this.metroLabel2.Text = "Số mail nhận:";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtCountMail
            // 
            this.txtCountMail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCountMail.Enabled = false;
            this.txtCountMail.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtCountMail.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtCountMail.Location = new System.Drawing.Point(511, 4);
            this.txtCountMail.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtCountMail.Name = "txtCountMail";
            this.txtCountMail.ReadOnly = true;
            this.txtCountMail.Size = new System.Drawing.Size(100, 23);
            this.txtCountMail.TabIndex = 2;
            this.txtCountMail.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // cbDinhKem
            // 
            this.cbDinhKem.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cbDinhKem.AutoSize = true;
            this.cbDinhKem.BackColor = System.Drawing.Color.Transparent;
            this.cbDinhKem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.cbDinhKem.Location = new System.Drawing.Point(3, 6);
            this.cbDinhKem.Name = "cbDinhKem";
            this.cbDinhKem.Size = new System.Drawing.Size(81, 19);
            this.cbDinhKem.TabIndex = 5;
            this.cbDinhKem.Text = "Đính Kèm";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.cbDinhKem.UseVisualStyleBackColor = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // MetroInfoMail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "MetroInfoMail";
            this.Size = new System.Drawing.Size(635, 435);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.gboxMail.ResumeLayout(false);
            this.gboxMail.PerformLayout();
            this.panelInfoMail.ResumeLayout(false);
            this.panelInfoMail.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.gboxList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.GroupBox gboxMail;
        private System.Windows.Forms.TableLayoutPanel panelInfoMail;
        private MetroControl.Components.MetroLabel metroLabel7;
        private MetroControl.Components.MetroLabel metroLabel11;
        private MetroControl.Components.MetroLabel metroLabel1;
        private MetroControl.Components.MetroLabel metroLabel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private MetroControl.Components.MetroLabel metroLabel2;
        public MetroControl.Components.MetroTextbox txtMailGui;
        public MetroControl.Components.MetroTextbox txtMatKhau;
        public MetroControl.Components.MetroCombobox cbbChuDe;
        public MetroControl.Components.MetroGridView grid;
        public MetroControl.Components.MetroTextbox txtCountMail;
        public MetroControl.Components.MetroCheckbox cbDinhKem;
        public MetroControl.Components.MetroLabel lblDinhKem;
        public System.Windows.Forms.GroupBox gboxList;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        public MetroControl.Components.MetroRichTextbox richTextBox1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}
