﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Net;
using MetroDataset;
using MetroControl;

namespace MetroUserControl.ContentMenu.Mail
{
    public partial class MetroMail : MetroBubble
    {

        public MetroBubble pre_user;

        public MetroMail()
        {
            InitializeComponent();
            this.Load += MetroMail_Load;
            pre_user = null;
        }

        private void MetroMail_Load(object sender, EventArgs e)
        {
            this.btnHuy.Click += BtnHuy_Click;
            this.btnGui.Click += (s, en) => EventHelper.Helper.TryCatch(this.infoMail.SentMail);

        }

        private void BtnHuy_Click(object sender, EventArgs e)
        {
            if (pre_user != null)
                MetroHelper.GoToTab(this.ParentForm, pre_user);
            else
                this.ParentForm.Close();
        }

        public MetroMail SetStaff(NHANVIEN staff)
        {
            if (staff != null)
            {
                infoMail.txtMailGui.Text = staff.MAIL;
            }
            return this;
        }



        //kiểm tra địa chỉ mail
        public bool IsValidEmailAddress(string emailaddress)
        {
            try
            {
                Regex rx = new Regex(
            @"^[-!#$%&'*+/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+/0-9=?A-Z^_a-z{|}~])*@[a-zA-Z](-?[a-zA-Z0-9])*(\.[a-zA-Z](-?[a-zA-Z0-9])*)+$");
                return rx.IsMatch(emailaddress);
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public MetroMail SetDataClass(List<object> dt, string id_class)
        {
            infoMail.grid.DataSource = dt;
            infoMail.gboxList.Text = "Danh sách mail lớp " + id_class;
            infoMail.txtCountMail.Text = dt.Count.ToString();

            //staff
            NHANVIEN staff = this.GetStaff();
            if (staff != null)
                infoMail.txtMailGui.Text = staff.MAIL;
            return this;
        }

        public MetroMail SetDataReceiver(List<object> receiver)
        {
            infoMail.grid.DataSource = receiver;
            infoMail.gboxList.Text = "Thông tin người nhận";
            infoMail.txtCountMail.Text = receiver.Count.ToString();

            //staff
            NHANVIEN staff = this.GetStaff();
            if (staff != null)
                infoMail.txtMailGui.Text = staff.MAIL;

            return this;
        }


    }
}
