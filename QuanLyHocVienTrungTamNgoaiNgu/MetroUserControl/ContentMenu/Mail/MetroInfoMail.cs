﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroDataset;
using MetroControl;
using System.Net.Mail;
using System.Net;

namespace MetroUserControl.ContentMenu.Mail
{
    public partial class MetroInfoMail : UserControl
    {
        MailMessage message;
        SmtpClient smtp;
        public MetroInfoMail()
        {
            InitializeComponent();
            cbbChuDe.DataSource =
                new List<object>
                {
                    new { Key = "THÔNG BÁO", Value = "THÔNG BÁO" },
                    new { Key = "THÔNG BÁO MỞ LỚP", Value = "THÔNG BÁO MỞ LỚP" },
                    new { Key = "THÔNG BÁO THAY ĐỔI LỊCH HỌC", Value = "THÔNG BÁO THAY ĐỔI LỊCH HỌC" },
                    new { Key =  "THÔNG BÁO TẠO TÀI KHOẢN", Value = "THÔNG BÁO TẠO TÀI KHOẢN" },
                    new { Key =   "THÔNG BÁO TỔ CHỨC THI" , Value = "THÔNG BÁO TỔ CHỨC THI" }
                };
            cbDinhKem.CheckedChanged += CbDinhKem_CheckedChanged;
            EventHelper.Helper.SetHandHover(lblDinhKem);
            lblDinhKem.Visible = false;
            setupOpenDialog();

        }

        public void SetDataInfoMail(List<string> madangkykhoahoc, string magiangvien, string makhoahoc)
        {

            cbbChuDe.SelectedIndex = 1;
            //lấy danh sách thông tin mail
            var data = (new Select()).HienThiMailThongBaoMoLop(madangkykhoahoc, magiangvien, makhoahoc);

            grid.DataSource = data;
            gboxList.Text = "Danh sách mail thông báo mở lớp";
            txtCountMail.Text = data.Count.ToString();

            //staff
            NHANVIEN staff = MetroHelper.staff;
            if (staff != null)
                txtMailGui.Text = staff.MAIL;
        }

        private void CbDinhKem_CheckedChanged(object sender, EventArgs e)
        {
            bool @checked = cbDinhKem.Checked;
            if (@checked)
            {
                DialogResult r = openFileDialog1.ShowDialog();
                if (r == DialogResult.OK)
                {
                    lblDinhKem.Text = openFileDialog1.FileName;
                    lblDinhKem.Visible = true;
                }
                else
                    cbDinhKem.Checked = false;
            }
            else
            {
                lblDinhKem.Text = "None";
                lblDinhKem.Visible = false;
            }
        }

        private void setupOpenDialog()
        {
            openFileDialog1.Title = "Chọn tệp đính kèm";
            openFileDialog1.DefaultExt = "jpg";
            openFileDialog1.Filter = "JPG Files (*.jpg)|*.jpg|WinRar Files (*.rar)|*.rar|WinZip Files (*.zip)|*.zip|All Files (*.*)|*.*";
            openFileDialog1.CheckFileExists = true;
            openFileDialog1.CheckPathExists = true;
        }

        public void SentMail()
        {
            //kiểm tra
            string mailGui = txtMailGui.Text;
            string matKhau = txtMatKhau.Text;
            string chuDe = cbbChuDe.Text;
            string noiDung = richTextBox1.Text;
            int countMailNhan = grid.RowCount;
            if (String.IsNullOrWhiteSpace(mailGui) || String.IsNullOrWhiteSpace(matKhau)
                || String.IsNullOrWhiteSpace(chuDe) || String.IsNullOrWhiteSpace(noiDung) || countMailNhan == 0)
                return;
            else
            {
                message = new MailMessage();
                smtp = new SmtpClient();
                smtp.Port = 587;
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                //gửi
                try
                {
                    message.From = new MailAddress(mailGui);
                    message.Subject = chuDe;
                    message.Body = noiDung;
                }
                catch (Exception)
                {
                    MessageBox.Show("Hãy kiểm tra lại thông tin người gửi !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                //đính kèm 
                if (cbDinhKem.Checked)
                    message.Attachments.Add(new Attachment(lblDinhKem.Text));
                smtp.Credentials = new NetworkCredential(mailGui, matKhau);

                foreach (DataGridViewRow r in grid.Rows)
                {
                    string strMail = r.Cells[2].Value.ToString();
                    message.To.Add(strMail);
                }

                //tiến hành gửi
                //smtp.Send(message);
                MessageBox.Show("Gửi mail");

            }
        }
    }
}
