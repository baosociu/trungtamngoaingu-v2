﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroControl;
using static MetroUserControl.DataSetQuanLyHocVien;
using MetroDataset;
using MetroDataset.Model;

namespace MetroUserControl.ContentMenu.Class
{
    public partial class MetroDetailClass : MetroBubble
    {
        String malop;
        public enum Type
        {
            CreateClass, DetailClass
        }
        public MetroDetailClass(String malop)
        {
            InitializeComponent();
            this.Load += MetroDetailClass_Load;
            this.malop = malop;
        }


        private void MetroDetailClass_Load(object sender, EventArgs e)
        {
            btnTaoLop.Hide();
            object[] data = (new MetroDataset.Select()).HienThiThongTinLopHoc(malop);
            if (data != null)
            {
                EventHelper.Helper.TryCatch(() =>
                {
                    LOPHOC lophoc = (LOPHOC)data[0];
                    KHOAHOC khoahoc = (KHOAHOC)data[1];
                    GIANGVIEN giangvien = (GIANGVIEN)data[2];

                    txtMaLop.Text = lophoc.MALOPHOC;
                    txtMaKhoaHoc.Text = khoahoc.MAKHOAHOC;
                    cbbKhoaHoc.PromptText = khoahoc.TENKHOAHOC;
                    txtSoLuong.Text = lophoc.SOLUONG.ToString();
                    txtHocPhiInfClass.Text = khoahoc.HOCPHI.ToString();
                    txtMaGiangVien.Text = giangvien.MAGIANGVIEN;
                    cbbGiangVien.PromptText = giangvien.TENGIANGVIEN;
                    txtTrinhDo.Text = giangvien.TRINHDO;
                    txtChuyenMon.Text = giangvien.CHUYENMON;
                });
            }

            this.radioHocVien.CheckedChanged += (s, en) => { LoadGridHocVien(this.radioHocVien.Checked, malop); };
            this.radioLichHoc.CheckedChanged += (s, en) => { LoadGridLichHoc(this.radioLichHoc.Checked, malop); };
            this.btnIn.Click += (s, en) => ClickIn();

            cbbKhoaHoc.Enabled = false;
            cbbGiangVien.Enabled = false;
            radioHocVien.Checked = false;
            radioHocVien.Checked = true;
            this.btnHuy.Click += (s, en) => { this.ParentForm.Close(); };
        }

        private void ClickIn()
        {
            if (radioHocVien.Checked)
                BieuMau.InDanhSachHocVien(txtMaLop.Text);
            else
                BieuMau.InDanhSachLichHoc(txtMaLop.Text);

        }


        #region sub grid
        private void LoadGridHocVien(bool @checked, string malop)
        {
            //Hiển thị học viên của lớp đó
            if (@checked)
            {
                this.btnIn.Text = "IN DS HỌC VIÊN";
                var c = ((new MetroDataset.Select()).HienThiHocVienLopHoc(malop));
                grid.DataSource = c;
                grid.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                grid.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                grid.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }
        }

        private void LoadGridLichHoc(bool @checked, string malop)
        {
            if (@checked)
            {
                this.btnIn.Text = "IN DS LỊCH HỌC";
                var c = ((new MetroDataset.Select()).HienThiLichHocLopHoc(malop));
                grid.DataSource = c;
                grid.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                grid.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                grid.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }
        }
        #endregion
    }
}
