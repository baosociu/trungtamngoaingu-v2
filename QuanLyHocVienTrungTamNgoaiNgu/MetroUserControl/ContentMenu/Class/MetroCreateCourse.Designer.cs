﻿namespace MetroUserControl.ContentMenu.Class
{
    partial class MetroCreateCourse
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grdCourse = new MetroControl.Controls.MetroGridViewPage();
            this.tableLayoutPanelCourse = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.picCreate = new MetroControl.Components.MetroPicture();
            this.metroLabel3 = new MetroControl.Components.MetroLabel();
            this.txtHocPhiKhoaHoc = new MetroControl.Components.MetroTextbox();
            this.metroLabel4 = new MetroControl.Components.MetroLabel();
            this.txtMaKhoaHoc = new MetroControl.Components.MetroTextbox();
            this.txtTenKhoaHoc = new MetroControl.Components.MetroTextbox();
            this.metroLabel5 = new MetroControl.Components.MetroLabel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.rdbtnCapNhatKhoaHoc = new System.Windows.Forms.RadioButton();
            this.rdbtnThemKhoaHoc = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.btnHuy = new MetroControl.Components.MetroButton();
            this.btnXoa = new MetroControl.Components.MetroButton();
            this.metroLabel1 = new MetroControl.Components.MetroLabel();
            this.cbbLoc = new MetroControl.Components.MetroCombobox();
            this.metroLabel2 = new MetroControl.Components.MetroLabel();
            this.txtTimKiem = new MetroControl.Components.MetroTextbox();
            this.picSearch = new MetroControl.Components.MetroPicture();
            this.tableLayoutPanelCourse.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCreate)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSearch)).BeginInit();
            this.SuspendLayout();
            // 
            // grdCourse
            // 
            this.tableLayoutPanelCourse.SetColumnSpan(this.grdCourse, 6);
            this.grdCourse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdCourse.Location = new System.Drawing.Point(3, 150);
            this.grdCourse.Name = "grdCourse";
            this.grdCourse.Size = new System.Drawing.Size(782, 390);
            this.grdCourse.TabIndex = 30;
            // 
            // tableLayoutPanelCourse
            // 
            this.tableLayoutPanelCourse.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanelCourse.ColumnCount = 6;
            this.tableLayoutPanelCourse.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanelCourse.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.45454F));
            this.tableLayoutPanelCourse.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tableLayoutPanelCourse.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanelCourse.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.45454F));
            this.tableLayoutPanelCourse.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanelCourse.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanelCourse.Controls.Add(this.tableLayoutPanel4, 0, 7);
            this.tableLayoutPanelCourse.Controls.Add(this.metroLabel1, 0, 4);
            this.tableLayoutPanelCourse.Controls.Add(this.cbbLoc, 1, 4);
            this.tableLayoutPanelCourse.Controls.Add(this.metroLabel2, 3, 4);
            this.tableLayoutPanelCourse.Controls.Add(this.txtTimKiem, 4, 4);
            this.tableLayoutPanelCourse.Controls.Add(this.picSearch, 5, 4);
            this.tableLayoutPanelCourse.Controls.Add(this.grdCourse, 0, 5);
            this.tableLayoutPanelCourse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelCourse.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelCourse.Name = "tableLayoutPanelCourse";
            this.tableLayoutPanelCourse.RowCount = 8;
            this.tableLayoutPanelCourse.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanelCourse.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanelCourse.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanelCourse.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.549794F));
            this.tableLayoutPanelCourse.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanelCourse.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90.90497F));
            this.tableLayoutPanelCourse.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545248F));
            this.tableLayoutPanelCourse.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanelCourse.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelCourse.Size = new System.Drawing.Size(788, 600);
            this.tableLayoutPanelCourse.TabIndex = 6;
            // 
            // groupBox1
            // 
            this.tableLayoutPanelCourse.SetColumnSpan(this.groupBox1, 6);
            this.groupBox1.Controls.Add(this.tableLayoutPanel5);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanelCourse.SetRowSpan(this.groupBox1, 3);
            this.groupBox1.Size = new System.Drawing.Size(782, 90);
            this.groupBox1.TabIndex = 54;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thêm khóa học";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 6;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.45455F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.09091F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.45454F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 59F));
            this.tableLayoutPanel5.Controls.Add(this.picCreate, 5, 1);
            this.tableLayoutPanel5.Controls.Add(this.metroLabel3, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.txtHocPhiKhoaHoc, 4, 1);
            this.tableLayoutPanel5.Controls.Add(this.metroLabel4, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.txtMaKhoaHoc, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.txtTenKhoaHoc, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.metroLabel5, 3, 1);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel3, 4, 2);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 13);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 4;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(782, 77);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // picCreate
            // 
            this.picCreate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picCreate.Image = global::MetroUserControl.Properties.Resources.group_profile_users_create;
            this.picCreate.Location = new System.Drawing.Point(724, 9);
            this.picCreate.Name = "picCreate";
            this.picCreate.Size = new System.Drawing.Size(55, 26);
            this.picCreate.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picCreate.TabIndex = 53;
            this.picCreate.TabStop = false;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel3.IsFocus = false;
            this.metroLabel3.Location = new System.Drawing.Point(3, 6);
            this.metroLabel3.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(87, 32);
            this.metroLabel3.TabIndex = 51;
            this.metroLabel3.Text = "Mã khóa học:";
            this.metroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtHocPhiKhoaHoc
            // 
            this.txtHocPhiKhoaHoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHocPhiKhoaHoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtHocPhiKhoaHoc.InputType = MetroControl.Components.MetroTextbox.Input.NumberInput;
            this.txtHocPhiKhoaHoc.Location = new System.Drawing.Point(484, 10);
            this.txtHocPhiKhoaHoc.Margin = new System.Windows.Forms.Padding(0);
            this.txtHocPhiKhoaHoc.Name = "txtHocPhiKhoaHoc";
            this.txtHocPhiKhoaHoc.Size = new System.Drawing.Size(237, 23);
            this.txtHocPhiKhoaHoc.TabIndex = 0;
            this.txtHocPhiKhoaHoc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel4.IsFocus = false;
            this.metroLabel4.Location = new System.Drawing.Point(3, 38);
            this.metroLabel4.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(87, 32);
            this.metroLabel4.TabIndex = 50;
            this.metroLabel4.Text = "Tên khóa học:";
            this.metroLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtMaKhoaHoc
            // 
            this.txtMaKhoaHoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaKhoaHoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtMaKhoaHoc.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtMaKhoaHoc.Location = new System.Drawing.Point(100, 10);
            this.txtMaKhoaHoc.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtMaKhoaHoc.Name = "txtMaKhoaHoc";
            this.txtMaKhoaHoc.ReadOnly = true;
            this.txtMaKhoaHoc.Size = new System.Drawing.Size(237, 23);
            this.txtMaKhoaHoc.TabIndex = 52;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtTenKhoaHoc
            // 
            this.txtTenKhoaHoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTenKhoaHoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtTenKhoaHoc.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtTenKhoaHoc.Location = new System.Drawing.Point(100, 42);
            this.txtTenKhoaHoc.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtTenKhoaHoc.Name = "txtTenKhoaHoc";
            this.txtTenKhoaHoc.ReadOnly = true;
            this.txtTenKhoaHoc.Size = new System.Drawing.Size(237, 23);
            this.txtTenKhoaHoc.TabIndex = 54;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel5.IsFocus = false;
            this.metroLabel5.Location = new System.Drawing.Point(387, 6);
            this.metroLabel5.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(87, 32);
            this.metroLabel5.TabIndex = 0;
            this.metroLabel5.Text = "Học phí:";
            this.metroLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.rdbtnCapNhatKhoaHoc, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.rdbtnThemKhoaHoc, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(484, 38);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(237, 32);
            this.tableLayoutPanel3.TabIndex = 55;
            // 
            // rdbtnCapNhatKhoaHoc
            // 
            this.rdbtnCapNhatKhoaHoc.AutoSize = true;
            this.rdbtnCapNhatKhoaHoc.BackColor = System.Drawing.Color.Transparent;
            this.rdbtnCapNhatKhoaHoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rdbtnCapNhatKhoaHoc.Location = new System.Drawing.Point(118, 0);
            this.rdbtnCapNhatKhoaHoc.Margin = new System.Windows.Forms.Padding(0);
            this.rdbtnCapNhatKhoaHoc.Name = "rdbtnCapNhatKhoaHoc";
            this.rdbtnCapNhatKhoaHoc.Size = new System.Drawing.Size(119, 32);
            this.rdbtnCapNhatKhoaHoc.TabIndex = 1;
            this.rdbtnCapNhatKhoaHoc.Text = "Cập nhật khóa học";
            this.rdbtnCapNhatKhoaHoc.UseVisualStyleBackColor = false;
            // 
            // rdbtnThemKhoaHoc
            // 
            this.rdbtnThemKhoaHoc.AutoSize = true;
            this.rdbtnThemKhoaHoc.BackColor = System.Drawing.Color.Transparent;
            this.rdbtnThemKhoaHoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rdbtnThemKhoaHoc.Location = new System.Drawing.Point(0, 0);
            this.rdbtnThemKhoaHoc.Margin = new System.Windows.Forms.Padding(0);
            this.rdbtnThemKhoaHoc.Name = "rdbtnThemKhoaHoc";
            this.rdbtnThemKhoaHoc.Size = new System.Drawing.Size(118, 32);
            this.rdbtnThemKhoaHoc.TabIndex = 0;
            this.rdbtnThemKhoaHoc.Text = "Thêm khóa học";
            this.rdbtnThemKhoaHoc.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 5;
            this.tableLayoutPanelCourse.SetColumnSpan(this.tableLayoutPanel4, 6);
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Controls.Add(this.btnHuy, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnXoa, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 562);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(788, 38);
            this.tableLayoutPanel4.TabIndex = 24;
            // 
            // btnHuy
            // 
            this.btnHuy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnHuy.Location = new System.Drawing.Point(495, 3);
            this.btnHuy.Name = "btnHuy";
            this.btnHuy.Size = new System.Drawing.Size(94, 32);
            this.btnHuy.TabIndex = 4;
            this.btnHuy.Text = "HỦY";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnHuy.UseSelectable = true;
            // 
            // btnXoa
            // 
            this.btnXoa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnXoa.Location = new System.Drawing.Point(199, 3);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(94, 32);
            this.btnXoa.TabIndex = 5;
            this.btnXoa.Text = "XÓA";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnXoa.UseSelectable = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel1.IsFocus = false;
            this.metroLabel1.Location = new System.Drawing.Point(3, 115);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(87, 32);
            this.metroLabel1.TabIndex = 25;
            this.metroLabel1.Text = "Lọc:";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // cbbLoc
            // 
            this.cbbLoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbLoc.DisplayMember = "Value";
            this.cbbLoc.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbbLoc.FormattingEnabled = true;
            this.cbbLoc.ItemHeight = 19;
            this.cbbLoc.Items.AddRange(new object[] {
            "Toàn Bộ",
            "Trình Độ"});
            this.cbbLoc.Location = new System.Drawing.Point(100, 118);
            this.cbbLoc.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.cbbLoc.Name = "cbbLoc";
            this.cbbLoc.Size = new System.Drawing.Size(240, 25);
            this.cbbLoc.TabIndex = 0;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.cbbLoc.UseSelectable = true;
            this.cbbLoc.ValueMember = "Key";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel2.IsFocus = false;
            this.metroLabel2.Location = new System.Drawing.Point(391, 115);
            this.metroLabel2.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(87, 32);
            this.metroLabel2.TabIndex = 27;
            this.metroLabel2.Text = "Tìm Kiếm:";
            this.metroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtTimKiem
            // 
            this.txtTimKiem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTimKiem.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtTimKiem.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtTimKiem.Location = new System.Drawing.Point(488, 119);
            this.txtTimKiem.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtTimKiem.Name = "txtTimKiem";
            this.txtTimKiem.Size = new System.Drawing.Size(240, 23);
            this.txtTimKiem.TabIndex = 28;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // picSearch
            // 
            this.picSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picSearch.Image = global::MetroUserControl.Properties.Resources.magnifying_glass;
            this.picSearch.Location = new System.Drawing.Point(731, 118);
            this.picSearch.Name = "picSearch";
            this.picSearch.Size = new System.Drawing.Size(54, 26);
            this.picSearch.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picSearch.TabIndex = 29;
            this.picSearch.TabStop = false;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // MetroCreateCourse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanelCourse);
            this.Name = "MetroCreateCourse";
            this.Size = new System.Drawing.Size(788, 600);
            this.tableLayoutPanelCourse.ResumeLayout(false);
            this.tableLayoutPanelCourse.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCreate)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picSearch)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal MetroControl.Controls.MetroGridViewPage grdCourse;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelCourse;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private MetroControl.Components.MetroButton btnHuy;
        private MetroControl.Components.MetroLabel metroLabel1;
        private MetroControl.Components.MetroCombobox cbbLoc;
        private MetroControl.Components.MetroLabel metroLabel2;
        private MetroControl.Components.MetroTextbox txtTimKiem;
        private MetroControl.Components.MetroPicture picSearch;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private MetroControl.Components.MetroLabel metroLabel3;
        private MetroControl.Components.MetroLabel metroLabel4;
        private MetroControl.Components.MetroTextbox txtHocPhiKhoaHoc;
        private MetroControl.Components.MetroTextbox txtMaKhoaHoc;
        private MetroControl.Components.MetroPicture picCreate;
        private MetroControl.Components.MetroTextbox txtTenKhoaHoc;
        private MetroControl.Components.MetroLabel metroLabel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.RadioButton rdbtnCapNhatKhoaHoc;
        private System.Windows.Forms.RadioButton rdbtnThemKhoaHoc;
        private MetroControl.Components.MetroButton btnXoa;
    }
}
