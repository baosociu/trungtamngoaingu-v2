﻿using MetroControl.Components;

namespace MetroUserControl.ContentMenu.Class
{
    partial class MetroCreateClass
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.btnHuy = new MetroControl.Components.MetroButton();
            this.btnTaoLop = new MetroControl.Components.MetroButton();
            this.tabControl = new MetroControl.Components.MetroTabControl();
            this.tabHocVien = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.studentDetailGroupBox = new MetroUserControl.GroupBox.MetroDetailScheduleStudentGroupBox();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.listStudentRegisterGroupBox = new MetroUserControl.GroupBox.MetroListStudentRegisterGroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.gbGiangVien = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.txtMaGiangVien = new MetroControl.Components.MetroTextbox();
            this.txtTrinhDo = new MetroControl.Components.MetroTextbox();
            this.metroLabel3 = new MetroControl.Components.MetroLabel();
            this.txtChuyenMon = new MetroControl.Components.MetroTextbox();
            this.metroLabel2 = new MetroControl.Components.MetroLabel();
            this.metroLabel4 = new MetroControl.Components.MetroLabel();
            this.metroLabel10 = new MetroControl.Components.MetroLabel();
            this.cbbGiangVien = new MetroControl.Components.MetroCombobox();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.gboxLopHoc = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.metroLabel7 = new MetroControl.Components.MetroLabel();
            this.txtSoLuong = new MetroControl.Components.MetroTextbox();
            this.txtMaLop = new MetroControl.Components.MetroTextbox();
            this.metroLabel1 = new MetroControl.Components.MetroLabel();
            this.metroLabel5 = new MetroControl.Components.MetroLabel();
            this.txtHocPhiInfClass = new MetroControl.Components.MetroTextbox();
            this.metroLabel9 = new MetroControl.Components.MetroLabel();
            this.cbbKhoaHoc = new MetroControl.Components.MetroCombobox();
            this.cboxConfirmHocVien = new MetroControl.Components.MetroCheckbox();
            this.tabLich = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.cboxConfirmLichHoc = new MetroControl.Components.MetroCheckbox();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.listDetailScheduleGroupBox = new MetroUserControl.GroupBox.MetroListScheduleGroupBox();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.settingsScheduleGroupBox = new MetroUserControl.GroupBox.MetroSettingsScheduleGroupBox();
            this.tabMail = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.cboxConfirmMaiL = new MetroControl.Components.MetroCheckbox();
            this.infoMail = new MetroUserControl.ContentMenu.Mail.MetroInfoMail();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabHocVien.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            this.tableLayoutPanel17.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.gbGiangVien.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.gboxLopHoc.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tabLich.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.tabMail.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tabControl, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(751, 529);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 5;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel4, 5);
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Controls.Add(this.btnHuy, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnTaoLop, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 493);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(751, 36);
            this.tableLayoutPanel4.TabIndex = 25;
            // 
            // btnHuy
            // 
            this.btnHuy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnHuy.Location = new System.Drawing.Point(469, 3);
            this.btnHuy.Name = "btnHuy";
            this.btnHuy.Size = new System.Drawing.Size(94, 30);
            this.btnHuy.TabIndex = 2;
            this.btnHuy.Text = "HUỶ";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnHuy.UseSelectable = true;
            // 
            // btnTaoLop
            // 
            this.btnTaoLop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTaoLop.Location = new System.Drawing.Point(186, 3);
            this.btnTaoLop.Name = "btnTaoLop";
            this.btnTaoLop.Size = new System.Drawing.Size(94, 30);
            this.btnTaoLop.TabIndex = 1;
            this.btnTaoLop.Text = "TẠO LỚP";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnTaoLop.UseSelectable = true;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabHocVien);
            this.tabControl.Controls.Add(this.tabLich);
            this.tabControl.Controls.Add(this.tabMail);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(3, 3);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 1;
            this.tabControl.Size = new System.Drawing.Size(745, 487);
            this.tabControl.TabIndex = 0;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.tabControl.UseSelectable = true;
            // 
            // tabHocVien
            // 
            this.tabHocVien.BackColor = System.Drawing.Color.Transparent;
            this.tabHocVien.Controls.Add(this.tableLayoutPanel2);
            this.tabHocVien.Location = new System.Drawing.Point(4, 38);
            this.tabHocVien.Name = "tabHocVien";
            this.tabHocVien.Size = new System.Drawing.Size(737, 445);
            this.tabHocVien.TabIndex = 0;
            this.tabHocVien.Text = "HỌC VIÊN";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel10, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.cboxConfirmHocVien, 0, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(737, 445);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 3;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.95961F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.080784F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.95961F));
            this.tableLayoutPanel10.Controls.Add(this.tableLayoutPanel16, 2, 0);
            this.tableLayoutPanel10.Controls.Add(this.tableLayoutPanel17, 0, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(3, 209);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(731, 200);
            this.tableLayoutPanel10.TabIndex = 27;
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 1;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.Controls.Add(this.studentDetailGroupBox, 0, 0);
            this.tableLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel16.Location = new System.Drawing.Point(372, 0);
            this.tableLayoutPanel16.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 1;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(359, 200);
            this.tableLayoutPanel16.TabIndex = 47;
            // 
            // studentDetailGroupBox
            // 
            this.studentDetailGroupBox.BackColor = System.Drawing.Color.Transparent;
            this.studentDetailGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.studentDetailGroupBox.Location = new System.Drawing.Point(3, 3);
            this.studentDetailGroupBox.Name = "studentDetailGroupBox";
            this.studentDetailGroupBox.Size = new System.Drawing.Size(353, 194);
            this.studentDetailGroupBox.TabIndex = 0;
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.ColumnCount = 1;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.Controls.Add(this.listStudentRegisterGroupBox, 0, 0);
            this.tableLayoutPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel17.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel17.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 1;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(357, 200);
            this.tableLayoutPanel17.TabIndex = 48;
            // 
            // listStudentRegisterGroupBox
            // 
            this.listStudentRegisterGroupBox.BackColor = System.Drawing.Color.Transparent;
            this.listStudentRegisterGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listStudentRegisterGroupBox.Location = new System.Drawing.Point(3, 3);
            this.listStudentRegisterGroupBox.Name = "listStudentRegisterGroupBox";
            this.listStudentRegisterGroupBox.Size = new System.Drawing.Size(351, 194);
            this.listStudentRegisterGroupBox.TabIndex = 0;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.95961F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.080784F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.95961F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel5, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel8, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(731, 200);
            this.tableLayoutPanel3.TabIndex = 25;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 4;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.33333F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel5.Controls.Add(this.gbGiangVien, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(372, 0);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(359, 200);
            this.tableLayoutPanel5.TabIndex = 16;
            // 
            // gbGiangVien
            // 
            this.tableLayoutPanel5.SetColumnSpan(this.gbGiangVien, 4);
            this.gbGiangVien.Controls.Add(this.tableLayoutPanel6);
            this.gbGiangVien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbGiangVien.Location = new System.Drawing.Point(3, 3);
            this.gbGiangVien.Name = "gbGiangVien";
            this.gbGiangVien.Size = new System.Drawing.Size(353, 162);
            this.gbGiangVien.TabIndex = 43;
            this.gbGiangVien.TabStop = false;
            this.gbGiangVien.Text = "Thông tin giảng viên";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.AllowDrop = true;
            this.tableLayoutPanel6.AutoScroll = true;
            this.tableLayoutPanel6.AutoSize = true;
            this.tableLayoutPanel6.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel6.ColumnCount = 4;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.33333F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel6.Controls.Add(this.txtMaGiangVien, 2, 1);
            this.tableLayoutPanel6.Controls.Add(this.txtTrinhDo, 2, 3);
            this.tableLayoutPanel6.Controls.Add(this.metroLabel3, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.txtChuyenMon, 2, 4);
            this.tableLayoutPanel6.Controls.Add(this.metroLabel2, 1, 4);
            this.tableLayoutPanel6.Controls.Add(this.metroLabel4, 1, 2);
            this.tableLayoutPanel6.Controls.Add(this.metroLabel10, 1, 3);
            this.tableLayoutPanel6.Controls.Add(this.cbbGiangVien, 2, 2);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 6;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(347, 143);
            this.tableLayoutPanel6.TabIndex = 10;
            // 
            // txtMaGiangVien
            // 
            this.txtMaGiangVien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaGiangVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtMaGiangVien.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtMaGiangVien.Location = new System.Drawing.Point(129, 11);
            this.txtMaGiangVien.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtMaGiangVien.Name = "txtMaGiangVien";
            this.txtMaGiangVien.ReadOnly = true;
            this.txtMaGiangVien.Size = new System.Drawing.Size(197, 23);
            this.txtMaGiangVien.TabIndex = 41;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtTrinhDo
            // 
            this.txtTrinhDo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTrinhDo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtTrinhDo.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtTrinhDo.Location = new System.Drawing.Point(129, 75);
            this.txtTrinhDo.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtTrinhDo.Name = "txtTrinhDo";
            this.txtTrinhDo.ReadOnly = true;
            this.txtTrinhDo.Size = new System.Drawing.Size(197, 23);
            this.txtTrinhDo.TabIndex = 40;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel3.IsFocus = false;
            this.metroLabel3.Location = new System.Drawing.Point(22, 7);
            this.metroLabel3.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(97, 32);
            this.metroLabel3.TabIndex = 39;
            this.metroLabel3.Text = "Mã Giảng Viên:";
            this.metroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtChuyenMon
            // 
            this.txtChuyenMon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtChuyenMon.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtChuyenMon.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtChuyenMon.Location = new System.Drawing.Point(129, 107);
            this.txtChuyenMon.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtChuyenMon.Name = "txtChuyenMon";
            this.txtChuyenMon.ReadOnly = true;
            this.txtChuyenMon.Size = new System.Drawing.Size(197, 23);
            this.txtChuyenMon.TabIndex = 38;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel2.IsFocus = false;
            this.metroLabel2.Location = new System.Drawing.Point(22, 103);
            this.metroLabel2.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(97, 32);
            this.metroLabel2.TabIndex = 37;
            this.metroLabel2.Text = "Chuyên Môn:";
            this.metroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel4.IsFocus = true;
            this.metroLabel4.Location = new System.Drawing.Point(19, 39);
            this.metroLabel4.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(100, 32);
            this.metroLabel4.TabIndex = 2;
            this.metroLabel4.Text = "Giảng Viên(*):";
            this.metroLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel10.IsFocus = false;
            this.metroLabel10.Location = new System.Drawing.Point(22, 71);
            this.metroLabel10.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(97, 32);
            this.metroLabel10.TabIndex = 29;
            this.metroLabel10.Text = "Trình Độ:";
            this.metroLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // cbbGiangVien
            // 
            this.cbbGiangVien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbGiangVien.DisplayMember = "Value";
            this.cbbGiangVien.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbbGiangVien.FormattingEnabled = true;
            this.cbbGiangVien.ItemHeight = 19;
            this.cbbGiangVien.Location = new System.Drawing.Point(129, 42);
            this.cbbGiangVien.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.cbbGiangVien.Name = "cbbGiangVien";
            this.cbbGiangVien.Size = new System.Drawing.Size(197, 25);
            this.cbbGiangVien.TabIndex = 32;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.cbbGiangVien.UseSelectable = true;
            this.cbbGiangVien.ValueMember = "Key";
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 4;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.33333F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel8.Controls.Add(this.gboxLopHoc, 0, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel8.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 2;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(357, 200);
            this.tableLayoutPanel8.TabIndex = 14;
            // 
            // gboxLopHoc
            // 
            this.tableLayoutPanel8.SetColumnSpan(this.gboxLopHoc, 4);
            this.gboxLopHoc.Controls.Add(this.tableLayoutPanel9);
            this.gboxLopHoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gboxLopHoc.Location = new System.Drawing.Point(3, 3);
            this.gboxLopHoc.Name = "gboxLopHoc";
            this.gboxLopHoc.Size = new System.Drawing.Size(351, 162);
            this.gboxLopHoc.TabIndex = 41;
            this.gboxLopHoc.TabStop = false;
            this.gboxLopHoc.Text = "Thông tin lớp học";
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.AllowDrop = true;
            this.tableLayoutPanel9.AutoScroll = true;
            this.tableLayoutPanel9.AutoSize = true;
            this.tableLayoutPanel9.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel9.ColumnCount = 4;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.33333F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel9.Controls.Add(this.metroLabel7, 1, 2);
            this.tableLayoutPanel9.Controls.Add(this.txtSoLuong, 2, 3);
            this.tableLayoutPanel9.Controls.Add(this.txtMaLop, 2, 1);
            this.tableLayoutPanel9.Controls.Add(this.metroLabel1, 1, 1);
            this.tableLayoutPanel9.Controls.Add(this.metroLabel5, 1, 3);
            this.tableLayoutPanel9.Controls.Add(this.txtHocPhiInfClass, 2, 4);
            this.tableLayoutPanel9.Controls.Add(this.metroLabel9, 1, 4);
            this.tableLayoutPanel9.Controls.Add(this.cbbKhoaHoc, 2, 2);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 6;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(345, 143);
            this.tableLayoutPanel9.TabIndex = 10;
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel7.IsFocus = true;
            this.metroLabel7.Location = new System.Drawing.Point(20, 39);
            this.metroLabel7.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(90, 32);
            this.metroLabel7.TabIndex = 41;
            this.metroLabel7.Text = "Khoá Học(*):";
            this.metroLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtSoLuong
            // 
            this.txtSoLuong.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSoLuong.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtSoLuong.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtSoLuong.Location = new System.Drawing.Point(120, 75);
            this.txtSoLuong.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtSoLuong.Name = "txtSoLuong";
            this.txtSoLuong.ReadOnly = true;
            this.txtSoLuong.Size = new System.Drawing.Size(204, 23);
            this.txtSoLuong.TabIndex = 40;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtMaLop
            // 
            this.txtMaLop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaLop.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtMaLop.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtMaLop.Location = new System.Drawing.Point(120, 11);
            this.txtMaLop.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtMaLop.Name = "txtMaLop";
            this.txtMaLop.ReadOnly = true;
            this.txtMaLop.Size = new System.Drawing.Size(204, 23);
            this.txtMaLop.TabIndex = 37;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel1.IsFocus = false;
            this.metroLabel1.Location = new System.Drawing.Point(20, 7);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(90, 32);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "Mã Lớp Học:";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel5.IsFocus = false;
            this.metroLabel5.Location = new System.Drawing.Point(20, 71);
            this.metroLabel5.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(90, 32);
            this.metroLabel5.TabIndex = 22;
            this.metroLabel5.Text = "Số Lượng:";
            this.metroLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtHocPhiInfClass
            // 
            this.txtHocPhiInfClass.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHocPhiInfClass.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtHocPhiInfClass.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtHocPhiInfClass.Location = new System.Drawing.Point(120, 107);
            this.txtHocPhiInfClass.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtHocPhiInfClass.Name = "txtHocPhiInfClass";
            this.txtHocPhiInfClass.ReadOnly = true;
            this.txtHocPhiInfClass.Size = new System.Drawing.Size(204, 23);
            this.txtHocPhiInfClass.TabIndex = 27;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel9.IsFocus = false;
            this.metroLabel9.Location = new System.Drawing.Point(23, 103);
            this.metroLabel9.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(87, 32);
            this.metroLabel9.TabIndex = 29;
            this.metroLabel9.Text = "Học Phí:";
            this.metroLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // cbbKhoaHoc
            // 
            this.cbbKhoaHoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbKhoaHoc.DisplayMember = "Value";
            this.cbbKhoaHoc.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbbKhoaHoc.FormattingEnabled = true;
            this.cbbKhoaHoc.ItemHeight = 19;
            this.cbbKhoaHoc.Location = new System.Drawing.Point(120, 42);
            this.cbbKhoaHoc.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.cbbKhoaHoc.Name = "cbbKhoaHoc";
            this.cbbKhoaHoc.Size = new System.Drawing.Size(204, 25);
            this.cbbKhoaHoc.TabIndex = 32;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.cbbKhoaHoc.UseSelectable = true;
            this.cbbKhoaHoc.ValueMember = "Key";
            // 
            // cboxConfirmHocVien
            // 
            this.cboxConfirmHocVien.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cboxConfirmHocVien.AutoSize = true;
            this.cboxConfirmHocVien.BackColor = System.Drawing.Color.Transparent;
            this.cboxConfirmHocVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.cboxConfirmHocVien.Location = new System.Drawing.Point(3, 419);
            this.cboxConfirmHocVien.Name = "cboxConfirmHocVien";
            this.cboxConfirmHocVien.Size = new System.Drawing.Size(176, 19);
            this.cboxConfirmHocVien.TabIndex = 28;
            this.cboxConfirmHocVien.Text = "Xác nhận thông tin học viên";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.cboxConfirmHocVien.UseVisualStyleBackColor = false;
            // 
            // tabLich
            // 
            this.tabLich.BackColor = System.Drawing.Color.Transparent;
            this.tabLich.Controls.Add(this.tableLayoutPanel11);
            this.tabLich.Location = new System.Drawing.Point(4, 38);
            this.tabLich.Name = "tabLich";
            this.tabLich.Size = new System.Drawing.Size(737, 445);
            this.tabLich.TabIndex = 2;
            this.tabLich.Text = "LỊCH HỌC";
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 3;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.95961F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.080784F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.95961F));
            this.tableLayoutPanel11.Controls.Add(this.cboxConfirmLichHoc, 0, 1);
            this.tableLayoutPanel11.Controls.Add(this.tableLayoutPanel12, 2, 0);
            this.tableLayoutPanel11.Controls.Add(this.tableLayoutPanel13, 0, 0);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 2;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(737, 445);
            this.tableLayoutPanel11.TabIndex = 27;
            // 
            // cboxConfirmLichHoc
            // 
            this.cboxConfirmLichHoc.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cboxConfirmLichHoc.AutoSize = true;
            this.cboxConfirmLichHoc.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel11.SetColumnSpan(this.cboxConfirmLichHoc, 3);
            this.cboxConfirmLichHoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.cboxConfirmLichHoc.Location = new System.Drawing.Point(3, 419);
            this.cboxConfirmLichHoc.Name = "cboxConfirmLichHoc";
            this.cboxConfirmLichHoc.Size = new System.Drawing.Size(173, 19);
            this.cboxConfirmLichHoc.TabIndex = 49;
            this.cboxConfirmLichHoc.Text = "Xác nhận thông tin lịch học";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.cboxConfirmLichHoc.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 1;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.Controls.Add(this.listDetailScheduleGroupBox, 0, 0);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(375, 0);
            this.tableLayoutPanel12.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 1;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(362, 413);
            this.tableLayoutPanel12.TabIndex = 47;
            // 
            // listDetailScheduleGroupBox
            // 
            this.listDetailScheduleGroupBox.BackColor = System.Drawing.Color.Transparent;
            this.listDetailScheduleGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listDetailScheduleGroupBox.Location = new System.Drawing.Point(3, 3);
            this.listDetailScheduleGroupBox.Name = "listDetailScheduleGroupBox";
            this.listDetailScheduleGroupBox.Size = new System.Drawing.Size(356, 407);
            this.listDetailScheduleGroupBox.TabIndex = 0;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 1;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.Controls.Add(this.settingsScheduleGroupBox, 0, 0);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel13.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 1;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(360, 413);
            this.tableLayoutPanel13.TabIndex = 48;
            // 
            // settingsScheduleGroupBox
            // 
            this.settingsScheduleGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.settingsScheduleGroupBox.Location = new System.Drawing.Point(3, 3);
            this.settingsScheduleGroupBox.Name = "settingsScheduleGroupBox";
            this.settingsScheduleGroupBox.Size = new System.Drawing.Size(354, 407);
            this.settingsScheduleGroupBox.TabIndex = 0;
            // 
            // tabMail
            // 
            this.tabMail.BackColor = System.Drawing.Color.Transparent;
            this.tabMail.Controls.Add(this.tableLayoutPanel7);
            this.tabMail.Location = new System.Drawing.Point(4, 38);
            this.tabMail.Name = "tabMail";
            this.tabMail.Size = new System.Drawing.Size(737, 445);
            this.tabMail.TabIndex = 3;
            this.tabMail.Text = "MAIL THÔNG BÁO";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Controls.Add(this.cboxConfirmMaiL, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.infoMail, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel7.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 2;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(737, 445);
            this.tableLayoutPanel7.TabIndex = 49;
            // 
            // cboxConfirmMaiL
            // 
            this.cboxConfirmMaiL.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cboxConfirmMaiL.AutoSize = true;
            this.cboxConfirmMaiL.BackColor = System.Drawing.Color.Transparent;
            this.cboxConfirmMaiL.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.cboxConfirmMaiL.Location = new System.Drawing.Point(3, 419);
            this.cboxConfirmMaiL.Name = "cboxConfirmMaiL";
            this.cboxConfirmMaiL.Size = new System.Drawing.Size(155, 19);
            this.cboxConfirmMaiL.TabIndex = 29;
            this.cboxConfirmMaiL.Text = "Xác nhận thông tin mail";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.cboxConfirmMaiL.UseVisualStyleBackColor = false;
            // 
            // infoMail
            // 
            this.infoMail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.infoMail.Location = new System.Drawing.Point(3, 3);
            this.infoMail.Name = "infoMail";
            this.infoMail.Size = new System.Drawing.Size(731, 407);
            this.infoMail.TabIndex = 0;
            // 
            // MetroCreateClass
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "MetroCreateClass";
            this.Size = new System.Drawing.Size(751, 529);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tabHocVien.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel16.ResumeLayout(false);
            this.tableLayoutPanel17.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.gbGiangVien.ResumeLayout(false);
            this.gbGiangVien.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.gboxLopHoc.ResumeLayout(false);
            this.gboxLopHoc.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.tabLich.ResumeLayout(false);
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tabMail.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private MetroButton btnHuy;
        private MetroButton btnTaoLop;
        private MetroTabControl tabControl;
        private System.Windows.Forms.TabPage tabHocVien;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        private GroupBox.MetroDetailScheduleStudentGroupBox studentDetailGroupBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;
        private GroupBox.MetroListStudentRegisterGroupBox listStudentRegisterGroupBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.GroupBox gbGiangVien;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private MetroTextbox txtMaGiangVien;
        private MetroTextbox txtTrinhDo;
        private MetroLabel metroLabel3;
        private MetroTextbox txtChuyenMon;
        private MetroLabel metroLabel2;
        private MetroLabel metroLabel4;
        private MetroLabel metroLabel10;
        private MetroCombobox cbbGiangVien;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.GroupBox gboxLopHoc;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private MetroLabel metroLabel7;
        private MetroTextbox txtSoLuong;
        private MetroTextbox txtMaLop;
        private MetroLabel metroLabel1;
        private MetroLabel metroLabel5;
        private MetroTextbox txtHocPhiInfClass;
        private MetroLabel metroLabel9;
        private MetroCombobox cbbKhoaHoc;
        private System.Windows.Forms.TabPage tabLich;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private GroupBox.MetroListScheduleGroupBox listDetailScheduleGroupBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private GroupBox.MetroSettingsScheduleGroupBox settingsScheduleGroupBox;
        private System.Windows.Forms.TabPage tabMail;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private MetroCheckbox cboxConfirmHocVien;
        private MetroCheckbox cboxConfirmLichHoc;
        private Mail.MetroInfoMail infoMail;
        private MetroCheckbox cboxConfirmMaiL;
    }
}
