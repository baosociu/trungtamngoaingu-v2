﻿namespace MetroUserControl.ContentMenu.Class
{
    partial class MetroListClass
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.btnXemChiTiet = new MetroControl.Components.MetroButton();
            this.btnTaoLop = new MetroControl.Components.MetroButton();
            this.metroLabel1 = new MetroControl.Components.MetroLabel();
            this.cbbLoc = new MetroControl.Components.MetroCombobox();
            this.metroLabel2 = new MetroControl.Components.MetroLabel();
            this.txtTimKiem = new MetroControl.Components.MetroTextbox();
            this.picSearch = new MetroControl.Components.MetroPicture();
            this.grdLop = new MetroControl.Controls.MetroGridViewPage();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.gửiMailChoLớpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnTaoKhoaHoc = new MetroControl.Components.MetroButton();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSearch)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.ColumnCount = 6;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 47.61905F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.761905F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 47.61905F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel1, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.cbbLoc, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel2, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtTimKiem, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.picSearch, 5, 0);
            this.tableLayoutPanel2.Controls.Add(this.grdLop, 0, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 6;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545455F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90.90909F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545455F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(660, 460);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 7;
            this.tableLayoutPanel2.SetColumnSpan(this.tableLayoutPanel1, 6);
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 367);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(660, 40);
            this.tableLayoutPanel1.TabIndex = 30;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 7;
            this.tableLayoutPanel2.SetColumnSpan(this.tableLayoutPanel4, 6);
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00062F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00062F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00062F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.99813F));
            this.tableLayoutPanel4.Controls.Add(this.btnXemChiTiet, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnTaoLop, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnTaoKhoaHoc, 5, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 423);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(660, 37);
            this.tableLayoutPanel4.TabIndex = 24;
            // 
            // btnXemChiTiet
            // 
            this.btnXemChiTiet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnXemChiTiet.Location = new System.Drawing.Point(93, 3);
            this.btnXemChiTiet.Name = "btnXemChiTiet";
            this.btnXemChiTiet.Size = new System.Drawing.Size(94, 31);
            this.btnXemChiTiet.TabIndex = 0;
            this.btnXemChiTiet.Text = "XEM CHI TIẾT";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnXemChiTiet.UseSelectable = true;
            // 
            // btnTaoLop
            // 
            this.btnTaoLop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTaoLop.Location = new System.Drawing.Point(283, 3);
            this.btnTaoLop.Name = "btnTaoLop";
            this.btnTaoLop.Size = new System.Drawing.Size(94, 31);
            this.btnTaoLop.TabIndex = 1;
            this.btnTaoLop.Text = "TẠO LỚP";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnTaoLop.UseSelectable = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel1.IsFocus = false;
            this.metroLabel1.Location = new System.Drawing.Point(3, 0);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(87, 32);
            this.metroLabel1.TabIndex = 25;
            this.metroLabel1.Text = "Lọc:";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // cbbLoc
            // 
            this.cbbLoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbLoc.DisplayMember = "Value";
            this.cbbLoc.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbbLoc.FormattingEnabled = true;
            this.cbbLoc.ItemHeight = 19;
            this.cbbLoc.Items.AddRange(new object[] {
            "Toàn Bộ",
            "Trình Độ"});
            this.cbbLoc.Location = new System.Drawing.Point(103, 3);
            this.cbbLoc.Name = "cbbLoc";
            this.cbbLoc.Size = new System.Drawing.Size(194, 25);
            this.cbbLoc.TabIndex = 0;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.cbbLoc.UseSelectable = true;
            this.cbbLoc.ValueMember = "Key";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel2.IsFocus = false;
            this.metroLabel2.Location = new System.Drawing.Point(323, 0);
            this.metroLabel2.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(87, 32);
            this.metroLabel2.TabIndex = 27;
            this.metroLabel2.Text = "Tìm Kiếm:";
            this.metroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtTimKiem
            // 
            this.txtTimKiem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTimKiem.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtTimKiem.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtTimKiem.Location = new System.Drawing.Point(420, 4);
            this.txtTimKiem.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtTimKiem.Name = "txtTimKiem";
            this.txtTimKiem.Size = new System.Drawing.Size(200, 23);
            this.txtTimKiem.TabIndex = 28;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // picSearch
            // 
            this.picSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picSearch.Image = global::MetroUserControl.Properties.Resources.magnifying_glass;
            this.picSearch.Location = new System.Drawing.Point(623, 3);
            this.picSearch.Name = "picSearch";
            this.picSearch.Size = new System.Drawing.Size(34, 26);
            this.picSearch.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picSearch.TabIndex = 29;
            this.picSearch.TabStop = false;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // grdLop
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.grdLop, 6);
            this.grdLop.ContextMenuStrip = this.contextMenuStrip1;
            this.grdLop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdLop.Location = new System.Drawing.Point(3, 51);
            this.grdLop.Name = "grdLop";
            this.grdLop.Size = new System.Drawing.Size(654, 313);
            this.grdLop.TabIndex = 31;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gửiMailChoLớpToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(162, 26);
            // 
            // gửiMailChoLớpToolStripMenuItem
            // 
            this.gửiMailChoLớpToolStripMenuItem.Name = "gửiMailChoLớpToolStripMenuItem";
            this.gửiMailChoLớpToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.gửiMailChoLớpToolStripMenuItem.Text = "Gửi mail cho lớp";
            // 
            // btnTaoKhoaHoc
            // 
            this.btnTaoKhoaHoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTaoKhoaHoc.Location = new System.Drawing.Point(473, 3);
            this.btnTaoKhoaHoc.Name = "btnTaoKhoaHoc";
            this.btnTaoKhoaHoc.Size = new System.Drawing.Size(94, 31);
            this.btnTaoKhoaHoc.TabIndex = 2;
            this.btnTaoKhoaHoc.Text = "TẠO KHÓA HỌC";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnTaoKhoaHoc.UseSelectable = true;
            // 
            // MetroListClass
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel2);
            this.Name = "MetroListClass";
            this.Size = new System.Drawing.Size(660, 460);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picSearch)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private MetroControl.Components.MetroButton btnXemChiTiet;
        private MetroControl.Components.MetroButton btnTaoLop;
        private MetroControl.Components.MetroLabel metroLabel1;
        private MetroControl.Components.MetroCombobox cbbLoc;
        private MetroControl.Components.MetroLabel metroLabel2;
        private MetroControl.Components.MetroTextbox txtTimKiem;
        private MetroControl.Components.MetroPicture picSearch;
        internal MetroControl.Controls.MetroGridViewPage grdLop;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem gửiMailChoLớpToolStripMenuItem;
        private MetroControl.Components.MetroButton btnTaoKhoaHoc;
    }
}
