﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroControl;
using EventHelper;
using MetroUserControl.ContentMenu.Mail;
using MetroDataset;

namespace MetroUserControl.ContentMenu.Class
{
    public partial class MetroListClass : MetroBubble
    {
        public MetroListClass()
        {
            InitializeComponent();
            this.Load += MetroListClass_Load;
            this.gửiMailChoLớpToolStripMenuItem.Click += (s, en) => MailByClass(grdLop.grid.IndexCurrentRow());
            this.VisibleChanged += MetroListClass_VisibleChanged;
        }

        private void MetroListClass_VisibleChanged(object sender, EventArgs e)
        {
            this.SettingLoadByPageFilterSearch(this.grdLop, (new LoadByPage()).HienThiLopHoc, this.cbbLoc, (new Select()).HienThiFilterLopHoc_Cbb, picSearch, txtTimKiem);

        }

        private void MailByClass(int v)
        {
            string id_class = grdLop.grid.Rows[v].Cells[0].Value.ToString();
            List<object> data = (new MetroDataset.LoadByPage()).HienThiMailLopHoc(id_class);
            MetroHelper.OpenNewTab(this, new MetroMail().SetDataClass(data, id_class));
        }

        private void MetroListClass_Load(object sender, EventArgs e)
        {
            EventHelper.Helper.SetDoubleClickItemGridView(this.grdLop.grid, this.btnXemChiTiet.PerformClick);
            //load page
            this.btnTaoLop.Click += (s, en) =>
            {
                MetroCreateClass c = new MetroCreateClass();
                MetroHelper.OpenNewTab(this, c, c.ClosingCreateClass);
            };
            this.btnTaoKhoaHoc.Click += (s, en) =>
            {
                 MetroCreateCourse k = new MetroCreateCourse();
                 MetroHelper.OpenNewTab(this,k,k.ClosingCreateCourse);
               
            };
            this.btnXemChiTiet.Click += (s, en) => MetroHelper.OpenNewTab(this, new MetroDetailClass(grdLop.grid.SelectedRows[0].Cells[0].Value.ToString()));

            this.btnTaoLop.TypeData.Add(MetroControl.Components.Type.Insert);
            this.VisibleByPermission();
        }


    }
}
