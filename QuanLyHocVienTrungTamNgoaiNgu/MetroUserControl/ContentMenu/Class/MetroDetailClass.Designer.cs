﻿using MetroControl.Components;

namespace MetroUserControl.ContentMenu.Class
{
    partial class MetroDetailClass
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.btnIn = new MetroControl.Components.MetroButton();
            this.btnHuy = new MetroControl.Components.MetroButton();
            this.btnTaoLop = new MetroControl.Components.MetroButton();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.gboxLopHoc = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.txtMaKhoaHoc = new MetroControl.Components.MetroTextbox();
            this.metroLabel7 = new MetroControl.Components.MetroLabel();
            this.txtSoLuong = new MetroControl.Components.MetroTextbox();
            this.txtMaLop = new MetroControl.Components.MetroTextbox();
            this.metroLabel11 = new MetroControl.Components.MetroLabel();
            this.metroLabel1 = new MetroControl.Components.MetroLabel();
            this.metroLabel5 = new MetroControl.Components.MetroLabel();
            this.txtHocPhiInfClass = new MetroControl.Components.MetroTextbox();
            this.metroLabel9 = new MetroControl.Components.MetroLabel();
            this.cbbKhoaHoc = new MetroControl.Components.MetroCombobox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.gbGiangVien = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.txtMaGiangVien = new MetroControl.Components.MetroTextbox();
            this.txtTrinhDo = new MetroControl.Components.MetroTextbox();
            this.metroLabel3 = new MetroControl.Components.MetroLabel();
            this.txtChuyenMon = new MetroControl.Components.MetroTextbox();
            this.metroLabel2 = new MetroControl.Components.MetroLabel();
            this.metroLabel4 = new MetroControl.Components.MetroLabel();
            this.metroLabel10 = new MetroControl.Components.MetroLabel();
            this.cbbGiangVien = new MetroControl.Components.MetroCombobox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.radioHocVien = new MetroControl.Components.MetroRadioButton();
            this.radioLichHoc = new MetroControl.Components.MetroRadioButton();
            this.grid = new MetroControl.Components.MetroGridView();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.gboxLopHoc.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.gbGiangVien.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(696, 393);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 7;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel4, 5);
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.Controls.Add(this.btnIn, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnHuy, 5, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnTaoLop, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 357);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(696, 36);
            this.tableLayoutPanel4.TabIndex = 24;
            // 
            // btnIn
            // 
            this.btnIn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnIn.Location = new System.Drawing.Point(301, 3);
            this.btnIn.Name = "btnIn";
            this.btnIn.Size = new System.Drawing.Size(94, 30);
            this.btnIn.TabIndex = 3;
            this.btnIn.Text = "IN DS HỌC VIÊN";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnIn.UseSelectable = true;
            // 
            // btnHuy
            // 
            this.btnHuy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnHuy.Location = new System.Drawing.Point(500, 3);
            this.btnHuy.Name = "btnHuy";
            this.btnHuy.Size = new System.Drawing.Size(94, 30);
            this.btnHuy.TabIndex = 2;
            this.btnHuy.Text = "HUỶ";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnHuy.UseSelectable = true;
            // 
            // btnTaoLop
            // 
            this.btnTaoLop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTaoLop.Location = new System.Drawing.Point(102, 3);
            this.btnTaoLop.Name = "btnTaoLop";
            this.btnTaoLop.Size = new System.Drawing.Size(94, 30);
            this.btnTaoLop.TabIndex = 1;
            this.btnTaoLop.Text = "TẠO LỚP";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnTaoLop.UseSelectable = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 47.05882F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.882353F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 47.05882F));
            this.tableLayoutPanel2.Controls.Add(this.gboxLopHoc, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel5, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.grid, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(690, 351);
            this.tableLayoutPanel2.TabIndex = 25;
            // 
            // gboxLopHoc
            // 
            this.gboxLopHoc.Controls.Add(this.tableLayoutPanel3);
            this.gboxLopHoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gboxLopHoc.Location = new System.Drawing.Point(3, 3);
            this.gboxLopHoc.Name = "gboxLopHoc";
            this.gboxLopHoc.Size = new System.Drawing.Size(318, 194);
            this.gboxLopHoc.TabIndex = 11;
            this.gboxLopHoc.TabStop = false;
            this.gboxLopHoc.Text = "Thông tin lớp học";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.AllowDrop = true;
            this.tableLayoutPanel3.AutoScroll = true;
            this.tableLayoutPanel3.AutoSize = true;
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.33334F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanel3.Controls.Add(this.txtMaKhoaHoc, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.metroLabel7, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.txtSoLuong, 2, 4);
            this.tableLayoutPanel3.Controls.Add(this.txtMaLop, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.metroLabel11, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.metroLabel1, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.metroLabel5, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.txtHocPhiInfClass, 2, 5);
            this.tableLayoutPanel3.Controls.Add(this.metroLabel9, 1, 5);
            this.tableLayoutPanel3.Controls.Add(this.cbbKhoaHoc, 2, 3);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 7;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(312, 175);
            this.tableLayoutPanel3.TabIndex = 10;
            // 
            // txtMaKhoaHoc
            // 
            this.txtMaKhoaHoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaKhoaHoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtMaKhoaHoc.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtMaKhoaHoc.Location = new System.Drawing.Point(117, 43);
            this.txtMaKhoaHoc.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtMaKhoaHoc.Name = "txtMaKhoaHoc";
            this.txtMaKhoaHoc.ReadOnly = true;
            this.txtMaKhoaHoc.Size = new System.Drawing.Size(176, 23);
            this.txtMaKhoaHoc.TabIndex = 42;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel7.Location = new System.Drawing.Point(17, 71);
            this.metroLabel7.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(90, 32);
            this.metroLabel7.TabIndex = 41;
            this.metroLabel7.Text = "Khoá Học:";
            this.metroLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtSoLuong
            // 
            this.txtSoLuong.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSoLuong.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtSoLuong.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtSoLuong.Location = new System.Drawing.Point(117, 107);
            this.txtSoLuong.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtSoLuong.Name = "txtSoLuong";
            this.txtSoLuong.ReadOnly = true;
            this.txtSoLuong.Size = new System.Drawing.Size(176, 23);
            this.txtSoLuong.TabIndex = 40;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtMaLop
            // 
            this.txtMaLop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaLop.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtMaLop.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtMaLop.Location = new System.Drawing.Point(117, 11);
            this.txtMaLop.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtMaLop.Name = "txtMaLop";
            this.txtMaLop.ReadOnly = true;
            this.txtMaLop.Size = new System.Drawing.Size(176, 23);
            this.txtMaLop.TabIndex = 37;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel11.Location = new System.Drawing.Point(17, 39);
            this.metroLabel11.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(90, 32);
            this.metroLabel11.TabIndex = 36;
            this.metroLabel11.Text = "Mã Khoá Học:";
            this.metroLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel1.Location = new System.Drawing.Point(17, 7);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(90, 32);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "Mã Lớp Học:";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel5.Location = new System.Drawing.Point(17, 103);
            this.metroLabel5.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(90, 32);
            this.metroLabel5.TabIndex = 22;
            this.metroLabel5.Text = "Số Lượng:";
            this.metroLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtHocPhiInfClass
            // 
            this.txtHocPhiInfClass.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHocPhiInfClass.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtHocPhiInfClass.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtHocPhiInfClass.Location = new System.Drawing.Point(117, 139);
            this.txtHocPhiInfClass.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtHocPhiInfClass.Name = "txtHocPhiInfClass";
            this.txtHocPhiInfClass.ReadOnly = true;
            this.txtHocPhiInfClass.Size = new System.Drawing.Size(176, 23);
            this.txtHocPhiInfClass.TabIndex = 27;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel9.Location = new System.Drawing.Point(20, 135);
            this.metroLabel9.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(87, 32);
            this.metroLabel9.TabIndex = 29;
            this.metroLabel9.Text = "Học Phí:";
            this.metroLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // cbbKhoaHoc
            // 
            this.cbbKhoaHoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbKhoaHoc.DisplayMember = "Value";
            this.cbbKhoaHoc.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbbKhoaHoc.FormattingEnabled = true;
            this.cbbKhoaHoc.ItemHeight = 19;
            this.cbbKhoaHoc.Location = new System.Drawing.Point(117, 74);
            this.cbbKhoaHoc.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.cbbKhoaHoc.Name = "cbbKhoaHoc";
            this.cbbKhoaHoc.Size = new System.Drawing.Size(176, 25);
            this.cbbKhoaHoc.TabIndex = 32;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.cbbKhoaHoc.UseSelectable = true;
            this.cbbKhoaHoc.ValueMember = "Key";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 4;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.33333F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel5.Controls.Add(this.gbGiangVien, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel7, 1, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(364, 0);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(326, 200);
            this.tableLayoutPanel5.TabIndex = 12;
            // 
            // gbGiangVien
            // 
            this.tableLayoutPanel5.SetColumnSpan(this.gbGiangVien, 4);
            this.gbGiangVien.Controls.Add(this.tableLayoutPanel6);
            this.gbGiangVien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbGiangVien.Location = new System.Drawing.Point(3, 3);
            this.gbGiangVien.Name = "gbGiangVien";
            this.gbGiangVien.Size = new System.Drawing.Size(320, 162);
            this.gbGiangVien.TabIndex = 13;
            this.gbGiangVien.TabStop = false;
            this.gbGiangVien.Text = "Thông tin giảng viên";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.AllowDrop = true;
            this.tableLayoutPanel6.AutoScroll = true;
            this.tableLayoutPanel6.AutoSize = true;
            this.tableLayoutPanel6.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel6.ColumnCount = 4;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.33333F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel6.Controls.Add(this.txtMaGiangVien, 2, 1);
            this.tableLayoutPanel6.Controls.Add(this.txtTrinhDo, 2, 3);
            this.tableLayoutPanel6.Controls.Add(this.metroLabel3, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.txtChuyenMon, 2, 4);
            this.tableLayoutPanel6.Controls.Add(this.metroLabel2, 1, 4);
            this.tableLayoutPanel6.Controls.Add(this.metroLabel4, 1, 2);
            this.tableLayoutPanel6.Controls.Add(this.metroLabel10, 1, 3);
            this.tableLayoutPanel6.Controls.Add(this.cbbGiangVien, 2, 2);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 6;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(314, 143);
            this.tableLayoutPanel6.TabIndex = 10;
            // 
            // txtMaGiangVien
            // 
            this.txtMaGiangVien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaGiangVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtMaGiangVien.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtMaGiangVien.Location = new System.Drawing.Point(127, 11);
            this.txtMaGiangVien.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtMaGiangVien.Name = "txtMaGiangVien";
            this.txtMaGiangVien.ReadOnly = true;
            this.txtMaGiangVien.Size = new System.Drawing.Size(170, 23);
            this.txtMaGiangVien.TabIndex = 41;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtTrinhDo
            // 
            this.txtTrinhDo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTrinhDo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtTrinhDo.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtTrinhDo.Location = new System.Drawing.Point(127, 75);
            this.txtTrinhDo.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtTrinhDo.Name = "txtTrinhDo";
            this.txtTrinhDo.ReadOnly = true;
            this.txtTrinhDo.Size = new System.Drawing.Size(170, 23);
            this.txtTrinhDo.TabIndex = 40;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel3.Location = new System.Drawing.Point(20, 7);
            this.metroLabel3.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(97, 32);
            this.metroLabel3.TabIndex = 39;
            this.metroLabel3.Text = "Mã Giảng Viên:";
            this.metroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtChuyenMon
            // 
            this.txtChuyenMon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtChuyenMon.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtChuyenMon.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtChuyenMon.Location = new System.Drawing.Point(127, 107);
            this.txtChuyenMon.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtChuyenMon.Name = "txtChuyenMon";
            this.txtChuyenMon.ReadOnly = true;
            this.txtChuyenMon.Size = new System.Drawing.Size(170, 23);
            this.txtChuyenMon.TabIndex = 38;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel2.Location = new System.Drawing.Point(20, 103);
            this.metroLabel2.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(97, 32);
            this.metroLabel2.TabIndex = 37;
            this.metroLabel2.Text = "Chuyên Môn:";
            this.metroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel4.Location = new System.Drawing.Point(17, 39);
            this.metroLabel4.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(100, 32);
            this.metroLabel4.TabIndex = 2;
            this.metroLabel4.Text = "Giảng Viên:";
            this.metroLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel10.Location = new System.Drawing.Point(20, 71);
            this.metroLabel10.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(97, 32);
            this.metroLabel10.TabIndex = 29;
            this.metroLabel10.Text = "Trình Độ:";
            this.metroLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // cbbGiangVien
            // 
            this.cbbGiangVien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbGiangVien.DisplayMember = "Value";
            this.cbbGiangVien.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbbGiangVien.FormattingEnabled = true;
            this.cbbGiangVien.ItemHeight = 19;
            this.cbbGiangVien.Location = new System.Drawing.Point(127, 42);
            this.cbbGiangVien.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.cbbGiangVien.Name = "cbbGiangVien";
            this.cbbGiangVien.Size = new System.Drawing.Size(170, 25);
            this.cbbGiangVien.TabIndex = 32;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.cbbGiangVien.UseSelectable = true;
            this.cbbGiangVien.ValueMember = "Key";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel5.SetColumnSpan(this.tableLayoutPanel7, 2);
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.Controls.Add(this.radioHocVien, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.radioLichHoc, 1, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(18, 168);
            this.tableLayoutPanel7.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(288, 32);
            this.tableLayoutPanel7.TabIndex = 40;
            // 
            // radioHocVien
            // 
            this.radioHocVien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.radioHocVien.AutoSize = true;
            this.radioHocVien.Checked = true;
            this.radioHocVien.Location = new System.Drawing.Point(38, 3);
            this.radioHocVien.Name = "radioHocVien";
            this.radioHocVien.Size = new System.Drawing.Size(68, 26);
            this.radioHocVien.TabIndex = 2;
            this.radioHocVien.TabStop = true;
            this.radioHocVien.Text = "Học viên";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.radioHocVien.UseVisualStyleBackColor = true;
            // 
            // radioLichHoc
            // 
            this.radioLichHoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.radioLichHoc.AutoSize = true;
            this.radioLichHoc.Location = new System.Drawing.Point(183, 3);
            this.radioLichHoc.Name = "radioLichHoc";
            this.radioLichHoc.Size = new System.Drawing.Size(66, 26);
            this.radioLichHoc.TabIndex = 0;
            this.radioLichHoc.Text = "Lịch học";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.radioLichHoc.UseVisualStyleBackColor = true;
            // 
            // grid
            // 
            this.grid.AllowUserToAddRows = false;
            this.grid.AllowUserToDeleteRows = false;
            this.grid.AllowUserToResizeRows = false;
            this.grid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.grid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.grid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableLayoutPanel2.SetColumnSpan(this.grid, 3);
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grid.DefaultCellStyle = dataGridViewCellStyle2;
            this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid.EnableHeadersVisualStyles = false;
            this.grid.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.grid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.grid.Location = new System.Drawing.Point(3, 203);
            this.grid.MultiSelect = false;
            this.grid.Name = "grid";
            this.grid.ReadOnly = true;
            this.grid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grid.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.grid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grid.Size = new System.Drawing.Size(684, 145);
            this.grid.TabIndex = 13;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // MetroDetailClass
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "MetroDetailClass";
            this.Size = new System.Drawing.Size(696, 393);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.gboxLopHoc.ResumeLayout(false);
            this.gboxLopHoc.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.gbGiangVien.ResumeLayout(false);
            this.gbGiangVien.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private MetroButton btnHuy;
        private MetroButton btnTaoLop;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.GroupBox gboxLopHoc;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private MetroLabel metroLabel1;
        private MetroLabel metroLabel5;
        private MetroTextbox txtHocPhiInfClass;
        private MetroLabel metroLabel9;
        private MetroCombobox cbbKhoaHoc;
        private MetroLabel metroLabel11;
        private MetroTextbox txtMaLop;
        private MetroTextbox txtSoLuong;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.GroupBox gbGiangVien;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private MetroTextbox txtChuyenMon;
        private MetroLabel metroLabel2;
        private MetroLabel metroLabel4;
        private MetroLabel metroLabel10;
        private MetroCombobox cbbGiangVien;
        private MetroGridView grid;
        private MetroLabel metroLabel3;
        private MetroTextbox txtMaKhoaHoc;
        private MetroLabel metroLabel7;
        private MetroTextbox txtMaGiangVien;
        private MetroTextbox txtTrinhDo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private MetroRadioButton radioLichHoc;
        private MetroRadioButton radioHocVien;
        private MetroButton btnIn;
    }
}
