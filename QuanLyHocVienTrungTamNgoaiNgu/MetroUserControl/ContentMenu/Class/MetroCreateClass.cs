﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroControl;
using static MetroUserControl.DataSetQuanLyHocVien;
using MetroDataset;
using MetroDataset.Model;
using MetroUserControl.GroupBox;

namespace MetroUserControl.ContentMenu.Class
{
    public partial class MetroCreateClass : MetroBubble
    {
        List<object> data_student; //dữ liệu tạm, nahan viên có thể xoá, sửa ...
        bool isReloadTabLich = true;
        bool isReloadTabMail = true;

        bool focusLose = false;
        //group box

        public MetroCreateClass()
        {
            InitializeComponent();
            this.tabControl.SelectedIndexChanged += TabControl_SelectedIndexChanged;
            this.tabControl.SelectedTab = this.tabHocVien;
            this.btnTaoLop.Click += BtnTaoLop_Click;
            this.btnHuy.Click += (s, en) => { this.ParentForm.Close(); };

            FirstLoad();
        }

        private void BtnTaoLop_Click(object sender, EventArgs e)
        {
            if (cboxConfirmHocVien.Checked)
            {
                if (cboxConfirmLichHoc.Checked)
                {
                    if (cboxConfirmMaiL.Checked)
                    {
                        ThoiKhoaBieu tkb = settingsScheduleGroupBox.infoScheduleGroupBox.GetCurrentSchedule();
                        PhongHoc p = settingsScheduleGroupBox.infoScheduleGroupBox.GetCurrentRoom();
                        //điều kiện ràng buộc khác
                        if (data_student.Count <= p.Soluongchongoi)
                        {
                            //tạo lớp 
                            EventHelper.Helper.MessageInfomationYesNo("Đồng ý tạo lớp mới ?", () =>
                            {
                                LOPHOC lophoc = new LOPHOC()
                                {
                                    MAKHOAHOC = cbbKhoaHoc.SelectedValue.ToString(),
                                    MAGIANGVIEN = cbbGiangVien.SelectedValue.ToString(),
                                    SOLUONG = data_student.Count
                                };
                                ResultExcuteQuery r = (new Insert()).ThemLopHoc(ref lophoc);
                                EventHelper.Helper.MessageInfomation(r.GetMessage());

                                if (r.isSuccessfully())
                                {
                                    //ĐGhi danh các hoc viên vào lớp mới
                                    List<string> madangky = new List<string>();
                                    foreach (DataGridViewRow row in listStudentRegisterGroupBox.grid.Rows)
                                        madangky.Add(row.Cells[0].Value.ToString());

                                    ResultExcuteQuery s = (new Insert()).ThemHocVienLopHoc(madangky, lophoc.MALOPHOC);
                                    EventHelper.Helper.MessageInfomation(s.GetMessage());

                                    //cập nhật mã lớp vào biên lai
                                    ResultExcuteQuery t = (new Update()).CapNhatMaLopBienLai(madangky, lophoc.MALOPHOC);
                                    //EventHelper.Helper.MessageInfomation(t.GetMessage());

                                    if (s.isSuccessfully() && t.isSuccessfully())
                                    {
                                        //tạo lịch
                                        ResultExcuteQuery q = (new Insert()).ThemLichHocLopHoc(tkb, p, lophoc.MALOPHOC);
                                        EventHelper.Helper.MessageInfomation(q.GetMessage());
                                        if (q.isSuccessfully())
                                        {
                                            focusLose = true;
                                            btnHuy.PerformClick();
                                        }
                                        //gửi mail
                                        EventHelper.Helper.TryCatch(infoMail.SentMail);
                                        
                                    }
                                }
                            });
                        }
                        else
                            EventHelper.Helper.MessageInfomation("Phòng học \'" + p.Macoso + "\' tại cơ sở \'" + p.Tencoso + "\' không đủ số chỗ ngồi cho học viên.");
                    }
                    else
                        EventHelper.Helper.MessageInfomation("Vui lòng xác nhận thông tin mail thông báo");

                }
                else
                    EventHelper.Helper.MessageInfomation("Vui lòng xác nhận thông tin lịch học");

            }
            else
                EventHelper.Helper.MessageInfomation("Vui lòng xác nhận thông tin học viên");

        }

        public void ClosingCreateClass(object sender, FormClosingEventArgs e)
        {
            if (focusLose)
                e.Cancel = false;
            else
                EventHelper.Helper.MessageInfomationYesNo("Đóng cửa sổ tạo lớp học ?", () => e.Cancel = false, () => e.Cancel = true);
        }

        private void TabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.tabControl.SelectedTab.Equals(this.tabLich) && isReloadTabLich)
            {
                LoadDefaultSchedule();
                settingsScheduleGroupBox.btnTaoLich.PerformClick();
                isReloadTabLich = false;
            }
            else if (this.tabControl.SelectedTab.Equals(this.tabMail) && isReloadTabMail)
            {
                LoadMail();
                isReloadTabMail = false;
            }
        }

        private void LoadMail()
        {
            List<string> ds_madangky = GetDSMaDangKy();
            if (cbbKhoaHoc.SelectedValue == null)
                EventHelper.Helper.MessageInfomation("Vui lòng chọn khoá học đển tiến hành gửi mail");
            else
                if (cbbGiangVien.SelectedValue == null)
                EventHelper.Helper.MessageInfomation("Vui lòng chọn giảng viên cho khoá học này");
            else
            if (ds_madangky.Count == 0)
                EventHelper.Helper.MessageInfomation("Không tồn tại thông tin học viên đăng ký khoá học này.");
            else
                infoMail.SetDataInfoMail(ds_madangky, cbbGiangVien.SelectedValue.ToString(), cbbKhoaHoc.SelectedValue.ToString());
        }

        private void LoadTabHocVien()
        {
            LoadCourse();
            LoadTeacher();
        }


        private void FirstLoad()
        {
            //load cbb KhoaHoc
            List<object> data = (new MetroDataset.Select()).HienThiTatCaKhoaHoc_Cbb();
            if (data != null && data.Count != 0)
            {
                cbbKhoaHoc.DataSource = data;
                cbbKhoaHoc.SelectedIndex = 0;
                LoadCourse();

            }
            //load cbb GiangVien
            List<object> data2 = (new MetroDataset.Select()).HienThiTatCaGiangVien_Cbb();
            if (data2 != null && data2.Count != 0)
            {
                cbbGiangVien.DataSource = data2;
                cbbGiangVien.SelectedIndex = 0;
                LoadTeacher();
            }

            cbbKhoaHoc.SelectedIndexChanged += (s, en) =>
            {
                LoadCourse();
            };
            cbbGiangVien.SelectedIndexChanged += (s, en) =>
            {
                LoadTeacher();
                isReloadTabLich = true;
                isReloadTabMail = true;
                settingsScheduleGroupBox.infoScheduleGroupBox.ClearData();
                listDetailScheduleGroupBox.ClearData();
            };


            settingsScheduleGroupBox.infoScheduleGroupBox.SetValueScheduleChanged(() =>
            {
                ThoiKhoaBieu t = settingsScheduleGroupBox.infoScheduleGroupBox.GetCurrentSchedule();
                listDetailScheduleGroupBox.LoadDetailListSchedule(t);
            });


            //event danh sashc học viên dược chọn
            listStudentRegisterGroupBox.grid.DataSourceChanged += Grid_DataSourceChanged;
            listStudentRegisterGroupBox.btnXoa.Click += (s, en) => XoaHocVien();
            listStudentRegisterGroupBox.grid.SelectionChanged += Grid_SelectionChanged;
            listStudentRegisterGroupBox.btnRefresh.Click += (s, en) =>
            {
                data_student = null;
                LoadStudentsByCourse(cbbKhoaHoc.SelectedValue.ToString());
            };

            //event cài đặt lịch học
            settingsScheduleGroupBox.btnTaoLich.Click += (s, en) => CreateSchedule();
            settingsScheduleGroupBox.btnLoadDefault.Click += (s, en) => clickLoadDefaultSchedule();

            //cbox khoá
            cboxConfirmHocVien.CheckedChanged += CboxConfirmHocVien_CheckedChanged; //xác nhận học viên
            cboxConfirmLichHoc.CheckedChanged += CboxConfirmLichHoc_CheckedChanged; // xác nhận lịch học
            cboxConfirmMaiL.CheckedChanged += CboxConfirmMaiL_CheckedChanged; // xác nhận mail

            //thay đổi nội dung ==> checkbox.check = false (cho nhân viên xác nhận lại)
            infoMail.txtMailGui.TextChanged += (s, e) => cboxConfirmMaiL.Checked = false;
            infoMail.txtMatKhau.TextChanged += (s, e) => cboxConfirmMaiL.Checked = false;
            infoMail.richTextBox1.TextChanged += (s, e) => cboxConfirmMaiL.Checked = false;
            infoMail.txtCountMail.TextChanged += (s, e) => cboxConfirmMaiL.Checked = false;

            listDetailScheduleGroupBox.grid.DataSourceChanged += (s, e) => cboxConfirmLichHoc.Checked = false;
            settingsScheduleGroupBox.infoScheduleGroupBox.cbbPhongHoc.SelectedValueChanged += (s, e) => cboxConfirmLichHoc.Checked = false;

            cbbKhoaHoc.SelectedValueChanged += (s, e) =>
              {
                  cboxConfirmHocVien.Checked = false;
                  cboxConfirmMaiL.Checked = false;
                  cboxConfirmLichHoc.Checked = false;
              };

            cbbGiangVien.SelectedValueChanged += (s, e) =>
            {
                cboxConfirmHocVien.Checked = false;
                cboxConfirmMaiL.Checked = false;
                cboxConfirmLichHoc.Checked = false;
            };

            listStudentRegisterGroupBox.grid.DataSourceChanged += (s, e) =>
            {
                cboxConfirmHocVien.Checked = false;
                cboxConfirmMaiL.Checked = false;
                cboxConfirmLichHoc.Checked = false;
            };
        }

        private void CboxConfirmMaiL_CheckedChanged(object sender, EventArgs e)
        {
            bool isChecked = (sender as CheckBox).Checked;
            if (isChecked)
            {
                //kiểm tra mail gửi
                bool sent = String.IsNullOrWhiteSpace(infoMail.txtMailGui.Text) || String.IsNullOrEmpty(infoMail.txtMailGui.Text);
                if (sent)
                {
                    EventHelper.Helper.MessageInfomation("Vui lòng cung cấp thông tin mail gửi");
                    (sender as CheckBox).Checked = false;
                    return;
                }
                //kiểm tra mật khẩu
                bool pass = String.IsNullOrWhiteSpace(infoMail.txtMatKhau.Text) || String.IsNullOrEmpty(infoMail.txtMatKhau.Text);
                if (pass)
                {
                    EventHelper.Helper.MessageInfomation("Vui lòng cung cấp thông tin mật khẩu của mail gửi");
                    (sender as CheckBox).Checked = false;
                    return;
                }
                //kiểm tra content
                bool content = String.IsNullOrWhiteSpace(infoMail.richTextBox1.Text) || String.IsNullOrEmpty(infoMail.richTextBox1.Text);
                if (content)
                {
                    EventHelper.Helper.MessageInfomation("Vui lòng cung cấp nội dung mail.");
                    (sender as CheckBox).Checked = false;
                    return;
                }
                //kiểm danh sách người nhận -- số lượng
                bool count = String.IsNullOrWhiteSpace(infoMail.txtCountMail.Text) || String.IsNullOrEmpty(infoMail.txtCountMail.Text);
                if (count)
                {
                    EventHelper.Helper.MessageInfomation("Vui lòng cung cấp danh sách người nhận");
                    (sender as CheckBox).Checked = false;
                    return;
                }

            }
        }

        private void CboxConfirmLichHoc_CheckedChanged(object sender, EventArgs e)
        {
            bool isChecked = (sender as CheckBox).Checked;
            if (isChecked)
            {
                if (isChecked)
                {
                    //kiểm tra grid detail lịch học
                    bool schedule = settingsScheduleGroupBox.infoScheduleGroupBox.GetCurrentSchedule() == null;
                    if (schedule)
                    {
                        EventHelper.Helper.MessageInfomation("Vui lòng cung cấp thông tin lịch học");
                        (sender as CheckBox).Checked = false;
                        return;
                    }
                    //kiểm tra phòng
                    bool room = settingsScheduleGroupBox.infoScheduleGroupBox.GetCurrentRoom() == null;
                    if (room)
                    {
                        EventHelper.Helper.MessageInfomation("Vui lòng cung cấp thông tin phòng học");
                        (sender as CheckBox).Checked = false;
                        return;
                    }
                }
            }
        }

        private void CboxConfirmHocVien_CheckedChanged(object sender, EventArgs e)
        {
            bool isChecked = (sender as CheckBox).Checked;
            if (isChecked)
            {
                //kiểm tra khoá học
                bool course = cbbKhoaHoc.SelectedValue == null;
                if (course)
                {
                    EventHelper.Helper.MessageInfomation("Vui lòng cung thông tin khoá học cho lớp học");
                    (sender as CheckBox).Checked = false;
                    return;
                }
                //kiểm tra giảng viên
                bool teacher = cbbGiangVien.SelectedValue == null;
                if (teacher)
                {
                    EventHelper.Helper.MessageInfomation("Vui lòng cung cấp thông tin giảng viên cho lớp học");
                    (sender as CheckBox).Checked = false;
                    return;
                }
                //kiểm tra học viên
                bool student = data_student == null || listStudentRegisterGroupBox.grid.RowCount == 0;
                if (student)
                {
                    EventHelper.Helper.MessageInfomation("Vui lòng cung cấp thông tin học viên cho lớp học.");
                    (sender as CheckBox).Checked = false;
                    return;
                }


            }
        }

        private void Grid_DataSourceChanged(object sender, EventArgs e)
        {
            isReloadTabLich = true;
            isReloadTabMail = true;

            settingsScheduleGroupBox.infoScheduleGroupBox.ClearData();
            listDetailScheduleGroupBox.ClearData();
        }

        private void CreateSchedule()
        {
            //tạo lịch từ grid đã chọn
            Func<List<Lich>> f = new Func<List<Lich>>(() => { return this.settingsScheduleGroupBox.GetListScheduleGrid(); });
            object a = EventHelper.Helper.TryCatchReturnValue(f);
            if (a != null)
            {
                List<Lich> data = (List<Lich>)a;
                if (data.Count != 0)
                {
                    settingsScheduleGroupBox.infoScheduleGroupBox.CreateSchedule(data,txtMaGiangVien.Text);
                    return;
                }
            }
            EventHelper.Helper.MessageInfomation("Vui lòng chọn thời gian để xếp lịch học");

        }

        private void XoaHocVien()
        {
            int index = listStudentRegisterGroupBox.grid.IndexCurrentRow();
            bool r = false;
            if (index != -1)
                r = EventHelper.Helper.TryCatch(() =>
                {
                    data_student.RemoveAt(index);
                    listStudentRegisterGroupBox.grid.DataSource = null;
                    listStudentRegisterGroupBox.grid.DataSource = data_student;
                });
            EventHelper.Helper.MessageInfomation(r ? "Đã xoá học viên này ra khỏi danh sách" : "Không thể thực hiện thao tác");
            if (r)
                LoadStudentsByCourse(cbbKhoaHoc.SelectedValue.ToString());
        }


        private void clickLoadDefaultSchedule()
        {
            EventHelper.Helper.MessageInfomationYesNo("Tải dữ liệu giờ học mặc định để tiếp tục xếp lớp ?", LoadDefaultSchedule);
        }

        private void LoadDefaultSchedule()
        {
            //1. Tạo lich theo giờ học viên đăng ký
            List<object> time_students = new List<object>();

            //lấy ds giờ trống của học viên đã chọn
            string khoahoc = cbbKhoaHoc.SelectedValue.ToString();
            //lấy mã học viên
            string magiangvien = cbbGiangVien.SelectedValue.ToString();
            List<string> ds_madangky = GetDSMaDangKy();
            if (ds_madangky.Count == 0)
                return;
            List<Lich> lich_dangky = (new Select()).LayLichTrongTKBHocVienDangKy(ds_madangky, magiangvien);

            //load lại grid chọn ca-thứ
            settingsScheduleGroupBox.SetDataSourceGrid(lich_dangky);

            //2. Tạo lịch theo giờ trống của học viên
        }

        private List<string> GetDSMaDangKy()
        {
            List<string> ds_madangky = new List<string>();
            foreach (DataGridViewRow row in listStudentRegisterGroupBox.grid.Rows)
            {
                string madangky = row.Cells[0].Value.ToString();
                ds_madangky.Add(madangky);
            }
            return ds_madangky;
        }

        private void LoadCourse()
        {
            string makhoahoc = cbbKhoaHoc.SelectedValue.ToString();
            KHOAHOC gv = (new Select()).GetCourseByID(makhoahoc);
            txtHocPhiInfClass.Text = gv.HOCPHI.ToString();
            string makhoa = gv.MAKHOAHOC.ToString();
            CreateClassIDAuto(makhoa);
            if (cbbKhoaHoc.SelectedValue != null)
            {
                data_student = null;
                LoadStudentsByCourse(cbbKhoaHoc.SelectedValue.ToString());
            }
        }

        private void LoadStudentsByCourse(string makhoa)
        {
            if (data_student != null)
            {
                listStudentRegisterGroupBox.grid.DataSource = data_student;
                //lưu lại thao tác thêm xoá học viên
            }
            else
            {
                //danh sách học viên dăng ký khoá học
                var c = (new Select()).HienThiHocVienDangKyKhoaHoc(makhoa);
                listStudentRegisterGroupBox.grid.DataSource = c;
                data_student = c;
            }
            txtSoLuong.Text = listStudentRegisterGroupBox.grid.RowCount.ToString();
            if (listStudentRegisterGroupBox.grid.Rows.Count != 0)
            {
                if (listStudentRegisterGroupBox.grid.CurrentRow == null)
                    listStudentRegisterGroupBox.grid.SetCurrentRow(0);
                LoadRightGridView(listStudentRegisterGroupBox.grid.CurrentRow.Cells[0].Value.ToString());
            }
            else
            {
                studentDetailGroupBox.grid.DataSource = null;
                EventHelper.Helper.MessageInfomation("Không tồn tại dữ liệu học viên đăng ký khoá học \'" + makhoa + "\'");
            }
            //(new Select()).HienThiHocVienKhoaHoc(makhoa); //xét trường hợp học viên đăng ký 2 khoá ==> 1 trường đã xếp lớp
        }

        private void LoadTeacher()
        {
            string magiangvien = cbbGiangVien.SelectedValue.ToString();
            GIANGVIEN gv = (new Select()).GetTeacherByID(magiangvien);
            txtTrinhDo.Text = gv.TRINHDO.ToString();
            txtChuyenMon.Text = gv.CHUYENMON.ToString();
            txtMaGiangVien.Text = gv.MAGIANGVIEN.ToString();
        }

        private void CreateClassIDAuto(string makhoa)
        {
            List<object> data = (new Select()).TaoMaLopTuKhoaHoc(makhoa);
            if (data != null && data.Count != 0)
            {
                string[] str = Helper.GetValuesFromAnonymous(data[0]);
                txtMaLop.Text = str[0];
            }
        }

        private void Grid_SelectionChanged(object sender, EventArgs e)
        {
            //load thông tin bên trái 
            EventHelper.Helper.TryCatch(() =>
            {
                string madangky = listStudentRegisterGroupBox.grid.CurrentRow.Cells[0].Value.ToString();
                LoadRightGridView(madangky);
            });
        }

        private void LoadRightGridView(String madangky)
        {
            using (DataSetQuanLyHocVienTableAdapters.HienThiTTChonGioHocHocVienTableAdapter dt = new DataSetQuanLyHocVienTableAdapters.HienThiTTChonGioHocHocVienTableAdapter())
            {
                DataTable tb = dt.GetData(madangky);
                foreach (System.Data.DataColumn col in tb.Columns)
                    col.ReadOnly = false;
                studentDetailGroupBox.grid.DataSource = tb;
            }
            EventHelper.Helper.TryCatch(() =>
            {
                studentDetailGroupBox.grid.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                studentDetailGroupBox.grid.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                studentDetailGroupBox.grid.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            });
        }


    }
}
