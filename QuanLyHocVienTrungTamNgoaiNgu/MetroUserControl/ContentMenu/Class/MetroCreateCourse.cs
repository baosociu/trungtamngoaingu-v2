﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroDataset;
using MetroControl;

namespace MetroUserControl.ContentMenu.Class
{
    public partial class MetroCreateCourse : MetroBubble
    {
        KHOAHOC kh;
        bool focusLose = false;
        public MetroCreateCourse()
        {
            InitializeComponent();

            btnHuy.Click += (s, en) => { (this.ParentForm).Close(); };
            btnXoa.Click += btnXoa_Click;
            picCreate.Click += PicCreate_Click;

            this.rdbtnThemKhoaHoc.Checked = true;
            rdbtnThemKhoaHoc.CheckedChanged += (s, en) => settingLayoutForThem();
            rdbtnCapNhatKhoaHoc.CheckedChanged += (s, en) => settingLayoutForCapNhat();
            grdCourse.grid.SelectionChanged += Grid_SelectionChanged;
            this.VisibleChanged += MetroCreateCourse_VisibleChanged;

            settingLayoutForThem();

        }

        private void MetroCreateCourse_VisibleChanged(object sender, EventArgs e)
        {
            this.SettingLoadByPageFilterSearch(this.grdCourse, (new LoadByPage()).HienThiKhoaHoc_Grid, this.cbbLoc, (new Select()).HienThiFilterTaoKhoaHoc_Cbb, picSearch, txtTimKiem);

        }

        private void settingLayoutForCapNhat()
        {
            EventHelper.Helper.TryCatch(() =>
            {
                int index = grdCourse.grid.IndexCurrentRow();
                txtMaKhoaHoc.Text = grdCourse.grid.Rows[index].Cells[0].Value.ToString();
                txtTenKhoaHoc.Text = grdCourse.grid.Rows[index].Cells[1].Value.ToString();
                txtHocPhiKhoaHoc.Text = grdCourse.grid.Rows[index].Cells[2].Value.ToString();
            });
            picCreate.Image = Properties.Resources.refresh_page_option;

        }

        private void PicCreate_Click(object sender, EventArgs e)
        {
            if (rdbtnThemKhoaHoc.Checked)
                ThemKhocHoc();
            else
                CapNhatKhoaHoc();
        }

        public void settingLayoutForThem()
        {
            txtMaKhoaHoc.Text = (new Select()).TaoMaKhoaHocTuDong();
            txtTenKhoaHoc.Text = "";
            txtTenKhoaHoc.ReadOnly = false;
            txtHocPhiKhoaHoc.Text = "0";
            picCreate.Image = Properties.Resources.group_profile_users_create;

        }

        private void CapNhatKhoaHoc()
        {

            //kiểm tra rỗng
            if (MetroHelper.IsNullOrEmpty(new object[] { txtMaKhoaHoc.Text, txtTenKhoaHoc.Text, txtHocPhiKhoaHoc.Text }))
            {
                EventHelper.Helper.MessageLessInformation();
                return;
            }

            string makhoahoc = txtMaKhoaHoc.Text;
            string tenkhoahoc = txtTenKhoaHoc.Text;
            float hocphikhoahoc = float.Parse(txtHocPhiKhoaHoc.Text);
            //kiểm tra thay đổi
            kh = (new Select()).GetCourseByID(makhoahoc);
            if (hocphikhoahoc == kh.HOCPHI && tenkhoahoc == kh.TENKHOAHOC)
            {
                EventHelper.Helper.MessageNoChangedInformation();
                return;
            }
            //Kiem Tra hoc phi
            if (hocphikhoahoc <= 0)
            {
                EventHelper.Helper.MessageInfomation("Tiền học phí phải lớn hơn 0 đồng!");
                return;
            }
            //kiểm tra khóa học có tồn tại lớp học đang học
            if (kh.LOPHOCs.Count > 0)
            {
                EventHelper.Helper.MessageInfomation("Không thể sửa thông tin của khóa học " + makhoahoc + " vì khóa học này tồn tại lớp đang mở !");
                return;
            }
            //cập nhật học phí
            KHOAHOC khoahoc = new KHOAHOC();
            khoahoc.MAKHOAHOC = kh.MAKHOAHOC;
            khoahoc.TENKHOAHOC = kh.TENKHOAHOC;

            khoahoc.HOCPHI = hocphikhoahoc;
            khoahoc.TENKHOAHOC = tenkhoahoc;


            ResultExcuteQuery result = (new Update()).CapNhatKhoaHoc(khoahoc);
            EventHelper.Helper.MessageInfomation(result.GetMessage());
            if (result.isSuccessfully())
            {
                kh = khoahoc;
                loadDataGridView();


            }


        }

        private void loadDataGridView()
        {
            EventHelper.Helper.TryCatch(() =>
            {
                int indexold = this.grdCourse.grid.IndexCurrentRow();
                this.ReloadData(this.grdCourse.grid, true);
                this.grdCourse.grid.SetCurrentRow(indexold);
                if (rdbtnCapNhatKhoaHoc.Checked)
                    settingLayoutForCapNhat();

            });

        }

        private void ThemKhocHoc()
        {
            string makhoahoc = txtMaKhoaHoc.Text;
            string tenkhoahoc = txtTenKhoaHoc.Text;
            float hocphikhoahoc = float.Parse(txtHocPhiKhoaHoc.Text);


            EventHelper.Helper.MessageInfomationYesNo("Đồng ý tạo khóa học mới ?", () =>
            {
                //kiểm tra rỗng
                if (MetroHelper.IsNullOrEmpty(new object[] { tenkhoahoc, hocphikhoahoc.ToString() }))
                {
                    EventHelper.Helper.MessageLessInformation();
                    return;
                }
                //Kiem Tra hoc phi
                if (hocphikhoahoc <= 0)
                {
                    EventHelper.Helper.MessageInfomation("Tiền học phí phải lớn hơn 0 đồng!");
                    txtHocPhiKhoaHoc.Focus();
                    return;
                }
                //thêm khóa học
                KHOAHOC khoahoc = new KHOAHOC();
                khoahoc.MAKHOAHOC = makhoahoc;
                khoahoc.TENKHOAHOC = tenkhoahoc.ToUpper();
                khoahoc.HOCPHI = hocphikhoahoc;

                ResultExcuteQuery r = (new Insert()).ThemKhoaHoc(khoahoc);
                EventHelper.Helper.MessageInfomation(r.GetMessage());
                if (r.isSuccessfully())
                {
                    loadDataGridView();
                    settingLayoutForThem();
                }

            });
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            string makhoahoc = grdCourse.grid.CurrentRow.Cells[0].Value.ToString();

            if (makhoahoc != "")
            {
                EventHelper.Helper.MessageInfomationYesNo("Bạn có chắc chắn muốn xóa khóa học " + makhoahoc + " ?", () =>
                {
                    ResultExcuteQuery r = (new Delete()).XoaKhoaHoc(makhoahoc);
                    EventHelper.Helper.MessageInfomation(r.GetMessage());
                    if (r.isSuccessfully())
                        loadDataGridView();


                });
                
            }
        }



        private void Grid_SelectionChanged(object sender, EventArgs e)
        {
            if (rdbtnCapNhatKhoaHoc.Checked)
                settingLayoutForCapNhat();
        }

        public void ClosingCreateCourse(object sender, FormClosingEventArgs e)
        {
            if (focusLose)
                e.Cancel = false;
            else
                EventHelper.Helper.MessageInfomationYesNo("Đóng cửa sổ tạo khóa học ?", () => e.Cancel = false, () => e.Cancel = true);
        }


    }
}
