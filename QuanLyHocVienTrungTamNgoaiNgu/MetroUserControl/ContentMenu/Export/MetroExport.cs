﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using MetroFramework.Controls;
using Export;
using MetroControl;

namespace MetroUserControl.ContentMenu.Export
{
    public partial class MetroExport : MetroBubble
    {
        string name;
        Excel excel;
        const int soluong = 20;
        Func<List<object>> datadefault;
        public MetroExport(string name, Func<List<object>> datadefault)
        {
            InitializeComponent();
            this.Load += MetroExport_Load;
            this.name = name.ToUpper();
            this.datadefault = datadefault;
            excel = new Excel();
            this.metroProgressBar1.Visible = false;
        }

        private void MetroExport_Load(object sender, EventArgs e)
        {
           new Thread(new ThreadStart(() => loadingProgressbar())).Start();

            //event
            txtFrom.KeyPress += (S, E) => { E.Handled = !(Char.IsDigit(E.KeyChar) || Char.IsControl(E.KeyChar)); };
            txtTo.KeyPress += (S, E) => { E.Handled = !(Char.IsDigit(E.KeyChar) || Char.IsControl(E.KeyChar)) ; };

            txtName.Enter += (S, E) => { txtName.ReadOnly = false; };
            txtName.Leave += (S, E) => { txtName.ReadOnly = true; };
            btnXacNhan.Click += BtnXacNhan_Click;
            btnhuy.Click += (S, E) => this.ParentForm.Close();
            cbbMode.SelectedIndexChanged += (S, E) => VisibleAdvancedMode(cbbMode.SelectedIndex != 0);
            radioAllPages.CheckedChanged += (S, E) =>
            {
                radioPages.Checked = !radioAllPages.Checked;
            };
            radioPages.CheckedChanged += (S, E) =>
            {
                radioAllPages.Checked = !radioPages.Checked;
                VisibleAdvancedPagesMode(radioPages.Checked);
            };
            //load giao diện
            txtName.Text = this.name;
            txtFrom.Text = "1";
            txtTo.Text = "1";
            cbbMode.DataSource = new List<string> { "DANH SÁCH HIỆN TẠI", "TUỲ CHỌN NÂNG CAO" };
            cbbMode.SelectedIndex = 0;
            VisibleAdvancedPagesMode(false);
        }

        private void BtnXacNhan_Click(object sender, EventArgs e)
        {
            Func<List<object>> data = datadefault;
            if (cbbMode.SelectedIndex != 0)
            {
                if (radioAllPages.Checked)
                    data = XuatTatCa();
                else
                    data = XuatTheoTrang(int.Parse(txtFrom.Text), int.Parse(txtTo.Text));
            }
            if (MetroHelper.staff != null && data != null)
            {
                excel.SaveAsFromListData(MetroHelper.staff.TENNHANVIEN, txtName.Text, data);
            }
        }

        private Func<List<object>> XuatTheoTrang(int from, int to)
        {
            if(from > to)
            {
                if (MessageBox.Show("Thông tin trang không hợp lệ", "Thông báo", MessageBoxButtons.OK) == DialogResult.OK)
                    txtTo.Focus();
                return null;
            }
            int page = from;
            int count = (to - from + 1) * soluong;
            Func<List<object>> data = () => (new MetroDataset.LoadByPage()).HienThiTatCaDiem(page, count);
            return data;
        }

        private Func<List<object>> XuatTatCa()
        {
            Func<List<object>> data = () => (new MetroDataset.LoadByPage()).HienThiTatCaDiem();
            return data;
        }

        private void VisibleAdvancedPagesMode(bool visible)
        {
            metroLabel4.Visible = visible;
            txtFrom.Visible = visible;
            metroLabel5.Visible = visible;
            txtTo.Visible = visible;
        }

        private void VisibleAdvancedMode(bool visible)
        {
            metroLabel3.Visible = visible;
            radioAllPages.Visible = visible;
            radioPages.Visible = visible;
            if (radioPages.Checked)
            {
                metroLabel4.Visible = visible;
                txtFrom.Visible = visible;
                metroLabel5.Visible = visible;
                txtTo.Visible = visible;
            }
        }

        private void loadingProgressbar()
        {
            while (1==1)
            {
                EventHelper.Helper.TryCatch(() =>
                {
                    Thread.Sleep(20);
                    SetValueProgress(metroProgressBar1);
                });
            }
        }

        delegate void SetValueProgressCallBack(ProgressBar progress);
        private void SetValueProgress(ProgressBar progress)
        {
            if (progress.InvokeRequired)
            {
                SetValueProgressCallBack d = new SetValueProgressCallBack(SetValueProgress);
                this.Invoke(d, new object[] { progress });
            }
            else
            {
                if (!excel.completed)
                {
                    progress.Value += 1;
                    if (progress.Value == progress.Maximum)
                        progress.Value = 0;
                    metroProgressBar1.Visible = !excel.completed;
                    metroProgressBar1.Refresh();
                }
                else if (metroProgressBar1.Visible)
                {
                    MessageBox.Show("Đã lưu danh sách thành công", "Thông báo", MessageBoxButtons.OK);
                    metroProgressBar1.Visible = !excel.completed;
                    metroProgressBar1.Refresh();
                }
            }
        }


    }
}
