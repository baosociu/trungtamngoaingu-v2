﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroControl.Components;
using MetroControl;
using MetroDataset;

namespace MetroUserControl.ContentMenu.Permission
{
    public partial class MetroActionPermission : MetroBubble
    {



        private class Value
        {
            string idgiaodien;
            string idthaotac;
            bool check;

            public string IDGiaoDien { get => idgiaodien; }
            public string IDThaoTac { get => idthaotac; }
            public bool Check { get => check; }


            public Value(string idgiaodien, string idthaotac, bool check)
            {
                this.check = check;
                this.idthaotac = idthaotac;
                this.idgiaodien = idgiaodien;
            }
        }
        List<Value> values;

        public MetroActionPermission()
        {
            InitializeComponent();
            this.Load += MetroActionPermission_Load;
            this.cbbTypeGroup.DataSource = new List<object>()
            {
                new {Key = "H", Value = "STUDENT"},
                new {Key = "G", Value = "TEACHER"},
                new {Key = "S", Value = "STAFF"}
            };
        }

        private bool isErrorDefault(String idgroup, String idgiaodien)
        {
            if (MetroHelper.isStudentByIDGroup(idgroup))
                return !MetroHelper.default_student.Contains(idgiaodien);
            if (MetroHelper.isTeacherByIDGroup(idgroup))
                return !MetroHelper.default_teacher.Contains(idgiaodien);
            if (MetroHelper.isStaffByIDGroup(idgroup))
                return !MetroHelper.default_staff.Contains(idgiaodien);
            if (idgroup == MetroHelper.XMASTER)
                return !MetroHelper.default_master.Contains(idgiaodien);
            return false;
        }

        private void GrdThaoTac_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex >= 2 && e.RowIndex != -1)
            {
                MetroGridView grid = (sender as MetroGridView);
                string id_tt = (new MetroDataType()).GetStringByIndex(e.ColumnIndex - 2);
                string id_gd = grid[0, e.RowIndex].Value.ToString();
                string id_group = cbbGroup.SelectedValue.ToString();
                if (isErrorDefault(id_group, id_gd) || id_group ==MetroHelper.XMASTER)
                {
                    EventHelper.Helper.MessageDefaultSettings();
                    return;
                }
                else
                    if ((id_group == MetroHelper.XMASTER || id_group == MetroHelper.STAFF) && !MetroHelper.isMaster())
                {
                    EventHelper.Helper.MessageLessPermision();
                    return;
                }

                bool b = !bool.Parse(grid[e.ColumnIndex, e.RowIndex].Value.ToString());
                grid[e.ColumnIndex, e.RowIndex].Value = b;

                //LƯU THAO TÁC CỦA NGƯỜI DÙNG

                this.values.Add(new Value(id_gd, id_tt, b));
            }
        }

        private void GrdThaoTac_DataSourceChanged(object sender, EventArgs e)
        {
            DataGridView grid = (sender as DataGridView);
            for (int i = 2; i < grid.Columns.Count; i++)
                grid.Columns[i].ReadOnly = false;
        }

        private void MetroActionPermission_Load(object sender, EventArgs e)
        {
            this.gridPage.grid.ReadOnly = true;
            this.gridPage.grid.DataSourceChanged += GrdThaoTac_DataSourceChanged;
            this.gridPage.grid.CellContentClick += GrdThaoTac_CellContentClick;
            this.values = new List<Value>();

            //pic
            EventHelper.Helper.SetHandHover(this.picCreate);
            EventHelper.Helper.SetHandHover(this.picRemove);
            this.picCreate.Click += (s, en) => addNhomNguoiDung(txtCreateNew.Text);
            this.picRemove.Click += (s, en) => removeNhomNguoiDung(cbbGroup.SelectedValue.ToString());

            //button
            this.btnSave.Click += (s, en) => saveThaoTac();
            this.btnBack.Click += (s, en) => MetroHelper.GoToTab(this.ParentForm, new MetroGroupPermission());

            //combobox
            LoadDataComboBox(0);
            cbbGroup.SelectedIndexChanged += CbbGroup_SelectedIndexChanged;

            string id = cbbGroup.SelectedValue.ToString();
            LoadDataGridViewByGroupID(id);
        }

        private void removeNhomNguoiDung(string idmanhom)
        {
            if (MetroHelper.default_group.Contains(idmanhom))
            {
                EventHelper.Helper.MessageDefaultSettings();
                return;
            }

            EventHelper.Helper.MessageInfomationYesNo("Đồng ý xoá nhóm người dùng \'" + cbbGroup.Text + "\' ?", () =>
            {
                ResultExcuteQuery r = (new MetroDataset.Delete()).XoaNhomNguoiDung(idmanhom);
                EventHelper.Helper.MessageInfomation(r.GetMessage());

                if (r.isSuccessfully())
                    LoadDataComboBox(0);
            });
        }

        private void LoadDataComboBox(int postion)
        {
            List<object> data = (new MetroDataset.Select()).HienThiTatCaNhomNguoiDung_Cbb();
            if (data != null && data.Count != 0)
                cbbGroup.DataSource = data;
            cbbGroup.SelectedIndex = postion;
        }

        private void addNhomNguoiDung(string text)
        {
            if (String.IsNullOrWhiteSpace(text) || text.Length <= 3)
            {
                EventHelper.Helper.MessageLessInformation();
                return;
            }
            String type = cbbTypeGroup.SelectedValue.ToString();
            ResultExcuteQuery r = (new Insert()).ThemNhomNguoiDung(text, type);
            EventHelper.Helper.MessageInfomation(r.GetMessage());
            if (r.isSuccessfully())
            {
                LoadDataComboBox(cbbGroup.Items.Count - 1);
                txtCreateNew.Clear();
            }

        }

        private void saveThaoTac()
        {
            if (values.Count == 0)
            {
                EventHelper.Helper.MessageNoChangedInformation();
                return;
            }
            string manhom = cbbGroup.SelectedValue.ToString();
            foreach (Value v in values)
            {
                string mathaotac = v.IDThaoTac;
                string magiaodien = v.IDGiaoDien;
                bool check = v.Check;
                (new MetroDataset.Update()).CapNhatThaoTacCuaNhomNguoiDung(manhom, magiaodien, mathaotac, check);
            }

            EventHelper.Helper.MessageInfomation("Thông tin phân quyền thao tác đã được lưu lại");

            LoadDataGridViewByGroupID(manhom);
        }

        private void CbbGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            string id = (sender as ComboBox).SelectedValue.ToString();
            LoadDataGridViewByGroupID(id);
        }

        private void LoadDataGridViewByGroupID(string id)
        {
            values = new List<Value>();

            DataSetQuanLyHocVienTableAdapters.HienThiThongTinThaoTacCuaNhomNguoiDungTableAdapter
                dt = new DataSetQuanLyHocVienTableAdapters.HienThiThongTinThaoTacCuaNhomNguoiDungTableAdapter();
            DataTable tb = dt.GetData(id);
            foreach (System.Data.DataColumn col in tb.Columns)
                col.ReadOnly = false;
            this.gridPage.grid.DataSource = tb;

        }
    }
}
