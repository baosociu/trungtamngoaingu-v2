﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroControl;
using MetroUserControl.DataSetQuanLyHocVienTableAdapters;

namespace MetroUserControl.ContentMenu.Permission
{

    public partial class MetroGroupPermission : MetroBubble
    {
        //List ghi nhận thao tác Thêm - Xoá người dùng khỏi 1 nhóm người dùng

        List<Action> listAction;
        public MetroGroupPermission()
        {
            InitializeComponent();
            this.Load += MetroGroupPermission_Load;

        }

        private void MetroGroupPermission_Load(object sender, EventArgs e)
        {
            //pic
            EventHelper.Helper.SetHandHover(picSetting);
            EventHelper.Helper.SetHandHover(picToLeft);
            EventHelper.Helper.SetHandHover(picToRight);
            btnHuy.Click += (s, en) => this.ParentForm.Close();
            picToRight.Click += (s, en) => addNguoiDung();
            picToLeft.Click += (s, en) => removeNguoiDung();
            picSetting.Click += (s, en) => MetroHelper.GoToTab(this.ParentForm, new MetroActionPermission());
            //button
            btnSave.Click += (s, en) => saveNguoiDung();

            //COMBOBOX nhomnguoidung
            List<object> data = (new MetroDataset.Select()).HienThiTatCaNhomNguoiDung_Cbb();
            if(data != null && data.Count != 0)
                cbbGroup.DataSource = data;

            if(this.cbbGroup.Tag != null)
                cbbGroup.SelectedValue = this.cbbGroup.Tag.ToString();
            cbbGroup.SelectedIndexChanged += CbbGroup_SelectedIndexChanged;

            string id = cbbGroup.SelectedValue.ToString();
            LoadDataGridViewByGroupID(id);
        }

        public MetroGroupPermission SetAsGroup(string manhom)
        {
            cbbGroup.Tag = manhom;
            return this;
        }

        private void saveNguoiDung()
        {
            if(listAction.Count == 0)
            {
                EventHelper.Helper.MessageNoChangedInformation();
                return;
            }


            foreach (Action ac in listAction)
            {
                string manhom = cbbGroup.SelectedValue.ToString();
                string manguoidung = ac.getId;

                bool i = false;
                if (ac.getAction == Action.action.Insert)
                    i = (new MetroDataset.Insert()).ThemNguoiDungVaoNhomNguoiDung(manhom, manguoidung);
                else
                    i = (new MetroDataset.Delete()).XoaNguoiDungTuNhomNguoiDung(manhom, manguoidung);
            }
            EventHelper.Helper.MessageInfomation("Thông tin phân quyền đã được lưu lại");
            string group_id = cbbGroup.SelectedValue.ToString();
            LoadDataGridViewByGroupID(group_id);
        }

        private void removeNguoiDung()
        {
            string id_nguoidung = grdNguoiDung_TrongNhom.CurrentRow.Cells[0].Value.ToString();
            if (id_nguoidung != MetroHelper.iduser)
            {
                if ((cbbGroup.SelectedValue.ToString() == MetroHelper.STAFF && !MetroHelper.isMaster()) || cbbGroup.SelectedValue.ToString() == MetroHelper.XMASTER)
                {
                    EventHelper.Helper.MessageLessPermision();
                    return;
                }
            }

            listAction.Add(new Action(id_nguoidung, Action.action.Delete));

            DataGridViewRow row = grdNguoiDung_TrongNhom.CurrentRow;

            DataTable dt = (DataTable)grdNguoiDung_KhongTrongNhom.DataSource;

            DataRow newrow = dt.NewRow();
            newrow[0] = row.Cells[0].Value;
            newrow[1] = row.Cells[1].Value;

            dt.Rows.Add(newrow);
            dt.AcceptChanges();

            grdNguoiDung_TrongNhom.Rows.Remove(row);
        }

        private void addNguoiDung()
        {
            if ((cbbGroup.SelectedValue.ToString() == MetroHelper.STAFF || cbbGroup.SelectedValue.ToString() == MetroHelper.XMASTER) && !MetroHelper.isMaster())
            {
                EventHelper.Helper.MessageLessPermision();
                return;
            }
            string id_nguoidung = grdNguoiDung_KhongTrongNhom.CurrentRow.Cells[0].Value.ToString();
            string id_group = cbbGroup.SelectedValue.ToString();

            if ((MetroHelper.isStudentByID(id_nguoidung) && MetroHelper.isStudentByIDGroup(id_group)) ||
                (MetroHelper.isStaffByID(id_nguoidung) && MetroHelper.isStaffByIDGroup(id_group)) ||
                (MetroHelper.isTeacherByID(id_nguoidung) && MetroHelper.isTeacherByIDGroup(id_group)) ||
               (MetroHelper.isStaffByID(id_nguoidung) && id_group == MetroHelper.XMASTER))
            {
                listAction.Add(new Action(id_nguoidung, Action.action.Insert));

                DataGridViewRow row = grdNguoiDung_KhongTrongNhom.CurrentRow;

                DataTable dt = (DataTable)grdNguoiDung_TrongNhom.DataSource;

                DataRow newrow = dt.NewRow();
                newrow[0] = row.Cells[0].Value;
                newrow[1] = row.Cells[1].Value;

                dt.Rows.Add(newrow);
                dt.AcceptChanges();

                grdNguoiDung_KhongTrongNhom.Rows.Remove(row);
            }
            else
                EventHelper.Helper.MessageMissRequest();
        }

        private void LoadDataGridViewByGroupID(string id)
        {
            //clear listAction khi người dùng chọn group mới
            listAction = new List<Action>();

            //gridview nhóm người dùng
            using (DataSetQuanLyHocVienTableAdapters.HienThiNguoiDungTheoNhomNguoiDungTableAdapter
                dt = new DataSetQuanLyHocVienTableAdapters.HienThiNguoiDungTheoNhomNguoiDungTableAdapter())
            {
                this.grdNguoiDung_TrongNhom.DataSource = dt.GetData(id);
            }
            //gridview người dùng
            using (DataSetQuanLyHocVienTableAdapters.HienThiNguoiDungKhongTrongNhomNguoiDungTableAdapter
                dt = new DataSetQuanLyHocVienTableAdapters.HienThiNguoiDungKhongTrongNhomNguoiDungTableAdapter())
            {
                this.grdNguoiDung_KhongTrongNhom.DataSource = dt.GetData(id);
            }
        }



        private void CbbGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            string id = (sender as ComboBox).SelectedValue.ToString();
            LoadDataGridViewByGroupID(id);
        }
    }

    internal class Action
    {
        public enum action
        {
            Insert, Delete
        }
        string id;
        action ac;
        public action getAction { get => ac; }
        public string getId { get => id; }
        internal Action(string id, action a)
        {
            this.id = id;
            this.ac = a;
        }
    }
}
