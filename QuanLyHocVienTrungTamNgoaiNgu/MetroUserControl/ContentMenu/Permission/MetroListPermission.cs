﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroControl;
using MetroControl.Components;
using MetroDataset;
using static MetroControl.MetroHelper;

namespace MetroUserControl.ContentMenu.Permission
{
    public partial class MetroListPermission : MetroBubble
    {

        public MetroListPermission()
        {
            InitializeComponent();
            this.Load += MetroListPermission_Load;
            this.VisibleChanged += MetroListPermission_VisibleChanged;
        }

        private void MetroListPermission_VisibleChanged(object sender, EventArgs e)
        {
            this.SettingLoadByPageFilterSearch(this.gridPage, (new LoadByPage()).HienThiNguoiDungVaNhomNguoiDung, this.cbbLoc, (new Select()).HienThiFilterPhanQuyen_Cbb, picSearch, txtTimKiem);

        }

        private void MetroListPermission_Load(object sender, EventArgs e)
        {
            this.btnPhanQuyen.Click += (s, en) => MetroHelper.OpenNewTab(this, new MetroGroupPermission());
            this.btnXoa.Click += (s, en) => DeleteUserFromGroup(this.gridPage.grid);

            EventHelper.Helper.SetDoubleClickItemGridView(this.gridPage.grid, () =>
             {
                 if (btnPhanQuyen.Visible)
                     MetroHelper.OpenNewTab(this, new MetroGroupPermission().SetAsGroup(gridPage.grid.SelectedRows[0].Cells[3].Value.ToString()));
             });
            this.gridPage.grid.CellContentClick += Grid_CellContentClick;

            this.btnPhanQuyen.TypeData.Add(MetroControl.Components.Type.Master);
            this.btnXoa.TypeData.Add(MetroControl.Components.Type.Master);
            this.VisibleByPermission();
        }


        private void Grid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 5 && e.RowIndex != -1)
            {
                MetroGridView grd = (sender as MetroGridView);
                string id_nhomnguoidung = grd.CurrentRow.Cells[3].Value.ToString();
                string id_nguoidung = grd.CurrentRow.Cells[0].Value.ToString();

                if ((id_nhomnguoidung == MetroHelper.STAFF || id_nhomnguoidung == MetroHelper.XMASTER) && !isMaster())
                {
                    EventHelper.Helper.MessageLessPermision();
                    return;
                }
                else
                {
                    bool b = !bool.Parse(grd[e.ColumnIndex, e.RowIndex].Value.ToString());
                    EventHelper.Helper.MessageInfomationYesNo("Đồng ý " + (b ? "kích hoạt" : "huỷ") + " quyền ?", () =>
                            {
                                (grd.Rows[e.RowIndex].Cells[e.ColumnIndex]).Value = b;
                                ChangeStatusActive(grd, id_nhomnguoidung, id_nguoidung);
                            });
                }
            }
        }

        private void DeleteUserFromGroup(MetroGridView grd)
        {
            EventHelper.Helper.TryCatch(() =>
            {
                string id_manhom = grd.CurrentRow.Cells[3].Value.ToString();
                string id_manguoidung = grd.CurrentRow.Cells[0].Value.ToString();

                if((id_manhom == MetroHelper.STAFF || id_manhom == MetroHelper.XMASTER) && !isMaster())
                {
                    EventHelper.Helper.MessageLessPermision();
                    return;
                }

                EventHelper.Helper.MessageInfomationYesNo("Đồng ý xoá người dùng \'" + id_manguoidung + "\' ra khỏi nhóm \'" + id_manhom + "\' ?", () =>
                {
                    EventHelper.Helper.TryCatch(() =>
                    {
                        (new Delete()).XoaNguoiDungTrongNhomNguoiDung(id_manhom, id_manguoidung);
                        grd.Rows.Remove(grd.CurrentRow);
                        grd.Rows[0].Selected = true;
                    });
                });
            });

        }

        private void ChangeStatusActive(MetroGridView grd, String id_nhomnguoidung, String id_nguoidung)
        {
            try
            {
                ResultExcuteQuery i = (new MetroDataset.Update()).CapNhatHoatDongNguoiDungTrongNhomNguoiDung(id_nhomnguoidung, id_nguoidung);
                EventHelper.Helper.MessageInfomation(i.GetMessage());
            }
            catch (Exception e) { }
        }

    }
}
