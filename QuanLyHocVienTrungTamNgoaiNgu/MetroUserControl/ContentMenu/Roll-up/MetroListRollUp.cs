﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroControl;
using MetroDataset;
using Export;

namespace MetroUserControl.ContentMenu.Roll_up
{
    public partial class MetroListRollUp : MetroBubble
    {
        List<DiemDanh> list_diemdanh;


        private class DiemDanh
        {
            string malich;
            string mahocvien;
            string malophoc;
            bool diemdanh;

            public DiemDanh(string malich, string mahocvien, string malophoc, bool diemdanh)
            {
                this.mahocvien = mahocvien;
                this.malich = malich;
                this.malophoc = malophoc;
                this.diemdanh = diemdanh;
            }

            public string Malich { get => malich; set => malich = value; }
            public string Mahocvien { get => mahocvien; set => mahocvien = value; }
            public string Malophoc { get => malophoc; set => malophoc = value; }
            public bool Diemdanh { get => diemdanh; set => diemdanh = value; }
        }
        public MetroListRollUp()
        {
            InitializeComponent();
            this.Load += MetroListRollUp_Load;

        }


        private void MetroListRollUp_Load(object sender, EventArgs e)
        {
            list_diemdanh = new List<DiemDanh>();
            this.gridRollup.grid.CellContentClick += Grid_CellContentClick;
            this.cbbLop.SelectedValueChanged += (s, en) => CbbLop_SelectedValueChanged();
            this.cbbNgay.SelectedValueChanged += (s, en) => CbbNgay_SelectedValueChanged();
            this.gridRollup.grid.DataSourceChanged += Grid_DataSourceChanged;
            this.gridRollup.SetOnClick(CbbNgay_SelectedValueChanged);
            this.btnDiemDanh.Click += BtnDiemDanh_Click;
            this.cbbLop.DataSource = (new Select()).HienThiLopHocGiangVien_Cbb(MetroHelper.iduser);
            this.btnPrint.Click += (s, en) => InDanhSachDiemDanh();

            this.btnDiemDanh.TypeData.Add(MetroControl.Components.Type.Update);
            this.VisibleByPermission();
        }

        private void InDanhSachDiemDanh()
        {
            if (MetroHelper.IsNullOrEmpty(new object[] { cbbNgay.SelectedValue, cbbLop.SelectedValue }))
                return;
            string malich = this.cbbNgay.SelectedValue.ToString();
            string malop = this.cbbLop.SelectedValue.ToString();

            BieuMau.InDanhSachDiemDanh(malop, malich);
        }

        private void BtnDiemDanh_Click(object sender, EventArgs e)
        {
            if (list_diemdanh.Count == 0)
                EventHelper.Helper.MessageNoChangedInformation();
            else
                EventHelper.Helper.MessageInfomation("Đã lưu lại thông tin điểm danh");


            foreach (DiemDanh d in list_diemdanh)
                (new MetroDataset.Update()).CapNhatDiemDanhHocVien(d.Mahocvien, d.Malich, d.Malophoc, d.Diemdanh);
            list_diemdanh = new List<DiemDanh>();

        }

        private void Grid_DataSourceChanged(object sender, EventArgs e)
        {
            list_diemdanh = new List<DiemDanh>();
        }

        private void CbbNgay_SelectedValueChanged()
        {
            //khi chọn Lịch ==> thay grid điểm danh
            using (DataSetQuanLyHocVienTableAdapters.HienThiCTBuoiHocLichHocTableAdapter
                dt = new DataSetQuanLyHocVienTableAdapters.HienThiCTBuoiHocLichHocTableAdapter())
            {
                string malop = this.cbbLop.SelectedValue.ToString();
                string malich = this.cbbNgay.SelectedValue.ToString();
                int count = this.gridRollup.NumberRow;
                int page = this.gridRollup.PageNumber;
                DataSetQuanLyHocVien.HienThiCTBuoiHocLichHocDataTable tb = dt.GetData(malich, malop, page, count);
                tb.ĐIỂM_DANHColumn.ReadOnly = false;

                this.gridRollup.grid.DataSource = tb;
                this.gridRollup.grid.Columns[6].ReadOnly = false;
                this.gridRollup.AlignColumn(0, DataGridViewContentAlignment.MiddleCenter);
                this.gridRollup.AlignColumn(3, DataGridViewContentAlignment.MiddleCenter);
                this.gridRollup.AlignColumn(4, DataGridViewContentAlignment.MiddleCenter);
                this.gridRollup.AlignColumn(5, DataGridViewContentAlignment.MiddleCenter);
                this.gridRollup.AlignColumn(6, DataGridViewContentAlignment.MiddleCenter);
            }
        }

        private void CbbLop_SelectedValueChanged()
        {
            //khi chọn Lop ==> thay đổi lịch
            //using (DataSetQuanLyHocVienTableAdapters.LayLichHocLopHocTableAdapter
            //    dt = new DataSetQuanLyHocVienTableAdapters.LayLichHocLopHocTableAdapter())
            //{
            //    string malop = (((DataRowView)this.cbbLop.SelectedItem).Row as DataSetQuanLyHocVien.LayLopHocGiangVienRow).MALOPHOC;
            //    DataSetQuanLyHocVien.LayLichHocLopHocDataTable tb = dt.GetData(malop);
            //    this.cbbNgay.DataSource = tb;
            //    this.cbbNgay.DisplayMember = tb.LICHHOCColumn.Caption;
            //    this.cbbNgay.ValueMember = tb.MALICHHOCColumn.Caption;

            //    if (this.cbbNgay.Items.Count != 0)
            //        this.cbbNgay.SelectedItem = this.cbbNgay.Items[0];
            //}
            string malop = cbbLop.SelectedValue.ToString();
            List<object> data = (new MetroDataset.Select()).HienThiLichHocLopHoc_Cbb(malop);
            if (data != null && data.Count != 0)
            {
                this.cbbNgay.DataSource = data;
                this.cbbNgay.SelectedItem = this.cbbNgay.Items[0];
            }
        }

        private void Grid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 6)
            {
                bool b = !bool.Parse((sender as DataGridView)[e.ColumnIndex, e.RowIndex].Value.ToString());
                (sender as DataGridView)[e.ColumnIndex, e.RowIndex].Value = b;

                string malich = cbbNgay.SelectedValue.ToString();
                string malop = cbbLop.SelectedValue.ToString();
                string mahocvien = gridRollup.grid.Rows[e.RowIndex].Cells[0].Value.ToString();
                list_diemdanh.Add(new DiemDanh(malich, mahocvien, malop, b));
            }
        }
    }
}
