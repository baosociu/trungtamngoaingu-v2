﻿namespace MetroUserControl.ContentMenu.Roll_up
{
    partial class MetroListRollUp
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.metroLabel4 = new MetroControl.Components.MetroLabel();
            this.metroLabel3 = new MetroControl.Components.MetroLabel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.btnDiemDanh = new MetroControl.Components.MetroButton();
            this.btnPrint = new MetroControl.Components.MetroButton();
            this.gridRollup = new MetroControl.Controls.MetroGridViewPage();
            this.cbbLop = new MetroControl.Components.MetroCombobox();
            this.cbbNgay = new MetroControl.Components.MetroCombobox();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.ColumnCount = 6;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.45455F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.09091F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.45455F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            this.tableLayoutPanel2.Controls.Add(this.metroLabel4, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel3, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.gridRollup, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.cbbLop, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.cbbNgay, 4, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 5;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.549793F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90.90496F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.545249F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(774, 434);
            this.tableLayoutPanel2.TabIndex = 4;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel4.Location = new System.Drawing.Point(391, 0);
            this.metroLabel4.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(87, 32);
            this.metroLabel4.TabIndex = 34;
            this.metroLabel4.Text = "Ngày học:";
            this.metroLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel3.Location = new System.Drawing.Point(3, 0);
            this.metroLabel3.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(87, 32);
            this.metroLabel3.TabIndex = 33;
            this.metroLabel3.Text = "Lớp:";
            this.metroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 5;
            this.tableLayoutPanel2.SetColumnSpan(this.tableLayoutPanel4, 6);
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Controls.Add(this.btnDiemDanh, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnPrint, 3, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 396);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(774, 38);
            this.tableLayoutPanel4.TabIndex = 24;
            // 
            // btnDiemDanh
            // 
            this.btnDiemDanh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDiemDanh.Location = new System.Drawing.Point(194, 3);
            this.btnDiemDanh.Name = "btnDiemDanh";
            this.btnDiemDanh.Size = new System.Drawing.Size(94, 32);
            this.btnDiemDanh.TabIndex = 0;
            this.btnDiemDanh.Text = "LƯU";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnDiemDanh.UseSelectable = true;
            // 
            // btnPrint
            // 
            this.btnPrint.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPrint.Location = new System.Drawing.Point(485, 3);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(94, 32);
            this.btnPrint.TabIndex = 2;
            this.btnPrint.Text = "IN DANH SÁCH";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnPrint.UseSelectable = true;
            // 
            // gridRollup
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.gridRollup, 6);
            this.gridRollup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridRollup.Location = new System.Drawing.Point(3, 51);
            this.gridRollup.Name = "gridRollup";
            this.gridRollup.Size = new System.Drawing.Size(768, 326);
            this.gridRollup.TabIndex = 30;
            // 
            // cbbLop
            // 
            this.cbbLop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbLop.DisplayMember = "Value";
            this.cbbLop.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbbLop.FormattingEnabled = true;
            this.cbbLop.ItemHeight = 19;
            this.cbbLop.Location = new System.Drawing.Point(100, 3);
            this.cbbLop.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.cbbLop.Name = "cbbLop";
            this.cbbLop.Size = new System.Drawing.Size(240, 25);
            this.cbbLop.TabIndex = 31;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.cbbLop.UseSelectable = true;
            this.cbbLop.ValueMember = "Key";
            // 
            // cbbNgay
            // 
            this.cbbNgay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbNgay.DisplayMember = "Value";
            this.cbbNgay.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbbNgay.FormattingEnabled = true;
            this.cbbNgay.ItemHeight = 19;
            this.cbbNgay.Location = new System.Drawing.Point(488, 3);
            this.cbbNgay.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.cbbNgay.Name = "cbbNgay";
            this.cbbNgay.Size = new System.Drawing.Size(240, 25);
            this.cbbNgay.TabIndex = 32;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.cbbNgay.UseSelectable = true;
            this.cbbNgay.ValueMember = "Key";
            // 
            // MetroListRollUp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel2);
            this.Name = "MetroListRollUp";
            this.Size = new System.Drawing.Size(774, 434);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private MetroControl.Components.MetroButton btnDiemDanh;
        private MetroControl.Components.MetroButton btnPrint;
        internal MetroControl.Controls.MetroGridViewPage gridRollup;
        private MetroControl.Components.MetroLabel metroLabel4;
        private MetroControl.Components.MetroLabel metroLabel3;
        internal MetroControl.Components.MetroCombobox cbbLop;
        internal MetroControl.Components.MetroCombobox cbbNgay;
    }
}
