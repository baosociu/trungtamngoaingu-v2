﻿namespace MetroUserControl.ContentMenu.Systems
{
    partial class MetroBackup
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.txtDuongDanLuu = new MetroControl.Components.MetroTextbox();
            this.metroLabel2 = new MetroControl.Components.MetroLabel();
            this.metroLabel1 = new MetroControl.Components.MetroLabel();
            this.txtTenTapTinBackup = new MetroControl.Components.MetroTextbox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.btnHuyBackup = new MetroControl.Components.MetroButton();
            this.btnSaoLuuBackup = new MetroControl.Components.MetroButton();
            this.pickerNgayLuuBackup = new MetroControl.Components.MetroDatePicker();
            this.metroLabel4 = new MetroControl.Components.MetroLabel();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75F));
            this.tableLayoutPanel3.Controls.Add(this.txtDuongDanLuu, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.metroLabel2, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.metroLabel1, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtTenTapTinBackup, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel5, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.pickerNgayLuuBackup, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.metroLabel4, 0, 2);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 5;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(400, 200);
            this.tableLayoutPanel3.TabIndex = 3;
            // 
            // txtDuongDanLuu
            // 
            this.txtDuongDanLuu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDuongDanLuu.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtDuongDanLuu.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtDuongDanLuu.Location = new System.Drawing.Point(100, 68);
            this.txtDuongDanLuu.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtDuongDanLuu.Name = "txtDuongDanLuu";
            this.txtDuongDanLuu.ReadOnly = true;
            this.txtDuongDanLuu.Size = new System.Drawing.Size(300, 23);
            this.txtDuongDanLuu.TabIndex = 11;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel2.IsFocus = false;
            this.metroLabel2.Location = new System.Drawing.Point(0, 32);
            this.metroLabel2.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(90, 32);
            this.metroLabel2.TabIndex = 4;
            this.metroLabel2.Text = "Ngày lưu:";
            this.metroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel1.IsFocus = true;
            this.metroLabel1.Location = new System.Drawing.Point(0, 0);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(90, 32);
            this.metroLabel1.TabIndex = 3;
            this.metroLabel1.Text = "Tên tập tin(*):";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtTenTapTinBackup
            // 
            this.txtTenTapTinBackup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTenTapTinBackup.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtTenTapTinBackup.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtTenTapTinBackup.Location = new System.Drawing.Point(100, 4);
            this.txtTenTapTinBackup.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtTenTapTinBackup.Name = "txtTenTapTinBackup";
            this.txtTenTapTinBackup.ReadOnly = true;
            this.txtTenTapTinBackup.Size = new System.Drawing.Size(300, 23);
            this.txtTenTapTinBackup.TabIndex = 6;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 5;
            this.tableLayoutPanel3.SetColumnSpan(this.tableLayoutPanel5, 2);
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.Controls.Add(this.btnHuyBackup, 3, 0);
            this.tableLayoutPanel5.Controls.Add(this.btnSaoLuuBackup, 1, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 164);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(400, 36);
            this.tableLayoutPanel5.TabIndex = 9;
            // 
            // btnHuyBackup
            // 
            this.btnHuyBackup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnHuyBackup.Location = new System.Drawing.Point(253, 3);
            this.btnHuyBackup.Name = "btnHuyBackup";
            this.btnHuyBackup.Size = new System.Drawing.Size(94, 30);
            this.btnHuyBackup.TabIndex = 3;
            this.btnHuyBackup.Text = "HUỶ";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnHuyBackup.UseSelectable = true;
            // 
            // btnSaoLuuBackup
            // 
            this.btnSaoLuuBackup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSaoLuuBackup.Location = new System.Drawing.Point(53, 3);
            this.btnSaoLuuBackup.Name = "btnSaoLuuBackup";
            this.btnSaoLuuBackup.Size = new System.Drawing.Size(94, 30);
            this.btnSaoLuuBackup.TabIndex = 2;
            this.btnSaoLuuBackup.Text = "SAO LƯU";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.btnSaoLuuBackup.UseSelectable = true;
            // 
            // pickerNgayLuuBackup
            // 
            this.pickerNgayLuuBackup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pickerNgayLuuBackup.Enabled = false;
            this.pickerNgayLuuBackup.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.pickerNgayLuuBackup.FontSize = MetroFramework.MetroDateTimeSize.Small;
            this.pickerNgayLuuBackup.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.pickerNgayLuuBackup.Location = new System.Drawing.Point(100, 35);
            this.pickerNgayLuuBackup.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.pickerNgayLuuBackup.MinimumSize = new System.Drawing.Size(0, 25);
            this.pickerNgayLuuBackup.Name = "pickerNgayLuuBackup";
            this.pickerNgayLuuBackup.Size = new System.Drawing.Size(300, 25);
            this.pickerNgayLuuBackup.TabIndex = 10;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel4
            // 
            this.metroLabel4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel4.IsFocus = false;
            this.metroLabel4.Location = new System.Drawing.Point(19, 72);
            this.metroLabel4.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(71, 15);
            this.metroLabel4.TabIndex = 0;
            this.metroLabel4.Text = "Đường dẫn:";
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // MetroBackup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel3);
            this.Name = "MetroBackup";
            this.Size = new System.Drawing.Size(400, 200);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private MetroControl.Components.MetroLabel metroLabel2;
        private MetroControl.Components.MetroLabel metroLabel1;
        private MetroControl.Components.MetroTextbox txtTenTapTinBackup;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private MetroControl.Components.MetroButton btnHuyBackup;
        private MetroControl.Components.MetroButton btnSaoLuuBackup;
        private MetroControl.Components.MetroDatePicker pickerNgayLuuBackup;
        private MetroControl.Components.MetroTextbox txtDuongDanLuu;
        private MetroControl.Components.MetroLabel metroLabel4;
    }
}
