﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroControl;
using MetroDataset;
using System.Data.SqlClient;

namespace MetroUserControl.ContentMenu.Systems
{
    public partial class MetroListSystem : MetroBubble
    {
        public MetroListSystem()
        {
            InitializeComponent();
            btnSaoLuuSystem.Click += (s, e) => BackupData();
            btnPhucHoiSystem.Click += (s, e) =>
            {
                EventHelper.Helper.MessageInfomationYesNo("Bạn chắc chắn phục hồi lai dữ liệu ?", () => { EventHelper.Helper.TryCatch(RetoreData); });

            };
            this.grid.SetOnClick(() => { (this as MetroBubble).ReloadData(this.grid.grid, false); });
            this.ReloadData(this.grid.grid, false);
            this.VisibleChanged += MetroListScore_VisibleChanged;
            this.Load += MetroListSystem_Load;
        }

        private void MetroListScore_VisibleChanged(object sender, EventArgs e)
        {
            this.SettingLoadByPageFilterSearch(this.grid, (new LoadByPage()).HienThiTTBackup, null, null, this.picSearch, this.txtTimKiem);

        }

        private void MetroListSystem_Load(object sender, EventArgs e)
        {
            this.btnPhucHoiSystem.TypeData.Add(MetroControl.Components.Type.Master);
            this.btnSaoLuuSystem.TypeData.Add(MetroControl.Components.Type.Master);
            this.VisibleByPermission();
            this.grid.SetOnClick(() => { (this as MetroBubble).ReloadData(this.grid.grid, false); });

        }

        private void RetoreData()
        {
            object p = EventHelper.Helper.TryCatchReturnValue(() => { return grid.grid.CurrentRow.Cells[5].Value.ToString(); });
            if (p == null)
            {
                EventHelper.Helper.MessageInfomation("Vui lòng chọn tệp tin cần phục hồi!");
            }
            else
            {
                if (grid.grid.CurrentRow == null)
                    return;

                string path = (string)p;
                bool r = FullRestore(path);
                if (r == true)
                {
                    EventHelper.Helper.MessageInfomation("Khôi phục dữ liệu hoàn tất !");
                }
                else
                {
                    EventHelper.Helper.MessageInfomation("Không thể khôi phục dữ liệu !");
                }

            }
        }

        //private bool RestoreByPath(string path)
        //{

        //    bool l = EventHelper.Helper.TryCatch(() =>
        //    {
        //        SqlConnection conn = new SqlConnection(Helper.GetStringConnection());
        //        String databasename = conn.Database;
        //        conn.Open();
        //        string UseMaster = "USE master";
        //        SqlCommand UseMasterCommand = new SqlCommand(UseMaster, conn);
        //        UseMasterCommand.ExecuteNonQuery();

        //        string Alter1 = @"ALTER DATABASE " + databasename + " SET Single_User WITH Rollback Immediate";
        //        SqlCommand Alter1Cmd = new SqlCommand(Alter1, conn);
        //        Alter1Cmd.ExecuteNonQuery();

        //        string Restore = string.Format("Restore database " + databasename + " from disk='{0}'", path);
        //        SqlCommand RestoreCmd = new SqlCommand(Restore, conn);
        //        RestoreCmd.ExecuteNonQuery();


        //        conn.Close();
        //    });
        //    return l;
        //}

        private void BackupData()
        {
            MetroHelper.OpenNewTab(this, new MetroBackup(), new Size(600, 400), false);
        }

        private bool FullRestore(string path)
        {
            SqlConnection conn = new SqlConnection(Helper.GetStringConnection());
            String databasename = conn.Database;
            try
            {

                if (conn.State != ConnectionState.Open)
                    conn.Open();
                string useMaster = "USE MASTER RESTORE FILELISTONLY " + "FROM DISK = '" + path + "'";
                string single = @"ALTER DATABASE " + databasename + " SET SINGLE_USER WITH ROLLBACK IMMEDIATE";
                string restore = "RESTORE DATABASE " + databasename + " FROM DISK = '" + path + "'";
                string mutil = @"ALTER DATABASE " + databasename + " SET MULTI_USER";

                SqlCommand usecmd = new SqlCommand(useMaster, conn);
                SqlCommand singlecmd = new SqlCommand(single, conn);
                SqlCommand restoreCmd = new SqlCommand(restore, conn);
                SqlCommand mutilcmd = new SqlCommand(mutil, conn);

                usecmd.ExecuteNonQuery();
                singlecmd.ExecuteNonQuery();
                restoreCmd.ExecuteNonQuery();
                mutilcmd.ExecuteNonQuery();
                conn.Close();
                return true;
            }
            catch (Exception ex)
            {
                string mutil = @"ALTER DATABASE " + databasename + " SET MULTI_USER";
                SqlCommand mutilcmd = new SqlCommand(mutil, conn);
                mutilcmd.ExecuteNonQuery();
                conn.Close();
                MessageBox.Show(ex.Message);
                return false;
            }

        }

    }
}
