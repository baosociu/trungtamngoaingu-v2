﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroControl;
using MetroDataset;
using System.Data.SqlClient;

namespace MetroUserControl.ContentMenu.Systems
{
    public partial class MetroBackup : MetroBubble
    {
        const String KIEUBACKUP = "FULL BACKUP";
        string default_path = Application.StartupPath + @"\Backkup\";
        SaveFileDialog saveFileDialog;
        TT_BACKUP backup;


        public MetroBackup()
        {
            InitializeComponent();
            btnHuyBackup.Click += (s, e) => HuyBackup();
            btnSaoLuuBackup.Click += (s, e) =>
            {
                EventHelper.Helper.TryCatch(() =>
                {
                    BackUpData();
                });
            };
            settingBrowse();
        }


        //DATABASE COMPLETE
        //DATABASE DIFFERENTIAL
        //TRANSACTION LOG


        private void settingBrowse()
        {
            saveFileDialog = new SaveFileDialog();
            saveFileDialog.Title = "Chọn đường dẫn lưu tập tin";
            saveFileDialog.DefaultExt = "bak";
            saveFileDialog.Filter = "Full Back-up file (*.bak)|*.bak";
            saveFileDialog.RestoreDirectory = true;
            saveFileDialog.InitialDirectory = default_path;
            saveFileDialog.CheckPathExists = true;
            txtDuongDanLuu.Text = default_path;
            txtDuongDanLuu.Click += TxtDuongDanLuu_Click;
            EventHelper.Helper.SetHandHover(txtDuongDanLuu);
        }

        private void TxtDuongDanLuu_Click(object sender, EventArgs e)
        {
            saveFileDialog.FileName = txtTenTapTinBackup.Text;
            DialogResult r = saveFileDialog.ShowDialog();
            if (r == DialogResult.OK)
            {
                if (saveFileDialog.FileName.Contains("\\") && !saveFileDialog.FileName.EndsWith("\\"))
                {
                    txtTenTapTinBackup.Text = saveFileDialog.FileName.Substring(saveFileDialog.FileName.LastIndexOf("\\") + 1);
                    saveFileDialog.FileName = saveFileDialog.FileName.Substring(0, saveFileDialog.FileName.LastIndexOf("\\") + 1);
                }
                txtDuongDanLuu.Text = saveFileDialog.FileName;
                saveFileDialog.FileName = "";
            }
        }

        private void BackUpData()
        {
            if (MetroHelper.staff == null) //lấy nhân viên hiện tại
                return;

            if (MetroHelper.IsNullOrEmpty(new object[] { txtDuongDanLuu.Text, txtTenTapTinBackup.Text }))
            {
                EventHelper.Helper.MessageLessInformation();
                return;
            }
            // Thêm thông tin sao lưu vào bảng TT_BACKUP
            string MABACKUP = (new Select()).TaoMaTTBackUpTuDong(MetroHelper.staff.MANHANVIEN);
            string TENTAPTIN = txtTenTapTinBackup.Text;
            string DUONGDAN = txtDuongDanLuu.Text;
            DateTime NGAYLUU = pickerNgayLuuBackup.Value;

            string path = DUONGDAN + TENTAPTIN.Substring(0, TENTAPTIN.LastIndexOf("."));

            backup = new TT_BACKUP();
            backup.MABACKUP = MABACKUP;
            backup.TENTAPTIN = TENTAPTIN;
            backup.NGAYLUU = NGAYLUU;
            backup.MANHANVIEN = MetroHelper.staff.MANHANVIEN;
            backup.KIEUBACKUP = KIEUBACKUP;
            backup.DUONGDAN = path;

            ResultExcuteQuery result = (new Insert()).ThemTTBackup(backup);


            //Tiến hành sao lưu
            if (result.isSuccessfully())
            {
                
                DataSetQuanLyHocVienTableAdapters.QueriesTableAdapter q = new DataSetQuanLyHocVienTableAdapters.QueriesTableAdapter();
                int i = q.FullBackup(path);
                if (i != 0)
                {
                    EventHelper.Helper.MessageInfomation(result.GetMessage());
                    this.ParentForm.Close();
                }
                else
                { 
                    (new Delete()).XoaTTBackup(backup);
                    EventHelper.Helper.MessageInfomation("Sao lưu dữ liệu thất bại");
                }
            }
            else
                EventHelper.Helper.MessageInfomation(result.GetMessage());

        }

        private void HuyBackup()
        {
            (this.ParentForm).Close();
        }
        private void LogBackUp(string path)
        {
            SqlConnection conn = new SqlConnection(Helper.GetStringConnection());
            String databasename = conn.Database;
            conn.Open();
            string str = "USE" + databasename;
            string str1 = "BACKUP DATABASE " + databasename + " TO DISK =" + path + " WITH FORMAT;";
            SqlCommand cmd1 = new SqlCommand(str, conn);
            SqlCommand cmd2 = new SqlCommand(str1, conn);
            cmd1.ExecuteNonQuery();
            cmd2.ExecuteNonQuery();
            MessageBox.Show("Successd!");
            conn.Close();
        }
        private void FullBackUp(string path)
        {
            SqlConnection conn = new SqlConnection(Helper.GetStringConnection());
            String databasename = conn.Database;
            try
            {
                string command = @"BACKUP DATABASE " + databasename + " TO DISK='" + path + ".bak'";
                SqlCommand oCommand = null;

                if (conn.State != ConnectionState.Open)
                    conn.Open();
                oCommand = new SqlCommand(command, conn);
                oCommand.ExecuteNonQuery();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
