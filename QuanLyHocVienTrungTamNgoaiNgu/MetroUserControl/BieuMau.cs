﻿using Export;
using MetroControl;
using MetroDataset;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MetroUserControl
{
    public class BieuMau
    {

        #region BIỂU MẪU
        public static string BIEUMAU_CHONGIOHOC = "BM0001";
        public static string BIEUMAU_BIENLAIHOCPHI = "BM0002";
        public static string BIEUMAU_DIEMDANHHOCVIEN = "BM0003";
        public static string BIEUMAU_HOCVIENLOPHOC = "BM0004";
        public static string BIEUMAU_LICHHOCLOPHOC = "BM0005";
        public static string BIEUMAU_DOANHTHUTHEOHOCVIEN = "BM0006";
        public static string BIEUMAU_DOANHTHUTHEOLOPHOC = "BM0007";
        public static string BIEUMAU_DOANHTHUTHEOKHOAHOC = "BM0008";
        public static string BIEUMAU_DOANHTHUTHEOCOSO = "BM0009";
        public static string BIEUMAU_DOANHTHUTRUNGTAM = "BM0010";
        public static string BIEUMAU_TONGDOANHTHULOPHOC = "BM0011";
        public static string BIEUMAU_TONGDOANHTHUKHOAHOC = "BM0012";
        public static string BIEUMAU_TONGDOANHTHUCOSO = "BM0013";
        public static string BIEUMAU_TONGDOANHTHUTRUNGTAM = "BM0014";
        public static string BIEUMAU_DANHSACHDIEMHOCVIEN = "BM0015";
        public static string BIEUMAU_DANHSACHLICHDAY = "BM0016";
        public static string BIEUMAU_DANHSACHLICHHOC = "BM0017";
        public static string BIEUMAU_DANHSACHDIEM = "BM0018";
        // Tập tin Helper
        public static string BIEUMAU_HELPER_THONGTINCANHAN = "HP0005";
        public static string BIEUMAU_HELPER_QUANLYNGUOIDUNG = "HP0009";
        public static string BIEUMAU_HELPER_QUANLYBIENLAI = "HP0001";
        public static string BIEUMAU_HELPER_QUANLYLOPHOC = "HP0010";
        public static string BIEUMAU_HELPER_QUANLYLICH = "HP0011";
        public static string BIEUMAU_HELPER_QUANLYDIEM = "HP0002";
        public static string BIEUMAU_HELPER_QUANLYHETHONG = "HP0013";
        public static string BIEUMAU_HELPER_QUANLYTAIKHOAN = "HP0007";
        public static string BIEUMAU_HELPER_QUANLYPHANQUYEN = "HP0008";
        public static string BIEUMAU_HELPER_QUANLYTHONGKE = "HP0006";
        public static string BIEUMAU_HELPER_XEMLICHHOC = "HP0003";
        public static string BIEUMAU_HELPER_XEMLICHDAY = "HP0004";
        public static string BIEUMAU_HELPER_XEMKETQUAHOCTAP = "HP0012";
        public static string BIEUMAU_HELPER_QUANLYDIEMDANH = "HP0014";


        #endregion

        private static bool result(object r)
        {
            if (r == null)
                return false;
            return (bool)r;
        }

        //MẪU 1
        public static bool InChonGioHocHocVien(string maphieu)
        {
            object r = null;
            EventHelper.Helper.MessageInfomationYesNo("Đồng ý in phiếu " + maphieu + " ?", () =>
            {

                r = EventHelper.Helper.TryCatchReturnValue(() =>
                {
                    Select s = new MetroDataset.Select();
                    TT_DANGKYKHOAHOC dk = s.LayTTDangKhoaHoc(maphieu);
                    HOCVIEN hv = s.GetStudentByMaDangKy(maphieu);
                    NHANVIEN nv = s.GetStaffByID(MetroHelper.iduser);
                    List<int[]> lich = s.LayThongTinChonGioHocVien(maphieu);

                    //biểu mẫu từ csdl
                    BIEUMAU bm = (new Select()).LayBieuMau(BIEUMAU_CHONGIOHOC);
                    WordExport w = new WordExport(Application.StartupPath, bm.MABIEUMAU + "_" + bm.TENBIEUMAU, bm.MABIEUMAU + "_" + maphieu, true);
                    w.Export(bm.TAPTIN.ToArray(), () => w.PhieuDangKy(hv.MAHOCVIEN, hv.TENHOCVIEN, hv.NGAYSINH.Value.ToShortDateString(), hv.GIOITINH, hv.CMND, hv.SODIENTHOAI, dk.MADANGKYKHOAHOC, dk.MAKHOAHOC, dk.langhidanh.ToString(), dk.DIEMDAUVAO.ToString(), lich, nv.TENNHANVIEN));

                    return true;
                });
            });

            return result(r);
        }

        //MẪU 2
        public static bool InBienLaiThuHocPhi(string mabienlai)
        {
            object r = null;
            EventHelper.Helper.MessageInfomationYesNo("Đồng ý in biên lai " + mabienlai + " ?", () =>
            {

                r = EventHelper.Helper.TryCatchReturnValue(() =>
                {
                    BIENLAI bl = (new Select()).LayBienLai(mabienlai);
                    HOCVIEN hv = (new Select()).GetStudentByMaDangKy(bl.MADANGKYKHOAHOC);
                    KHOAHOC kh = (new Select()).LayKhoaHocByMaDangKy(bl.MADANGKYKHOAHOC);
                    BIEUMAU bm = (new Select()).LayBieuMau(BIEUMAU_BIENLAIHOCPHI);
                    using (WordExport w = new WordExport(Application.StartupPath, bm.MABIEUMAU + "_" + bm.TENBIEUMAU, bm.MABIEUMAU + "_" + bl.MABIENLAI, true))
                    {
                        w.Export(bm.TAPTIN.ToArray(), () => w.BienLai(bm.MABIEUMAU, bl.MABIENLAI, bl.MADANGKYKHOAHOC, bl.MALOPHOC, hv.MAHOCVIEN, hv.TENHOCVIEN, kh.TENKHOAHOC, bl.SOTIEN.ToString(), bl.LOAI_BIENLAI.TENLOAIBIENLAI, bl.DOT.ToString(), MetroHelper.staff.TENNHANVIEN));

                    }
                    return true;
                });
            });

            return result(r);
        }

        //MẪU 3
        public static bool InDanhSachDiemDanh(string malop, string malich)
        {
            object r = null;
            EventHelper.Helper.MessageInfomationYesNo("Đồng ý in danh sách kết quả điểm danh này ?", () =>
            {
                r = EventHelper.Helper.TryCatchReturnValue(() =>
                {
                    LICHHOC lich = (new Select()).LayLichByMaLopMaLich(malop, malich);
                    KHOAHOC k = (new Select()).LayKhoaHocByMaLop(malop);
                    if (k == null || lich == null || MetroHelper.iduser == null)
                        return false;

                    string tennguoilap = "";
                    if (MetroHelper.staff != null)
                        tennguoilap = MetroHelper.staff.TENNHANVIEN;
                    else if (MetroHelper.student != null)
                        tennguoilap = MetroHelper.student.TENHOCVIEN;
                    else if (MetroHelper.teacher != null)
                        tennguoilap = MetroHelper.teacher.TENGIANGVIEN;

                    BIEUMAU bm = (new Select()).LayBieuMau(BIEUMAU_DIEMDANHHOCVIEN);
                    using (ExcelExport e = new ExcelExport(Application.StartupPath, bm.MABIEUMAU, true))
                    {
                        DataSetQuanLyHocVienTableAdapters.HienThiCTBuoiHocLichHocTableAdapter
                        dt = new DataSetQuanLyHocVienTableAdapters.HienThiCTBuoiHocLichHocTableAdapter();
                        DataSetQuanLyHocVien.HienThiCTBuoiHocLichHocDataTable tb = dt.GetData(malich, malop, -1, -1);
                        List<object> data = new List<object>();
                        for (int i = 0; i < tb.Rows.Count; i++)
                            data.Add(new
                            {
                                STT = (i + 1).ToString(),
                                mahocvien = tb.Rows[i][0].ToString(),
                                tenhocvien = tb.Rows[i][1].ToString() + " " + tb.Rows[i][2].ToString(),
                                ngaysinh = tb[i][3].ToString(),
                                diemdanh = bool.Parse(tb.Rows[i][6].ToString()) ? "CÓ" : "VẮNG"
                            });
                        e.DanhSachDiemDanhLopHoc(bm.TAPTIN.ToArray(), data, malop, k.TENKHOAHOC, tennguoilap, data.Count.ToString(), lich.NGAYHOC.ToShortDateString());
                    }
                    return true;
                });
            });
            return result(r);
        }

        //MẪU 4
        public static bool InDanhSachHocVien(string malop)
        {
            object r = null;
            EventHelper.Helper.MessageInfomationYesNo("Đồng ý in danh sách học viên lớp " + malop + " ?", () =>
                {
                    r = EventHelper.Helper.TryCatchReturnValue(() =>
                    {
                        KHOAHOC k = (new Select()).LayKhoaHocByMaLop(malop);
                        if (k == null || MetroHelper.iduser == null)
                            return false;

                        string tennguoilap = "";
                        if (MetroHelper.staff != null)
                            tennguoilap = MetroHelper.staff.TENNHANVIEN;
                        else if (MetroHelper.student != null)
                            tennguoilap = MetroHelper.student.TENHOCVIEN;
                        else if (MetroHelper.teacher != null)
                            tennguoilap = MetroHelper.teacher.TENGIANGVIEN;

                        BIEUMAU bm = (new Select()).LayBieuMau(BIEUMAU_HOCVIENLOPHOC);

                        using (ExcelExport e = new ExcelExport(Application.StartupPath, bm.MABIEUMAU, true))
                        {
                            List<object> data = (new Select()).HienThiHocVienLopHoc_Template(malop);
                            e.DanhSachHocVienLopHoc(bm.TAPTIN.ToArray(), data, malop, k.TENKHOAHOC, tennguoilap, data.Count.ToString());
                        }
                        return true;
                    });
                });
            return result(r);
        }

        //MẪU 5
        public static bool InDanhSachLichHoc(string malop)
        {
            object r = null;
            EventHelper.Helper.MessageInfomationYesNo("Đồng ý in danh sách lịch học lớp " + malop + " ?", () =>
            {
                r = EventHelper.Helper.TryCatchReturnValue(() =>
                {
                    KHOAHOC k = (new Select()).LayKhoaHocByMaLop(malop);
                    if (k == null || MetroHelper.iduser == null)
                        return false;

                    string tennguoilap = "";
                    if (MetroHelper.staff != null)
                        tennguoilap = MetroHelper.staff.TENNHANVIEN;
                    else if (MetroHelper.student != null)
                        tennguoilap = MetroHelper.student.TENHOCVIEN;
                    else if (MetroHelper.teacher != null)
                        tennguoilap = MetroHelper.teacher.TENGIANGVIEN;

                    BIEUMAU bm = (new Select()).LayBieuMau(BIEUMAU_LICHHOCLOPHOC);

                    using (ExcelExport e = new ExcelExport(Application.StartupPath, bm.MABIEUMAU, true))
                    {
                        List<object> data = (new Select()).HienThiLichHocLopHoc_Template(malop);
                        e.DanhSachLichHocLopHoc(bm.TAPTIN.ToArray(), data, malop, k.TENKHOAHOC, tennguoilap, data.Count.ToString());
                    }
                    return true;
                });
            });
            return result(r);
        }

        //MẪU 6 Doanh thu theo học viên
        public static bool InDoanhThuTheoHocVien(string mahocvien, DateTime from, DateTime to)
        {
            object r = null;
            EventHelper.Helper.MessageInfomationYesNo("Đồng ý in doanh thu của học viên " + mahocvien + " ?", () =>
            {
                r = EventHelper.Helper.TryCatchReturnValue(() =>
                {
                    HOCVIEN k = (new Select()).GetStudentByID(mahocvien); //thông tin thêm
                    if (k == null || MetroHelper.iduser == null)
                        return false;

                    string tennguoilap = "";
                    if (MetroHelper.staff != null)
                        tennguoilap = MetroHelper.staff.TENNHANVIEN;
                    else if (MetroHelper.student != null)
                        tennguoilap = MetroHelper.student.TENHOCVIEN;
                    else if (MetroHelper.teacher != null)
                        tennguoilap = MetroHelper.teacher.TENGIANGVIEN;

                    BIEUMAU bm = (new Select()).LayBieuMau(BIEUMAU_DOANHTHUTHEOHOCVIEN); //lấy biểu mẫu

                    using (ExcelExport e = new ExcelExport(Application.StartupPath, bm.MABIEUMAU, true))
                    {
                        List<object> data = (List<object>)(new LoadByPage()).DoanhThuTheoHocVien_Grid(mahocvien, from, to, -1, -1); //dữ liệu
                        e.DoanhThuTheoHocVien(bm.TAPTIN.ToArray(), data, mahocvien, k.TENHOCVIEN, from.ToShortDateString(), to.ToShortDateString(), tennguoilap); //export
                    }
                    return true;
                });
            });
            return result(r);
        }


        //MẪU 7 Doanh thu theo lớp học 
        public static bool InDoanhThuTheoLopHoc(string malophoc, DateTime from, DateTime to)
        {
            object r = null;
            EventHelper.Helper.MessageInfomationYesNo("Đồng ý in doanh thu của lớp học " + malophoc + " ?", () =>
            {
                r = EventHelper.Helper.TryCatchReturnValue(() =>
                {
                    LOPHOC k = (new Select()).GetClassByID(malophoc); //thông tin thêm
                    if (k == null || MetroHelper.iduser == null)
                        return false;

                    string tennguoilap = "";
                    if (MetroHelper.staff != null)
                        tennguoilap = MetroHelper.staff.TENNHANVIEN;
                    else if (MetroHelper.student != null)
                        tennguoilap = MetroHelper.student.TENHOCVIEN;
                    else if (MetroHelper.teacher != null)
                        tennguoilap = MetroHelper.teacher.TENGIANGVIEN;

                    BIEUMAU bm = (new Select()).LayBieuMau(BIEUMAU_DOANHTHUTHEOLOPHOC); //lấy biểu mẫu

                    using (ExcelExport e = new ExcelExport(Application.StartupPath, bm.MABIEUMAU, true))
                    {
                        List<object> data = (List<object>)(new LoadByPage()).DoanhThuTheoLopHoc_Grid(malophoc, from, to, -1, -1); //dữ liệu
                        e.DoanhThuTheoLopHoc(bm.TAPTIN.ToArray(), data, malophoc, k.KHOAHOC.TENKHOAHOC, from.ToShortDateString(), to.ToShortDateString(), tennguoilap); //export
                    }
                    return true;
                });
            });
            return result(r);
        }

        //MẪU 8 Doanh thu theo khoá học 
        public static bool InDoanhThuTheoKhoaHoc(string makhoahoc, DateTime from, DateTime to)
        {
            object r = null;
            EventHelper.Helper.MessageInfomationYesNo("Đồng ý in doanh thu của khoá học " + makhoahoc + " ?", () =>
            {
                r = EventHelper.Helper.TryCatchReturnValue(() =>
                {
                    KHOAHOC k = (new Select()).GetCourseByID(makhoahoc); //thông tin thêm
                    if (k == null || MetroHelper.iduser == null)
                        return false;

                    string tennguoilap = "";
                    if (MetroHelper.staff != null)
                        tennguoilap = MetroHelper.staff.TENNHANVIEN;
                    else if (MetroHelper.student != null)
                        tennguoilap = MetroHelper.student.TENHOCVIEN;
                    else if (MetroHelper.teacher != null)
                        tennguoilap = MetroHelper.teacher.TENGIANGVIEN;

                    BIEUMAU bm = (new Select()).LayBieuMau(BIEUMAU_DOANHTHUTHEOKHOAHOC); //lấy biểu mẫu

                    using (ExcelExport e = new ExcelExport(Application.StartupPath, bm.MABIEUMAU, true))
                    {
                        List<object> data = (List<object>)(new LoadByPage()).DoanhThuTheoKhoaHoc_Grid(makhoahoc, from, to, -1, -1); //dữ liệu
                        e.DoanhThuTheoKhoaHoc(bm.TAPTIN.ToArray(), data, makhoahoc, k.TENKHOAHOC, from.ToShortDateString(), to.ToShortDateString(), tennguoilap); //export
                    }
                    return true;
                });
            });
            return result(r);
        }


        //MẪU 9 Doanh thu theo cơso 
        public static bool InDoanhThuTheoCoSo(string macoso, DateTime from, DateTime to)
        {
            object r = null;
            EventHelper.Helper.MessageInfomationYesNo("Đồng ý in doanh thu của cơ sở " + macoso + " ?", () =>
            {
                r = EventHelper.Helper.TryCatchReturnValue(() =>
                {
                    COSO k = (new Select()).GetBranchByID(macoso); //thông tin thêm
                    if (k == null || MetroHelper.iduser == null)
                        return false;

                    string tennguoilap = "";
                    if (MetroHelper.staff != null)
                        tennguoilap = MetroHelper.staff.TENNHANVIEN;
                    else if (MetroHelper.student != null)
                        tennguoilap = MetroHelper.student.TENHOCVIEN;
                    else if (MetroHelper.teacher != null)
                        tennguoilap = MetroHelper.teacher.TENGIANGVIEN;

                    BIEUMAU bm = (new Select()).LayBieuMau(BIEUMAU_DOANHTHUTHEOCOSO); //lấy biểu mẫu

                    using (ExcelExport e = new ExcelExport(Application.StartupPath, bm.MABIEUMAU, true))
                    {
                        List<object> data = (List<object>)(new LoadByPage()).DoanhThuTheoCoSo_Grid(macoso, from, to, -1, -1); //dữ liệu
                        e.DoanhThuTheoCoSo(bm.TAPTIN.ToArray(), data, macoso, k.TENCOSO, from.ToShortDateString(), to.ToShortDateString(), tennguoilap); //export
                    }
                    return true;
                });
            });
            return result(r);
        }


        //MẪU 9 Doanh thu của trung tâm
        public static bool InDoanhThuTrungTam(DateTime from, DateTime to)
        {
            object r = null;
            EventHelper.Helper.MessageInfomationYesNo("Đồng ý in doanh thu của trung tâm ?", () =>
            {
                r = EventHelper.Helper.TryCatchReturnValue(() =>
                {
                    if (MetroHelper.iduser == null)
                        return false;

                    string tennguoilap = "";
                    if (MetroHelper.staff != null)
                        tennguoilap = MetroHelper.staff.TENNHANVIEN;
                    else if (MetroHelper.student != null)
                        tennguoilap = MetroHelper.student.TENHOCVIEN;
                    else if (MetroHelper.teacher != null)
                        tennguoilap = MetroHelper.teacher.TENGIANGVIEN;

                    BIEUMAU bm = (new Select()).LayBieuMau(BIEUMAU_DOANHTHUTRUNGTAM); //lấy biểu mẫu

                    using (ExcelExport e = new ExcelExport(Application.StartupPath, bm.MABIEUMAU, true))
                    {
                        List<object> data = (List<object>)(new LoadByPage()).DoanhThuTheoTrungTam_Grid("", from, to, -1, -1); //dữ liệu
                        e.DoanhThuTrungTam(bm.TAPTIN.ToArray(), data, from.ToShortDateString(), to.ToShortDateString(), tennguoilap); //export
                    }
                    return true;
                });
            });
            return result(r);
        }


        //MẪU 11 tong Doanh thu theo lop 
        public static bool InTongDoanhThuTheoLop(string malop, DateTime from, DateTime to)
        {
            object r = null;
            EventHelper.Helper.MessageInfomationYesNo("Đồng ý in tổng doanh thu của lớp học " + malop + " ?", () =>
            {
                r = EventHelper.Helper.TryCatchReturnValue(() =>
                {
                    LOPHOC k = (new Select()).GetClassByID(malop); //thông tin thêm
                    if (k == null || MetroHelper.iduser == null)
                        return false;

                    string tennguoilap = "";
                    if (MetroHelper.staff != null)
                        tennguoilap = MetroHelper.staff.TENNHANVIEN;
                    else if (MetroHelper.student != null)
                        tennguoilap = MetroHelper.student.TENHOCVIEN;
                    else if (MetroHelper.teacher != null)
                        tennguoilap = MetroHelper.teacher.TENGIANGVIEN;

                    BIEUMAU bm = (new Select()).LayBieuMau(BIEUMAU_TONGDOANHTHULOPHOC); //lấy biểu mẫu

                    using (ExcelExport e = new ExcelExport(Application.StartupPath, bm.MABIEUMAU, true))
                    {
                        List<object> data = (List<object>)(new LoadByPage()).TongDoanhThuTheoLopHoc_Grid(malop, from, to, -1, -1); //dữ liệu
                        e.TongDoanhThuLopHoc(bm.TAPTIN.ToArray(), data, malop, k.KHOAHOC.TENKHOAHOC, from.ToShortDateString(), to.ToShortDateString(), tennguoilap); //export
                    }
                    return true;
                });
            });
            return result(r);
        }


        //MẪU 12 tong Doanh thu theo khoa hoc 
        public static bool InTongDoanhThuTheoKhoaHoc(string makhoahoc, DateTime from, DateTime to)
        {
            object r = null;
            EventHelper.Helper.MessageInfomationYesNo("Đồng ý in tổng doanh thu của khoá học " + makhoahoc + " ?", () =>
            {
                r = EventHelper.Helper.TryCatchReturnValue(() =>
                {
                    KHOAHOC k = (new Select()).GetCourseByID(makhoahoc); //thông tin thêm
                    if (k == null || MetroHelper.iduser == null)
                        return false;

                    string tennguoilap = "";
                    if (MetroHelper.staff != null)
                        tennguoilap = MetroHelper.staff.TENNHANVIEN;
                    else if (MetroHelper.student != null)
                        tennguoilap = MetroHelper.student.TENHOCVIEN;
                    else if (MetroHelper.teacher != null)
                        tennguoilap = MetroHelper.teacher.TENGIANGVIEN;

                    BIEUMAU bm = (new Select()).LayBieuMau(BIEUMAU_TONGDOANHTHUKHOAHOC); //lấy biểu mẫu

                    using (ExcelExport e = new ExcelExport(Application.StartupPath, bm.MABIEUMAU, true))
                    {
                        List<object> data = (List<object>)(new LoadByPage()).TongDoanhThuTheoKhoaHoc_Grid(makhoahoc, from, to, -1, -1); //dữ liệu
                        e.TongDoanhThuKhoaHoc(bm.TAPTIN.ToArray(), data, makhoahoc, k.TENKHOAHOC, from.ToShortDateString(), to.ToShortDateString(), tennguoilap); //export
                    }
                    return true;
                });
            });
            return result(r);
        }


        //MẪU 13 tong Doanh thu theo co so
        public static bool InTongDoanhThuTheoCoSo(string macoso, DateTime from, DateTime to)
        {
            object r = null;
            EventHelper.Helper.MessageInfomationYesNo("Đồng ý in tổng doanh thu của cơ sở " + macoso + " ?", () =>
            {
                r = EventHelper.Helper.TryCatchReturnValue(() =>
                {
                    COSO k = (new Select()).GetBranchByID(macoso); //thông tin thêm
                    if (k == null || MetroHelper.iduser == null)
                        return false;

                    string tennguoilap = "";
                    if (MetroHelper.staff != null)
                        tennguoilap = MetroHelper.staff.TENNHANVIEN;
                    else if (MetroHelper.student != null)
                        tennguoilap = MetroHelper.student.TENHOCVIEN;
                    else if (MetroHelper.teacher != null)
                        tennguoilap = MetroHelper.teacher.TENGIANGVIEN;

                    BIEUMAU bm = (new Select()).LayBieuMau(BIEUMAU_TONGDOANHTHUCOSO); //lấy biểu mẫu

                    using (ExcelExport e = new ExcelExport(Application.StartupPath, bm.MABIEUMAU, true))
                    {
                        List<object> data = (List<object>)(new LoadByPage()).TongDoanhThuTheoCoSo_Grid(macoso, from, to, -1, -1); //dữ liệu
                        e.TongDoanhThuCoSo(bm.TAPTIN.ToArray(), data, macoso, k.TENCOSO, from.ToShortDateString(), to.ToShortDateString(), tennguoilap); //export
                    }
                    return true;
                });
            });
            return result(r);
        }


        //MẪU 14 tong Doanh thu trung tâm
        public static bool InTongDoanhThuTrungTam(DateTime from, DateTime to)
        {
            object r = null;
            EventHelper.Helper.MessageInfomationYesNo("Đồng ý in tổng doanh thu của trung tâm ?", () =>
            {
                r = EventHelper.Helper.TryCatchReturnValue(() =>
                {
                    if (MetroHelper.iduser == null)
                        return false;

                    string tennguoilap = "";
                    if (MetroHelper.staff != null)
                        tennguoilap = MetroHelper.staff.TENNHANVIEN;
                    else if (MetroHelper.student != null)
                        tennguoilap = MetroHelper.student.TENHOCVIEN;
                    else if (MetroHelper.teacher != null)
                        tennguoilap = MetroHelper.teacher.TENGIANGVIEN;

                    BIEUMAU bm = (new Select()).LayBieuMau(BIEUMAU_TONGDOANHTHUTRUNGTAM); //lấy biểu mẫu

                    using (ExcelExport e = new ExcelExport(Application.StartupPath, bm.MABIEUMAU, true))
                    {
                        List<object> data = (List<object>)(new LoadByPage()).TongDoanhThuTheoTrungTam_Grid("", from, to, -1, -1); //dữ liệu
                        e.TongDoanhThuTrungTam(bm.TAPTIN.ToArray(), data, from.ToShortDateString(), to.ToShortDateString(), tennguoilap); //export
                    }
                    return true;
                });
            });
            return result(r);
        }
        //XUẤT FILE HELPER THEO TỪNG MÀN HÌNH CHỨC NĂNG
        public static bool XuatFileWordHelper(string mabieumau)
        {
            object r = null;
            //EventHelper.Helper.MessageInfomationYesNo("Xem trợ giúp ?", () =>
            //{

            r = EventHelper.Helper.TryCatchReturnValue(() =>
            {
                    //biểu mẫu từ csdl
                    BIEUMAU bm = (new Select()).LayBieuMau(mabieumau);
                WordExport w = new WordExport(Application.StartupPath, bm.MABIEUMAU, bm.MABIEUMAU, true);
                w.Export(bm.TAPTIN.ToArray(), w.TroGiup);
                return true;
            });
            //});

            return result(r);
        }
        //MẪU 15
        public static bool InDanhSachTatCaDiemHocVien(int index, string filter, string search)
        {
            object r = null;
            EventHelper.Helper.MessageInfomationYesNo("Đồng ý in danh sách điểm của học viên " + MetroHelper.student.MAHOCVIEN + " ?", () =>
               {
                   r = EventHelper.Helper.TryCatchReturnValue(() =>
                   {
                     // List<object> diem = (new Select()).LayThongTinTatCaDiemCuaHocVien_Template(mahocvien);
                     DataTable diem = (DataTable)(new LoadByPage()).HienThiDiem(-1, -1, index, filter, search);


                       if (diem == null || MetroHelper.iduser == null)
                           return false;


                       List<object> list = new List<object>();
                       foreach (DataRow row in diem.Rows)
                       {
                           if (row.ItemArray[5].Equals(MetroHelper.student.MAHOCVIEN))
                           {
                               list.Add(new
                               {
                                   stt = (list.Count + 1).ToString(),
                                   madangkykhoahoc = row.ItemArray[0],
                                   tenkhoahoc = row.ItemArray[3],
                                   malophoc = row.ItemArray[2],
                                   madiem = row.ItemArray[1],
                                   tenloaidiem = row.ItemArray[4],
                                   lan = row.ItemArray[7],
                                   diem = row.ItemArray[8],
                                   mahocvien = row.ItemArray[5],
                                   tenhocvien = row.ItemArray[6]
                               });
                           }

                       }


                       BIEUMAU bm = (new Select()).LayBieuMau(BIEUMAU_DANHSACHDIEMHOCVIEN);

                       using (ExcelExport e = new ExcelExport(Application.StartupPath, bm.MABIEUMAU, true))
                       {
                           e.DanhSachDiemCuaHocVien(bm.TAPTIN.ToArray(), list, MetroHelper.student.MAHOCVIEN, MetroHelper.student.TENHOCVIEN, MetroHelper.student.TENHOCVIEN);
                       }
                       return true;
                   });
               });
            return result(r);
        }
        //MẪU 16
        public static bool InDanhSachLichHocHocVien(int index, string filter, string search)
        {
            object r = null;
            EventHelper.Helper.MessageInfomationYesNo("Đồng ý in danh sách lịch học của học viên "+ MetroHelper.student.MAHOCVIEN + "?", () =>
            {
                r = EventHelper.Helper.TryCatchReturnValue(() =>
                {
                    DataTable diem = (DataTable)(new LoadByPage()).HienThiLichHocHocVien(-1, -1, index, filter, search);


                    if (diem == null || MetroHelper.iduser == null)
                        return false;


                    List<object> list = new List<object>();
                    foreach (DataRow row in diem.Rows)
                    {
                        list.Add(new
                        {
                            stt = (list.Count + 1).ToString(),
                            malichhoc = row.ItemArray[0],
                            khoahoc = row.ItemArray[2],
                            lophoc = row.ItemArray[1],
                            phong = row.ItemArray[3],
                            coso = row.ItemArray[4],
                            thu = (new Select()).GetDayOfWeek((DateTime)row.ItemArray[5]),
                            cahoc = row.ItemArray[6],
                            giobatdau = row.ItemArray[7],
                            gioketthuc = row.ItemArray[8],
                            ngayhoc = row.ItemArray[5],
                            tinhtrang = row.ItemArray[9],
                        });
                    }


                    BIEUMAU bm = (new Select()).LayBieuMau(BIEUMAU_DANHSACHLICHHOC);

                    using (ExcelExport e = new ExcelExport(Application.StartupPath, bm.MABIEUMAU, true))
                    {
                        e.DanhSachLichHoc(bm.TAPTIN.ToArray(), list, MetroHelper.student.MAHOCVIEN,MetroHelper.student.TENHOCVIEN, MetroHelper.student.TENHOCVIEN);
                    }
                    return true;
                });
            });
            return result(r);
        }

        public static bool InDanhSachLichDayGiangVien(int index, string filter, string search)
        {
            object r = null;
            EventHelper.Helper.MessageInfomationYesNo("Đồng ý in danh sách lịch dạy của giảng viên " + MetroHelper.teacher.MAGIANGVIEN + "?", () =>
            {
                r = EventHelper.Helper.TryCatchReturnValue(() =>
                {
                    DataTable diem = (DataTable)(new LoadByPage()).HienThiLichDayGiangVien(-1, -1, index, filter, search);


                    if (diem == null || MetroHelper.iduser == null)
                        return false;


                    List<object> list = new List<object>();
                    foreach (DataRow row in diem.Rows)
                    {
                        list.Add(new
                        {
                            stt = (list.Count + 1).ToString(),
                            malichhoc = row.ItemArray[0],
                            khoahoc = row.ItemArray[2],
                            lophoc = row.ItemArray[1],
                            phong = row.ItemArray[3],
                            coso = row.ItemArray[4],
                            thu = (new Select()).GetDayOfWeek((DateTime)row.ItemArray[5]),
                            cahoc = row.ItemArray[6],
                            giobatdau = row.ItemArray[7],
                            gioketthuc = row.ItemArray[8],
                            ngayhoc = row.ItemArray[5],
                            tinhtrang = row.ItemArray[9],
                        });
                    }


                    BIEUMAU bm = (new Select()).LayBieuMau(BIEUMAU_DANHSACHLICHDAY);

                    using (ExcelExport e = new ExcelExport(Application.StartupPath, bm.MABIEUMAU, true))
                    {
                        e.DanhSachLichDay(bm.TAPTIN.ToArray(), list, MetroHelper.teacher.MAGIANGVIEN, MetroHelper.teacher.TENGIANGVIEN, MetroHelper.teacher.TENGIANGVIEN);
                    }
                    return true;
                });
            });
            return result(r);
        }

        //MẪU 18
        public static bool InDanhSachTatCaDiem(int index, string filter, string search)
        {
            object r = null;
            EventHelper.Helper.MessageInfomationYesNo("Đồng ý in danh sách điểm ?", () =>
            {
                r = EventHelper.Helper.TryCatchReturnValue(() =>
                {
                    DataTable diem = (DataTable)(new LoadByPage()).HienThiDiem(-1, -1, index, filter, search);


                    if (diem == null || MetroHelper.iduser == null)
                        return false;


                    List<object> list = new List<object>();
                    foreach (DataRow row in diem.Rows)
                    {
                        list.Add(new
                        {
                            stt = (list.Count + 1).ToString(),
                            madangkykhoahoc = row.ItemArray[0],
                            madiem = row.ItemArray[1],
                            malophoc = row.ItemArray[2],
                            tenkhoahoc = row.ItemArray[3],
                            tenloaidiem = row.ItemArray[4],
                            mahocvien = row.ItemArray[5],
                            tenhocvien = row.ItemArray[6],
                            lan = row.ItemArray[7],
                            diem = row.ItemArray[8],
                        });
                    }


                    BIEUMAU bm = (new Select()).LayBieuMau(BIEUMAU_DANHSACHDIEM);

                    using (ExcelExport e = new ExcelExport(Application.StartupPath, bm.MABIEUMAU, true))
                    {
                        e.DanhSachDiem(bm.TAPTIN.ToArray(), list, MetroHelper.staff.TENNHANVIEN);
                    }
                    return true;
                });
            });
            return result(r);
        }


    }
}
