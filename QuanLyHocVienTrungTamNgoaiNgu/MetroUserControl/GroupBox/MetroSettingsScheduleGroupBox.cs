﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroDataset.Model;

namespace MetroUserControl.GroupBox
{
    public partial class MetroSettingsScheduleGroupBox : UserControl
    {
        bool[,] arr_datasource; //6,7 cn --> thứ 7
        bool[,] orgin_datasource;
        public MetroSettingsScheduleGroupBox()
        {
            InitializeComponent();
            this.grid.CellContentClick += Grid_CellContentClick;
            this.grid.UseSelectable = false;
        }

        private void Grid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex > 0 && e.RowIndex != -1)
            {
                int col = e.ColumnIndex-1;
                int row = e.RowIndex;

                if (!orgin_datasource[row, col])
                {
                    EventHelper.Helper.MessageInfomation("Không được phép chọn thời gian này. Vì xảy ra đụng độ lịch.");
                    return;
                }
                //bool b = !bool.Parse((sender as DataGridView)[e.ColumnIndex, e.RowIndex].Value.ToString());
                arr_datasource[row, col] = !arr_datasource[row, col]; //cn

                this.grid.DataSource = GetDataSourceByArray(arr_datasource);
                this.grid.SetCurrentRow(e.RowIndex);
            }
        }

        public void SetDataSourceGrid(List<Lich> lich_dangky)
        {
            bool[,] arr_b = new bool[6, 7];
            orgin_datasource = new bool[6, 7];

            foreach (Lich l in lich_dangky)
            {
                int index_ca = l.Ca - 1;
                int index_thu = l.Thu-1;
                arr_b[index_ca, index_thu] = true;
            //lưu lại dữ liệu gốc ==> ko co người dùng chọn ca - thứ đụng độ
                orgin_datasource[index_ca, index_thu] = true;
            }

            arr_datasource = arr_b;
            List<object> _datasoucre = GetDataSourceByArray(arr_b);
            this.grid.DataSource = _datasoucre;
        }

        public List<Lich> GetListScheduleGrid()
        {
            List<Lich> output = new List<Lich>();
            for (int i = 0; i < 7; i++)
                for (int j = 0; j < 6; j++)
                    if (arr_datasource[j, i])
                        output.Add(new Lich(i,j + 1));
            return output;
        }

        private List<object> GetDataSourceByArray(bool[,] arr_b)
        {
            List<object> _datasoucre = new List<object>();
            for (int i = 0; i < 6; i++)
            {
                _datasoucre.Add(new
                {
                    Ca = (i + 1).ToString(),
                    Thứ_2 = arr_b[i, 0],
                    Thứ_3 = arr_b[i, 1],
                    Thứ_4 = arr_b[i, 2],
                    Thứ_5 = arr_b[i,3],
                    Thứ_6 = arr_b[i, 4],
                    Thứ_7 = arr_b[i, 5],
                    Chủ_nhật = arr_b[i, 6]
                });
            }
            return _datasoucre;
        }
    }
}
