﻿namespace MetroUserControl.GroupBox
{
    partial class MetroInfoScheduleGroupBox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupRight = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.txtSoLuongChoNgoi = new MetroControl.Components.MetroTextbox();
            this.metroLabel5 = new MetroControl.Components.MetroLabel();
            this.txtCoSo = new MetroControl.Components.MetroTextbox();
            this.metroLabel4 = new MetroControl.Components.MetroLabel();
            this.cbbPhongHoc = new MetroControl.Components.MetroCombobox();
            this.metroLabel3 = new MetroControl.Components.MetroLabel();
            this.txtLoaiLich = new MetroControl.Components.MetroTextbox();
            this.metroLabel1 = new MetroControl.Components.MetroLabel();
            this.txtNgayBatDau = new MetroControl.Components.MetroTextbox();
            this.metroLabel12 = new MetroControl.Components.MetroLabel();
            this.groupRight.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupRight
            // 
            this.groupRight.BackColor = System.Drawing.Color.Transparent;
            this.groupRight.Controls.Add(this.tableLayoutPanel9);
            this.groupRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupRight.Location = new System.Drawing.Point(0, 0);
            this.groupRight.Name = "groupRight";
            this.groupRight.Size = new System.Drawing.Size(571, 501);
            this.groupRight.TabIndex = 46;
            this.groupRight.TabStop = false;
            this.groupRight.Text = "Thông tin lịch học";
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.AllowDrop = true;
            this.tableLayoutPanel9.AutoScroll = true;
            this.tableLayoutPanel9.AutoSize = true;
            this.tableLayoutPanel9.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel9.ColumnCount = 4;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.33333F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel9.Controls.Add(this.txtSoLuongChoNgoi, 2, 5);
            this.tableLayoutPanel9.Controls.Add(this.metroLabel5, 1, 5);
            this.tableLayoutPanel9.Controls.Add(this.txtCoSo, 2, 4);
            this.tableLayoutPanel9.Controls.Add(this.metroLabel4, 1, 4);
            this.tableLayoutPanel9.Controls.Add(this.cbbPhongHoc, 2, 3);
            this.tableLayoutPanel9.Controls.Add(this.metroLabel3, 1, 3);
            this.tableLayoutPanel9.Controls.Add(this.txtLoaiLich, 2, 1);
            this.tableLayoutPanel9.Controls.Add(this.metroLabel1, 1, 1);
            this.tableLayoutPanel9.Controls.Add(this.txtNgayBatDau, 2, 2);
            this.tableLayoutPanel9.Controls.Add(this.metroLabel12, 1, 2);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 7;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(565, 482);
            this.tableLayoutPanel9.TabIndex = 10;
            // 
            // txtSoLuongChoNgoi
            // 
            this.txtSoLuongChoNgoi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSoLuongChoNgoi.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtSoLuongChoNgoi.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtSoLuongChoNgoi.Location = new System.Drawing.Point(147, 293);
            this.txtSoLuongChoNgoi.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtSoLuongChoNgoi.Name = "txtSoLuongChoNgoi";
            this.txtSoLuongChoNgoi.ReadOnly = true;
            this.txtSoLuongChoNgoi.Size = new System.Drawing.Size(379, 23);
            this.txtSoLuongChoNgoi.TabIndex = 51;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel5.Location = new System.Drawing.Point(37, 289);
            this.metroLabel5.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(100, 32);
            this.metroLabel5.TabIndex = 50;
            this.metroLabel5.Text = "Sức chứa:";
            this.metroLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtCoSo
            // 
            this.txtCoSo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCoSo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtCoSo.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtCoSo.Location = new System.Drawing.Point(147, 261);
            this.txtCoSo.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtCoSo.Name = "txtCoSo";
            this.txtCoSo.ReadOnly = true;
            this.txtCoSo.Size = new System.Drawing.Size(379, 23);
            this.txtCoSo.TabIndex = 49;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel4.Location = new System.Drawing.Point(37, 257);
            this.metroLabel4.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(100, 32);
            this.metroLabel4.TabIndex = 48;
            this.metroLabel4.Text = "Cơ Sở:";
            this.metroLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // cbbPhongHoc
            // 
            this.cbbPhongHoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbPhongHoc.DisplayMember = "Value";
            this.cbbPhongHoc.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbbPhongHoc.FormattingEnabled = true;
            this.cbbPhongHoc.ItemHeight = 19;
            this.cbbPhongHoc.Location = new System.Drawing.Point(147, 228);
            this.cbbPhongHoc.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.cbbPhongHoc.Name = "cbbPhongHoc";
            this.cbbPhongHoc.Size = new System.Drawing.Size(379, 25);
            this.cbbPhongHoc.TabIndex = 47;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            this.cbbPhongHoc.UseSelectable = true;
            this.cbbPhongHoc.ValueMember = "Key";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel3.Location = new System.Drawing.Point(37, 225);
            this.metroLabel3.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(100, 32);
            this.metroLabel3.TabIndex = 46;
            this.metroLabel3.Text = "Phòng học:";
            this.metroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtLoaiLich
            // 
            this.txtLoaiLich.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLoaiLich.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtLoaiLich.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtLoaiLich.Location = new System.Drawing.Point(147, 165);
            this.txtLoaiLich.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtLoaiLich.Name = "txtLoaiLich";
            this.txtLoaiLich.ReadOnly = true;
            this.txtLoaiLich.Size = new System.Drawing.Size(379, 23);
            this.txtLoaiLich.TabIndex = 45;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel1.Location = new System.Drawing.Point(37, 161);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(100, 32);
            this.metroLabel1.TabIndex = 42;
            this.metroLabel1.Text = "Loại lịch học:";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // txtNgayBatDau
            // 
            this.txtNgayBatDau.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNgayBatDau.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtNgayBatDau.InputType = MetroControl.Components.MetroTextbox.Input.NormalInput;
            this.txtNgayBatDau.Location = new System.Drawing.Point(147, 197);
            this.txtNgayBatDau.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtNgayBatDau.Name = "txtNgayBatDau";
            this.txtNgayBatDau.ReadOnly = true;
            this.txtNgayBatDau.Size = new System.Drawing.Size(379, 23);
            this.txtNgayBatDau.TabIndex = 40;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.metroLabel12.Location = new System.Drawing.Point(40, 193);
            this.metroLabel12.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(97, 32);
            this.metroLabel12.TabIndex = 29;
            this.metroLabel12.Text = "Thời gian học:";
            this.metroLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // MetroInfoScheduleGroupBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.groupRight);
            this.Name = "MetroInfoScheduleGroupBox";
            this.Size = new System.Drawing.Size(571, 501);
            this.groupRight.ResumeLayout(false);
            this.groupRight.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupRight;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private MetroControl.Components.MetroLabel metroLabel12;
        public MetroControl.Components.MetroTextbox txtNgayBatDau;
        private MetroControl.Components.MetroLabel metroLabel1;
        public MetroControl.Components.MetroTextbox txtLoaiLich;
        public MetroControl.Components.MetroCombobox cbbPhongHoc;
        private MetroControl.Components.MetroLabel metroLabel3;
        public MetroControl.Components.MetroTextbox txtCoSo;
        private MetroControl.Components.MetroLabel metroLabel4;
        public MetroControl.Components.MetroTextbox txtSoLuongChoNgoi;
        private MetroControl.Components.MetroLabel metroLabel5;
    }
}
