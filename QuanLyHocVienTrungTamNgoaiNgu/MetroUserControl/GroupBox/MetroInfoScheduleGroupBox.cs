﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroDataset.Model;

namespace MetroUserControl.GroupBox
{
    public partial class MetroInfoScheduleGroupBox : UserControl
    {
        public Timetabling timetabling;
        private Action actionLoadSchedule;
        public MetroInfoScheduleGroupBox()
        {
            InitializeComponent();
        }

        public void SetValueScheduleChanged(Action a)
        {
            actionLoadSchedule = a;
            cbbPhongHoc.SelectedValueChanged += CbbPhongHoc_SelectedValueChanged;
        }

        private void CbbPhongHoc_SelectedValueChanged(object sender, EventArgs e)
        {
            EventHelper.Helper.TryCatch(() =>
            {
                ThoiKhoaBieu t = timetabling.Tkb;
                PhongHoc p = t.Phongs[int.Parse(cbbPhongHoc.SelectedValue.ToString())];

                txtCoSo.Text = p.Tencoso;
                txtSoLuongChoNgoi.Text = p.Soluongchongoi.ToString();
            });

        }

        private void LoadSchedules()
        {
            ThoiKhoaBieu t = timetabling.Tkb;
            txtLoaiLich.Text = t.Type;
            txtNgayBatDau.Text = t.Start.ToShortDateString() +" - "+t.End.ToShortDateString();

            var _datasource = new List<object>();
            for (int i = 0; i < t.Phongs.Count; i++)
                _datasource.Add(new { Key = i, Value = (t.Phongs[i].Maphong +" - "+t.Phongs[i].Macoso) }); //
            cbbPhongHoc.DataSource = _datasource;
        }

        public void CreateSchedule(List<Lich> data, String magiangvien)
        {
            timetabling = new Timetabling(data, magiangvien);
            timetabling.CreateSchedule();

            //đỗ dữ liệu mới
            LoadSchedules();
            actionLoadSchedule();
        }

        public ThoiKhoaBieu GetCurrentSchedule()
        {
            Func<ThoiKhoaBieu> f = new Func<ThoiKhoaBieu>(()=> { return timetabling.Tkb; });
            object o = EventHelper.Helper.TryCatchReturnValue(f);
            if(o != null)
                return (ThoiKhoaBieu)o;
            return null;
        }

        public PhongHoc GetCurrentRoom()
        {
            ThoiKhoaBieu tkb = GetCurrentSchedule();
            Func<PhongHoc> f = new Func<PhongHoc>(() => { return tkb.Phongs[int.Parse(cbbPhongHoc.SelectedValue.ToString())]; });
            object o = EventHelper.Helper.TryCatchReturnValue(f);
            if (o != null)
                return (PhongHoc)o;
            return null;
        }

        public void ClearData()
        {
            timetabling = null;
            txtLoaiLich.Clear();
            txtCoSo.Clear();
            txtNgayBatDau.Clear();
            txtSoLuongChoNgoi.Clear();
            cbbPhongHoc.DataSource = null;
        }

    }
}
