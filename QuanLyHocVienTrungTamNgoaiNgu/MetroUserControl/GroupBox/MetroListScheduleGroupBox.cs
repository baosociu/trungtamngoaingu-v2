﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroDataset.Model;
using MetroDataset;

namespace MetroUserControl.GroupBox
{
    public partial class MetroListScheduleGroupBox : UserControl
    {
        public MetroListScheduleGroupBox()
        {
            InitializeComponent();
        }


        public void LoadDetailListSchedule(ThoiKhoaBieu data)
        {
            var c = (new Select()).GetDetailSchedule(data);
            this.grid.DataSource = c;
            this.grid.ReadOnly = false;
            this.grid.AllowUserToOrderColumns = true;

            if (c.Count == 0)
                return;
            for (int i = 0; i < c[0].GetType().GetProperties().Length; i++)
                this.grid.AlignColumn(i, DataGridViewContentAlignment.MiddleCenter);
        }

        public void ClearData()
        {
            grid.DataSource = null;
        }
    }
}
