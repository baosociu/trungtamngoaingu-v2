﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLHelper
{
    public class SQLStatic
    {
        public static bool checkConnection(SqlConnection connection)
        {
            //return true nếu thành công
            //return false nếu thất bại
            try
            {
                connection.Open();
                connection.Close();
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public static Status checkConnectionStatus(SqlConnection connection)
        {
            //return true nếu thành công
            //return false nếu thất bại
            try
            {
                connection.Open();
                connection.Close();
            }
            catch (Exception e)
            {
                Status error = SQLStatus.ConnectionFailStatus;
                if (e is SqlException)
                {
                    SqlException sqlException = e as SqlException;
                    if(sqlException.ErrorCode == -2146232060)
                        error = SQLStatus.ConnectionFailStatus;
                }
                string connectionstring = connection.ConnectionString;
                return error;
            }
            return SQLStatus.ConnectionSuccessStatus;
        }
    }
}
