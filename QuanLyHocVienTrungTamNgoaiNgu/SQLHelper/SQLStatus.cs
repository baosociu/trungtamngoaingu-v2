﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLHelper
{
    public class SQLStatus
    {
        public static Status ConnectionSuccessStatus = new Status(0, "Kết nối máy chủ thành công");
        public static Status ConnectionFailStatus = new Status(1, "Kết nối máy chủ thất bại");
        public static Status LoginSuccess = new Status(2, "Đăng nhập thành công");
        public static Status LoginFail = new Status(3, "Đăng nhập thất bại");
        public static Status LoginBlocked = new Status(4, "Tài khoản không còn hoạt động");
        public static Status ExecuteSuccess = new Status(5, "Thực thi câu lệnh thành công");
        public static Status ExecuteFail = new Status(6, "Thực thi câu lệnh thất bại");
       // public static Status ConnectionFail
    }

    public class Status
    {
        private int id;
        private string comment;
        private object data;

        public Status(int id, string comment)
        {
            this.id = id;
            this.comment = comment;
        }

        public Status(Status status)
        {
            this.id = status.id;
            this.comment = status.comment;
        }

        public int Id { get => id; }
        public string Comment { get => comment; }

        public object Data { get => data; set => data = value; }
    }
}
