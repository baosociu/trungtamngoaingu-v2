﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLHelper
{
    public class SQLQuery
    {
        SqlConnection connection;
        public SQLQuery(string connectString)
        {
            connection = new SqlConnection(connectString);
        }

        public Status excuteSelectQuery(string selectQuery)
        {
            try
            {
                SqlCommand command = new SqlCommand(selectQuery, connection);
                connection.Open();
                DataTable table = new DataTable();
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(table);
                Status status = new Status(SQLStatus.ExecuteSuccess);
                status.Data = table;
                if (connection.State == System.Data.ConnectionState.Open)
                    connection.Close();
                return status;
            }catch(Exception e)
            {

            }
            return SQLStatus.ExecuteFail;
        }
        
    }
}
