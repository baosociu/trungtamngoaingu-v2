﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLHelper
{
    public class SQLLogin
    {
        SqlConnection connection;
        public SQLLogin(string source, string database, bool security) {
            string connectString = "Data Source=" + source + ";Initial Catalog=" + database + ";Integrated Security=" + security.ToString();
            connection = new SqlConnection(connectString);
        }

        public SQLLogin()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            string connectString = config.AppSettings.Settings["ConnectString"].Value;
            try
            {
                connection = new SqlConnection(connectString);
            }catch(Exception e)
            {

            }
        }

        public SQLLogin(string source, string database, bool security, string username, string password)
        {
            string connectString = "Data Source=" + source + ";Initial Catalog=" + database + ";Integrated Security=" + security.ToString() + ";User ID=" + username + ";Password=" + password;
            connection = new SqlConnection(connectString);
        }



        public Status login(string username, string password)
        {
            bool check = SQLStatic.checkConnection(connection);
            if (!check)
                return SQLStatus.ConnectionFailStatus;

            int result = 0;
            SqlCommand command = new SqlCommand();
            try
            {
                connection.Open();
                command = new SqlCommand("dbo.DangNhap", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@tentaikhoan", username);
                command.Parameters.AddWithValue("@matkhau", password);

                result = (int)command.ExecuteScalar();
            }catch(Exception e){
                result = 0;
                return new Status(10001, e.Message);
            }
            if (connection.State == ConnectionState.Open)
                connection.Close();
            return result == 1 ? SQLStatus.LoginSuccess : SQLStatus.LoginFail;
        }
    }
}
