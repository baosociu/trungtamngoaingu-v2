﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroControl.Properties;
using System.IO;
using System.Drawing.Text;
using System.Data.Sql;
using System.Threading;
using SQLHelper;
using System.Configuration;
using System.Data.SqlClient;

namespace MetroControl.Controls
{
    public partial class MetroConnection : UserControl
    {
        public MetroConnection()
        {
            InitializeComponent();
            this.Load += MetroConnection_Load;
        }


        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.ParentForm.Close();
        }

        public void SetOkPerform(Action action)
        {
            this.btnOk.Click += (s,e)=> {
                action();
                string connectString = getConnectString();
                System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.AppSettings.Settings["ConnectString"].Value = connectString;
                config.Save(ConfigurationSaveMode.Modified);
                this.ParentForm.Close();
            };

        }


        private void BtnTest_Click(object sender, EventArgs e)
        {
            (sender as Button).Enabled = false;
            string connectionString = getConnectString();
            if (!String.IsNullOrWhiteSpace(connectionString))
                (new Thread(() => testConnection(connectionString))).Start();
        }

        private void testConnection(string connectionString)
        {
            Status status = SQLStatic.checkConnectionStatus(new System.Data.SqlClient.SqlConnection(connectionString));
            GetResultTestConnection(btnTest, status);
        }

        private void CbbAuth_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool enable = (sender as ComboBox).SelectedIndex == 1;
            txtUser.Enabled = txtPass.Enabled = lblUser.Enabled = lblPass.Enabled = cbxSavePassword.Enabled = enable;
            if (!enable)
            {
                txtPass.Text = "";
                txtUser.Text = "";
                cbxSavePassword.Checked = false;
            }
        }

        private void CbbDatabase_DropDown(object sender, EventArgs e)
        {
            string connectstring = getConnectString();
            List<string> databasename = new List<string>();

            if (SQLStatic.checkConnection(new System.Data.SqlClient.SqlConnection(connectstring)))
            {
                SQLQuery query = new SQLQuery(connectstring);
                Status status = query.excuteSelectQuery("SELECT name FROM sys.databases");

                cbbDatabase.DataSource = null;
                if (status.Data != null)
                    foreach (DataRow r in ((DataTable)status.Data).Rows)
                        databasename.Add(r[0].ToString());
            }
            cbbDatabase.DataSource = databasename;
        }

        private string getConnectString()
        {
            string databasename = string.IsNullOrWhiteSpace(cbbDatabase.Text) ? "master" : cbbDatabase.Text;
            string servername = cbbServerName.Text;
            string connectstring = "";
            if (!string.IsNullOrWhiteSpace(servername))
                connectstring += "Data Source=" + servername;
            if (!string.IsNullOrWhiteSpace(databasename))
                connectstring += ";Initial Catalog=" + databasename;
            if (!string.IsNullOrWhiteSpace(servername) && !string.IsNullOrWhiteSpace(databasename))
            {
                if (cbbAuth.SelectedIndex == 1)
                {
                    string ID = txtUser.Text;
                    string pass = txtPass.Text;
                    if (!string.IsNullOrWhiteSpace(ID) && !string.IsNullOrWhiteSpace(pass))
                        connectstring += ";Integrated Security=False; User ID=" + ID + ";Password=" + pass;
                    else
                    {
                        if (string.IsNullOrWhiteSpace(ID))
                        {
                            MessageBox.Show("Hãy cung cấp thông tin \'" + lblUser.Text + "\'.", "Connection",MessageBoxButtons.OK,MessageBoxIcon.Information);
                            txtUser.Focus();
                            return "";
                        }
                        else if (string.IsNullOrWhiteSpace(pass))
                        {
                            MessageBox.Show("Hãy cung cấp thông tin \'" + lblPass.Text + "\'.", "Connection", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtPass.Focus();
                            return "";
                        }
                    }
                }
                else
                    connectstring += ";Integrated Security=True";
            }
            return (!string.IsNullOrWhiteSpace(connectstring) && connectstring[0] == ';') ? connectstring.Substring(1) : connectstring;
        }

        private void MetroConnection_Load(object sender, EventArgs e)
        {
            this.ParentForm.VisibleChanged += ParentForm_VisibleChanged;
            this.cbbServerName.DropDown += CbbServerName_DropDown;
            this.btnCapNhat.Click += BtnCapNhat_Click;
            this.cbbDatabase.DropDown += CbbDatabase_DropDown;
            this.cbbAuth.SelectedIndexChanged += CbbAuth_SelectedIndexChanged;
            this.btnTest.Click += BtnTest_Click;
            this.cbbServerName.TextChanged += TextChanged;
            this.txtUser.TextChanged += TextChanged;
            this.txtPass.TextChanged += TextChanged;
            this.btnCancel.Click += BtnCancel_Click;
            this.btnTest.Enabled = this.btnOk.Enabled = false;
            cbbAuth.DataSource = new List<string>()
            {
                "Windows Authentication",
                "SQL Server Authentication" //có password
            };
            cbbAuth.SelectedIndex = 0;
        }

        private void ParentForm_VisibleChanged(object sender, EventArgs e)
        {
            if((sender as Form).Visible)
                setBaseInfo();

        }

        private void BtnCapNhat_Click(object sender, EventArgs e)
        {
            if (cbbServerName.Enabled)
            {
                cbbServerName.DataSource = null;
                cbbServerName.Text = "";
                updateServerName(cbbServerName);
            }
        }

        private void CbbServerName_DropDown(object sender, EventArgs e)
        {
            updateServerName(sender);
        }

        private void updateServerName(object sender)
        {
            if ((sender as ComboBox).Items.Count == 0)
            {
                (sender as ComboBox).Text = "Loading...";
                (sender as ComboBox).Enabled = false;
                (new Thread(() => getServerName())).Start();
            }
        }

        public void setBaseInfo()
        {

            SqlConnectionStringBuilder builder = null;
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                string connectString = config.AppSettings.Settings["ConnectString"].Value;
                builder = new SqlConnectionStringBuilder(connectString);
                string server = builder["server"].ToString();
                string database = builder["database"].ToString();
                string user = builder["User ID"].ToString();
                string pass = builder["Password"].ToString();
                cbbServerName.Text = server;
                cbbDatabase.Items.Add(database);
                cbbDatabase.SelectedIndex = 0;
                if (user != "" && pass != "")
                {
                    cbbAuth.SelectedIndex = 1;
                    txtPass.Text = pass;
                    txtUser.Text = user;
                }
            }
            catch
            {
                return;
            }
            
        }


        private void getServerName()
        {
            List<string> names = new List<string>();
            DataTable table = SqlDataSourceEnumerator.Instance.GetDataSources();
            foreach (DataRow server in table.Rows)
            {
                names.Add(string.Format(@"{0}\{1}",
                  server[table.Columns["ServerName"]],
                  server[table.Columns["InstanceName"]]));
            }
            GetServerName(cbbServerName, names);
        }

        private void TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(cbbServerName.Text))
            {
                btnOk.Enabled = btnTest.Enabled = (cbbAuth.SelectedIndex == 0 ||
                    (!string.IsNullOrWhiteSpace(txtUser.Text) &&
                    !string.IsNullOrWhiteSpace(txtPass.Text)));

            }
            else
                btnOk.Enabled = btnTest.Enabled = false;
        }

        delegate void GetResultTestConnectionCallBack(Button btn, Status status);
        private void GetResultTestConnection(Button btn, Status status)
        {
            if (btn.InvokeRequired)
            {
                GetResultTestConnectionCallBack d = new GetResultTestConnectionCallBack(GetResultTestConnection);
                this.Invoke(d, new object[] { btn, status });
            }
            else
            {
                MessageBox.Show(status.Comment, "Connection", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btn.Enabled = true;
                btn.Focus();
            }
        }

        delegate void GetServerNameCallBack(ComboBox cbbServerName, List<string> names);
        private void GetServerName(ComboBox cbbServerName, List<string> names)
        {
            if (cbbServerName.InvokeRequired)
            {
                GetServerNameCallBack d = new GetServerNameCallBack(GetServerName);
                this.Invoke(d, new object[] { cbbServerName, names });
            }
            else
            {
                cbbServerName.Text = "";
                cbbServerName.Enabled = true;
                cbbServerName.DataSource = names;
                if (cbbServerName.Items.Count != 0)
                    cbbServerName.SelectedIndex = 0;
            }
        }
    }
}
