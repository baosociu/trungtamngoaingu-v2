﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Threading;

namespace MetroControl.Controls
{
    public partial class MetroItemMenu : UserControl
    {
        Bitmap image;
        Bitmap imageSelected;
        private string id;
        //TODO: Màu chữ
        Color foreColor = Color.White;
        public Color ForeColorItem { get => foreColor; set => SetForeColor(value); }
        public void SetForeColor(Color value)
        {
            foreColor = value;
            if (!selected)
                this.label1.ForeColor = foreColor;
        }
        Color foreColorSelected = Color.Black;
        public Color ForeColorSelected { get => foreColorSelected; set => SetForeColorSelected(value); }
        private void SetForeColorSelected(Color value)
        {
            foreColorSelected = value;
            if (selected)
                this.label1.ForeColor = foreColorSelected;
        }

        //TODO: Màu nền
        Color backColor = Color.LightSeaGreen;
        public Color BackColorItem { get => backColor; set => SetBackColor(value); }
        private void SetBackColor(Color value)
        {
            backColor = value;
            if (!selected)
                this.BackColor = backColor;
        }
        Color backColorSelected = Color.White;
        public Color BackColorSelected { get => backColorSelected; set => SetBackColorSelected(value); }
        private void SetBackColorSelected(Color value)
        {
            backColorSelected = value;
            if (selected)
                this.BackColor = backColor;
            this.BackColor = backColorSelected;
        }

        //TODO: user control
        MetroContentMenu userControl;
        public MetroContentMenu UserControl { get => userControl; set => SetUserControl(value); }
        private void SetUserControl(MetroContentMenu value)
        {
            this.userControl = value;
            this.userControl.Dock = DockStyle.Fill;

            try
            {
                ((this.userControl).GetChildUserControl() as MetroBridge.Bridge).Init();
                this.label1.Text = ((this.userControl).GetChildUserControl() as MetroBridge.Bridge).GetTitle().ToUpper();
                id = ((this.userControl).GetChildUserControl() as MetroBridge.Bridge).GetId();

            }
            catch (Exception e) { }
        }

        public string Title { get => label1.Text; } 

        //TODO: selected ?
        bool selected = false;
        public bool Selected { get => selected; set => SetSelected(value); }
        private void SetSelected(bool value)
        {
            selected = value;
            if (selected)
            {
                SetBackColorSelected(backColorSelected);
                SetForeColorSelected(foreColorSelected);
                iconItem.Image = imageSelected;
            }
            else
            {
                SetBackColor(backColor);
                SetForeColor(foreColor);
                iconItem.Image = image;
            }
        }

        //TODO: Icon
        public Bitmap ImageItem { get => image; set => SetImageItem(value); }
        private void SetImageItem(Bitmap value)
        {
            this.image = value;
            if (!selected)
                this.iconItem.Image = value;
        }

        public Bitmap ImageItemSelected { get => imageSelected; set => SetImageItemSelected(value); }
        private void SetImageItemSelected(Bitmap value)
        {
            this.imageSelected = value;
            if (selected)
                this.iconItem.Image = value;
        }


        public string ID { get => id; }

        public MetroItemMenu SetContent(MetroContentMenu content)
        {
            UserControl = content;
            try
            {
                (content.GetChildUserControl() as MetroBridge.Bridge).Init();
            }catch(Exception e) { };
            return this;
        }

        public MetroItemMenu()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
            this.selected = false;
            EventHelper.Helper.SetHandHover(this.label1);
            EventHelper.Helper.SetHandHover(this.iconItem);

        }


    }
}
