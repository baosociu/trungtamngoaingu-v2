﻿namespace MetroControl.Controls
{
    partial class MetroMenu
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainPanel = new System.Windows.Forms.TableLayoutPanel();
            this.layoutHeader = new System.Windows.Forms.TableLayoutPanel();
            this.lblTitle = new System.Windows.Forms.Label();
            this.picHelp = new System.Windows.Forms.PictureBox();
            this.layoutFooter = new System.Windows.Forms.TableLayoutPanel();
            this.userControlPanel = new System.Windows.Forms.Panel();
            this.layoutLeft = new System.Windows.Forms.TableLayoutPanel();
            this.panelMenu = new System.Windows.Forms.TableLayoutPanel();
            this.picCollap = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblTime = new MetroControl.Components.MetroLabel();
            this.lblWelcome = new MetroControl.Components.MetroLabel();
            this.mainPanel.SuspendLayout();
            this.layoutHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picHelp)).BeginInit();
            this.layoutFooter.SuspendLayout();
            this.layoutLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCollap)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainPanel
            // 
            this.mainPanel.ColumnCount = 2;
            this.mainPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.mainPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainPanel.Controls.Add(this.layoutHeader, 0, 0);
            this.mainPanel.Controls.Add(this.layoutFooter, 0, 2);
            this.mainPanel.Controls.Add(this.userControlPanel, 1, 1);
            this.mainPanel.Controls.Add(this.layoutLeft, 0, 1);
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel.Location = new System.Drawing.Point(0, 0);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.RowCount = 3;
            this.mainPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.mainPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.mainPanel.Size = new System.Drawing.Size(846, 460);
            this.mainPanel.TabIndex = 0;
            // 
            // layoutHeader
            // 
            this.layoutHeader.ColumnCount = 3;
            this.mainPanel.SetColumnSpan(this.layoutHeader, 2);
            this.layoutHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.layoutHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layoutHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.layoutHeader.Controls.Add(this.lblTitle, 1, 0);
            this.layoutHeader.Controls.Add(this.picHelp, 2, 0);
            this.layoutHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutHeader.Location = new System.Drawing.Point(0, 0);
            this.layoutHeader.Margin = new System.Windows.Forms.Padding(0);
            this.layoutHeader.Name = "layoutHeader";
            this.layoutHeader.RowCount = 1;
            this.layoutHeader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layoutHeader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.layoutHeader.Size = new System.Drawing.Size(846, 40);
            this.layoutHeader.TabIndex = 1;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(43, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(760, 40);
            this.lblTitle.TabIndex = 2;
            this.lblTitle.Text = "HỆ THỐNG QUẢN LÝ HỌC VIÊN TRUNG TÂM NGOẠI NGỮ";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picHelp
            // 
            this.picHelp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picHelp.Location = new System.Drawing.Point(809, 3);
            this.picHelp.Name = "picHelp";
            this.picHelp.Size = new System.Drawing.Size(34, 34);
            this.picHelp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picHelp.TabIndex = 3;
            this.picHelp.TabStop = false;
            // 
            // layoutFooter
            // 
            this.layoutFooter.ColumnCount = 3;
            this.mainPanel.SetColumnSpan(this.layoutFooter, 2);
            this.layoutFooter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.layoutFooter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layoutFooter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.layoutFooter.Controls.Add(this.tableLayoutPanel1, 1, 0);
            this.layoutFooter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutFooter.Location = new System.Drawing.Point(0, 420);
            this.layoutFooter.Margin = new System.Windows.Forms.Padding(0);
            this.layoutFooter.Name = "layoutFooter";
            this.layoutFooter.RowCount = 1;
            this.layoutFooter.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layoutFooter.Size = new System.Drawing.Size(846, 40);
            this.layoutFooter.TabIndex = 2;
            // 
            // userControlPanel
            // 
            this.userControlPanel.BackColor = System.Drawing.SystemColors.Control;
            this.userControlPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlPanel.Location = new System.Drawing.Point(200, 40);
            this.userControlPanel.Margin = new System.Windows.Forms.Padding(0);
            this.userControlPanel.Name = "userControlPanel";
            this.userControlPanel.Size = new System.Drawing.Size(646, 380);
            this.userControlPanel.TabIndex = 3;
            // 
            // layoutLeft
            // 
            this.layoutLeft.ColumnCount = 1;
            this.layoutLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layoutLeft.Controls.Add(this.panelMenu, 0, 0);
            this.layoutLeft.Controls.Add(this.picCollap, 0, 1);
            this.layoutLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutLeft.Location = new System.Drawing.Point(0, 40);
            this.layoutLeft.Margin = new System.Windows.Forms.Padding(0);
            this.layoutLeft.Name = "layoutLeft";
            this.layoutLeft.RowCount = 2;
            this.layoutLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layoutLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.layoutLeft.Size = new System.Drawing.Size(200, 380);
            this.layoutLeft.TabIndex = 4;
            // 
            // panelMenu
            // 
            this.panelMenu.BackColor = System.Drawing.SystemColors.Control;
            this.panelMenu.ColumnCount = 1;
            this.panelMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.panelMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMenu.Location = new System.Drawing.Point(0, 0);
            this.panelMenu.Margin = new System.Windows.Forms.Padding(0);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.RowCount = 5;
            this.panelMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.panelMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.panelMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.panelMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.panelMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.panelMenu.Size = new System.Drawing.Size(200, 360);
            this.panelMenu.TabIndex = 1;
            // 
            // picCollap
            // 
            this.picCollap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picCollap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picCollap.Location = new System.Drawing.Point(0, 360);
            this.picCollap.Margin = new System.Windows.Forms.Padding(0);
            this.picCollap.Name = "picCollap";
            this.picCollap.Size = new System.Drawing.Size(200, 20);
            this.picCollap.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picCollap.TabIndex = 2;
            this.picCollap.TabStop = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.lblWelcome, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblTime, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(40, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(766, 40);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lblTime
            // 
            this.lblTime.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTime.AutoSize = true;
            this.lblTime.BackColor = System.Drawing.Color.Transparent;
            this.lblTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTime.Location = new System.Drawing.Point(3, 25);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(760, 15);
            this.lblTime.TabIndex = 1;
            this.lblTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // lblWelcome
            // 
            this.lblWelcome.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWelcome.AutoSize = true;
            this.lblWelcome.BackColor = System.Drawing.Color.Transparent;
            this.lblWelcome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWelcome.Location = new System.Drawing.Point(3, 5);
            this.lblWelcome.Name = "lblWelcome";
            this.lblWelcome.Size = new System.Drawing.Size(760, 15);
            this.lblWelcome.TabIndex = 2;
            this.lblWelcome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            new MetroControl.Components.MetroDataType().Add(MetroControl.Components.Type.View);
            // 
            // MetroMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mainPanel);
            this.Name = "MetroMenu";
            this.Size = new System.Drawing.Size(846, 460);
            this.mainPanel.ResumeLayout(false);
            this.layoutHeader.ResumeLayout(false);
            this.layoutHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picHelp)).EndInit();
            this.layoutFooter.ResumeLayout(false);
            this.layoutLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picCollap)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel mainPanel;
        private System.Windows.Forms.TableLayoutPanel layoutHeader;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.PictureBox picHelp;
        private System.Windows.Forms.TableLayoutPanel layoutFooter;
        private System.Windows.Forms.Panel userControlPanel;
        private System.Windows.Forms.TableLayoutPanel layoutLeft;
        private System.Windows.Forms.TableLayoutPanel panelMenu;
        private System.Windows.Forms.PictureBox picCollap;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Components.MetroLabel lblWelcome;
        private Components.MetroLabel lblTime;
    }
}
