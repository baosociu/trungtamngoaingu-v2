﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MetroControl.Controls
{
    public partial class MetroContentMenu : UserControl
    {
        public MetroContentMenu()
        {
            InitializeComponent();
            this.BackColor = Color.White;
            this.Dock = DockStyle.Fill;
        }

        public MetroContentMenu AddUserControl(MetroBridge.Bridge user)
        {
            if(tablePanel.Controls.Count == 0)
            {
                user.Dock = DockStyle.Fill;
                tablePanel.Controls.Add(user, 1, 1);
            }
            return this;
        }

        public UserControl GetChildUserControl()
        {
            if (tablePanel.Controls.Count != 0)
                return tablePanel.Controls[0] as UserControl;
            return null;
        }
    }
}
