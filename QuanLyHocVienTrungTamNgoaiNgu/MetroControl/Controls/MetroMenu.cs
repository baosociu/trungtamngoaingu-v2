﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Imaging;
using MetroControl.Properties;
using System.Data.Common;

namespace MetroControl.Controls
{
    public partial class MetroMenu : UserControl
    {
        //TODO: properties:
        Color BackColorMenu = Color.DeepSkyBlue;
        Color ForeColorMenu = Color.White;

        //TODO: các item menu
        public List<MetroItemMenu> Items = new List<MetroItemMenu>();
        public void SetItemsAdapter(List<MetroItemMenu> value, int indexStart)
        {
            Items = new List<MetroItemMenu>();
            Items = value;
            for (int i = 0; i < Items.Count; i++)
            {
                Items[i].Selected = false;
                Items[i].BackColorItem = BackColorMenu;
                //click
                Items[i].label1.Click += MetroMenu_Click;
                Items[i].iconItem.Click += MetroMenu_Click;
            }

            ReloadLayout();
            ClickAt(indexStart);
        }

        private void MetroMenu_Click(object sender, EventArgs e)
        {
            int index = Items.IndexOf(((sender as Control).Parent).Parent as MetroItemMenu);
            ClickAt(index);
        }

        //TODO: Click
        int indexSelected = -1;

        public void ClearItems()
        {
            panelMenu.Controls.Clear();
            indexSelected = -1;
        }

        private void ClickAt(int newIndex)
        {
            if (indexSelected == newIndex)
                return;
            if (indexSelected >= 0)
                Items[indexSelected].Selected = !Items[indexSelected].Selected;
            indexSelected = newIndex;
            Items[indexSelected].Selected = !Items[indexSelected].Selected;
            lblTitle.Text = Items[indexSelected].Title;
            //TODO: change user control
            if (Items[indexSelected].UserControl != null)
            {

                if (userControlPanel.Controls.Count == 1)
                    userControlPanel.Controls.RemoveAt(0);
                userControlPanel.Controls.Add(Items[indexSelected].UserControl);
                //gọi load data
                Items[indexSelected].UserControl.Visible = false;
                Items[indexSelected].UserControl.Visible = true;
            }
        }

        public UserControl GetItemCurrent()
        {
            return Items[indexSelected].UserControl.GetChildUserControl();
        }

        //TODO: kích thước - ngang của menu
        int currentSizeMenu = 200;
        int minSizeMenu = 40;
        int maxSizeMenu = 200;
        [Browsable(true), EditorBrowsable(EditorBrowsableState.Always)]
        public int MaxSizeMenu { get => maxSizeMenu; set => SetMaxSizeMenu(value); }
        private void SetMaxSizeMenu(int value)
        {
            if (value < 100)
                return;
            maxSizeMenu = value;
            SetSizeMenu(value);
        }
        private void SetSizeMenu(int value)
        {
            currentSizeMenu = value;
            mainPanel.ColumnStyles[0].SizeType = SizeType.Absolute;
            mainPanel.ColumnStyles[0].Width = currentSizeMenu;
        }

        //help
        public void SetHelper(Image icon, EventHandler click)
        {
            picHelp.Image = icon;
            picHelp.Click += click;
            EventHelper.Helper.SetHandHover(this.picHelp);
        }
        private void ReloadLayout()
        {
            panelMenu.Controls.Clear();
            panelMenu.ColumnStyles.Clear();
            panelMenu.RowStyles.Clear();
            panelMenu.ColumnCount = 1;
            panelMenu.RowCount = Items.Count + 1;

            for (int i = 0; i <= Items.Count; i++)
            {
                if (i == Items.Count)
                    panelMenu.RowStyles.Add(new RowStyle(SizeType.Percent, 100));
                else
                {
                    panelMenu.RowStyles.Add(new RowStyle(SizeType.Absolute, 40));
                    panelMenu.Controls.Add(Items[i], 0, i);
                }
            }
        }

        public MetroMenu()
        {
            InitializeComponent();
            this.Load += MetroMenu_Load;
        }

        Timer timer;
        public void SetNameUser(string name)
        {
            name = name.Substring(0, 1).ToUpper() + name.Substring(1).ToLower();
            string s = "Chào, " + name + " !";
            
            lblWelcome.Text = s;
            lblWelcome.ForeColor = ForeColorMenu;
        }
        private void MetroMenu_Load(object sender, EventArgs e)
        {
            panelMenu.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            currentSizeMenu = maxSizeMenu;
            SetVerticalScrollMenu();

            picCollap.Click += PicCollap_Click;
            //TODO: Menu
            panelMenu.BackColor = Color.Transparent;


            //TODO: label
            lblTitle.ForeColor = ForeColorMenu;

            //TODO: header
            layoutHeader.BackColor = BackColorMenu;

            //TODO: footer
            layoutFooter.BackColor = BackColorMenu;

            //TODO: left
            picCollap.BackColor = BackColorMenu;
            picCollap.Image = Properties.Resources.right_to_left_white;
            EventHelper.Helper.SetHandHover(picCollap);

            //Timer
            lblTime.ForeColor = ForeColorMenu;
            timer = new Timer();
            timer.Interval = 900;
            timer.Enabled = true;
            timer.Tick += (s, en) =>
            {
                lblTime.Text = DateTime.Now.ToLongTimeString()+" "+DateTime.Now.ToShortDateString();
            };
        }

        private void SetVerticalScrollMenu()
        {
            panelMenu.HorizontalScroll.Maximum = 0;
            panelMenu.VerticalScroll.Maximum = 0;
            panelMenu.HorizontalScroll.Visible = false;
            panelMenu.VerticalScroll.Visible = false;
            panelMenu.HorizontalScroll.Enabled = false;
            panelMenu.VerticalScroll.Enabled = true;
            panelMenu.AutoScroll = true;
            
        }

        private void PicCollap_Click(object sender, EventArgs e)
        {
            //SetVerticalScrollMenu();

            if (currentSizeMenu == 40)
            {
                picCollap.Image = Properties.Resources.right_to_left_white;
                SetSizeMenu(maxSizeMenu);
            }
            else
            {
                picCollap.Image = Properties.Resources.left_to_right_white;
                SetSizeMenu(minSizeMenu);
            }
        }
    }
}
