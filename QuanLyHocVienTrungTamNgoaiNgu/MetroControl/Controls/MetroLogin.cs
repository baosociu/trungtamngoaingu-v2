﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroControl.Properties;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using SQLHelper;
using MetroFramework.Forms;
using System.Configuration;
using System.Security.Cryptography;

namespace MetroControl.Controls
{
    public partial class MetroLogin : UserControl
    {
        SQLLogin sqlLogin;
        Form frmConnection;
        Form frmLoginSuccess;

        [Browsable(true), EditorBrowsable(EditorBrowsableState.Always)]
        public string username { get => txtUser.Text; set => txtUser.Text = value; }

        [Browsable(true), EditorBrowsable(EditorBrowsableState.Always)]
        public string password { get => txtPass.Text; set => txtPass.Text = value; }

        [Browsable(true), EditorBrowsable(EditorBrowsableState.Always)]
        public MetroLogin()
        {
            InitializeComponent();
            this.Load += MetroLogin_Load;
        }



        public void SetForm(Form frmConnection, Form frmLoginSuccess)
        {
            this.frmConnection = frmConnection;
            this.frmLoginSuccess = frmLoginSuccess;
        }

        private void MetroLogin_Load(object sender, EventArgs e)
        {
            EventHelper.Helper.SetHandHover(this.picSetting);
            this.ParentForm.AcceptButton = btnDangNhap;
            this.ParentForm.CancelButton = btnHuy;
            this.btnDangNhap.Select();
           
        }

        public void SetEventButtonLogin(Action click)
        {
            this.btnDangNhap.Click += (s, e) => click();
        }

        public void SetEventButtonCancel(Action click)
        {
            this.btnHuy.Click += (s, e) => click();
        }

        public void SetEventButtonConnection(Action click)
        {
            this.picSetting.Click += (s, e) => click();
        }


        public void FocusUsername()
        {
            txtUser.Focus();
        }

        public void FocusPassword()
        {
            txtPass.Focus();
        }


    }


}
