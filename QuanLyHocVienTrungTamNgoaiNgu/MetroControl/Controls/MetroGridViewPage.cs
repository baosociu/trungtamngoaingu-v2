﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroControl.Components;
using System.Reflection;

namespace MetroControl.Controls
{
    public partial class MetroGridViewPage : UserControl
    {
        int pagenumber = 1;
        int numberrow = 20;

        public int PageNumber { get => pagenumber; }
        public int PageInput { get => String.IsNullOrWhiteSpace(page.Text) ? PageNumber : int.Parse(page.Text); }

        public int NumberRow { get => numberrow; }
        public MetroGridViewPage()
        {
            InitializeComponent();
            this.Load += MetroGridViewPage_Load;
        }

        private void MetroGridViewPage_Load(object sender, EventArgs e)
        {
            EventHelper.Helper.SetHandHover(this.picBack);
            EventHelper.Helper.SetHandHover(this.picNext);
            EventHelper.Helper.SetHandHover(this.picGo);
            page.Text = PageNumber.ToString();
            page.KeyPress += Page_KeyPress;
        }

        private void Page_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        public void SetOnClick(Action ac)
        {
            SetOnClickBack(ac);
            SetOnClickGo(ac);
            SetOnClickNext(ac);
        }

        private void SetOnClickBack(Action ac)
        {
            picBack.Click += (s, e) =>
             {
                 if (PageNumber == 1)
                     return;
                 pagenumber--;
                 page.Text = PageNumber.ToString();
                 ac();
             };
        }

        private void SetOnClickNext(Action ac)
        {
            picNext.Click += (s, e) =>
            {
                if (grid.RowCount != NumberRow)
                    return;
                pagenumber++;
                page.Text = PageNumber.ToString();
                ac();
            };
        }

        private void SetOnClickGo(Action ac)
        {
            picGo.Click += (s, e) =>
             {
                 int p = String.IsNullOrWhiteSpace(page.Text) ? -1 : int.Parse(page.Text);
                 if (p <= 0)
                     page.Text = pagenumber.ToString();
                 else
                 {
                     pagenumber = p;
                     ac();
                 }
             };
        }


        private void RemoveClickEvent(Control b)
        {
            FieldInfo f1 = typeof(Control).GetField("EventClick",
                BindingFlags.Static | BindingFlags.NonPublic);
            object obj = f1.GetValue(b);
            PropertyInfo pi = b.GetType().GetProperty("Events",
                BindingFlags.NonPublic | BindingFlags.Instance);
            EventHandlerList list = (EventHandlerList)pi.GetValue(b, null);
            list.RemoveHandler(obj, list[obj]);
        }

        public void AlignColumn(int index, DataGridViewContentAlignment alignment)
        {
            try
            {
                this.grid.Columns[index].DefaultCellStyle.Alignment = alignment;
            }catch(Exception e) { }
        }

        public void Renew()
        {
            this.pagenumber = 1;
            this.numberrow = 20;
            this.page.Text = "1";
            RemoveClickEvent(this.picGo);
            RemoveClickEvent(this.picBack);
            RemoveClickEvent(this.picNext);
        }

    }
}
