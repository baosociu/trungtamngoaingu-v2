﻿namespace MetroControl.Controls
{
    partial class MetroConnection
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbbServerName = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.gbLogon = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.txtPass = new System.Windows.Forms.TextBox();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.cbbAuth = new System.Windows.Forms.ComboBox();
            this.lblPass = new System.Windows.Forms.Label();
            this.lblUser = new System.Windows.Forms.Label();
            this.lblAuth = new System.Windows.Forms.Label();
            this.cbxSavePassword = new System.Windows.Forms.CheckBox();
            this.gbConnectDatabase = new System.Windows.Forms.GroupBox();
            this.cbbDatabase = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btnTest = new MetroControl.Components.MetroButton();
            this.btnCapNhat = new MetroControl.Components.MetroButton();
            this.btnOk = new MetroControl.Components.MetroButton();
            this.btnCancel = new MetroControl.Components.MetroButton();
            this.tableLayoutPanel1.SuspendLayout();
            this.gbLogon.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.gbConnectDatabase.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbbServerName
            // 
            this.cbbServerName.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cbbServerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.cbbServerName.FormattingEnabled = true;
            this.cbbServerName.Location = new System.Drawing.Point(23, 19);
            this.cbbServerName.Name = "cbbServerName";
            this.cbbServerName.Size = new System.Drawing.Size(294, 21);
            this.cbbServerName.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.cbbServerName, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.gbLogon, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.gbConnectDatabase, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.btnCapNhat, 2, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(440, 360);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // gbLogon
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.gbLogon, 2);
            this.gbLogon.Controls.Add(this.tableLayoutPanel2);
            this.gbLogon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbLogon.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.gbLogon.Location = new System.Drawing.Point(23, 53);
            this.gbLogon.Name = "gbLogon";
            this.gbLogon.Size = new System.Drawing.Size(394, 194);
            this.gbLogon.TabIndex = 2;
            this.gbLogon.TabStop = false;
            this.gbLogon.Text = "Thông tin đăng nhập";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.txtPass, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.txtUser, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.cbbAuth, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblPass, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.lblUser, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.lblAuth, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.cbxSavePassword, 1, 4);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 6;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(388, 175);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // txtPass
            // 
            this.txtPass.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtPass.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.txtPass.Location = new System.Drawing.Point(123, 97);
            this.txtPass.Name = "txtPass";
            this.txtPass.PasswordChar = '●';
            this.txtPass.Size = new System.Drawing.Size(262, 20);
            this.txtPass.TabIndex = 5;
            this.txtPass.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPass.UseSystemPasswordChar = true;
            this.txtPass.WordWrap = false;
            // 
            // txtUser
            // 
            this.txtUser.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.txtUser.Location = new System.Drawing.Point(123, 57);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(262, 20);
            this.txtUser.TabIndex = 4;
            this.txtUser.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cbbAuth
            // 
            this.cbbAuth.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cbbAuth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbAuth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.cbbAuth.FormattingEnabled = true;
            this.cbbAuth.Location = new System.Drawing.Point(123, 16);
            this.cbbAuth.Name = "cbbAuth";
            this.cbbAuth.Size = new System.Drawing.Size(262, 21);
            this.cbbAuth.TabIndex = 3;
            // 
            // lblPass
            // 
            this.lblPass.AutoSize = true;
            this.lblPass.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPass.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.lblPass.Location = new System.Drawing.Point(3, 87);
            this.lblPass.Name = "lblPass";
            this.lblPass.Size = new System.Drawing.Size(114, 40);
            this.lblPass.TabIndex = 2;
            this.lblPass.Text = "Mật khẩu";
            this.lblPass.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.lblUser.Location = new System.Drawing.Point(3, 47);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(114, 40);
            this.lblUser.TabIndex = 1;
            this.lblUser.Text = "Tên tài khoản";
            this.lblUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAuth
            // 
            this.lblAuth.AutoSize = true;
            this.lblAuth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAuth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.lblAuth.Location = new System.Drawing.Point(3, 7);
            this.lblAuth.Name = "lblAuth";
            this.lblAuth.Size = new System.Drawing.Size(114, 40);
            this.lblAuth.TabIndex = 0;
            this.lblAuth.Text = "Chứng thực";
            this.lblAuth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbxSavePassword
            // 
            this.cbxSavePassword.AutoSize = true;
            this.cbxSavePassword.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbxSavePassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.cbxSavePassword.Location = new System.Drawing.Point(123, 130);
            this.cbxSavePassword.Name = "cbxSavePassword";
            this.cbxSavePassword.Size = new System.Drawing.Size(262, 34);
            this.cbxSavePassword.TabIndex = 6;
            this.cbxSavePassword.Text = "Lưu tài khoản";
            this.cbxSavePassword.UseVisualStyleBackColor = true;
            // 
            // gbConnectDatabase
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.gbConnectDatabase, 2);
            this.gbConnectDatabase.Controls.Add(this.cbbDatabase);
            this.gbConnectDatabase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbConnectDatabase.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.gbConnectDatabase.Location = new System.Drawing.Point(23, 253);
            this.gbConnectDatabase.Name = "gbConnectDatabase";
            this.gbConnectDatabase.Size = new System.Drawing.Size(394, 54);
            this.gbConnectDatabase.TabIndex = 3;
            this.gbConnectDatabase.TabStop = false;
            this.gbConnectDatabase.Text = "Kết nối cơ sở dữ liệu";
            // 
            // cbbDatabase
            // 
            this.cbbDatabase.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbDatabase.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.cbbDatabase.FormattingEnabled = true;
            this.cbbDatabase.Location = new System.Drawing.Point(3, 21);
            this.cbbDatabase.Name = "cbbDatabase";
            this.cbbDatabase.Size = new System.Drawing.Size(388, 21);
            this.cbbDatabase.TabIndex = 1;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel3, 2);
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.Controls.Add(this.btnTest, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnOk, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnCancel, 3, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(23, 313);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(394, 34);
            this.tableLayoutPanel3.TabIndex = 4;
            // 
            // btnTest
            // 
            this.btnTest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTest.Location = new System.Drawing.Point(3, 3);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(94, 28);
            this.btnTest.TabIndex = 5;
            this.btnTest.Text = "KIỂM TRA";
            this.btnTest.UseSelectable = true;
            // 
            // btnCapNhat
            // 
            this.btnCapNhat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCapNhat.Location = new System.Drawing.Point(323, 13);
            this.btnCapNhat.Name = "btnCapNhat";
            this.btnCapNhat.Size = new System.Drawing.Size(94, 34);
            this.btnCapNhat.TabIndex = 5;
            this.btnCapNhat.Text = "CẬP NHẬT";
            this.btnCapNhat.UseSelectable = true;
            // 
            // btnOk
            // 
            this.btnOk.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnOk.Location = new System.Drawing.Point(197, 3);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(94, 28);
            this.btnOk.TabIndex = 6;
            this.btnOk.Text = "XÁC NHẬN";
            this.btnOk.UseSelectable = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCancel.Location = new System.Drawing.Point(297, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(94, 28);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "HUỶ";
            this.btnCancel.UseSelectable = true;
            // 
            // MetroConnection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "MetroConnection";
            this.Size = new System.Drawing.Size(440, 360);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.gbLogon.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.gbConnectDatabase.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbbServerName;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox gbLogon;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.ComboBox cbbAuth;
        private System.Windows.Forms.Label lblPass;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.Label lblAuth;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.TextBox txtPass;
        private System.Windows.Forms.CheckBox cbxSavePassword;
        private System.Windows.Forms.GroupBox gbConnectDatabase;
        private System.Windows.Forms.ComboBox cbbDatabase;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private MetroControl.Components.MetroButton btnTest;
        private MetroControl.Components.MetroButton btnCapNhat;
        private MetroControl.Components.MetroButton btnOk;
        private MetroControl.Components.MetroButton btnCancel;
    }
}
