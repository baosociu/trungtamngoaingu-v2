﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MetroControl.Components
{
    public partial class MetroLabel : Label
    {
        MetroDataType typeData;
        [Browsable(true), EditorBrowsable(EditorBrowsableState.Always)]
        public MetroDataType TypeData { get => typeData; set => typeData = value; }

        Boolean isfocus;
        [Browsable(true), EditorBrowsable(EditorBrowsableState.Always)]
        public Boolean IsFocus { get => isfocus; set => SetFocus(value); }

        private void SetFocus(bool value)
        {
            isfocus = value;
            if (isfocus && !this.Text.Contains("(*)"))
            {
                if (this.Text.TrimEnd().EndsWith(":"))
                    this.Text = this.Text.TrimEnd().Substring(0, this.Text.TrimEnd().Length - 1) + "(*):";
                else
                    this.Text += "(*):";
            }
            else if (!isfocus || this.Text.Contains("(*)"))
            {
                this.Text = this.Text.Replace("(*)", "");
            }
        }

        public MetroLabel()
        {
            InitializeComponent();
            this.Font = new Font(this.Font.FontFamily, 9);
            this.Parent = new PictureBox();
            this.BackColor = Color.Transparent;
            this.TypeData = new MetroDataType();

        }
    }
}
