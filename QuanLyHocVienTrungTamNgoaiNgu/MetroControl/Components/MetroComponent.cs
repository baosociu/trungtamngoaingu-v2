﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetroControl.Components
{
    public partial class MetroComponent : Component
    {
        public MetroComponent()
        {
            InitializeComponent();
        }

        public MetroComponent(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
    }
}
