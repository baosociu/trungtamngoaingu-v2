﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MetroControl.Components
{
    public partial class MetroCheckbox : CheckBox
    {
        MetroDataType typeData;
        [Browsable(true), EditorBrowsable(EditorBrowsableState.Always)]
        public MetroDataType TypeData { get => typeData; set => typeData = value; }
        public MetroCheckbox()
        {
            InitializeComponent();
            this.Font = new Font(this.Font.FontFamily, 9);
            this.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.AutoSize = true;
            this.BackColor = Color.Transparent;
            this.TypeData = new MetroDataType();
        }
    }
}
