﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetroControl.Components
{
    public enum Type
    {
        View, Select, Insert, Update, Delete, Master
    }


    public class MetroDataType : List<Type>
    {
        string[] arr = new string[]
{ "TT0001","TT0002","TT0003","TT0004","TT0005","TT0006" };
        public MetroDataType()
        {
            Add(Type.View);
        }
        public MetroDataType AddType(Type type)
        {
            Add(type);
            return this;
        }

        public List<string> ToListString()
        {
            return this.Select(x => GetString(x)).ToList<string>();
        }

        public string GetStringByIndex(int index)
        {
            return arr[index];
        }
        public string GetString(Type x)
        {

            switch (x)
            {
                case Type.View:
                    return arr[0];
                case Type.Select:
                    return arr[1];
                case Type.Insert:
                    return arr[2];
                case Type.Update:
                    return arr[3];
                case Type.Delete:
                    return arr[4];
                case Type.Master:
                    return arr[5];
                default: return "";
            }
        }

     


        public string GetStringID(String x)
        {
            //  View, Select, Insert, Update, Delete, Master
            switch (x.ToUpper())
            {
                case "VIEW":
                    return arr[0];
                case "SELECT":
                    return arr[1];
                case "INSERT":
                    return arr[2];
                case "UPDATE":
                    return arr[3];
                case "DELETE":
                    return arr[4];
                case "MASTER":
                    return arr[5];
                default: return "";
            }
        }
    }
}
