﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetroControl.Components
{
    public partial class MetroButton : MetroFramework.Controls.MetroButton
    {
        MetroDataType typeData;
        [Browsable(true), EditorBrowsable(EditorBrowsableState.Always)]
        public MetroDataType TypeData { get => typeData; set =>SetTypeDate(value); }

        private void SetTypeDate(MetroDataType value)
        {
            typeData = value;

        }

        public MetroButton()
        {
            InitializeComponent();
            this.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.Margin = new System.Windows.Forms.Padding(3, 3, 3, 3);
            //this.Style = MetroFramework.MetroColorStyle.Blue;
            //this.UseStyleColors = true;
            this.Text = "HUỶ";
            if(this.typeData ==null)
                this.typeData = new MetroDataType();
            EventHelper.Helper.SetHandHover(this);
        }

    }
}
