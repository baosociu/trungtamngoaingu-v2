﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MetroControl.Components
{
    public partial class MetroGridView : MetroFramework.Controls.MetroGrid
    {
        MetroDataType typeData;
        [Browsable(true), EditorBrowsable(EditorBrowsableState.Always)]
        public MetroDataType TypeData { get => typeData; set => typeData = value; }
        public MetroGridView()
        {
            InitializeComponent();

            this.CellBorderStyle = DataGridViewCellBorderStyle.Raised;
            this.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            this.ScrollBars = ScrollBars.Both;
            this.MultiSelect = false;
            this.AllowUserToAddRows = false;
            this.AllowUserToDeleteRows = false;
            this.ReadOnly = true;
            this.TypeData = new MetroDataType();
            this.DataSourceChanged += MetroGridView_DataSourceChanged;
            this.MouseDown += MetroGridView_MouseDown;
            this.BorderStyle = BorderStyle.FixedSingle;
        }

        private void MetroGridView_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var hti = HitTest(e.X, e.Y);
                ClearSelection();
                CurrentCell = Rows[hti.RowIndex].Cells[0];
                Rows[hti.RowIndex].Selected = true;
            }
        }

        private void MetroGridView_DataSourceChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewColumn c in Columns)
            {
                c.HeaderText = c.HeaderText.Replace("_", " ").ToUpper();
                c.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }
        }

        public int IndexCurrentRow()
        {
            try
            {
                return this.Rows.IndexOf(this.CurrentRow);
            }
            catch (Exception e) { };
            return -1;
        }

        public void SetCurrentRow(int row)
        {
            try
            {
                CurrentCell = Rows[row].Cells[0];
            }
            catch (Exception e) { };

        }

        public void AlignColumn(int index, DataGridViewContentAlignment alignment)
        {
            try
            {
                this.Columns[index].DefaultCellStyle.Alignment = alignment;
            }
            catch (Exception e) { }
        }
    }
}
