﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MetroControl.Components
{
    public partial class MetroProgressBar : MetroFramework.Controls.MetroProgressBar
    {

        MetroDataType typeData;
        [Browsable(true), EditorBrowsable(EditorBrowsableState.Always)]
        public MetroDataType TypeData { get => typeData; set => typeData = value; }
        public MetroProgressBar()
        {
            InitializeComponent();
            this.TypeData = new MetroDataType();
            this.Visible = false;
            this.ProgressBarStyle = System.Windows.Forms.ProgressBarStyle.Blocks;
            this.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Maximum = 100;
            this.Step = 10;
        }


        public void StartProgressBar()
        {
            VisibleProgress(this,true);
            while (this.Visible)
                SetValueProgress(this);
        }

        public void StopProgressBar()
        {
             VisibleProgress(this,false);
        }

        delegate void VisibleProgressCallBack(MetroProgressBar progress, bool visible);
        private void VisibleProgress(MetroProgressBar progress, bool visible)
        {
            if (progress.InvokeRequired)
            {
                VisibleProgressCallBack d = new VisibleProgressCallBack(VisibleProgress);
                this.Invoke(d, new object[] { progress, visible });
            }
            else
            {
                progress.Visible = visible;
                progress.Refresh();
            }
        }

        delegate void SetValueProgressCallBack(MetroProgressBar progress);
        private void SetValueProgress(MetroProgressBar progress)
        {
            if (progress.InvokeRequired)
            {
                SetValueProgressCallBack d = new SetValueProgressCallBack(SetValueProgress);
                this.Invoke(d, new object[] { progress });
            }
            else
            {
                if (progress.Value == 100)
                    progress.Value = 0;
                progress.Value++;
            }
        }
    }
}
