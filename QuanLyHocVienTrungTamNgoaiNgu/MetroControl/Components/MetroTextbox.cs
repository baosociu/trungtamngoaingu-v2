﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MetroControl.Components
{
    public partial class MetroTextbox : TextBox
    {
        MetroDataType typeData;
        [Browsable(true), EditorBrowsable(EditorBrowsableState.Always)]
        public MetroDataType TypeData { get => typeData; set => typeData = value; }
        public MetroTextbox()
        {
            InitializeComponent();
            this.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.Font = new System.Drawing.Font(this.Font.FontFamily, 10);
            this.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.TypeData = new MetroDataType();
        }

        private void MetroTextbox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
        (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        Input inputType;
        public enum Input
        {
            NormalInput, PasswordInput, NumberInput
        }
        [Browsable(true), EditorBrowsable(EditorBrowsableState.Always)]
        public Input InputType { get => inputType; set => SetInputType(value); }

        private void SetInputType(Input value)
        {
            inputType = value;
            if (inputType == Input.NumberInput)
                this.KeyPress += MetroTextbox_KeyPress;
            else if (inputType == Input.PasswordInput)
                this.UseSystemPasswordChar = true;
        }
    }
    
}
