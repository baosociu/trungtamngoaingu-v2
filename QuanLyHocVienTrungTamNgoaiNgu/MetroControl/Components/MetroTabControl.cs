﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetroControl.Components
{
    public partial class MetroTabControl : MetroFramework.Controls.MetroTabControl
    {
        MetroDataType typeData;
        [Browsable(true), EditorBrowsable(EditorBrowsableState.Always)]
        public MetroDataType TypeData { get => typeData; set => typeData = value; }
        public MetroTabControl()
        {
            InitializeComponent();
            this.TypeData = new MetroDataType();
        }
    }
}
