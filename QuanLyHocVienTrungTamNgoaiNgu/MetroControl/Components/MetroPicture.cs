﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MetroControl.Components
{
    public partial class MetroPicture : PictureBox
    {
        MetroDataType typeData;
        [Browsable(true), EditorBrowsable(EditorBrowsableState.Always)]
        public MetroDataType TypeData { get => typeData; set => typeData = value; }
        public MetroPicture()
        {
            InitializeComponent();
            this.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TypeData = new MetroDataType();
        }

    }
}
