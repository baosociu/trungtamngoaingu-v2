﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MetroControl.Components
{
    public partial class MetroCombobox : MetroFramework.Controls.MetroComboBox
    {
        MetroDataType typeData;
        [Browsable(true), EditorBrowsable(EditorBrowsableState.Always)]
        public MetroDataType TypeData { get => typeData; set => typeData = value; }
        public MetroCombobox()
        {
            InitializeComponent();
            this.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.FormattingEnabled = true;
            this.UseSelectable = true;
            this.TypeData = new MetroDataType();
            this.ValueMember = "Key";
            this.DisplayMember = "Value";
        }

        public new object DataSource
        {
            get { return base.DataSource; }
            set
            {
                string displayMem = string.IsNullOrEmpty(this.DisplayMember) ? "Value" : this.DisplayMember;
                string valueMem = this.ValueMember;

                base.DataSource = value;
                
                if (string.IsNullOrEmpty(this.DisplayMember) && string.IsNullOrEmpty(displayMem))
                {
                    base.DisplayMember = displayMem;
                    base.ValueMember = valueMem;
                }
                else
                {
                    base.DisplayMember = "Value";
                    base.ValueMember = "Key";
                }
            }
        }
    }
}
