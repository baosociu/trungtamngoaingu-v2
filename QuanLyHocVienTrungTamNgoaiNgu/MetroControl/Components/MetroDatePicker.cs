﻿using MetroFramework.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetroControl.Components
{
    public partial class MetroDatePicker : MetroDateTime
    {
        MetroDataType typeData;
        [Browsable(true), EditorBrowsable(EditorBrowsableState.Always)]
        public MetroDataType TypeData { get => typeData; set => typeData = value; }
        public MetroDatePicker()
        {
            InitializeComponent();
            this.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.Font = new System.Drawing.Font(this.Font.FontFamily, 10);
            this.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.TypeData = new MetroDataType();
            this.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Format = System.Windows.Forms.DateTimePickerFormat.Short;
        }

    }
}
