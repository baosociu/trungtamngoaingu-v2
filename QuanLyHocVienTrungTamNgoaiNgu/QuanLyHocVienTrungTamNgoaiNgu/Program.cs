﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyHocVienTrungTamNgoaiNgu
{
    public static class Program
    {
        public static frmLogin login;
        public static frmBoardMain boardMain;
        public static frmConnection connection;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //initial
            login = new frmLogin();
            boardMain = new frmBoardMain();
            connection = new frmConnection();
            Application.Run(login);
        }
    }
}
