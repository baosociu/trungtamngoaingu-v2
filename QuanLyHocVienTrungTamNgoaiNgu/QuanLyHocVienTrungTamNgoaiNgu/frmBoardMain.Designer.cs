﻿namespace QuanLyHocVienTrungTamNgoaiNgu
{
    partial class frmBoardMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBoardMain));
            this.metroMenu1 = new MetroControl.Controls.MetroMenu();
            this.SuspendLayout();
            // 
            // metroMenu1
            // 
            this.metroMenu1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroMenu1.Location = new System.Drawing.Point(20, 60);
            this.metroMenu1.MaxSizeMenu = 240;
            this.metroMenu1.Name = "metroMenu1";
            this.metroMenu1.Size = new System.Drawing.Size(920, 640);
            this.metroMenu1.TabIndex = 0;
            // 
            // frmBoardMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(960, 720);
            this.Controls.Add(this.metroMenu1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmBoardMain";
            this.Text = "HỆ THỐNG QUẢN LÝ HỌC VIÊN TRUNG TÂM NGOẠI NGỮ";
            this.ResumeLayout(false);

        }

        #endregion

        private MetroControl.Controls.MetroMenu metroMenu1;
        private MetroControl.Controls.MetroItemMenu metroItemMenu1;
    }
}