﻿namespace QuanLyHocVienTrungTamNgoaiNgu
{
    partial class frmConnection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConnection));
            this.metroConnection1 = new MetroControl.Controls.MetroConnection();
            this.SuspendLayout();
            // 
            // metroConnection1
            // 
            this.metroConnection1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroConnection1.Location = new System.Drawing.Point(20, 60);
            this.metroConnection1.Name = "metroConnection1";
            this.metroConnection1.Size = new System.Drawing.Size(400, 360);
            this.metroConnection1.TabIndex = 0;
            // 
            // frmConnection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(440, 440);
            this.Controls.Add(this.metroConnection1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmConnection";
            this.Text = "Kết nối cơ sở dữ liệu";
            this.ResumeLayout(false);

        }

        #endregion

        private MetroControl.Controls.MetroConnection metroConnection1;
    }
}