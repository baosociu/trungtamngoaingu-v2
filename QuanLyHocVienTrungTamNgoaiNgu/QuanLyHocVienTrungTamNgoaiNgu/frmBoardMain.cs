﻿using MetroControl;
using MetroControl.Controls;
using MetroDataset;
using MetroFramework.Forms;
using MetroFramework.Properties;
using MetroUserControl;
using MetroUserControl.ContentMenu.Account;
using MetroUserControl.ContentMenu.Class;
using MetroUserControl.ContentMenu.Mail;
using MetroUserControl.ContentMenu.Permission;
using MetroUserControl.ContentMenu.Receipt;
using MetroUserControl.ContentMenu.Roll_up;
using MetroUserControl.ContentMenu.Schedule;
using MetroUserControl.ContentMenu.Schedule.Staff;
using MetroUserControl.ContentMenu.Schedule.Student;
using MetroUserControl.ContentMenu.Schedule.Teacher;
using MetroUserControl.ContentMenu.Score;
using MetroUserControl.ContentMenu.Summary;
using MetroUserControl.ContentMenu.Systems;
using MetroUserControl.ContentMenu.User;
using MetroUserControl.ContentMenu.User.Staff;
using MetroUserControl.ContentMenu.User.Student;
using MetroUserControl.ContentMenu.User.Teacher;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyHocVienTrungTamNgoaiNgu
{
    public partial class frmBoardMain : MetroForm
    {
        public frmBoardMain()
        {
            InitializeComponent();
            this.FormClosing += FrmBoardMain_FormClosing;
            metroMenu1.SetHelper(Properties.Resources.help_white, Menu_Helper_Click);
            this.Load += FrmBoardMain_Load;
        }

        private void FrmBoardMain_Load(object sender, EventArgs e)
        {
            (new MetroDataset.Update()).CapNhatTinhTrangLichHoc();
            (new MetroDataset.Update()).CapNhatTinhTrangLopHoc();

        }

        public void SetupMenuByAccount(string mataikhoan)
        {

            //lấy danh sách chi tiết thao tác quyền
            MetroHelper.dschitietquyen = (new Select()).LayDanhSachChiTietThaoTacQuyen(mataikhoan);

            MetroDataset.Select select = new MetroDataset.Select();
            List<string> manhinhs = select.HienThiGiaoDienTheoTaiKhoan(MetroHelper.dschitietquyen);
            if (manhinhs == null)
                manhinhs = new List<string>();
            List<MetroItemMenu> list = ListItemMenu(mataikhoan);

            //Lọc theo nhóm quyền
            list = FilterByGroupPermission(list, manhinhs);
            if (list.Count == 0)
            {
                MetroItemMenu I = GetDetailUserMenu(mataikhoan);
                if (I != null)
                    list.Add(I);
            }



            metroMenu1.ClearItems();
            if (list.Count != 0)
            {
                metroMenu1.SetItemsAdapter(list, 0);
                this.Show();
            }
            else
            {
                EventHelper.Helper.MessageInfomation("Tài khoản không hợp lệ.");

                this.Show();
                this.Close();
            }
        }

        private MetroItemMenu GetDetailUserMenu(string mataikhoan)
        {
            if (mataikhoan.StartsWith("HV"))
                //TODO: Nếu tài khoản là học viên
                return (new MetroItemMenu()
                {
                    ImageItemSelected = Properties.Resources.home,
                    ImageItem = Properties.Resources.home_white
                }.SetContent(new MetroContentMenu().AddUserControl(new MetroDetailStudent().SetAs(MetroDetailStudent.Type.ChangePass))));

            if (mataikhoan.StartsWith("GV"))
                //TODO: Nếu tài khoản là giảng viên
                return (new MetroItemMenu()
                {
                    ImageItemSelected = Properties.Resources.home,
                    ImageItem = Properties.Resources.home_white
                }.SetContent(new MetroContentMenu().AddUserControl(
                    new MetroDetailTeacher()
                    .SetAs(MetroDetailTeacher.Type.ChangePass))));

            if (mataikhoan.StartsWith("NV"))
                //TODO: nếu tài khoản là nhân viên
                return (new MetroItemMenu()
                {
                    ImageItemSelected = Properties.Resources.home,
                    ImageItem = Properties.Resources.home_white
                }.SetContent(new MetroContentMenu().AddUserControl(new MetroDetailStaff()
                .SetAs(MetroDetailStaff.Type.ChangePass))));
            return null;
        }

        private List<MetroItemMenu> FilterByGroupPermission(List<MetroItemMenu> list, List<string> manhinhs)
        {
            List<MetroItemMenu> menu = new List<MetroItemMenu>();
            foreach (MetroItemMenu it in list)
                if (manhinhs.Contains(it.ID))
                    menu.Add(it);
                else
                {
                    if (((MetroContentMenu)it.UserControl).GetChildUserControl() is MetroTabListUser)
                    {
                        if ((((MetroContentMenu)it.UserControl).GetChildUserControl() as MetroTabListUser).ShowTabById(manhinhs))
                            menu.Add(it);

                    }
                }
            return menu;
        }

        private List<MetroItemMenu> ListItemMenu(string taikhoan)
        {
            MetroHelper.SetCurrentUser(taikhoan);

            //hello
            string onlyname = MetroHelper.GetOnlyNameUser();
            metroMenu1.SetNameUser(onlyname);

            List<MetroItemMenu> list = new List<MetroItemMenu>();
            //TODO: Nếu tài khoản là học viên
            list.Add(new MetroItemMenu()
            {
                ImageItemSelected = Properties.Resources.home,
                ImageItem = Properties.Resources.home_white
            }.SetContent(new MetroContentMenu().AddUserControl(new MetroDetailStudent().SetAs(MetroDetailStudent.Type.ChangePass))));

            //TODO: Nếu tài khoản là giảng viên
            list.Add(new MetroItemMenu()
            {
                ImageItemSelected = Properties.Resources.home,
                ImageItem = Properties.Resources.home_white
            }.SetContent(new MetroContentMenu().AddUserControl(
                new MetroDetailTeacher()
                .SetAs(MetroDetailTeacher.Type.ChangePass))));

            //TODO: nếu tài khoản là nhân viên
            list.Add(new MetroItemMenu()
            {
                ImageItemSelected = Properties.Resources.home,
                ImageItem = Properties.Resources.home_white
            }.SetContent(new MetroContentMenu().AddUserControl(new MetroDetailStaff()
            .SetAs(MetroDetailStaff.Type.ChangePass))));

            list.Add(new MetroItemMenu()
            {
                ImageItemSelected = Properties.Resources.stack,
                ImageItem = Properties.Resources.stack_white
            }.SetContent(new MetroContentMenu().AddUserControl(new MetroTabListUser())));

            //list.Add(new MetroItemMenu()
            //{
            //    ImageItemSelected = Properties.Resources.student,
            //    ImageItem = Properties.Resources.student_white
            //}.SetContent(new MetroContentMenu().AddUserControl(new MetroListUser()
            //.SetAs(MetroListUser.Type.Student))));

            //list.Add(new MetroItemMenu()
            //{
            //    ImageItemSelected = Properties.Resources.teacher,
            //    ImageItem = Properties.Resources.teacher_white
            //}.SetContent(new MetroContentMenu().AddUserControl(new MetroListUser()
            //.SetAs(MetroListUser.Type.Teacher))));

            //list.Add(new MetroItemMenu()
            //{
            //    ImageItemSelected = Properties.Resources.staff,
            //    ImageItem = Properties.Resources.staff_white
            //}.SetContent(new MetroContentMenu().AddUserControl(new MetroListUser()
            //.SetAs(MetroListUser.Type.Staff))));


            list.Add(new MetroItemMenu()
            {
                ImageItemSelected = Properties.Resources.receipt,
                ImageItem = Properties.Resources.receipt_white
            }.SetContent(new MetroContentMenu().AddUserControl(new MetroListReceipt()
            )));

            list.Add(new MetroItemMenu()
            {
                ImageItemSelected = Properties.Resources.group,
                ImageItem = Properties.Resources.group_white
            }.SetContent(new MetroContentMenu().AddUserControl(new MetroListClass()
            )));

            list.Add(new MetroItemMenu()
            {
                ImageItemSelected = Properties.Resources.calendar,
                ImageItem = Properties.Resources.calendar_white
            }.SetContent(new MetroContentMenu().AddUserControl(new MetroListSchedule())));

            list.Add(new MetroItemMenu()
            {
                ImageItemSelected = Properties.Resources.calendar,
                ImageItem = Properties.Resources.calendar_white
            }.SetContent(new MetroContentMenu().AddUserControl(new MetroListScheduleStudent())));

            list.Add(new MetroItemMenu()
            {
                ImageItemSelected = Properties.Resources.calendar,
                ImageItem = Properties.Resources.calendar_white
            }.SetContent(new MetroContentMenu().AddUserControl(new MetroListScheduleTeacher())));

            list.Add(new MetroItemMenu()
            {
                ImageItemSelected = Properties.Resources.achievement,
                ImageItem = Properties.Resources.achievement_white
            }.SetContent(new MetroContentMenu().AddUserControl(new MetroListScore()
            )));

            list.Add(new MetroItemMenu()
            {
                ImageItemSelected = Properties.Resources.system,
                ImageItem = Properties.Resources.system_white
            }.SetContent(new MetroContentMenu().AddUserControl(new MetroListSystem()
            )));

            list.Add(new MetroItemMenu()
            {
                ImageItemSelected = Properties.Resources.user,
                ImageItem = Properties.Resources.user_white
            }.SetContent(new MetroContentMenu().AddUserControl(new MetroListAccount()
            )));

            list.Add(new MetroItemMenu()
            {
                ImageItemSelected = Properties.Resources.permission,
                ImageItem = Properties.Resources.permission_white
            }.SetContent(new MetroContentMenu().AddUserControl(new MetroListPermission()
            )));

            list.Add(new MetroItemMenu()
            {
                ImageItemSelected = Properties.Resources.report,
                ImageItem = Properties.Resources.report_white
            }.SetContent(new MetroContentMenu().AddUserControl(new MetroTabListSummary()
            )));


            list.Add(new MetroItemMenu()
            {
                ImageItemSelected = Properties.Resources.rollup,
                ImageItem = Properties.Resources.rollup_white
            }.SetContent(new MetroContentMenu().AddUserControl(new MetroListRollUp()
            )));

            list.Add(new MetroItemMenu()
            {
                ImageItemSelected = Properties.Resources.score,
                ImageItem = Properties.Resources.score_white
            }.SetContent(new MetroContentMenu().AddUserControl(new MetroViewScore()
            )));


            return list;
        }

        private void Menu_Helper_Click(object sender, EventArgs e)
        {

            UserControl current = this.metroMenu1.GetItemCurrent();
            if (current is MetroTabListUser)
                BieuMau.XuatFileWordHelper(BieuMau.BIEUMAU_HELPER_QUANLYNGUOIDUNG);
            else if (current is MetroListReceipt)
                BieuMau.XuatFileWordHelper(BieuMau.BIEUMAU_HELPER_QUANLYBIENLAI);
            else if (current is MetroListClass)
                BieuMau.XuatFileWordHelper(BieuMau.BIEUMAU_HELPER_QUANLYLOPHOC);
            else if (current is MetroListSchedule)
                BieuMau.XuatFileWordHelper(BieuMau.BIEUMAU_HELPER_QUANLYLICH);
            else if (current is MetroListScheduleStudent)
                BieuMau.XuatFileWordHelper(BieuMau.BIEUMAU_HELPER_XEMLICHHOC);
            else if (current is MetroListScheduleTeacher)
                BieuMau.XuatFileWordHelper(BieuMau.BIEUMAU_HELPER_XEMLICHDAY);
            else if (current is MetroListScore)
                BieuMau.XuatFileWordHelper(BieuMau.BIEUMAU_HELPER_QUANLYDIEM);
            else if (current is MetroListAccount)
                BieuMau.XuatFileWordHelper(BieuMau.BIEUMAU_HELPER_QUANLYTAIKHOAN);
            else if (current is MetroListPermission)
                BieuMau.XuatFileWordHelper(BieuMau.BIEUMAU_HELPER_QUANLYPHANQUYEN);
            else if (current is MetroViewScore)
                BieuMau.XuatFileWordHelper(BieuMau.BIEUMAU_HELPER_XEMKETQUAHOCTAP);
            else if (current is MetroListRollUp)
                BieuMau.XuatFileWordHelper(BieuMau.BIEUMAU_HELPER_QUANLYDIEMDANH);
            else if (current is MetroListSystem)
                BieuMau.XuatFileWordHelper(BieuMau.BIEUMAU_HELPER_QUANLYHETHONG);
            else if (current is MetroTabListSummary)
                BieuMau.XuatFileWordHelper(BieuMau.BIEUMAU_HELPER_QUANLYTHONGKE);
            else if (current is MetroDetailStaff || current is MetroDetailStudent || current is MetroDetailTeacher)
                BieuMau.XuatFileWordHelper(BieuMau.BIEUMAU_HELPER_THONGTINCANHAN);

        }

        private void FrmBoardMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            EventHelper.Helper.MessageInfomationYesNo("Đăng xuất khỏi hệ thống ?", () =>
            {
                Program.login.Show();
                MetroHelper.ClearUser();
                e.Cancel = false;
            },()=> e.Cancel = true);
            

        }



    }
}
