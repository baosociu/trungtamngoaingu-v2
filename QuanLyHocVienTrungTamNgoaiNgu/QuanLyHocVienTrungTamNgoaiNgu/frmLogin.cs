﻿using Export;
using MetroControl;
using MetroControl.Components;
using MetroDataset;
using MetroDataset.Model;
using MetroFramework.Forms;
using QuanLyHocVienTrungTamNgoaiNgu.Properties;
using SQLHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyHocVienTrungTamNgoaiNgu
{
    public partial class frmLogin : MetroForm
    {
        public frmLogin()
        {
            InitializeComponent();
            this.Load += FrmLogin_Load;
            this.metroLogin.username = "NV0001";
            this.metroLogin.password = "1234";
            this.TopMost = true;


        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            metroLogin.SetEventButtonCancel(clickHuy);
            metroLogin.SetEventButtonLogin(clickLogin);
            metroLogin.SetEventButtonConnection(() =>
            {
                this.Hide();
                Program.connection.Show();
            });
             capnhatmatkhau(); // chi chạy 1 lần Khi restart csdl

        }

        #region nguy hiểm
        private void capnhatmatkhau()
        {
            using (MetroUserControl.DataSetQuanLyHocVienTableAdapters.NGUOIDUNGTableAdapter
                nd = new MetroUserControl.DataSetQuanLyHocVienTableAdapters.NGUOIDUNGTableAdapter())
            {
                string PASS = "1234";
                DataTable dt = nd.GetData();
                foreach (DataRow r in dt.Rows)
                {
                    string USER = r["MATAIKHOAN"].ToString();
                    string _PASS = TripleDES.TripleDES.Crypto.EncryptAccount(USER, PASS);
                    using (MetroUserControl.DataSetQuanLyHocVienTableAdapters.QueriesTableAdapter
                        q = new MetroUserControl.DataSetQuanLyHocVienTableAdapters.QueriesTableAdapter())
                    {
                        q.CapNhatMatKhau(USER, _PASS);
                    }
                }
            }
        }
        #endregion

        private void clickLogin()
        {
            bool isSuccess = MetroDataset.Helper.CheckConnection();

            if (!isSuccess)
            {
                EventHelper.Helper.MessageInfomation("Nguồn kết nối không tồn tại, hãy kiểm tra lại.");
                showConnectionForm();
                return;
            }

            string user = this.metroLogin.username;
            string pass = this.metroLogin.password;

            bool invalid = invalidInput(user, pass);
            if (invalid)
                return;

            string _pass = TripleDES.TripleDES.Crypto.EncryptAccount(user, pass);

            Func<ResultExcuteQuery> f = new Func<ResultExcuteQuery>(() => { return (new Select()).Login(user, _pass); });
            object o = (ResultExcuteQuery)EventHelper.Helper.TryCatchReturnValue(f);
            if (o == null)
            {
                EventHelper.Helper.MessageInfomation("Cơ sở dữ liệu không hợp lệ. Hãy cài đặt lại nguồn kết nối");
                Helper.SetConnectionNull();
                showConnectionForm();
                return;
            }

            ResultExcuteQuery r = (ResultExcuteQuery)o;
            EventHelper.Helper.MessageInfomation(r.GetMessage());



            if (r.isSuccessfully())
                showLoginSuccessfulAsync();
        }

        private void clickHuy()
        {
            this.Close();
        }

        private bool invalidInput(string user, string pass)
        {
            //return false nếu hợp lệ
            //return true nếu ko hợp lệ
            if (string.IsNullOrWhiteSpace(user) || string.IsNullOrWhiteSpace(pass))
            {
                EventHelper.Helper.MessageInfomation("Hãy cung cấp đầy đủ \"Tên tài khoản\" và \"Mật khẩu\".");
                if (string.IsNullOrWhiteSpace(user))
                    this.metroLogin.FocusUsername();
                else
                    this.metroLogin.FocusPassword();
                return true;
            }
            return false;
        }

        private void showConnectionForm()
        {
            Program.connection.Show();
            this.Hide();
        }
        Thread t;
        private void showLoginSuccessfulAsync()
        {
            //new Thread(() =>
            //{
            //    VisibleProgress(metroProgressBar1, true);
            //    while (this.Visible)
            //    {
            //        Thread.Sleep(50);
            //        SetValueProgress(metroProgressBar1);
            //    }

            //}).Start();

            //Program.boardMain = new frmBoardMain();
            //new Thread(() =>
            //{
            //    Program.boardMain.SetupMenuByAccount(this.metroLogin.username);

            //    // HideForm(Program.login);
            //    VisibleProgress(metroProgressBar1, false);
            //}).Start();

            Program.boardMain = new frmBoardMain();
            Program.boardMain.SetupMenuByAccount(this.metroLogin.username);
            this.Hide();
        }


        delegate void VisibleProgressCallBack(MetroProgressBar progress, bool visible);
        private void VisibleProgress(MetroProgressBar progress, bool visible)
        {
            if (progress.InvokeRequired)
            {
                VisibleProgressCallBack d = new VisibleProgressCallBack(VisibleProgress);
                this.Invoke(d, new object[] { progress, visible });
            }
            else
            {
                progress.Value = 0;
                progress.Visible = visible;
                progress.Refresh();
            }
        }

        delegate void HideFormCallBack(Form f);
        private void HideForm(Form f)
        {
            if (f.InvokeRequired)
            {
                HideFormCallBack d = new HideFormCallBack(HideForm);
                this.Invoke(d, new object[] { f });
            }
            else
            {
                f.Hide();
            }
        }

        delegate void SetValueProgressCallBack(MetroProgressBar progress);
        private void SetValueProgress(MetroProgressBar progress)
        {
            if (progress.InvokeRequired)
            {
                SetValueProgressCallBack d = new SetValueProgressCallBack(SetValueProgress);
                this.Invoke(d, new object[] { progress });
            }
            else
            {
                if (progress.Value == 100)
                    progress.Value = 0;
                progress.Value++;
            }
        }
    }

}
