﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyHocVienTrungTamNgoaiNgu
{
    public partial class frmConnection : MetroForm
    {
        public frmConnection()
        {
            InitializeComponent();
            this.Load += FrmConnection_Load;
            this.FormClosing += FrmConnection_FormClosing;
        }

        private void FrmConnection_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.login.Show();
            this.Hide();
            e.Cancel = true;
        }

        private void FrmConnection_Load(object sender, EventArgs e)
        {
            this.metroConnection1.SetOkPerform(MetroDataset.Helper.SetConnectionNull);
        }
    }
}
