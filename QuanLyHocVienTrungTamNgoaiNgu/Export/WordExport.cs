﻿using Syncfusion.DocIO;
using Syncfusion.DocIO.DLS;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Export
{
    public class WordExport : IDisposable
    {
        #region ---- Constants & Enum -----



        #endregion

        #region --- Member Variables ----

        private bool isPrintPreview = false;
        private String file_save_name;
        private String file_template_name;
        private String folder_save_name = "Save";
        private String folder_template_name = "Template";
        private String AppPath;

        private String pathTemplateFile;
        private String pathSaveFile;
        #endregion

        #region --- Private medthods ---

        public WordExport(string AppPath, string filenameTemplate, string filenameSave, bool isPrintPreview)
        {
            this.AppPath = AppPath;
            this.file_save_name = filenameSave + ".docx";
            this.file_template_name = filenameTemplate + ".docx";
            this.isPrintPreview = isPrintPreview;

            this.pathSaveFile = AppPath + Constants.CHAR_FLASH + folder_save_name + Constants.CHAR_FLASH + file_save_name;
            this.pathTemplateFile = AppPath + Constants.CHAR_FLASH + folder_template_name + Constants.CHAR_FLASH + file_template_name;
        }

        private void PrinPriview(string fileToPrint)
        {
            object missing = System.Type.Missing;
            object objFile = fileToPrint;
            object readOnly = true;
            object addToRecentOpen = false;

            Microsoft.Office.Interop.Word._Application wordApplication = new Microsoft.Office.Interop.Word.Application();
            try
            {
                Microsoft.Office.Interop.Word._Document wordDocument = wordApplication.Documents.Open(ref objFile, ref missing, ref readOnly, ref addToRecentOpen);

                wordApplication.Options.SaveNormalPrompt = false;

                if (wordDocument != null)
                {
                    wordApplication.Visible = true;
                    wordDocument.PrintPreview();
                    wordDocument.Activate();
                    while (isPrintPreview)
                    {
                        wordDocument.ActiveWindow.View.Magnifier = true;
                        Thread.Sleep(500);
                    }

                    wordDocument.Close(ref missing, ref missing, ref missing);
                    wordDocument = null;
                }
            }
            catch
            {

            }
            finally
            {
                wordApplication.Quit(ref missing, ref missing, ref missing);
                wordApplication = null;
            }
        }


        private void WordExport_DocumentBeforeClose(Microsoft.Office.Interop.Word.Document Doc, ref bool Cancel)
        {
            isPrintPreview = false;
        }


        private void Merge(string[] filesToMerge, string outputFilename, bool insertPageBreaks)
        {
            Merge(filesToMerge, outputFilename, insertPageBreaks, pathTemplateFile);
        }

        private void Merge(string[] filesToMerge, string outputFilename, bool insertPageBreaks, string documentTemplate)
        {
            object defaultTemplate = documentTemplate;
            object missing = System.Type.Missing;
            object pageBreak = Microsoft.Office.Interop.Word.WdBreakType.wdPageBreak;
            object outputFile = outputFilename;

            Microsoft.Office.Interop.Word._Application wordApplication = new Microsoft.Office.Interop.Word.Application();
            if (filesToMerge.Count() == 1)
                pageBreak = false;
            try
            {
                Microsoft.Office.Interop.Word._Document wordDocument = wordApplication.Documents.Add(ref defaultTemplate, ref missing, ref missing, ref missing);

                Microsoft.Office.Interop.Word.Selection selection = wordApplication.Selection;

                int index = 0;

                foreach (string file in filesToMerge)
                {
                    selection.InsertFile(file, ref missing, ref missing, ref missing, ref missing);

                    if (insertPageBreaks && index != filesToMerge.Count() - 1)
                    {
                        selection.InsertBreak(ref pageBreak);
                    }

                    index++;
                }

                wordDocument.SaveAs(ref outputFile, ref missing, ref missing, ref missing, ref missing,
                                     ref missing, ref missing, ref missing, ref missing, ref missing,
                                     ref missing, ref missing, ref missing, ref missing, ref missing, ref missing);

                wordDocument.Close(ref missing, ref missing, ref missing);
                wordDocument = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                wordApplication.Quit(ref missing, ref missing, ref missing);
            }
        }

        private void PageSetup(ref WordDocument document)
        {
            foreach (WSection section in document.Sections)
            {

                section.PageSetup.Margins.Top = 72f;
                section.PageSetup.Margins.Bottom = 90f;
                section.PageSetup.Margins.Left = 72f;
                section.PageSetup.Margins.Right = 72f;

                section.PageSetup.HeaderDistance = 1;
                section.PageSetup.FooterDistance = 1;

                section.PageSetup.PageSize = PageSize.A4;

                section.PageSetup.Orientation = PageOrientation.Landscape;
            }
        }

        #endregion

        public void Export(byte[] fileBytes, Func<object[]> func)
        {
            new Thread(() =>
            {
                WordDocument doc = GetDocument(fileBytes);
                object[] o = func();
                SaveFile(doc, (string[])o[0], (string[])o[1]);
            }).Start();
        }

        private void SaveFile(WordDocument doc, string[] fields, string[] values)
        {
            doc.MailMerge.Execute(fields, values);
            doc.Save(pathSaveFile, FormatType.Docx);
            doc.Close();
            this.PrinPriview(pathSaveFile);
        }

        private WordDocument GetDocument(byte[] fileBytes)
        {
            MemoryStream mStream = null;
            WordDocument document = null;
            CultureInfo vietnam = new CultureInfo(1066);
            NumberFormatInfo vnfi = vietnam.NumberFormat;
            vnfi.CurrencySymbol = Constants.VN_UNIT;
            vnfi.CurrencyDecimalSeparator = Constants.CHAR_COMMA;
            vnfi.CurrencyDecimalDigits = 0;

            String path_save_folder = AppPath + Constants.CHAR_FLASH + folder_save_name;
            String path_template_folder = AppPath + Constants.CHAR_FLASH + folder_template_name;

            if (!Directory.Exists(path_save_folder))
                Directory.CreateDirectory(path_save_folder);
            if (!Directory.Exists(path_template_folder))
                Directory.CreateDirectory(path_template_folder);
            //File.WriteAllBytes(this.pathSaveFile, fileBytes);

            //string[] DocFile = Directory.GetFiles(path_save_folder + Constants.CHAR_FLASH, Constants.CHAR_STAR + Constants.FILE_EXT_DOCX);

            bool b = false;
            do
            {
                try
                {

                    if (File.Exists(pathSaveFile))
                        File.Delete(pathSaveFile);
                    if (File.Exists(pathTemplateFile))
                        File.Delete(pathTemplateFile);
                    File.WriteAllBytes(pathTemplateFile, fileBytes);
                    b = true;
                }
                catch (Exception e)
                {
                    MessageBox.Show("Đóng tập tin đang mở để tiếp tục.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            } while (!b);

            try
            {

                File.Copy(pathTemplateFile, pathSaveFile); //CLONE
                mStream = new MemoryStream(File.ReadAllBytes(pathTemplateFile));
                document = new WordDocument(mStream);
                mStream.Close();
            }
            catch
            {

                return null;
            }
            return document;
        }



        //******************************************SET VALUES

        public object[] TroGiup()
        {
            return new object[] { new string[] { }, new string[] { } };
        }

        public object[] BienLai(params string[] args)
        {
            DateTime now = DateTime.Now;
            string mabieumau = args[0];
            string mabienlai = args[1];
            string ngay = now.Day.ToString();
            string thang = now.Month.ToString();
            string nam = now.Year.ToString();
            string madangky = args[2];
            string malophoc = args[3];
            string mahocvien = args[4];
            string hotenhocvien = args[5];
            string khoahoc = args[6];
            string sotien = args[7];
            string loaibienlai = args[8];
            string lanthu = args[9];
            string nhanvien = args[10];
            List<string> fields = new List<string> { "mabieumau", "mabienlai", "ngay", "thang", "nam", "madangky", "malophoc", "mahocvien",
                "hotenhocvien", "khoahoc", "sotien","loaibienlai", "lanthu", "nhanvien"};
            List<string> values = new List<string> { mabieumau, mabienlai, ngay, thang, nam, madangky, malophoc, mahocvien,
                hotenhocvien, khoahoc, sotien, loaibienlai,lanthu, nhanvien };
            return new object[] { fields.ToArray(), values.ToArray() };

        }

        public object[] PhieuDangKy(params object[] args)
        {
            //params string[] args
            string mahocvien = (string)args[0];
            string tenhocvien = (string)args[1];
            string ngaysinh = (string)args[2];
            string gioitinh = (string)args[3];
            string cmnd = (string)args[4];
            string dienthoai = (string)args[5];
            string maphieudangky = (string)args[6];
            string khoahoc = (string)args[7];
            string langhidanh = (string)args[8];
            string diemdauvao = (string)args[9];
            List<int[]> lISt = (List<int[]>)args[10];
            string nhanvien = (string)args[11];
            string ngay = DateTime.Now.Day.ToString();
            string thang = DateTime.Now.Month.ToString();
            string nam = DateTime.Now.Year.ToString();
            List<string> fields = new List<string> { "mahocvien", "tenhocvien", "ngaysinh", "gioitinh", "cmnd", "dienthoai", "maphieudangky", "khoahoc", "langhidanh", "diemdauvao", "nhanvien", "ngay", "thang", "nam" };
            List<string> values = new List<string> { mahocvien, tenhocvien, ngaysinh, gioitinh, cmnd, dienthoai, maphieudangky, khoahoc, langhidanh, diemdauvao, nhanvien, ngay, thang, nam };

            foreach (int[] int_array in lISt)
            {
                int thu = int_array[0] - 1;
                if (thu == -1)
                    thu = 6;
                int ca = int_array[1] - 1;
                fields.Add("cell" + ca.ToString() + thu.ToString());
                values.Add("X");
            }

            return new object[] { fields.ToArray(), values.ToArray() };
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
