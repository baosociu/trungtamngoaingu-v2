﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Export
{
    public class Constants
    {
        public static string FILTER_EXCEL = "Excel file (*.xlsx)|*.xlsx|All files (*.*)|*.*";
        public static string FILE_EXT_XLS = ".xlsx";
        public static string VN_UNIT = "vi-VN";
        public static string CHAR_COMMA = ",";
        public static string CHAR_FLASH = "\\";
        public static string CHAR_STAR = "*";
        public static string FILE_EXT_DOCX = ".docx";
    }
}
