﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Export
{
    public class Excel
    {
        string author;
        string title;
        public bool completed;
        SaveFileDialog saveFileDialog1;

        public Excel()
        {
            this.completed = true;

            SetUpSaveDiaglog();

        }
        private void SetUpSaveDiaglog()
        {
            saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Title = "Lưu danh sách";
            saveFileDialog1.DefaultExt = "xlsx";
            saveFileDialog1.Filter = "Excel file (*.xlsx)|*.xlsx|All files (*.*)|*.*";
        }

        public void SaveAsFromListData(string author, string title,Func<List<object>> list)
        {
            this.author = author;
            this.title = title;
            this.completed = true;

            saveFileDialog1.FileName = title;

            DialogResult r = saveFileDialog1.ShowDialog();

            if (r == DialogResult.OK)
            {
               new Thread(() => SaveAsFromListDataThreading(list)).Start();
            }
        }

        private void SaveAsFromListDataThreading(Func<List<object>> func_list)
        {
            completed = false;

            List<object> list = func_list();

            if(list.Count == 0)
            {
                MessageBox.Show("Dữ liệu rỗng ! Hãy thử lại !", "Thông báo", MessageBoxButtons.OK);
                completed = true;
                return;
            }

            FileInfo file = new System.IO.FileInfo(saveFileDialog1.FileName);
            using (ExcelPackage pck = new ExcelPackage(file))
            {

                pck.Workbook.Properties.Author = author;
                pck.Workbook.Properties.Title = title;

                int colnumber = list[0].GetType().GetProperties().Length;
                int rownumber = list.Count;

                while (pck.Workbook.Worksheets.Count != 0)
                    pck.Workbook.Worksheets.Delete(1);

                pck.Workbook.Worksheets.Add(title);
                ExcelWorksheet worksheet = pck.Workbook.Worksheets[title];

                worksheet.Cells[1, 1, 1, colnumber].Merge = true;
                worksheet.Cells[1, 1].Value = title.ToUpper();
                SetAlignment(worksheet, 1, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                SetFontSize(worksheet, 1, 1, 18);
                SetFontStyleBold(worksheet, 1, 1);

                for (int j = 0; j < colnumber; j++)
                {
                    worksheet.Cells[2, j + 1].Value = list[0].GetType().GetProperties()[j].Name.ToUpper().Replace("_"," ");
                    SetAlignment(worksheet, 2, j + 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                    SetBorder(worksheet, 2, j + 1);
                    SetFontStyleBold(worksheet, 2, j + 1);
                    SetBackgroundColor(worksheet, 2, j + 1, Color.Yellow);
                    SetFontColor(worksheet, 2, j + 1, Color.Red);
                }
                for (int i = 0; i < rownumber; i++)
                {
                    string[] arr_value = GetValuesFromAnonymous(list[i]);

                    for (int j = 0; j < colnumber; j++)
                    {
                        string v = arr_value[j];
                        worksheet.Cells[i + 3, j + 1].Value = v;
                        //if (grid.Columns[j].DefaultCellStyle.Alignment == DataGridViewContentAlignment.MiddleCenter)
                        //    SetAlignment(worksheet, i + 3, j + 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetBorder(worksheet, i + 3, j + 1);
                    }
                }

                for (int j = 0; j < colnumber; j++)
                    worksheet.Column(j + 1).AutoFit();
                pck.SaveAs(file);
                
                
            }
            completed = true;
        }

        private string[] GetValuesFromAnonymous(object v)
        {
            string str = v.ToString();
            str = str.Replace("{ ", "");
            str = str.Replace(" }", "");
            List<string> list = new List<string>();
            PropertyInfo[] properties = v.GetType().GetProperties();
            int n = properties.Length;
            for (int i = 0; i <  n-1;i++)
            {
                PropertyInfo p = properties[i];
                string name_p = p.Name + " = ";

                PropertyInfo p_next = properties[i+1];
                string name_p_next = ", "+p_next.Name + " = ";

                int start = str.IndexOf(name_p) + name_p.Length;
                int length = str.IndexOf(name_p_next) - start;
                list.Add(str.Substring(start, length));
            }
            string name = properties[n - 1].Name + " = ";

            list.Add(str.Substring(str.IndexOf(name) + name.Length));
            return list.ToArray();
        }

        public void SaveAsFromGrid(DataGridView grid)
        {
            DialogResult r = saveFileDialog1.ShowDialog();
            if (r == DialogResult.OK)
            {
                FileInfo file = new System.IO.FileInfo(saveFileDialog1.FileName);
                using (ExcelPackage pck = new ExcelPackage(file))
                {

                    pck.Workbook.Properties.Author = author;
                    pck.Workbook.Properties.Title = title;

                    int colnumber = grid.ColumnCount;

                    while (pck.Workbook.Worksheets.Count != 0)
                        pck.Workbook.Worksheets.Delete(1);

                    pck.Workbook.Worksheets.Add(title);
                    ExcelWorksheet worksheet = pck.Workbook.Worksheets[title];

                    worksheet.Cells[1, 1, 1, colnumber].Merge = true;
                    worksheet.Cells[1, 1].Value = title.ToUpper();
                    SetAlignment(worksheet, 1, 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                    SetFontSize(worksheet, 1, 1, 18);
                    SetFontStyleBold(worksheet, 1, 1);

                    for (int j = 0; j < grid.ColumnCount; j++)
                    {
                        worksheet.Cells[2, j + 1].Value = grid.Columns[j].HeaderText;
                        SetAlignment(worksheet, 2, j + 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                        SetBorder(worksheet, 2, j + 1);
                        SetFontStyleBold(worksheet, 2, j + 1);
                        SetBackgroundColor(worksheet, 2, j + 1, Color.Yellow);
                        SetFontColor(worksheet, 2, j + 1, Color.Red);
                    }
                    for (int i = 0; i < grid.RowCount; i++)
                        for (int j = 0; j < grid.ColumnCount; j++)
                        {
                            DataGridViewCell cell = grid.Rows[i].Cells[j];
                            worksheet.Cells[i + 3, j + 1].Value = cell.Value;
                            if (grid.Columns[j].DefaultCellStyle.Alignment == DataGridViewContentAlignment.MiddleCenter)
                                SetAlignment(worksheet, i + 3, j + 1, OfficeOpenXml.Style.ExcelHorizontalAlignment.Center);
                            SetBorder(worksheet, i + 3, j + 1);
                        }

                    for (int j = 0; j < grid.ColumnCount; j++)
                        worksheet.Column(j + 1).AutoFit();
                    pck.SaveAs(file);
                }
            }
        }


        private void SetFontSize(ExcelWorksheet worksheet, int row, int column, int size)
        {
            string address = getStringAddress(worksheet, row, column);
            worksheet.Cells[address].Style.Font.Size = size;
        }

        private void SetFontColor(ExcelWorksheet worksheet, int row, int column, Color color)
        {
            string address = getStringAddress(worksheet, row, column);
            worksheet.Cells[address].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells[address].Style.Font.Color.SetColor(color);
        }

        private void SetBackgroundColor(ExcelWorksheet worksheet, int row, int column, Color color)
        {
            string address = getStringAddress(worksheet, row, column);
            worksheet.Cells[address].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells[address].Style.Fill.BackgroundColor.SetColor(color);

        }

        private void SetFontStyleBold(ExcelWorksheet worksheet, int row, int column)
        {
            string address = getStringAddress(worksheet, row, column);
            worksheet.Cells[address].Style.Font.Bold = true;
        }

        private void SetBorder(ExcelWorksheet worksheet, int row, int column)
        {
            string address = getStringAddress(worksheet, row, column);
            worksheet.Cells[address].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
        }

        private void SetAlignment(ExcelWorksheet worksheet, int row, int column, OfficeOpenXml.Style.ExcelHorizontalAlignment alignment)
        {
            string address = getStringAddress(worksheet, row, column);
            worksheet.Cells[address].Style.HorizontalAlignment = alignment;
        }

        private string getStringAddress(ExcelWorksheet worksheet, int row, int column)
        {
            return worksheet.Cells[row, column].Address;
        }

    }
}
