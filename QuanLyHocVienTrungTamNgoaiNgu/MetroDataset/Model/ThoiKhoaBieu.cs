﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetroDataset.Model
{
    public class ThoiKhoaBieu
    {
        List<LichHoc> data;
        List<PhongHoc> phongs;
        string type;
        DateTime start;
        DateTime end;
    

        public ThoiKhoaBieu(List<LichHoc> data, List<PhongHoc> phongs)
        {
            if (data == null || data.Count == 0)
                return;

            this.phongs = phongs.Select(t => new PhongHoc(t.Maphong,t.Macoso, t.Tencoso,t.Soluongchongoi)).ToList<PhongHoc>();
            this.Data = data.Select(t => new LichHoc(t.Thu, t.Ca, t.Ngay)).ToList<LichHoc>();
            this.Data = this.Data.OrderBy(t => t.Ngay).ToList<LichHoc>();
            this.Type = CreateType(this.Data);
            this.Start = this.Data[0].Ngay;
            this.End = this.Data[this.Data.Count - 1].Ngay;

        }

        public List<LichHoc> Data { get => data; set => data = value; }
        public DateTime Start { get => start; set => start = value; }
        public DateTime End { get => end; set => end = value; }
        public string Type { get => type; set => type = value; }
        public List<PhongHoc> Phongs { get => phongs; set => phongs = value; }

        private string CreateType(List<LichHoc> data)
        {
            string ty = "";
            var c = data.GroupBy(t => t.Thu).OrderBy(t => t.Key).Select(t => (t.Key+1).ToString()).ToList<string>();
            c.ForEach(t => ty = ty + " - " + (int.Parse(t)+1).ToString());
            ty = ty.Replace("1", "CN");
            EventHelper.Helper.TryCatch(() => ty = ty.Substring(3));

            string ca = "";
            var d = data.GroupBy(t => t.Ca).OrderBy(t => t.Key).Select(t => t.Key.ToString()).ToList();
            d.ForEach(t => ca = ca + " - " + t);
            EventHelper.Helper.TryCatch(() => ca = ca.Substring(3));

            return "THỨ "+ ty + "    CA "+ca;
        }


    }
}
