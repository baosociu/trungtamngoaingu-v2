﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetroDataset.Model
{
    public class LichHoc
    {
        int ca;
        int thu;
        DateTime ngay;

        public LichHoc(int thu, int ca, DateTime ngay)
        {
            this.ca = ca;
            this.thu = thu;
            this.ngay = ngay;
        }

        public int Ca { get => ca; set => ca = value; }
        public int Thu { get => thu; set => thu = value; }
        public DateTime Ngay { get => ngay; set => ngay = value; }
    }
}
