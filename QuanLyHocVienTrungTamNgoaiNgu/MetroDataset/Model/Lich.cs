﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetroDataset.Model
{
    public class Lich
    {
        int thu; //1-->7
        int ca; //1-->6
        public Lich(int thu, int ca)
        {
            this.thu = (int)thu;
            this.ca = ca;
        }

        public bool equalsValue(Lich l1, Lich l2)
        {
            return l1.Ca == l2.Ca && l1.Thu == l2.Thu;
        }

        public int Thu { get => thu; set => thu = value; }
        public int Ca { get => ca; set => ca = value; }
       
    }
}
