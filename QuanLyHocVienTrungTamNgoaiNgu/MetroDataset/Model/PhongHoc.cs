﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetroDataset.Model
{

    public class PhongHoc
    {
        string maphong;
        string macoso;
        string tencoso;
        string key;
        int soluongchongoi;

        public PhongHoc(string maphong, string macoso, string tencoso, int soluongchongoi)
        {
            this.maphong = maphong;
            this.macoso = macoso;
            this.tencoso = tencoso;
            this.soluongchongoi = soluongchongoi;
            this.key = this.maphong + " - " + this.macoso;
        }

        public string Tencoso { get => tencoso; set => tencoso = value; }
        public string Macoso { get => macoso; set => macoso = value; }
        public string Maphong { get => maphong; set => maphong = value; }
        public int Soluongchongoi { get => soluongchongoi; set => soluongchongoi = value; }
        public string Key { get => key; set => key = value; }
    }
}
