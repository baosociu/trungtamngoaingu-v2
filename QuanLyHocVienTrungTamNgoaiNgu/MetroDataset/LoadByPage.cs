﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetroDataset
{
    public class LoadByPage
    {

        DataQuanLyHocVienDataContext context;
        public LoadByPage()
        {
            string connectString = Helper.GetStringConnection();
            context = new DataQuanLyHocVienDataContext(connectString);
        }
        //load theo page
        #region load theo page
        public object HienThiTaiKhoanNguoiDung(int trang, int soluong, int indexColumnFilter, string keyFilter, string keySearch)
        {

            DataTable dt = new DataTable();
            //T.Mã_tài_khoản, T.Tên_người_dùng, T.Hoạt_động
            dt.Columns.Add("Mã tài khoản", System.Type.GetType("System.String"));
            dt.Columns.Add("Họ người dùng", System.Type.GetType("System.String"));
            dt.Columns.Add("Tên người dùng", System.Type.GetType("System.String"));
            dt.Columns.Add("Hoạt động", System.Type.GetType("System.Boolean"));

            var gv = from n in context.NGUOIDUNGs
                     from g in context.GIANGVIENs
                     where n.MATAIKHOAN == g.MATAIKHOAN
                     select new { Mã_tài_khoản = n.MATAIKHOAN, Tên_người_dùng = g.TENGIANGVIEN, Hoạt_động = (bool)n.HOATDONG };
            var hv = from n in context.NGUOIDUNGs
                     from h in context.HOCVIENs
                     where n.MATAIKHOAN == h.MATAIKHOAN
                     select new { Mã_tài_khoản = n.MATAIKHOAN, Tên_người_dùng = h.TENHOCVIEN, Hoạt_động = (bool)n.HOATDONG };
            var nv = from n in context.NGUOIDUNGs
                     from _n in context.NHANVIENs
                     where n.MATAIKHOAN == _n.MATAIKHOAN
                     select new { Mã_tài_khoản = n.MATAIKHOAN, Tên_người_dùng = _n.TENNHANVIEN, Hoạt_động = (bool)n.HOATDONG };
            List<object[]> l = new List<object[]>();
            l.AddRange(gv.Select(T => new object[] { T.Mã_tài_khoản, T.Tên_người_dùng, T.Hoạt_động }).ToList<object[]>());
            l.AddRange(hv.Select(T => new object[] { T.Mã_tài_khoản, T.Tên_người_dùng, T.Hoạt_động }).ToList<object[]>());
            l.AddRange(nv.Select(T => new object[] { T.Mã_tài_khoản, T.Tên_người_dùng, T.Hoạt_động }).ToList<object[]>());

            if (indexColumnFilter != -1 && !String.IsNullOrEmpty(keyFilter) && !String.IsNullOrWhiteSpace(keyFilter))
            {
                //filter
                if (indexColumnFilter == 0) //mã
                    l = l.Where(t => t[indexColumnFilter].ToString().StartsWith(keyFilter)).ToList();
                else if (indexColumnFilter == 2) //hoạt động
                    l = l.Where(t => bool.Parse(t[indexColumnFilter].ToString()) == bool.Parse(keyFilter.ToString())).ToList();
            }

            if (!String.IsNullOrEmpty(keySearch) && !String.IsNullOrWhiteSpace(keySearch)) //search
                l = l.Where(t => t.ToList<object>()
                .Select(y => y == null ? "" : y.ToString()).ToList<string>()
                .Count(g => g.ToUpper()
                .Contains(keySearch.ToUpper())) != 0).ToList();

            l = l.Skip((trang - 1) * soluong).Take(soluong).ToList();

            l.ForEach(t => dt.Rows.Add(t[0], Helper.GetHoTen(t[1].ToString()).Ho, Helper.GetHoTen(t[1].ToString()).Ten, t[2]));
            dt.Columns[3].ReadOnly = false;
            return dt;
        }

        public object HienThiNguoiDungVaNhomNguoiDung(int trang, int soluong, int indexColumnFilter, string keyFilter, string keySearch)
        {

            DataTable dt = new DataTable();
            //T.Mã_tài_khoản, T.Tên_người_dùng, T.Mã_nhóm, T.Tên_nhóm_người_dùng, T.Hoạt_động
            dt.Columns.Add("Mã tài khoản", System.Type.GetType("System.String"));
            dt.Columns.Add("Họ người dùng", System.Type.GetType("System.String"));
            dt.Columns.Add("Tên người dùng", System.Type.GetType("System.String"));
            dt.Columns.Add("Mã nhóm", System.Type.GetType("System.String"));
            dt.Columns.Add("Tên nhóm người dùng", System.Type.GetType("System.String"));
            dt.Columns.Add("Hoạt động", System.Type.GetType("System.Boolean"));

            var gv = from n in context.NGUOIDUNG_NHOMNGUOIDUNGs
                     from g in context.GIANGVIENs
                     from nhom in context.NHOMNGUOIDUNGs
                     where n.MATAIKHOAN == g.MATAIKHOAN
                     && n.MANHOMNGUOIDUNG == nhom.MANHOMNGUOIDUNG
                     select new { Mã_tài_khoản = n.MATAIKHOAN, Tên_người_dùng = g.TENGIANGVIEN, Mã_nhóm = n.MANHOMNGUOIDUNG, Tên_nhóm_người_dùng = nhom.TENNHOMNGUOIDUNG, Hoạt_động = (bool)n.HOATDONG };
            var hv = from n in context.NGUOIDUNG_NHOMNGUOIDUNGs
                     from h in context.HOCVIENs
                     from nhom in context.NHOMNGUOIDUNGs
                     where n.MATAIKHOAN == h.MATAIKHOAN
                     && n.MANHOMNGUOIDUNG == nhom.MANHOMNGUOIDUNG
                     select new { Mã_tài_khoản = n.MATAIKHOAN, Tên_người_dùng = h.TENHOCVIEN, Mã_nhóm = n.MANHOMNGUOIDUNG, Tên_nhóm_người_dùng = nhom.TENNHOMNGUOIDUNG, Hoạt_động = (bool)n.HOATDONG };
            var nv = from n in context.NGUOIDUNG_NHOMNGUOIDUNGs
                     from _n in context.NHANVIENs
                     from nhom in context.NHOMNGUOIDUNGs
                     where n.MATAIKHOAN == _n.MATAIKHOAN
                     && n.MANHOMNGUOIDUNG == nhom.MANHOMNGUOIDUNG
                     select new { Mã_tài_khoản = n.MATAIKHOAN, Tên_người_dùng = _n.TENNHANVIEN, Mã_nhóm = n.MANHOMNGUOIDUNG, Tên_nhóm_người_dùng = nhom.TENNHOMNGUOIDUNG, Hoạt_động = (bool)n.HOATDONG };
            List<object[]> l = new List<object[]>();
            l.AddRange(gv.Select(T => new object[] { T.Mã_tài_khoản, T.Tên_người_dùng, T.Mã_nhóm, T.Tên_nhóm_người_dùng, T.Hoạt_động }).ToList<object[]>());
            l.AddRange(hv.Select(T => new object[] { T.Mã_tài_khoản, T.Tên_người_dùng, T.Mã_nhóm, T.Tên_nhóm_người_dùng, T.Hoạt_động }).ToList<object[]>());
            l.AddRange(nv.Select(T => new object[] { T.Mã_tài_khoản, T.Tên_người_dùng, T.Mã_nhóm, T.Tên_nhóm_người_dùng, T.Hoạt_động }).ToList<object[]>());

            if (indexColumnFilter != -1 && !String.IsNullOrEmpty(keyFilter) && !String.IsNullOrWhiteSpace(keyFilter))
            {
                //filter
                if (indexColumnFilter == 0) //nhân viên
                    l = l.Where(t => t[indexColumnFilter].ToString().StartsWith(keyFilter)).ToList();
                else if (indexColumnFilter == 2)
                    l = l.Where(t => t[indexColumnFilter].ToString().Equals(keyFilter)).ToList();
                else if (indexColumnFilter == 4)
                    l = l.Where(t => bool.Parse(t[indexColumnFilter].ToString()) == bool.Parse(keyFilter.ToString())).ToList();
            }
            //search
            if (!String.IsNullOrEmpty(keySearch) && !String.IsNullOrWhiteSpace(keySearch)) //search
                l = l.Where(t =>
                t.ToArray<object>()
                .Select(y => y == null ? "" : y.ToString()).ToList<string>()
                .Count(g => g.ToUpper().Contains(keySearch.ToUpper())) != 0)
                .ToList();
            l = l.Skip((trang - 1) * soluong).Take(soluong).ToList();

            l.ForEach(t => dt.Rows.Add(t[0], Helper.GetHoTen(t[1].ToString()).Ho, Helper.GetHoTen(t[1].ToString()).Ten, t[2], t[3], t[4]));
            dt.Columns[4].ReadOnly = false;
            return dt;
        }
        public object HienThiBienLai(int trang, int soluong, int indexColumnFilter, string keyFilter, string keySearch)
        {
            keyFilter = keyFilter.Replace("_", " ");

            //tạo DataTable
            DataTable dt = new DataTable();
            dt.Columns.Add("mã biên lai", System.Type.GetType("System.String"));
            dt.Columns.Add("loại biên lai", System.Type.GetType("System.String"));
            dt.Columns.Add("lớp", System.Type.GetType("System.String"));
            dt.Columns.Add("khoá học", System.Type.GetType("System.String"));
            dt.Columns.Add("mã học viên", System.Type.GetType("System.String"));
            dt.Columns.Add("tên học viên", System.Type.GetType("System.String"));
            dt.Columns.Add("mã nhân viên", System.Type.GetType("System.String"));
            dt.Columns.Add("tên nhân viên", System.Type.GetType("System.String"));
            dt.Columns.Add("Số tiền", System.Type.GetType("System.Double"));
            dt.Columns.Add("Đợt", System.Type.GetType("System.Int32"));
            dt.Columns.Add("Ngày xác nhận", System.Type.GetType("System.DateTime"));

            //lấy dữ liệu
            var alias = from bl in context.BIENLAIs
                        select new
                        {
                            Biên_lai = bl,
                            Đăng_ký = bl.TT_DANGKYKHOAHOC,
                            Lớp = bl.MALOPHOC,
                            Khoá_học = bl.TT_DANGKYKHOAHOC.KHOAHOC,
                            Học_viên = bl.TT_DANGKYKHOAHOC.HOCVIEN,
                            Nhân_viên = bl.NHANVIEN,
                            Loại_biên_lai = bl.LOAI_BIENLAI
                        };

            //chuyển sang array object ==> filter & search
            List<object[]> l = new List<object[]>();
            l.AddRange(alias.Select(t => new object[] {
                t.Biên_lai.MABIENLAI,
                t.Loại_biên_lai.MALOAIBIENLAI,
                t.Loại_biên_lai.TENLOAIBIENLAI,
                t.Biên_lai.MALOPHOC,
                t.Khoá_học.MAKHOAHOC,
                t.Khoá_học.TENKHOAHOC,
                t.Học_viên.MAHOCVIEN,
                t.Học_viên.TENHOCVIEN,
                t.Nhân_viên.MANHANVIEN,
                t.Nhân_viên.TENNHANVIEN,
                t.Biên_lai.SOTIEN,
                t.Biên_lai.DOT,
                t.Biên_lai.NGAYXACNHAN
            }));

            //filter
            if (indexColumnFilter != -1 && !String.IsNullOrEmpty(keyFilter) && !String.IsNullOrWhiteSpace(keyFilter))
            {
                //filter: maloaibienlai 1 , makhoahoc 4
                l = l.Where(t => t[indexColumnFilter].ToString().Equals(keyFilter)).ToList();
            }

            //search
            if (!String.IsNullOrEmpty(keySearch) && !String.IsNullOrWhiteSpace(keySearch)) //search
                l = l.Where(t =>
                t.ToArray<object>()
                .Select(y => y == null ? "" : y.ToString()).ToList<string>()
                .Count(g => g.ToUpper().Contains(keySearch.ToUpper())) != 0)
                .ToList();

            //phân trang
            l = l.OrderBy(t => t[11].ToString()).ThenBy(t => (DateTime)t[12]).Skip((trang - 1) * soluong).Take(soluong).ToList();

            //fill datatable
            l.ForEach(t => dt.Rows.Add(t[0], t[2], t[3], t[5], t[6], t[7], t[8], t[9], t[10], t[11], t[12]));
            return dt;

        }

        public object HienThiLichSuHuyBienLai(int trang, int soluong, int indexColumnFilter, string keyFilter, string keySearch)
        {
            keyFilter = keyFilter.Replace("_", " ");

            //tạo DataTable
            DataTable dt = new DataTable();
            dt.Columns.Add("mã biên lai", System.Type.GetType("System.String"));
            dt.Columns.Add("loại biên lai", System.Type.GetType("System.String"));
            dt.Columns.Add("lớp", System.Type.GetType("System.String"));
            dt.Columns.Add("mã đăng ký khoá học", System.Type.GetType("System.String"));
            dt.Columns.Add("mã nhân viên lập", System.Type.GetType("System.String"));
            dt.Columns.Add("Ngày xác nhận", System.Type.GetType("System.DateTime"));
            dt.Columns.Add("Số tiền", System.Type.GetType("System.Double"));
            dt.Columns.Add("Đợt", System.Type.GetType("System.Int32"));
            dt.Columns.Add("mã nhân viên huỷ", System.Type.GetType("System.String"));
            dt.Columns.Add("Ngày huỷ", System.Type.GetType("System.DateTime"));

            //lấy dữ liệu
            var alias = from bl in context.LICHSU_HUYBIENLAIs
                        select new { bl };

            //chuyển sang array object ==> filter & search
            List<object[]> l = new List<object[]>();
            l.AddRange(alias.Select(t => new object[] {
                t.bl.MABIENLAI,
                t.bl.MALOAIBIENLAI,
                t.bl.MALOPHOC,
                t.bl.MADANGKYKHOAHOC,
                t.bl.MANHANVIENLAP,
                t.bl.NGAYXACNHAN,
                t.bl.SOTIEN,
                t.bl.DOT,
                t.bl.MANHANVIENHUY,
                t.bl.NGAYHUY
            }));

            //filter
            if (indexColumnFilter != -1 && !String.IsNullOrEmpty(keyFilter) && !String.IsNullOrWhiteSpace(keyFilter))
            {
                l = l.Where(t => t[indexColumnFilter].ToString().Equals(keyFilter)).ToList();
            }

            //search
            if (!String.IsNullOrEmpty(keySearch) && !String.IsNullOrWhiteSpace(keySearch)) //search
                l = l.Where(t =>
                t.ToArray<object>()
                .Select(y => y == null ? "" : y.ToString()).ToList<string>()
                .Count(g => g.ToUpper().Contains(keySearch.ToUpper())) != 0)
                .ToList();

            //phân trang
            l = l.Skip((trang - 1) * soluong).Take(soluong).ToList();

            //fill datatable
            l.ForEach(t => dt.Rows.Add(t[0], t[1], t[2], t[3], t[4], t[5], t[6], t[7], t[8], t[9]));
            return dt;

        }

        public object HienThiCoSo(int trang, int soluong, int indexColumnFilter, string keyFilter, string keySearch5)
        {
            //tạo DataTable
            DataTable dt = new DataTable();
            dt.Columns.Add("mã cơ sở", System.Type.GetType("System.String"));
            dt.Columns.Add("tên cơ sở", System.Type.GetType("System.String"));
            //lấy dữ liệu
            var alias = from cs in context.COSOs
                        select new
                        {
                            cs.MACOSO,
                            cs.TENCOSO
                        };
            //chuyển sang array object ==> filter & search
            List<object[]> l = new List<object[]>();
            l.AddRange(alias.Select(T => new object[] {   T.MACOSO,
                            T.TENCOSO }));

            //phân trang
            l = l.OrderBy(t => t[0].ToString()).Skip((trang - 1) * soluong).Take(soluong).ToList();

            //fill datatable
            l.ForEach(t => dt.Rows.Add(t[0], t[1]));
            return dt;
        }

        public object HienThiKhoaHoc_Grid(int trang, int soluong, int indexColumnFilter, string keyFilter, string keySearch)
        {

            //tạo DataTable
            DataTable dt = new DataTable();
            dt.Columns.Add("mã khoá học", System.Type.GetType("System.String"));
            dt.Columns.Add("tên khoá học", System.Type.GetType("System.String"));
            dt.Columns.Add("học phí", System.Type.GetType("System.String"));
            dt.Columns.Add("số lượng lớp", System.Type.GetType("System.Int32"));
            //lấy dữ liệu
            var alias = from k in context.KHOAHOCs
                        select new
                        {
                            k.MAKHOAHOC,
                            k.TENKHOAHOC,
                            k.HOCPHI,
                            k.LOPHOCs.Count
                        };
            //chuyển sang array object ==> filter & search
            List<object[]> l = new List<object[]>();
            l.AddRange(alias.Select(t => new object[] { t.MAKHOAHOC, t.TENKHOAHOC, t.HOCPHI, t.Count }));

            //filter
            if (indexColumnFilter != -1 && !String.IsNullOrEmpty(keyFilter) && !String.IsNullOrWhiteSpace(keyFilter))
            {
                if (indexColumnFilter == 3)
                {
                    bool colop = int.Parse(keyFilter) == 1;
                    if(colop)
                        l = l.Where(t => int.Parse(t[indexColumnFilter].ToString()) > 0).ToList();
                    else
                        l = l.Where(t => int.Parse(t[indexColumnFilter].ToString()) == 0).ToList();


                }
            }

            //search
            if (!String.IsNullOrEmpty(keySearch) && !String.IsNullOrWhiteSpace(keySearch)) //search
                l = l.Where(t =>
                t.ToArray<object>()
                .Select(y => y == null ? "" : y.ToString()).ToList<string>()
                .Count(g => g.ToUpper().Contains(keySearch.ToUpper())) != 0)
                .ToList();

            //phân trang
            l = l.OrderBy(t => t[0].ToString()).Skip((trang - 1) * soluong).Take(soluong).ToList();

            //fill datatable
            l.ForEach(t => dt.Rows.Add(t[0], t[1], t[2], t[3]));
            return dt;
        }

        public object HienThiKhoaHoc(int trang, int soluong, int indexColumnFilter, string keyFilter, string keySearch)
        {

            //tạo DataTable
            DataTable dt = new DataTable();
            dt.Columns.Add("mã khoá học", System.Type.GetType("System.String"));
            dt.Columns.Add("tên khoá học", System.Type.GetType("System.String"));
            dt.Columns.Add("học phí", System.Type.GetType("System.String"));
            //lấy dữ liệu
            var alias = from k in context.KHOAHOCs
                        select new
                        {
                            k.MAKHOAHOC,
                            k.TENKHOAHOC,
                            k.HOCPHI
                        };
            //chuyển sang array object ==> filter & search
            List<object[]> l = new List<object[]>();
            l.AddRange(alias.Select(t => new object[] { t.MAKHOAHOC, t.TENKHOAHOC, t.HOCPHI }));

            //phân trang
            l = l.OrderBy(t => t[0].ToString()).Skip((trang - 1) * soluong).Take(soluong).ToList();

            //fill datatable
            l.ForEach(t => dt.Rows.Add(t[0], t[1], t[2]));
            return dt;
        }

        public object HienThiLopHoc(int trang, int soluong, int indexColumnFilter, string keyFilter, string keySearch)
        {
            keyFilter = keyFilter.Replace("_", " ");

            //tạo DataTable
            DataTable dt = new DataTable();
            dt.Columns.Add("lớp học", System.Type.GetType("System.String"));
            dt.Columns.Add("khoá học", System.Type.GetType("System.String"));
            dt.Columns.Add("mã giảng viên", System.Type.GetType("System.String"));
            dt.Columns.Add("Tên giảng viên", System.Type.GetType("System.String"));
            dt.Columns.Add("Trình độ", System.Type.GetType("System.String"));
            dt.Columns.Add("Chuyên môn", System.Type.GetType("System.String"));
            dt.Columns.Add("Bắt đầu", System.Type.GetType("System.DateTime"));
            dt.Columns.Add("Kết thúc", System.Type.GetType("System.DateTime"));
            dt.Columns.Add("Học phí", System.Type.GetType("System.Double"));
            dt.Columns.Add("Số lượng", System.Type.GetType("System.Int32"));
            dt.Columns.Add("Tình trạng", System.Type.GetType("System.String"));
            //lấy dữ liệu
            var alias = from lop in context.LOPHOCs
                        select new
                        {
                            lop.MALOPHOC,
                            lop.MAKHOAHOC,
                            lop.KHOAHOC.TENKHOAHOC,
                            lop.MAGIANGVIEN,
                            lop.GIANGVIEN.TENGIANGVIEN,
                            lop.GIANGVIEN.TRINHDO,
                            lop.GIANGVIEN.CHUYENMON,
                            Bắt_đầu = context.LICHHOCs.Where(t => t.MALOPHOC == lop.MALOPHOC).OrderBy(t => t.NGAYHOC).FirstOrDefault().NGAYHOC,
                            Kết_thúc = context.LICHHOCs.Where(t => t.MALOPHOC == lop.MALOPHOC).OrderByDescending(t => t.NGAYHOC).FirstOrDefault().NGAYHOC,
                            lop.KHOAHOC.HOCPHI,
                            lop.SOLUONG,
                            lop.TINHTRANG
                        };
            //chuyển sang array object ==> filter & search
            List<object[]> l = new List<object[]>();
            l.AddRange(alias.Select(t => new object[] { t.MALOPHOC, t.MAKHOAHOC, t.TENKHOAHOC, t.MAGIANGVIEN, t.TENGIANGVIEN, t.TRINHDO, t.CHUYENMON, t.Bắt_đầu, t.Kết_thúc, t.HOCPHI, t.SOLUONG, t.TINHTRANG }));


            //filter
            if (indexColumnFilter != -1 && !String.IsNullOrEmpty(keyFilter) && !String.IsNullOrWhiteSpace(keyFilter))
            {
                if (indexColumnFilter == 1)
                    l = l.Where(t => t[indexColumnFilter].ToString().Equals(keyFilter)).ToList();
                else if (indexColumnFilter == 11)
                    l = l.Where(t => t[indexColumnFilter].ToString().Equals(keyFilter)).ToList();
            }

            //search
            if (!String.IsNullOrEmpty(keySearch) && !String.IsNullOrWhiteSpace(keySearch)) //search
                l = l.Where(t =>
                t.ToArray<object>()
                .Select(y => y == null ? "" : y.ToString()).ToList<string>()
                .Count(g => g.ToUpper().Contains(keySearch.ToUpper())) != 0)
                .ToList();

            //phân trang
            l = l.OrderBy(t => t[11].ToString()).ThenBy(t => (DateTime)t[7]).Skip((trang - 1) * soluong).Take(soluong).ToList();

            //fill datatable
            l.ForEach(t => dt.Rows.Add(t[0], t[1], t[3], t[4], t[5], t[6], t[7], t[8], t[9], t[10], t[11]));
            return dt;
        }

        public object HienThiDiem(int trang, int soluong, int indexColumnFilter, string keyFilter, string keySearch)
        {
            keyFilter = keyFilter.Replace("_", " ");

            //tạo DataTable
            DataTable dt = new DataTable();
            dt.Columns.Add("Mã đăng ký", System.Type.GetType("System.String"));
            dt.Columns.Add("Mã điểm", System.Type.GetType("System.String"));
            dt.Columns.Add("Mã lớp", System.Type.GetType("System.String"));
            dt.Columns.Add("Khoá học", System.Type.GetType("System.String"));
            dt.Columns.Add("Loại điểm", System.Type.GetType("System.String"));
            dt.Columns.Add("Mã học viên", System.Type.GetType("System.String"));
            dt.Columns.Add("Tên học viên", System.Type.GetType("System.String"));
            dt.Columns.Add("Lần", System.Type.GetType("System.Int32"));
            dt.Columns.Add("Điểm", System.Type.GetType("System.Double"));

            //lấy dữ liệu
            var alias = from diem in context.DIEMs
                        select new
                        {
                            Mã_đăng_ký = diem.MADANGKYKHOAHOC,
                            Mã_điểm = diem.MADIEM,
                            Mã_lớp = diem.MALOPHOC,
                            Mã_khoá = diem.TT_GHIDANH.TT_DANGKYKHOAHOC.MAKHOAHOC, //3
                            Khoá_học = diem.TT_GHIDANH.TT_DANGKYKHOAHOC.KHOAHOC.TENKHOAHOC,
                            Mã_loại_điểm = diem.MALOAIDIEM, //5
                            Loại_điểm = diem.LOAIDIEM.TENLOAIDIEM,
                            Mã_học_viên = diem.TT_GHIDANH.TT_DANGKYKHOAHOC.MAHOCVIEN,
                            Tên_học_viên = diem.TT_GHIDANH.TT_DANGKYKHOAHOC.HOCVIEN.TENHOCVIEN,
                            Lần = diem.LAN,
                            Điểm = diem.DIEM1
                        };

            //chuyển sang array object ==> filter & search
            List<object[]> l = new List<object[]>();
            l.AddRange(alias.Select(t => new object[] { t.Mã_đăng_ký, t.Mã_điểm, t.Mã_lớp, t.Mã_khoá, t.Khoá_học, t.Mã_loại_điểm, t.Loại_điểm, t.Mã_học_viên, t.Tên_học_viên, t.Lần, t.Điểm }));

            //filter
            if (indexColumnFilter != -1 && !String.IsNullOrEmpty(keyFilter) && !String.IsNullOrWhiteSpace(keyFilter))
                l = l.Where(t => t[indexColumnFilter].ToString().Equals(keyFilter)).ToList();

            //search
            if (!String.IsNullOrEmpty(keySearch) && !String.IsNullOrWhiteSpace(keySearch)) //search
                l = l.Where(t =>
                t.ToArray<object>()
                .Select(y => y == null ? "" : y.ToString()).ToList<string>()
                .Count(g => g.ToUpper().Contains(keySearch.ToUpper())) != 0)
                .ToList();

            //phân trang
            if (soluong > 0 && trang > 0)
                l = l.OrderBy(t => t[1].ToString()).Skip((trang - 1) * soluong).Take(soluong).ToList();

            //fill datatable
            l.ForEach(t => dt.Rows.Add(t[0], t[1], t[2], t[4], t[6], t[7], t[8], t[9], t[10]));
            return dt;
        }



        public object HienThiTTBackup(int trang, int soluong, int indexColumnFilter, string keyFilter, string keySearch)
        {
            keyFilter = keyFilter.Replace("_", " ");

            //tạo DataTable
            DataTable dt = new DataTable();
            dt.Columns.Add("Mã backup", System.Type.GetType("System.String"));
            dt.Columns.Add("Tên backup", System.Type.GetType("System.String"));
            dt.Columns.Add("Ngày lưu", System.Type.GetType("System.TimeSpan"));
            dt.Columns.Add("Mã nhân viên", System.Type.GetType("System.String"));
            dt.Columns.Add("Tên nhân viên", System.Type.GetType("System.String"));
            dt.Columns.Add("Đường dẫn", System.Type.GetType("System.String"));

            //lấy dữ liệu
            var alias = from bk in context.TT_BACKUPs
                        select new
                        {
                            Mã_backup = bk.MABACKUP,
                            Tên_backup = bk.TENTAPTIN,
                            Ngày_lưu = bk.NGAYLUU,
                            Mã_nhân_viên = bk.MANHANVIEN,
                            Tên_nhân_viên = bk.NHANVIEN.TENNHANVIEN,
                            Đường_dẫn = bk.DUONGDAN
                        };

            //chuyển sang array object ==> filter & search
            List<object[]> l = new List<object[]>();
            l.AddRange(alias.Select(t => new object[] { t.Mã_backup,
                t.Tên_backup, t.Ngày_lưu, t.Mã_nhân_viên, t.Tên_nhân_viên, t.Đường_dẫn }));
            //filter

            //search
            if (!String.IsNullOrEmpty(keySearch) && !String.IsNullOrWhiteSpace(keySearch)) //search
                l = l.Where(t =>
                t.ToArray<object>()
                .Select(y => y == null ? "" : y.ToString()).ToList<string>()
                .Count(g => g.ToUpper().Contains(keySearch.ToUpper())) != 0)
                .ToList();

            //phân trang
            if (soluong > 0 && trang > 0)
                l = l.OrderBy(t => t[2].ToString()).Skip((trang - 1) * soluong).Take(soluong).ToList();

            //fill datatable
            l.ForEach(t => dt.Rows.Add(t[0], t[1], t[2], t[3], t[4], t[5]));
            return dt;
        }

        public object HienThiDiemHocVien(int trang, int soluong, int indexColumnFilter, string keyFilter, string keySearch)
        {
            keyFilter = keyFilter.Replace("_", " ");

            if (Helper.idUser == null)
                Helper.idUser = "";

            //tạo DataTable
            DataTable dt = new DataTable();
            dt.Columns.Add("Mã đăng ký", System.Type.GetType("System.String"));
            dt.Columns.Add("Mã điểm", System.Type.GetType("System.String"));
            dt.Columns.Add("Mã lớp", System.Type.GetType("System.String"));
            dt.Columns.Add("Khoá học", System.Type.GetType("System.String"));
            dt.Columns.Add("Loại điểm", System.Type.GetType("System.String"));
            dt.Columns.Add("Mã học viên", System.Type.GetType("System.String"));
            dt.Columns.Add("Tên học viên", System.Type.GetType("System.String"));
            dt.Columns.Add("Lần", System.Type.GetType("System.Int32"));
            dt.Columns.Add("Điểm", System.Type.GetType("System.Double"));

            //lấy dữ liệu
            var alias = from diem in context.DIEMs
                        where
                        diem.TT_GHIDANH.TT_DANGKYKHOAHOC.MAHOCVIEN == Helper.idUser //lọc by mã học viên
                        select new
                        {
                            Mã_đăng_ký = diem.MADANGKYKHOAHOC,
                            Mã_điểm = diem.MADIEM,
                            Mã_lớp = diem.MALOPHOC, //2
                            Mã_khoá = diem.TT_GHIDANH.TT_DANGKYKHOAHOC.MAKHOAHOC,
                            Khoá_học = diem.TT_GHIDANH.TT_DANGKYKHOAHOC.KHOAHOC.TENKHOAHOC,
                            Mã_loại_điểm = diem.MALOAIDIEM, //5
                            Loại_điểm = diem.LOAIDIEM.TENLOAIDIEM,
                            Mã_học_viên = diem.TT_GHIDANH.TT_DANGKYKHOAHOC.MAHOCVIEN,
                            Tên_học_viên = diem.TT_GHIDANH.TT_DANGKYKHOAHOC.HOCVIEN.TENHOCVIEN,
                            Lần = diem.LAN,
                            Điểm = diem.DIEM1
                        };

            //chuyển sang array object ==> filter & search
            List<object[]> l = new List<object[]>();
            l.AddRange(alias.Select(t => new object[] { t.Mã_đăng_ký, t.Mã_điểm, t.Mã_lớp, t.Mã_khoá, t.Khoá_học, t.Mã_loại_điểm, t.Loại_điểm, t.Mã_học_viên, t.Tên_học_viên, t.Lần, t.Điểm }));

            //filter
            if (indexColumnFilter != -1 && !String.IsNullOrEmpty(keyFilter) && !String.IsNullOrWhiteSpace(keyFilter))
                l = l.Where(t => t[indexColumnFilter].ToString().Equals(keyFilter)).ToList();

            //search
            if (!String.IsNullOrEmpty(keySearch) && !String.IsNullOrWhiteSpace(keySearch)) //search
                l = l.Where(t =>
                t.ToArray<object>()
                .Select(y => y == null ? "" : y.ToString()).ToList<string>()
                .Count(g => g.ToUpper().Contains(keySearch.ToUpper())) != 0)
                .ToList();

            //phân trang
            l = l.OrderBy(t => t[1].ToString()).Skip((trang - 1) * soluong).Take(soluong).ToList();

            //fill datatable
            l.ForEach(t => dt.Rows.Add(t[0], t[1], t[2], t[4], t[6], t[7], t[8], t[9], t[10]));
            return dt;
        }

        public object HienThiLichHoc(int trang, int soluong, int indexColumnFilter, string keyFilter, string keySearch)
        {
            keyFilter = keyFilter.Replace("_", " ");

            //tạo DataTable
            DataTable dt = new DataTable();
            dt.Columns.Add("Lịch học", System.Type.GetType("System.String"));
            dt.Columns.Add("Lớp", System.Type.GetType("System.String"));
            dt.Columns.Add("Khoá học", System.Type.GetType("System.String"));
            dt.Columns.Add("Phòng", System.Type.GetType("System.String"));
            dt.Columns.Add("Cơ sở", System.Type.GetType("System.String"));
            dt.Columns.Add("Ngày học", System.Type.GetType("System.DateTime"));
            dt.Columns.Add("Ca học", System.Type.GetType("System.String"));
            dt.Columns.Add("Bắt đầu", System.Type.GetType("System.TimeSpan"));
            dt.Columns.Add("Kết thúc", System.Type.GetType("System.TimeSpan"));
            dt.Columns.Add("Tình trạng", System.Type.GetType("System.String"));

            //lấy dữ liệu
            var alias = from lich in context.LICHHOCs
                        select new
                        {
                            Lịch_học = lich.MALICHHOC,
                            Lớp = lich.MALOPHOC,
                            Mã_khoá = lich.LOPHOC.MAKHOAHOC,
                            Khoá_học = lich.LOPHOC.KHOAHOC.TENKHOAHOC,
                            Phòng = lich.MAPHONG,
                            Mã_cơ_sở = lich.MACOSO,
                            Cơ_sở = lich.PHONG.COSO.TENCOSO,
                            Ngày_học = lich.NGAYHOC,
                            Ca_học = lich.MACA,
                            Bắt_đầu = lich.THOIGIANHOC.CAHOC.GIOBATDAU,
                            Kết_thúc = lich.THOIGIANHOC.CAHOC.GIOKETTHUC,
                            Tình_trạng = lich.TINHTRANG
                        };

            //chuyển sang array object ==> filter & search
            List<object[]> l = new List<object[]>();
            l.AddRange(alias.Select(t => new object[] { t.Lịch_học, t.Lớp, t.Mã_khoá, t.Khoá_học, t.Phòng, t.Mã_cơ_sở, t.Cơ_sở, t.Ngày_học, t.Ca_học, t.Bắt_đầu, t.Kết_thúc, t.Tình_trạng }));

            //filter
            if (indexColumnFilter != -1 && !String.IsNullOrEmpty(keyFilter) && !String.IsNullOrWhiteSpace(keyFilter))
            {
                if (indexColumnFilter == 2) //mã khoá
                    l = l.Where(t => t[indexColumnFilter].ToString().Equals(keyFilter)).ToList();
                else if (indexColumnFilter == 5)
                    l = l.Where(t => t[indexColumnFilter].ToString().Equals(keyFilter)).ToList();
                else if (indexColumnFilter == 8)
                    l = l.Where(t => t[indexColumnFilter].ToString().Equals(keyFilter)).ToList();
                else if (indexColumnFilter == 11)
                    l = l.Where(t => t[indexColumnFilter].ToString().Equals(keyFilter)).ToList();
            }
            //search
            if (!String.IsNullOrEmpty(keySearch) && !String.IsNullOrWhiteSpace(keySearch)) //search
                l = l.Where(t =>
                t.ToArray<object>()
                .Select(y => y == null ? "" : y.ToString()).ToList<string>()
                .Count(g => g.ToUpper().Contains(keySearch.ToUpper())) != 0)
                .ToList();

            //phân trang
            if (trang != -1 && soluong != -1)
                l = l.OrderBy(t => t[11].ToString()).ThenBy(t => (DateTime)t[7]).ThenBy(t => t[8].ToString()).Skip((trang - 1) * soluong).Take(soluong).ToList();

            //fill datatable
            l.ForEach(t => dt.Rows.Add(t[0], t[1], t[3], t[4], t[6], t[7], t[8], t[9], t[10], t[11]));
            return dt;
        }

        public object HienThiLichDayGiangVien(int trang, int soluong, int indexColumnFilter, string keyFilter, string keySearch)
        {
            keyFilter = keyFilter.Replace("_", " ");

            //tạo DataTable
            DataTable dt = new DataTable();
            dt.Columns.Add("Lịch học", System.Type.GetType("System.String"));
            dt.Columns.Add("Lớp", System.Type.GetType("System.String"));
            dt.Columns.Add("Khoá học", System.Type.GetType("System.String"));
            dt.Columns.Add("Phòng", System.Type.GetType("System.String"));
            dt.Columns.Add("Cơ sở", System.Type.GetType("System.String"));
            dt.Columns.Add("Ngày học", System.Type.GetType("System.DateTime"));
            dt.Columns.Add("Ca học", System.Type.GetType("System.String"));
            dt.Columns.Add("Bắt đầu", System.Type.GetType("System.TimeSpan"));
            dt.Columns.Add("Kết thúc", System.Type.GetType("System.TimeSpan"));
            dt.Columns.Add("Tình trạng", System.Type.GetType("System.String"));

            //lấy dữ liệu
            var alias = from lich in context.LICHHOCs
                        where lich.LOPHOC.MAGIANGVIEN == Helper.idUser
                        select new
                        {
                            Lịch_học = lich.MALICHHOC,
                            Lớp = lich.MALOPHOC,
                            Mã_khoá = lich.LOPHOC.MAKHOAHOC,
                            Khoá_học = lich.LOPHOC.KHOAHOC.TENKHOAHOC,
                            Phòng = lich.MAPHONG,
                            Mã_cơ_sở = lich.PHONG.MACOSO,
                            Cơ_sở = lich.PHONG.COSO.TENCOSO,
                            Ngày_học = lich.NGAYHOC,
                            Ca_học = lich.MACA,
                            Bắt_đầu = lich.THOIGIANHOC.CAHOC.GIOBATDAU,
                            Kết_thúc = lich.THOIGIANHOC.CAHOC.GIOKETTHUC,
                            Tình_trạng = lich.TINHTRANG
                        };

            //chuyển sang array object ==> filter & search
            List<object[]> l = new List<object[]>();
            l.AddRange(alias.Select(t => new object[] { t.Lịch_học, t.Lớp, t.Mã_khoá, t.Khoá_học, t.Phòng, t.Mã_cơ_sở, t.Cơ_sở, t.Ngày_học, t.Ca_học, t.Bắt_đầu, t.Kết_thúc, t.Tình_trạng }));

            //filter
            if (indexColumnFilter != -1 && !String.IsNullOrEmpty(keyFilter) && !String.IsNullOrWhiteSpace(keyFilter))
            {
                if (indexColumnFilter == 1 || indexColumnFilter == 11) //mã khoá
                    l = l.Where(t => t[indexColumnFilter].ToString().Equals(keyFilter)).ToList();
            }
            //search
            if (!String.IsNullOrEmpty(keySearch) && !String.IsNullOrWhiteSpace(keySearch)) //search
                l = l.Where(t =>
                t.ToArray<object>()
                .Select(y => y == null ? "" : y.ToString()).ToList<string>()
                .Count(g => g.ToUpper().Contains(keySearch.ToUpper())) != 0)
                .ToList();

            //phân trang
            if (trang != -1 && soluong != -1)
                l = l.OrderBy(t => t[11].ToString()).ThenBy(t => (DateTime)t[7]).ThenBy(t => t[8].ToString()).Skip((trang - 1) * soluong).Take(soluong).ToList();

            //fill datatable
            l.ForEach(t => dt.Rows.Add(t[0], t[1], t[3], t[4], t[6], t[7], t[8], t[9], t[10], t[11]));
            return dt;
        }

        public object HienThiLichHocHocVien(int trang, int soluong, int indexColumnFilter, string keyFilter, string keySearch)
        {
            keyFilter = keyFilter.Replace("_", " ");

            //tạo DataTable
            DataTable dt = new DataTable();
            dt.Columns.Add("Lịch học", System.Type.GetType("System.String"));
            dt.Columns.Add("Lớp", System.Type.GetType("System.String"));
            dt.Columns.Add("Khoá học", System.Type.GetType("System.String"));
            dt.Columns.Add("Phòng", System.Type.GetType("System.String"));
            dt.Columns.Add("Cơ sở", System.Type.GetType("System.String"));
            dt.Columns.Add("Ngày học", System.Type.GetType("System.DateTime"));
            dt.Columns.Add("Ca học", System.Type.GetType("System.String"));
            dt.Columns.Add("Bắt đầu", System.Type.GetType("System.TimeSpan"));
            dt.Columns.Add("Kết thúc", System.Type.GetType("System.TimeSpan"));
            dt.Columns.Add("Tình trạng", System.Type.GetType("System.String"));

            //lấy dữ liệu
            var alias = from lich in context.LICHHOCs
                        where lich.LOPHOC.TT_GHIDANHs.Any(t => t.TT_DANGKYKHOAHOC.MAHOCVIEN == Helper.idUser)
                        select new
                        {
                            Lịch_học = lich.MALICHHOC,
                            Lớp = lich.MALOPHOC,
                            Mã_khoá = lich.LOPHOC.MAKHOAHOC,
                            Khoá_học = lich.LOPHOC.KHOAHOC.TENKHOAHOC,
                            Phòng = lich.MAPHONG,
                            Mã_cơ_sở = lich.PHONG.MACOSO,
                            Cơ_sở = lich.PHONG.COSO.TENCOSO,
                            Ngày_học = lich.NGAYHOC,
                            Ca_học = lich.MACA,
                            Bắt_đầu = lich.THOIGIANHOC.CAHOC.GIOBATDAU,
                            Kết_thúc = lich.THOIGIANHOC.CAHOC.GIOKETTHUC,
                            Tình_trạng = lich.TINHTRANG
                        };

            //chuyển sang array object ==> filter & search
            List<object[]> l = new List<object[]>();
            l.AddRange(alias.Select(t => new object[] { t.Lịch_học, t.Lớp, t.Mã_khoá, t.Khoá_học, t.Phòng, t.Mã_cơ_sở, t.Cơ_sở, t.Ngày_học, t.Ca_học, t.Bắt_đầu, t.Kết_thúc, t.Tình_trạng }));

            //filter
            if (indexColumnFilter != -1 && !String.IsNullOrEmpty(keyFilter) && !String.IsNullOrWhiteSpace(keyFilter))
            {
                if (indexColumnFilter == 1 || indexColumnFilter == 11) //mã khoá
                    l = l.Where(t => t[indexColumnFilter].ToString().Equals(keyFilter)).ToList();
            }
            //search
            if (!String.IsNullOrEmpty(keySearch) && !String.IsNullOrWhiteSpace(keySearch)) //search
                l = l.Where(t =>
                t.ToArray<object>()
                .Select(y => y == null ? "" : y.ToString()).ToList<string>()
                .Count(g => g.ToUpper().Contains(keySearch.ToUpper())) != 0)
                .ToList();

            //phân trang
            if (soluong != -1 && trang != -1)
                l = l.OrderBy(t => t[11].ToString()).ThenBy(t => (DateTime)t[7]).ThenBy(t => t[8].ToString()).Skip((trang - 1) * soluong).Take(soluong).ToList();

            //fill datatable
            l.ForEach(t => dt.Rows.Add(t[0], t[1], t[3], t[4], t[6], t[7], t[8], t[9], t[10], t[11]));
            return dt;
        }

        public object HienThiNhanVien(int trang, int soluong, int indexColumnFilter, string keyFilter, string keySearch)
        {
            keyFilter = keyFilter.Replace("_", " ");

            //tạo DataTable
            DataTable dt = new DataTable();
            dt.Columns.Add("Mã_nhân_viên", System.Type.GetType("System.String"));
            dt.Columns.Add("Họ_nhân_viên", System.Type.GetType("System.String"));
            dt.Columns.Add("Tên_nhân_viên", System.Type.GetType("System.String"));
            dt.Columns.Add("Giới_tính", System.Type.GetType("System.String"));
            dt.Columns.Add("Số_CMND", System.Type.GetType("System.String"));
            dt.Columns.Add("Số_điện_thoại", System.Type.GetType("System.String"));
            dt.Columns.Add("Mail", System.Type.GetType("System.String"));
            dt.Columns.Add("Địa_chỉ", System.Type.GetType("System.String"));

            //lấy dữ liệu
            var alias = from nhanvien in context.NHANVIENs
                        select new
                        {
                            Mã_nhân_viên = nhanvien.MANHANVIEN,
                            Họ_nhân_viên = Helper.GetHoTen(nhanvien.TENNHANVIEN).Ho,
                            Tên_nhân_viên = Helper.GetHoTen(nhanvien.TENNHANVIEN).Ten,
                            Giới_tính = nhanvien.GIOITINH, //3
                            Số_CMND = nhanvien.CMND,
                            Số_điện_thoại = nhanvien.SODIENTHOAI,
                            Mail = nhanvien.MAIL,
                            Địa_chỉ = nhanvien.DIACHI
                        };

            //chuyển sang array object ==> filter & search
            List<object[]> l = new List<object[]>();
            l.AddRange(alias.Select(t => new object[] { t.Mã_nhân_viên, t.Họ_nhân_viên, t.Tên_nhân_viên, t.Giới_tính,
                t.Số_CMND, t.Số_điện_thoại, t.Mail, t.Địa_chỉ}));

            //filter
            if (indexColumnFilter != -1 && !String.IsNullOrEmpty(keyFilter) && !String.IsNullOrWhiteSpace(keyFilter))
            {
                l = l.Where(t => t[indexColumnFilter].ToString().Equals(keyFilter)).ToList();
            }
            //search
            if (!String.IsNullOrEmpty(keySearch) && !String.IsNullOrWhiteSpace(keySearch)) //search
                l = l.Where(t =>
                t.ToArray<object>()
                .Select(y => y == null ? "" : y.ToString()).ToList<string>()
                .Count(g => g.ToUpper().Contains(keySearch.ToUpper())) != 0)
                .ToList();

            //phân trang
            l = l.OrderBy(t => t[0].ToString()).Skip((trang - 1) * soluong).Take(soluong).ToList();

            //fill datatable
            l.ForEach(t => dt.Rows.Add(t[0], t[1], t[2], t[3], t[4], t[5], t[6], t[7]));
            return dt;
        }

        public object HienThiHocVien(int trang, int soluong, int indexColumnFilter, string keyFilter, string keySearch)
        {
            keyFilter = keyFilter.Replace("_", " ");

            //tạo DataTable
            DataTable dt = new DataTable();
            dt.Columns.Add("Mã_học_viên", System.Type.GetType("System.String"));
            dt.Columns.Add("Họ_học_viên", System.Type.GetType("System.String"));
            dt.Columns.Add("Tên_học_viên", System.Type.GetType("System.String"));
            dt.Columns.Add("Giới_tính", System.Type.GetType("System.String"));
            dt.Columns.Add("Số_CMND", System.Type.GetType("System.String"));
            dt.Columns.Add("Số_điện_thoại", System.Type.GetType("System.String"));
            dt.Columns.Add("Mail", System.Type.GetType("System.String"));
            dt.Columns.Add("Địa_chỉ", System.Type.GetType("System.String"));

            //lấy dữ liệu
            var alias = from hocvien in context.HOCVIENs
                        select new
                        {
                            Mã_học_viên = hocvien.MAHOCVIEN,
                            Họ_học_viên = Helper.GetHoTen(hocvien.TENHOCVIEN).Ho,
                            Tên_học_viên = Helper.GetHoTen(hocvien.TENHOCVIEN).Ten,
                            Giới_tính = hocvien.GIOITINH, //3
                            Số_CMND = hocvien.CMND,
                            Số_điện_thoại = hocvien.SODIENTHOAI,
                            Mail = hocvien.MAIL,
                            Địa_chỉ = hocvien.DIACHI,
                        };

            //chuyển sang array object ==> filter & search
            List<object[]> l = new List<object[]>();
            l.AddRange(alias.Select(t => new object[] { t.Mã_học_viên, t.Họ_học_viên, t.Tên_học_viên, t.Giới_tính,
                t.Số_CMND, t.Số_điện_thoại, t.Mail, t.Địa_chỉ}));

            //filter
            if (indexColumnFilter != -1 && !String.IsNullOrEmpty(keyFilter) && !String.IsNullOrWhiteSpace(keyFilter))
            {
                l = l.Where(t => t[indexColumnFilter].ToString().Equals(keyFilter)).ToList();
            }
            //search
            if (!String.IsNullOrEmpty(keySearch) && !String.IsNullOrWhiteSpace(keySearch)) //search
                l = l.Where(t =>
                t.ToArray<object>()
                .Select(y => y == null ? "" : y.ToString()).ToList<string>()
                .Count(g => g.ToUpper().Contains(keySearch.ToUpper())) != 0)
                .ToList();

            //phân trang
            l = l.OrderBy(t => t[0].ToString()).Skip((trang - 1) * soluong).Take(soluong).ToList();

            //fill datatable
            l.ForEach(t => dt.Rows.Add(t[0], t[1], t[2], t[3], t[4], t[5], t[6], t[7]));
            return dt;
        }

        public object HienThiGiangVien(int trang, int soluong, int indexColumnFilter, string keyFilter, string keySearch)
        {
            keyFilter = keyFilter.Replace("_", " ");

            //tạo DataTable
            DataTable dt = new DataTable();
            dt.Columns.Add("Mã_giảng_viên", System.Type.GetType("System.String"));
            dt.Columns.Add("Họ_giảng_viên", System.Type.GetType("System.String"));
            dt.Columns.Add("Tên_giảng_viên", System.Type.GetType("System.String"));
            dt.Columns.Add("Giới_tính", System.Type.GetType("System.String"));
            dt.Columns.Add("Số_CMND", System.Type.GetType("System.String"));
            dt.Columns.Add("Số_điện_thoại", System.Type.GetType("System.String"));
            dt.Columns.Add("Mail", System.Type.GetType("System.String"));
            dt.Columns.Add("Địa_chỉ", System.Type.GetType("System.String"));

            //lấy dữ liệu
            var alias = from giangvien in context.GIANGVIENs
                        select new
                        {
                            Mã_giảng_viên = giangvien.MAGIANGVIEN,
                            Họ_giảng_viên = Helper.GetHoTen(giangvien.TENGIANGVIEN).Ho,
                            Tên_giảng_viên = Helper.GetHoTen(giangvien.TENGIANGVIEN).Ten,
                            Giới_tính = giangvien.GIOITINH, //3
                            Số_CMND = giangvien.CMND,
                            Số_điện_thoại = giangvien.SODIENTHOAI,
                            Mail = giangvien.MAIL,
                            Địa_chỉ = giangvien.DIACHI,
                        };

            //chuyển sang array object ==> filter & search
            List<object[]> l = new List<object[]>();
            l.AddRange(alias.Select(t => new object[] { t.Mã_giảng_viên, t.Họ_giảng_viên, t.Tên_giảng_viên, t.Giới_tính,
                t.Số_CMND, t.Số_điện_thoại, t.Mail, t.Địa_chỉ}));

            //filter
            if (indexColumnFilter != -1 && !String.IsNullOrEmpty(keyFilter) && !String.IsNullOrWhiteSpace(keyFilter))
            {
                l = l.Where(t => t[indexColumnFilter].ToString().Equals(keyFilter)).ToList();
            }
            //search
            if (!String.IsNullOrEmpty(keySearch) && !String.IsNullOrWhiteSpace(keySearch)) //search
                l = l.Where(t =>
                t.ToArray<object>()
                .Select(y => y == null ? "" : y.ToString()).ToList<string>()
                .Count(g => g.ToUpper().Contains(keySearch.ToUpper())) != 0)
                .ToList();

            //phân trang
            l = l.OrderBy(t => t[0].ToString()).Skip((trang - 1) * soluong).Take(soluong).ToList();

            //fill datatable
            l.ForEach(t => dt.Rows.Add(t[0], t[1], t[2], t[3], t[4], t[5], t[6], t[7]));
            return dt;
        }

        //*********************************************************************************************************************
        public List<object> HienThiTatCaDiem()
        {
            var q = (from d in context.DIEMs
                     from h in context.HOCVIENs
                     from l in context.LOPHOCs
                     from ld in context.LOAIDIEMs
                     from k in context.KHOAHOCs
                     from dk in context.TT_DANGKYKHOAHOCs
                     where d.MADANGKYKHOAHOC == dk.MADANGKYKHOAHOC
                         && dk.MAHOCVIEN == h.MAHOCVIEN
                         && dk.MAKHOAHOC == k.MAKHOAHOC
                         && d.MALOPHOC == l.MALOPHOC
                         && d.MALOAIDIEM == ld.MALOAIDIEM
                         && k.MAKHOAHOC == l.MAKHOAHOC
                     select new
                     {
                         Mã_điểm = d.MADIEM,
                         Mã_học_viên = h.MAHOCVIEN,
                         Tên_học_viên = h.TENHOCVIEN,
                         Mã_lớp = l.MALOPHOC,
                         Tên_khoá_học = k.TENKHOAHOC,
                         Loại_điểm = ld.TENLOAIDIEM,
                         Lần = d.LAN,
                         Điểm = d.DIEM1
                     });

            return q.ToList<object>();
        }
        public List<object> HienThiTatCaDiem(int trang, int soluong)
        {
            var q = (from d in context.DIEMs
                     from h in context.HOCVIENs
                     from dk in context.TT_DANGKYKHOAHOCs
                     from ld in context.LOAIDIEMs
                     from k in context.KHOAHOCs
                     where dk.MAHOCVIEN == h.MAHOCVIEN
                        && dk.MADANGKYKHOAHOC == d.MADANGKYKHOAHOC
                        && dk.MAKHOAHOC == k.MAKHOAHOC
                         && d.MALOAIDIEM == ld.MALOAIDIEM
                     select new
                     {
                         Mã_đăng_ký = d.MADANGKYKHOAHOC,
                         Mã_điểm = d.MADIEM,
                         Mã_học_viên = h.MAHOCVIEN,
                         Tên_học_viên = h.TENHOCVIEN,
                         Mã_lớp = d.MALOPHOC,
                         Tên_khoá_học = k.TENKHOAHOC,
                         Loại_điểm = ld.TENLOAIDIEM,
                         Lần = d.LAN,
                         Điểm = d.DIEM1
                     }).Skip((trang - 1) * soluong).Take(soluong);

            return q.ToList<object>();
        }
        public List<object> HienThiTatCaDiemHocVien(string mahocvien)
        {
            var q = (from d in context.DIEMs
                     from dk in context.TT_DANGKYKHOAHOCs
                     from l in context.LOPHOCs
                     from ld in context.LOAIDIEMs
                     from k in context.KHOAHOCs
                     where dk.MAHOCVIEN == mahocvien
                        && dk.MADANGKYKHOAHOC == d.MADANGKYKHOAHOC
                        && dk.MAKHOAHOC == k.MAKHOAHOC
                         && d.MALOPHOC == l.MALOPHOC
                         && d.MALOAIDIEM == ld.MALOAIDIEM
                         && k.MAKHOAHOC == l.MAKHOAHOC
                     select new
                     {
                         Mã_lớp = l.MALOPHOC,
                         Tên_khoá_học = k.TENKHOAHOC,
                         Loại_điểm = ld.TENLOAIDIEM,
                         Lần = d.LAN,
                         Điểm = d.DIEM1
                     });

            return q.ToList<object>();
        }
        public List<object> HienThiTatCaDiemHocVien(int trang, int soluong, string mahocvien)
        {
            var q = (from d in context.DIEMs
                     from dk in context.TT_DANGKYKHOAHOCs
                     from l in context.LOPHOCs
                     from ld in context.LOAIDIEMs
                     from k in context.KHOAHOCs
                     where dk.MAHOCVIEN == mahocvien
                        && dk.MAKHOAHOC == k.MAKHOAHOC
                        && dk.MADANGKYKHOAHOC == d.MADANGKYKHOAHOC
                         && d.MALOPHOC == l.MALOPHOC
                         && d.MALOAIDIEM == ld.MALOAIDIEM
                         && k.MAKHOAHOC == l.MAKHOAHOC
                     select new
                     {
                         Mã_lớp = l.MALOPHOC,
                         Tên_khoá_học = k.TENKHOAHOC,
                         Loại_điểm = ld.TENLOAIDIEM,
                         Lần = d.LAN,
                         Điểm = d.DIEM1
                     }).Skip((trang - 1) * soluong).Take(soluong);

            return q.ToList<object>();
        }
        public List<object> HienThiTatCaDiemHocVienLopHoc(string mahocvien, string malophoc)
        {
            var q = (from d in context.DIEMs
                     from l in context.LOPHOCs
                     from ld in context.LOAIDIEMs
                     from dk in context.TT_DANGKYKHOAHOCs
                     from k in context.KHOAHOCs
                     where dk.MAHOCVIEN == mahocvien
                     && dk.MAKHOAHOC == k.MAKHOAHOC
                     && dk.MADANGKYKHOAHOC == d.MADANGKYKHOAHOC
                         && d.MALOPHOC == malophoc
                         && d.MALOPHOC == l.MALOPHOC
                         && d.MALOAIDIEM == ld.MALOAIDIEM
                         && k.MAKHOAHOC == l.MAKHOAHOC
                     select new
                     {
                         Mã_lớp = l.MALOPHOC,
                         Tên_khoá_học = k.TENKHOAHOC,
                         Loại_điểm = ld.TENLOAIDIEM,
                         Lần = d.LAN,
                         Điểm = d.DIEM1
                     });

            return q.ToList<object>();
        }
        public List<object> HienThiTatCaDiemHocVienLopHoc(int trang, int soluong, string mahocvien, string malophoc)
        {
            var q = (from d in context.DIEMs
                     from l in context.LOPHOCs
                     from ld in context.LOAIDIEMs
                     from k in context.KHOAHOCs
                     from dk in context.TT_DANGKYKHOAHOCs
                     where dk.MAHOCVIEN == mahocvien
                     && dk.MAKHOAHOC == k.MAKHOAHOC
                     && dk.MADANGKYKHOAHOC == d.MADANGKYKHOAHOC
                         && d.MALOPHOC == malophoc
                         && d.MALOPHOC == l.MALOPHOC
                         && d.MALOAIDIEM == ld.MALOAIDIEM
                         && k.MAKHOAHOC == l.MAKHOAHOC
                     select new
                     {
                         Mã_lớp = l.MALOPHOC,
                         Tên_khoá_học = k.TENKHOAHOC,
                         Loại_điểm = ld.TENLOAIDIEM,
                         Lần = d.LAN,
                         Điểm = d.DIEM1
                     }).Skip((trang - 1) * soluong).Take(soluong);

            return q.ToList<object>();
        }
        public List<object> HienThiTatCaLopHoc()
        {
            var q = (from k in context.KHOAHOCs
                     from l in context.LOPHOCs
                     from gv in context.GIANGVIENs
                     where k.MAKHOAHOC == l.MAKHOAHOC
                         && l.MAGIANGVIEN == gv.MAGIANGVIEN
                     select new
                     {
                         Mã_lớp = l.MALOPHOC,
                         Mã_khoá = k.MAKHOAHOC,
                         Tên_khoá_học = k.TENKHOAHOC,
                         Mã_giảng_viên = gv.MAGIANGVIEN,
                         Tên_giảng_viên = gv.TENGIANGVIEN,
                         Số_lượng_hv = l.SOLUONG,
                         Học_phí = k.HOCPHI
                     });

            return q.ToList<object>();
        }
        public List<object> HienThiTatCaLopHoc(int trang, int soluong)
        {
            var q = (from k in context.KHOAHOCs
                     from l in context.LOPHOCs
                     from gv in context.GIANGVIENs
                     where k.MAKHOAHOC == l.MAKHOAHOC
                         && l.MAGIANGVIEN == gv.MAGIANGVIEN
                     select new
                     {
                         Mã_lớp = l.MALOPHOC,
                         Mã_khoá = k.MAKHOAHOC,
                         Tên_khoá_học = k.TENKHOAHOC,
                         Mã_giảng_viên = gv.MAGIANGVIEN,
                         Tên_giảng_viên = gv.TENGIANGVIEN,
                         Số_lượng_hv = l.SOLUONG,
                         Học_phí = k.HOCPHI
                     }).Skip((trang - 1) * soluong).Take(soluong);

            return q.ToList<object>();
        }
        public List<object> HienThiMailLopHoc(string malophoc)
        {
            var c = (from h in context.HOCVIENs
                     from t in context.TT_GHIDANHs
                     from dk in context.TT_DANGKYKHOAHOCs
                     where dk.MADANGKYKHOAHOC == t.MADANGKYKHOAHOC
                     && h.MAHOCVIEN == dk.MAHOCVIEN
                        && t.MALOPHOC == malophoc
                     select new { Mã_tài_khoản = h.MATAIKHOAN, Người_nhận = h.TENHOCVIEN, Địa_chỉ_mail = h.MAIL, Số_điện_thoại = h.SODIENTHOAI }).ToList<object>();
            var d = (from gv in context.GIANGVIENs
                     from l in context.LOPHOCs
                     where l.MAGIANGVIEN == gv.MAGIANGVIEN
                        && l.MALOPHOC == malophoc
                     select new { Mã_tài_khoản = gv.MATAIKHOAN, Người_nhận = gv.TENGIANGVIEN, Địa_chỉ_mail = gv.MAIL, Số_điện_thoại = gv.SODIENTHOAI }).FirstOrDefault();
            if (d != null)
                c.Add(d);
            return c;
        }
        public List<object> HienThiTatCaHocVien(int @trang, int @soluong)
        {
            var hv = (from hocvien in context.HOCVIENs
                      select new
                      {
                          Mã_học_viên = hocvien.MAHOCVIEN,
                          Họ_học_viên = Helper.GetHoTen(hocvien.TENHOCVIEN).Ho,
                          Tên_học_viên = Helper.GetHoTen(hocvien.TENHOCVIEN).Ten,
                          Giới_tính = hocvien.GIOITINH,
                          Số_CMND = hocvien.CMND,
                          Số_điện_thoại = hocvien.SODIENTHOAI,
                          Mail = hocvien.MAIL,
                          Địa_chỉ = hocvien.DIACHI,
                      }).Skip((trang - 1) * soluong).Take(soluong);
            return hv.ToList<object>();
        }
        public List<object> HienThiTatCaHocVien()
        {
            var hv = (from hocvien in context.HOCVIENs
                      select new
                      {
                          Mã_học_viên = hocvien.MAHOCVIEN,
                          Họ_học_viên = Helper.GetHoTen(hocvien.TENHOCVIEN).Ho,
                          Tên_học_viên = Helper.GetHoTen(hocvien.TENHOCVIEN).Ten,
                          Giới_tính = hocvien.GIOITINH,
                          Số_CMND = hocvien.CMND,
                          Số_điện_thoại = hocvien.SODIENTHOAI,
                          Mail = hocvien.MAIL,
                          Địa_chỉ = hocvien.DIACHI,
                      });
            return hv.ToList<object>();
        }
        public List<object> HienThiTatCaNhanVien(int @trang, int @soluong)
        {
            var nv = (from hocvien in context.NHANVIENs
                      select new
                      {
                          Mã_nhân_viên = hocvien.MANHANVIEN,
                          Họ_nhân_viên = Helper.GetHoTen(hocvien.TENNHANVIEN).Ho,
                          Tên_nhân_viên = Helper.GetHoTen(hocvien.TENNHANVIEN).Ten,
                          Giới_tính = hocvien.GIOITINH,
                          Số_CMND = hocvien.CMND,
                          Số_điện_thoại = hocvien.SODIENTHOAI,
                          Mail = hocvien.MAIL,
                          Địa_chỉ = hocvien.DIACHI,
                      }).Skip((trang - 1) * soluong).Take(soluong);
            return nv.ToList<object>();
        }
        public List<object> HienThiTatCaNhanVien()
        {
            var nv = (from hocvien in context.NHANVIENs
                      select new
                      {
                          Mã_nhân_viên = hocvien.MANHANVIEN,
                          Họ_nhân_viên = Helper.GetHoTen(hocvien.TENNHANVIEN).Ho,
                          Tên_nhân_viên = Helper.GetHoTen(hocvien.TENNHANVIEN).Ten,
                          Giới_tính = hocvien.GIOITINH,
                          Số_CMND = hocvien.CMND,
                          Số_điện_thoại = hocvien.SODIENTHOAI,
                          Mail = hocvien.MAIL,
                          Địa_chỉ = hocvien.DIACHI,
                      });
            return nv.ToList<object>();
        }
        public List<object> HienThiTatCaGiangVien(int @trang, int @soluong)
        {
            var gv = (from giangvien in context.GIANGVIENs
                      select new
                      {
                          Mã_giảng_viên = giangvien.MAGIANGVIEN,
                          Họ_giảng_viên = Helper.GetHoTen(giangvien.TENGIANGVIEN).Ho,
                          Tên_giảng_viên = Helper.GetHoTen(giangvien.TENGIANGVIEN).Ten,
                          Giới_tính = giangvien.GIOITINH,
                          Số_CMND = giangvien.CMND,
                          Số_điện_thoại = giangvien.SODIENTHOAI,
                          Mail = giangvien.MAIL,
                          Địa_chỉ = giangvien.DIACHI,
                          Trình_độ = giangvien.TRINHDO,
                          Chuyên_môn = giangvien.CHUYENMON,
                      }).Skip((trang - 1) * soluong).Take(soluong);
            return gv.ToList<object>();
        }
        public List<object> HienThiTatCaGiangVien()
        {
            var gv = (from giangvien in context.GIANGVIENs
                      select new
                      {
                          Mã_giảng_viên = giangvien.MAGIANGVIEN,
                          Họ_giảng_viên = Helper.GetHoTen(giangvien.TENGIANGVIEN).Ho,
                          Tên_giảng_viên = Helper.GetHoTen(giangvien.TENGIANGVIEN).Ten,
                          Giới_tính = giangvien.GIOITINH,
                          Số_CMND = giangvien.CMND,
                          Số_điện_thoại = giangvien.SODIENTHOAI,
                          Mail = giangvien.MAIL,
                          Địa_chỉ = giangvien.DIACHI,
                          Trình_độ = giangvien.TRINHDO,
                          Chuyên_môn = giangvien.CHUYENMON,
                      });
            return gv.ToList<object>();
        }




        //thống kê
        //thống kê các biên lai 1 học viên tham gia tất cả khoá học tại trung tâm
        public object DoanhThuTheoHocVien_Grid(string mahocvien, DateTime ngaybatdau, DateTime ngayketthuc, int trang, int soluong)
        {


            var c = from bl in context.BIENLAIs
                    where
                    bl.TT_DANGKYKHOAHOC.MAHOCVIEN == mahocvien
                    && bl.NGAYXACNHAN >= ngaybatdau
                    && bl.NGAYXACNHAN <= ngayketthuc
                    select new
                    {
                        Mã_biên_lai = bl.MABIENLAI,
                        Mã_đăng_ký = bl.MADANGKYKHOAHOC,
                        Mã_lớp_học = bl.MALOPHOC,
                        Tên_khoá_học = bl.TT_DANGKYKHOAHOC.KHOAHOC.TENKHOAHOC,
                        Mã_nhân_viên = bl.NHANVIEN.MANHANVIEN,
                        Tên_nhân_viên = bl.NHANVIEN.TENNHANVIEN,
                        Ngày_thu = bl.NGAYXACNHAN.ToShortDateString(),
                        Hình_thức = bl.LOAI_BIENLAI.TENLOAIBIENLAI,
                        Đợt_thu = bl.DOT,
                        Số_tiền = bl.SOTIEN
                    };


            if (soluong != -1 && trang != -1)
                return c.Skip((trang - 1) * soluong).Take(soluong).ToList();


            //thêm stt
            List<object> list = new List<object>();
            List<object[]> array_object_list = c.Select(t => new object[]
            {
                t.Mã_biên_lai,
                t.Mã_đăng_ký,
                t.Mã_lớp_học,
                t.Tên_khoá_học,
               t.Mã_nhân_viên,
                 t.Tên_nhân_viên,
                t.Ngày_thu,
               t.Hình_thức,
               t.Đợt_thu,
               t.Số_tiền
            }).ToList<object[]>();

            for (int i = 0; i < array_object_list.Count; i++)
                list.Add(new
                {
                    STT = i + 1,
                    Mã_biên_lai = array_object_list[i][0],
                    Mã_đăng_ký = array_object_list[i][1],
                    Mã_lớp_học = array_object_list[i][2],
                    Tên_khoá_học = array_object_list[i][3],
                    Mã_nhân_viên = array_object_list[i][4],
                    Tên_nhân_viên = array_object_list[i][5],
                    Ngày_thu = array_object_list[i][6],
                    Hình_thức = array_object_list[i][7],
                    Đợt_thu = array_object_list[i][8],
                    Số_tiền = array_object_list[i][9],
                });
            return list.ToList<object>();
        }

        //thống kê các biên lai của học viên 1 lớp học
        public object DoanhThuTheoLopHoc_Grid(string malop, DateTime ngaybatdau, DateTime ngayketthuc, int trang, int soluong)
        {

            var c = from bl in context.BIENLAIs
                    where bl.MALOPHOC == malop
                    && bl.NGAYXACNHAN >= ngaybatdau
                    && bl.NGAYXACNHAN <= ngayketthuc
                    select new
                    {
                        Mã_biên_lai = bl.MABIENLAI,
                        Mã_đăng_ký = bl.MADANGKYKHOAHOC,
                        Mã_học_viên = bl.TT_DANGKYKHOAHOC.MAHOCVIEN,
                        Tên_học_viên = bl.TT_DANGKYKHOAHOC.HOCVIEN.TENHOCVIEN,
                        Mã_nhân_viên = bl.NHANVIEN.MANHANVIEN,
                        Tên_nhân_viên = bl.NHANVIEN.TENNHANVIEN,
                        Ngày_thu = bl.NGAYXACNHAN.ToShortDateString(),
                        Hình_thức = bl.LOAI_BIENLAI.TENLOAIBIENLAI,
                        Đợt_thu = bl.DOT,
                        Số_tiền = bl.SOTIEN,
                    };
            if (soluong != -1 && trang != -1)
                return c.Skip((trang - 1) * soluong).Take(soluong).ToList();


            //thêm stt
            List<object> list = new List<object>();
            List<object[]> array_object_list = c.Select(t => new object[]
            {
                t.Mã_biên_lai,
                t.Mã_đăng_ký,
                t.Mã_học_viên,
                t.Tên_học_viên,
               t.Mã_nhân_viên,
                 t.Tên_nhân_viên,
                t.Ngày_thu,
               t.Hình_thức,
               t.Đợt_thu,
               t.Số_tiền
            }).ToList<object[]>();

            for (int i = 0; i < array_object_list.Count; i++)
                list.Add(new
                {
                    STT = i + 1,
                    Mã_biên_lai = array_object_list[i][0],
                    Mã_đăng_ký = array_object_list[i][1],
                    Mã_học_viên = array_object_list[i][2],
                    Tên_học_viên = array_object_list[i][3],
                    Mã_nhân_viên = array_object_list[i][4],
                    Tên_nhân_viên = array_object_list[i][5],
                    Ngày_thu = array_object_list[i][6],
                    Hình_thức = array_object_list[i][7],
                    Đợt_thu = array_object_list[i][8],
                    Số_tiền = array_object_list[i][9],
                });
            return list.ToList<object>();
        }

        //thống kê các biên lai của học viên 1 khoá học
        public object DoanhThuTheoKhoaHoc_Grid(string makhoa, DateTime ngaybatdau, DateTime ngayketthuc, int trang, int soluong)
        {
            var c = from bl in context.BIENLAIs
                    where bl.NGAYXACNHAN >= ngaybatdau
                    && bl.NGAYXACNHAN <= ngayketthuc

                    && bl.TT_DANGKYKHOAHOC.MAKHOAHOC == makhoa
                    select new
                    {
                        Mã_biên_lai = bl.MABIENLAI,
                        Mã_đăng_ký = bl.MADANGKYKHOAHOC,
                        Mã_học_viên = bl.TT_DANGKYKHOAHOC.HOCVIEN.MAHOCVIEN,
                        Tên_học_viên = bl.TT_DANGKYKHOAHOC.HOCVIEN.TENHOCVIEN,
                        Mã_lớp_học = bl.MALOPHOC,
                        Mã_nhân_viên = bl.NHANVIEN.MANHANVIEN,
                        Tên_nhân_viên = bl.NHANVIEN.TENNHANVIEN,
                        Ngày_thu = bl.NGAYXACNHAN.ToShortDateString(),
                        Hình_thức = bl.LOAI_BIENLAI.TENLOAIBIENLAI,
                        Đợt_thu = bl.DOT,
                        Số_tiền = bl.SOTIEN,
                    };
            if (soluong != -1 && trang != -1)
                return c.Skip((trang - 1) * soluong).Take(soluong).ToList();

            //thêm stt
            List<object> list = new List<object>();
            List<object[]> array_object_list = c.Select(t => new object[]
            {
                t.Mã_biên_lai,
                t.Mã_đăng_ký,
                t.Mã_học_viên,
                t.Tên_học_viên,
                t.Mã_lớp_học,
                t.Mã_nhân_viên,
                t.Tên_nhân_viên,
                t.Ngày_thu,
                t.Hình_thức,
                t.Đợt_thu,
                t.Số_tiền
            }).ToList<object[]>();

            for (int i = 0; i < array_object_list.Count; i++)
                list.Add(new
                {
                    STT = i + 1,
                    Mã_biên_lai = array_object_list[i][0],
                    Mã_đăng_ký = array_object_list[i][1],
                    Mã_học_viên = array_object_list[i][2],
                    Tên_học_viên = array_object_list[i][3],
                    Mã_lớp_học = array_object_list[i][4],
                    Mã_nhân_viên = array_object_list[i][5],
                    Tên_nhân_viên = array_object_list[i][6],
                    Ngày_thu = array_object_list[i][7],
                    Hình_thức_thu = array_object_list[i][8],
                    Đợt_thu = array_object_list[i][9],
                    Số_tiền = array_object_list[i][10],
                });
            return list.ToList<object>();
        }

        //thống kê các biên lai của học viên 1 cơ sở
        public object DoanhThuTheoCoSo_Grid(string macoso, DateTime ngaybatdau, DateTime ngayketthuc, int trang, int soluong)
        {
            var c = from bl in context.BIENLAIs
                    where bl.NGAYXACNHAN >= ngaybatdau
                    && bl.NGAYXACNHAN <= ngayketthuc

                    && bl.TT_DANGKYKHOAHOC.TT_GHIDANHs.Any(T => T.LOPHOC.LICHHOCs.Count(t => t.PHONG.MACOSO == macoso) != 0)
                    select new
                    {
                        Mã_biên_lai = bl.MABIENLAI,
                        Mã_đăng_ký = bl.MADANGKYKHOAHOC,
                        Mã_học_viên = bl.TT_DANGKYKHOAHOC.HOCVIEN.MAHOCVIEN,
                        Tên_học_viên = bl.TT_DANGKYKHOAHOC.HOCVIEN.TENHOCVIEN,
                        Mã_lớp_học = bl.MALOPHOC,
                        Mã_khoá_học = bl.TT_DANGKYKHOAHOC.MAKHOAHOC,
                        Tên_khoá_học = bl.TT_DANGKYKHOAHOC.KHOAHOC.TENKHOAHOC,
                        Mã_nhân_viên = bl.NHANVIEN.MANHANVIEN,
                        Tên_nhân_viên = bl.NHANVIEN.TENNHANVIEN,
                        Ngày_thu = bl.NGAYXACNHAN.ToShortDateString(),
                        Hình_thức = bl.LOAI_BIENLAI.TENLOAIBIENLAI,
                        Đợt_thu = bl.DOT,
                        Số_tiền = bl.SOTIEN,
                    };
            if (soluong != -1 && trang != -1)
                return c.Skip((trang - 1) * soluong).Take(soluong).ToList();

            //thêm stt
            List<object> list = new List<object>();
            List<object[]> array_object_list = c.Select(t => new object[]
            {
                t.Mã_biên_lai,
                t.Mã_đăng_ký,
                t.Mã_học_viên,
                t.Tên_học_viên,
                t.Mã_lớp_học,
                t.Mã_khoá_học,
                t.Tên_khoá_học,
                t.Mã_nhân_viên,
                t.Tên_nhân_viên,
                t.Ngày_thu,
                t.Hình_thức,
                t.Đợt_thu,
                t.Số_tiền
            }).ToList<object[]>();

            for (int i = 0; i < array_object_list.Count; i++)
                list.Add(new
                {
                    STT = i + 1,
                    Mã_biên_lai = array_object_list[i][0],
                    Mã_đăng_ký = array_object_list[i][1],
                    Mã_học_viên = array_object_list[i][2],
                    Tên_học_viên = array_object_list[i][3],
                    Mã_lớp_học = array_object_list[i][4],
                    Mã_khoá_học = array_object_list[i][5],
                    Tên_khoá_học = array_object_list[i][6],
                    Mã_nhân_viên = array_object_list[i][7],
                    Tên_nhân_viên = array_object_list[i][8],
                    Ngày_thu = array_object_list[i][9],
                    Hình_thức = array_object_list[i][10],
                    Đợt_thu = array_object_list[i][11],
                    Số_tiền = array_object_list[i][12],
                });
            return list.ToList<object>();
        }


        //thống kê các biên lai của học viên
        public object DoanhThuTheoTrungTam_Grid(string v, DateTime ngaybatdau, DateTime ngayketthuc, int trang, int soluong)
        {
            var c = from bl in context.BIENLAIs
                    where bl.NGAYXACNHAN >= ngaybatdau
                    && bl.NGAYXACNHAN <= ngayketthuc

                    select new
                    {
                        Mã_biên_lai = bl.MABIENLAI,
                        Mã_đăng_ký = bl.MADANGKYKHOAHOC,
                        Mã_học_viên = bl.TT_DANGKYKHOAHOC.HOCVIEN.MAHOCVIEN,
                        Tên_học_viên = bl.TT_DANGKYKHOAHOC.HOCVIEN.TENHOCVIEN,
                        Mã_lớp_học = bl.MALOPHOC,
                        Mã_khoá_học = bl.TT_DANGKYKHOAHOC.KHOAHOC.MAKHOAHOC,
                        Tên_khoá_học = bl.TT_DANGKYKHOAHOC.KHOAHOC.TENKHOAHOC,
                        Mã_nhân_viên = bl.NHANVIEN.MANHANVIEN,
                        Tên_nhân_viên = bl.NHANVIEN.TENNHANVIEN,
                        Ngày_thu = bl.NGAYXACNHAN.ToShortDateString(),
                        Hình_thức = bl.LOAI_BIENLAI.TENLOAIBIENLAI,
                        Đợt_thu = bl.DOT,
                        Số_tiền = bl.SOTIEN,
                    };
            if (soluong != -1 && trang != -1)
                return c.Skip((trang - 1) * soluong).Take(soluong).ToList();

            //thêm stt
            List<object> list = new List<object>();
            List<object[]> array_object_list = c.Select(t => new object[]
            {
                t.Mã_biên_lai,
                t.Mã_đăng_ký,
                t.Mã_học_viên,
                t.Tên_học_viên,
                t.Mã_lớp_học,
                t.Mã_khoá_học,
                t.Tên_khoá_học,
                t.Mã_nhân_viên,
                t.Tên_nhân_viên,
                t.Ngày_thu,
                t.Hình_thức,
                t.Đợt_thu,
                t.Số_tiền
            }).ToList<object[]>();

            for (int i = 0; i < array_object_list.Count; i++)
                list.Add(new
                {
                    STT = i + 1,
                    Mã_biên_lai = array_object_list[i][0],
                    Mã_đăng_ký = array_object_list[i][1],
                    Mã_học_viên = array_object_list[i][2],
                    Tên_học_viên = array_object_list[i][3],
                    Mã_lớp_học = array_object_list[i][4],
                    Mã_khoá_học = array_object_list[i][5],
                    Tên_khoá_học = array_object_list[i][6],
                    Mã_nhân_viên = array_object_list[i][7],
                    Tên_nhân_viên = array_object_list[i][8],
                    Ngày_thu = array_object_list[i][9],
                    Hình_thức = array_object_list[i][10],
                    Đợt_thu = array_object_list[i][11],
                    Số_tiền = array_object_list[i][12],
                });
            return list.ToList<object>();
        }


        //tổng doanh thu *****************************************
        public object TongDoanhThuTheoTrungTam_Grid(string v, DateTime ngaybatdau, DateTime ngayketthuc, int trang, int soluong)
        {
            List<object> listData = new List<object>();
            foreach (COSO cs in context.COSOs)
            {
                int countBienLai = context.BIENLAIs.Count(t => t.TT_DANGKYKHOAHOC.TT_GHIDANHs.Any(T => T.LOPHOC.LICHHOCs.Any(a => a.MACOSO == cs.MACOSO))
                && t.NGAYXACNHAN >= ngaybatdau && t.NGAYXACNHAN <= ngayketthuc);

                int countGhiDanh = context.TT_GHIDANHs.Count(t => t.LOPHOC.LICHHOCs.Any(a => a.MACOSO == cs.MACOSO)
                && t.TT_DANGKYKHOAHOC.NGAY >= ngaybatdau && t.TT_DANGKYKHOAHOC.NGAY <= ngayketthuc);

                int countLopHoc = context.LOPHOCs.Count(t => t.LICHHOCs.Any(a => a.MACOSO == cs.MACOSO)
                && t.LICHHOCs.First().NGAYHOC >= ngaybatdau && t.LICHHOCs.First().NGAYHOC <= ngayketthuc);

                int countKhoaHoc = context.KHOAHOCs.Count(t => t.LOPHOCs.Any(l => l.LICHHOCs.Any(a => a.MACOSO == cs.MACOSO)));
                double? sumDoanhThu = context.BIENLAIs.Where(t => t.TT_DANGKYKHOAHOC.TT_GHIDANHs.Any(T => T.LOPHOC.LICHHOCs.Any(a => a.MACOSO == cs.MACOSO))).Sum(s => (double?)s.SOTIEN);
                double? sumPhaiThu = context.LOPHOCs.Where(t => t.LICHHOCs.Any(a => a.MACOSO == cs.MACOSO)).Sum(l => (double?)l.SOLUONG * (double?)l.KHOAHOC.HOCPHI);
                double? sumConThieu = sumPhaiThu - sumDoanhThu;

                if (soluong != -1 && trang != -1)
                    listData.Add(new
                    {
                        Mã_cơ_sở = cs.MACOSO,
                        Tên_cơ_sở = cs.TENCOSO,
                        Tổng_số_biên_lai = countBienLai,
                        Tổng_số_ghi_danh = countGhiDanh,
                        Tổng_số_lớp_học_mở = countLopHoc,
                        Tổng_số_khoá_học = countKhoaHoc,
                        Tổng_doanh_thu = sumDoanhThu,
                        Tổng_phải_thu = sumPhaiThu,
                        Tổng_còn_thiếu = sumConThieu
                    });
                else
                    listData.Add(new
                    {
                        STT = listData.Count + 1,
                        Mã_cơ_sở = cs.MACOSO,
                        Tên_cơ_sở = cs.TENCOSO,
                        Tổng_số_biên_lai = countBienLai,
                        Tổng_số_ghi_danh = countGhiDanh,
                        Tổng_số_lớp_học_mở = countLopHoc,
                        Tổng_số_khoá_học = countKhoaHoc,
                        Tổng_doanh_thu = sumDoanhThu,
                        Tổng_phải_thu = sumPhaiThu,
                        Tổng_còn_thiếu = sumConThieu
                    });
            }
            if (soluong != -1 && trang != -1)
                return listData.Skip((trang - 1) * soluong).Take(soluong).ToList();
            return listData.ToList<object>();
        }

        public object TongDoanhThuTheoCoSo_Grid(string macoso, DateTime ngaybatdau, DateTime ngayketthuc, int trang, int soluong)
        {
            List<object> listData = new List<object>();
            foreach (KHOAHOC k in context.KHOAHOCs)
            {
                if (!k.LOPHOCs.Any(t => t.LICHHOCs.Any(s => s.MACOSO == macoso)))
                    continue;

                int countBienLai = context.BIENLAIs.Count(t => t.TT_DANGKYKHOAHOC.TT_GHIDANHs.Any(T => T.LOPHOC.LICHHOCs.Any(a => a.MACOSO == macoso))
                && t.TT_DANGKYKHOAHOC.MAKHOAHOC == k.MAKHOAHOC
                && t.NGAYXACNHAN >= ngaybatdau && t.NGAYXACNHAN <= ngayketthuc
                );

                int countGhiDanh = context.TT_GHIDANHs.Count(t => t.LOPHOC.LICHHOCs.Any(a => a.MACOSO == macoso)
                && t.LOPHOC.MAKHOAHOC == k.MAKHOAHOC
                && t.TT_DANGKYKHOAHOC.NGAY >= ngaybatdau && t.TT_DANGKYKHOAHOC.NGAY <= ngayketthuc);

                int countLopHoc = context.LOPHOCs.Count(t => t.LICHHOCs.Any(a => a.MACOSO == macoso)
                && t.MAKHOAHOC == k.MAKHOAHOC
                && t.LICHHOCs.First().NGAYHOC >= ngaybatdau && t.LICHHOCs.First().NGAYHOC <= ngayketthuc);

                double? sumDoanhThu = context.BIENLAIs.Where(t => t.TT_DANGKYKHOAHOC.TT_GHIDANHs.Any(T => T.LOPHOC.LICHHOCs.Any(a => a.MACOSO == macoso))
                && t.TT_DANGKYKHOAHOC.MAKHOAHOC == k.MAKHOAHOC
                && t.NGAYXACNHAN >= ngaybatdau && t.NGAYXACNHAN <= ngayketthuc
                ).Sum(s => (double?)s.SOTIEN);

                double? sumPhaiThu = context.LOPHOCs.Where(t => t.LICHHOCs.Any(a => a.MACOSO == macoso) && t.MAKHOAHOC == k.MAKHOAHOC).Sum(l => (double?)l.SOLUONG * (double?)l.KHOAHOC.HOCPHI);
                double? sumConThieu = sumPhaiThu - sumDoanhThu;

                if (soluong != -1 && trang != -1)
                    listData.Add(new
                    {
                        Mã_khoá_học = k.MAKHOAHOC,
                        Tên_khoá_học = k.TENKHOAHOC,
                        Tổng_số_lớp_học_mở = countLopHoc,
                        Tổng_số_ghi_danh = countGhiDanh,
                        Tổng_số_biên_lai = countBienLai,
                        Tổng_doanh_thu = sumDoanhThu,
                        Tổng_phải_thu = sumPhaiThu,
                        Tổng_còn_thiếu = sumConThieu
                    });
                else
                    listData.Add(new
                    {
                        STT = listData.Count + 1,
                        Mã_khoá_học = k.MAKHOAHOC,
                        Tên_khoá_học = k.TENKHOAHOC,
                        Tổng_số_lớp_học_mở = countLopHoc,
                        Tổng_số_ghi_danh = countGhiDanh,
                        Tổng_số_biên_lai = countBienLai,
                        Tổng_doanh_thu = sumDoanhThu,
                        Tổng_phải_thu = sumPhaiThu,
                        Tổng_còn_thiếu = sumConThieu
                    });
            }

            if (soluong != -1 && trang != -1)
                return listData.Skip((trang - 1) * soluong).Take(soluong).ToList();
            return listData.ToList<object>();
        }

        public object TongDoanhThuTheoKhoaHoc_Grid(string makhoahoc, DateTime ngaybatdau, DateTime ngayketthuc, int trang, int soluong)
        {
            List<object> listData = new List<object>();
            foreach (LOPHOC l in context.LOPHOCs)
            {
                if (l.LICHHOCs[0].NGAYHOC < ngaybatdau || l.LICHHOCs[0].NGAYHOC > ngayketthuc || l.MAKHOAHOC != makhoahoc)
                    continue;

                int countBienLai = context.BIENLAIs.Count(t => t.MALOPHOC == l.MALOPHOC
                && t.NGAYXACNHAN >= ngaybatdau && t.NGAYXACNHAN <= ngayketthuc
                );

                int? countGhiDanh = (int?)l.SOLUONG;

                double? sumDoanhThu = context.BIENLAIs.Where(t => t.MALOPHOC == l.MALOPHOC).Sum(s => (double?)s.SOTIEN);
                double? sumPhaiThu = (double?)l.SOLUONG * (double?)l.KHOAHOC.HOCPHI;
                double? sumConThieu = sumPhaiThu - sumDoanhThu;

                if (soluong != -1 && trang != -1)
                    listData.Add(new
                    {
                        Mã_lớp_học = l.MALOPHOC,
                        Tổng_số_ghi_danh = countGhiDanh,
                        Tổng_số_biên_lai = countBienLai,
                        Tổng_doanh_thu = sumDoanhThu,
                        Tổng_phải_thu = sumPhaiThu,
                        Tổng_còn_thiếu = sumConThieu
                    });
                else
                    listData.Add(new
                    {
                        STT = listData.Count + 1,
                        Mã_lớp_học = l.MALOPHOC,
                        Tổng_số_ghi_danh = countGhiDanh,
                        Tổng_số_biên_lai = countBienLai,
                        Tổng_doanh_thu = sumDoanhThu,
                        Tổng_phải_thu = sumPhaiThu,
                        Tổng_còn_thiếu = sumConThieu
                    });
            }
            if (soluong != -1 && trang != -1)
                return listData.Skip((trang - 1) * soluong).Take(soluong).ToList();
            return listData.ToList<object>();
        }


        public object TongDoanhThuTheoLopHoc_Grid(string malophoc, DateTime ngaybatdau, DateTime ngayketthuc, int trang, int soluong)
        {
            List<object> listData = new List<object>();
            foreach (TT_GHIDANH hv in context.TT_GHIDANHs)
            {
                if (hv.TT_DANGKYKHOAHOC.NGAY < ngaybatdau || hv.TT_DANGKYKHOAHOC.NGAY > ngayketthuc)
                    continue;

                int countBienLai = context.BIENLAIs.Count(t => t.MADANGKYKHOAHOC == hv.MADANGKYKHOAHOC
                && t.NGAYXACNHAN >= ngaybatdau && t.NGAYXACNHAN <= ngayketthuc
                );

                double? sumDoanhThu = context.BIENLAIs.Where(t => t.MADANGKYKHOAHOC == hv.MADANGKYKHOAHOC
                && t.NGAYXACNHAN >= ngaybatdau && t.NGAYXACNHAN <= ngayketthuc).Sum(s => (double?)s.SOTIEN);

                double? sumPhaiThu = (double?)hv.LOPHOC.KHOAHOC.HOCPHI;
                double? sumConThieu = sumPhaiThu - sumDoanhThu;

                if (soluong != -1 && trang != -1)
                    listData.Add(new
                    {
                        Mã_đăng_ký = hv.MADANGKYKHOAHOC,
                        Mã_học_viên = hv.TT_DANGKYKHOAHOC.MAHOCVIEN,
                        Tên_học_viên = hv.TT_DANGKYKHOAHOC.HOCVIEN.TENHOCVIEN,
                        Tổng_số_biên_lai = countBienLai,
                        Tổng_doanh_thu = sumDoanhThu,
                        Tổng_phải_thu = sumPhaiThu,
                        Tổng_còn_thiếu = sumConThieu
                    });
                else
                    listData.Add(new
                    {
                        STT = listData.Count + 1,
                        Mã_đăng_ký = hv.MADANGKYKHOAHOC,
                        Mã_học_viên = hv.TT_DANGKYKHOAHOC.MAHOCVIEN,
                        Tên_học_viên = hv.TT_DANGKYKHOAHOC.HOCVIEN.TENHOCVIEN,
                        Tổng_số_biên_lai = countBienLai,
                        Tổng_doanh_thu = sumDoanhThu,
                        Tổng_phải_thu = sumPhaiThu,
                        Tổng_còn_thiếu = sumConThieu
                    });
            }
            if (soluong != -1 && trang != -1)
                return listData.Skip((trang - 1) * soluong).Take(soluong).ToList();
            return listData.ToList<object>();
        }
        #endregion

    }
}
