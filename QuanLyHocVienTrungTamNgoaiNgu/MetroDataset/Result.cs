﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetroDataset
{
    public class ResultExcuteQuery
    {
        bool result;
        string message;

        public ResultExcuteQuery(bool isSuccess, string message)
        {
            this.result = isSuccess;
            this.message = message;
        }

        public bool isSuccessfully()
        {
            return result;
        }

        public bool isFailure()
        {
            return !result;
        }

        public string GetMessage()
        {
            return message;
        }
    }
}
