﻿using EventHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TripleDES;
using MetroDataset.Model;
using System.Data.Linq;
using System.Globalization;

namespace MetroDataset
{
    public class Insert
    {
        DataQuanLyHocVienDataContext context;
        public Insert()
        {
            string connectString = Helper.GetStringConnection();
            context = new DataQuanLyHocVienDataContext(connectString);
        }

        private bool Submit()
        {
            return EventHelper.Helper.TryCatch(context.SubmitChanges, () => { });
        }

        public bool ThemNguoiDungVaoNhomNguoiDung(string @manhom, string @manguoidung)
        {
            NGUOIDUNG_NHOMNGUOIDUNG n = new NGUOIDUNG_NHOMNGUOIDUNG()
            {
                MATAIKHOAN = @manguoidung,
                MANHOMNGUOIDUNG = @manhom,
                HOATDONG = true
            };
            context.NGUOIDUNG_NHOMNGUOIDUNGs.InsertOnSubmit(n);
            return Submit();
        }

        public bool ThemNhomNguoiDungGiaoDien(string @magiaodien, string @manhomnguoidung, string @mathaotac)
        {
            NHOMNGUOIDUNG_GIAODIEN n = new NHOMNGUOIDUNG_GIAODIEN()
            {
                MAGIAODIEN = magiaodien,
                MANHOMNGUOIDUNG = manhomnguoidung,
                MATHAOTAC = mathaotac
            };
            context.NHOMNGUOIDUNG_GIAODIENs.InsertOnSubmit(n);
            return Submit();
        }

        public bool ThemTTChonGioHoc(string madangkykhoahoc, string @macahoc, string @mathu)
        {
            TT_CHONGIOHOC n = new TT_CHONGIOHOC()
            {
                MADANGKYKHOAHOC = madangkykhoahoc,
                MACAHOC = @macahoc,
                MATHU = @mathu
            };
            context.TT_CHONGIOHOCs.InsertOnSubmit(n);
            return Submit();
        }

        public ResultExcuteQuery ThemPhieuDangKyKhoaHoc(string mahocvien, string makhoahoc, string manhanvien)
        {
            List<int> c = context.TT_DANGKYKHOAHOCs.Where(t => t.MAHOCVIEN == mahocvien && t.MAKHOAHOC == makhoahoc).Select(s=>int.Parse(s.MADANGKYKHOAHOC.Substring(10)))
                .ToList<int>();

            foreach(LICHSU_HUYBIENLAI ls in context.LICHSU_HUYBIENLAIs)
            {
                string _makhoahoc = ls.MADANGKYKHOAHOC.Substring(2, 4);
                string _mahocvien = ls.MADANGKYKHOAHOC.Substring(6, 4);
                if (makhoahoc.Substring(2) == _makhoahoc && mahocvien.Substring(2) == _mahocvien)
                    c.Add(int.Parse(ls.MADANGKYKHOAHOC.Substring(10)));
            }

            int lan = 1;
            if (c.Count() != 0)
                lan = c.Max() + 1;

            string ma = "DK" + makhoahoc.Substring(2) + mahocvien.Substring(2) + lan.ToString();

            TT_DANGKYKHOAHOC dk = new TT_DANGKYKHOAHOC()
            {
                MADANGKYKHOAHOC = ma,
                MAHOCVIEN = mahocvien,
                MAKHOAHOC = makhoahoc,
                MANHANVIEN = manhanvien,
                langhidanh = lan,
                NGAY = DateTime.Now
            };

            context.TT_DANGKYKHOAHOCs.InsertOnSubmit(dk);
            bool r = Submit();
            return new ResultExcuteQuery(r, "Thêm phiếu đăng ký khoá học " + (r ? "thành công" : "thất bại"));
        }

        public ResultExcuteQuery ThemHocVienLopHoc(List<string> madangky, string lophoc)
        {
            List<TT_GHIDANH> list = new List<TT_GHIDANH>();
            foreach(string ma in madangky)
            {
                TT_GHIDANH gd = new TT_GHIDANH()
                {
                    MADANGKYKHOAHOC = ma,
                    MALOPHOC = lophoc
                };
                list.Add(gd);
            }
            context.TT_GHIDANHs.InsertAllOnSubmit(list);
            bool r = Submit();
            return new ResultExcuteQuery(r, "Thêm " + list.Count.ToString() + " học viên vào lớp \'" + lophoc + "\' " + (r ? "thành công" : "thất bại"));
        }

        public ResultExcuteQuery ThemLichHocLopHoc(ThoiKhoaBieu tkb, PhongHoc phong, string malop)
        {
            List<NGAYHOC> ngayhoc = new List<NGAYHOC>();
            List<THOIGIANHOC> tgh = new List<THOIGIANHOC>();
            List<LICHHOC> list = new List<LICHHOC>();
            for(int i = 0; i < tkb.Data.Count;i++)
            {
                LichHoc l = tkb.Data[i];
                DateTime dt = new DateTime(l.Ngay.Year, l.Ngay.Month, l.Ngay.Day,0,0,0,0);
                ngayhoc.Add(new NGAYHOC()
                {
                    NGAYHOC1 =dt ,
                    NGHILE = false
                });

                tgh.Add(new THOIGIANHOC()
                {
                    NGAYHOC =dt,
                    MACA = "CA" + l.Ca
                });


                LICHHOC _l = new LICHHOC()
                {
                    MALICHHOC = "LICH" + ((i + 1) < 10 ? "0" : "") + (i + 1).ToString(),
                    MACA = "CA" + l.Ca,
                    NGAYHOC =  dt,
                    MACOSO = phong.Macoso,
                    MAPHONG = phong.Maphong,
                    MALOPHOC = malop,
                    TINHTRANG = "CHƯA KẾT THÚC"
                };

                list.Add(_l);
            }


            //filter
            ngayhoc = ngayhoc.Where(n => context.NGAYHOCs.All(a => a.NGAYHOC1 != n.NGAYHOC1)).ToList();
            context.NGAYHOCs.InsertAllOnSubmit(ngayhoc);
            bool t = Submit();

            tgh = tgh.Where(m => context.THOIGIANHOCs.All(a => a.NGAYHOC != m.NGAYHOC || a.MACA != m.MACA)).ToList();
            context.THOIGIANHOCs.InsertAllOnSubmit(tgh);
            bool s = Submit();

            context.LICHHOCs.InsertAllOnSubmit(list);
            bool r = Submit();
            return new ResultExcuteQuery(r, "Thêm lịch học cho lớp học \'" + malop + "\' " + (r ? "thành công" : "thất bại"));
        }

        public ResultExcuteQuery ThemLopHoc(ref LOPHOC lophoc)
        {
            //tạo mở tư động
            string makhoahoc = lophoc.MAKHOAHOC;
            int count = context.LOPHOCs.Count(t => t.MAKHOAHOC == makhoahoc);
            count++;
            string ma = "000" + count;
            ma = ma.Substring(ma.Length - 4);
            ma = "L" + lophoc.MAKHOAHOC.Substring(3) + ma;
            lophoc.MALOPHOC = ma;
            LOPHOC _lophoc = new LOPHOC()
            {
                MALOPHOC = ma,
                MAKHOAHOC = lophoc.MAKHOAHOC,
                MAGIANGVIEN = lophoc.MAGIANGVIEN,
                SOLUONG = lophoc.SOLUONG,
                TINHTRANG = "CHƯA KẾT THÚC"
            };
            context.LOPHOCs.InsertOnSubmit(_lophoc);
            bool r = Submit();
            return new ResultExcuteQuery(r, "Thêm lớp học \'" + ma + "\' " + (r ? "thành công" : "thất bại"));
        }
        public ResultExcuteQuery ThemKhoaHoc(KHOAHOC khoahoc)
        {
            KHOAHOC _khoahoc = new KHOAHOC()
            {
                MAKHOAHOC = khoahoc.MAKHOAHOC,
                TENKHOAHOC = khoahoc.TENKHOAHOC,
                HOCPHI = khoahoc.HOCPHI
            };
            context.KHOAHOCs.InsertOnSubmit(_khoahoc);
            bool r = Submit();
            return new ResultExcuteQuery(r, "Thêm khóa học \'" + _khoahoc.MAKHOAHOC + "\' " + (r ? "thành công" : "thất bại"));
        }
        public ResultExcuteQuery ThemDiemMoi(DIEM diem)
        {
            context.DIEMs.InsertOnSubmit(diem);
            bool r = Submit();
            return new ResultExcuteQuery(r, "Thêm điểm mới học viên " + (r ? "thành công" : "thất bại"));
        }

        public bool ThemDiemDanhHocVien(string madangkykhoahoc, string malich, string malophoc, bool comat)
        {
            CT_BUOIHOC buoihoc = new CT_BUOIHOC()
            {
                MADANGKYKHOAHOC = madangkykhoahoc,
                MALICHHOC = malich,
                MALOPHOC = malophoc,
                DIEMDANH = comat
            };
            context.CT_BUOIHOCs.InsertOnSubmit(buoihoc);
            return Submit();
        }
        //Thêm người dùng vào nhóm người dùng
        public bool ThemNguoiDungVaoNhomNguoiDung(NGUOIDUNG nd)
        {
            string manhom = "NND001";   //học viên
            if (nd.MATAIKHOAN.StartsWith("GV"))
                manhom = "NND002";
            else if (nd.MATAIKHOAN.StartsWith("NV"))
                manhom = "NND003";
            NGUOIDUNG_NHOMNGUOIDUNG nguoidungnhomnguoidung = new NGUOIDUNG_NHOMNGUOIDUNG()
            {
                HOATDONG = true,
                MATAIKHOAN = nd.MATAIKHOAN,
                MANHOMNGUOIDUNG = manhom
            };
            context.NGUOIDUNG_NHOMNGUOIDUNGs.InsertOnSubmit(nguoidungnhomnguoidung);
            return Submit();
        }
        //Thêm học viên
        public ResultExcuteQuery ThemHocVien(HOCVIEN hv)
        {
            string matkhau = TripleDES.TripleDES.Crypto.EncryptAccount(hv.MAHOCVIEN, "1234");
            NGUOIDUNG nguoidung = new NGUOIDUNG()
            {
                MATAIKHOAN = hv.MAHOCVIEN,
                MATKHAU = matkhau,
                HOATDONG = true
            };

            //thêm người dùng
            context.NGUOIDUNGs.InsertOnSubmit(nguoidung);

            //thêm giảng viên
            context.HOCVIENs.InsertOnSubmit(hv);

            //thêm vào nhóm giảng viên
            bool b = ThemNguoiDungVaoNhomNguoiDung(nguoidung);

            bool r = Submit() && b;
            return new ResultExcuteQuery(r, "Thêm người dùng " + hv.MAHOCVIEN + " " + (r ? "thành công" : "thất bại"));
        }


        //Thêm nhân viên
        public ResultExcuteQuery ThemNhanVien(NHANVIEN nv)
        {
            //string manhanvien = (new Select()).TaoMaNhanVienTuDong();
            string matkhau = TripleDES.TripleDES.Crypto.EncryptAccount(nv.MANHANVIEN, "1234");
            NGUOIDUNG nguoidung = new NGUOIDUNG()
            {
                MATAIKHOAN = nv.MANHANVIEN,
                MATKHAU = matkhau,
                HOATDONG = true
            };

            //thêm người dùng
            context.NGUOIDUNGs.InsertOnSubmit(nguoidung);

            //thêm nhân viên
            context.NHANVIENs.InsertOnSubmit(nv);

            //thêm vào nhóm nhân viên
            bool b = ThemNguoiDungVaoNhomNguoiDung(nguoidung);

            bool r = Submit() && b;
            return new ResultExcuteQuery(r, "Thêm người dùng " + nv.MANHANVIEN + " " + (r ? "thành công" : "thất bại"));
        }

        public ResultExcuteQuery ThemDiemDauVao(TT_DANGKYKHOAHOC dangkykhoahoc)
        {
            context.TT_DANGKYKHOAHOCs.InsertOnSubmit(dangkykhoahoc);
            bool r = Submit();
            return new ResultExcuteQuery(r, "Đăng ký khoá học " + (r ? "thành công" : "thất bại"));
        }

        //Thêm giảng viên
        public ResultExcuteQuery ThemGiangVien(GIANGVIEN gv)
        {
            string matkhau = TripleDES.TripleDES.Crypto.EncryptAccount(gv.MAGIANGVIEN, "1234");
            NGUOIDUNG nguoidung = new NGUOIDUNG()
            {
                MATAIKHOAN = gv.MAGIANGVIEN,
                MATKHAU = matkhau,
                HOATDONG = true
            };

            //thêm người dùng
            context.NGUOIDUNGs.InsertOnSubmit(nguoidung);

            //thêm giảng viên
            context.GIANGVIENs.InsertOnSubmit(gv);

            //thêm vào nhóm giảng viên
            bool b = ThemNguoiDungVaoNhomNguoiDung(nguoidung);

            bool r = Submit() && b;
            return new ResultExcuteQuery(r, "Thêm người dùng " + gv.MAGIANGVIEN + " " + (r ? "thành công" : "thất bại"));
        }
        //Thêm thông tin BACKUP
        public ResultExcuteQuery ThemTTBackup(TT_BACKUP backup)
        {
            TT_BACKUP tt_backup = new TT_BACKUP()
            {
                MABACKUP = backup.MABACKUP,
                MANHANVIEN = backup.MANHANVIEN,
                KIEUBACKUP = backup.KIEUBACKUP,
                TENTAPTIN = backup.TENTAPTIN,
                NGAYLUU = backup.NGAYLUU,
                DUONGDAN = backup.DUONGDAN
            };
            //ghi vào hệ thống
            context.TT_BACKUPs.InsertOnSubmit(tt_backup);

            bool r = Submit();
            return new ResultExcuteQuery(r, "Sao lưu dữ liệu " + (r ? "thành công" : "thất bại"));
        }

        public ResultExcuteQuery ThemNhomNguoiDung(string tennhom, String loainhom)
        {
            bool r = false;
            if (!context.NHOMNGUOIDUNGs.Select(t => t.TENNHOMNGUOIDUNG).Contains(tennhom))
            {
                int count = context.NHOMNGUOIDUNGs.Count(t=>t.MANHOMNGUOIDUNG.StartsWith(loainhom))+1;
                string id = "0000" + count.ToString();
                id = loainhom + id.Substring(id.Length - 5);
                NHOMNGUOIDUNG nnd = new NHOMNGUOIDUNG()
                {
                    MANHOMNGUOIDUNG = id,
                    TENNHOMNGUOIDUNG = tennhom
                };
                context.NHOMNGUOIDUNGs.InsertOnSubmit(nnd);
                r = Submit();
            }
            return new ResultExcuteQuery(r, "Thêm nhóm người dùng \'" + tennhom + "\' " + (r ? "thành công" : "thất bại"));
        }

        public bool ThemLichSuHuyBienLai(BIENLAI bl, string manhanvienhuy)
        {
            LICHSU_HUYBIENLAI ls = new LICHSU_HUYBIENLAI()
            {
                MABIENLAI = bl.MABIENLAI,
                MALOAIBIENLAI = bl.MALOAIBIENLAI,
                MADANGKYKHOAHOC = bl.MADANGKYKHOAHOC,
                MALOPHOC = bl.MALOPHOC,
                MANHANVIENLAP = bl.MANHANVIEN,
                MANHANVIENHUY = manhanvienhuy,
                SOTIEN = bl.SOTIEN,
                DOT = bl.DOT,
                NGAYXACNHAN =bl.NGAYXACNHAN,
                NGAYHUY = DateTime.Now
            };
            context.LICHSU_HUYBIENLAIs.InsertOnSubmit(ls);
            return Submit();
        }

        //Thêm thông tin Biên Lai
        public ResultExcuteQuery ThemTTBienLai(BIENLAI tt_bienlai)
        {
            context.BIENLAIs.InsertOnSubmit(tt_bienlai);
            bool r = Submit();
            return new ResultExcuteQuery(r, "Lập biên lai " + (r ? "thành công" : "thất bại"));
        }
    }
}
