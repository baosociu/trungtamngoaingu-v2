﻿using MetroDataset.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static MetroDataset.Helper;

namespace MetroDataset
{
    public class Select
    {
        public DataQuanLyHocVienDataContext context;
        public Select()
        {
            string connectString = Helper.GetStringConnection();
            context = new DataQuanLyHocVienDataContext(connectString);
        }

        public object[] HienThiThongTinLopHoc(string @malop)
        {
            LOPHOC lophoc = context.LOPHOCs.SingleOrDefault(t => t.MALOPHOC == @malop);
            KHOAHOC khoahoc = lophoc.KHOAHOC;
            GIANGVIEN giangvien = lophoc.GIANGVIEN;
            return new object[] { lophoc, khoahoc, giangvien };
        }

        public List<LICHHOC> LayLichDayGiangVien(string magiangvien)
        {
            return context.LICHHOCs.Where(t => t.LOPHOC.MAGIANGVIEN == magiangvien && t.TINHTRANG == "CHƯA KẾT THÚC").ToList<LICHHOC>();
        }

        public List<CAHOC> HienThiTatCaCaHoc()
        {
            return context.CAHOCs.ToList<CAHOC>();
        }

    

        public List<int[]> LayThongTinChonGioHocVien(string iddangkykhoahoc)
        {
            return (from tt in context.TT_CHONGIOHOCs
                    from t in context.THUs
                    where t.MATHU == tt.MATHU
                    && tt.MADANGKYKHOAHOC == iddangkykhoahoc
                    select new int[] { t.THU_INT, int.Parse(tt.MACAHOC.Substring(2)) }).ToList<int[]>();
        }

        public CAHOC LayCaHocByID(string mACA)
        {
            return context.CAHOCs.FirstOrDefault(t => t.MACA == mACA);
        }

        public object HienThiHocVienByLopHoc_Cbb(string malop)
        {
            var c = from l in context.LOPHOCs
                    from gd in context.TT_GHIDANHs
                    from hv in context.HOCVIENs
                    from dk in context.TT_DANGKYKHOAHOCs
                    where l.MALOPHOC == gd.MALOPHOC
                    && gd.MADANGKYKHOAHOC == dk.MADANGKYKHOAHOC
                    && dk.MAKHOAHOC == l.MAKHOAHOC
                    && hv.MAHOCVIEN == dk.MAHOCVIEN
                    && l.MALOPHOC == malop
                    select new { Key = hv.MAHOCVIEN, Value = hv.TENHOCVIEN };
            return c.ToList<object>();
        }

        public object HienThiPhongHocCuaCoSo_Cbb(string mACOSO)
        {
            var ph = from p in context.PHONGs
                     where p.MACOSO == mACOSO
                     select new
                     {
                         Key = p.MAPHONG,
                         Value = "Phòng " + p.MAPHONG
                     };
            return ph.ToList<object>();
                     
        }

        public List<NHOMNGUOIDUNG_GIAODIEN> LayDanhSachChiTietThaoTacQuyen(string mataikhoan)
        {
            List<NGUOIDUNG_NHOMNGUOIDUNG> ndnnd = context.NGUOIDUNG_NHOMNGUOIDUNGs.Where(t => t.MATAIKHOAN == mataikhoan && t.HOATDONG == true).ToList<NGUOIDUNG_NHOMNGUOIDUNG>();
            List<NHOMNGUOIDUNG_GIAODIEN> lsst = new List<NHOMNGUOIDUNG_GIAODIEN>();
            foreach(NGUOIDUNG_NHOMNGUOIDUNG n in ndnnd)
            {
                lsst.AddRange(context.NHOMNGUOIDUNG_GIAODIENs.Where(t => t.MANHOMNGUOIDUNG == n.MANHOMNGUOIDUNG));
            }

            return lsst;
        }

        public object HienThiTatCaCaHoc_Cbb()
        {
            var c = context.CAHOCs.Select(ch => 
                    new
                    {
                        Key = ch.MACA,
                        Value = "Ca " + ch.MACA.Replace("CA", "") +
                            " (" + 
                            ((ch.GIOBATDAU.Value.Hours.ToString().Length == 1 ? "0" : "") + 
                            ch.GIOBATDAU.Value.Hours.ToString()) + ":" + 
                            ((ch.GIOBATDAU.Value.Minutes.ToString().Length == 1 ? "0" : "") + 
                            ch.GIOBATDAU.Value.Minutes.ToString())+
                            " - "+
                            ((ch.GIOKETTHUC.Value.Hours.ToString().Length == 1 ? "0" : "") +
                            ch.GIOKETTHUC.Value.Hours.ToString()) + ":" +
                            ((ch.GIOKETTHUC.Value.Minutes.ToString().Length == 1 ? "0" : "") +
                            ch.GIOKETTHUC.Value.Minutes.ToString()) + 
                            ")"
                    });
            return c.ToList<object>();
        }


        public object HienThiLopHocByKhoaHoc_Cbb(string makhoa)
        {
            List<object> c = new List<object>();
            foreach (LOPHOC l in context.LOPHOCs)
                c.Add(new { Key = l.MALOPHOC, Value = "LỚP " + l.MALOPHOC });
            c.Add(new { Key = "CHUAXEPLOP", Value = "CHƯA XẾP LỚP" });
            return c;
        }

        public object HienThiHocVienChuaXepLop_Cbb(string makhoahoc)
        {
            List<object> c = new List<object>();
            foreach(TT_DANGKYKHOAHOC dk in context.TT_DANGKYKHOAHOCs.Where(t=>t.MAKHOAHOC == makhoahoc))
            {
                if (!context.TT_GHIDANHs.Any(t => t.MADANGKYKHOAHOC == dk.MADANGKYKHOAHOC))
                    c.Add(new { Key = dk.MADANGKYKHOAHOC, Value = dk.MAHOCVIEN});
            }
            return c;

        }

        public List<object> HienThiKhoaHocByCoSo_Cbb(string coso)
        {
            var k = from cs in context.COSOs
                    from l in context.LICHHOCs
                    from p in context.PHONGs
                    where cs.MACOSO == p.MACOSO
                    && p.MAPHONG == l.MAPHONG
                    group new { cs, l, p } by l.MALOPHOC
                     into grp
                    select new { grp.Key };
            List<string> malops = k.Select(t => t.Key.ToString()).ToList();

            var c = from kh in context.KHOAHOCs
                    from l in context.LOPHOCs
                    where kh.MAKHOAHOC == l.MAKHOAHOC
                    && malops.Contains(l.MALOPHOC)
                    group new { kh, l } by new { kh.MAKHOAHOC, kh.TENKHOAHOC }
                    into grp
                    select new { Key = grp.Key.MAKHOAHOC, Value = grp.Key.TENKHOAHOC };
            return c.ToList<object>();
        }

        public BIENLAI LayBienLaiKhongHuyByMaDangKy(string mADANGKYKHOAHOC)
        {
            return context.BIENLAIs.FirstOrDefault(t => t.MADANGKYKHOAHOC == mADANGKYKHOAHOC);
        }

        public double LaySoTienBienLai(string madangky)
        {
            TT_DANGKYKHOAHOC dk = context.TT_DANGKYKHOAHOCs.FirstOrDefault(t => t.MADANGKYKHOAHOC == madangky);
            if(dk != null)
            {
                double hocphi = dk.KHOAHOC.HOCPHI.GetValueOrDefault();
                List<BIENLAI> bienlais = context.BIENLAIs.Where(t => t.MADANGKYKHOAHOC == madangky).ToList<BIENLAI>();
                if (bienlais.Count != 0)
                    return hocphi - bienlais.Sum(t => t.SOTIEN);
                return hocphi;

            }
            return 0.0;
        }

        public List<object> HienThiCoSo_Cbb()
        {
            var c = from cs in context.COSOs
                    select new { Key = cs.MACOSO, Value = cs.TENCOSO };
            return c.ToList<object>();
        }

        public LOAI_BIENLAI LayLoaiBienLaiDangKyKhoaHoc(string madangky)
        {
            BIENLAI bl = context.BIENLAIs.FirstOrDefault(t => t.MADANGKYKHOAHOC == madangky);
            if (bl == null)
                return null;

            return bl.LOAI_BIENLAI;
        }

        public List<object[]> HienThiLichTheoTuanHocVien(string mahocvien, DateTime ngaybatdau, DateTime ngayketthuc)
        {
            var v = from lich in context.LICHHOCs
                    from lop in context.LOPHOCs
                    from k in context.KHOAHOCs
                    from c in context.CAHOCs
                    from cs in context.COSOs
                    from gd in context.TT_GHIDANHs
                    from dk in context.TT_DANGKYKHOAHOCs
                    where lop.MALOPHOC == lich.MALOPHOC
                        && lop.MAKHOAHOC == k.MAKHOAHOC
                        && c.MACA == lich.MACA
                        && cs.MACOSO == lich.MACOSO
                        && gd.MALOPHOC == lop.MALOPHOC
                        && gd.MADANGKYKHOAHOC == dk.MADANGKYKHOAHOC
                        && dk.MAKHOAHOC == k.MAKHOAHOC
                        && dk.MAHOCVIEN == mahocvien
                        && lich.NGAYHOC <= ngayketthuc
                        && lich.NGAYHOC >= ngaybatdau
                    select new
                    {
                        Lịch_học = lich.MALICHHOC,
                        Lớp = lich.MALOPHOC,
                        Mã_khoá = k.MAKHOAHOC,
                        Khoá_học = k.TENKHOAHOC,
                        Phòng = lich.MAPHONG,
                        Mã_cơ_sở = cs.MACOSO,
                        Cơ_sở = cs.TENCOSO,
                        Ngày_học = lich.NGAYHOC,
                        Ca_học = lich.MACA,
                        Bắt_đầu = c.GIOBATDAU,
                        Kết_thúc = c.GIOKETTHUC,
                        Tình_trạng = lich.TINHTRANG
                    };

            List<object[]> l = new List<object[]>();
            l.AddRange(v.Select(t => new object[] { t.Lịch_học, t.Lớp, t.Mã_khoá, t.Khoá_học, t.Phòng, t.Mã_cơ_sở, t.Cơ_sở, t.Ngày_học, t.Ca_học, t.Bắt_đầu, t.Kết_thúc, t.Tình_trạng }));

            return l;
        }

        public string TaoDotBienLaiByMaDangKy(string madangky)
        {
            var bienlais = context.BIENLAIs.Where(t => t.MADANGKYKHOAHOC == madangky);
            if (bienlais.Count() == 0)
                return "1";
            else
                return (bienlais.Count() + 1).ToString();
        }

        public LICHHOC LayLichByMaLopMaLich(string malop, string malich)
        {
            return context.LICHHOCs.FirstOrDefault(t => t.MALOPHOC == malop && t.MALICHHOC == malich);
        }

        public KHOAHOC LayKhoaHocByMaLop(string malop)
        {
            LOPHOC l = context.LOPHOCs.FirstOrDefault(t => t.MALOPHOC == malop);
            if (l != null)
                return context.KHOAHOCs.FirstOrDefault(t => t.MAKHOAHOC == l.MAKHOAHOC);
            return null;
        }

        public KHOAHOC LayKhoaHocByMaDangKy(string madangky)
        {
            LOPHOC l = LayLopHocDangKyKhoaHoc(madangky);
            if (l != null)
                return context.KHOAHOCs.FirstOrDefault(t => t.MAKHOAHOC == l.MAKHOAHOC);
            return null;
        }

        public BIENLAI LayBienLai(string mabienlai)
        {
            return context.BIENLAIs.FirstOrDefault(t => t.MABIENLAI == mabienlai);
        }

        public List<object[]> HienThiLichTheoTuanGiangVien(string magiangvien, DateTime ngaybatdau, DateTime ngayketthuc)
        {
            var v = from lich in context.LICHHOCs
                    from lop in context.LOPHOCs
                    from k in context.KHOAHOCs
                    from c in context.CAHOCs
                    from cs in context.COSOs
                    where lop.MALOPHOC == lich.MALOPHOC
                        && lop.MAKHOAHOC == k.MAKHOAHOC
                        && c.MACA == lich.MACA
                        && cs.MACOSO == lich.MACOSO
                        && lop.MAGIANGVIEN == magiangvien
                        && lich.NGAYHOC <= ngayketthuc
                        && lich.NGAYHOC >= ngaybatdau
                    select new
                    {
                        Lịch_học = lich.MALICHHOC,
                        Lớp = lich.MALOPHOC,
                        Mã_khoá = k.MAKHOAHOC,
                        Khoá_học = k.TENKHOAHOC,
                        Phòng = lich.MAPHONG,
                        Mã_cơ_sở = cs.MACOSO,
                        Cơ_sở = cs.TENCOSO,
                        Ngày_học = lich.NGAYHOC,
                        Ca_học = lich.MACA,
                        Bắt_đầu = c.GIOBATDAU,
                        Kết_thúc = c.GIOKETTHUC,
                        Tình_trạng = lich.TINHTRANG
                    };

            List<object[]> l = new List<object[]>();
            l.AddRange(v.Select(t => new object[] { t.Lịch_học, t.Lớp, t.Mã_khoá, t.Khoá_học, t.Phòng, t.Mã_cơ_sở, t.Cơ_sở, t.Ngày_học, t.Ca_học, t.Bắt_đầu, t.Kết_thúc, t.Tình_trạng }));

            return l;
        }

        public List<object> HienThiMailThongBaoMoLop(List<string> madangkykhoahoc, string magiangvien, string makhoahoc)
        {
            List<object> _hv = (from dk in context.TT_DANGKYKHOAHOCs
                                from hv in context.HOCVIENs
                                from k in context.KHOAHOCs
                                where dk.MAHOCVIEN == hv.MAHOCVIEN
                                && k.MAKHOAHOC == dk.MAKHOAHOC
                                && k.MAKHOAHOC == makhoahoc
                                && madangkykhoahoc.Contains(dk.MADANGKYKHOAHOC)
                                select new
                                {
                                    Mã_người_dùng = hv.MAHOCVIEN,
                                    Họ_người_nhận = Helper.GetHoTen(hv.TENHOCVIEN).Ho,
                                    Tên_người_nhận = Helper.GetHoTen(hv.TENHOCVIEN).Ten,
                                    Mail_người_nhận = hv.MAIL,
                                    Số_điện_thoại_người_nhận = hv.SODIENTHOAI,
                                    Địa_chỉ_người_nhận = hv.DIACHI,
                                    Mã_khoá_học = k.MAKHOAHOC,
                                    Tên_khoá_học = k.TENKHOAHOC

                                }).ToList<object>();

            List<object> _gv = (from k in context.KHOAHOCs
                                from gv in context.GIANGVIENs
                                where k.MAKHOAHOC == makhoahoc
                                && gv.MAGIANGVIEN == magiangvien
                                select new
                                {
                                    Mã_người_dùng = gv.MAGIANGVIEN,
                                    Họ_người_nhận = Helper.GetHoTen(gv.TENGIANGVIEN).Ho,
                                    Tên_người_nhận = Helper.GetHoTen(gv.TENGIANGVIEN).Ten,
                                    Mail_người_nhận = gv.MAIL,
                                    Số_điện_thoại_người_nhận = gv.SODIENTHOAI,
                                    Địa_chỉ_người_nhận = gv.DIACHI,
                                    Mã_khoá_học = k.MAKHOAHOC,
                                    Tên_khoá_học = k.TENKHOAHOC
                                }).ToList<object>();

            _gv.AddRange(_hv);
            return _gv;
        }

        public BIEUMAU LayBieuMau(string mabieumau)
        {
            return context.BIEUMAUs.FirstOrDefault(t => t.MABIEUMAU == mabieumau);
        }

        public object HienThiFilterDiem_Cbb()
        {
            List<object> a = new List<object>();
            a.Add(new { Key = "ALL - -1", Value = "Tất cả dữ liệu".ToUpper() });
            a.AddRange((from k in context.KHOAHOCs
                        select new { Key = k.MAKHOAHOC + " - 3", Value = "KHOÁ HỌC: " + k.TENKHOAHOC }).ToList<object>());
            a.AddRange((from k in context.LOAIDIEMs
                        select new { Key = k.MALOAIDIEM + " - 5", Value = "LOẠI ĐIỂM: " + k.TENLOAIDIEM }).ToList<object>());
            return a;
        }

        public object HienThiFilterDiemHocVien_Cbb()
        {
            List<object> a = new List<object>();
            a.Add(new { Key = "ALL - -1", Value = "Tất cả dữ liệu".ToUpper() });
            a.AddRange((from gd in context.TT_GHIDANHs
                        from dk in context.TT_DANGKYKHOAHOCs
                        from lop in context.LOPHOCs
                        from k in context.KHOAHOCs
                        where gd.MADANGKYKHOAHOC == dk.MADANGKYKHOAHOC
                            && dk.MAHOCVIEN == idUser
                            && dk.MAKHOAHOC == k.MAKHOAHOC
                            && gd.MALOPHOC == lop.MALOPHOC
                            && lop.MAKHOAHOC == k.MAKHOAHOC
                        select new { Key = lop.MALOPHOC + " - 2", Value = "LỚP: " + k.TENKHOAHOC + " - " + lop.MALOPHOC }).ToList<object>());
            a.AddRange((from k in context.LOAIDIEMs
                        select new { Key = k.MALOAIDIEM + " - 5", Value = "LOẠI ĐIỂM: " + k.TENLOAIDIEM }).ToList<object>());
            return a;
        }

        public LOPHOC GetClassByID(string malophoc)
        {
            return context.LOPHOCs.FirstOrDefault(t => t.MALOPHOC == malophoc);
        }

        public object HienThiFilterNhanVien_Cbb()
        {
            List<object> a = new List<object>();
            a.Add(new { Key = "ALL - -1", Value = "Tất cả dữ liệu".ToUpper() });
            a.Add(new { Key = "NAM - 3", Value = "Nhân viên: NAM".ToUpper() });
            a.Add(new { Key = "NỮ - 3", Value = "Nhân viên: NỮ".ToUpper() });
            return a;
        }

        public object HienThiFilterHocVien_Cbb()
        {
            List<object> a = new List<object>();
            a.Add(new { Key = "ALL - -1", Value = "Tất cả dữ liệu".ToUpper() });
            a.Add(new { Key = "NAM - 3", Value = "Nhân viên: NAM".ToUpper() });
            a.Add(new { Key = "NỮ - 3", Value = "Nhân viên: NỮ".ToUpper() });
            return a;
        }

        public object HienThiFilterGiangVien_Cbb()
        {
            List<object> a = new List<object>();
            a.Add(new { Key = "ALL - -1", Value = "Tất cả dữ liệu".ToUpper() });
            a.Add(new { Key = "NAM - 3", Value = "Nhân viên: NAM".ToUpper() });
            a.Add(new { Key = "NỮ - 3", Value = "Nhân viên: NỮ".ToUpper() });
            return a;
        }


        public object HienThiFilterBienLai_Cbb()
        {
            List<object> a = new List<object>();
            a.Add(new { Key = "ALL - -1", Value = "Tất cả dữ liệu".ToUpper() });
            a.AddRange((from k in context.LOAI_BIENLAIs
                        select new { Key = k.MALOAIBIENLAI + " - 1", Value = "LOẠI BIÊN LAI: " + k.TENLOAIBIENLAI }).ToList<object>());
            a.AddRange((from k in context.KHOAHOCs
                        select new { Key = k.MAKHOAHOC + " - 4", Value = "KHOÁ HỌC: " + k.TENKHOAHOC }).ToList<object>());
            return a;
        }

        public object HienThiFilterLichSuHuyBienLai_Cbb()
        {
            List<object> a = new List<object>();
            a.Add(new { Key = "ALL - -1", Value = "Tất cả dữ liệu".ToUpper() });
            a.AddRange((from k in context.LOAI_BIENLAIs
                        select new { Key = k.MALOAIBIENLAI + " - 1", Value = "LOẠI BIÊN LAI: " + k.TENLOAIBIENLAI }).ToList<object>());
            return a;
        }

        public object HienThiFilterLichHoc_Cbb()
        {
            List<object> a = new List<object>();
            a.Add(new { Key = "ALL - -1", Value = "Tất cả dữ liệu".ToUpper() });
            a.AddRange((from k in context.KHOAHOCs
                        select new { Key = k.MAKHOAHOC + " - 2", Value = "KHOÁ HỌC: " + k.TENKHOAHOC }).ToList<object>());
            a.AddRange((from k in context.COSOs
                        select new { Key = k.MACOSO + " - 5", Value = "CƠ SỞ: " + k.TENCOSO }).ToList<object>());
            a.AddRange((from k in context.CAHOCs
                        select new { Key = k.MACA + " - 8", Value = "CA HỌC: " + k.MACA.Replace("CA", "") }).ToList<object>());
            a.Add(new { Key = "ĐÃ_KẾT_THÚC - 11", Value = "TÌNH TRẠNG: ĐÃ KẾT THÚC".ToUpper() });
            a.Add(new { Key = "CHƯA_KẾT_THÚC - 11", Value = "TÌNH TRẠNG: CHƯA KẾT THÚC".ToUpper() });
            return a;
        }

        public COSO GetBranchByID(string macoso)
        {
            return context.COSOs.FirstOrDefault(t => t.MACOSO == macoso);
        }

        public object HienThiFilterLichDayGiangVien_Cbb()
        {
            List<object> a = new List<object>();
            a.Add(new { Key = "ALL - -1", Value = "Tất cả dữ liệu".ToUpper() });
            a.AddRange((from k in context.LOPHOCs
                        from khoa in context.KHOAHOCs
                        where k.MAGIANGVIEN == idUser
                            && khoa.MAKHOAHOC == k.MAKHOAHOC
                        select new { Key = k.MALOPHOC + " - 1", Value = "LỚP HỌC: " + khoa.TENKHOAHOC + " - " + k.MALOPHOC }).ToList<object>());
            a.Add(new { Key = "ĐÃ_KẾT_THÚC - 11", Value = "TÌNH TRẠNG: ĐÃ KẾT THÚC".ToUpper() });
            a.Add(new { Key = "CHƯA_KẾT_THÚC - 11", Value = "TÌNH TRẠNG: CHƯA KẾT THÚC".ToUpper() });
            return a;
        }

        public object HienThiFilterLichHocHocVien_Cbb()
        {
            List<object> a = new List<object>();
            a.Add(new { Key = "ALL - -1", Value = "Tất cả dữ liệu".ToUpper() });
            a.AddRange((from k in context.LOPHOCs
                        from khoa in context.KHOAHOCs
                        from gd in context.TT_GHIDANHs
                        from dk in context.TT_DANGKYKHOAHOCs
                        where k.MAKHOAHOC == dk.MAKHOAHOC
                            && k.MALOPHOC == gd.MALOPHOC
                            && dk.MADANGKYKHOAHOC == gd.MADANGKYKHOAHOC
                            && dk.MAHOCVIEN == idUser
                            && khoa.MAKHOAHOC == dk.MAKHOAHOC

                        select new { Key = k.MALOPHOC + " - 1", Value = "LỚP HỌC: " + khoa.TENKHOAHOC + " - " + k.MALOPHOC }).ToList<object>());
            a.Add(new { Key = "ĐÃ_KẾT_THÚC - 11", Value = "TÌNH TRẠNG: ĐÃ KẾT THÚC".ToUpper() });
            a.Add(new { Key = "CHƯA_KẾT_THÚC - 11", Value = "TÌNH TRẠNG: CHƯA KẾT THÚC".ToUpper() });
            return a;
        }

        public object HienThiFilterLopHoc_Cbb()
        {
            List<object> a = new List<object>();
            a.Add(new { Key = "ALL - -1", Value = "Tất cả dữ liệu".ToUpper() });
            a.AddRange((from k in context.KHOAHOCs
                        select new { Key = k.MAKHOAHOC + " - 1", Value = "KHOÁ HỌC: " + k.TENKHOAHOC }).ToList<object>());
            a.Add(new { Key = "ĐÃ_KẾT_THÚC - 11", Value = "TÌNH TRẠNG: ĐÃ KẾT THÚC".ToUpper() });
            a.Add(new { Key = "CHƯA_KẾT_THÚC - 11", Value = "TÌNH TRẠNG: CHƯA KẾT THÚC".ToUpper() });
            return a;
        }
        public object HienThiFilterKhoaHoc_Cbb()
        {
            List<object> a = new List<object>();
            a.Add(new { Key = "ALL - -1", Value = "Tất cả dữ liệu".ToUpper() });
            a.AddRange((from k in context.KHOAHOCs
                        select new { Key = k.MAKHOAHOC + " - 1", Value = "KHOÁ HỌC: " + k.TENKHOAHOC }).ToList<object>());
            return a;
        }

        public object HienThiFilterTaoKhoaHoc_Cbb()
        {
            List<object> a = new List<object>();
            a.Add(new { Key = "ALL - -1", Value = "Tất cả dữ liệu".ToUpper() });
            a.Add(new { Key = "1 - 3", Value = "Tồn tại lớp học".ToUpper() });
            a.Add(new { Key = "0 - 3", Value = "Không tồn tại lớp học".ToUpper() });
            return a;
        }

        public List<object> HienThiFilterPhanQuyen_Cbb()
        {
            List<object> d = new List<object>() {
                new { Key = "All - -1", Value = "Tất cả dữ liệu".ToUpper() },
                new { Key = "NV - 0", Value = "Đối tượng: Nhân viên".ToUpper() },
                new { Key = "GV - 0", Value = "Đối tượng: Giảng viên".ToUpper() },
                new{ Key = "HV - 0", Value = "Đối tượng: Học viên".ToUpper()},
                new {Key = "True - 4", Value ="Tình trạng: Hoạt động".ToUpper() },
                new { Key = "False - 4", Value = "Tình trạng: Không hoạt động".ToUpper()}
            };
            List<object> n = (from nnd in context.NHOMNGUOIDUNGs
                              select new { Key = nnd.MANHOMNGUOIDUNG + " - 2", Value = ("Nhóm: " + nnd.TENNHOMNGUOIDUNG).ToUpper() }).ToList<object>();
            d.AddRange(n);
            return d;
        }

        public List<object> HienThiFilterTaiKhoan_Cbb()
        {
            List<object> d = new List<object>() {
                new { Key = "All - -1", Value = "Tất cả dữ liệu".ToUpper() },
                new { Key = "NV - 0", Value = "Đối tượng: Nhân viên".ToUpper() },
                new { Key = "GV - 0", Value = "Đối tượng: Giảng viên".ToUpper() },
                new{ Key = "HV - 0", Value = "Đối tượng: Học viên".ToUpper()},
                new {Key = "True - 2", Value ="Tình trạng: Hoạt động".ToUpper() },
                new { Key = "False - 2", Value = "Tình trạng: Không hoạt động".ToUpper()}
            };
            return d;
        }

        public List<object> GetDetailSchedule(ThoiKhoaBieu data)
        {
            //List<LichHoc> _data = data.Data.Select(t => new LichHoc(t.Thu, t.Ca, t.Ngay)).ToList<LichHoc>();
            List<object> output = new List<object>();

            foreach (LichHoc l in data.Data)
            {
                CAHOC cahoc = context.CAHOCs.FirstOrDefault(t => t.MACA == "CA" + l.Ca.ToString());
                THU thu = context.THUs.FirstOrDefault(t => t.THU_INT-1 == l.Thu);

                string _thu = thu.TENTHU_VI;
                int _ca = l.Ca;
                string _batdau = cahoc.GIOBATDAU.Value.ToString().Substring(0, 5);
                string _ketthuc = cahoc.GIOKETTHUC.Value.ToString().Substring(0, 5);
                string _ngay = l.Ngay.ToShortDateString();

                output.Add(new
                {
                    Thứ = _thu,
                    Ca = _ca,
                    Bắt_đầu = _batdau,
                    Kết_thúc = _ketthuc,
                    Ngày = _ngay
                });
            }
            return output;
        }

        public List<PhongHoc> LayPhongHocChoLichHoc(List<LichHoc> lh)
        {
            var lichhoc = from g in context.LICHHOCs
                          where g.TINHTRANG == "CHƯA KẾT THÚC"
                          group g by new { g.MACA, g.MACOSO, g.MAPHONG, g.NGAYHOC } into grp
                          select new { grp.Key.MACA, grp.Key.MACOSO, grp.Key.MAPHONG, grp.Key.NGAYHOC };

            var phongs = from p in context.PHONGs
                         from c in context.COSOs
                         where p.MACOSO == c.MACOSO
                         select new { c.MACOSO, c.TENCOSO, p.MAPHONG, p.SOLUONGCHONGOI };

            foreach (var l in lh)
            {
                var f = lichhoc.FirstOrDefault(t => t.NGAYHOC == l.Ngay && "CA" + l.Ca.ToString() == t.MACA);

                if (f != null)
                    phongs = from p in phongs
                             where p.MAPHONG != f.MAPHONG
                             && p.MACOSO != f.MACOSO
                             select new { p.MACOSO, p.TENCOSO, p.MAPHONG, p.SOLUONGCHONGOI };
            }
            return phongs.Select(t => new PhongHoc(t.MAPHONG, t.MACOSO, t.TENCOSO, t.SOLUONGCHONGOI)).ToList();
        }

        public NHANVIEN GetStaffByID(string id)
        {
            var c = (from nv in context.NHANVIENs
                     where nv.MANHANVIEN == id
                     select nv).FirstOrDefault();
            return c;
        }

        public GIANGVIEN GetTeacherByID(string id)
        {
            var c = (from gv in context.GIANGVIENs
                     where gv.MAGIANGVIEN == id
                     select gv).FirstOrDefault();
            return c;
        }

        public HOCVIEN GetStudentByID(string @mahocvien)
        {
            var c = (from hv in context.HOCVIENs
                     where hv.MAHOCVIEN == mahocvien
                     select hv).FirstOrDefault();
            return c;
        }

        public HOCVIEN GetStudentByMaDangKy(string @madangky)
        {
            var c = (from hv in context.HOCVIENs
                     from dk in context.TT_DANGKYKHOAHOCs
                     where dk.MADANGKYKHOAHOC == @madangky
                     && hv.MAHOCVIEN == dk.MAHOCVIEN
                     select hv).FirstOrDefault();
            return c;
        }

        public NGUOIDUNG GetAccountByID(string @mataikhoan)
        {
            NGUOIDUNG a = context.NGUOIDUNGs.SingleOrDefault(t => t.MATAIKHOAN == mataikhoan);
            return a;
        }

        public KHOAHOC GetCourseByID(string @makhoa)
        {
            var c = context.KHOAHOCs.SingleOrDefault(t => t.MAKHOAHOC == makhoa);
            return c;
        }

        public object[] HienThiThongTinDiem(string @madiem, string madangky, string @malophoc)
        {
            object[] c = (from d in context.DIEMs
                          from tt in context.TT_GHIDANHs
                          from l in context.LOPHOCs
                          from k in context.KHOAHOCs
                          from ld in context.LOAIDIEMs
                          from h in context.HOCVIENs
                          where tt.MADANGKYKHOAHOC == d.MADANGKYKHOAHOC
                            && tt.MADANGKYKHOAHOC == madangky
                            && tt.MALOPHOC == l.MALOPHOC
                             && d.MALOPHOC == l.MALOPHOC
                             && k.MAKHOAHOC == l.MAKHOAHOC
                             && d.MALOAIDIEM == ld.MALOAIDIEM
                             && d.MADIEM == madiem
                             && l.MALOPHOC == malophoc
                          select new object[] { d, h, l, k, ld }).FirstOrDefault();

            return c;
        }

        public bool TonTaiLopChuaXep(string mahocvien, string makhoahoc)
        {
            var c = from dk in context.TT_DANGKYKHOAHOCs
                    where dk.MAHOCVIEN == mahocvien
                    && dk.MAKHOAHOC == makhoahoc
                    select dk.MADANGKYKHOAHOC;

            var g = from gd in context.TT_GHIDANHs
                    from dk in context.TT_DANGKYKHOAHOCs
                    where dk.MAHOCVIEN == mahocvien
                    && dk.MAKHOAHOC == makhoahoc
                    && dk.MADANGKYKHOAHOC == gd.MADANGKYKHOAHOC
                    select dk.MADANGKYKHOAHOC;
            return g.Count() != c.Count();
        }

        public List<object> HienThiHocVienDangKyKhoaHoc(string makhoa)
        {
            var dacolop = from gd in context.TT_GHIDANHs
                          select gd.MADANGKYKHOAHOC;

            //lấy ra các phần tử đã đăng ký khoá học nhưng chưa được xếp lớp
            var c = from dk in context.TT_DANGKYKHOAHOCs
                    from hv in context.HOCVIENs
                    where dk.MAHOCVIEN == hv.MAHOCVIEN
                    && dk.MAKHOAHOC == makhoa
                    && !dacolop.Contains(dk.MADANGKYKHOAHOC)
                    select new
                    {
                        Mã_phiếu_đăng_ký = dk.MADANGKYKHOAHOC,
                        Mã_học_viên = hv.MAHOCVIEN,
                        Tên_học_viên = hv.TENHOCVIEN,
                        Điểm_đầu_vào = dk.DIEMDAUVAO,
                        Ngày_sinh = hv.NGAYSINH,
                        Số_điện_thoại = hv.SODIENTHOAI
                    };
            return c.ToList<object>();
        }

        public string GetDayOfWeek(DateTime dateTime)
        {
            int i = (int)dateTime.DayOfWeek;
            THU thu = context.THUs.FirstOrDefault(t => t.THU_INT == i);
            if (thu != null)
                return thu.TENTHU_VI;
            return "";
        }

        public ResultExcuteQuery Login(string user, string pass)
        {
            NGUOIDUNG n = context.NGUOIDUNGs.FirstOrDefault(t => t.MATAIKHOAN.Equals(user) && t.MATKHAU.Equals(pass));
            if (n == null)
                return new ResultExcuteQuery(false, "Tên tài khoản hoặc mật khẩu tài khoản không chính xác");
            else
            {
                if (n.HOATDONG != true)
                    return new ResultExcuteQuery(false, "Tài khoản này tạm thời bị khoá");
                else
                {
                    String id = n.MATAIKHOAN.Substring(0, 2);
                    if(id == "GV")
                    {
                        GIANGVIEN gv = GetTeacherByID(n.MATAIKHOAN);
                        if(gv != null)
                            return new ResultExcuteQuery(true, "Hệ thống trung tâm Anh Ngữ xin chào '" + gv.TENGIANGVIEN + "\' !");

                    }
                    else if(id == "NV")
                    {
                        NHANVIEN nv = GetStaffByID(n.MATAIKHOAN);
                        if (nv != null)
                            return new ResultExcuteQuery(true, "Hệ thống trung tâm Anh Ngữ xin chào '" + nv.TENNHANVIEN + "\' !");
                    }
                    else
                    {
                        HOCVIEN hv = GetStudentByID(n.MATAIKHOAN);
                        if (hv != null)
                            return new ResultExcuteQuery(true, "Hệ thống trung tâm Anh Ngữ xin chào '" + hv.TENHOCVIEN + "\' !");
                    }

                    return new ResultExcuteQuery(true, "Hệ thống trung tâm Anh Ngữ xin chào '" + n.MATAIKHOAN + "\' !");
                }
            }
        }
        public bool KiemTraMaDangKyMonHoc(string @madangky)
        {
            TT_DANGKYKHOAHOC dk = context.TT_DANGKYKHOAHOCs.FirstOrDefault(t => t.MADANGKYKHOAHOC.Equals(madangky));
            if (dk == null)
            {
                EventHelper.Helper.MessageInfomation("Chưa có biên lai nào cho đăng ký này trước đó!");
                return false;
            }
            else
            {
                EventHelper.Helper.MessageInfomation("Đã có biên lai đóng tiền cho đăng ký này! Tiến hành đóng đợt tiếp theo!");
                return true;
            }
        }


        //mã tự động
        #region Mã tự động

        public string TaoMaDiemTuDong(string @malophoc, string @madangkykhoahoc)
        {
            string madiem = "0000";
            var _d = from hv in context.HOCVIENs
                     from dk in context.TT_DANGKYKHOAHOCs
                     from gd in context.TT_GHIDANHs
                     where hv.MAHOCVIEN == dk.MAHOCVIEN
                        && dk.MADANGKYKHOAHOC == gd.MADANGKYKHOAHOC
                        && gd.MALOPHOC == malophoc
                        && gd.MADANGKYKHOAHOC == madangkykhoahoc
                     select dk.MADANGKYKHOAHOC;

            int count = context.DIEMs.Count(t => _d.Contains(t.MADANGKYKHOAHOC)) + 1;
            madiem = ReverseString(madiem + count.ToString());
            madiem = "D" + ReverseString(madiem.Substring(0, 5));
            return madiem;
        }

        public string TaoMaLanDiemTuDong(string madiem, string madnagky, string malophoc, string maloaidiem)
        {
            var g = (from d in context.DIEMs
                     where d.MADANGKYKHOAHOC == madnagky
                        && d.MALOPHOC == malophoc
                        && d.MALOAIDIEM == maloaidiem
                     select d);
            int count = g.Where(t => t.MADIEM == madiem).Count();
            if (count == 0)
            {
                int max = 0;
                if (g.Count() != 0)
                    max = g.Max(t => t.LAN);
                count = max + 1;
            }
            return count.ToString();
        }

        public List<object> TaoMaLopTuKhoaHoc(string @makhoahoc)
        {
            int count = context.LOPHOCs.Count(t => t.MAKHOAHOC == makhoahoc) + 1;
            string malophoc = "0000" + count.ToString();
            malophoc = (Helper.ReverseString(malophoc)).Substring(0, 4);
            malophoc = "L" + makhoahoc.Substring(3) + ReverseString(malophoc);

            List<object> a = new List<object>();
            a.Add(new { Key = malophoc, Value = malophoc });
            return a;
        }

        //Tạo mã học viên tự động
        public string TaoMaHocVienTuDong()
        {
            string mahocvien = "HV0001";
            HOCVIEN gv = context.HOCVIENs.OrderByDescending(t => t.MAHOCVIEN).FirstOrDefault();
            if (gv != null)
            {
                mahocvien = "000" + (int.Parse(gv.MAHOCVIEN.Substring(2)) + 1).ToString();
                mahocvien = "HV" + mahocvien.Substring(mahocvien.Length - 4);
            }
            return mahocvien;
        }

        public TT_DANGKYKHOAHOC LayTTDangKhoaHoc(string madangkykhoahoc)
        {
            TT_DANGKYKHOAHOC c = context.TT_DANGKYKHOAHOCs.FirstOrDefault(
                t => t.MADANGKYKHOAHOC == madangkykhoahoc);
            return c;
        }



        //Tạo mã nhân viên tự động
        public string TaoMaNhanVienTuDong()
        {
            string manhanvien = "NV0001";
            NHANVIEN nv = context.NHANVIENs.OrderByDescending(t => t.MANHANVIEN).FirstOrDefault();
            if (nv != null)
            {
                manhanvien = "000" + (int.Parse(nv.MANHANVIEN.Substring(2)) + 1).ToString();
                manhanvien = "NV" + manhanvien.Substring(manhanvien.Length - 4);
            }
            return manhanvien;
        }

        //Tạo mã giảng viên tự động
        public string TaoMaGiangVienTuDong()
        {
            string magiangvien = "GV0001";
            GIANGVIEN gv = context.GIANGVIENs.OrderByDescending(t => t.MAGIANGVIEN).FirstOrDefault();
            if (gv != null)
            {
                magiangvien = "000" + (int.Parse(gv.MAGIANGVIEN.Substring(2)) + 1).ToString();
                magiangvien = "GV" + magiangvien.Substring(magiangvien.Length - 4);
            }
            return magiangvien;
        }
        // Tạo mã backup tự động
        public string TaoMaTTBackUpTuDong(string manhanvien)
        {
            string mabackup = DateTime.Now.ToString("ddMMyyyyhhmmss");
            mabackup += manhanvien;
            return mabackup;
        }
        //Tạo mã biên lai tự động
        public string TaoMaBienLaiTuDong()
        {
            string mabienlai = "BL0000000001"; //BL  + Ngay+Thang+Nam+
            BIENLAI bl = context.BIENLAIs.OrderByDescending(t => t.MABIENLAI).FirstOrDefault();
            if (bl != null)
            {
                mabienlai = "000000000" + (int.Parse(bl.MABIENLAI.Substring(2)) + 1).ToString();
                mabienlai = "BL" + mabienlai.Substring(mabienlai.Length - 10);
            }
            return mabienlai;
        }
        public LOPHOC LayLopHocDangKyKhoaHoc(string madangky)
        {
            LOPHOC l = (from dk in context.TT_DANGKYKHOAHOCs
                        from gd in context.TT_GHIDANHs
                        from lh in context.LOPHOCs
                        where dk.MADANGKYKHOAHOC == gd.MADANGKYKHOAHOC
                        && gd.MALOPHOC == lh.MALOPHOC
                         && dk.MADANGKYKHOAHOC == madangky
                        select lh).FirstOrDefault();
            return l;
        }
        //Đợt đóng tiền tự động
        public BIENLAI TimTTBienLaiTuDong(string @madangky)
        {
            BIENLAI bl = (from bienlai in context.BIENLAIs
                          where bienlai.MADANGKYKHOAHOC == madangky
                          orderby bienlai.DOT descending
                          select bienlai).FirstOrDefault();
            return bl;
        }
        //Tạo mã khóa học tự động
        public string TaoMaKhoaHocTuDong()
        {
            string makhoahoc = "KH0001";
            KHOAHOC kh = context.KHOAHOCs.OrderByDescending(t => t.MAKHOAHOC).FirstOrDefault();
            if (kh != null)
            {
                makhoahoc = "000" + (int.Parse(kh.MAKHOAHOC.Substring(2)) + 1).ToString();
                makhoahoc = "KH" + makhoahoc.Substring(makhoahoc.Length - 4);
            }
            return makhoahoc;
        }

        #endregion



        //list
        public List<object> HienThiMailNguoiDung(string manguoidung)
        {
            string type = manguoidung.Substring(0, 2);
            if (type.Equals("HV"))
                return (from hv in context.HOCVIENs
                        where hv.MAHOCVIEN == manguoidung
                        select new
                        {
                            Mã_tài_khoản = hv.MATAIKHOAN,
                            Người_nhận = hv.TENHOCVIEN,
                            Địa_chỉ_mail = hv.MAIL,
                            Số_điện_thoại = hv.SODIENTHOAI
                        }).ToList<object>();
            else if (type.Equals("GV"))
                return (from hv in context.GIANGVIENs
                        where hv.MAGIANGVIEN == manguoidung
                        select new
                        {
                            Mã_tài_khoản = hv.MATAIKHOAN,
                            Người_nhận = hv.TENGIANGVIEN,
                            Địa_chỉ_mail = hv.MAIL,
                            Số_điện_thoại = hv.SODIENTHOAI
                        }).ToList<object>();
            else if (type.Equals("NV"))
                return (from hv in context.NHANVIENs
                        where hv.MANHANVIEN == manguoidung
                        select new
                        {
                            Mã_tài_khoản = hv.MATAIKHOAN,
                            Người_nhận = hv.TENNHANVIEN,
                            Địa_chỉ_mail = hv.MAIL,
                            Số_điện_thoại = hv.SODIENTHOAI
                        }).ToList<object>();
            return null;
        }

        public List<string> HienThiGiaoDienTheoTaiKhoan(List<NHOMNGUOIDUNG_GIAODIEN> nnd_gd)
        {
            List<string> litsString = new List<string>();
            foreach(NHOMNGUOIDUNG_GIAODIEN n in nnd_gd) { 
                var q = from data in context.NHOMNGUOIDUNG_GIAODIENs
                        where data.MANHOMNGUOIDUNG == n.MANHOMNGUOIDUNG

                        select new { data.MAGIAODIEN };
                litsString.AddRange(q.Select(s => s.MAGIAODIEN).ToList<string>());
                litsString = litsString.Distinct().ToList();
            }
            return litsString.OrderBy(t=>t).ToList();
        }

        public List<object> HienThiLichHocHocVien(string @taikhoan)
        {
            var c = (from data in context.TT_GHIDANHs
                     from dk in context.TT_DANGKYKHOAHOCs
                     where dk.MAHOCVIEN == taikhoan
                     && dk.MADANGKYKHOAHOC == data.MADANGKYKHOAHOC
                     select new { data.MALOPHOC }).Select(s => s.MALOPHOC).ToList<string>();

            var d = (from LH in context.LICHHOCs
                     from CH in context.CAHOCs
                     from KH in context.KHOAHOCs
                     from Lop in context.LOPHOCs
                     where LH.MACA == CH.MACA
                       && KH.MAKHOAHOC == Lop.MAKHOAHOC
                       && Lop.MALOPHOC == LH.MALOPHOC
                       && c.Contains(Lop.MALOPHOC)
                     select new
                     {
                         LH.MALICHHOC,
                         Lop.MALOPHOC,
                         KH.TENKHOAHOC,
                         CH.MACA,
                         CH.GIOBATDAU,
                         CH.GIOKETTHUC,
                         LH.NGAYHOC,
                         LH.MACOSO,
                         LH.MAPHONG
                     }
                   );
            return d.ToList<object>();
        }

        public List<object> HienThiLichHocLopHoc(string @malop)
        {
            var c = (from lh in context.LICHHOCs
                     from ch in context.CAHOCs
                     from cs in context.COSOs
                     where cs.MACOSO == lh.MACOSO
                        && ch.MACA == lh.MACA
                        && lh.MALOPHOC == malop
                     select new
                     {
                         Mã_lịch = "LỊCH " + lh.MALICHHOC.Substring(4),
                         Ngày_học = lh.NGAYHOC,
                         Bắt_đầu = ch.GIOBATDAU,
                         Kết_thúc = ch.GIOKETTHUC,
                         Phòng = lh.MAPHONG,
                         Cơ_sở = cs.TENCOSO,
                         Tình_trạng = lh.TINHTRANG
                     });
            return c.ToList<object>();
        }

        public List<object> HienThiLichHocLopHoc_Template(string @malop)
        {
            var c = (from lh in context.LICHHOCs
                     from ch in context.CAHOCs
                     from cs in context.COSOs
                     from t in context.THUs
                     where cs.MACOSO == lh.MACOSO
                        && ch.MACA == lh.MACA
                        && lh.MALOPHOC == malop
                        && t.THU_INT == (int)lh.NGAYHOC.DayOfWeek
                     select new
                     {
                         stt = int.Parse(lh.MALICHHOC.Substring(4)),
                         thu = t.TENTHU_VI,
                         giobatdau = ch.GIOBATDAU,
                         gioketthuc = ch.GIOKETTHUC,
                         cahoc = "CA" + ch.MACA.Substring(2),
                         phong = lh.MAPHONG,
                         coso = cs.TENCOSO,
                         ngayhoc = lh.NGAYHOC.ToShortDateString()
                     });
            return c.ToList<object>();
        }
        public List<object> LayThongTinTatCaDiemCuaHocVienTheoLop_Template(string mahocvien,string malop)
        {
            var q = (from d in context.DIEMs
                     from h in context.HOCVIENs
                     from l in context.LOPHOCs
                     from ld in context.LOAIDIEMs
                     from k in context.KHOAHOCs
                     from dk in context.TT_DANGKYKHOAHOCs
                     where d.MADANGKYKHOAHOC == dk.MADANGKYKHOAHOC
                         && dk.MAHOCVIEN == h.MAHOCVIEN
                         && dk.MAKHOAHOC == k.MAKHOAHOC
                         && d.MALOPHOC == l.MALOPHOC
                         && d.MALOAIDIEM == ld.MALOAIDIEM
                         && k.MAKHOAHOC == l.MAKHOAHOC
                         && h.MAHOCVIEN == mahocvien
                         && d.MALOPHOC == malop
                     group h.MAHOCVIEN by h.MAHOCVIEN
                         into g
                     select g.Key);

            List<object> r = new List<object>();
            r.Add(new
            {
                stt = (r.Count + 1).ToString(),
                madangkykhoahoc = ((DIEM)q).MADANGKYKHOAHOC,
                tenkhoahoc =((KHOAHOC)q).TENKHOAHOC,
                malophoc = ((DIEM)q).MALOPHOC,
                madiem = ((DIEM)q).MADIEM,
                tenloaidiem = ((LOAIDIEM)q).TENLOAIDIEM,
                lan = ((DIEM)q).LAN,
                diem = ((DIEM)q).DIEM1,
                mahocvien = ((HOCVIEN)q).MAHOCVIEN,
                tenhocvien = ((HOCVIEN)q).TENHOCVIEN

            });
            return r;
        }
        public List<object> LayThongTinTatCaDiemCuaHocVien_Template(string mahocvien)
        {
            List<DIEM> q = (from d in context.DIEMs
                     from h in context.HOCVIENs
                     from l in context.LOPHOCs
                     from ld in context.LOAIDIEMs
                     from k in context.KHOAHOCs
                     from dk in context.TT_DANGKYKHOAHOCs
                     where d.MADANGKYKHOAHOC == dk.MADANGKYKHOAHOC
                         && dk.MAHOCVIEN == h.MAHOCVIEN
                         && dk.MAKHOAHOC == k.MAKHOAHOC
                         && d.MALOPHOC == l.MALOPHOC
                         && d.MALOAIDIEM == ld.MALOAIDIEM
                         && k.MAKHOAHOC == l.MAKHOAHOC
                         && h.MAHOCVIEN == mahocvien
                     select d ).ToList<DIEM>();

            List<object> r = new List<object>();

            q.ForEach(t => r.Add(new  {
                stt = (r.Count + 1).ToString(),
                madangkykhoahoc = t.MADANGKYKHOAHOC,
                tenkhoahoc = t.TT_GHIDANH.LOPHOC.KHOAHOC.TENKHOAHOC,
                malophoc = t.TT_GHIDANH.MALOPHOC,
                madiem = t.MADIEM,
                tenloaidiem = t.LOAIDIEM.TENLOAIDIEM,
                lan = t.LAN,
                diem = t.DIEM1,
                mahocvien = t.TT_GHIDANH.TT_DANGKYKHOAHOC.MAHOCVIEN,
                tenhocvien = t.TT_GHIDANH.TT_DANGKYKHOAHOC.HOCVIEN.TENHOCVIEN
            }));

            return r;
        }




        public NHANVIEN GetStaffByID(object iduser)
        {
            throw new NotImplementedException();
        }

        public List<object> HienThiHocVienLopHoc(string @malop)
        {
            var a = (from gd in context.TT_GHIDANHs
                     from dk in context.TT_DANGKYKHOAHOCs
                     where gd.MALOPHOC == malop
                     && dk.MADANGKYKHOAHOC == gd.MADANGKYKHOAHOC
                     group dk.MAHOCVIEN by dk.MAHOCVIEN
                     into g
                     select g.Key);

            var c = (from hv in context.HOCVIENs
                     where a.Contains(hv.MAHOCVIEN)
                     select new
                     {
                         Mã_học_viên = hv.MAHOCVIEN,
                         Tên_học_viên = hv.TENHOCVIEN,
                         Giới_tính = hv.GIOITINH,
                         Ngày_sinh = hv.NGAYSINH,
                         Số_điện_thoại = hv.SODIENTHOAI,
                         Mail = hv.MAIL
                     });
            return c.ToList<object>();
        }

        public List<object> HienThiHocVienLopHoc_Template(string @malop)
        {
            var a = (from gd in context.TT_GHIDANHs
                     from dk in context.TT_DANGKYKHOAHOCs
                     where gd.MALOPHOC == malop
                     && dk.MADANGKYKHOAHOC == gd.MADANGKYKHOAHOC
                     group dk.MAHOCVIEN by dk.MAHOCVIEN
                     into g
                     select g.Key);

            List<object> r = new List<object>();
            foreach (HOCVIEN hv in context.HOCVIENs)
            {
                if (a.Contains(hv.MAHOCVIEN))
                    r.Add(new
                    {
                        STT = (r.Count + 1).ToString(),
                        mahocvien = hv.MAHOCVIEN,
                        tenhocvien = hv.TENHOCVIEN,
                        ngaysinh = hv.NGAYSINH,
                        cmnd = hv.CMND,
                        sdt = hv.SODIENTHOAI,
                        mail = hv.MAIL,
                        diachi = hv.DIACHI
                    });
            }
            return r;
        }

        public List<object> HienThiLopHocKhoaHoc(string @makhoa)
        {
            var c = (from lh in context.LOPHOCs
                     from kh in context.KHOAHOCs
                     where lh.MAKHOAHOC == kh.MAKHOAHOC
                        && lh.MAKHOAHOC == @makhoa
                     select new
                     {
                         Mã_lớp_học = lh.MALOPHOC,
                         Mã_khoá_học = kh.MAKHOAHOC,
                         Tên_khoá_học = kh.TENKHOAHOC,
                         Mã_giảng_viên = lh.MAGIANGVIEN,
                         Số_lượng = lh.SOLUONG.ToString(),
                         Học_phí = kh.HOCPHI.ToString()
                     });
            return c.ToList<object>();
        }
        public List<object> HienThiTatCaTTBackUp(int @trang, int @soluong)
        {
            var b = (from bu in context.TT_BACKUPs
                     from nv in context.NHANVIENs
                     where nv.MANHANVIEN == bu.MANHANVIEN
                     select new
                     {
                         Mã_Backup = bu.MABACKUP,
                         Tên_tập_tin = bu.TENTAPTIN,
                         Nhân_Viên_Lưu = nv.TENNHANVIEN,
                         Kiểu_Backup = bu.KIEUBACKUP,
                         Ngày_lưu = bu.NGAYLUU,
                         Đường_dẫn = bu.DUONGDAN
                     }).Skip((trang - 1) * soluong).Take(soluong);
            return b.ToList<object>();
        }
        public List<object> HienThiDanhSachBienLai(int @trang, int @soluong)
        {
            var bl = (from bienlai in context.BIENLAIs
                      select new
                      {
                          Mã_biên_lai = bienlai.MABIENLAI,
                          Mã_loại_biên_lai = bienlai.MALOAIBIENLAI,
                          Mã_đăng_ký_khóa_học = bienlai.MADANGKYKHOAHOC,
                          Mã_lớp_học = bienlai.MALOPHOC,
                          Ngày_xác_nhận = bienlai.NGAYXACNHAN,
                          Số_tiền = bienlai.SOTIEN,
                          Đợt = bienlai.DOT
                      }).Skip((trang - 1) * soluong).Take(soluong);
            return bl.ToList<object>();
        }

        public object[] HienThiChiTietBienLai(string @mabienlai)
        {
            object[] d = (from bienlai in context.BIENLAIs
                          from lophoc in context.LOPHOCs
                          from giangvien in context.GIANGVIENs
                          from dangky in context.TT_DANGKYKHOAHOCs
                          from loaibienlai in context.LOAI_BIENLAIs
                          from khoahoc in context.KHOAHOCs
                          from hocvien in context.HOCVIENs
                          from nhanvien in context.NHANVIENs
                          where bienlai.MADANGKYKHOAHOC == dangky.MADANGKYKHOAHOC
                             && bienlai.MALOPHOC == lophoc.MALOPHOC
                             && bienlai.MALOAIBIENLAI == loaibienlai.MALOAIBIENLAI
                             && lophoc.MAKHOAHOC == khoahoc.MAKHOAHOC
                             && dangky.MAHOCVIEN == hocvien.MAHOCVIEN
                             && dangky.MANHANVIEN == nhanvien.MANHANVIEN
                             && giangvien.MAGIANGVIEN == lophoc.MAGIANGVIEN
                             && bienlai.MABIENLAI == mabienlai
                          select new object[] { bienlai, hocvien, dangky, lophoc, khoahoc, loaibienlai, nhanvien, giangvien }).FirstOrDefault();
            return d;
        }



        //cbb
        #region Combobox
        public List<object> HienThiLopHocGiangVien_Cbb(string @magiangvien)
        {
            var d = (from l in context.LOPHOCs
                     from k in context.KHOAHOCs
                     where l.MAKHOAHOC == k.MAKHOAHOC
                        && l.MAGIANGVIEN == magiangvien
                     select new { Key = l.MALOPHOC, Value = l.MALOPHOC + " - " + k.TENKHOAHOC }).ToList<object>();

            return d;
        }
        public List<object> HienThiLichHocLopHoc_Cbb(string @malophoc)
        {
            var d = (from l in context.LICHHOCs
                     where l.MALOPHOC == malophoc
                     select new { Key = l.MALICHHOC, Value = l.MALICHHOC.Substring(4, 2) + " - " + l.NGAYHOC });
            return d.ToList<object>();
        }
        public List<object> HienThiLopHocHocVien_Cbb(string @mahocvien)
        {
            //CHỈ DÙNG Ở CBB
            var c = (from g in context.TT_GHIDANHs
                     from dk in context.TT_DANGKYKHOAHOCs
                     from k in context.KHOAHOCs
                     from l in context.LOPHOCs
                     where dk.MADANGKYKHOAHOC == g.MADANGKYKHOAHOC
                     && dk.MAKHOAHOC == k.MAKHOAHOC
                     && dk.MAHOCVIEN == mahocvien
                        && g.MALOPHOC == l.MALOPHOC
                        && l.MAKHOAHOC == k.MAKHOAHOC
                     select new
                     {
                         Key = g.MALOPHOC,
                         Value = k.TENKHOAHOC + " (" + g.MALOPHOC + ")"
                     });

            List<object> list = c.ToList<object>();
            list.Insert(0, new { Key = "ALL", Value = "TẤT CẢ LỚP HỌC" });

            return list;
        }

        public List<object> HienThiHocVienLopHoc_Cbb(string @malop)//thông tin ghi danh 
        {
            var a = (from gd in context.TT_GHIDANHs
                     where gd.MALOPHOC == malop
                     select gd.MADANGKYKHOAHOC); //mã học viên đã ghi dnah lớp đó

            var c = (from dk in context.TT_DANGKYKHOAHOCs
                     where a.Contains(dk.MADANGKYKHOAHOC)
                     select new
                     {
                         Key = dk.MADANGKYKHOAHOC,
                         Value = dk.MAHOCVIEN
                     });
            return c.ToList<object>();
        }
        public List<object> HienThiLopHocKhoaHoc_Cbb(string @makhoa)
        {
            var c = (from lh in context.LOPHOCs
                     from kh in context.KHOAHOCs
                     where lh.MAKHOAHOC == kh.MAKHOAHOC
                        && lh.MAKHOAHOC == @makhoa
                     select new
                     {
                         Key = lh.MALOPHOC,
                         Value = kh.TENKHOAHOC + " - " + lh.MALOPHOC
                     });
            return c.ToList<object>();
        }
        public List<object> HienThiTatCaKhoaHoc_Cbb()
        {
            var c = (from kh in context.KHOAHOCs
                     select new
                     {
                         Key = kh.MAKHOAHOC,
                         Value = kh.TENKHOAHOC
                     });
            return c.ToList<object>();
        }
        public List<object> HienThiTatCaGiangVien_Cbb()
        {
            var c = (from kh in context.GIANGVIENs
                     select new
                     {
                         Key = kh.MAGIANGVIEN,
                         Value = kh.TENGIANGVIEN
                     });
            return c.ToList<object>();
        }
        public List<object> HienThiTatCaNhomNguoiDung_Cbb()
        {
            var c = (from kh in context.NHOMNGUOIDUNGs
                     select new
                     {
                         Key = kh.MANHOMNGUOIDUNG,
                         Value = kh.TENNHOMNGUOIDUNG
                     });
            return c.ToList<object>();
        }
        public List<object> HienThiTatCaLoaiBienLai_Cbb()
        {
            var c = (from bl in context.LOAI_BIENLAIs
                     select new
                     {
                         Key = bl.MALOAIBIENLAI,
                         Value = bl.TENLOAIBIENLAI
                     });
            return c.ToList<object>();
        }

        public List<object> HienThiDangKyKhoaHocHocVien_Cbb(string makhoahoc, string mahocvien)
        {
            var c = (from dk in context.TT_DANGKYKHOAHOCs
                     where dk.MAKHOAHOC == makhoahoc
                     && dk.MAHOCVIEN == mahocvien
                     select new
                     {
                         Key = dk.MADANGKYKHOAHOC,
                         Value = dk.MADANGKYKHOAHOC
                     });
            return c.ToList<object>();
        }

        public List<object> HienThiKhoaHocHocVienDangKy_Cbb(string mahocvien)
        {
            var c = (from dk in context.TT_DANGKYKHOAHOCs
                     from kh in context.KHOAHOCs
                     where dk.MAKHOAHOC == kh.MAKHOAHOC
                      && dk.MAHOCVIEN == mahocvien
                     select new
                     {
                         Key = kh.MAKHOAHOC,
                         Value = kh.TENKHOAHOC
                     });
            return c.ToList<object>();
        }

        #endregion


        //lịch
        #region LỊCH

        public List<Lich> LayLichHocVienDangKy(string madangkykhoahoc)
        {
            //lich đăng ký
            var c = from chongiohoc in context.TT_CHONGIOHOCs
                    from thu in context.THUs
                    where chongiohoc.MADANGKYKHOAHOC == madangkykhoahoc
                    && thu.MATHU == chongiohoc.MATHU

                    select new Lich((int)thu.THU_INT, int.Parse(chongiohoc.MACAHOC.Substring(2)));
            return c.ToList<Lich>();
        }
        public List<Lich> LayLichTrongHocVien(string mahocvien)
        {
            //đung lịch
            var g = from lichhoc in context.LICHHOCs
                    from lophoc in context.LOPHOCs
                    from cahoc in context.CAHOCs
                    from ghidanh in context.TT_GHIDANHs
                    from dangky in context.TT_DANGKYKHOAHOCs
                    where dangky.MADANGKYKHOAHOC == ghidanh.MADANGKYKHOAHOC
                    && lichhoc.MACA == cahoc.MACA
                       && lichhoc.MALOPHOC == lophoc.MALOPHOC
                       && ghidanh.MALOPHOC == lophoc.MALOPHOC
                       && dangky.MAHOCVIEN == mahocvien
                       && lichhoc.TINHTRANG == "CHƯA KẾT THÚC"
                    group new { lichhoc, lophoc, cahoc, ghidanh }
                    by new { Ca = cahoc.MACA, Thu = (int)lichhoc.NGAYHOC.DayOfWeek }
                      into grp
                    select new { Thu = grp.Key.Thu, Ca = grp.Key.Ca };

            //toàn bộ
            var f = from thu in context.THUs
                    from cahoc in context.CAHOCs
                    select new { Thu = (int)thu.THU_INT, Ca = cahoc.MACA };

            //lich ko đụng
            List<Lich> lich = (from fi in f
                               where !g.Contains(new { Thu = fi.Thu, Ca = fi.Ca })
                               select new Lich(fi.Thu, int.Parse(fi.Ca.Substring(2)))).ToList();
            return lich;
        }
        public List<Lich> LayLichTrongGiangVien(string magiangvien)
        {
            //lich dạy giảng viên
            var g = from lichhoc in context.LICHHOCs
                    from lophoc in context.LOPHOCs
                    from cahoc in context.CAHOCs
                    where lichhoc.MALOPHOC == lophoc.MALOPHOC
                       && lichhoc.MACA == cahoc.MACA
                       && lophoc.MAGIANGVIEN == magiangvien
                       && lichhoc.TINHTRANG == "CHƯA KẾT THÚC"
                    group new { lichhoc, lophoc, cahoc }
                    by new { Ca = cahoc.MACA, Thu = (int)lichhoc.NGAYHOC.DayOfWeek }
                    into grp
                    select new { Thu = grp.Key.Thu, Ca = grp.Key.Ca };

            //toàn bộ lịch
            var f = from thu in context.THUs
                    from cahoc in context.CAHOCs
                    select new { Thu = (int)thu.THU_INT, Ca = cahoc.MACA };

            //lịch trống
            List<Lich> lich = (from fi in f
                               where !g.Contains(new { Thu = fi.Thu, Ca = fi.Ca })
                               select new Lich(fi.Thu, int.Parse(fi.Ca.Substring(2)))).ToList();
            return lich;
        }
        public List<Lich> LayLichTrongTKBHocVienDangKy(List<string> ds_madangky, string magiangvien)
        {
            if (ds_madangky.Count == 0)
                return null;
            //List<Lich> lich = LayLichTrongGiangVien(magiangvien);
            List<Lich> lich = LayLichHocVienDangKy(ds_madangky[0]);

            for (int i = 1; i < ds_madangky.Count; i++)
            {
                List<Lich> next = LayLichHocVienDangKy(ds_madangky[i]);
                lich = lich.Where(t => next.FirstOrDefault(u => u.Ca == t.Ca && u.Thu == t.Thu) != null).ToList<Lich>();
            }
            return lich.OrderBy(t => t.Thu).ThenBy(t => t.Ca).ToList<Lich>();
        }
        #endregion




    }
}
