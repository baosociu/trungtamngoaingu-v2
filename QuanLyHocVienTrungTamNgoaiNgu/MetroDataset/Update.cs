﻿using EventHelper;
using MetroDataset.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetroDataset
{
    public class Update
    {
        DataQuanLyHocVienDataContext context;
        public Update()
        {
            string connectString = Helper.GetStringConnection();
            context = new DataQuanLyHocVienDataContext(connectString);
        }

        private bool Submit()
        {
            return EventHelper.Helper.TryCatch(context.SubmitChanges, () => { });
        }

        public ResultExcuteQuery UpdateStudent(HOCVIEN _hv)
        {
            bool r = false;
            HOCVIEN hv = context.HOCVIENs.SingleOrDefault(t => t.MAHOCVIEN == _hv.MAHOCVIEN);
            if (hv != null)
            {
                hv.TENHOCVIEN = _hv.TENHOCVIEN;
                hv.GIOITINH = _hv.GIOITINH;
                hv.NGAYSINH = _hv.NGAYSINH;
                hv.SODIENTHOAI = hv.SODIENTHOAI;
                hv.CMND = _hv.CMND;
                hv.DIACHI = _hv.DIACHI;
                hv.MAIL = _hv.MAIL;
                hv.HINH = _hv.HINH;
                hv.GHICHU = _hv.GHICHU;
                r = Submit();
            }
            return new ResultExcuteQuery(r, "Cập nhật thông tin học viên " + (r ? "thành công" : "thất bại"));
        }

        public ResultExcuteQuery UpdateTeacher(GIANGVIEN _gv)
        {
            bool r = false;
            GIANGVIEN gv = context.GIANGVIENs.SingleOrDefault(t => t.MAGIANGVIEN == _gv.MAGIANGVIEN);
            if (gv != null)
            {
                gv.TENGIANGVIEN = _gv.TENGIANGVIEN;
                gv.GIOITINH = _gv.GIOITINH;
                gv.NGAYSINH = _gv.NGAYSINH;
                gv.SODIENTHOAI = _gv.SODIENTHOAI;
                gv.CMND = _gv.CMND;
                gv.DIACHI = _gv.DIACHI;
                gv.MAIL = _gv.MAIL;
                gv.CHUYENMON = _gv.CHUYENMON;
                gv.TRINHDO = _gv.TRINHDO;
                gv.GHICHU = _gv.GHICHU;
                gv.HINH = _gv.HINH;
                r = Submit();
            }
            return new ResultExcuteQuery(r, "Cập nhật thông tin giảng viên " + (r ? "thành công" : "thất bại"));
        }
        public ResultExcuteQuery CapNhatKhoaHoc(KHOAHOC _kh)
        {
            bool r = false;
            KHOAHOC kh = context.KHOAHOCs.SingleOrDefault(t => t.MAKHOAHOC == _kh.MAKHOAHOC);
            if (kh != null)
            {
                kh.HOCPHI = _kh.HOCPHI;
                kh.TENKHOAHOC = _kh.TENKHOAHOC;
                r = Submit();
            }
            return new ResultExcuteQuery(r, "Cập nhật thông tin khóa học " + (r ? "thành công" : "thất bại"));
        }


        public ResultExcuteQuery ThayDoiLichHoc(LICHHOC lichhoc)
        {
            LICHHOC _lich = context.LICHHOCs.FirstOrDefault(t => t.MALOPHOC == lichhoc.MALOPHOC && t.MALICHHOC == lichhoc.MALICHHOC);
            if (_lich != null)
            {
                foreach (LICHHOC l in context.LICHHOCs)
                    if (l.NGAYHOC.ToShortDateString() == lichhoc.NGAYHOC.ToShortDateString() && l.MACA == lichhoc.MACA)
                    {
                        if (l.MALOPHOC == lichhoc.MALOPHOC)
                            return new ResultExcuteQuery(false, "Lịch học thay đổi trùng với thông tin giờ học của lịch " + lichhoc.MALICHHOC + ", lớp " + lichhoc.MALOPHOC + ". Hãy thay đổi với thông tin khác.");
                        else
                            if (l.MACOSO == lichhoc.MACOSO && l.MAPHONG == lichhoc.MAPHONG)
                            return new ResultExcuteQuery(false, "Lịch học thay đổi trùng với thông tin của lịch " + lichhoc.MALICHHOC + ", lớp " + lichhoc.MALOPHOC + ". Hãy thay đổi với thông tin khác.");
                    }

                _lich.MACOSO = lichhoc.MACOSO;
                _lich.MAPHONG = lichhoc.MAPHONG;
                _lich.MACA = lichhoc.MACA;
                _lich.NGAYHOC = lichhoc.NGAYHOC;
                bool r = Submit();
                return new ResultExcuteQuery(r, "Thay đổi lịch học " + (r ? "thành công" : "thất bại"));

            }
            return new ResultExcuteQuery(false, "Không tìm thấy thông tin lịch học");
        }

        public ResultExcuteQuery CapNhatMaLopBienLai(List<string> madangky, string malop)
        {
            List<BIENLAI> lst = new List<BIENLAI>();
            foreach(string madk in madangky)
            {
                List<BIENLAI> b = context.BIENLAIs.Where(t => t.MADANGKYKHOAHOC == madk).ToList();
                b.ForEach(t => t.MALOPHOC = malop);
                lst.AddRange(b);
            }
            bool r = Submit();
            return new ResultExcuteQuery(r, "Cập nhật thông tin biên lai" + (r ? "thành công" : "thất bại"))
;        }

        public ResultExcuteQuery UpdateStaff(NHANVIEN _nv)
        {
            bool r = false;
            NHANVIEN nv = context.NHANVIENs.SingleOrDefault(t => t.MANHANVIEN == _nv.MANHANVIEN);
            if (nv != null)
            {
                nv.TENNHANVIEN = _nv.TENNHANVIEN;
                nv.GIOITINH = _nv.GIOITINH;
                nv.NGAYSINH = _nv.NGAYSINH;
                nv.SODIENTHOAI = _nv.SODIENTHOAI;
                nv.CMND = _nv.CMND;
                nv.DIACHI = _nv.DIACHI;
                nv.MAIL = _nv.MAIL;
                nv.GHICHU = _nv.GHICHU;
                nv.HINH = _nv.HINH;
                r = Submit();
            }
            return new ResultExcuteQuery(r, "Cập nhật thông tin nhân viên " + (r ? "thành công" : "thất bại"));
        }

        public bool CapNhatThaoTacCuaNhomNguoiDung(string @manhomnguoidung, string @magiaodien, string @mathaotac, bool @checked)
        {
            int count = (from data in context.NHOMNGUOIDUNG_GIAODIENs
                         where data.MAGIAODIEN == magiaodien
                         && data.MANHOMNGUOIDUNG == manhomnguoidung
                         && data.MATHAOTAC == mathaotac
                         select data).ToList().Count;
            if (count == 1)
            {
                if (!@checked)
                    return (new Delete()).XoaNhomNguoiDungGiaoDien(magiaodien, manhomnguoidung, mathaotac);
            }
            else
                return (new Insert()).ThemNhomNguoiDungGiaoDien(magiaodien, manhomnguoidung, mathaotac);
            return true;
        }

        public ResultExcuteQuery CapNhatTTDangKyKhoaHoc(TT_DANGKYKHOAHOC ttdangky)
        {
            bool r = false;
            TT_DANGKYKHOAHOC _dangky = context.TT_DANGKYKHOAHOCs.FirstOrDefault(T => T.MADANGKYKHOAHOC == ttdangky.MADANGKYKHOAHOC);
            if (_dangky != null)
            {
                _dangky.DIEMDAUVAO = ttdangky.DIEMDAUVAO;
                _dangky.NGAY = ttdangky.NGAY;
                r = Submit();
            }
            //else
            //{
            //    //thêm
            //    r = (new Insert()).ThemDiemDauVao(diemdauvao).isSuccessfully();
            //}
            return new ResultExcuteQuery(r, "Cập nhật điểm đầu vào " + (r ? "thành công" : "thất bại"));
        }

        public bool CapNhatTTChonGioHocVien(string @madangkykhoahoc, string @maca, string @mathu, bool @checked)
        {
            int count = (from data in context.TT_CHONGIOHOCs
                         where data.MADANGKYKHOAHOC == @madangkykhoahoc
                         && data.MACAHOC == @maca
                         && data.MATHU == @mathu
                         select data).ToList().Count;
            if (count == 1)
            {
                if (!@checked)
                    return (new Delete()).XoaTTChonGioHoc(madangkykhoahoc, @maca, @mathu);
            }
            else
                return (new Insert()).ThemTTChonGioHoc(madangkykhoahoc, @maca, @mathu);
            return true;
        }

        public ResultExcuteQuery CapNhatHoatDongNguoiDungTrongNhomNguoiDung(string @manhomnguoidung, string @manguoidung)
        {
            NGUOIDUNG_NHOMNGUOIDUNG n = context.NGUOIDUNG_NHOMNGUOIDUNGs.SingleOrDefault(
                c => c.MANHOMNGUOIDUNG == manhomnguoidung && c.MATAIKHOAN == manguoidung);
            bool r = false;
            if (n != null)
            {
                n.HOATDONG = !n.HOATDONG;
                r = Submit();
            }
            return new ResultExcuteQuery(r, "Thay đổi quyền hạn "+(r?"thành công":"thất bại"));
        }

        //tự động cập nhật lịch học đã kết thúc hay chưa
        public bool CapNhatTinhTrangLichHoc()
        {
            DateTime now = DateTime.Now;
            var lichhoc = (from lh in context.LICHHOCs where lh.NGAYHOC.Date < now.Date select lh).ToList();
            lichhoc.ForEach(l => l.TINHTRANG = "Đã kết thúc".ToUpper());
            return Submit();
        }

        public void CapNhatTinhTrangLopHoc()
        {
            var grp = (from lh in context.LICHHOCs
                       from lop in context.LOPHOCs
                       where lh.MALOPHOC == lop.MALOPHOC
                       && lh.TINHTRANG == "Chưa kết thúc".ToUpper()
                        && lop.TINHTRANG == "Chưa kết thúc".ToUpper()
                       group lh by new { lh.MALOPHOC } into g
                       select new { g.Key.MALOPHOC, count = g.Count() })
                       .Where(t => t.count == 0);

            foreach (var g in grp)
            {
                if (g.count == 0)
                {
                    //cập nahatj lại tình trjang lớp học là đã kết thúc
                    LOPHOC lop = context.LOPHOCs.FirstOrDefault(t => t.MALOPHOC == g.MALOPHOC);
                    if (lop != null)
                    {
                        lop.TINHTRANG = "ĐÃ KẾT THÚC";
                        Submit();
                    }
                }
            }
        }



        public ResultExcuteQuery CapNhatDiemHocVienLopHoc(DIEM _d)
        {
            bool r = false;

            DIEM d = context.DIEMs.SingleOrDefault(c => c.MADIEM == _d.MADIEM && c.MADANGKYKHOAHOC == _d.MADANGKYKHOAHOC && c.MALOPHOC == _d.MALOPHOC);
            if (d != null)
            {
                d.DIEM1 = _d.DIEM1;
                d.LAN = _d.LAN;
                d.MALOAIDIEM = _d.MALOAIDIEM;
                r = Submit();
            }
            return new ResultExcuteQuery(r, "Cập nhật thông tin điểm số của học viên " + (r ? "thành công" : "thất bại"));
        }

        public bool CapNhatDiemDanhHocVien(string @mahocvien, string @malich, string @malophoc, bool @comat)
        {
            var c = (from g in context.TT_GHIDANHs
                     from d in context.TT_DANGKYKHOAHOCs
                     where g.MALOPHOC == @malophoc
                     && d.MAHOCVIEN == mahocvien
                      && g.MADANGKYKHOAHOC == d.MADANGKYKHOAHOC
                     select new { g.MADANGKYKHOAHOC }).FirstOrDefault();
            if (c == null)
                return false;
            string madangkykhoahoc = c.MADANGKYKHOAHOC;
            CT_BUOIHOC buoihoc
                = context.CT_BUOIHOCs.SingleOrDefault(ct => ct.MADANGKYKHOAHOC == @madangkykhoahoc
                        && ct.MALOPHOC == malophoc
                        && ct.MALICHHOC == malich);

            if (buoihoc != null)
            {
                buoihoc.DIEMDANH = @comat;
                return Submit();
            }
            else
                return (new Insert()).ThemDiemDanhHocVien(@madangkykhoahoc, @malich, @malophoc, @comat);
        }

        public ResultExcuteQuery HuyBienLai(string mabienlai, string manhanvien)
        {
            BIENLAI bl = context.BIENLAIs.FirstOrDefault(t => t.MABIENLAI == mabienlai);
            if (bl != null)
            {

                (new Insert()).ThemLichSuHuyBienLai(bl, manhanvien);
                context.BIENLAIs.DeleteOnSubmit(bl);
                bool r = Submit();
                return new ResultExcuteQuery(r, "Huỷ biên lai " + mabienlai + " " + (r ? "thành công" : "thất bại"));
            }
            return new ResultExcuteQuery(false, "Không tìm thấy thông tin biên lai " + mabienlai);
        }

        public bool CapNhatHoatDongTaiKhoanNguoiDung(string @mataikhoan, bool @hoatdong)
        {
            NGUOIDUNG nguoidung = context.NGUOIDUNGs.SingleOrDefault(g => g.MATAIKHOAN == mataikhoan);
            if (nguoidung != null)
            {
                nguoidung.HOATDONG = @hoatdong;
                return Submit();
            }
            return false;
        }
        //Đổi mật khẩu người dùng
        public ResultExcuteQuery DoiMatKhau(string @mataikhoan, string @matkhau)
        {
            bool r = false;
            NGUOIDUNG nguoidung = context.NGUOIDUNGs.SingleOrDefault(nd => nd.MATAIKHOAN == mataikhoan);
            if (nguoidung != null)
            {
                nguoidung.MATKHAU = matkhau;
                r = Submit();
            }
            return new ResultExcuteQuery(r, "Cập nhật mật khẩu " + (r ? "thành công" : "thất bại"));
        }
        //Cập nhật thông tin biên lai
        public ResultExcuteQuery UpdateReceipt(BIENLAI tt_bienlai)
        {
            bool r = false;
            BIENLAI bl = context.BIENLAIs.SingleOrDefault(t => t.MABIENLAI == tt_bienlai.MABIENLAI);
            if (bl != null)
            {
                bl.MALOAIBIENLAI = tt_bienlai.MALOAIBIENLAI;
                bl.DOT = tt_bienlai.DOT;
                bl.SOTIEN = tt_bienlai.SOTIEN;
                bl.NGAYXACNHAN = tt_bienlai.NGAYXACNHAN;
                r = Submit();
            }
            return new ResultExcuteQuery(r, "Cập nhật thông tin biên lai " + tt_bienlai.MABIENLAI + (r ? "thành công" : "thất bại"));
        }

    }
}
