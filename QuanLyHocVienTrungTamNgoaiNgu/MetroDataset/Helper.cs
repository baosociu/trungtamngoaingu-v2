﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MetroDataset
{
    public class Helper
    {
        static string connectionString;
        public static string GetValuesFromDictionary(Dictionary<string, string> dictionary, string key)
        {
            string r;
            dictionary.TryGetValue(key, out r);
            return r;
        }



        public static string GetStringConnection()
        {
            if (connectionString != null)
                return connectionString;

            connectionString =
            EventHelper.Helper.TryCatchReturnValue(() =>
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                string connectString = config.AppSettings.Settings["ConnectString"].Value;
                return connectString;
            }).ToString();

            return connectionString;
        }

        //phương thức chuyển string to byte[] để lưu ảnh dưới dạng dữ liệu text
        public static byte[] GetBytes(string str)
        {
            byte[] arr = Encoding.ASCII.GetBytes(str);
            return arr;
        }

        public static String GetStringTime(TimeSpan ts)
        {
            string h = ts.Hours.ToString();
            string m = ts.Minutes.ToString();
            string str_h = (h.Length == 1 ? "0" : "") + h;
            string str_m = (m.Length == 1 ? "0" : "") + m;
            return str_h + ":" + str_m;
        }

        public static string[] GetValuesFromAnonymous(object v)
        {
            string str = v.ToString();
            str = str.Replace("{ ", "");
            str = str.Replace(" }", "");
            List<string> list = new List<string>();
            PropertyInfo[] properties = v.GetType().GetProperties();
            int n = properties.Length;
            for (int i = 0; i < n - 1; i++)
            {
                PropertyInfo p = properties[i];
                string name_p = p.Name + " = ";

                PropertyInfo p_next = properties[i + 1];
                string name_p_next = ", " + p_next.Name + " = ";

                int start = str.IndexOf(name_p) + name_p.Length;
                int length = str.IndexOf(name_p_next) - start;
                list.Add(str.Substring(start, length));
            }
            string name = properties[n - 1].Name + " = ";

            list.Add(str.Substring(str.IndexOf(name) + name.Length));
            return list.ToArray();
        }

        public static bool CheckConnection()
        {
            String str = GetStringConnection();
            try
            {
                SqlConnection connection = new SqlConnection(str);
                connection.Open();
                connection.Close();
                return true;
            }
            catch (Exception E)
            {
                connectionString = null;
            }
            return false;
        }

        public static string ReverseString(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        public static void SetConnectionNull()
        {
            connectionString = null;
        }

        public static string DecodeFromUtf8(string utf8String)
        {
            byte[] utf8Bytes = new byte[utf8String.Length];
            for (int i = 0; i < utf8String.Length; ++i)
                utf8Bytes[i] = (byte)utf8String[i];

            return Encoding.UTF8.GetString(utf8Bytes, 0, utf8Bytes.Length);
        }


        public class HoTen
        {
            string ho;
            string ten;

            public HoTen(string ho, string ten)
            {
                this.ho = ho;
                this.ten = ten;
            }

            public string Ho { get => ho; set => ho = value; }
            public string Ten { get => ten; set => ten = value; }
        }

        public static HoTen GetHoTen(string fullname)
        {
            fullname = fullname.Trim();
            if (fullname.Contains(" "))
            {
                string ten = fullname.Substring(fullname.LastIndexOf(" ") + 1);
                string ho = fullname.Substring(0, fullname.LastIndexOf(" "));
                return new HoTen(ho, ten);
            }
            return new HoTen("", fullname);
        }

        public static string idUser;
    }
}
