﻿using EventHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetroDataset
{
    public class Delete
    {

        DataQuanLyHocVienDataContext context;
        public Delete()
        {
            string connectString = Helper.GetStringConnection();
            context = new DataQuanLyHocVienDataContext(connectString);
        }

        private bool Submit()
        {
            return EventHelper.Helper.TryCatch(context.SubmitChanges, () => { });
        }

        public bool XoaNguoiDungTuNhomNguoiDung(string @manhom, string @mataikhoan)
        {
            NGUOIDUNG_NHOMNGUOIDUNG n = context.NGUOIDUNG_NHOMNGUOIDUNGs.SingleOrDefault(C => C.MANHOMNGUOIDUNG == @manhom && C.MATAIKHOAN == @mataikhoan);
            if (n != null)
                context.NGUOIDUNG_NHOMNGUOIDUNGs.DeleteOnSubmit(n);
            return Submit();
        }

        public bool XoaNhomNguoiDungGiaoDien(string @magiaodien, string @manhomnguoidung, string @mathaotac)
        {
            NHOMNGUOIDUNG_GIAODIEN n = context.NHOMNGUOIDUNG_GIAODIENs.SingleOrDefault(
                g => g.MAGIAODIEN == magiaodien && g.MATHAOTAC == mathaotac && g.MANHOMNGUOIDUNG == manhomnguoidung
                );
            if (n != null)
            {
                context.NHOMNGUOIDUNG_GIAODIENs.DeleteOnSubmit(n);
                return Submit();
            }
            return false;
        }

        public bool XoaTTChonGioHoc(string madangkykhoahoc, string @macahoc, string @mathu)
        {
            TT_CHONGIOHOC n = context.TT_CHONGIOHOCs.SingleOrDefault(
                g => g.MACAHOC == @macahoc 
                && g.MADANGKYKHOAHOC == madangkykhoahoc
                && g.MATHU == @mathu
                );
            if (n != null)
            {
                context.TT_CHONGIOHOCs.DeleteOnSubmit(n);
                return Submit();
            }
            return false;
        }

        public ResultExcuteQuery XoaNhomNguoiDung(string @manhom)
        {
            bool b =false;

            if (manhom != "XMASTER'" )
            {
                NGUOIDUNG_NHOMNGUOIDUNG _n = context.NGUOIDUNG_NHOMNGUOIDUNGs.FirstOrDefault(t => t.MANHOMNGUOIDUNG == @manhom);
                if (_n != null)
                    return new ResultExcuteQuery(false, "Hãy chắc chắn rằng nhóm người dùng \'" + manhom + "\' không tồn tại người dùng nào");

                NHOMNGUOIDUNG n = context.NHOMNGUOIDUNGs.SingleOrDefault(
                    t => t.MANHOMNGUOIDUNG == manhom);
                if (n != null)
                {
                    //xoá giao diên cho nhóm người dùng
                    List<NHOMNGUOIDUNG_GIAODIEN> nhomnguoidung_giaodien = context.NHOMNGUOIDUNG_GIAODIENs.Where(t => t.MANHOMNGUOIDUNG == n.MANHOMNGUOIDUNG).ToList<NHOMNGUOIDUNG_GIAODIEN>();
                    context.NHOMNGUOIDUNG_GIAODIENs.DeleteAllOnSubmit(nhomnguoidung_giaodien);
                    context.NHOMNGUOIDUNGs.DeleteOnSubmit(n);
                    b = Submit();
                }
            }
            return new ResultExcuteQuery(b, "Xoá nhóm người dùng \'" + manhom + "\' " + (b ? "thành công": "thất bại"));
        }

        public ResultExcuteQuery XoaTaiKhoan(string mataikhoan)
        {
            bool r = false;
            //kiểm tra người dùng này có tồn tại trong nhóm người dùng ?
            NGUOIDUNG_NHOMNGUOIDUNG _n = context.NGUOIDUNG_NHOMNGUOIDUNGs.FirstOrDefault(t => t.MATAIKHOAN == mataikhoan);
            if (_n != null)
                return new ResultExcuteQuery(false,"Tài khoản \'" + mataikhoan + "\' tồn tại trong nhóm quyền \'" + _n.MANHOMNGUOIDUNG + "\'. Không thể xoá");
           
            //xoá thông tin người dùng
            string id_group = EventHelper.Helper.TryCatchReturnValue(() => { return mataikhoan.Substring(0, 2); }).ToString();
            bool @check = false;
            if (id_group == "HV")
                @check = XoaHocVien(mataikhoan);
            else if (id_group == "GV")
                @check = XoaGiangVien(mataikhoan);
            else if (id_group == "NV")
                @check = XoaNhanVien(mataikhoan);
            r = @check;

            //xoá tài khoản
            NGUOIDUNG nguoidung = context.NGUOIDUNGs.SingleOrDefault(t => t.MATAIKHOAN == mataikhoan);
            if (nguoidung == null)
                return new ResultExcuteQuery(false, "Không tồn tại tài khoản \'" + mataikhoan + "\'");
            if(r)
            {
                context.NGUOIDUNGs.DeleteOnSubmit(nguoidung);
                r = Submit();
            }
            return new ResultExcuteQuery(r, "Xoá tài khoản \'" + mataikhoan + "\' "+(r ? "thành công" : "thất bại"));
        }

        public bool XoaTTBackup(TT_BACKUP backup)
        {
            TT_BACKUP _backup = context.TT_BACKUPs.FirstOrDefault(t => t.MABACKUP == backup.MABACKUP);
            if (_backup == null)
                return false;
            context.TT_BACKUPs.DeleteOnSubmit(_backup);
            return Submit();
        }
        public ResultExcuteQuery XoaKhoaHoc(string makhoahoc)
        {
            KHOAHOC kh = context.KHOAHOCs.FirstOrDefault(t => t.MAKHOAHOC == makhoahoc);
            if (kh != null)
            {
                if (kh.LOPHOCs.Count == 0)
                {
                    context.KHOAHOCs.DeleteOnSubmit(kh);
                    bool r = Submit();
                    return new ResultExcuteQuery(r, "Xóa khóa học " + (r ? "thành công" : "thất bại"));
                }
                else
                    return new ResultExcuteQuery(false, "Không thể xóa khóa học "+makhoahoc+" vì khóa học đang mở lớp học !");
            }
            else
                return new ResultExcuteQuery(false, "Hãy chọn khóa học để xóa !");

        }

        public ResultExcuteQuery XoaPhieuDangKyKhoaHoc(string madangkykhoahoc)
        {
            bool r = false;
            TT_DANGKYKHOAHOC dk = context.TT_DANGKYKHOAHOCs.SingleOrDefault(t => t.MADANGKYKHOAHOC == madangkykhoahoc);
            if (dk != null)
            {
                var c = context.TT_CHONGIOHOCs.Where(t => t.MADANGKYKHOAHOC == dk.MADANGKYKHOAHOC);
                context.TT_CHONGIOHOCs.DeleteAllOnSubmit(c);
                context.TT_DANGKYKHOAHOCs.DeleteOnSubmit(dk);
                r = Submit();
            }
            return new ResultExcuteQuery(r, "Huỷ phiếu đăng ký \'" + dk.MADANGKYKHOAHOC + "\' " + (r ? "thành công" : "thất bại"));
        }

        private bool XoaNhanVien(string mataikhoan)
        {
            NHANVIEN nhanvien = context.NHANVIENs.SingleOrDefault(t => t.MATAIKHOAN == mataikhoan);
            if (nhanvien == null)
                return false;
            context.NHANVIENs.DeleteOnSubmit(nhanvien);
            return Submit();
        }

        private bool XoaGiangVien(string mataikhoan)
        {
            GIANGVIEN giangvien = context.GIANGVIENs.SingleOrDefault(t => t.MATAIKHOAN == mataikhoan);
            if (giangvien == null)
                return false;
            context.GIANGVIENs.DeleteOnSubmit(giangvien);
            return Submit();
        }

        private bool XoaHocVien(string mataikhoan)
        {
            HOCVIEN hocvien = context.HOCVIENs.SingleOrDefault(t => t.MATAIKHOAN == mataikhoan);
            if (hocvien == null)
                return false;
            context.HOCVIENs.DeleteOnSubmit(hocvien);
            return Submit();
        }

        public ResultExcuteQuery XoaNguoiDungTrongNhomNguoiDung(string manhomnguoidung, string manguoidung)
        {
            bool r = false;
            NGUOIDUNG_NHOMNGUOIDUNG n = context.NGUOIDUNG_NHOMNGUOIDUNGs.SingleOrDefault(t => t.MANHOMNGUOIDUNG == manhomnguoidung && t.MATAIKHOAN == manguoidung);
            if(n != null)
            {
                context.NGUOIDUNG_NHOMNGUOIDUNGs.DeleteOnSubmit(n);
                r = Submit();
            }
            return new ResultExcuteQuery(r, "Xoá người dùng " + (r ? "thành công" : "thất bại")); 
        }
    }
}
