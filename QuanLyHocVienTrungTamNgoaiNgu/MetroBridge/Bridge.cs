﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MetroBridge
{
    public abstract partial class Bridge : UserControl
    {
        public Bridge()
        {
            InitializeComponent();
        }

        public abstract void Init();

        public abstract string GetId();
        public abstract string GetTitle();

    }
}
